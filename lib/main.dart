import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:slova_lms/binding/Splash_binding.dart';
import 'package:slova_lms/binding/app_binding.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/translations/localization_service.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';
import 'package:slova_lms/view/mobile/screen_config/screen_config_controller.dart';
import 'commom/utils/global.dart';
import 'commom/utils/preference_utils.dart';
import 'package:firebase_core/firebase_core.dart';
import 'dart:io';
import 'data/base_service/socket_io.dart';
import 'firebase_options.dart';
import 'notification/notification_controller.dart';
import 'notification/notification_service.dart';
import 'package:flutter_device_type/flutter_device_type.dart';

void main() async {
  HttpOverrides.global = MyHttpOverrides();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
  await PreferenceUtils.init();
  // await AppCache().setUpNotificationSound();
  await Permission.notification.isDenied.then((value) {
    if (value) {
      Permission.notification.request();
    }
  });
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  AppBinding().dependencies();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  ScreenConfigController screenConfigController = Get.find<ScreenConfigController>();

  void setValueLandScapeStudentScreen(Orientation orientation){
    if(Get.isRegistered<StudentHomeController>()) {
      if (orientation == Orientation.portrait) {
        Get.find<StudentHomeController>().isLandScape.value = false;
      }else{
        Get.find<StudentHomeController>().isLandScape.value = true;
      }
      Get.find<StudentHomeController>().update();
    }
  }
  @override
  Widget build(BuildContext context) {
    LocalNotificationService.initialize(context);

   if(Device.get().isTablet && GetPlatform.isAndroid){
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp
    ]);}
    return OrientationBuilder(
        builder: (BuildContext context, Orientation orientation) {
          if (mounted) {
            if (Device.get().isTablet) {
              if (orientation == Orientation.portrait) {
                screenConfigController.setIsIpadLandScape(false);
                screenConfigController.setSize(const Size(600, 780));
              } else {
                screenConfigController.setIsIpadLandScape(true);
                screenConfigController.setSize(const Size(600, 780));
              }
              setValueLandScapeStudentScreen(orientation);
            } else {
               screenConfigController.setSize(const Size(360, 690));
            }
          }
          return GetBuilder<ScreenConfigController>(
              builder: (screenConfigController1) {
                return ScreenUtilInit(
                  splitScreenMode: true,
                  minTextAdapt: true,
                  designSize: screenConfigController1.size,
                  builder: (context, widget) => GetMaterialApp(
                    title: 'LMS',
                    color: Colors.white,
                    debugShowCheckedModeBanner: false,
                    // theme: ThemeData(useMaterial3: true, colorScheme: lightColorScheme),
                    // darkTheme: ThemeData(useMaterial3: true, colorScheme: darkColorScheme),
                    builder: EasyLoading.init(builder: ((context, widget) {
                      return MediaQuery(
                        data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                        child: GestureDetector(
                            onTap: () => dismissKeyboard(),
                            behavior: HitTestBehavior.translucent,
                            child: widget!),
                      );
                    })),
                    locale: LocalizationService.locale,
                    translations: LocalizationService(),
                    getPages: AppPages.pages,
                    initialRoute: Routes.splash,

                    localizationsDelegates: const [
                      GlobalMaterialLocalizations.delegate,
                      // DefaultCupertinoLocalizations.delegate,
                      GlobalCupertinoLocalizations.delegate, // Here !
                      DefaultWidgetsLocalizations.delegate,
                    ],
                    supportedLocales: const [
                      Locale('vi'),
                      Locale('en'),
                    ],
                  ),
                );
              }
          );
        });
  }

}

