import 'package:get/get.dart';
import 'package:slova_lms/binding/Splash_binding.dart';
import 'package:slova_lms/view/mobile/account/changePassword/change_pass_page.dart';
import 'package:slova_lms/view/mobile/account/recovery/password_recovery_page.dart';
import 'package:slova_lms/view/mobile/account/resetPass/reset_pass_page.dart';
import 'package:slova_lms/view/mobile/account/verifyCode/verify_code_page.dart';
import 'package:slova_lms/view/mobile/event_news/all_event_news/all_event_news.dart';
import 'package:slova_lms/view/mobile/event_news/detail_event_news/detail_event_news.dart';
import 'package:slova_lms/view/mobile/event_news/event_news_page.dart';
import 'package:slova_lms/view/mobile/message/search_message/search_message_page.dart';
import 'package:slova_lms/view/mobile/message/send_new_message/create_new_group_chat/create_new_group_chat_page.dart';
import 'package:slova_lms/view/mobile/role/manager/diligent_management_manager/detail_list_class_by_manager/list_class_in_block_diligent_manager_page.dart';
import 'package:slova_lms/view/mobile/role/manager/transcript_manager/list_block_transcript/list_class_transcript/class_transcript/class_transcript_page.dart';
import 'package:slova_lms/view/mobile/role/manager/transcript_manager/statistical_transcipt_manager/statistical_transcipt_manager_page.dart';
import 'package:slova_lms/view/mobile/role/parent/detail_subject_parent/view_exam_parent/detail_exam_parent/detail_exam_parent_page.dart';
import 'package:slova_lms/view/mobile/role/parent/detail_subject_parent/view_exam_parent/view_exam_parent_page.dart';
import 'package:slova_lms/view/mobile/role/parent/detail_subject_parent/view_exercise_parent/view_exercise_parent_page.dart';
import 'package:slova_lms/view/mobile/role/parent/diligence_parent/statistical_parent/detail_statistical_parent/detail_all_stastical_parent_page.dart';
import 'package:slova_lms/view/mobile/role/parent/diligence_parent/statistical_parent/detail_statistical_parent/detail_statistical_parent_page.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_page.dart';
import 'package:slova_lms/view/mobile/role/student/detail_subject/detail_subject_student_page.dart';
import 'package:slova_lms/view/mobile/role/student/detail_subject/view_exam_student/detail_exam_student/student_doing_exam/student_doing_exam_page.dart';
import 'package:slova_lms/view/mobile/role/student/detail_subject/view_exam_student/view_exam_student_page.dart';
import 'package:slova_lms/view/mobile/role/student/detail_subject/view_exercise/detail_exercise_student/detail_exercise_student_page.dart';
import 'package:slova_lms/view/mobile/role/student/detail_subject/view_exercise/view_exercise_student_page.dart';
import 'package:slova_lms/view/mobile/role/student/diligence_student/statistical_student/detail_statistical_student/detail_all_stastical_page.dart';
import 'package:slova_lms/view/mobile/role/student/diligence_student/statistical_student/detail_statistical_student/detail_statistical_student_page.dart';
import 'package:slova_lms/view/mobile/role/student/personal_information_student/detail_avatar/detail_avatar_page.dart';
import 'package:slova_lms/view/mobile/role/student/personal_information_student/personal_information_student_page.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_page.dart';
import 'package:slova_lms/view/mobile/role/student/subjects/subjects_page.dart';
import 'package:slova_lms/view/mobile/role/teacher/student_list/student_list_page.dart';
import 'package:slova_lms/view/mobile/schedule/schedule_page.dart';
import 'package:slova_lms/view/mobile/schedule/teaching_schedule/teaching_schedule_page.dart';
import 'package:slova_lms/view/mobile/splash/splash_page.dart';
import '../binding/dashboard_binding.dart';
import '../binding/login_binding.dart';
import '../view/mobile/account/login/login_page.dart';
import '../view/mobile/home/home_page.dart';
import '../view/mobile/message/message_content/add_user_group_chat/add_user_group_chat_page.dart';
import '../view/mobile/message/message_content/list_user_in_group_chat/detail_message_private/detail_message_private_page.dart';
import '../view/mobile/message/message_content/list_user_in_group_chat/information_user/information_user_page.dart';
import '../view/mobile/message/message_content/list_user_in_group_chat/list_user_in_group_chat_page.dart';
import '../view/mobile/message/message_content/message_content_page.dart';
import '../view/mobile/message/search_message/history_search_message/history_search_message_page.dart';
import '../view/mobile/message/send_new_message/create_new_group_chat/list_user_by_class_create_new_group_chat/create_new_group_chat_all_user/create_new_group_chat_all_user_page.dart';
import '../view/mobile/message/send_new_message/create_new_group_chat/list_user_by_class_create_new_group_chat/create_new_group_chat_parent/create_new_group_chat_parent_page.dart';
import '../view/mobile/message/send_new_message/create_new_group_chat/list_user_by_class_create_new_group_chat/create_new_group_chat_student/create_new_group_chat_student_page.dart';
import '../view/mobile/message/send_new_message/create_new_group_chat/list_user_by_class_create_new_group_chat/create_new_group_chat_teacher/create_new_group_chat_teacher_page.dart';
import '../view/mobile/message/send_new_message/send_new_message_page.dart';
import '../view/mobile/notification/detail_notify_other/detail_notify_other_page.dart';
import '../view/mobile/phonebook/detail_phonebook/Detail_information_by_phonebook/detail_information_phonebook_page.dart';
import '../view/mobile/phonebook/detail_phonebook/detail_phonebook_page.dart';
import '../view/mobile/role/manager/diligent_management_manager/detail_list_class_by_manager/detail_diligent_by_class_manager/detail_diligent_by_class_manager_page.dart';
import '../view/mobile/role/manager/schedule_manager/list_class_in_block_manager/list_class_in_block_manager_page.dart';
import '../view/mobile/role/manager/transcript_manager/list_block_transcript/list_block_transcript_manager_page.dart';
import '../view/mobile/role/manager/transcript_manager/list_block_transcript/list_class_transcript/list_class_in_block_transcript_manager_page.dart';
import '../view/mobile/role/manager/transcript_manager/list_subject_manager/list_subject_by_class_manager_page.dart';
import '../view/mobile/role/manager/transcript_manager/list_subject_manager/transcript_subject_manager/transcript_subject_manager_page.dart';
import '../view/mobile/role/manager/transcript_manager/statistical_transcipt_manager/statistical_transcript_by_top/statistical_transcript_by_top_block/statistical_transcript_by_top_block_page.dart';
import '../view/mobile/role/manager/transcript_manager/statistical_transcipt_manager/statistical_transcript_by_top/statistical_transcript_by_top_page.dart';
import '../view/mobile/role/manager/transcript_manager/statistical_transcipt_manager/statistical_transcript_by_top/statistical_transcript_top_class/statistical_transcript_top_class_page.dart';
import '../view/mobile/role/manager/transcript_manager/statistical_transcipt_manager/statistical_transcript_by_top/statistical_transcript_top_school/statistical_transcript_top_school_page.dart';
import '../view/mobile/role/manager/transcript_manager/statistical_transcipt_manager/statistical_transcript_school_in_percent/list_student_statistical_person/list_student_statistical_person_page.dart';
import '../view/mobile/role/manager/transcript_manager/statistical_transcipt_manager/statistical_transcript_school_in_percent/statistical_transcript_school_in_percent_page.dart';
import '../view/mobile/role/manager/transcript_manager/transcipt_synthetic_manager/view_transcipt_synthetic_manager_page.dart';
import '../view/mobile/role/manager/transcript_manager/transcript_manager_page.dart';
import '../view/mobile/role/manager/update_infomation_manager/update_information_manager_page.dart';
import '../view/mobile/role/parent/detail_subject_parent/detail_subject_parent_page.dart';
import '../view/mobile/role/parent/detail_subject_parent/view_exercise_parent/detail_exercise_parent/detail_exercise_parent_page.dart';
import '../view/mobile/role/parent/detail_subject_parent/view_transcript_subject_parent/view_transcript_subject_parent_page.dart';
import '../view/mobile/role/parent/diligence_parent/approval_parent/approval_parent_page.dart';
import '../view/mobile/role/parent/diligence_parent/approval_parent/cancel_leaving_application/cancel_leave_application_page.dart';
import '../view/mobile/role/parent/diligence_parent/approval_parent/create_a_leave_application/create_a_leave_application_page.dart';
import '../view/mobile/role/parent/diligence_parent/approval_parent/detail_leave_application/detail_leave_application_page.dart';
import '../view/mobile/role/parent/diligence_parent/approval_parent/edit_leaving_application/edit_leaving_application_page.dart';
import '../view/mobile/role/parent/diligence_parent/diligence_parent_page.dart';
import '../view/mobile/role/parent/personal_information_parent/personal_information_parent_page.dart';
import '../view/mobile/role/parent/personal_information_parent/update_infomation_parent/update_infomation_parent_page.dart';
import '../view/mobile/role/parent/transcrip_synthetic_parent/transcrip_synthetic_parent_page.dart';
import '../view/mobile/role/student/detail_subject/view_exam_student/detail_exam_student/detail_exam_student_page.dart';
import '../view/mobile/role/student/detail_subject/view_exercise/detail_exercise_student/student_doing_homework/student_doing_homework_page.dart';
import '../view/mobile/role/student/detail_subject/view_exercise/detail_exercise_student/view_all_notification/detail_notification_subject/detail_notification_subject_page.dart';
import '../view/mobile/role/student/detail_subject/view_exercise/detail_exercise_student/view_all_notification/view_all_notification_page.dart';
import '../view/mobile/role/student/detail_subject/view_transcript_subject_student/view_transcript_subject_student_page.dart';
import '../view/mobile/role/student/diligence_student/diligence_monitor_student/detail_diligent_management_teacher/detail_diligent_management_student_page.dart';
import '../view/mobile/role/student/diligence_student/diligence_monitor_student/diligence_monitor_student_page.dart';
import '../view/mobile/role/student/diligence_student/diligence_student_page.dart';
import '../view/mobile/role/student/personal_information_student/update_information_student/update_information_student_page.dart';
import '../view/mobile/role/student/transcrip_synthetic_student/transcrip_synthetic_student_page.dart';
import '../view/mobile/role/teacher/diligent_management_teacher/detail_diligent_management_teacher/detail_diligent_management_teacher_page.dart';
import '../view/mobile/role/teacher/diligent_management_teacher/diligent_management_teacher_page.dart';
import '../view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/approved_leave_application_teacher/detail_approved_leave_application_teacher/detail_approved_leave_application_teacher_page.dart';
import '../view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/cancel_leave_application_teacher/detail_cancel_leave_application_teacher/detail_cancel_leave_application_teacher_page.dart';
import '../view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/manager_leave_application_teacher_page.dart';
import '../view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/pending_teacher/detail_pending_teacher/detail_pending_teacher_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/creat_exercise/creat_exercise_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/create_exam/create_exam_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/learning_management_teacher_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/list_all_notify_subject/create_new_notification/create_notify_subject_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/list_all_notify_subject/list_all_notify_subject_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/list_all_notify_subject/update_notify_subject/update_notify_subject_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/result_exam/list_student_exam_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/result_exam/list_student_submit_result_exam/result_exam_question/result_exam_question_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/result_exam/list_student_submit_result_exam/result_exam_upload_file/result_exam_upload_file_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/result_exercise/list_student_excise_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/result_exercise/list_student_submit_result_exercise/result_exercise_assign_link/result_exercise_assign_link_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/result_exercise/list_student_submit_result_exercise/result_exercise_question/result_exercise_question_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/result_exercise/list_student_submit_result_exercise/result_exercise_upload_file/result_exercise_upload_file_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/detail_exam/detail_exam_assign_link/detail_exam_assign_link_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/detail_exam/detail_exam_upload_file/detail_exam_upload_file_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/detail_exam/view_exam_question/view_exam_question_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/grading_exam/grading_exam_assign_link/grading_exercise_assign_link_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/grading_exam/grading_exam_question/grading_exam_question_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/grading_exam/grading_exam_upload_file/grading_exam_upload_file_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/grading_exam/list_student_submitted_exam/detail_student_do_exam_assign_link/detail_student_do_exam_assign_link_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/grading_exam/list_student_submitted_exam/detail_student_do_exam_question/detail_student_do_exam_question_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/grading_exam/list_student_submitted_exam/detail_student_do_exam_upload_file/detail_student_do_exam_upload_file_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/update_exam/update_exam_assign_link/update_exam_assign_link_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/update_exam/update_exam_question/update_exam_question_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/update_exam/update_exam_upload_file/update_exam_upload_file_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/view_exam_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/detail_exercise/detail_exercise_assign_link/detail_exercise_assign_link_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/detail_exercise/detail_exercise_upload_file/detail_exercise_upload_file_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/detail_exercise/view_exercise_question/view_exercise_question_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/grading_exercise/grading_exercise_assign_link/grading_exercise_assign_link_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/grading_exercise/grading_exercise_question/grading_exercise_question_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/grading_exercise/grading_exercise_upload_file/grading_exercise_upload_file_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/grading_exercise/list_student_submitted_exercise/student_do_exercise_assign_link/student_do_exercise_assign_link_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/grading_exercise/list_student_submitted_exercise/student_do_exercise_question/student_do_exercise_question_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/grading_exercise/list_student_submitted_exercise/student_do_exercise_upload_file/student_do_exercise_upload_file_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/update_exercise/update_exercise_assign_link/update_exercise_assign_link_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/update_exercise/update_exercise_question/update_exercise_question_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/update_exercise/update_exercise_upload_file/update_exercise_upload_file_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/view_exercise_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_transcripts_teacher/detail_transcript_teacher/detail_transcript_teacher_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_transcripts_teacher/list_transcripts_teacher_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_transcripts_teacher/update_transcript_subject/update_transcript_teacher_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/subjects_teacher_page.dart';
import '../view/mobile/role/teacher/learning_management_teacher/view_transcript_synthetic_teacher/view_transcript_synthetic_teacher_page.dart';
import '../view/mobile/role/teacher/send_notification_teacher/detail_send_notification_in_app_teacher/detail_send_notification_in_app_teacher_page.dart';
import '../view/mobile/role/teacher/send_notification_teacher/detail_send_notify_sms/detail_send_notify_sms_page.dart';
import '../view/mobile/role/teacher/send_notification_teacher/list_class_by_send_notification_in_app/list_class_by_send_notification_in_app_page.dart';
import '../view/mobile/role/teacher/send_notification_teacher/list_class_by_send_notification_in_app/list_user_by_class_send_notification/send_notify_all_user/list_all_user_by_class_send_notification_page.dart';
import '../view/mobile/role/teacher/send_notification_teacher/list_class_by_send_notification_in_app/list_user_by_class_send_notification/send_notify_parent/send_notify_parent_page.dart';
import '../view/mobile/role/teacher/send_notification_teacher/list_class_by_send_notification_in_app/list_user_by_class_send_notification/send_notify_student/send_notify_student_page.dart';
import '../view/mobile/role/teacher/send_notification_teacher/list_class_by_send_notification_in_app/list_user_by_class_send_notification/send_notify_teacher/send_notify_teacher_page.dart';
import '../view/mobile/role/teacher/send_notification_teacher/send_notification_teacher_page.dart';
import '../view/mobile/role/teacher/update_infomation_teacher/update_infomation_teacher_page.dart';
import '../view/mobile/staticPage/static_page_page.dart';

part './app_routes.dart';

class AppPages {
  static final pages = [
    GetPage(
        name: Routes.splash,
        page: () => SplashPage(),
        binding: SplashBinding()),
    GetPage(
        name: Routes.login, page: () => LoginPage(), binding: LoginBinding()),
    GetPage(name: Routes.recoveryPass, page: () => PasswordRecoveryPage()),
    GetPage(name: Routes.retrievingCode, page: () => VerifyCodePage()),
    GetPage(name: Routes.resetPass, page: () => ResetPassPage()),
    GetPage(name: Routes.home, page: () => const HomePage(), binding: HomeBinding()),
    GetPage(name: Routes.dashboardParent, page: () => ParentHomePage()),
    GetPage(name: Routes.dashboardStudent, page: () => StudentHomePage()),
    GetPage(name: Routes.schedule, page: () => SchedulePage()),
    GetPage(name: Routes.scheduleTeaching, page: () => TeachingSchedulePage()),
    GetPage(
        name: Routes.personalInformation,
        page: () => PersonalInformationStudentPage()),
    GetPage(name: Routes.subject, page: () => SubjectPage()),
    GetPage(name: Routes.detailPhonebook, page: () => DetailPhoneBookPage()),
    GetPage(
        name: Routes.updateInfoTeacher, page: () => UpdateInfoTeacherPage()),
    GetPage(
        name: Routes.updateInfoStudent, page: () => UpdateInfoStudentPage()),
    GetPage(
        name: Routes.updateInfoManager, page: () => UpdateInfoManagerPage()),
    GetPage(name: Routes.updateInfoParent, page: () => UpdateInfoParentPage()),
    GetPage(name: Routes.studentListPage, page: () => StudentListPage()),
    GetPage(
        name: Routes.personalInfoParent,
        page: () => PersonalInformationParentPage()),
    GetPage(name: Routes.schedulePage, page: () => SchedulePage()),
    GetPage(name: Routes.changePass, page: () => ChangePassPage()),
    GetPage(name: Routes.getavatar, page: () => DetailAvatarPage()),
    GetPage(name: Routes.staticPage, page: () => StaticPage()),
    GetPage(
        name: Routes.listClassInBlock,
        page: () => ListClassInBlockManagerPage()),
    GetPage(
        name: Routes.detailJobByTeacherInTimeTable,
        page: () => TeachingSchedulePage()),
    GetPage(
        name: Routes.infoInPhoneBook,
        page: () => DetailInformationPhoneBookPage()),
    GetPage(
        name: Routes.listClassInBlockDiligentManager,
        page: () => ListClassInBlockDiligentManagerPage()),
    GetPage(name: Routes.approvalParent, page: () => ApprovalParentPage()),
    GetPage(
        name: Routes.createALeavingApplication,
        page: () => CreateALeavingApplicationPage()),
    GetPage(
        name: Routes.editLeavingApplication,
        page: () => EditLeavingApplicationPage()),
    GetPage(
        name: Routes.cancelLeavingApplication,
        page: () => CancelLeaveApplicationPage()),
    GetPage(
        name: Routes.diligentManagementTeacher,
        page: () => DiligentManagementTeacherPage(),),
    GetPage(
        name: Routes.detailPendingLeaveApplicationTeacher,
        page: () => DetailPendingLeaveApplicationTeacherPage()),
    GetPage(
        name: Routes.detailCancelLeaveApplicationTeacher,
        page: () => DetailCancelLeaveApplicationTeacherPage()),
    GetPage(
        name: Routes.detailApprovedLeaveApplicationTeacher,
        page: () => DetailApprovedTeacherPage()),
    GetPage(name: Routes.diligenceStudent, page: () => DiligenceStudentPage()),
    GetPage(
        name: Routes.diligenceMonitorStudent,
        page: () => DiligentManagementMonitorStudentPage()),
    GetPage(
        name: Routes.diligenceParentPage, page: () => DiligenceParentPage()),
    GetPage(
        name: Routes.detailstatisticalparentpage,
        page: () => DetailStatisticalParentPage()),
    GetPage(
        name: Routes.detailDiligenceStudent,
        page: () => DetailStatisticalStudentPage()),
    GetPage(
        name: Routes.detailDiligentManagementTeacherPage,
        page: () => DetailDiligentManagementTeacherPage()),
    GetPage(
        name: Routes.detailDiligentManagementStudentPage,
        page: () => DetailDiligentManagementStudentPage()),
    GetPage(
        name: Routes.detailDiligentByClassManager,
        page: () => DetailDiligentByClassManagerPage()),
    GetPage(
        name: Routes.managerLeaveApplicationTeacherPage,
        page: () => ManagerLeaveApplicationTeacherPage()),
    GetPage(
        name: Routes.detailAllStasticalPage,
        page: () => DetailStatisticalAllStudentPage()),
    GetPage(
        name: Routes.detailLeaveApplicationParentPage,
        page: () => DetailLeaveApplicationParentPage()),
    GetPage(
        name: Routes.detailAllStasticalPageParent,
        page: () => DetailAllStatisticalParentPage()),
    GetPage(name: Routes.createExercisePage, page: () => CreateExercisePage()),
    GetPage(name: Routes.createExamPage, page: () => CreateExamPage()),
    GetPage(
        name: Routes.subjectLearningManagementTeacherPage,
        page: () => const SubjectLearningManagementTeacherPage()),
    GetPage(
        name: Routes.detailSubjectStudentPage,
        page: () => DetailSubjectStudentPage()),
    GetPage(
        name: Routes.detailSubjectParentPage,
        page: () => DetailSubjectParentPage()),
    GetPage(
        name: Routes.detailExerciseStudentPage,
        page: () => DetailExerciseStudentPage()),
    GetPage(
        name: Routes.detailExamsStudentPage,
        page: () => DetailExamStudentPage()),
    GetPage(
        name: Routes.detailExamsParentPage,
        page: () => DetailExamParentPage()),
    GetPage(
        name: Routes.studentDoingExercise,
        page: () => StudentDoingHomeWorkPage()),
    GetPage(name: Routes.studentDoingExam, page: () => StudentDoingExamPage()),
    GetPage(
        name: Routes.detailExerciseParentPage,
        page: () => DetailExerciseParentPage()),
    GetPage(
        name: Routes.detailNotificationSubjectPage,
        page: () => DetailNotificationSubjectPage()),
    GetPage(
        name: Routes.viewExerciseStudentPage,
        page: () => ViewExerciseStudentPage()),
    GetPage(
        name: Routes.viewExerciseParentPage,
        page: () => ViewExerciseParentPage()),
    GetPage(
        name: Routes.viewExamStudentPage, page: () => ViewExamStudentPage()),
    GetPage(name: Routes.viewExamParentPage, page: () => ViewExamParentPage()),
    GetPage(
        name: Routes.viewExerciseTeacherPage,
        page: () => ViewExerciseTeacherPage()),
    GetPage(
        name: Routes.listClasBySendNotificationPage,
        page: () => ListClassSendNotificationPage()),
    GetPage(
        name: Routes.listUserByClassSendNotificationPage,
        page: () => ListAllUserByClassSendNotificationPage()),
    GetPage(
        name: Routes.createNewNotifySubjectPage,
        page: () => CreateNewNotifySubjectPage()),
    GetPage(name: Routes.subjectTeacherPage, page: () => const SubjectTeacherPage()),
    GetPage(
        name: Routes.detailExerciseAssignLinkPage,
        page: () => DetailExerciseAssignLinkPage()),
    GetPage(
        name: Routes.detailExerciseUploadFilePage,
        page: () => DetailExerciseUploadFilePage()),
    GetPage(
        name: Routes.viewExerciseQuestionPage,
        page: () => ViewExerciseQuestionPage()),
    GetPage(
        name: Routes.updateExerciseAssignLinkPage,
        page: () => UpdateExerciseAssignLinkPage()),
    GetPage(
        name: Routes.updateExerciseUploadFilePage,
        page: () => UpdateExerciseUploadFilePage()),
    GetPage(
        name: Routes.updateExerciseQuestionPage,
        page: () => UpdateExerciseQuestionPage()),
    GetPage(
        name: Routes.listAllNotifySubjectPage,
        page: () => ListAllNotifySubjectPage()),
    GetPage(
        name: Routes.updateNotifySubjectPage,
        page: () => UpdateNotifySubjectPage()),
    GetPage(
        name: Routes.gradingExerciseAssignLinkPage,
        page: () => GradingExerciseAssignLinkPage()),
    GetPage(
        name: Routes.detailStudentDoExerciseAssignLinkPage,
        page: () => StudentDoExerciseAssignLinkPage()),
    GetPage(
        name: Routes.gradingExerciseUploadFilePage,
        page: () => GradingExerciseUploadFilePage()),
    GetPage(
        name: Routes.detailStudentDoExerciseUploadFilePage,
        page: () => StudentDoExerciseUploadFilePage()),
    GetPage(
        name: Routes.gradingExerciseQuestionPage,
        page: () => GradingExerciseQuestionPage()),
    GetPage(
        name: Routes.detailStudentDoExerciseQuestionPage,
        page: () => StudentDoExerciseQuestionPage()),
    GetPage(
        name: Routes.viewExamTeacherPage, page: () => ViewExamTeacherPage()),
    GetPage(
        name: Routes.detailExamAssignLinkPage,
        page: () => DetailExamAssignLinkPage()),
    GetPage(
        name: Routes.detailExamUploadFilePage,
        page: () => DetailExamUploadFilePage()),
    GetPage(
        name: Routes.viewExamQuestionPage, page: () => ViewExamQuestionPage()),
    GetPage(
        name: Routes.viewAllNotification,
        page: () => ViewAllNotificationPage()),
    GetPage(
        name: Routes.updateExamAssignLinkPage,
        page: () => UpdateExamAssignLinkPage()),
    GetPage(
        name: Routes.updateExamUploadFilePage,
        page: () => UpdateExamUploadFilePage()),
    GetPage(
        name: Routes.updateExamQuestionPage,
        page: () => UpdateExamQuestionPage()),
    GetPage(
        name: Routes.gradingExamAssignLinkPage,
        page: () => GradingExamAssignLinkPage()),
    GetPage(
        name: Routes.detailStudentDoExamAssignLinkPage,
        page: () => DetailStudentDoExamAssignLinkPage()),
    GetPage(
        name: Routes.gradingExamUploadFilePage,
        page: () => GradingExamUploadFilePage()),
    GetPage(
        name: Routes.detailStudentDoExamUploadFilePage,
        page: () => DetailStudentDoExamUploadFilePage()),
    GetPage(
        name: Routes.gradingExamQuestionPage,
        page: () => GradingExamQuestionPage()),
    GetPage(
        name: Routes.detailStudentDoExamQuestionPage,
        page: () => DetailStudentDoExamQuestionPage()),
    GetPage(
        name: Routes.listStudentExercisePage,
        page: () => ListStudentExercisePage()),
    GetPage(
        name: Routes.resultExerciseQuestionPage,
        page: () => ResultExerciseQuestionPage()),
    GetPage(
        name: Routes.resultExerciseUploadFilePage,
        page: () => ResultExerciseUploadFilePage()),
    GetPage(
        name: Routes.resultExerciseAssignLinkPage,
        page: () => ResultExerciseAssignLinkPage()),
    GetPage(
        name: Routes.listStudentExamPage, page: () => ListStudentExamPage()),
    GetPage(
        name: Routes.resultExamQuestionPage,
        page: () => ResultExamQuestionPage()),
    GetPage(
        name: Routes.resultExamUploadFilePage,
        page: () => ResultExamUploadFilePage()),
    GetPage(
        name: Routes.listTranscriptsSubjectTeacherPage,
        page: () => ListTranscriptsSubjectTeacherPage()),
    GetPage(
        name: Routes.transcriptsSyntheticTeacherPage,
        page: () => TranscriptsSyntheticTeacherPage()),
    GetPage(
        name: Routes.detailTranscriptPage,
        page: () => DetailTranscriptSubjectPage()),
    GetPage(
        name: Routes.transcriptsSyntheticStudentPage,
        page: () => TranscriptsSyntheticStudentPage()),
    GetPage(
        name: Routes.viewTranscriptSubjectStudentPage,
        page: () => ViewTranscriptSubjectStudentPage()),
    GetPage(
        name: Routes.transcriptsSyntheticParentPage,
        page: () => TranscriptsSyntheticParentPage()),
    GetPage(
        name: Routes.viewTranscriptSubjectParentPage,
        page: () => ViewTranscriptSubjectParentPage()),
    GetPage(
        name: Routes.updateTranscriptPage,
        page: () => UpdateTranscriptTeacherPage()),
    GetPage(
        name: Routes.transcriptManagerPage,
        page: () => TranscriptManagerPage()),
    GetPage(
        name: Routes.listBlockTranscriptManagerPage,
        page: () => ListBlockTranscriptManagerPage()),
    GetPage(
        name: Routes.listClassInBlockTranscriptManagerPage,
        page: () => ListClassInBlockTranscriptManagerPage()),
    GetPage(
        name: Routes.classTranscriptManagerPage,
        page: () => ClassTranscriptManagerPage()),
    GetPage(
        name: Routes.viewTranscriptsSyntheticManagerPage,
        page: () => ViewTranscriptsSyntheticManagerPage()),
    GetPage(
        name: Routes.listSubjectByClassManagerPage,
        page: () => ListSubjectByClassManagerPage()),
    GetPage(
        name: Routes.viewTranscriptSubjectManagerController,
        page: () => ListTranscriptSubjectManagerPage()),
    GetPage(
        name: Routes.statisticalTranscriptManagerPage,
        page: () => StatisticalTranscriptManagerPage()),
    GetPage(
        name: Routes.statisticalTranscriptByTopPage,
        page: () => StatisticalTranscriptByTopPage()),
    GetPage(
        name: Routes.statisticalTranscriptTopSchoolPage,
        page: () => StatisticalTranscriptTopSchoolPage()),
    GetPage(
        name: Routes.statisticalTranscriptTopBlockPage,
        page: () => StatisticalTranscriptTopBlockPage()),
    GetPage(
        name: Routes.statisticalTranscriptTopClassPage,
        page: () => StatisticalTranscriptTopClassPage()),
    GetPage(
        name: Routes.statisticalTranscriptSchoolInPercentPage,
        page: () => StatisticalTranscriptSchoolInPercentPage()),
    GetPage(
        name: Routes.listStudentTranscriptPercentPage,
        page: () => ListStudentTranscriptPercentPage()),
    GetPage(
        name: Routes.listEventNewsPage,
        page: () => const EventNewsPage()),
    GetPage(
        name: Routes.detailNewsPage,
        page: () => DetailNewsPage()),
    GetPage(
        name: Routes.allEventNewsPage,
        page: () => AllEventNewsPage()),
    GetPage(name: Routes.searchMessagePage, page: () => SearchMessagePage()),
    GetPage(name: Routes.messageContent, page: () => MessageContent()),
    GetPage(
        name: Routes.detailNotifyOtherPage,
        page: () => DetailNotifyOtherPage()),
    GetPage(
        name: Routes.createNewGroupChatPage,
        page: () => CreateNewGroupChatPage()),
    GetPage(
        name: Routes.createNewGroupChatStudentPage,
        page: () => CreateNewGroupChatStudentPage()),
    GetPage(
        name: Routes.createNewGroupChatParentPage,
        page: () => CreateNewGroupChatParentPage()),
    GetPage(
        name: Routes.createNewGroupChatTeacherPage,
        page: () => CreateNewGroupChatTeacherPage()),
    GetPage(
        name: Routes.createNewGroupChatAllUserPage,
        page: () => CreateNewGroupChatAllUserPage()),
    GetPage(
        name: Routes.listUserInGroupChatPage,
        page: () => ListUserInGroupChatPage()),
    GetPage(
        name: Routes.detailMessagePrivatePage,
        page: () => DetailMessagePrivatePage()),
    GetPage(
        name: Routes.addUserGroupChatPage,
        page: () => AddUserGroupChatPage()),
    GetPage(
        name: Routes.informationUserPage,
        page: () => InformationUserPage()),
    GetPage(
        name: Routes.historySearchMessagePage,
        page: () => HistorySearchMessagePage()),
    GetPage(
        name: Routes.sendNewMessagePage,
        page: () => SendNewMessagePage()),
    GetPage(
        name: Routes.sendNotifyParentPage,
        page: () => SendNotifyParentPage()),
    GetPage(
        name: Routes.sendNotifyStudentPage,
        page: () => SendNotifyStudentPage()),
    GetPage(
        name: Routes.sendNotifyTeacherPage,
        page: () => SendNotifyTeacherPage()),
    GetPage(
        name: Routes.detailSendSMSNotification,
        page: () => DetailSendSMSNotification()),
    GetPage(
        name: Routes.detailSendInAppNotification,
        page: () => DetailSendInAppNotification()),
    GetPage(
        name: Routes.sendNotificationTeacherPage,
        page: () => SendNotificationTeacherPage()),
  ];
}
