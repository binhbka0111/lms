part of './app_pages.dart';

abstract class Routes {
  static const splash = '/splash';
  static const login = '/login';
  static const recoveryPass = '/recoveryPass';
  static const retrievingCode = '/retrievingCode';
  static const resetPass = '/resetPass';
  static const home = '/homePage';
  static const dashboardStudent = '/dashboardStudent';
  static const dashboardParent = '/dashboardParent';
  static const dashboardTeacher = '/dashboardTeacher';
  static const dashboardManager = '/dashboardManager';
  static const schedule = '/schedule';
  static const scheduleTeaching = '/scheduleTeaching';
  static const personalInformation = '/personalInformation';
  static const subject = '/subject';
  static const detailPhonebook = '/detailPhonebook';
  static const updateInfoTeacher = '/updateInfoTeacher';
  static const updateInfoStudent = '/updateInfoStudent';
  static const updateInfoManager = '/updateInfoManager';
  static const updateInfoParent = '/updateInfoParent';
  static const studentListPage = '/studentListPage';
  static const personalInfoParent = '/personalInfoParent';
  static const schedulePage = '/schedulePage';
  static const changePass = '/changePass';
  static const getavatar = '/getavatar';
  static const staticPage = '/staticPage';
  static const infoInPhoneBook= '/infoInPhoneBook';
  static const listClassInBlock= '/listClassInBlock';
  static const detailJobByTeacherInTimeTable= '/detailJobByTeacherInTimeTable';
  static const listClassInBlockDiligentManager= '/detaillistclassbymanager';
  static const detailDiligentByClassManager= '/detailDiligentByClassManager';
  static const detailAppoinment= '/detailappoinment';
  static const detailDiligenceStudent= '/detailDiligenceStudent';
  static const listDiligence= '/listDiligence';
  static const diligenceStudent= '/diligenceStudent';
  static const diligenceParentPage= '/diligenceParentPage';

  static const approvalParent= '/approvalParent';
  static const createALeavingApplication= '/createALeavingApplication';
  static const editLeavingApplication= '/editLeavingApplication';
  static const cancelLeavingApplication= '/cancelLeavingApplication';
  static const diligentManagementTeacher= '/diligentManagementTeacher';
  static const detailPendingLeaveApplicationTeacher= '/detailPendingLeaveApplicationTeacher';
  static const detailApprovedLeaveApplicationTeacher= '/detailApprovedLeaveApplicationTeacher';
  static const detailCancelLeaveApplicationTeacher= '/detailCancelLeaveApplicationTeacher';
  static const detailstatisticalparentpage= '/detailstatisticalparentpage';
  static const detailDiligentManagementTeacherPage= '/detailDiligentManagementTeacherPage';
  static const detailDiligentManagementStudentPage= '/detailDiligentManagementStudentPage';
  static const managerLeaveApplicationTeacherPage= '/managerLeaveApplicationTeacherPage';
  static const detailLeaveApplicationParentPage= '/detailLeaveApplicationParentPage';
  static const detailAllStasticalPage= '/detailAllStasticalPage';
  static const detailAllStasticalPageParent= '/detailAllStasticalPageParent';
  static const diligenceMonitorStudent= '/diligenceMonitorStudent';
  static const createExercisePage= '/createExercisePage';
  static const createExamPage= '/createExamPage';

  static const detailSubjectStudentPage= '/detailSubjectStudentPage';
  static const detailSubjectParentPage= '/detailSubjectParentPage';
  static const subjectLearningManagementTeacherPage= '/subjectLearningManagementTeacherPage';

  static const detailExerciseStudentPage= '/detailExerciseStudentPage';
  static const detailNotificationStudentPage= '/detailNotificationStudentPage';
  static const detailExamsStudentPage= '/detailExamsStudentPage';
  static const detailExamsParentPage= '/detailExamsParentPage';
  static const studentDoingExercise= '/studentDoingExercise';
  static const studentDoingExam= '/studentDoingExam';
  static const studentDoingExerciseMultiphechoice= '/studentDoingExerciseMultiphechoice';
  static const studentDoingExerciseEssay= '/studentDoingExerciseEssay';
  static const detailExerciseParentPage= '/detailExerciseParentPage';
  static const detailNotificationSubjectPage= '/detailNotificationSubjectPage';


  static const viewExerciseStudentPage= '/viewExerciseStudentPage';
  static const viewExerciseParentPage= '/viewExerciseParentPage';
  static const viewExamStudentPage= '/viewExamStudentPage';
  static const viewExamParentPage= '/viewExamParentPage';

  static const viewExerciseTeacherPage= '/viewExerciseTeacherPage';
  static const listClasBySendNotificationPage= '/listClasBySendNotificationPage';
  static const listUserByClassSendNotificationPage= '/listUserByClassSendNotificationPage';
  static const sendNotifyParentPage= '/sendNotifyParentPage';
  static const sendNotifyStudentPage= '/sendNotifyStudentPage';
  static const sendNotifyTeacherPage= '/sendNotifyStudentPage';

  static const detailSendInAppNotification= '/detailSendInAppNotification';



  static const createNewNotifySubjectPage= '/createNewNotifySubjectPage';
  static const subjectTeacherPage= '/subjectTeacherPage';

  static const detailExerciseAssignLinkPage= '/detailExerciseAssignLinkPage';
  static const detailExerciseUploadFilePage= '/detailExerciseUploadFilePage';
  static const viewExerciseQuestionPage= '/viewExerciseQuestionPage';

  static const updateExerciseAssignLinkPage= '/updateExerciseAssignLinkPage';
  static const updateExerciseUploadFilePage= '/updateExerciseUploadFilePage';
  static const updateExerciseQuestionPage= '/updateExerciseQuestionPage';

  static const listAllNotifySubjectPage= '/listAllNotifySubjectPage';
  static const updateNotifySubjectPage= '/updateNotifySubjectPage';

  static const gradingExerciseAssignLinkPage= '/gradingExerciseAssignLinkPage';
  static const detailStudentDoExerciseAssignLinkPage= '/detailStudentDoExerciseAssignLinkPage';
  static const gradingExerciseUploadFilePage= '/gradingExerciseUploadFilePage';
  static const detailStudentDoExerciseUploadFilePage= '/detailStudentDoExerciseUploadFilePage';
  static const gradingExerciseQuestionPage= '/gradingExerciseQuestionPage';
  static const detailStudentDoExerciseQuestionPage= '/detailStudentDoExerciseQuestionPage';
  static const viewExamTeacherPage= '/viewExamTeacherPage';

  static const detailExamAssignLinkPage= '/detailExamAssignLinkPage';
  static const detailExamUploadFilePage= '/detailExamUploadFilePage';
  static const viewExamQuestionPage= '/viewExamQuestionPage';
  static const viewAllNotification= '/viewAllNotification';

  static const updateExamAssignLinkPage= '/updateExamAssignLinkPage';
  static const updateExamUploadFilePage= '/updateExamUploadFilePage';
  static const updateExamQuestionPage= '/updateExamQuestionPage';

  static const gradingExamAssignLinkPage= '/gradingExamAssignLinkPage';
  static const detailStudentDoExamAssignLinkPage= '/detailStudentDoExamAssignLinkPage';
  static const gradingExamUploadFilePage= '/gradingExamUploadFilePage';
  static const detailStudentDoExamUploadFilePage= '/detailStudentDoExamUploadFilePage';
  static const gradingExamQuestionPage= '/gradingExamQuestionPage';
  static const detailStudentDoExamQuestionPage= '/detailStudentDoExamQuestionPage';


  static const listStudentExercisePage= '/listStudentExercisePage';
  static const resultExerciseQuestionPage= '/resultExerciseQuestionPage';
  static const resultExerciseAssignLinkPage= '/resultExerciseAssignLinkPage';
  static const resultExerciseUploadFilePage= '/resultExerciseUploadFilePage';


  static const listStudentExamPage= '/listStudentExamPage';
  static const resultExamQuestionPage= '/resultExamQuestionPage';
  static const resultExamAssignLinkPage= '/resultExamAssignLinkPage';
  static const resultExamUploadFilePage= '/resultExamUploadFilePage';


  static const listTranscriptsSubjectTeacherPage= '/listTranscriptsSubjectTeacherPage';
  static const transcriptsSyntheticTeacherPage= '/transcriptsSyntheticTeacherPage';
  static const detailTranscriptPage= '/detailTranscriptPage';

  static const transcriptsSyntheticStudentPage= '/transcriptsSyntheticStudentPage';
  static const viewTranscriptSubjectStudentPage= '/viewTranscriptSubjectStudentPage';


  static const transcriptsSyntheticParentPage= '/transcriptsSyntheticParentPage';
  static const viewTranscriptSubjectParentPage= '/viewTranscriptSubjectParentPage';
  static const updateTranscriptPage= '/updateTranscriptSubjectTeacherPage';


  static const transcriptManagerPage= '/transcriptManagerPage';
  static const listBlockTranscriptManagerPage= '/listBlockTranscriptManagerPage';
  static const listClassInBlockTranscriptManagerPage= '/listClassInBlockTranscriptManagerPage';
  static const classTranscriptManagerPage= '/classTranscriptManagerPage';
  static const viewTranscriptsSyntheticManagerPage= '/viewTranscriptsSyntheticManagerPage';
  static const listSubjectByClassManagerPage= '/listSubjectByClassManagerPage';
  static const viewTranscriptSubjectManagerController= '/viewTranscriptSubjectManagerController';
  static const statisticalTranscriptManagerPage= '/statisticalTranscriptManagerPage';
  static const statisticalTranscriptByTopPage= '/statisticalTranscriptByTopPage';
  static const statisticalTranscriptTopSchoolPage= '/statisticalTranscriptTopSchoolPage';
  static const statisticalTranscriptTopBlockPage= '/statisticalTranscriptTopBlockPage';
  static const statisticalTranscriptTopClassPage= '/statisticalTranscriptTopClassPage';
  static const statisticalTranscriptSchoolInPercentPage= '/statisticalTranscriptSchoolInPercentPage';
  static const listStudentTranscriptPercentPage= '/listStudentTranscriptPercentPage';

  static const listEventNewsPage = '/listEventNewsPage';
  static const detailNewsPage = '/detailNewsPage';
  static const allEventNewsPage = '/allEventNewsPage';

  static const detailSendSMSNotification= '/detailSendSMSNotification';
  static const messageContent= '/messageContent';
  static const searchMessagePage= '/searchMessagePage';
  static const detailNotifyOtherPage= '/detailNotifyOtherPage';
  static const createNewGroupChatPage= '/createNewGroupChatPage';
  static const createNewGroupChatStudentPage= '/createNewGroupChatStudentPage';
  static const createNewGroupChatParentPage= '/createNewGroupChatParentPage';
  static const createNewGroupChatTeacherPage= '/createNewGroupChatTeacherPage';
  static const createNewGroupChatAllUserPage= '/createNewGroupChatAllUserPage';
  static const listUserInGroupChatPage= '/listUserInGroupChatPage';
  static const detailMessagePrivatePage= '/detailMessagePrivatePage';
  static const addUserGroupChatPage= '/addUserGroupChatPage';
  static const informationUserPage= '/informationUserPage';
  static const historySearchMessagePage= '/historySearchMessagePage';
  static const sendNewMessagePage= '/sendNewMessagePage';
  static const sendNotificationTeacherPage= '/SendNotificationTeacherPage';



}
