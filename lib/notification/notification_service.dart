
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';

import '../routes/app_pages.dart';

class LocalNotificationService {
  static final FlutterLocalNotificationsPlugin _notificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static void initialize(BuildContext context) async {
    _notificationsPlugin.resolvePlatformSpecificImplementation<
        AndroidFlutterLocalNotificationsPlugin>()?.requestPermission();
    const InitializationSettings initializationSettings =
        InitializationSettings(
            android: AndroidInitializationSettings("@mipmap/ic_launcher"),
            iOS: DarwinInitializationSettings());
    _notificationsPlugin.initialize(

      initializationSettings,
      onDidReceiveNotificationResponse: (details) async {
        goNotify();
      },
      onDidReceiveBackgroundNotificationResponse: goNotifyOnBackGround,

    );
  }

  static void display(RemoteMessage message) async {
    try {
      final id = DateTime.now().millisecondsSinceEpoch ~/ 1000;
      NotificationDetails notificationDetails = NotificationDetails(
          android: AndroidNotificationDetails(
            "SLOVA ${DateTime.now().millisecond}", "SLOVA - LMS",
            importance: Importance.max,
            priority: Priority.high,
            icon: "@mipmap/ic_launcher",

            // enableVibration: AppCache().checkIsVibration()
          ),
          iOS: const DarwinNotificationDetails());
      await _notificationsPlugin.show(
        id,
        message.notification?.title,
        message.notification?.body,
        notificationDetails,
      );
    } on Exception catch (e) {
      AppUtils.shared.printLog(e);
    }
  }
}

Future<void> goNotify() async {
  Get.until((route) => route.settings.name == Routes.home);
  if (Get.isRegistered<HomeController>()) {
    Get.find<HomeController>().comeNotification();
  }
}

@pragma('vm:entry-point')
Future<void> goNotifyOnBackGround(NotificationResponse notificationResponse) async {
  try {
    if (Get.isRegistered<HomeController>()) {
      Get.until((route) => route.settings.name == Routes.home);
      Get.find<HomeController>().comeNotification();
    }else{
      AppCache().setIsClickNotifyWhenKillApp(true);
    }
  } catch (ex) {
    if (kDebugMode) {
      print(ex);
    }
  }

}
