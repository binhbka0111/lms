import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import '../routes/app_pages.dart';
import '../view/mobile/home/home_controller.dart';
import '../view/mobile/lifecycle_controller.dart';
import 'notification_service.dart';

class NotifyController extends GetxController {
  FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
  String newMessageId = '';

  @override
  void onInit() {
    configNotify();
    super.onInit();
  }

  configNotify() async {
    var  token = '';
    try{
      token = await FirebaseMessaging.instance.getToken()??'';
    }catch(e){
      print(e);
    }



    print("FCM TOKEN: $token");

    AppCache().fcmToken = token;

    ///gives you the message on which user taps
    ///and it opened the app from terminated state
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message != null) {
        RemoteNotification? notification = message.notification;
        AndroidNotification? android = message.notification?.android;
        if (notification != null && android != null) {
          LocalNotificationService.display(message);
        }
        AppCache().setIsClickNotifyWhenKillApp(true);
        if(message.data["total"] != null||message.data["total"] != "undefined"){
          FlutterAppBadger.updateBadgeCount(int.parse(message.data["total"]));
        }
      }
    });
    await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );

    ///foreground work
    FirebaseMessaging.onMessage.listen((message) {
      if (message.notification != null) {
        if(newMessageId == message.messageId){
          LocalNotificationService.display(message);
        }
        RemoteNotification? notification = message.notification;
        AndroidNotification? android = message.notification?.android;
        if (notification != null && android != null) {
          LocalNotificationService.display(message);
        }

        if(message.data["total"] != null||message.data["total"] != "undefined"){
          FlutterAppBadger.updateBadgeCount(int.parse(message.data["total"]));
        }

        newMessageId = message.messageId!;
        getCountNotify();
      }
    });

    ///When the app is in background but opened and user taps
    ///on the notification
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      Get.until((route) => route.settings.name == Routes.home);
      if (Get.isRegistered<HomeController>()) {
        Get.find<HomeController>().comeNotification();
      }
    });
  }



}

@pragma('vm:entry-point')
Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // LocalNotificationService.display(message);
  if(message.data["total"] != null ||message.data["total"] != "undefined" ){
    FlutterAppBadger.updateBadgeCount(int.parse(message.data["total"]));
  }
}
