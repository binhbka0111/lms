import '../../routes/app_pages.dart';
import '../../view/mobile/home/home_controller.dart';
import '../../view/mobile/notification/notification_controller.dart';
import '../../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/learning_management_teacher_controller.dart';
import 'package:get/get.dart';

import '../../view/mobile/role/teacher/learning_management_teacher/subjects_teacher_controller.dart';
clickSubjectTeacher() async{
  Get.put(SubjectTeacherController(),permanent: true);
  Get.put(LearningManagementTeacherController());
  if (Get.find<HomeController>().tabController?.index == Get.find<HomeController>().listTabView.indexOf(Get.find<HomeController>().notify)) {
    if(Get.find<SubjectTeacherController>().isClick){
      Get.find<LearningManagementTeacherController>().detailSubject.value = Get.find<SubjectTeacherController>().detailSubject.value;
    }else{
      Get.find<LearningManagementTeacherController>().detailSubject.value = Get.find<NotificationController>().detailSubject.value;
    }
  }else{
    Get.find<LearningManagementTeacherController>().detailSubject.value = Get.find<SubjectTeacherController>().detailSubject.value;
  }
  Get.find<LearningManagementTeacherController>().setUpData();
  Get.find<LearningManagementTeacherController>().getListNotify();
}


comeToHome(){
  Get.until((route) => route.settings.name == Routes.home);
  if(Get.isRegistered<SubjectTeacherController>()){
    Get.delete<SubjectTeacherController>(force: true);
  }

}