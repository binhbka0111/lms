import 'package:get/get.dart';
import '../../data/base_service/api_response.dart';
import '../../data/repository/chat/chat_repo.dart';
import '../../routes/app_pages.dart';
import '../../view/mobile/home/home_controller.dart';
import '../../view/mobile/message/message_controller.dart';
import '../constants/string_constant.dart';
import 'app_utils.dart';
import 'check_user_group_permission.dart';
import 'color_utils.dart';

final ChatRepo _chatRepo = ChatRepo();

createNewGroupChat(name, type, listId) {
  _chatRepo.createNewGroupChat(name, type, listId).then((value) async{
    if (value.state == Status.SUCCESS) {
      AppUtils.shared.showToast("Tạo nhóm chat thành công");
      Get.find<MessageController>().socketIo.createGroupChat(value.object?.id);
      Get.until((route) => route.settings.name == Routes.home);
      Get.find<HomeController>().comeMessage();
      Get.find<MessageController>().onInit();
      checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> goToMessageContent(value.object?.id),StringConstant.FEATURE_MESSAGE_DETAIL);
    } else {
      AppUtils.shared.hideLoading();
      AppUtils.shared.showToast(value.message ?? "Tạo nhóm chat thất bại",
          backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
    }
  });
}

void goToMessageContent(id){
  Get.toNamed(Routes.messageContent,arguments: id);
}