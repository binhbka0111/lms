import 'package:flutter/material.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/preference_utils.dart';

enum ThemeType { DARK, LIGHT }

class ThemeService {
  static ThemeData themeData() {
    int themeType = PreferenceUtils.getInt(StringConstant.SAVETHEME);
    switch (themeType) {
      case 0:
        return darkTheme();
      default:
        return lightTheme();
    }
  }

  static ThemeData lightTheme() {
    final ThemeData base = ThemeData.light();
    TextTheme _basicTextTheme(TextTheme base) {
      return base.copyWith(
        headline1: base.headline1?.copyWith(
          color: ColorUtils.COLOR_TEXT_BLACK87,
        ),
        headline2: base.headline2
            ?.copyWith(fontFamily: "Roboto", color: ColorUtils.COLOR_RED),
        headline3: base.headline3
            ?.copyWith(fontFamily: "Roboto", color: ColorUtils.BG_BUTTON_BUY),
        headline4: base.headline3
            ?.copyWith(fontFamily: "Roboto", color: ColorUtils.BG_COLOR),
        headline5: base.headline3
            ?.copyWith(fontFamily: "Roboto", color: ColorUtils.BG_STATUS_CANEL),
      );
    }

    ColorScheme colorScheme(ColorScheme base) {
      return base.copyWith(
        secondary: ColorUtils.COLOR_GREEN_BOLD,
        primary: ColorUtils.text_list,
        onPrimary: ColorUtils.text_list.withOpacity(0.7),
        surface: ColorUtils.BG_BASE2,
        onSurface: ColorUtils.BG_COLOR,
        onBackground: ColorUtils.COLOR_GREEN_BOLD,
      );
    }

    return base.copyWith(
      textTheme: _basicTextTheme(base.textTheme),
      splashColor: ColorUtils.BG_BUTTON_BUY,
      primaryColor: ColorUtils.COLOR_WHITE,
      backgroundColor: ColorUtils.BG_COLOR,
      bottomAppBarColor: ColorUtils.BG_FOOTER,
      disabledColor: ColorUtils.COLOR_BLACK,
      errorColor: ColorUtils.COLOR_RED,
      indicatorColor: ColorUtils.BG_COLOR,
      focusColor: ColorUtils.BG_COLOR,
      dividerColor: Colors.grey,
      selectedRowColor: ColorUtils.BG_BASE2,
      cardColor: ColorUtils.COLOR_WHITE,
      scaffoldBackgroundColor: ColorUtils.BG_BASE2,
      hoverColor: ColorUtils.COLOR_WHITE,
      primaryColorLight: Colors.black54,
      primaryColorDark: ColorUtils.BG_COLOR,
      hintColor: ColorUtils.COLOR_BLACK,
      highlightColor: ColorUtils.text_list,
      colorScheme: colorScheme(base.colorScheme),
      secondaryHeaderColor: ColorUtils.COLOR_WHITE,
    );
  }

  static ThemeData darkTheme() {
    final ThemeData base = ThemeData.dark();
    TextTheme _basicTextTheme(TextTheme base) {
      return base.copyWith(
        headline1: base.headline1
            ?.copyWith(fontFamily: "Roboto", color: ColorUtils.COLOR_WHITE),
        headline2: base.headline2
            ?.copyWith(fontFamily: "Roboto", color: ColorUtils.COLOR_BLACK),
        headline3: base.headline3
            ?.copyWith(fontFamily: "Roboto", color: ColorUtils.COLOR_RED),
        headline4: base.headline4
            ?.copyWith(fontFamily: "Roboto", color: ColorUtils.BG_BUTTON_BUY),
        headline5: base.headline5
            ?.copyWith(fontFamily: "Roboto", color: ColorUtils.BG_COLOR),
        headline6: base.headline6
            ?.copyWith(fontFamily: "Roboto", color: ColorUtils.BG_STATUS_CANEL),
      );
    }

    ColorScheme colorScheme(ColorScheme base) {
      return base.copyWith(
        secondary: ColorUtils.COLOR_GREEN,
        primary: ColorUtils.COLOR_TEXT_GREY,
        onPrimary: ColorUtils.COLOR_TEXT_GREY,
        surface: ColorUtils.BG_BASE_BLACK2,
        onSurface: ColorUtils.BG_BASE_BLACK2,
        onBackground: ColorUtils.BG_COLOR,
      );
    }

    return base.copyWith(
      textTheme: _basicTextTheme(base.textTheme),
      splashColor: ColorUtils.BG_COLOR,
      primaryColor: ColorUtils.COLOR_TEXT_BLACK87,
      disabledColor: ColorUtils.COLOR_WHITE,
      bottomAppBarColor: ColorUtils.BG_BASE_BLACK2,
      errorColor: ColorUtils.COLOR_RED,
      dividerColor: ColorUtils.COLOR_TEXT_BLACK87,
      hintColor: ColorUtils.COLOR_WHITE,
      indicatorColor: ColorUtils.COLOR_WHITE,
      focusColor: ColorUtils.COLOR_TEXT_BLACK87,
      selectedRowColor: ColorUtils.COLOR_BLACK,
      cardColor: ColorUtils.COLOR_BLACK,
      scaffoldBackgroundColor: ColorUtils.COLOR_TEXT_BLACK87,
      hoverColor: ColorUtils.BG_COLOR,
      primaryColorLight: ColorUtils.line_list,
      primaryColorDark: ColorUtils.COLOR_BLACK,
      highlightColor: ColorUtils.COLOR_WHITE,
      colorScheme: colorScheme(base.colorScheme),
      secondaryHeaderColor: ColorUtils.BG_BASE_BLACK2,
    );
  }
}
