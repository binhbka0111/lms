import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';

class DeviceInfoUtil {
  static final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  static String? deviceName;

  static Future<String> getDeviceName() async {
    if (deviceName != null) return deviceName ?? '';
    if (Platform.isIOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      deviceName = iosDeviceInfo.model;
    } else if (Platform.isAndroid) {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      deviceName = androidDeviceInfo.model;
    }
    deviceName ??= '';
    return deviceName ?? '';
  }
}
