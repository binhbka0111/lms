import 'package:flutter/cupertino.dart';

class OnSelectionText{
  static  onSelection({required FocusNode focusNode,required TextEditingController controller}) {
    if (focusNode.hasFocus) {
      controller.selection = TextSelection.fromPosition(
        TextPosition(
          offset: controller.text.length,
        ),
      );
    }
  }
}