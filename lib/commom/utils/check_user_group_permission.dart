import '../../data/model/common/user_group_by_app.dart';
import 'app_utils.dart';
import 'color_utils.dart';

checkVisiblePage(List<UserGroupByApp> list,page){
  if(list.isEmpty){
    return false;
  }
  for(int i = 0 ; i< list.length; i ++){
    if(list[i].page?.code == page){
      return true;
    }
  }
  var listChild = getListChildren(list);
  return checkVisiblePage(listChild, page);
}

getListChildren(List<UserGroupByApp> list){
  var listChild = <UserGroupByApp>[];
  for(int i = 0 ; i < list.length ; i ++){
    for(int j = 0 ; j < list[i].page!.children!.length ; j ++){
      listChild.add(list[i].page!.children![j]);
    }
  }

  return listChild;
}

checkVisibleFeature(List<UserGroupByApp> list,feature){
  if(list.isEmpty){
    return false;
  }
  for(int i = 0 ; i< list.length; i ++){
    if(list[i].features!.contains(feature)){
      return true;
    }
  }
  var listChild = getListChildren(list);
  return checkVisibleFeature(listChild, feature);
}

checkClickPage(List<UserGroupByApp> list,Function function,page){
  if(list.isEmpty){
    return showToastNotPermission();
  }
  for(int i = 0 ; i< list.length; i ++){
    if(list[i].page?.code == page){
      return function();
    }
  }
  var listChild = getListChildren(list);
  return checkClickPage(listChild,function, page);
}

checkClickFeature(List<UserGroupByApp> list,Function function,feature){
  if(list.isEmpty){
    return showToastNotPermission();
  }
  for(int i = 0 ; i< list.length; i ++){
    if(list[i].features!.contains(feature)){
      return function();
    }
  }
  var listChild = getListChildren(list);
  return checkClickFeature(listChild,function, feature);
}

showToastNotPermission(){
  AppUtils.shared.showToast("bạn không có quyền xem nội dung này",
      backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
}


setHeightPopupMenuItem(index,list){
  var height = 30.0;
  if(index == 0 || index == list.length -1){
    height = 36.0;
  }
  return height;
}

setVisiblePopupMenuItem(index,list){
  var isShow = true;
  if(list.length <= 1 || index == list.length -1) isShow = false;
  return isShow;
}