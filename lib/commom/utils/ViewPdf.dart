import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/textstyle.dart';
import 'package:slova_lms/commom/widget/empty_screen.dart';
//ignore: must_be_immutable
class ViewPdfPage extends StatelessWidget {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  ViewPdfPage({Key? key, required this.url}) : super(key: key);
  String url;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Scaffold(
          extendBody: false,
          appBar: getAppBar("Trình xem tài liệu", []),
          body: url != ""
              ? SfPdfViewer.network(
                  url,
                  key: _pdfViewerKey,
                )
              : EmptyScreen(
                  onTap: () {},
                )),
    );
  }
}

getAppBar(
  String title,
  List<Widget> action,
) {
  return AppBar(
    automaticallyImplyLeading: true,
    flexibleSpace: Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/icons/new/bg_toolbar.png"),
          fit: BoxFit.fill,
        ),
      ),
    ),
    backgroundColor: ColorUtils.COLOR_BG_TOOLBAR,
    centerTitle: true,
    title: Text(
      title,
      style: TextStyleUtils.sizeText18Weight400()
          ?.copyWith(fontFamily: "Montserrat", color: ColorUtils.COLOR_WHITE),
    ),
    actions: action,
  );
}
