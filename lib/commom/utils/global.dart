import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_svg/svg.dart';
import 'package:path/path.dart' as p;
import '../widget/empty_screen.dart';

// Widget svgIcon(String assetName) {
//   return SvgPicture.asset("assets/icons/" + assetName);
// }
// var themeColor = Theme.of(Get.context!);

String getFileIcon(file) {
  var extension = p.extension(file).replaceAll(".", "");
  if(extension == "png" || extension== "jpg" || extension == "bmp" || extension == "jpeg"){
    return "assets/icons/file/icon_image.png";
  }
  if(extension == "doc" || extension== "docx"){
    return "assets/icons/file/icon_word.png";
  }
  if(extension == "xlsx" || extension== "xls"){
    return "assets/icons/file/icon_excel.png";
  }
  if(extension == "pptx" || extension== "ppt"){
    return "assets/icons/file/icon_ppt.png";
  }
  if(extension == "pdf"){
    return "assets/icons/file/icon_pdf.png";
  }
  return "assets/icons/file/icon_unknow.png";
}



String getIcon(ext) {
  if(ext == "png" || ext== "jpg" || ext == "bmp" || ext == "jpeg"){
    return "assets/icons/file/icon_image.png";
  }
  if(ext == "doc" || ext== "docx"){
    return "assets/icons/file/icon_word.png";
  }
  if(ext == "xlsx" || ext== "xls"){
    return "assets/icons/file/icon_excel.png";
  }
  if(ext == "pptx" || ext== "ppt"){
    return "assets/icons/file/icon_ppt.png";
  }
  if(ext == "pdf"){
    return "assets/icons/file/icon_pdf.png";
  }
  return "assets/icons/file/icon_unknow.png";
}


String getAssetsIconNew(icon) {
  return "assets/icons/new/$icon";
}
String getAssetsIcon(String icon) {
  return "assets/icons/$icon";
}

String getAssetsImage(String image) {
  return "assets/images/$image";
}

getEmptyWidget(Function callback){
  return Container(
    width: Get.width,
    height: Get.height,
    child: Center(child: EmptyScreen(
      onTap: () => callback.call(),
    ),),);
}
//
// getEmptyWidgetList(Function callback){
//   return SizedBox(
//     width: Get.width/2,
//     height: Get.width/2,
//     child: Center(child: EmptyScreen(
//       onTap: () => callback.call(),
//     ),),);
// }

Widget svgIcon(String path, {double? width, double? height}) {
  return SvgPicture.asset(
    "assets/svg/$path",
    color: Colors.red,
    width: width,
    height: height,
  );
}

void dismissKeyboard() {
  FocusScope.of(Get.context!).requestFocus(FocusNode());
}
