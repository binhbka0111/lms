import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/Theme/theme_service.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/number_constant.dart';
import 'package:slova_lms/commom/utils/preference_utils.dart';

class TextStyleUtils {
  static TextStyle? textAbout() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: cmn_11.sp);
  }

  //TextStyle?9
  static TextStyle? sizeText9Weight400() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 9.sp, fontWeight: FontWeight.w400);
  }

  static TextStyle? sizeText9Weight500() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 9.sp, fontWeight: FontWeight.w500);
  }

  //TextStyle?10
  static TextStyle? sizeText10Weight300() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 10.sp, fontWeight: FontWeight.w300);
  }

  static TextStyle? sizeText10Weight400() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 10.sp, fontWeight: FontWeight.w400);
  }

  static TextStyle? sizeText10Weight500() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 10.sp, fontWeight: FontWeight.w500);
  }

  static TextStyle? sizeText10Weight600() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 10.sp, fontWeight: FontWeight.w600);
  }

  static TextStyle? sizeText10Weight700() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 10.sp, fontWeight: FontWeight.w700);
  }

  //TextStyle?11
  static TextStyle? sizeText11Weight300() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 11.sp, fontWeight: FontWeight.w300);
  }

  static TextStyle? sizeText11Weight400() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 11.sp, fontWeight: FontWeight.w400);
  }

  static TextStyle? sizeText11Weight500() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 11.sp, fontWeight: FontWeight.w500);
  }

  static TextStyle? sizeText11Weight600() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 11.sp, fontWeight: FontWeight.w600);
  }

  static TextStyle? sizeText11Weight700() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 11.sp, fontWeight: FontWeight.w700);
  }

  //TextStyle?12
  static TextStyle? sizeText12Weight300() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 12.sp, fontWeight: FontWeight.w300);
  }

  static TextStyle? sizeText12Weight400() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 12.sp, fontWeight: FontWeight.w400);
  }

  static TextStyle? sizeText12Weight500() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 12.sp, fontWeight: FontWeight.w500);
  }

  static TextStyle? sizeText12Weight600() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 12.sp, fontWeight: FontWeight.w600);
  }

  static TextStyle? sizeText12Weight700() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 12.sp, fontWeight: FontWeight.w700);
  }

  //TextStyle?13
  static TextStyle? sizeText13Weight300() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 13.sp, fontWeight: FontWeight.w300);
  }

  static TextStyle? sizeText13Weight400() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 13.sp, fontWeight: FontWeight.w400);
  }

  static TextStyle? sizeText13Weight500() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 13.sp, fontWeight: FontWeight.w500);
  }

  static TextStyle? sizeText13Weight600() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 13.sp, fontWeight: FontWeight.w600);
  }

  static TextStyle? sizeText13Weight700() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 13.sp, fontWeight: FontWeight.w700);
  }

  //TextStyle?14
  static TextStyle? sizeText14Weight300() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 14.sp, fontWeight: FontWeight.w300);
  }

  static TextStyle? sizeText14Weight400() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 14.sp, fontWeight: FontWeight.w400);
  }

  static TextStyle? sizeText14Weight500() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 14.sp, fontWeight: FontWeight.w500);
  }

  static TextStyle? sizeText14Weight600() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 14.sp, fontWeight: FontWeight.w600);
  }

  static TextStyle? sizeText14Weight700() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 14.sp, fontWeight: FontWeight.w700);
  }

  //TextStyle?15
  static TextStyle? sizeText15Weight300() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 15.sp, fontWeight: FontWeight.w300);
  }

  static TextStyle? sizeText15Weight400() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 15.sp, fontWeight: FontWeight.w400);
  }

  static TextStyle? sizeText15Weight500() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 15.sp, fontWeight: FontWeight.w500);
  }

  static TextStyle? sizeText15Weight600() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 15.sp, fontWeight: FontWeight.w600);
  }

  static TextStyle? sizeText15Weight700() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 15.sp, fontWeight: FontWeight.w700);
  }

  //TextStyle?16
  static TextStyle? sizeText16Weight300() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 16.sp, fontWeight: FontWeight.w300);
  }

  static TextStyle? sizeText16Weight400() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 16.sp, fontWeight: FontWeight.w400);
  }

  static TextStyle? sizeText16Weight500() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 16.sp, fontWeight: FontWeight.w500);
  }

  static TextStyle? sizeText16Weight600() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 16.sp, fontWeight: FontWeight.w600);
  }

  static TextStyle? sizeText16Weight700() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 16.sp, fontWeight: FontWeight.w700);
  }

  //TextStyle?17
  static TextStyle? sizeText17Weight300() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 17.sp, fontWeight: FontWeight.w300);
  }

  static TextStyle? sizeText17Weight400() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 17.sp, fontWeight: FontWeight.w400);
  }

  static TextStyle? sizeText17Weight500() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 17.sp, fontWeight: FontWeight.w500);
  }

  static TextStyle? sizeText17Weight600() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 17.sp, fontWeight: FontWeight.w600);
  }

  static TextStyle? sizeText17Weight700() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 17.sp, fontWeight: FontWeight.w700);
  }

  //TextStyle?18
  static TextStyle? sizeText18Weight300() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 18.sp, fontWeight: FontWeight.w300);
  }

  static TextStyle? sizeText18Weight400() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 18.sp, fontWeight: FontWeight.w400);
  }

  static TextStyle? sizeText18Weight500() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 18.sp, fontWeight: FontWeight.w500);
  }

  static TextStyle? sizeText18Weight600() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 18.sp, fontWeight: FontWeight.w600);
  }

  static TextStyle? sizeText18Weight700() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 18.sp, fontWeight: FontWeight.w700);
  }

  //TextStyle?19
  static TextStyle? sizeText19Weight300() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 19.sp, fontWeight: FontWeight.w300);
  }

  static TextStyle? sizeText19Weight400() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 19.sp, fontWeight: FontWeight.w400);
  }

  static TextStyle? sizeText19Weight500() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 19.sp, fontWeight: FontWeight.w500);
  }

  static TextStyle? sizeText19Weight600() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 19.sp, fontWeight: FontWeight.w600);
  }

  static TextStyle? sizeText19Weight700() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 19.sp, fontWeight: FontWeight.w700);
  }

  //TextStyle?20
  static TextStyle? sizeText20Weight300() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 20.sp, fontWeight: FontWeight.w300);
  }

  static TextStyle? sizeText20Weight400() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 20.sp, fontWeight: FontWeight.w400);
  }

  static TextStyle? sizeText20Weight500() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 20.sp, fontWeight: FontWeight.w500);
  }

  static TextStyle? sizeText20Weight600() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 20.sp, fontWeight: FontWeight.w600);
  }

  static TextStyle? sizeText20Weight700() {
    return Theme.of(Get.context!)
        .textTheme
        .headline1
        ?.copyWith(fontSize: 20.sp, fontWeight: FontWeight.w700);
  }

  static TextStyle titleBold14Weigh500 = TextStyle(
    fontSize: 14.sp,
    color: ColorUtils.COLOR_TEXT_BLACK87,
    fontWeight: FontWeight.w500,
  );

  static void changeTheme(ThemeData theme, ThemeType type) {
    Get.changeTheme(theme);
    Get.changeThemeMode(ThemeMode.light);
    PreferenceUtils.setInt(StringConstant.SAVETHEME, type.index);
  }
}
