import 'package:get/get.dart';

class ValidateUtils {
  static bool isEmail(String email) {
    if (RegExp(
            "^[a-zA-Z0-9.!#/%&'\$\*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*")
        .hasMatch(email)) {
      return false;
    } else {
      return true;
    }
  }

  // static bool isValidateName(String name) {
  //   if ((RegExp(r"^[\p{L} '-]*$",
  //           caseSensitive: false, unicode: true, dotAll: true)
  //       .hasMatch(name))) {
  //     return false;
  //   }
  //   return true;
  // }

  static bool isPhoneNumber(String phoneNumber) {
    if (RegExp(r'^0[0-9]{9,12}$').hasMatch(phoneNumber)) {
      return true;
    } else {
      return false;
    }
  }

  static bool isValidUsername(userName) {
    if (GetUtils.isEmail(userName) || isPhoneNumber(userName)) return true;
    return false;
  }

  static bool isPassword(String password) {
    if (password.length >= 6) {
      return false;
    } else {
      return true;
    }
  }
}
