import 'package:flutter/widgets.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_page.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_page.dart';
import 'package:slova_lms/view/mobile/schedule/schedule_page.dart';

import '../../view/mobile/phonebook/phone_book_page.dart';
import '../../view/mobile/role/manager/manager_home_page.dart';
import '../../view/mobile/role/manager/schedule_manager/schedule_manager_page.dart';
import '../../view/mobile/role/teacher/teacher_home_page.dart';
// ignore_for_file: constant_identifier_names
class RoleAction {
  static const TEACHER = 'TEACHER';

  static const MANAGER = 'MANAGER';
  static const PARENT = 'PARENT';
  static const STUDENT = 'STUDENT';
  static const ADMIN = 'ADMIN';

  getActionByRole(
      {required String currentRole,
      required Function parentAction,
      required Function studentAction,
      required Function   teacherAction,
      required Function managerAction}) {
    switch (currentRole) {
      case TEACHER:
        teacherAction.call();
        break;
      case PARENT:
        parentAction.call();
        break;
      case MANAGER:
        managerAction.call();
        break;
      default:
        studentAction.call();
        break;
    }
  }

  Widget getDashboardUIByRole({required String currentRole}) {
    switch (currentRole) {
      case TEACHER:
        return TeacherHomePage();
      case PARENT:
        return ParentHomePage();
      case MANAGER:
        return ManagerHomePage();
      default:
        return StudentHomePage();
    }
  }

  Widget getPhoneBookUIByRole({required String currentRole}) {
    switch (currentRole) {
      case TEACHER:
        return PhoneBookPage();
      case PARENT:
        return PhoneBookPage();
      case MANAGER:
        return PhoneBookPage();
      default:
        return PhoneBookPage();
    }
  }
  Widget getScheduleUIByRole({required String currentRole}) {
    switch (currentRole) {
      case TEACHER:
        return SchedulePage();
      case PARENT:
        return SchedulePage();
      case MANAGER:
        return ScheduleManagerPage();
      default:
        return SchedulePage();
    }
  }
}
