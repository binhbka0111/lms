import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/data/base_service/api_response.dart';

import '../../data/repository/account/auth_repo.dart';
import '../../routes/app_pages.dart';
import '../app_cache.dart';
import '../utils/color_utils.dart';

class Logout extends StatelessWidget {
  const Logout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        Container(
          margin: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  height:  50.h,
                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                  decoration: const ShapeDecoration(
                      color: ColorUtils.PRIMARY_COLOR,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(6),
                            topRight: Radius.circular(6)),
                      )),
                  child: Container(
                    width: double.infinity,
                    alignment: Alignment.centerLeft,
                    child: const Text("Đăng Xuất",
                        style: TextStyle(color: Colors.white)),
                  )),
              Container(
                  color: Colors.white,
                  width: double.infinity,
                  height: 57.h,
                  alignment: Alignment.center,
                  child: const Text("Bạn có muốn đăng xuất không?")),
              Row(
                children: [
                  Expanded(
                      child: SizedBox(
                        height: 50.h,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.grey,
                              shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(6)))),
                          onPressed: () {
                            Get.back();
                          },
                          child: const Text(
                            "Hủy",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      )),
                  Expanded(
                    child: SizedBox(
                      height: 50.h,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.red,
                            shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(6)))),
                        onPressed: () {
                          logout();
                        },
                        child: const Text(
                          "Đăng Xuất",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

final AuthRepo _authRepo = AuthRepo();
logout() async {

  await _authRepo.logout().then((value) {
    if (value.state == Status.SUCCESS) {
      // Get.back();
      goToSplash();
    } else {
      AppUtils.shared.snackbarError(
          "Đăng xuất thất bại", value.message);
    }



  });
  AppCache().deleteInfoLogin();
  AppCache().saveInfoLogin(false);
  FlutterAppBadger.removeBadge();
}

void goToSplash() {
  // Get.offNamedUntil(Routes.splash, (route) => false);
  Get.offAllNamed(Routes.splash);
  // Get.toNamed(Routes.login);
}
