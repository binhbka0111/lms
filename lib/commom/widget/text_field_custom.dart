import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/on_selection_text.dart';

enum StateType { ERROR, SUCCESS, DEFAULT, DISABLE }

getIconSuffix(stateInput) {
  switch (stateInput) {
    case StateType.SUCCESS:
      return "assets/images/icon_success.png";
    case StateType.ERROR:
      return "assets/images/icon_err.png";
    case StateType.DISABLE:
      return "assets/images/iconDefault.png";
    case StateType.DEFAULT:
      return "";
  }
}
//ignore: must_be_immutable
class OutlineBorderTextFormField extends StatefulWidget {
  FocusNode focusNode = FocusNode();
  TextEditingController? controller;
  String? labelText;
  TextInputType? keyboardType;
  bool? autofocus = false;
  TextInputAction? textInputAction;
  Function? validation;
  Function? onChange;
  bool enable = true;
  bool? showHelperText = false;
  StateType state = StateType.DEFAULT;
  String? iconPrefix;
  String? iconSuffix;
  double? height = 48;
  String? helperText;
  bool? ishowIconPrefix;
  bool? readOnly = false;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OutlineBorderTextFormField();
  }

  OutlineBorderTextFormField(
      {super.key, this.labelText,
      this.autofocus,
      this.controller,
      this.keyboardType,
      this.textInputAction,
      this.validation,
      required this.enable,
      this.showHelperText,
      required this.iconPrefix,
      required this.iconSuffix,
      required this.state,
      this.height,
      this.helperText,
      this.ishowIconPrefix,
      required this.focusNode,
      this.onChange,
      this.readOnly});
}

class _OutlineBorderTextFormField extends State<OutlineBorderTextFormField> {
  bool focusText = false;
  String statusString = "";

  getLabelTextStyle(color) {
    return TextStyle(
        fontSize: 12.0.sp,
        color: color,
        fontWeight: FontWeight.w500,
        fontFamily: 'assets/font/static/Inter-Medium.ttf');
  } //label text style

  getTextFieldStyle() {
    return  TextStyle(
      fontSize: 12.0.sp,
      color: const Color.fromRGBO(26, 26, 26, 1),
    );
  } //textfield style

  getStatusTextFieldStyle() {
    var color = getColorState(widget.state);
    return TextStyle(
      fontSize: 10.0.sp,
      color: color,
    );
  } // Error text style

  getBorderColor() {
    return widget.focusNode.hasFocus
        ? ColorUtils.PRIMARY_COLOR
        : const Color.fromRGBO(192, 192, 192, 1);
  } //Border colors according to focus

  Widget getIconSuffix(String? iconSuffix) {
    var iconSuffix = widget.iconSuffix ?? "";
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Visibility(
          visible: iconSuffix.isNotEmpty ? true : false,
          child: Image.asset(
            iconSuffix,
            width: 14.sp,
            height: 14.sp,
            fit: BoxFit.cover,
            colorBlendMode: BlendMode.multiply,
            // color: widget.enable
            //     ? (widget.focusNode.hasFocus
            //         ? const Color.fromRGBO(26, 26, 26, 1)
            //         : getColorState(widget.state))
            //     : const Color.fromRGBO(177, 177, 177, 1),
          ),
        ),
        const SizedBox(
          width: 6,
        )
      ],
    );
  }

  Widget getIconPrefix(String? path) {
    return Container(
      margin: EdgeInsets.zero,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            path ?? "",
            width: 14,
            height: 24,
            color: Colors.black,
          )
        ],
      ),
    );
  }

  getColorState(state) {
    switch (state) {
      case StateType.SUCCESS:
        return const Color.fromRGBO(77, 197, 145, 1);
      case StateType.ERROR:
        return const Color.fromRGBO(255, 69, 89, 1);
      case StateType.DISABLE:
        return const Color.fromRGBO(246, 246, 246, 1);
      default:
        return const Color.fromRGBO(177, 177, 177, 1);
    }
  }

  @override
  initState() {
    super.initState();
    widget.focusNode.addListener(() {
      OnSelectionText.onSelection(
          focusNode: widget.focusNode, controller: widget.controller!);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          FocusScope(
            node: FocusScopeNode(),
            child: Focus(
                onFocusChange: (focus) {
                  setState(() {
                    focusText = focus;
                    getStatusTextFieldStyle();
                  });
                },
                child: Container(
                  alignment: Alignment.centerLeft,
                  height: widget.height,
                  padding: const EdgeInsets.all(2.0),
                  decoration: BoxDecoration(
                      color: widget.enable
                          ? Colors.white
                          : const Color.fromRGBO(246, 246, 246, 1),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(6.0)),
                      border: Border.all(
                        width: 1,
                        style: BorderStyle.solid,
                        color: widget.state == StateType.DEFAULT
                            ? getBorderColor()
                            : getColorState(widget.state),
                      )),
                  child: widget.enable
                      ? TextFormField(
                          cursorColor: ColorUtils.PRIMARY_COLOR,
                          focusNode: widget.focusNode,
                          controller: widget.controller,
                          style: getTextFieldStyle(),
                          autofocus: widget.autofocus!,
                          keyboardType: widget.keyboardType,
                          textInputAction: widget.textInputAction,
                          validator: (string) {
                            if (widget.validation!(widget.controller?.text)
                                .toString()
                                .isNotEmpty) {
                              setState(() {
                                focusText = true;
                                statusString =
                                    widget.validation!(widget.controller?.text);
                              });
                              return "";
                            } else {
                              setState(() {
                                focusText = false;
                                statusString =
                                    widget.validation!(widget.controller?.text);
                              });
                            }
                            return null;
                          },
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(50),
                          ],
                          onChanged: (value) {
                            setState(() {
                              if (widget.controller?.text.trim() == "") {
                                widget.showHelperText = true;
                              } else {
                                widget.showHelperText = false;
                              }
                            });
                          },
                          decoration: InputDecoration(
                            labelText: widget.labelText,
                            labelStyle: getLabelTextStyle(
                                widget.state == StateType.DEFAULT
                                    ? const Color.fromRGBO(177, 177, 177, 1)
                                    : getColorState(widget.state)),
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 16),
                            prefixIcon: widget.ishowIconPrefix!
                                ? getIconPrefix(widget.iconPrefix)
                                : null,
                            suffixIcon: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Visibility(
                                    visible:
                                        widget.controller?.text.isNotEmpty ==
                                            true,
                                    child: InkWell(
                                      onTap: () {
                                        widget.controller?.text = "";
                                      },
                                      child: const Icon(
                                        Icons.close_outlined,
                                        color: Colors.black,
                                        size: 20,
                                      ),
                                    )),
                                const Padding(
                                    padding: EdgeInsets.only(left: 4)),
                                getIconSuffix(widget.iconSuffix)
                              ],
                            ),
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            border: InputBorder.none,
                            errorStyle: const TextStyle(height: 0),
                            focusedErrorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                          ),
                        )
                      : Container(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${widget.labelText}",
                                style:  TextStyle(
                                    fontSize: 10.sp,
                                    color: const Color.fromRGBO(177, 177, 177, 1),
                                    fontWeight: FontWeight.w500,
                                    fontFamily:
                                        'assets/font/static/Inter-Medium.ttf'),
                              ),
                              const Padding(padding: EdgeInsets.only(top: 4)),
                              Text(
                                "${widget.controller?.text}",
                                style:  TextStyle(
                                  fontSize: 12.sp,
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                ),
                              )
                            ],
                          ),
                        ),
                )),
          ),
          Visibility(
            visible: widget.showHelperText!,
            child: Container(
                padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                child: Text(
                  "${widget.helperText}",
                  style: TextStyle(color: Colors.red, fontSize: 12.sp),
                )),
          )
        ],
      ),
    );
  }
}
//ignore: must_be_immutable
class OutlineBorderTextFormField2 extends StatefulWidget {
  FocusNode focusNode = FocusNode();
  TextEditingController? controller;
  String? labelText;
  String? hintText;
  TextInputType? keyboardType;
  bool? autofocus = false;
  TextInputAction? textInputAction;
  Function? validation;
  Function? onChange;
  bool enable = true;
  bool? showHelperText = false;
  StateType state = StateType.DEFAULT;
  String? iconPrefix;
  String? iconSuffix;
  double? height = 30;
  String? helperText;
  bool? isShowIconPrefix;
  bool? readOnly = false;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OutlineBorderTextFormField2();
  }

  OutlineBorderTextFormField2(
      {super.key,
      this.labelText,
      this.autofocus,
      this.hintText,
      this.controller,
      this.keyboardType,
      this.textInputAction,
      this.validation,
      required this.enable,
      this.showHelperText,
      required this.iconPrefix,
      required this.iconSuffix,
      required this.state,
      this.height,
      this.helperText,
      this.isShowIconPrefix,
      required this.focusNode,
      this.onChange,
      this.readOnly});
}

class _OutlineBorderTextFormField2 extends State<OutlineBorderTextFormField2> {
  bool focusText = false;
  String statusString = "";

  getLabelTextStyle(color) {
    return TextStyle(
        fontSize: 14.0.sp,
        color: color,
        fontWeight: FontWeight.w500,
        fontFamily: 'assets/font/static/Inter-Medium.ttf');
  } //label text style

  getTextFieldStyle() {
    return  TextStyle(
      fontSize: 12.0.sp,
      color: const Color.fromRGBO(26, 26, 26, 1),
    );
  } //textfield style

  getStatusTextFieldStyle() {
    var color = getColorState(widget.state);
    return TextStyle(
      fontSize: 10.0.sp,
      color: color,
    );
  } // Error text style

  getBorderColor() {
    return widget.focusNode.hasFocus
        ? ColorUtils.PRIMARY_COLOR
        : const Color.fromRGBO(192, 192, 192, 1);
  } //Border colors according to focus

  Widget getIconSuffix(String? iconSuffix) {
    var iconSuffix = widget.iconSuffix ?? "";
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Visibility(
          visible: iconSuffix.isNotEmpty ? true : false,
          child: Image.asset(
            iconSuffix,
            width: 14,
            height: 14,
            fit: BoxFit.cover,
            colorBlendMode: BlendMode.multiply,
            // color: widget.enable
            //     ? (widget.focusNode.hasFocus
            //         ? const Color.fromRGBO(26, 26, 26, 1)
            //         : getColorState(widget.state))
            //     : const Color.fromRGBO(177, 177, 177, 1),
          ),
        ),
        const SizedBox(
          width: 6,
        )
      ],
    );
  }

  Widget getIconPrefix(String? path) {
    return Container(
      margin: EdgeInsets.zero,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            path ?? "",
            width: 14,
            height: 24,
            color: Colors.black,
          )
        ],
      ),
    );
  }

  getColorState(state) {
    switch (state) {
      case StateType.SUCCESS:
        return const Color.fromRGBO(77, 197, 145, 1);
      case StateType.ERROR:
        return const Color.fromRGBO(255, 69, 89, 1);
      case StateType.DISABLE:
        return const Color.fromRGBO(246, 246, 246, 1);
      default:
        return const Color.fromRGBO(177, 177, 177, 1);
    }
  }

  @override
  initState() {
    super.initState();
    widget.focusNode.addListener(() {
      OnSelectionText.onSelection(
          focusNode: widget.focusNode, controller: widget.controller!);
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: null,
      onChanged: (value) {},
      controller: widget.controller,
      decoration: InputDecoration(
          border: OutlineInputBorder(
              borderSide:
                  const BorderSide(color: Color.fromRGBO(177, 177, 177, 1)),
              borderRadius: BorderRadius.circular(6)),
          suffixIcon: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              Visibility(
                  visible: widget.controller?.text.isNotEmpty == true,
                  child: InkWell(
                    onTap: () {
                      widget.controller?.text = "";
                    },
                    child: const Icon(
                      Icons.close_outlined,
                      color: Colors.black,
                      size: 20,
                    ),
                  )),
              const Padding(padding: EdgeInsets.only(left: 4)),
              getIconSuffix(widget.iconSuffix)
            ],
          ),
          contentPadding: EdgeInsets.only(left: 8.w),
          hintText: widget.hintText,
          isCollapsed: true,
          hintStyle: TextStyle(
              fontSize: 10.sp, color: const Color.fromRGBO(177, 177, 177, 1)),
          focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: ColorUtils.PRIMARY_COLOR))),
      textAlignVertical: TextAlignVertical.center,
      cursorColor: ColorUtils.PRIMARY_COLOR,
    );
  }
}
//ignore: must_be_immutable
class MyTextFormFieldBinh extends StatefulWidget {
  FocusNode focusNode = FocusNode();
  TextEditingController? controller;
  String? labelText;
  TextInputType? keyboardType;
  bool? autofocus = false;
  TextInputAction? textInputAction;
  Function? validation;
  Function? onChange;
  bool enable = true;
  bool? showHelperText = false;
  StateType state = StateType.DEFAULT;
  String? iconPrefix;
  String? iconSuffix;
  double? height = 48;
  String? helperText;
  bool? ishowIconPrefix;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return OutlineBorderTextFormFieldBinh2();
  }

  MyTextFormFieldBinh(
      {super.key, this.labelText,
      this.autofocus,
      this.controller,
      this.keyboardType,
      this.textInputAction,
      this.validation,
      required this.enable,
      this.showHelperText,
      required this.iconPrefix,
      required this.iconSuffix,
      required this.state,
      this.height,
      this.helperText,
      this.ishowIconPrefix,
      required this.focusNode,
      this.onChange});
}

class OutlineBorderTextFormFieldBinh2 extends State<MyTextFormFieldBinh> {
  bool focusText = false;
  String statusString = "";

  getLabelTextStyle(color) {
    return TextStyle(
        fontSize: 14.0,
        color: color,
        fontWeight: FontWeight.w500,
        fontFamily: 'assets/font/static/Inter-Medium.ttf');
  } //label text style

  getTextFieldStyle() {
    return const TextStyle(
      fontSize: 12.0,
      color: Color.fromRGBO(26, 26, 26, 1),
    );
  } //textfield style

  getStatusTextFieldStyle() {
    var color = getColorState(widget.state);
    return TextStyle(
      fontSize: 10.0,
      color: color,
    );
  } // Error text style

  getBorderColor() {
    return widget.focusNode.hasFocus
        ? ColorUtils.PRIMARY_COLOR
        : const Color.fromRGBO(192, 192, 192, 1);
  } //Border colors according to focus

  Widget getIconSuffix(String? iconSuffix) {
    var iconSuffix = widget.iconSuffix ?? "";
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Visibility(
          visible: iconSuffix.isNotEmpty ? true : false,
          child: Image.asset(
            iconSuffix,
            width: 14,
            height: 14,
            fit: BoxFit.cover,
            colorBlendMode: BlendMode.multiply,
            // color: widget.enable
            //     ? (widget.focusNode.hasFocus
            //         ? const Color.fromRGBO(26, 26, 26, 1)
            //         : getColorState(widget.state))
            //     : const Color.fromRGBO(177, 177, 177, 1),
          ),
        ),
        const SizedBox(
          width: 6,
        )
      ],
    );
  }

  Widget getIconPrefix(String? path) {
    return Container(
      margin: EdgeInsets.zero,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            path ?? "",
            width: 14,
            height: 24,
            color: Colors.black,
          )
        ],
      ),
    );
  }

  getColorState(state) {
    switch (state) {
      case StateType.SUCCESS:
        return const Color.fromRGBO(77, 197, 145, 1);
      case StateType.ERROR:
        return const Color.fromRGBO(255, 69, 89, 1);
      case StateType.DISABLE:
        return const Color.fromRGBO(246, 246, 246, 1);
      default:
        return const Color.fromRGBO(177, 177, 177, 1);
    }
  }

  @override
  initState() {
    super.initState();
    widget.focusNode.addListener(() {
      OnSelectionText.onSelection(
          focusNode: widget.focusNode, controller: widget.controller!);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.w),
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          FocusScope(
            node: FocusScopeNode(),
            child: Focus(
                onFocusChange: (focus) {
                  setState(() {
                    focusText = focus;
                    getStatusTextFieldStyle();
                  });
                },
                child: Container(
                  alignment: Alignment.centerLeft,
                  height: widget.height,
                  padding: const EdgeInsets.all(2.0),
                  decoration: BoxDecoration(
                      color: widget.enable
                          ? Colors.white
                          : const Color.fromRGBO(246, 246, 246, 1),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(6.0)),
                      border: Border.all(
                        width: 1,
                        style: BorderStyle.solid,
                        color: widget.state == StateType.DEFAULT
                            ? getBorderColor()
                            : getColorState(widget.state),
                      )),
                  child: widget.enable
                      ? TextFormField(
                          cursorColor: ColorUtils.PRIMARY_COLOR,
                          focusNode: widget.focusNode,
                          controller: widget.controller,
                          style: getTextFieldStyle(),
                          autofocus: widget.autofocus!,
                          keyboardType: widget.keyboardType,
                          textInputAction: widget.textInputAction,
                          validator: (string) {
                            if (widget.validation!(widget.controller?.text)
                                .toString()
                                .isNotEmpty) {
                              setState(() {
                                focusText = true;
                                statusString =
                                    widget.validation!(widget.controller?.text);
                              });
                              return "";
                            } else {
                              setState(() {
                                focusText = false;
                                statusString =
                                    widget.validation!(widget.controller?.text);
                              });
                            }
                            return null;
                          },
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(
                                RegExp('[0-9.,]')),
                          ],
                          onChanged: (value) {},
                          maxLines: null,
                          decoration: InputDecoration(
                            labelText: widget.labelText,
                            labelStyle: getLabelTextStyle(
                                widget.state == StateType.DEFAULT
                                    ? const Color.fromRGBO(177, 177, 177, 1)
                                    : getColorState(widget.state)),
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 16),
                            prefixIcon: widget.ishowIconPrefix!
                                ? getIconPrefix(widget.iconPrefix)
                                : null,
                            suffixIcon: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Visibility(
                                    visible:
                                        widget.controller?.text.isNotEmpty ==
                                            true,
                                    child: InkWell(
                                      onTap: () {
                                        widget.controller?.text = "";
                                      },
                                      child: const Icon(
                                        Icons.close_outlined,
                                        color: Colors.black,
                                        size: 20,
                                      ),
                                    )),
                                const Padding(
                                    padding: EdgeInsets.only(left: 4)),
                                getIconSuffix(widget.iconSuffix)
                              ],
                            ),
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            border: InputBorder.none,
                            errorStyle: const TextStyle(height: 0),
                            focusedErrorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                          ),
                        )
                      : Container(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${widget.labelText}",
                                style: const TextStyle(
                                    fontSize: 10.0,
                                    color: Color.fromRGBO(177, 177, 177, 1),
                                    fontWeight: FontWeight.w500,
                                    fontFamily:
                                        'assets/font/static/Inter-Medium.ttf'),
                              ),
                              const Padding(padding: EdgeInsets.only(top: 4)),
                              Text(
                                "${widget.controller?.text}",
                                style: const TextStyle(
                                  fontSize: 12.0,
                                  color: Color.fromRGBO(26, 26, 26, 1),
                                ),
                              )
                            ],
                          ),
                        ),
                )),
          ),
          Visibility(
            visible: widget.showHelperText!,
            child: Container(
                padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                child: Text(
                  "${widget.helperText}",
                  style: TextStyle(color: Colors.red, fontSize: 12.sp),
                )),
          )
        ],
      ),
    );
  }
}
//ignore: must_be_immutable
class MyOutlineBorderTextFormFieldNhat extends StatefulWidget {
  FocusNode focusNode = FocusNode();
  TextEditingController? controller;
  String? labelText;
  TextInputType? keyboardType;
  bool? autofocus = false;
  TextInputAction? textInputAction;
  Function? validation;
  bool enable = true;
  bool? showHelperText = false;
  StateType state = StateType.DEFAULT;
  String? iconPrefix;
  String? iconSuffix;
  double? height = 48;
  String? helperText;
  bool? ishowIconPrefix;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return OutlineBorderTextFormFieldNhat();
  }

  MyOutlineBorderTextFormFieldNhat(
      {super.key,
      this.labelText,
      this.autofocus,
      this.controller,
      this.keyboardType,
      this.textInputAction,
      this.validation,
      required this.enable,
      this.showHelperText,
      required this.iconPrefix,
      required this.iconSuffix,
      required this.state,
      this.height,
      this.helperText,
      this.ishowIconPrefix,
      required this.focusNode});
}

class OutlineBorderTextFormFieldNhat
    extends State<MyOutlineBorderTextFormFieldNhat> {
  bool focusText = false;
  String statusString = "";

  getLabelTextStyle(color) {
    return TextStyle(
        fontSize: 14.0,
        color: color,
        fontWeight: FontWeight.w500,
        fontFamily: 'assets/font/static/Inter-Medium.ttf');
  } //label text style

  getTextFieldStyle() {
    return const TextStyle(
      fontSize: 14.0,
      color: Color.fromRGBO(26, 26, 26, 1),
    );
  } //textfield style

  getStatusTextFieldStyle() {
    var color = getColorState(widget.state);
    return TextStyle(
      fontSize: 10.0,
      color: color,
    );
  } // Error text style

  getBorderColor() {
    return widget.focusNode.hasFocus
        ? ColorUtils.PRIMARY_COLOR
        : const Color.fromRGBO(192, 192, 192, 1);
  } //Border colors according to focus

  Widget getIconSuffix(String? iconSuffix) {
    var iconSuffix = widget.iconSuffix ?? "";
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Visibility(
          visible: iconSuffix.isNotEmpty ? true : false,
          child: Container(
            margin: const EdgeInsets.only(top: 16),
            child: SvgPicture.asset(
              iconSuffix,
              width: 14,
              height: 14,
              fit: BoxFit.scaleDown,
              alignment: Alignment.bottomRight,
              // color: widget.enable
              //     ? (widget.focusNode.hasFocus
              //         ? const Color.fromRGBO(26, 26, 26, 1)
              //         : getColorState(widget.state))
              //     : const Color.fromRGBO(177, 177, 177, 1),
            ),
          ),
        ),
        const SizedBox(
          width: 6,
        )
      ],
    );
  }

  Widget getIconPrefix(String? path) {
    return Container(
      margin: EdgeInsets.zero,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            path ?? "",
            width: 14,
            height: 24,
            color: Colors.black,
          )
        ],
      ),
    );
  }

  getColorState(state) {
    switch (state) {
      case StateType.SUCCESS:
        return const Color.fromRGBO(77, 197, 145, 1);
      case StateType.ERROR:
        return const Color.fromRGBO(255, 69, 89, 1);
      case StateType.DISABLE:
        return const Color.fromRGBO(246, 246, 246, 1);
      default:
        return const Color.fromRGBO(177, 177, 177, 1);
    }
  }

  @override
  initState() {
    super.initState();
    widget.focusNode.addListener(() {
      OnSelectionText.onSelection(
          focusNode: widget.focusNode, controller: widget.controller!);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          FocusScope(
            node: FocusScopeNode(),
            child: Focus(
                onFocusChange: (focus) {
                  setState(() {
                    focusText = focus;
                    getStatusTextFieldStyle();
                  });
                },
                child: Container(
                  alignment: Alignment.centerLeft,
                  height: widget.height,
                  padding: const EdgeInsets.all(2.0),
                  decoration: BoxDecoration(
                      color: widget.enable
                          ? Colors.white
                          : const Color.fromRGBO(246, 246, 246, 1),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(6.0)),
                      border: Border.all(
                        width: 1,
                        style: BorderStyle.solid,
                        color: widget.state == StateType.DEFAULT
                            ? getBorderColor()
                            : getColorState(widget.state),
                      )),
                  child: widget.enable
                      ? TextFormField(
                          onTap: () {
                            if (widget.controller?.selection ==
                                TextSelection.fromPosition(TextPosition(
                                    offset: widget.controller?.text.length
                                            .toInt() ??
                                        0 - 1))) {
                              setState(() {
                                widget.controller?.selection =
                                    TextSelection.fromPosition(TextPosition(
                                        offset: widget.controller?.text.length
                                                .toInt() ??
                                            0));
                              });
                            }
                          },
                          maxLines: null,
                          cursorColor: ColorUtils.PRIMARY_COLOR,
                          focusNode: widget.focusNode,
                          controller: widget.controller,
                          style: getTextFieldStyle(),
                          autofocus: widget.autofocus!,
                          keyboardType: widget.keyboardType,
                          textInputAction: widget.textInputAction,
                          validator: (string) {
                            if (widget.validation!(widget.controller?.text)
                                .toString()
                                .isNotEmpty) {
                              setState(() {
                                focusText = true;
                                statusString =
                                    widget.validation!(widget.controller?.text);
                              });
                              return "";
                            } else {
                              setState(() {
                                focusText = false;
                                statusString =
                                    widget.validation!(widget.controller?.text);
                              });
                            }
                            return null;
                          },
                          onFieldSubmitted: (value) {},
                          decoration: InputDecoration(
                            labelText: widget.labelText,
                            labelStyle: getLabelTextStyle(
                                widget.state == StateType.DEFAULT
                                    ? const Color.fromRGBO(177, 177, 177, 1)
                                    : getColorState(widget.state)),
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 4, horizontal: 16),
                            prefixIcon: widget.ishowIconPrefix!
                                ? getIconPrefix(widget.iconPrefix)
                                : null,
                            suffixIcon: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Visibility(
                                    visible:
                                        widget.controller?.text.isNotEmpty ==
                                            true,
                                    child: InkWell(
                                      onTap: () {
                                        widget.controller?.text = "";
                                      },
                                      child: const Icon(
                                        Icons.close_outlined,
                                        color: Colors.black,
                                        size: 20,
                                      ),
                                    )),
                                const Padding(
                                    padding: EdgeInsets.only(left: 4)),
                                getIconSuffix(widget.iconSuffix)
                              ],
                            ),
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            border: InputBorder.none,
                            errorStyle: const TextStyle(height: 0),
                            focusedErrorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                          ),
                        )
                      : Container(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${widget.labelText}",
                                style: const TextStyle(
                                    fontSize: 10.0,
                                    color: Color.fromRGBO(177, 177, 177, 1),
                                    fontWeight: FontWeight.w400,
                                    fontFamily:
                                        'assets/font/static/Inter-Medium.ttf'),
                              ),
                              const Padding(padding: EdgeInsets.only(top: 4)),
                              Text(
                                "${widget.controller?.text}",
                                style: const TextStyle(
                                  fontSize: 12.0,
                                  color: Color.fromRGBO(26, 26, 26, 1),
                                ),
                              )
                            ],
                          ),
                        ),
                )),
          ),
          Visibility(
            visible: widget.showHelperText!,
            child: Container(
                padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                child: Text(
                  "${widget.helperText}",
                  style: const TextStyle(color: Colors.red),
                )),
          )
        ],
      ),
    );
  }
}
//ignore: must_be_immutable
class MyOutlineBorderTextFormFieldBinh extends StatefulWidget {
  FocusNode focusNode = FocusNode();
  TextEditingController? controller;
  String? labelText;
  TextInputType? keyboardType;
  bool? autofocus = false;
  TextInputAction? textInputAction;
  Function? validation;
  bool enable = true;
  bool? showHelperText = false;
  StateType state = StateType.DEFAULT;
  String? iconPrefix;
  String? iconSuffix;
  double? height = 48;
  String? helperText;
  bool? ishowIconPrefix;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return OutlineBorderTextFormFieldBinh();
  }

  MyOutlineBorderTextFormFieldBinh(
      {super.key, this.labelText,
      this.autofocus,
      this.controller,
      this.keyboardType,
      this.textInputAction,
      this.validation,
      required this.enable,
      this.showHelperText,
      required this.iconPrefix,
      required this.iconSuffix,
      required this.state,
      this.height,
      this.helperText,
      this.ishowIconPrefix,
      required this.focusNode});
}

class OutlineBorderTextFormFieldBinh
    extends State<MyOutlineBorderTextFormFieldBinh> {
  bool focusText = false;
  String statusString = "";

  getLabelTextStyle(color) {
    return TextStyle(
        fontSize: 14.0,
        color: color,
        fontWeight: FontWeight.w500,
        fontFamily: 'assets/font/static/Inter-Medium.ttf');
  } //label text style

  getTextFieldStyle() {
    return const TextStyle(
      fontSize: 14.0,
      color: Color.fromRGBO(26, 26, 26, 1),
    );
  } //textfield style

  getStatusTextFieldStyle() {
    var color = getColorState(widget.state);
    return TextStyle(
      fontSize: 10.0,
      color: color,
    );
  } // Error text style

  getBorderColor() {
    return widget.focusNode.hasFocus
        ? ColorUtils.PRIMARY_COLOR
        : const Color.fromRGBO(192, 192, 192, 1);
  } //Border colors according to focus

  Widget getIconSuffix(String? iconSuffix) {
    var iconSuffix = widget.iconSuffix ?? "";
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Visibility(
          visible: iconSuffix.isNotEmpty ? true : false,
          child: Container(
            margin: const EdgeInsets.only(top: 16),
            child: SvgPicture.asset(
              iconSuffix,
              width: 14,
              height: 14,
              fit: BoxFit.scaleDown,
              alignment: Alignment.bottomRight,
              // color: widget.enable
              //     ? (widget.focusNode.hasFocus
              //         ? const Color.fromRGBO(26, 26, 26, 1)
              //         : getColorState(widget.state))
              //     : const Color.fromRGBO(177, 177, 177, 1),
            ),
          ),
        ),
        const SizedBox(
          width: 6,
        )
      ],
    );
  }

  Widget getIconPrefix(String? path) {
    return Container(
      margin: EdgeInsets.zero,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            path ?? "",
            width: 14,
            height: 24,
            color: Colors.black,
          )
        ],
      ),
    );
  }

  getColorState(state) {
    switch (state) {
      case StateType.SUCCESS:
        return const Color.fromRGBO(77, 197, 145, 1);
      case StateType.ERROR:
        return const Color.fromRGBO(255, 69, 89, 1);
      case StateType.DISABLE:
        return const Color.fromRGBO(246, 246, 246, 1);
      default:
        return const Color.fromRGBO(177, 177, 177, 1);
    }
  }

  @override
  initState() {
    super.initState();
    widget.focusNode.addListener(() {
      OnSelectionText.onSelection(
          focusNode: widget.focusNode, controller: widget.controller!);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          FocusScope(
            node: FocusScopeNode(),
            child: Focus(
                onFocusChange: (focus) {
                  setState(() {
                    focusText = focus;
                    getStatusTextFieldStyle();
                  });
                },
                child: Container(
                  alignment: Alignment.centerLeft,
                  height: widget.height,
                  padding: const EdgeInsets.all(2.0),
                  decoration: BoxDecoration(
                      color: widget.enable
                          ? Colors.white
                          : const Color.fromRGBO(246, 246, 246, 1),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(6.0)),
                      border: Border.all(
                        width: 1,
                        style: BorderStyle.solid,
                        color: widget.state == StateType.DEFAULT
                            ? getBorderColor()
                            : getColorState(widget.state),
                      )),
                  child: widget.enable
                      ? TextFormField(
                          onTap: () {
                            if (widget.controller?.selection ==
                                TextSelection.fromPosition(TextPosition(
                                    offset: widget.controller?.text.length
                                            .toInt() ??
                                        0 - 1))) {
                              setState(() {
                                widget.controller?.selection =
                                    TextSelection.fromPosition(TextPosition(
                                        offset: widget.controller?.text.length
                                                .toInt() ??
                                            0));
                              });
                            }
                          },
                          maxLines: null,
                          cursorColor: ColorUtils.PRIMARY_COLOR,
                          focusNode: widget.focusNode,
                          controller: widget.controller,
                          style: getTextFieldStyle(),
                          autofocus: widget.autofocus!,
                          keyboardType: widget.keyboardType,
                          textInputAction: widget.textInputAction,
                          validator: (string) {
                            if (widget.validation!(widget.controller?.text)
                                .toString()
                                .isNotEmpty) {
                              setState(() {
                                focusText = true;
                                statusString =
                                    widget.validation!(widget.controller?.text);
                              });
                              return "";
                            } else {
                              setState(() {
                                focusText = false;
                                statusString =
                                    widget.validation!(widget.controller?.text);
                              });
                            }
                            return null;
                          },
                          onFieldSubmitted: (value) {},
                          decoration: InputDecoration(
                            labelText: widget.labelText,
                            labelStyle: getLabelTextStyle(
                                widget.state == StateType.DEFAULT
                                    ? const Color.fromRGBO(177, 177, 177, 1)
                                    : getColorState(widget.state)),
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 16),
                            prefixIcon: widget.ishowIconPrefix!
                                ? getIconPrefix(widget.iconPrefix)
                                : null,
                            suffixIcon: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Visibility(
                                    visible:
                                        widget.controller?.text.isNotEmpty ==
                                            true,
                                    child: InkWell(
                                      onTap: () {
                                        widget.controller?.text = "";
                                      },
                                      child: const Icon(
                                        Icons.close_outlined,
                                        color: Colors.black,
                                        size: 20,
                                      ),
                                    )),
                                const Padding(
                                    padding: EdgeInsets.only(left: 4)),
                                getIconSuffix(widget.iconSuffix)
                              ],
                            ),
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            border: InputBorder.none,
                            errorStyle: const TextStyle(height: 0),
                            focusedErrorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                          ),
                        )
                      : Container(
                          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${widget.labelText}",
                                style: const TextStyle(
                                    fontSize: 10.0,
                                    color: Color.fromRGBO(177, 177, 177, 1),
                                    fontWeight: FontWeight.w500,
                                    fontFamily:
                                        'assets/font/static/Inter-Medium.ttf'),
                              ),
                              const Padding(padding: EdgeInsets.only(top: 4)),
                              Text(
                                "${widget.controller?.text}",
                                style: const TextStyle(
                                  fontSize: 12.0,
                                  color: Colors.black,
                                ),
                              )
                            ],
                          ),
                        ),
                )),
          ),
          Visibility(
            visible: widget.showHelperText!,
            child: Container(
                padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                child: Text(
                  "${widget.helperText}",
                  style: TextStyle(fontSize: 12.sp, color: Colors.red),
                )),
          )
        ],
      ),
    );
  }
}
//ignore: must_be_immutable
class LabelOutSideTextFormField extends StatefulWidget {
  FocusNode focusNode = FocusNode();
  TextEditingController? controller;
  TextInputType? keyboardType;
  bool? autofocus = false;
  String? hintText;
  TextInputAction? textInputAction;
  Function? validation;
  bool enable = true;
  bool? showHelperText = false;
  StateType state = StateType.DEFAULT;
  String? iconPrefix;
  String? iconSuffix;
  double? height = 48;
  String? helperText;
  String? errorText;
  bool? obscureText = false;
  String? labelText;
  bool? showIconHideShow;
  Function? functionOnChange;
  Function? functionOnTap;
  Function? functionOnSubmit;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LabelOutSideTextFormField();
  }

  LabelOutSideTextFormField({super.key,
    this.autofocus,
    this.controller,
    this.keyboardType,
    this.textInputAction,
    this.validation,
    required this.enable,
    this.showHelperText,
    required this.iconPrefix,
    required this.iconSuffix,
    required this.state,
    this.height,
    this.helperText,
    this.errorText,
    this.hintText,
    this.obscureText,
    this.labelText,
    this.showIconHideShow,
    this.functionOnChange,
    required this.focusNode,
    this.functionOnTap,
    this.functionOnSubmit
  });
}

class _LabelOutSideTextFormField extends State<LabelOutSideTextFormField> {
  bool focusText = false;
  String statusString = "";

  getLabelTextStyle(color) {
    return TextStyle(fontSize: 12.0, color: color);
  } //label text style

  getTextFieldStyle() {
    return const TextStyle(
      fontSize: 16.0,
      color: Color.fromRGBO(26, 26, 26, 1),
    );
  } //textfield style

  getStatusTextFieldStyle() {
    var color = getColorState(widget.state);
    return TextStyle(
      fontSize: 10.0,
      color: color,
    );
  } // Error text style

  getBorderColor() {
    return widget.focusNode.hasFocus
        ? ColorUtils.PRIMARY_COLOR
        : const Color.fromRGBO(192, 192, 192, 1);
  } //Border colors according to focus

  Widget getIconSuffix(String? iconSuffix) {
    var iconSuffix = widget.iconSuffix ?? "";
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Visibility(
          visible: iconSuffix.isNotEmpty ? true : false,
          child: Image.asset(
            iconSuffix,
            width: 14,
            height: 14,
            color: widget.enable
                ? widget.state == StateType.DEFAULT
                    ? ((widget.focusNode.hasFocus
                        ? const Color.fromRGBO(26, 26, 26, 1)
                        : getColorState(widget.state)))
                    : (getColorState(widget.state))
                : const Color.fromRGBO(177, 177, 177, 1),
          ),
        ),
        const SizedBox(
          width: 10,
        )
      ],
    );
  }

  Widget getIconPrefix(String? path) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          path ?? "",
          width: 16,
          height: 16,
          color: Colors.black,
        ),
        const SizedBox(
          width: 10,
        )
      ],
    );
  }

  getColorState(state) {
    switch (state) {
      case StateType.SUCCESS:
        return const Color.fromRGBO(77, 197, 145, 1);
      case StateType.ERROR:
        return const Color.fromRGBO(255, 69, 89, 1);
      case StateType.DISABLE:
        return const Color.fromRGBO(246, 246, 246, 1);
      default:
        return const Color.fromRGBO(177, 177, 177, 1);
    }
  }

  @override
  initState() {
    super.initState();
    widget.focusNode.addListener(() {
      OnSelectionText.onSelection(
          focusNode: widget.focusNode, controller: widget.controller!);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 16,
      ),
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(bottom: 2),
            child: Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
              Text(
                "${widget.labelText}",
                style: const TextStyle(
                    color: Color.fromRGBO(26, 26, 26, 1),
                    fontSize: 14,
                    fontWeight: FontWeight.w500),
              ),
              Flexible(
                  child: Visibility(
                      visible: widget.state == StateType.ERROR,
                      child: Text(
                        "${widget.errorText}",
                        style: const TextStyle(
                            color: Color.fromRGBO(255, 69, 89, 1),
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                      )))
            ]),
          ),
          FocusScope(
            node: FocusScopeNode(),
            child: Focus(
                onFocusChange: (focus) {
                  //Called when ever focus changes
                  setState(() {
                    focusText = focus;
                    if (focusText == true) {
                    } else {
                      if (widget.controller!.text.isEmpty) {
                        widget.state = StateType.DEFAULT;
                        widget.iconSuffix = "";
                      }
                    }
                  });
                },
                child: Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.all(2.0),
                  decoration: BoxDecoration(
                      color: widget.enable
                          ? Colors.white
                          : const Color.fromRGBO(246, 246, 246, 1),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(6.0)),
                      border: Border.all(
                        width: 1,
                        style: BorderStyle.solid,
                        color: widget.state == StateType.DEFAULT
                            ? getBorderColor()
                            : getColorState(widget.state),
                      )),
                  child: TextFormField(
                    onTap: () {
                      if (widget.controller?.selection ==
                          TextSelection.fromPosition(TextPosition(
                              offset: widget.controller?.text.length.toInt() ??
                                  0 - 1))) {
                        setState(() {
                          widget.controller?.selection =
                              TextSelection.fromPosition(TextPosition(
                                  offset:
                                      widget.controller?.text.length.toInt() ??
                                          0));
                        });
                      }
                      widget.functionOnTap?.call();
                    },
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(50),
                    ],
                    cursorColor: ColorUtils.PRIMARY_COLOR,
                    focusNode: widget.focusNode,
                    controller: widget.controller,
                    obscureText: widget.obscureText!,
                    style: getTextFieldStyle(),
                    autofocus: widget.autofocus!,
                    keyboardType: widget.keyboardType,
                    textInputAction: widget.textInputAction,
                    maxLines: widget.obscureText == true?1:null,
                    validator: (string) {
                      if (widget.validation!(widget.controller?.text)
                          .toString()
                          .isNotEmpty) {
                        setState(() {
                          focusText = true;
                          statusString =
                              widget.validation!(widget.controller?.text);
                        });
                        return "";
                      } else {
                        setState(() {
                          focusText = false;
                          statusString =
                              widget.validation!(widget.controller?.text);
                        });
                      }
                      return null;
                    },
                    onFieldSubmitted: (value) {
                      widget.functionOnSubmit?.call();
                    },
                    onChanged: (value) {
                      widget.functionOnChange?.call();
                    },
                    textAlignVertical: TextAlignVertical.center,
                    decoration: InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(vertical: 7),
                        hintText: widget.hintText,
                        hintStyle: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Color.fromRGBO(177, 177, 177, 1)),
                        prefixIcon: getIconPrefix(widget.iconPrefix),
                        suffixIcon: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Visibility(
                                  visible: widget.controller?.text.isNotEmpty ==
                                      true,
                                  child: InkWell(
                                    onTap: () {
                                      widget.controller?.text = "";
                                    },
                                    child: const Icon(
                                      Icons.close_outlined,
                                      color: Color.fromRGBO(192, 192, 192, 1),
                                      size: 20,
                                    ),
                                  )),
                              const Padding(padding: EdgeInsets.only(left: 4)),
                              getIconSuffix(widget.iconSuffix),
                              Visibility(
                                  visible: widget.showIconHideShow!,
                                  child: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        widget.obscureText =
                                            !widget.obscureText!;
                                      });
                                    },
                                    constraints: const BoxConstraints(),
                                    padding:
                                        const EdgeInsets.fromLTRB(0, 0, 6, 0),
                                    splashColor: Colors.transparent,
                                    icon: Icon(
                                      widget.obscureText!
                                          ? Icons.visibility_off_rounded
                                          : Icons.visibility_rounded,
                                      size: 18,
                                      color: const Color.fromRGBO(
                                          133, 133, 133, 1),
                                    ),
                                  ))
                            ]),
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        border: InputBorder.none,
                        errorStyle: const TextStyle(height: 0),
                        focusedErrorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        floatingLabelBehavior: FloatingLabelBehavior.auto),
                  ),
                )),
          ),
          Visibility(
            visible: widget.showHelperText!,
            child: Container(
                padding: const EdgeInsets.only(top: 2.0),
                child: Text(
                  "${widget.helperText}",
                  style: const TextStyle(
                      color: Color.fromRGBO(192, 192, 192, 1), fontSize: 12),
                )),
          )
        ],
      ),
    );
  }
}
