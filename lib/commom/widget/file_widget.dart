import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/global.dart';
import 'package:slova_lms/commom/utils/textstyle.dart';
import 'package:path/path.dart' as p;
import 'package:slova_lms/data/model/common/learning_managerment.dart';
import '../../data/model/common/req_file.dart';
import '../utils/ViewPdf.dart';
import '../utils/open_url.dart';

class FileWidget {
  static itemFile(context,
      {required ReqFile file,
      required Function function,
      required bool remove}) {
    File? fileData;
    var fileName;
    var url = file.url;
    Widget widget = Container();

    if (url == null) {
      fileData = file.file!;
      fileName = fileData.path;
    } else {
      fileName = file.name;
    }

    var action = 0;
    var extension = p.extension(fileName).replaceAll(".", "");
    if (extension == "png" ||
        extension == "jpg" ||
        extension == "jpeg" ||
        extension == "gif" ||
        extension == "bmp") {
      action = 1;
    } else if (extension == "pdf") {
      action = 2;
    } else {
      action = 0;
    }

    switch (action) {
      case 1:
        if(url != null){
          widget = Image.network(
            url,
            errorBuilder: (
                BuildContext context,
                Object error,
                StackTrace? stackTrace,
                ) {
              return Image.asset(
                getFileIcon(file.name),
                height: 35,
                width: 30,
              );
            },
            height: 35,
            width: 30,
          );
        } else {
          widget = Image.file(
            fileData!,
            errorBuilder: (
                BuildContext context,
                Object error,
                StackTrace? stackTrace,
                ) {
              return Image.asset(
                getFileIcon(file.name),
                height: 35,
                width: 30,
              );
            },
            height: 35,
            width: 30,
          );
        }
        break;
      default:
        widget = Image.asset(
          getFileIcon(file.name),
          height: 35,
          width: 30,
        );
        break;
    }


    return GestureDetector(
      onTap: () {
        switch (action) {
          case 1:
            OpenUrl.openImageViewer(context, file.url!, file: fileData);
            break;
          case 2:
            Get.to(ViewPdfPage(url: file.url!));
            break;
          default:
            OpenUrl.openFile(file.url!);
            break;
        }
      },
      child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: const Color.fromRGBO(246, 246, 246, 1),
          ),
          child: Row(
            children: [
              const SizedBox(width: 4),
              widget,
              Expanded(
                  child: Container(
                    padding:
                    const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 4,
                        ),
                        Text(file.name ?? "Tệp đính kèm",
                            style: TextStyleUtils.sizeText15Weight500()
                                ?.copyWith(color: const Color.fromRGBO(26, 59, 112, 1))),
                      ],
                    ),
                  )),
              const SizedBox(
                width: 12,
              ),
              Visibility(
                visible: remove,
                child: GestureDetector(
                    child: const Icon(
                      Icons.delete_outline_outlined,
                      color: ColorUtils.COLOR_WORK_TYPE_4,
                      size: 24,
                    ),
                    onTap: () {
                      function.call();
                    }),
              ),
              const SizedBox(width: 8),
            ],
          )),
    );
  }

}
class FileWidget2 {
  static itemFile(context,
      {required FilesExercise file,
        required Function function,
        required bool remove}) {
    File? fileData;
    var fileName;
    var url = file.link;
    Widget widget = Container();

    if (url == null) {
      fileName = file.name;
    } else {
      fileName = file.name;
    }

    var action = 0;
    var extension = p.extension(fileName).replaceAll(".", "");
    if (extension == "png" ||
        extension == "jpg" ||
        extension == "jpeg" ||
        extension == "gif" ||
        extension == "bmp") {
      action = 1;
    } else if (extension == "pdf") {
      action = 2;
    } else {
      action = 0;
    }

    switch (action) {
      case 1:
        if(url != null){
          widget = Image.network(
            url,
            errorBuilder: (
                BuildContext context,
                Object error,
                StackTrace? stackTrace,
                ) {
              return Image.asset(
                getFileIcon(file.name),
                height: 35,
                width: 30,
              );
            },
            height: 35,
            width: 30,
          );
        } else {
          widget = Image.file(
            fileData!,
            errorBuilder: (
                BuildContext context,
                Object error,
                StackTrace? stackTrace,
                ) {
              return Image.asset(
                getFileIcon(file.name),
                height: 35,
                width: 30,
              );
            },
            height: 35,
            width: 30,
          );
        }
        break;
      default:
        widget = Image.asset(
          getFileIcon(file.name),
          height: 35,
          width: 30,
        );
        break;
    }


    return GestureDetector(
      onTap: () {
        switch (action) {
          case 1:
            OpenUrl.openImageViewer(context, file.link!, file: fileData);
            break;
          case 2:
            Get.to(ViewPdfPage(url: file.link!));
            break;
          default:
            OpenUrl.openFile(file.link!);
            break;
        }
      },
      child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: const Color.fromRGBO(246, 246, 246, 1),
          ),
          child: Row(
            children: [
              const SizedBox(width: 4),
              widget,
              Expanded(
                  child: Container(
                    padding:
                    const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 4,
                        ),
                        Text(file.name ?? "Tệp đính kèm",
                            style: TextStyleUtils.sizeText15Weight500()
                                ?.copyWith(color: const Color.fromRGBO(26, 59, 112, 1))),
                      ],
                    ),
                  )),
              const SizedBox(
                width: 12,
              ),
              Visibility(
                visible: remove,
                child: GestureDetector(
                    child: const Icon(
                      Icons.delete_outline_outlined,
                      color: ColorUtils.COLOR_WORK_TYPE_4,
                      size: 24,
                    ),
                    onTap: () {
                      function.call();
                    }),
              ),
              const SizedBox(width: 8),
            ],
          )),
    );
  }

}