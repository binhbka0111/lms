import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import '../app_cache.dart';

Widget appointmentBuilderTimeLineDay(BuildContext context,
    CalendarAppointmentDetails calendarAppointmentDetails) {
  final Appointment appointment = calendarAppointmentDetails.appointments.first;
  var outputFormat = DateFormat('HH:mm a');
  var outputDateFormat = DateFormat('dd/MM/yyyy');

  OnDialog(){
    return
     Dialog(
       child:
     IntrinsicHeight(
       child:   Container(
         padding: const EdgeInsets.all(16),
         child: Column(
           children: [
             Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children: [
                 Text("Thông tin môn học", style: TextStyle(color:const Color.fromRGBO(26, 26, 26, 1), fontSize: 14.sp, fontWeight: FontWeight.w500 ),),

         InkWell(
           onTap: (){
             Get.toNamed(Routes.detailJobByTeacherInTimeTable, arguments: appointment);
           },
           child: Container(
             alignment: Alignment.center,
             width: 100.w,
             height: 16.h,
             decoration: BoxDecoration(
                 color: ColorUtils.PRIMARY_COLOR,
                 borderRadius: BorderRadius.circular(8)
             ),
             child: Text('Xem chi tiết', textAlign: TextAlign.center,style: TextStyle(color: Colors.white, fontSize: 10.sp, fontWeight: FontWeight.w400),),
           ),
         )
               ],
             ),
             Padding(padding: EdgeInsets.only(top: 16.h)),
             Container(
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(6),
                   color: Colors.white),
               padding: const EdgeInsets.all(16),
               child: Column(
                 children: [
                   Row(
                     children: [
                       Text(
                         "Môn học :",
                         style: TextStyle(
                             fontSize: 14.sp,
                             color: const Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w400),
                       ),
                       Expanded(child: Container()),
                        Text(appointment.subject,
                           style:  TextStyle(
                               fontSize: 14.sp,
                               color: const Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w500))
                     ],
                   ),
                   const Padding(padding: EdgeInsets.only(top: 16)),
                   Row(
                     children: [
                        Text(
                         "Lớp :",
                         style: TextStyle(
                             fontSize: 14.sp,
                             color: const Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w400),
                       ),
                       Expanded(child: Container()),
                        Text("${appointment.notes}",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color:ColorUtils.PRIMARY_COLOR,
                               fontWeight: FontWeight.w500))
                     ],
                   ),
                   const Padding(padding: EdgeInsets.only(top: 16)),
                   Row(
                     children: [
                        Text(
                         "Thời gian bắt đầu :",
                         style: TextStyle(
                             fontSize: 14.sp,
                             color: const Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w400),
                       ),
                       Expanded(child: Container()),
                        Text("${ outputFormat.format(appointment.startTime)} ",
                           style:  TextStyle(
                               fontSize: 14.sp,
                               color: const Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w500))
                     ],
                   ),
                   const Padding(padding: EdgeInsets.only(top: 16)),
                   Row(
                     children: [
                        Text(
                         "Thời gian kết thúc :",
                         style: TextStyle(
                             fontSize: 14.sp,
                             color: const Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w400),
                       ),
                       Expanded(child: Container()),
                       Text(outputFormat.format(appointment.endTime),
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: const Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w500))
                     ],
                   ),
                   const Padding(padding: EdgeInsets.only(top: 16)),
                   Row(
                     children: [
                        Text(
                         "Ngày :",
                         style: TextStyle(
                             fontSize: 14.sp,
                             color: const Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w400),
                       ),
                       Expanded(child: Container()),
                        Text(outputDateFormat.format(appointment.startTime),
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: const Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w500))
                     ],
                   ),
                   const Padding(padding: EdgeInsets.only(top: 16)),
                   AppCache().userType != "TEACHER"? Row(
                     children: [
                        Text(
                         "Giáo viên: ",
                         style: TextStyle(
                             fontSize: 14.sp,
                             color: const Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w400),
                       ),
                       Expanded(child: Container()),
                       Text("${appointment.recurrenceId}",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: const Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w500))
                     ],
                   )
                       : Container(),
                 ],
               ),
             )
           ],
         ),
       ),
     ),
     );
  }
  void getDialog(){
    Get.dialog(OnDialog());
  }
  return
    InkWell(
      onTap: (){
        checkClickFeature(Get.find<HomeController>().userGroupByApp,   () =>getDialog(), StringConstant.FEATURE_LESSON_DASHBROAD_DETAIL);
      },
      child: Container(
        padding: const EdgeInsets.all(4),
          height: calendarAppointmentDetails.bounds.height,
          color: appointment.color,
          alignment: Alignment.center,
          width: calendarAppointmentDetails.bounds.width,
          child: Text(
            AppCache().userType == "TEACHER" ? appointment.notes!:appointment.subject,
            style:  TextStyle(color: Colors.white, fontSize: 12.sp),
          )
      ),
    );




}

