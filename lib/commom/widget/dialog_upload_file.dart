import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';



void showDialogUploadFile() {
  Get.dialog(
     const CupertinoAlertDialog(
      title: Text(
          'Đang tải...'),
    ),
    barrierDismissible: false,
  );
}