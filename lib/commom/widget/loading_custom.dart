import 'package:flutter/material.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';

class LoadingCustom extends StatelessWidget {
  const LoadingCustom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(
          color: ColorUtils.PRIMARY_COLOR
      ),
    );
  }
}
