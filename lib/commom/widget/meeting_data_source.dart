import 'dart:ui';

import 'package:syncfusion_flutter_calendar/calendar.dart';

class MeetingDataSource extends CalendarDataSource {
  MeetingDataSource(List<Appointment> source) {
    appointments = source;
  }
}

List<Appointment> getAppointments() {
  List<Appointment> meetings = <Appointment>[];
  final DateTime today = DateTime.now();
  final DateTime startTime =
      DateTime(today.year, today.month, today.day, 9, 0, 0);
  final DateTime endTime = startTime.add(const Duration(hours: 2));
  final DateTime startTimeChemistry =
      DateTime(today.year, today.month, today.day, 10, 0, 0);
  final DateTime endTimeChemistry =
      startTimeChemistry.add(const Duration(hours: 2));
  final DateTime startTimePhysical =
      DateTime(today.year, today.month, today.day, 11, 0, 0);
  final DateTime endTimePhysical =
      startTimePhysical.add(const Duration(hours: 2));
  meetings.add(Appointment(
      startTime: startTime,
      endTime: endTime,
      subject: "Toán",
      color: const Color.fromRGBO(249, 154, 81, 1)));
  meetings.add(Appointment(
      startTime: startTimeChemistry,
      endTime: endTimeChemistry,
      subject: "Hóa",
      color: const Color.fromRGBO(72, 98, 141, 1)));
  meetings.add(Appointment(
      startTime: startTimePhysical,
      endTime: endTimePhysical,
      subject: "Lý",
      color: const Color.fromRGBO(253, 185, 36, 1)));
  return meetings;
}

List<Appointment> getAppointment() {
  List<Appointment> meeting = <Appointment>[];
  final DateTime today = DateTime.now();
  final DateTime startTime =
      DateTime(today.year, today.month, today.day, 9, 0, 0);
  final DateTime endTime = startTime.add(const Duration(hours: 2));
  final DateTime startTimeChemistry =
      DateTime(today.year, today.month, today.day, 10, 0, 0);
  final DateTime endTimeChemistry =
      startTimeChemistry.add(const Duration(hours: 2));
  final DateTime startTimePhysical =
      DateTime(today.year, today.month, today.day, 11, 0, 0);
  final DateTime endTimePhysical =
      startTimePhysical.add(const Duration(hours: 2));
  meeting.add(Appointment(
      startTime: startTime,
      endTime: endTime,
      subject: "Lớp 12A2",
      color: const Color.fromRGBO(249, 154, 81, 1)));
  meeting.add(Appointment(
      startTime: startTimeChemistry,
      endTime: endTimeChemistry,
      subject: "Lớp 12A1",
      color: const Color.fromRGBO(72, 98, 141, 1)));
  meeting.add(Appointment(
      startTime: startTimePhysical,
      endTime: endTimePhysical,
      subject: "Lớp 12A3",
      color: const Color.fromRGBO(253, 185, 36, 1)));
  return meeting;
}
