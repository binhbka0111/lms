import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/textstyle.dart';

class EmptyScreen extends StatelessWidget {
  final Function()? onTap;

  const EmptyScreen({Key? key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {},
      child: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: SizedBox(
          width: Get.width,
          height: Get.height * 0.5,
          child: Center(
            child: GestureDetector(
              onTap: onTap,
              child: Scaffold(
                backgroundColor: ColorUtils.BG_COLOR,
                body: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset("assets/images/box.png",
                          width: Get.size.height * 0.2,
                          height: Get.size.height * 0.2,
                          fit: BoxFit.cover),
                      SizedBox(height: 30.h),
                      Text(
                        "no_data".tr,
                        style: TextStyleUtils.sizeText16Weight700()?.copyWith(
                          color: ColorUtils.COLOR_WHITE,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
