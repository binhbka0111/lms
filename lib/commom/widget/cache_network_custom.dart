import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';

class CacheNetWorkCustom extends StatelessWidget {
  const CacheNetWorkCustom({Key? key, required this.urlImage, this.radiusParam}) : super(key: key);
  final String urlImage;
  final double? radiusParam;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl:urlImage,
      imageBuilder: (context, imageProvider) =>
          Container(
            decoration: BoxDecoration(
              shape: radiusParam ==null ? BoxShape.circle : BoxShape.rectangle,
              borderRadius: radiusParam ==null ?  null: BorderRadius.circular(radiusParam ?? 40),
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,),
            ),
          ),
      placeholder: (context, url) =>  Center(child: Image.asset('assets/images/loading.gif')),
      color: ColorUtils.PRIMARY_COLOR,
      errorWidget: (context, url, error) =>
          Image.asset(
            "assets/images/img_Noavt.png",
          ),
    );
  }
}



class CacheNetWorkCustomBanner extends StatelessWidget {
  const CacheNetWorkCustomBanner({Key? key, required this.urlImage}) : super(key: key);
  final String urlImage;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl:urlImage,
      imageBuilder: (context, imageProvider) =>
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.scaleDown,),
            ),
          ),
      placeholder: (context, url) =>  Center(child: Image.asset('assets/images/loading.gif')),
      color: ColorUtils.PRIMARY_COLOR,
      errorWidget: (context, url, error) =>
          Image.asset(
            "assets/images/image_error_load.jpg",
          ),
    );
  }
}


class CacheNetWorkCustomBanner2 extends StatelessWidget {
  const CacheNetWorkCustomBanner2({Key? key, required this.urlImage}) : super(key: key);
  final String urlImage;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl:urlImage,
      imageBuilder: (context, imageProvider) =>
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,),
            ),
          ),
      placeholder: (context, url) =>  Center(child: Image.asset('assets/images/loading.gif')),
      color: ColorUtils.PRIMARY_COLOR,
      errorWidget: (context, url, error) =>
          Image.asset(
            "assets/images/image_error_load.jpg",
          ),
    );
  }
}

