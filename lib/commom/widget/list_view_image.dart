import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/ViewPdf.dart';
import 'package:slova_lms/commom/utils/global.dart';
import 'package:slova_lms/commom/utils/open_url.dart';
import 'package:get/get.dart';

class ListViewShowImage{
  static showGridviewImage(listImage){
    return Visibility(
        visible: listImage.isNotEmpty,
        child:  GridView.builder(
            padding: const EdgeInsets.all(10),
            gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: !Device.get().isTablet ? 2 : 4,
              childAspectRatio: 1,
              crossAxisSpacing: 10.w,
              mainAxisSpacing: 10.h
            ),
            scrollDirection: Axis.vertical,
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: listImage.length,
            itemBuilder: (context, indexGrid) {
              return InkWell(
                onTap: () {
                  OpenUrl.openImageViewer(context, listImage[indexGrid].link!);
                },
                child: Container(
                  padding: EdgeInsets.all(2.h),
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey
                    ),
                    borderRadius: BorderRadius.circular(6.r)
                  ),
                    child:
                    Image.network(
                      listImage[indexGrid].link ??
                          "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                      fit: BoxFit.scaleDown,
                      errorBuilder:
                          (context, object, stackTrace) {
                        return Image.asset(
                          "assets/images/image_error_load.jpg",
                        );
                      },
                    )

                ),
              );
            }));
  }


  static showListViewNotImage(listNotImage){
    return Visibility(
        visible:listNotImage.isNotEmpty,
        child: ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            padding: EdgeInsets.zero,
            itemCount: listNotImage.length,
            itemBuilder: (context, indexNotImage) {
              return InkWell(
                onTap: () {
                  var action = 0;
                  var extension = listNotImage[indexNotImage].ext;
                  if (extension == "png" ||
                      extension == "jpg" ||
                      extension == "jpeg" ||
                      extension == "gif" ||
                      extension == "bmp") {
                    action = 1;
                  } else if (extension == "pdf") {
                    action = 2;
                  } else {
                    action = 0;
                  }
                  switch (action) {
                    case 1:
                      OpenUrl.openImageViewer(context, listNotImage[indexNotImage].link!);
                      break;
                    case 2:
                      Get.to(ViewPdfPage(url:listNotImage[indexNotImage].link!));
                      break;
                    default:
                      OpenUrl.openFile(listNotImage[indexNotImage].link!);
                      break;
                  }
                },
                child: Container(
                  margin: const EdgeInsets.only(top: 12),
                  padding: const EdgeInsets.all(12),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: const Color.fromRGBO(246, 246, 246, 1)),
                  child: Row(
                    children: [
                      Image.network(
                        listNotImage[indexNotImage].link!,
                        errorBuilder: (
                            BuildContext context,
                            Object error,
                            StackTrace? stackTrace,
                            ) {
                          return Image.asset(
                            getFileIcon(listNotImage[indexNotImage].name),
                            height: 35,
                            width: 30,
                          );
                        },
                        height: 35,
                        width: 30,
                      ),
                      const Padding(padding: EdgeInsets.only(left: 8)),
                      Expanded(child: SizedBox(
                        child: Text(
                          '${listNotImage[indexNotImage].name}',
                          style: TextStyle(
                              color: const Color.fromRGBO(26, 59, 112, 1),
                              fontSize: 14.sp,
                              fontFamily: 'assets/font/static/Inter-Medium.ttf',
                              fontWeight: FontWeight.w500),
                        ),
                      ),),
                      Image.asset('assets/images/icon_upfile_subject.png',height: 16,width: 16,),
                    ],
                  ),
                ),
              );
            }));
  }
}