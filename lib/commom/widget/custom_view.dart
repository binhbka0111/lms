import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';

Widget getDialogConfirm(String title, Widget content,
    {String? btnLeft,
    String? btnRight,
    Color? colorBtnOk,
    Color? colorTextBtnOk,
    Function? funcLeft,
    Function? funcRight}) {
  return Container(
    decoration: const BoxDecoration(
      color: ColorUtils.COLOR_WHITE,
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(7.0),
        topRight: Radius.circular(7.0),
        bottomRight: Radius.circular(7.0),
        bottomLeft: Radius.circular(7.0),
      ),
    ),
    margin: EdgeInsets.only(
        left: 16, right: 16, bottom: Platform.isAndroid ? 65.h : 36.h),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: Get.width,
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          decoration: const BoxDecoration(
            color: ColorUtils.PRIMARY_COLOR,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(7.0),
              topRight: Radius.circular(7.0),
            ),
          ),
          child: Text.rich(
            TextSpan(
              children: [
                TextSpan(
                    text: title,
                    style: const TextStyle(
                      color: ColorUtils.COLOR_WHITE,
                      fontWeight: FontWeight.w700,
                    )),
              ],
            ),
            textHeightBehavior:
                const TextHeightBehavior(applyHeightToFirstAscent: false),
            softWrap: false,
          ),
        ),
        SizedBox(
          height: 16.h,
        ),
        Container(
          child: content,
          padding: const EdgeInsets.symmetric(horizontal: 8),
        ),
        SizedBox(
          height: 16.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
                child: GestureDetector(
              onTap: () {
                funcLeft?.call();
              },
              child: Container(
                  height: 40.h,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                  decoration: const BoxDecoration(
                    color: Color(0xffE4EEF7),
                    borderRadius:
                        BorderRadius.only(bottomLeft: Radius.circular(7.0)),
                  ),
                  child: Center(
                    child: Text(btnLeft ?? "Hủy",
                        style: const TextStyle(
                          fontSize: 14,
                          color: ColorUtils.PRIMARY_COLOR,
                          fontWeight: FontWeight.w700,
                        )),
                  )),
            )),
            Container(width: 1.w, color: Colors.white,),
            Expanded(
                child: GestureDetector(
              child: Container(
                  height: 40.h,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                  decoration: BoxDecoration(
                    color: Color(0xffE4EEF7),
                    borderRadius: const BorderRadius.only(
                        bottomRight: Radius.circular(7.0)),
                  ),
                  child: Center(
                    child: Text(btnRight ?? "Xác nhận",
                        style: TextStyle(
                          fontSize: 14,
                          color: colorBtnOk ?? ColorUtils.COLOR_WORK_TYPE_3,
                          fontWeight: FontWeight.w700,
                        )),
                  )),
              onTap: () {
                funcRight?.call();
              },
            ))
          ],
        ),
      ],
    ),
  );
}
