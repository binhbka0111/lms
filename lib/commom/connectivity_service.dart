import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import '../view/mobile/message/message_controller.dart';

class ConnectivityService extends GetxService {
  bool isShowingDialog = false;
  final Connectivity _connectivity = Connectivity();
  @override
  void onInit() async {
    super.onInit();
    final result = await _connectivity.checkConnectivity();
    if (result != ConnectivityResult.mobile && result != ConnectivityResult.wifi) {
      isShowingDialog = true;
      showDialog();
    }

    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.mobile ||
          result == ConnectivityResult.wifi) {
        if (isShowingDialog) {
          Get.back();
          if (Get.isRegistered<HomeController>()) {
            if (Get.find<HomeController>().tabController?.index == Get.find<HomeController>().listTabView.indexOf(Get.find<HomeController>().message)) {
              if (Get.find<MessageController>().socketIo.socket!.disconnected) {
                Get.find<MessageController>().socketIo.initSocket();
              }
            }
          }
        }
        isShowingDialog = false;
        AppUtils.shared.printLog(isShowingDialog);
      } else {
        if (!isShowingDialog) {
          isShowingDialog = true;
          showDialog();
          if(Get.isRegistered<HomeController>()){
            if(Get.find<HomeController>().tabController?.index == Get.find<HomeController>().listTabView.indexOf(Get.find<HomeController>().message)){
              Get.find<MessageController>().socketIo.socket?.disconnect();
              Get.find<MessageController>().socketIo.socket?.dispose();
            }
          }
        }
      }
    });

  }

  void showDialog() {
    Get.dialog(
      const CupertinoAlertDialog(
        title: Text(
            'Please turn on network connection to continue using this app'),
      ),
      barrierDismissible: false,
    );
  }
}
