// ignore_for_file: constant_identifier_names
class AppConstant {
  static const APP_STORE_URL =
      'https://apps.apple.com/vn/app/apple-store/id1599322138';
  static const CH_PLAY_URL =
      'https://play.google.com/store/apps/details?id=vn.greenstock.app';

  static const int TIME_UPDATE_STATE = 500; //MILISECOND
//loại dao dịch
  static const int TRADE_TYPE_BUY = 2; //mua
  static const int TRADE_TYPE_SELL = 1; //bán
//trạng thái lệnh
  static const int ODER_STATUS_ALL = 0; //TẤT CẢ
  static const int ODER_STATUS_WAIT_INPUT = 1; //CHỜ NHẬP
  static const int ODER_STATUS_NOT_APPROVED_YET = 1; //CHƯA DUYỆT
  static const int ODER_STATUS_WAIT_MATCH = 2; //CHỜ Khớp
  static const int ODER_STATUS_MATCH_A_PART = 3; //KHỚP 1 PHẦN
  static const int ODER_STATUS_MATCH = 4; //KHỚP HẾT
  static const int ODER_STATUS_WAIT_CANCEL = 5; //CHỜ HỦY
  static const int ODER_STATUS_CANCEL = 6; //ĐÃ HỦY
  static const int ODER_STATUS_MATCH_A_PART_CANCLE = 7; //KHỚP 1 PHẦN ĐÃ HỦY

//status subAccount
  static const int ACTIVE = 2; //Hoạt động
  static const int PENDING = 1; //Chờ duyệt
  static const int CANCLED = 9; //Đã xóa
//type SubAccount
  static const String BASIC_1 = "Cơ sở";
  static const String INCURRED = "Phát sinh";
  static const String FOREX = "FOREX";
  static const String BASIC = "BASIC";
  static const int SUB_ACC_TYPE_BASIC_1 = 1;
  static const int SUB_ACC_TYPE_INCURRED = 2;
  static const int SUB_ACC_TYPE_FOREX = 3;
  static const int SUB_ACC_TYPE_BASIC = 7;

//Rút tiền status
  static const int ALL = 0;
  static const int NOT_APPROVED = 1;
  static const int APPROVED = 2;
  static const int CANCELLED = 255;

//Rút tiền methodstatic const int SUB_ACC_TYPE_BASIC_1= 0;
  static const int TRANSFER_BANK = 1;
  static const int WITHDRAWAL = 2;
  static const int INTERNAL_TRANSFER = 3;

//type Statement
  static const int STATEMENT_TYPE_1 = 1; //Nộp rút tiền
  static const int STATEMENT_TYPE_2 = 2; //Đăng ký mua thêm
  static const int STATEMENT_TYPE_3 = 3; //Cổ tức tiền mặt
  static const int STATEMENT_TYPE_4 = 4; //Đóng vị thế
  static const int STATEMENT_TYPE_DOWN = 1; //Giảm
  static const int STATEMENT_TYPE_UP = 2; //Tăng

  //Gender type
  static const int GENDER_MALE = 1; //Nam
  static const int GENDER_FEMALE = 2; //Nữ
  //Type view expan
  static const int EXPAND = 0; //Nam
  static const int ASSET = 1; //Nam

  //Card type
  static const int CARD_TYPE_1 = 1; //Chứng minh nhân dân
  static const int CARD_TYPE_2 = 2; //Căn cước công dân
  static const int CARD_TYPE_3 = 3; //Hộ chiếu
  static const int CARD_TYPE_4 = 4; //Giấy phép kinh doanh
  static const int CARD_TYPE_5 = 5; //Khác

  static const String emailRegExp =
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";

  //NotifyStatus
  //loại dao dịch
  static const int READ = 2; //mua
  static const int UNREAD = 1; //bán

// chiều cao footer button
  static const double HEIGHT_FOOTER = 0.1;
}
