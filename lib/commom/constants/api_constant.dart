// ignore_for_file: constant_identifier_names

class ApiConstants {
  static const int CONNECT_TIMEOUT = 30000;
  static const int RECIEVE_TIMEOUT = 30000;

  //<=============== ENV dev ===============>


 //xtel
  static const String BASE_URL = "https://cms.lms.xteldev.com/";
  static const String ENDPOINT_URL = "api/"; //endpoint



  // slova
  // static const String BASE_URL = "https://api.lms.slova.dev/";
  // static const String ENDPOINT_URL = ""; //endpoint


  static const String socketServerURL = "http://103.147.34.112:3115";



  // static const String BASE_URL_API = "https://cms.lms.xteldev.com/api"; //HOST
  // static const String BASE_URL_GS = "https://www.greenstock.vn/greenstock/api/"; //HOST_GS
  // static const String BASE_URL_DATA = "https://trading.greenstock.vn/data/api/"; //HOST_DATA


  // static const String SOCKET_URL = "wss://test.greenstock.vn";

  // <============== ENV product =================>

  // static const String BASE_URL = "https://trading.greenstock.vn/";
  // static const String BASE_URL_ = "https://trading.greenstock.vn/frontapi/api/"; //HOST
  // static const String BASE_URL_GS = "https://trading.greenstock.vn/gsapi/api/"; //HOST_GS
  // static const String BASE_URL_DATA = "https://trading.greenstock.vn/data/api/"; //HOST_DATA
  // static const String ENDPOINT_URL = "frontapi/api/";  //endpoint
  // static const String SOCKET_URL = "wss://trading.greenstock.vn/ws";

  /*<=================================================>*/

  static const String API_LOGIN = "$BASE_URL${ENDPOINT_URL}auth/login";
  static const String API_NONE = "$BASE_URL$ENDPOINT_URL";
  static const String API_BASE_USER = "$BASE_URL${ENDPOINT_URL}users/";
  static const String API_LOGOUT = "$BASE_URL${ENDPOINT_URL}auth/logout";
  static const String API_USER_PROFILE = "$BASE_URL${ENDPOINT_URL}auth/this-profiles";
  static const String API_VERIFY_OTP = "$BASE_URL${ENDPOINT_URL}users/forgot-password/verify-otp";
  static const String API_SEND_OTP = "$BASE_URL${ENDPOINT_URL}users/forgot-password/send-otp";
  static const String API_CHANGE_PASS = "$BASE_URL${ENDPOINT_URL}users/forgot-password/change-password";
  static const String API_BANNER = "$BASE_URL${ENDPOINT_URL}banners";
  static const String API_LIST_STUDENT = "$BASE_URL${ENDPOINT_URL}students";
  static const String API_LIST_STUDENT_BY_TEACHER = "$BASE_URL${ENDPOINT_URL}classes";
  static const String API_STUDENT_BY_USER = "$BASE_URL${ENDPOINT_URL}parents";
  static const String API_LIST_CLASS_BY_USER = "$BASE_URL${ENDPOINT_URL}directorys";
  static const String API_UPLOAD_MULTI_FILE = "$BASE_URL${ENDPOINT_URL}files/upload-files";
  static const String API_UPLOAD_MULTI_FILE_AVATAR = "$BASE_URL${ENDPOINT_URL}files/upload";
  static const String API_UPLOAD_MULTI_FILE_EX = "$BASE_URL${ENDPOINT_URL}files/upload-files";
  static const String API_STATIC_PAGES = "$BASE_URL${ENDPOINT_URL}static-pages/this/view?type=";
  static const String API_SCHOOL_ID = "$BASE_URL${ENDPOINT_URL}config-info/current-school-year";
  static const String API_SCHOOL_ID_By_User = "$BASE_URL${ENDPOINT_URL}school-years/this/byUser";
  static const String API_CRPYPTO_SECRET = "$BASE_URL${ENDPOINT_URL}config-info/crypto-secret";
  static const String API_HOME_ROOM_TEACHER = "$BASE_URL${ENDPOINT_URL}config-info/crypto-secret";
  static const String API_LEAVING_APPLICATIONS = "$BASE_URL${ENDPOINT_URL}leave-applications";
  static const String API_DILIGENTS = "$BASE_URL${ENDPOINT_URL}class-diligents";
  static const String API_NOTIFICATION = "$BASE_URL${ENDPOINT_URL}notifications";
  static const String API_EXERCISES = "$BASE_URL${ENDPOINT_URL}exercises";
  static const String API_EXAMS = "$BASE_URL${ENDPOINT_URL}exams";
  static const String API_TRANSCRIPTS = "$BASE_URL${ENDPOINT_URL}transcripts";
  static const String API_SCHOOL = "$BASE_URL${ENDPOINT_URL}schools";
  static const String API_CHAT_ROOM = "$BASE_URL${ENDPOINT_URL}chat-rooms";


  static const String API_EVENT_NEWS = "$BASE_URL${ENDPOINT_URL}news";
  static const String API_EVENT_NEWS_CATEGORY = "$BASE_URL${ENDPOINT_URL}news-categories/by-user";
}
