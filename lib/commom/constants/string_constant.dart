// ignore_for_file: constant_identifier_names
class StringConstant {
  static const String SAVETHEME = 'SaveTheme';
  static const String SAVELANGUAGE = 'SaveLanguage';
  static const String SAVELOGINID = 'SaveLoginId';
  static const String SAVE_INFO_LOGIN = 'IS_SAVE_LOGIN';
  static const String LOCATION =
      "https://www.google.com/maps/place/46+Ng%E1%BB%A5y+Nh%C6%B0+Kon+Tum,+Nh%C3%A2n+Ch%C3%ADnh,+Thanh+Xu%C3%A2n,+H%C3%A0+N%E1%BB%99i/@20.9984631,105.7978953,17z/data=!3m1!4b1!4m5!3m4!1s0x3135acbc84a95147:0x4719ea81640609df!8m2!3d20.9984504!4d105.800065";
  static const String FACEBOOK = "https://www.facebook.com/greenstock.vn";
  static const String WEB = "https://greenstock.vn";
  static const String PHONE_NUMBER = "tel:19004514";
  static const String EMAIL = "customer@greenstock.vn";
  static const String TERMS_OF_USE = "https://greenstock.vn/customize";
  static const String saveNotiSilent = "saveNotiApproveMoney";
  static const String saveNotiSound = "saveNotiApproveCommand";
  static const String saveVibration = "saveVibration";




  static const String PAGE_NOTIFY = "PAGE_NOTIFY";
  static const String PAGE_LESSON = "PAGE_LESSON";
  static const String PAGE_HOME = "PAGE_HOME";
  static const String PAGE_DIRECTORY = "PAGE_DIRECTORY";
  static const String PAGE_MESSAGE = "PAGE_MESSAGE";

  static const String FEATURE_NOTIFY_SHARE = "FEATURE_NOTIFY_SHARE";
  static const String FEATURE_NOTIFY_DELETE = "FEATURE_NOTIFY_DELETE";
  static const String FEATURE_NOTIFY_SEEN = "FEATURE_NOTIFY_SEEN";
  static const String FEATURE_NOTIFY_CHOOSE = "FEATURE_NOTIFY_CHOOSE";
  static const String FEATURE_NOTIFY_DETAIL = "FEATURE_NOTIFY_DETAIL";
  static const String FEATURE_DIRECTORY_CALL = "FEATURE_DIRECTORY_CALL";
  static const String FEATURE_DIRECTORY_MESSAGE = "FEATURE_DIRECTORY_MESSAGE";
  static const String FEATURE_DIRECTORY_DETAIL = "FEATURE_DIRECTORY_DETAIL";
  static const String FEATURE_MESSAGE_ADD = "FEATURE_MESSAGE_ADD";
  static const String FEATURE_MESSAGE_NAME_EDIT = "FEATURE_MESSAGE_NAME_EDIT";
  static const String FEATURE_MESSAGE_IMAGE_EDIT = "FEATURE_MESSAGE_IMAGE_EDIT";
  static const String FEATURE_MESSAGE_USER_LIST = "FEATURE_MESSAGE_USER_LIST";
  static const String FEATURE_MESSAGE_NOTIFY = "FEATURE_MESSAGE_NOTIFY";
  static const String FEATURE_MESSAGE_DELETE = "FEATURE_MESSAGE_DELETE";
  static const String FEATURE_MESSAGE_USER_ADD = "FEATURE_MESSAGE_USER_ADD";
  static const String FEATURE_MESSAGE_DETAIL = "FEATURE_MESSAGE_DETAIL";
  static const String FEATURE_MESSAGE_FIND = "FEATURE_MESSAGE_FIND";
  static const String FEATURE_NOTIFY_CLASS = "FEATURE_NOTIFY_CLASS";
  static const String PAGE_MESSAGE_USER = "PAGE_MESSAGE_USER";
  static const String FEATURE_SEND_MESSAGE = "FEATURE_SEND_MESSAGE";
  static const String FEATURE_MESSAGE_USER_DELETE = "FEATURE_MESSAGE_USER_DELETE";
  static const String FEATURE_VIEW_PROFILE = "FEATURE_VIEW_PROFILE";
  static const String FEATURE_LESSON_DETAIL = "FEATURE_LESSON_DETAIL";
  static const String FEATURE_CLASS_LIST = "FEATURE_CLASS_LIST";
  static const String FEATURE_SCHOOL_YEARS_LIST = "FEATURE_SCHOOL_YEARS_LIST";
  static const String PAGE_MANAGE_DILIGENCE = "PAGE_MANAGE_DILIGENCE";
  static const String PAGE_MANAGE_LEARNING_MANAGEMENT = "PAGE_MANAGE_LEARNING_MANAGEMENT";
  static const String PAGE_SEND_NOTIFY = "PAGE_SEND_NOTIFY";
  static const String PAGE_LIST_STUDENT = "PAGE_LIST_STUDENT";
  static const String PAGE_NEWS = "PAGE_NEWS";
  static const String FEATURE_SUBJECT_DETAIL = "FEATURE_SUBJECT_DETAIL";
  static const String FEATURE_SEND_NOTIFY_SMS = "FEATURE_SEND_NOTIFY_SMS";
  static const String FEATURE_SEND_NOTIFY_IN_APP = "FEATURE_SEND_NOTIFY_IN_APP";
  static const String PAGE_MANAGE_LEAVE_APPLICATION = "PAGE_MANAGE_LEAVE_APPLICATION";
  static const String PAGE_STATISTICAL_DILIGENCE = "PAGE_STATISTICAL_DILIGENCE";
  static const String PAGE_DIARY_DILIGENCE = "PAGE_DIARY_DILIGENCE";
  static const String PAGE_DETAIL_DILIGENCE = "PAGE_DETAIL_DILIGENCE";
  static const String PAGE_ATTENDANCE = "PAGE_ATTENDANCE";
  static const String PAGE_LIST_LEAVE_APPLICATION = "PAGE_LIST_LEAVE_APPLICATION";
  static const String FEATURE_START_ATTENDANCE = "FEATURE_START_ATTENDANCE";
  static const String FEATURE_ATTENDANCE_CLASS = "FEATURE_ATTENDANCE_CLASS";
  static const String FEATURE_ON_TIME_LIST = "FEATURE_ON_TIME_LIST";
  static const String FEATURE_NOT_ON_TIME_LIST = "FEATURE_NOT_ON_TIME_LIST";
  static const String FEATURE_ABSENT_WITHOUT_LIST = "FEATURE_ABSENT_WITHOUT_LIST";
  static const String FEATURE_EXCUSED_ABSENCE_LIST = "FEATURE_EXCUSED_ABSENCE_LIST";
  static const String PAGE_PENDING_LIST = "PAGE_PENDING_LIST";
  static const String FEATURE_LEAVE_APPLICATION_ACTION = "FEATURE_LEAVE_APPLICATION_ACTION";
  static const String FEATURE_LEAVE_APPLICATION_DETAIL = "FEATURE_LEAVE_APPLICATION_DETAIL";
  static const String PAGE_CANCEL_LIST = "PAGE_CANCEL_LIST";
  static const String PAGE_APPROVED_LIST = "PAGE_APPROVED_LIST";
  static const String PAGE_TRANSCRIPT_SYNTHETIC = "PAGE_TRANSCRIPT_SYNTHETIC";
  static const String PAGE_LIST_SUBJECT = "PAGE_LIST_SUBJECT";
  static const String FEATURE_TRANSCRIPT_SYNTHETIC_ANNOUNCED = "FEATURE_TRANSCRIPT_SYNTHETIC_ANNOUNCED";
  static const String FEATURE_TRANSCRIPT_SYNTHETIC_DELETE = "FEATURE_TRANSCRIPT_SYNTHETIC_DELETE";
  static const String FEATURE_TRANSCRIPT_SYNTHETIC_EDIT = "FEATURE_TRANSCRIPT_SYNTHETIC_EDIT";
  static const String FEATURE_TRANSCRIPT_SYNTHETIC_DETAIL = "FEATURE_TRANSCRIPT_SYNTHETIC_DETAIL";
  static const String FEATURE_STUDENT_LIST = "FEATURE_STUDENT_LIST";
  static const String PAGE_TRANSCRIPT = "PAGE_TRANSCRIPT";
  static const String FEATURE_TRANSCRIPT_DETAIL = "FEATURE_TRANSCRIPT_DETAIL";
  static const String FEATURE_ANNOUNCED_TRANSCRIPT = "FEATURE_ANNOUNCED_TRANSCRIPT";
  static const String FEATURE_EDIT_TRANSCRIPT = "FEATURE_EDIT_TRANSCRIPT";
  static const String FEATURE_DELETE_TRANSCRIPT = "FEATURE_DELETE_TRANSCRIPT";
  static const String FEATURE_EXERCISE_EDIT = "FEATURE_EXERCISE_EDIT";
  static const String FEATURE_EXERCISE_DELETE = "FEATURE_EXERCISE_DELETE";
  static const String FEATURE_EXERCISE_PUBLIC = "FEATURE_EXERCISE_PUBLIC";
  static const String FEATURE_EXERCISE_PUBLIC_SCORE = "FEATURE_EXERCISE_PUBLIC_SCORE";
  static const String FEATURE_EXERCISE_MARK = "FEATURE_EXERCISE_MARK";
  static const String FEATURE_RESULT_EXERCISE_VIEW = "FEATURE_RESULT_EXERCISE_VIEW";
  static const String FEATURE_EXERCISE_DETAIL  = "FEATURE_EXERCISE_DETAIL";
  static const String FEATURE_EXAM_EDIT = "FEATURE_EXAM_EDIT";
  static const String FEATURE_EXAM_DELETE = "FEATURE_EXAM_DELETE";
  static const String FEATURE_EXAM_PUBLIC = "FEATURE_EXAM_PUBLIC";
  static const String FEATURE_EXAM_PUBLIC_SCORE = "FEATURE_EXAM_PUBLIC_SCORE";
  static const String FEATURE_EXAM_MARK = "FEATURE_EXAM_MARK";
  static const String FEATURE_RESULT_EXAM_VIEW = "FEATURE_RESULT_EXAM_VIEW";
  static const String FEATURE_EXAM_DETAIL = "FEATURE_EXAM_DETAIL";
  static const String FEATURE_NOTIFY_CREATE = "FEATURE_NOTIFY_CREATE";
  static const String FEATURE_LIST_NOTIFY = "FEATURE_LIST_NOTIFY";
  static const String FEATURE_NOTIFY_EDIT = "FEATURE_NOTIFY_EDIT";
  static const String FEATURE_NOTIFY_PIN = "FEATURE_NOTIFY_PIN";
  static const String FEATURE_LIST_NOTIFICATION = "FEATURE_LIST_NOTIFICATION";
  static const String PAGE_VIEW_EXERCISE = "PAGE_VIEW_EXERCISE";
  static const String FEATURE_EXERCISE_CREATE = "FEATURE_EXERCISE_CREATE";
  static const String FEATURE_EXAMS_CREATE = "FEATURE_EXAMS_CREATE";
  static const String PAGE_VIEW_EXAM = "PAGE_VIEW_EXAM";
  static const String PAGE_DILIGENCE = "PAGE_DILIGENCE";
  static const String PAGE_SUBJECT = "PAGE_SUBJECT";
  static const String PAGE_LESSON_LIST = "PAGE_LESSON_LIST";
  static const String PAGE_LEAVE_APPLICATION = "PAGE_LEAVE_APPLICATION";
  static const String PAGE_STATISTICAL = "PAGE_STATISTICAL";
  static const String PAGE_DIARY = "PAGE_DIARY";
  static const String FEATURE_LEAVE_APPLICATION_EDIT = "FEATURE_LEAVE_APPLICATION_EDIT";
  static const String FEATURE_LEAVE_APPLICATION_CANCEL = "FEATURE_LEAVE_APPLICATION_CANCEL";
  static const String FEATURE_LEAVE_APPLICATION_ADD = "FEATURE_LEAVE_APPLICATION_ADD";
  static const String FEATURE_STATISTICAL_DILIGENCE_DETAIL = "FEATURE_STATISTICAL_DILIGENCE_DETAIL";
  static const String FEATURE_DIARY_DILIGENCE_DETAIL = "FEATURE_DIARY_DILIGENCE_DETAIL";
  static const String PAGE_NOTIFY_SUBJECT = "PAGE_NOTIFY_SUBJECT";
  static const String FEATURE_NOTIFY_SUBJECT_PIN_LIST = "FEATURE_NOTIFY_SUBJECT_PIN_LIST";
  static const String FEATURE_NOTIFY_SUBJECT_PIN_DETAIL = "FEATURE_NOTIFY_SUBJECT_PIN_DETAIL";
  static const String PAGE_EXERCISE = "PAGE_EXERCISE";
  static const String PAGE_EXAMS = "PAGE_EXAMS";
  static const String FEATURE_EXERCISE_LIST = "FEATURE_EXERCISE_LIST";
  static const String FEATURE_EXAM_LIST = "FEATURE_EXAM_LIST";
  static const String FEATURE_TRANSCRIPT_LIST = "FEATURE_TRANSCRIPT_LIST";
  static const String FEATURE_DIARY_DILIGENCE_LIST = "FEATURE_DIARY_DILIGENCE_LIST";
  static const String FEATURE_TRANSCRIPT_SYNTHETIC_LIST = "FEATURE_TRANSCRIPT_SYNTHETIC_LIST";
  static const String FEATURE_LESSON_LIST = "FEATURE_LESSON_LIST";
  static const String FEATURE_NEWS_DETAIL = "FEATURE_NEWS_DETAIL";
  static const String FEATURE_NEWS_LIST = "FEATURE_NEWS_LIST";
  static const String FEATURE_LEAVE_APPLICATION_PENDING_DETAIL = "FEATURE_LEAVE_APPLICATION_PENDING_DETAIL";
  static const String FEATURE_LEAVE_APPLICATION_CANCEL_DETAIL = "FEATURE_LEAVE_APPLICATION_CANCEL_DETAIL";
  static const String FEATURE_LEAVE_APPLICATION_APPROVED_DETAIL = "FEATURE_LEAVE_APPLICATION_APPROVED_DETAIL";
  static const String PAGE_HOME_NOTIFY = "PAGE_HOME_NOTIFY";
  static const String FEATURE_STATISTICAL_DILIGENCE_LIST = "FEATURE_STATISTICAL_DILIGENCE_LIST";
  static const String PAGE_SUBJECT_DETAIL = "PAGE_SUBJECT_DETAIL";

  static const String PAGE_DETAIL_SUBJECT = "PAGE_DETAIL_SUBJECT";

  static const String PAGE_DOING_EXERCISE = "PAGE_DOING_EXERCISE";

  static const String FEATURE_NOTIFY_SUBJECT_DETAIL = "FEATURE_NOTIFY_SUBJECT_DETAIL";
  static const String FEATURE_EXERCISE_DOING = "FEATURE_EXERCISE_DOING";
  static const String FEATURE_EXERCISE_SAVE = "FEATURE_EXERCISE_SAVE";
  static const String FEATURE_EXERCISE_SUBMIT = "FEATURE_EXERCISE_SUBMIT";
  static const String FEATURE_EXAM_DOING = "FEATURE_EXAM_DOING";
  static const String FEATURE_EXAM_SAVE = "FEATURE_EXAM_SAVE";

  static const String FEATURE_EXAM_SUBMIT = "FEATURE_EXAM_SUBMIT";
  static const String PAGE_EXAM = "PAGE_EXAM";
  static const String PAGE_DOING_EXAM = "PAGE_DOING_EXAM";
  static const String PAGE_LESSON_DASHBROAD_LIST = "PAGE_LESSON_DASHBROAD_LIST";
  static const String FEATURE_LESSON_DASHBROAD_LIST = "FEATURE_LESSON_DASHBROAD_LIST";
  static const String FEATURE_LESSON_DASHBROAD_DETAIL = "FEATURE_LESSON_DASHBROAD_DETAIL";
  static const String PAGE_MANAGE_TRANSCRIPT = "PAGE_MANAGE_TRANSCRIPT";
  static const String PAGE_LIST_TRANSCRIPT = "PAGE_LIST_TRANSCRIPT";
  static const String PAGE_STATISTICAL_TRANSCRIPT = "PAGE_STATISTICAL_TRANSCRIPT";
  static const String PAGE_STATISTICAL_SYNTHETIC_PERCENT = "PAGE_STATISTICAL_SYNTHETIC_PERCENT";
  static const String PAGE_STATISTICAL_SYNTHETIC_TOP = "PAGE_STATISTICAL_SYNTHETIC_TOP";
  static const String FEATURE_TRANSCRIPT_PERCENT_LIST = "FEATURE_TRANSCRIPT_PERCENT_LIST";
  static const String FEATURE_TRANSCRIPT_PERCENT_DETAIL = "FEATURE_TRANSCRIPT_PERCENT_DETAIL";
  static const String FEATURE_TRANSCRIPT_SCHOOL_LIST = "FEATURE_TRANSCRIPT_SCHOOL_LIST";
  static const String FEATURE_TRANSCRIPT_BLOCK_LIST = "FEATURE_TRANSCRIPT_BLOCK_LIST";
  static const String FEATURE_TRANSCRIPT_CLASS_LIST = "FEATURE_TRANSCRIPT_CLASS_LIST";
  static const String PAGE_DETAIL_EXAM = "PAGE_DETAIL_EXAM";
  static const String PAGE_DETAIL_EXERCISE = "PAGE_DETAIL_EXERCISE";

}
