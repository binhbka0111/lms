class BaseModel {
  String? requestId;
  String? path;
  String? requestAt;
  int? code;
  String? messageCode;
  String? message;
  String? timestamp;
  dynamic data;

  BaseModel({
    this.requestId,
    this.path,
    this.requestAt,
    this.code,
    this.messageCode,
    this.message,
    this.timestamp,
    this.data,
  });

  BaseModel.fromJson(Map<String, dynamic> json) {
    requestId = json['requestId'];
    path = json['path'];
    requestAt = json['requestAt'];
    code = json['code'];
    messageCode = json['messageCode'].toString();
    message = json['message'];
    timestamp = json['timestamp'];
    data = json['data'];
  }
}
