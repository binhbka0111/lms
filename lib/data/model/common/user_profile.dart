import 'package:slova_lms/data/model/common/Position.dart';
import 'package:slova_lms/data/model/common/title.dart';
import 'package:slova_lms/data/model/res/file/response_file.dart';

import '../res/class/School.dart';
import 'contacts.dart';

class UserProfile {
  List<dynamic>? student;
  String? id;
  String? fullName;
  String? phone;
  String? email;
  SchoolData? school;
  Position? position;
  Title? title;
  ResponseFileUpload? img;
  List<Specialty>? specialty;
  String? status;
  String? workingStatus;
  String? sex;
  int? birthday;
  String? address;
  String? type;
  String? description;
  dynamic createdAt;
  String? createdBy;
  int? v;
  String? image;
  dynamic updatedAt;
  String? updatedBy;
  List<String>? classesOfHomeroomTeacher;
  List<String>? classOfMonitorStudent;
  int? dateStartWorked;
  ItemUserProfile? item;

  UserProfile(
      {this.student,
      this.id,
      this.fullName,
      this.phone,
      this.email,
      this.school,
      this.position,
      this.title,
      this.specialty,
      this.status,
      this.workingStatus,
      this.sex,
      this.birthday,
      this.address,
      this.type,
      this.description,
      this.createdAt,
      this.createdBy,
      this.v,
      this.image,
      this.updatedAt,
      this.updatedBy,
      this.dateStartWorked,
      this.item,
      this.classesOfHomeroomTeacher,
      this.classOfMonitorStudent});

  UserProfile.fromJson(dynamic json) {
    student = json["student"] ?? [];
    id = json["_id"];
    fullName = json["fullName"]??"";
    phone = json["phone"]??"";
    email = json["email"]??"";
    if(json["school"] == null || json["school"] == ""){
      school = null;
    } else {
      school = SchoolData.fromJson(json["school"]);
    }
    if(json["position"] == null || json["position"] == ""){
      position = null;
    } else {
      position = Position.fromJson(json["position"]);
    }
    if(json["title"] == null || json["title"] == ""){
      title = null;
    } else {
      title = Title.fromJson(json["title"]);
    }
    if (json['specialty'] != null) {
      specialty = [];
      json['specialty'].forEach((v) {
        specialty?.add(Specialty?.fromJson(v));
      });
    }

    status = json["status"];
    workingStatus = json["workingStatus"];
    sex = json["sex"];
    birthday = json["birthday"];
    address = json["address"];
    type = json["type"];
    description = json["description"];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"];
    v = json["__v"];
    image = json["image"]??"";
    updatedAt = json["updatedAt"];
    updatedBy = json["updatedBy"];
    dateStartWorked = json["dateStartWorked"];
    if(json["item"] == null || json["item"] == ""){
      item = null;
    } else {
      item = ItemUserProfile.fromJson(json["item"]);
    }
    classesOfHomeroomTeacher = json["classesOfHomeroomTeacher"] == null ? null : List<String>.from(json["classesOfHomeroomTeacher"]);
    classOfMonitorStudent = json["classOfMonitorStudent"] == null ? null : List<String>.from(json["classOfMonitorStudent"]);

  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if (student != null) {
      _data["student"] = student;
    }
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["phone"] = phone;
    _data["email"] = email;
    if (school != null) {
      _data["school"] = school?.toJson();
    }
    if (position != null) {
      _data["position"] = position?.toJson();
    }
    if (title != null) {
      _data["title"] = title?.toJson();
    }
    if (specialty != null) {
      _data["specialty"] = specialty?.map((e) => e.toJson()).toList();
    }
    _data["status"] = status;
    _data["workingStatus"] = workingStatus;
    _data["sex"] = sex;
    _data["birthday"] = birthday;
    _data["address"] = address;
    _data["type"] = type;
    _data["description"] = description;
    _data["createdAt"] = createdAt;
    _data["createdBy"] = createdBy;
    _data["__v"] = v;
    _data["image"] = image;
    _data["updatedAt"] = updatedAt;
    _data["updatedBy"] = updatedBy;
    _data["dateStartWorked"] = dateStartWorked;
    if (item != null) {
      _data["item"] = item?.toJson();
    }
    final img = this.img;
    if (img != null) {
      _data['img'] = img.toJson();
    }
    if(classesOfHomeroomTeacher != null) {
      _data["classesOfHomeroomTeacher"] = classesOfHomeroomTeacher;
    }
    if(classOfMonitorStudent != null) {
      _data["classOfMonitorStudent"] = classOfMonitorStudent;
    }

    return _data;
  }
}

class ItemUserProfile {
  List<Student>? students;
  List<Clazzs>? clazzs;
  List<Teachers>? teachers;

  ItemUserProfile({this.students, this.clazzs, this.teachers});

  ItemUserProfile.fromJson(dynamic json) {
    if (json['students'] != null) {
      students = [];
      json['students'].forEach((v) {
        students?.add(Student?.fromJson(v));
      });
    }
    if (json['clazzs'] != null) {
      clazzs = [];
      json['clazzs'].forEach((v) {
        clazzs?.add(Clazzs?.fromJson(v));
      });
    }

    if (json['teachers'] != null) {
      teachers = [];
      json['teachers'].forEach((v) {
        teachers?.add(Teachers?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if (students != null) {
      _data["students"] = students;
    }
    if (clazzs != null) {
      _data["clazzs"] = clazzs;
    }
    if (teachers != null) {
      _data["teachers"] = teachers?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class Teachers {
  String? type;
  dynamic subjectName;
  List<Clazzs>? clazz;

  Teachers({this.type, this.subjectName, this.clazz});

  Teachers.fromJson(dynamic json) {
    type = json["type"];
    subjectName = json["subjectName"];
    if (json['clazz'] != null) {
      clazz = [];
      json['clazz'].forEach((v) {
        clazz?.add(Clazzs?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["type"] = type;
    _data["subjectName"] = subjectName;
    if (clazz != null) {
      _data["clazz"] = clazz;
    }
    return _data;
  }
}

class Clazzs {
  String? id;
  String? name;

  Clazzs({this.id, this.name});

  Clazzs.fromJson(dynamic json) {
    id = json["_id"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["name"] = name;
    return _data;
  }
}

class Specialty {
  String? id;
  String? key;
  String? name;
  String? description;
  int? v;

  Specialty({this.id, this.key, this.name, this.description, this.v});

  Specialty.fromJson(dynamic json) {
    id = json["_id"];
    key = json["_key"];
    name = json["name"];
    description = json["description"];
    v = json["__v"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["_key"] = key;
    _data["name"] = name;
    _data["description"] = description;
    _data["__v"] = v;
    return _data;
  }
}

