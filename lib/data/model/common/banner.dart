class BannerHomePage {
  num? pageIndex;
  String? pageSize;
  num? totalPages;
  num? totalItems;
  List<ItemBanner>? items;

  BannerHomePage(
      {this.pageIndex,
        this.pageSize,
        this.totalPages,
        this.totalItems,
        this.items});

  BannerHomePage.fromJson(dynamic json) {
    pageIndex = json['pageIndex'];
    pageSize = json['pageSize'];
    totalPages = json['totalPages'];
    totalPages = json['totalPages'];
    if (json['items'] != null) {
      items = [];
      json['items'].forEach((v) {
        items?.add(ItemBanner?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['pageIndex'] = pageIndex;
    map['pageSize'] = pageSize;
    map['totalPages'] = totalPages;
    map['totalPages'] = totalPages;
    final items = this.items;
    if (items != null) {
      map['items'] = items.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class ItemBanner {
  String? id;
  String? name;
  String? url;
  String? type;
  bool selected = false;

  ItemBanner(
      {this.id,
        this.url,
        this.name,
        this.type,});

  ItemBanner.fromJson(dynamic json) {
    id = json['_id'];
    name = json['name'];
    url = json['url'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['url'] = url;
    map['name'] = name;
    map['type'] = type;
    return map;
  }
}
