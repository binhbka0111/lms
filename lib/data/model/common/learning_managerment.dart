
import 'package:slova_lms/data/model/res/exercise/exercise.dart';
import '../res/file/response_file.dart';

class ExerciseStudent {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  List<ItemsExercise>? items;

  ExerciseStudent({this.pageIndex, this.pageSize, this.totalPages, this.totalItems, this.items});

  ExerciseStudent.fromJson(Map<String, dynamic> json) {
    pageIndex = json["pageIndex"];
    pageSize = json["pageSize"];
    totalPages = json["totalPages"];
    totalItems = json["totalItems"];
    items = json["items"] == null ? null : (json["items"] as List).map((e) => ItemsExercise.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["pageIndex"] = pageIndex;
    _data["pageSize"] = pageSize;
    _data["totalPages"] = totalPages;
    _data["totalItems"] = totalItems;
    if(items != null) {
      _data["items"] = items?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}
class ItemsExercise  {
  String? id;
  String? title;
  String? teacher;
  String? typeExercise;
  String? typeExamExercise;
  int? scoreFactor;
  int? deadline;
  String? status;
  String? description;
  List<ResponseFileUpload>? files;
  int? createdAt;
  int? updatedAt;
  String? createdBy;
  String? updatedBy;
  int? v;
  String? isPublic;
  String ? statusExerciseByStudent;
  List<String>? questions;
  String? link;
  String? isSubmit;
  String? isPublicScore;
  dynamic scoreOfExercise;
  int ? examTime;
  dynamic  startTime;
  dynamic  endTime;

  ItemsExercise ({this.id, this.scoreOfExercise,this.typeExamExercise,this.examTime, this.title, this.teacher, this.questions,
    this.startTime, this.endTime,
    this.isSubmit,
    this.isPublicScore,
    this.typeExercise, this.scoreFactor, this.deadline, this.status, this.description, this.files, this.createdAt, this.updatedAt, this.createdBy, this.updatedBy, this.v, this.isPublic,this.statusExerciseByStudent});

  ItemsExercise.fromJson(dynamic json) {
    id = json["_id"];
    title = json["title"];
    teacher = json["teacher"];

    typeExercise = json["typeExercise"];
    typeExamExercise = json["typeExamExercise"];
    scoreFactor = json["scoreFactor"];
    deadline = json["deadline"];
    status = json["status"];
    description = json["description"];
    files = json["files"] == null ? null : (json["files"] as List).map((e) => ResponseFileUpload.fromJson(e)).toList();
    createdAt = json["createdAt"];
    isPublicScore = json["isPublicScore"];
    startTime = json["startTime"];
    endTime = json["endTime"];
    scoreOfExercise = json["scoreOfExercise"];
    examTime = json["examTime"];
    updatedAt = json["updatedAt"];
    createdBy = json["createdBy"];
    updatedBy = json["updatedBy"];
    v = json["__v"];
    isPublic = json["isPublic"];
    isSubmit = json["isSubmit"];
    statusExerciseByStudent = json["statusExerciseByStudent"];
    questions = json["questions"] == null ? null : List<String>.from(json["questions"]);
    link = json["link"]??"";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["title"] = title;
    _data["teacher"] = teacher;
    _data["startTime"] = startTime;
    _data["endTime"] = endTime;
    _data["scoreOfExercise"] = scoreOfExercise;
    _data["examTime"] = examTime;

    if(questions != null) {
      _data["questions"] = questions;
    }
    _data["typeExercise"] = typeExercise;
    _data["typeExamExercise"] = typeExamExercise;
    _data["scoreFactor"] = scoreFactor;
    _data["deadline"] = deadline;
    _data["status"] = status;
    _data["description"] = description;
    if(files != null) {
      _data["files"] = files?.map((e) => e.toJson()).toList();
    }
    _data["createdAt"] = createdAt;
    _data["updatedAt"] = updatedAt;
    _data["createdBy"] = createdBy;
    _data["updatedBy"] = updatedBy;
    _data["__v"] = v;
    _data["isPublic"] = isPublic;
    _data["isSubmit"] = isSubmit;
    _data["statusExerciseByStudent"] = statusExerciseByStudent;
    return _data;
  }
}


class ExamStudent {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  List<ItemsExam>? items;

  ExamStudent({this.pageIndex, this.pageSize, this.totalPages, this.totalItems, this.items});

  ExamStudent.fromJson(Map<String, dynamic> json) {
    pageIndex = json["pageIndex"];
    pageSize = json["pageSize"];
    totalPages = json["totalPages"];
    totalItems = json["totalItems"];
    items = json["items"] == null ? null : (json["items"] as List).map((e) => ItemsExam.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["pageIndex"] = pageIndex;
    _data["pageSize"] = pageSize;
    _data["totalPages"] = totalPages;
    _data["totalItems"] = totalItems;
    if(items != null) {
      _data["items"] = items?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}
class ItemsExam  {
  String? id;
  String? title;
  String? teacher;
  String? typeExercise;
  int? scoreFactor;
  int? startTime;
  int? endTime;
  String? status;
  String? description;
  List<ResponseFileUpload>? files;
  int? createdAt;
  int? updatedAt;
  String? createdBy;
  String? updatedBy;
  int? v;
  String? isPublic;
  String ? statusExerciseByStudent;
  List<String>? questions;
  String? link;
  String? isSubmit;
  String? isPublicScore;
  dynamic scoreOfExam;

  ItemsExam ({this.scoreOfExam,this.id, this.title, this.teacher,  this.questions,
    this.isSubmit,
    this.isPublicScore,
    this.typeExercise, this.scoreFactor, this.startTime,this.endTime,this.link, this.status, this.description, this.files, this.createdAt, this.updatedAt, this.createdBy, this.updatedBy, this.v, this.isPublic,this.statusExerciseByStudent});

  ItemsExam.fromJson(dynamic json) {
    id = json["_id"];
    title = json["title"];
    teacher = json["teacher"];

    typeExercise = json["typeExercise"];
    scoreFactor = json["scoreFactor"];
    startTime = json["startTime"];
    endTime = json["endTime"];
    status = json["status"];
    description = json["description"];
    files = json["files"] == null ? null : (json["files"] as List).map((e) => ResponseFileUpload.fromJson(e)).toList();
    createdAt = json["createdAt"];
    updatedAt = json["updatedAt"];
    createdBy = json["createdBy"];
    updatedBy = json["updatedBy"];
    v = json["__v"];
    isPublic = json["isPublic"];
    isSubmit = json["isSubmit"];
    statusExerciseByStudent = json["statusExerciseByStudent"];
    isPublicScore = json["isPublicScore"];
    scoreOfExam = json["scoreOfExam"];
    questions = json["questions"] == null ? null : List<String>.from(json["questions"]);
    link = json["link"]??"";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["title"] = title;
    _data["teacher"] = teacher;

    if(questions != null) {
      _data["questions"] = questions;
    }
    _data["typeExercise"] = typeExercise;
    _data["scoreFactor"] = scoreFactor;
    _data["status"] = status;
    _data["description"] = description;
    if(files != null) {
      _data["files"] = files?.map((e) => e.toJson()).toList();
    }
    _data["createdAt"] = createdAt;
    _data["updatedAt"] = updatedAt;
    _data["createdBy"] = createdBy;
    _data["updatedBy"] = updatedBy;
    _data["__v"] = v;
    _data["isPublic"] = isPublic;
    _data["isSubmit"] = isSubmit;
    _data["statusExerciseByStudent"] = statusExerciseByStudent;
    return _data;
  }
}


class DetailExerciser {
  String? id;
  String? title;
  Teacher? teacher;
  Subject? subject;
  Clazz? clazz;
  List<QuestionsStudent>? questions;
  String? typeExercise;
  int ? scoreOfExercise;
  int? deadline;
  String? isPublic;
  String? status;
  String? description;
  int? createdAt;
  int? updatedAt;
  String? createdBy;
  String? updatedBy;
  int? v;
  String? statusExerciseByStudent;
  Student? student;
  List<QuestionAnswers>? questionAnswers;
  List<FilesExercise> ? files;
  List<FilesExercise> ? filesAnswer;
  String? link;
  String ? contentAnswer;
  String ? teacherComment;
  String ? isPublicScore;
  int ? score;
  dynamic startTime;
  dynamic endTime;
  int ? scoreOfExam;
  int ? examTime;
  String ? isGrade;
  String ? isSubmit;

  DetailExerciser({this.id,this.isGrade,this.isSubmit,this.scoreOfExercise,this.scoreOfExam, this.examTime,this.startTime, this.endTime, this.isPublicScore,this.title, this.teacher, this.subject, this.clazz, this.questions,
    this.typeExercise, this.deadline, this.isPublic, this.status, this.description, this.createdAt,
    this.updatedAt,this.teacherComment,this.score, this.createdBy, this.updatedBy, this.v, this.statusExerciseByStudent, this.student, this.questionAnswers,this.files,this.link, this.contentAnswer});

  DetailExerciser.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    title = json["title"];
    teacher = json["teacher"] == null ? null : Teacher.fromJson(json["teacher"]);
    student = json["student"] == null ? null : Student.fromJson(json["student"]);
    subject = json["subject"] == null ? null : Subject.fromJson(json["subject"]);
    clazz = json["clazz"] == null ? null : Clazz.fromJson(json["clazz"]);
    questions = json["questions"] == null ? null : (json["questions"] as List).map((e) => QuestionsStudent.fromJson(e)).toList();
    typeExercise = json["typeExercise"];
    deadline = json["deadline"];
    isPublic = json["isPublic"];
    status = json["status"];
    description = json["description"];
    createdAt = json["createdAt"];
    updatedAt = json["updatedAt"];
    createdBy = json["createdBy"];
    updatedBy = json["updatedBy"];
    teacherComment = json["teacherComment"];
    scoreOfExercise = json["scoreOfExercise"];
    isPublicScore = json["isPublicScore"];
    score = json["score"];
    isGrade = json["isGrade"];
    isSubmit = json["isSubmit"];
    scoreOfExam = json["scoreOfExam"];
    examTime = json["examTime"];
    startTime = json["startTime"];
    endTime = json["endTime"];
    link = json["link"];
    contentAnswer = json["contentAnswer"];
    v = json["__v"];
    files = json["files"] == null ? null : (json["files"] as List).map((e) => FilesExercise.fromJson(e)).toList();
    filesAnswer = json["filesAnswer"] == null ? null : (json["filesAnswer"] as List).map((e) => FilesExercise.fromJson(e)).toList();
    statusExerciseByStudent = json["statusExerciseByStudent"];
    questionAnswers = json["questionAnswers"] == null ? null : (json["questionAnswers"] as List).map((e) => QuestionAnswers.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["title"] = title;
    if(teacher != null) {
      _data["teacher"] = teacher?.toJson();
    }
    if(student != null) {
      _data["student"] = student?.toJson();
    }
    if(subject != null) {
      _data["subject"] = subject?.toJson();
    }
    if(clazz != null) {
      _data["clazz"] = clazz?.toJson();
    }
    if(questions != null) {
      _data["questions"] = questions?.map((e) => e.toJson()).toList();
    }
    if(files != null) {
      _data["files"] = files?.map((e) => e.toJson()).toList();
    }
    if(filesAnswer != null) {
      _data["filesAnswer"] = filesAnswer?.map((e) => e.toJson()).toList();
    }
    _data["typeExercise"] = typeExercise;
    _data["isGrade"] = isGrade;
    _data["isSubmit"] = isSubmit;
    _data["scoreOfExercise"] = scoreOfExercise;
    _data["endTime"] = endTime;
    _data["startTime"] = startTime;
    _data["examTime"] = examTime;
    _data["scoreOfExam"] = scoreOfExam;
    _data["teacherComment"] = teacherComment;
    _data["isPublicScore"] = isPublicScore;
    _data["deadline"] = deadline;
    _data["isPublic"] = isPublic;
    _data["status"] = status;
    _data["description"] = description;
    _data["createdAt"] = createdAt;
    _data["updatedAt"] = updatedAt;
    _data["createdBy"] = createdBy;
    _data["updatedBy"] = updatedBy;
    _data["score"] = score;
    _data["__v"] = v;
    _data["link"] = link;
    _data["contentAnswer"] = contentAnswer;
    _data["statusExerciseByStudent"] = statusExerciseByStudent;
    _data["student"] = student;
    if(questionAnswers != null) {
      _data["questionAnswers"] = questionAnswers?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class QuestionsStudent {
  String? id;
  String? content;
  dynamic point;
  String? typeQuestion;
  List<AnswerOptionExStudent>? answerOption;
  List<FilesExercise>? files;
  int? createdAt;
  int? updatedAt;
  String ? typeExamExercise;
  String? createdBy;
  String? updatedBy;
  int? v;

  QuestionsStudent({this.id,this.typeExamExercise, this.content, this.point, this.typeQuestion, this.answerOption, this.files, this.createdAt, this.updatedAt, this.createdBy, this.updatedBy, this.v});

  QuestionsStudent.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    content = json["content"];
    point = json["point"];
    typeQuestion = json["typeQuestion"];
    answerOption = json["answerOption"] == null ? null : (json["answerOption"] as List).map((e) => AnswerOptionExStudent.fromJson(e)).toList();
    files = json["files"] == null ? null : (json["files"] as List).map((e) => FilesExercise.fromJson(e)).toList();
    createdAt = json["createdAt"];
    typeExamExercise = json["typeExamExercise"];
    updatedAt = json["updatedAt"];
    createdBy = json["createdBy"];
    updatedBy = json["updatedBy"];
    v = json["__v"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["content"] = content;
    _data["point"] = point;
    _data["typeQuestion"] = typeQuestion;
    if(answerOption != null) {
      _data["answerOption"] = answerOption?.map((e) => e.toJson()).toList();
    }
    if(files != null) {
      _data["files"] = files?.map((e) => e.toJson()).toList();
    }
    _data["createdAt"] = createdAt;
    _data["typeExamExercise"] = typeExamExercise;
    _data["updatedAt"] = updatedAt;
    _data["createdBy"] = createdBy;
    _data["updatedBy"] = updatedBy;
    _data["__v"] = v;
    return _data;
  }
}

class Clazz {
  String? id;
  ClassCategory? classCategory;

  Clazz({this.id, this.classCategory});

  Clazz.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    classCategory = json["classCategory"] == null ? null : ClassCategory.fromJson(json["classCategory"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(classCategory != null) {
      _data["classCategory"] = classCategory?.toJson();
    }
    return _data;
  }
}

class ClassCategory {
  String? id;
  String? name;

  ClassCategory({this.id, this.name});

  ClassCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["name"] = name;
    return _data;
  }
}

class Subject {
  String? id;
  SubjectCategory? subjectCategory;

  Subject({this.id, this.subjectCategory});

  Subject.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    subjectCategory = json["subjectCategory"] == null ? null : SubjectCategory.fromJson(json["subjectCategory"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(subjectCategory != null) {
      _data["subjectCategory"] = subjectCategory?.toJson();
    }
    return _data;
  }
}

class SubjectCategory {
  String? id;
  String? name;

  SubjectCategory({this.id, this.name});

  SubjectCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["name"] = name;
    return _data;
  }
}

class Teacher {
  String? id;
  String? fullName;

  Teacher({this.id, this.fullName});

  Teacher.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    return _data;
  }
}
class FilesExercise {
  String? link;
  String? originalFileName;
  String? name;
  String? tmpFolderUpload;
  String? size;
  String ? ext;


  FilesExercise({this.link, this.originalFileName, this.name, this.tmpFolderUpload, this.size,this.ext});

  FilesExercise.fromJson(Map<String, dynamic> json) {
    link = json["link"];
    originalFileName = json["originalFileName"];
    name = json["name"];
    tmpFolderUpload = json["tmpFolderUpload"];
    size = json["size"];
    ext = json["ext"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["link"] = link;
    _data["originalFileName"] = originalFileName;
    _data["name"] = name;
    _data["tmpFolderUpload"] = tmpFolderUpload;
    _data["size"] = size;
    _data["ext"] = ext;
    return _data;
  }
}
class QuestionAnswers {
  String? id;
  String? exercise;
  QuestionsStudent? question;
  String? exerciseAnswer;
  String? student;
  String? answer;
  dynamic point;
  List<FilesExercise>? files;
  int? createdAt;
  int? v;
  String? typeQuestion;
  String? teacherComment;

  QuestionAnswers({this.id, this.teacherComment,this.exercise, this.question, this.exerciseAnswer, this.student, this.answer, this.point, this.files, this.createdAt, this.v, this.typeQuestion});

  QuestionAnswers.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    student = json["student"];
    exercise = json["exercise"];
    question = json["question"] == null ? null : QuestionsStudent.fromJson(json["question"]);
    exerciseAnswer = json["exerciseAnswer"];
    answer = json["answer"];
    point = json["point"];
    files = json["files"] == null ? null : (json["files"] as List).map((e) => FilesExercise.fromJson(e)).toList();
    createdAt = json["createdAt"];
    v = json["__v"];
    typeQuestion = json["typeQuestion"];
    teacherComment = json["teacherComment"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["exercise"] = exercise;
    if(question != null) {
      _data["question"] = question?.toJson();
    }
    _data["exerciseAnswer"] = exerciseAnswer;
    _data["student"] = student;
    _data["answer"] = answer;
    _data["point"] = point;
    if(files != null) {
      _data["files"] = files?.map((e) => e.toJson()).toList();
    }
    _data["createdAt"] = createdAt;
    _data["__v"] = v;
    _data["typeQuestion"] = typeQuestion;
    _data["teacherComment"] = teacherComment;
    return _data;
  }
}

class Student {
  String? id;
  String? fullName;
  String? image;
  int? birthday;

  Student({this.id, this.fullName, this.image,this.birthday});

  Student.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    image = json["image"];
    birthday = json["birthday"];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["image"] = image;

    return _data;
  }
}


class QuestinAnswersPost{
  String ? questionId;
  String ? answer;
  List<FilesPost>? files;


  QuestinAnswersPost({this.questionId, this.answer, this.files});

  QuestinAnswersPost.fromJson(Map<String, dynamic> json) {
    questionId = json["questionId"];
    answer = json["answer"];
    files = json["files"] == null ? null : (json["files"] as List).map((e) => FilesPost.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["questionId"] = questionId;
    _data["answer"] = answer;
    if(files != null) {
      _data["files"] = files?.map((e) => e.toJson()).toList();
    }

    return _data;
  }
}


class FilesPost {
  String ? name;
  String ? tmpFolderUpload;
  String ? size;
  FilesPost({this.name, this.tmpFolderUpload, this.size});

  FilesPost.fromJson(Map<String, dynamic> json) {
    name = json["name"];
    tmpFolderUpload = json["tmpFolderUpload"];
    size = json["size"];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["name"] = name;
    _data["tmpFolderUpload"] = tmpFolderUpload;
    _data["size"] = size;

    return _data;
  }

}




