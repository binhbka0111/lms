import 'package:slova_lms/data/model/common/user_profile.dart';
import '../res/class/School.dart';

class SchoolYear {
  String? id;
  SchoolData? school;
  int? fromYear;
  int? toYear;
  String? status;
  int? createdAt;
  String? createdBy;
  int? v;
  String? image;
  String? fullName;
  List<Clazzs>? clazz;


  SchoolYear({this.id, this.school, this.fromYear, this.toYear, this.status, this.createdAt, this.createdBy, this.v, this.image, this.fullName,this.clazz});

  SchoolYear.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    if(json["school"] == null || json["school"] == ""){
      school = null;
    } else {
      school = SchoolData.fromJson(json["school"]);
    }
    fromYear = json["fromYear"];
    toYear = json["toYear"];
    status = json["status"];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"];
    v = json["__v"];
    image = json["image"];
    fullName = json["fullName"];
    if (json['clazz'] != null) {
      clazz = [];
      json['clazz'].forEach((v) {
        clazz?.add(Clazzs?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(school != null) {
      _data["school"] = school?.toJson();
    }
    _data["fromYear"] = fromYear;
    _data["toYear"] = toYear;
    _data["status"] = status;
    _data["createdAt"] = createdAt;
    _data["createdBy"] = createdBy;
    _data["__v"] = v;
    _data["image"] = image;
    _data["fullName"] = fullName;
    final clazz = this.clazz;
    if (clazz != null) {
      _data['clazz'] = clazz.map((v) => v.toJson()).toList();
    }
    return _data;
  }
}

