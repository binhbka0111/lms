import 'package:slova_lms/data/model/common/user_profile.dart';
import 'package:slova_lms/data/model/res/class/School.dart';

class StudentByParent {
  String? id;
  String? email;
  String? phone;
  String? fullName;
  String? image;
  int? birthday;
  SchoolData? school;
  List<Clazzs>? clazz;
  String? position;

  StudentByParent({this.id, this.email, this.phone, this.fullName, this.image, this.birthday, this.school, this.clazz, this.position});

  StudentByParent.fromJson(dynamic json) {
    id = json["_id"];
    email = json["email"];
    phone = json["phone"];
    fullName = json["fullName"]??"";
    image = json["image"];
    birthday = json["birthday"];
    if(json["school"] == null || json["school"] == ""){
      school = null;
    } else {
      school = SchoolData.fromJson(json["school"]);
    }
    if (json['clazz'] != null) {
      clazz = [];
      json['clazz'].forEach((v) {
        clazz?.add(Clazzs?.fromJson(v));
      });
    }
    position = json["position"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["email"] = email;
    _data["phone"] = phone;
    _data["fullName"] = fullName;
    _data["image"] = image;
    _data["birthday"] = birthday;
    if(school != null) {
      _data["school"] = school?.toJson();
    }
    if(clazz != null) {
      _data["clazz"] = clazz?.map((e) => e.toJson()).toList();
    }
    _data["position"] = position;
    return _data;
  }
}
