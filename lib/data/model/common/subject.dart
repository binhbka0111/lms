
class SubjectRes {
  String? id;
  SubjectCategory? subjectCategory;
  String? school;
  String? schoolYear;
  String? status;
  String? block;
  String? clazz;
  int? v;
  SubjectTeacher? teacher;

  SubjectRes({this.id, this.subjectCategory, this.school, this.schoolYear, this.status, this.block, this.clazz, this.v, this.teacher});

  SubjectRes.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    subjectCategory = json["subjectCategory"] == null ? null : SubjectCategory.fromJson(json["subjectCategory"]);
    school = json["school"];
    schoolYear = json["schoolYear"];
    status = json["status"];
    block = json["block"];
    clazz = json["clazz"];
    v = json["__v"];
    teacher = json["teacher"] == null ? null : SubjectTeacher.fromJson(json["teacher"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(subjectCategory != null) {
      _data["subjectCategory"] = subjectCategory?.toJson();
    }
    _data["school"] = school;
    _data["schoolYear"] = schoolYear;
    _data["status"] = status;
    _data["block"] = block;
    _data["clazz"] = clazz;
    _data["__v"] = v;
    if(teacher != null) {
      _data["teacher"] = teacher?.toJson();
    }
    return _data;
  }
}

class SubjectTeacher {
  String? id;
  String? fullName;
  String? phone;
  String? email;
  String? school;
  String? position;
  String? title;
  List<String>? subjectCategories;
  String? status;
  String? workingStatus;
  String? sex;
  int? birthday;
  String? address;
  String? type;
  String? description;
  int? dateStartWorked;
  int? createdAt;
  String? createdBy;
  List<String>? schoolYearId;
  int? v;
  int? updatedAt;
  String? updatedBy;
  List<String>? classId;
  List<dynamic>? homeRoomOfClass;
  List<String>? schoolYear;
  List<String>? subjectId;
  List<String>? classes;
  List<String>? subject;
  List<dynamic>? fcmTokens;
  List<dynamic>? classesOfHomeroomTeacher;
  List<dynamic>? specialty;
  List<dynamic>? student;

  SubjectTeacher({this.id, this.fullName, this.phone, this.email, this.school, this.position, this.title, this.subjectCategories, this.status, this.workingStatus, this.sex, this.birthday, this.address, this.type, this.description, this.dateStartWorked, this.createdAt, this.createdBy, this.schoolYearId, this.v, this.updatedAt, this.updatedBy, this.classId, this.homeRoomOfClass, this.schoolYear, this.subjectId, this.classes, this.subject, this.fcmTokens, this.classesOfHomeroomTeacher, this.specialty, this.student});

  SubjectTeacher.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    phone = json["phone"];
    email = json["email"];
    school = json["school"];
    position = json["position"];
    title = json["title"];
    subjectCategories = json["subjectCategories"] == null ? null : List<String>.from(json["subjectCategories"]);
    status = json["status"];
    workingStatus = json["workingStatus"];
    sex = json["sex"];
    birthday = json["birthday"];
    address = json["address"];
    type = json["type"];
    description = json["description"];
    dateStartWorked = json["dateStartWorked"];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"];
    schoolYearId = json["schoolYearId"] == null ? null : List<String>.from(json["schoolYearId"]);
    v = json["__v"];
    updatedAt = json["updatedAt"];
    updatedBy = json["updatedBy"];
    classId = json["classId"] == null ? null : List<String>.from(json["classId"]);
    homeRoomOfClass = json["homeRoomOfClass"] ?? [];
    schoolYear = json["schoolYear"] == null ? null : List<String>.from(json["schoolYear"]);
    subjectId = json["subjectId"] == null ? null : List<String>.from(json["subjectId"]);
    classes = json["classes"] == null ? null : List<String>.from(json["classes"]);
    subject = json["subject"] == null ? null : List<String>.from(json["subject"]);
    fcmTokens = json["fcmTokens"] ?? [];
    classesOfHomeroomTeacher = json["classesOfHomeroomTeacher"] ?? [];
    specialty = json["specialty"] ?? [];
    student = json["student"] ?? [];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["phone"] = phone;
    _data["email"] = email;
    _data["school"] = school;
    _data["position"] = position;
    _data["title"] = title;
    if(subjectCategories != null) {
      _data["subjectCategories"] = subjectCategories;
    }
    _data["status"] = status;
    _data["workingStatus"] = workingStatus;
    _data["sex"] = sex;
    _data["birthday"] = birthday;
    _data["address"] = address;
    _data["type"] = type;
    _data["description"] = description;
    _data["dateStartWorked"] = dateStartWorked;
    _data["createdAt"] = createdAt;
    _data["createdBy"] = createdBy;
    if(schoolYearId != null) {
      _data["schoolYearId"] = schoolYearId;
    }
    _data["__v"] = v;
    _data["updatedAt"] = updatedAt;
    _data["updatedBy"] = updatedBy;
    if(classId != null) {
      _data["classId"] = classId;
    }
    if(homeRoomOfClass != null) {
      _data["homeRoomOfClass"] = homeRoomOfClass;
    }
    if(schoolYear != null) {
      _data["schoolYear"] = schoolYear;
    }
    if(subjectId != null) {
      _data["subjectId"] = subjectId;
    }
    if(classes != null) {
      _data["classes"] = classes;
    }
    if(subject != null) {
      _data["subject"] = subject;
    }
    if(fcmTokens != null) {
      _data["fcmTokens"] = fcmTokens;
    }
    if(classesOfHomeroomTeacher != null) {
      _data["classesOfHomeroomTeacher"] = classesOfHomeroomTeacher;
    }
    if(specialty != null) {
      _data["specialty"] = specialty;
    }
    if(student != null) {
      _data["student"] = student;
    }
    return _data;
  }
}

class SubjectCategory {
  String? id;
  int? v;
  String? code;
  int? createdAt;
  String? description;
  String? name;
  String? status;
  String? image;
  int? updatedAt;
  String? updatedBy;
  String? school;

  SubjectCategory({this.id, this.v, this.code, this.createdAt, this.description, this.name, this.status, this.image, this.updatedAt, this.updatedBy, this.school});

  SubjectCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    v = json["__v"];
    code = json["code"];
    createdAt = json["createdAt"];
    description = json["description"];
    name = json["name"];
    status = json["status"];
    updatedAt = json["updatedAt"];
    updatedBy = json["updatedBy"];
    school = json["school"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["__v"] = v;
    _data["code"] = code;
    _data["createdAt"] = createdAt;
    _data["description"] = description;
    _data["name"] = name;
    _data["status"] = status;
    _data["image"] = image;
    _data["updatedAt"] = updatedAt;
    _data["updatedBy"] = updatedBy;
    _data["school"] = school;
    return _data;
  }
}


class DetailSubject {
  String? id;
  SubjectCategory? subjectCategory;
  String? school;
  String? schoolYear;
  String? status;
  String? block;
  String? clazz;
  int? v;
  DetailSubjectTeacher? teacher;

  DetailSubject({this.id, this.subjectCategory, this.school, this.schoolYear, this.status, this.block, this.clazz, this.v, this.teacher});

  DetailSubject.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    subjectCategory = json["subjectCategory"] == null ? null : SubjectCategory.fromJson(json["subjectCategory"]);
    school = json["school"];
    schoolYear = json["schoolYear"];
    status = json["status"];
    block = json["block"];
    clazz = json["clazz"];
    v = json["__v"];
    teacher = json["teacher"] == null ? null : DetailSubjectTeacher.fromJson(json["teacher"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(subjectCategory != null) {
      _data["subjectCategory"] = subjectCategory?.toJson();
    }
    _data["school"] = school;
    _data["schoolYear"] = schoolYear;
    _data["status"] = status;
    _data["block"] = block;
    _data["clazz"] = clazz;
    _data["__v"] = v;
    if(teacher != null) {
      _data["teacher"] = teacher?.toJson();
    }
    return _data;
  }
}

class DetailSubjectTeacher {
  String? id;
  String? fullName;
  int? birthday;
  String? image;

  DetailSubjectTeacher({this.id, this.fullName, this.birthday, this.image});

  DetailSubjectTeacher.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    birthday = json["birthday"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["birthday"] = birthday;
    _data["image"] = image;
    return _data;
  }
}


class ShowNotification {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  List<Items>? items;

  ShowNotification(
      {this.pageIndex,
        this.pageSize,
        this.totalPages,
        this.totalItems,
        this.items});

  ShowNotification.fromJson(Map<String, dynamic> json) {
    pageIndex = json['pageIndex'];
    pageSize = json['pageSize'];
    totalPages = json['totalPages'];
    totalItems = json['totalItems'];
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pageIndex'] = pageIndex;
    data['pageSize'] = pageSize;
    data['totalPages'] = totalPages;
    data['totalItems'] = totalItems;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String? id;
  String? subject;
  String? clazz;
  String? title;
  String? content;
  String? status;
  List<Files>? files;
  int? createdAt;
  int? updatedAt;
  String? createdBy;
  String? updatedBy;
  int? v;

  Items(
      {this.id,
        this.subject,
        this.clazz,
        this.title,
        this.content,
        this.status,
        this.files,
        this.createdAt,
        this.updatedAt,
        this.createdBy,
        this.updatedBy,
        this.v});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    subject = json['subject'];
    clazz = json['clazz'];
    title = json['title'];
    content = json['content'];
    status = json['status'];
    if (json['files'] != null) {
      files = <Files>[];
      json['files'].forEach((v) {
        files!.add(Files.fromJson(v));
      });
    }
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    createdBy = json['createdBy'];
    updatedBy = json['updatedBy'];
    v = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = id;
    data['subject'] = subject;
    data['clazz'] = clazz;
    data['title'] = title;
    data['content'] = content;
    data['status'] = status;
    if (files != null) {
      data['files'] = files!.map((v) => v.toJson()).toList();
    }
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['createdBy'] = createdBy;
    data['updatedBy'] = updatedBy;
    data['__v'] = v;
    return data;
  }
}

class Files {
  String? originalFileName;
  String? link;
  String? tmpFolderUpload;
  String? name;
  String? ext;
  String? size;

  Files(
      {this.originalFileName,
        this.link,
        this.tmpFolderUpload,
        this.name,
        this.ext,
        this.size});

  Files.fromJson(Map<String, dynamic> json) {
    originalFileName = json['originalFileName'];
    link = json['link'];
    tmpFolderUpload = json['tmpFolderUpload'];
    name = json['name'];
    ext = json['ext'];
    size = json['size'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['originalFileName'] = originalFileName;
    data['link'] = link;
    data['tmpFolderUpload'] = tmpFolderUpload;
    data['name'] = name;
    data['ext'] = ext;
    data['size'] = size;
    return data;
  }
}


class DetailNotification {
  String? id;
  String? title;
  String? content;
  List<Files>? files;
  int? createdAt;
  int? updatedAt;
  String? createdBy;
  String? updatedBy;
  int? v;

  String? status;

  DetailNotification({this.id, this.title, this.content, this.files, this.createdAt, this.updatedAt, this.createdBy, this.updatedBy, this.v, this.status});

  DetailNotification.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    title = json["title"];
    content = json["content"];
    files = json["files"] == null ? null : (json["files"] as List).map((e) => Files.fromJson(e)).toList();
    createdAt = json["createdAt"];
    updatedAt = json["updatedAt"];
    createdBy = json["createdBy"];
    updatedBy = json["updatedBy"];
    v = json["__v"];
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["title"] = title;
    _data["content"] = content;
    if(files != null) {
      _data["files"] = files?.map((e) => e.toJson()).toList();
    }
    _data["createdAt"] = createdAt;
    _data["updatedAt"] = updatedAt;
    _data["createdBy"] = createdBy;
    _data["updatedBy"] = updatedBy;
    _data["__v"] = v;

    _data["status"] = status;
    return _data;
  }
}

