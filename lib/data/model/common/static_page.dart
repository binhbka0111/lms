
class ItemsStaticPage {
  String? id;
  String? title;
  String? content;
  String? description;
  String? type;
  int? v;
  String? status;
  String? url;
  int? updatedAt;
  String? updatedBy;

  ItemsStaticPage({this.id, this.title, this.content, this.description, this.type, this.v, this.status, this.url, this.updatedAt, this.updatedBy});

  ItemsStaticPage.fromJson(dynamic json) {
    id = json["_id"];
    title = json["title"];
    content = json["content"];
    description = json["description"];
    type = json["type"];
    v = json["__v"];
    status = json["status"];
    url = json["url"];
    updatedAt = json["updatedAt"];
    updatedBy = json["updatedBy"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["title"] = title;
    _data["content"] = content;
    _data["description"] = description;
    _data["type"] = type;
    _data["__v"] = v;
    _data["status"] = status;
    _data["url"] = url;
    _data["updatedAt"] = updatedAt;
    _data["updatedBy"] = updatedBy;
    return _data;
  }
}