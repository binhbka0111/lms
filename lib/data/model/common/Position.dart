class Position {
  Position({
    this.name,
    this.key,
    this.type,
    this.v,
    this.id,
  });

  Position.fromJson(dynamic json) {
    if (json["name"] != null||json["name"]!="") {
      name = json["name"];
    }
    if (json["id"] != null||json["id"]!="") {
      id = json["id"];
    }
    if (json["key"] != null||json["key"]!="") {
      key = json["key"];
    }
    if (json["__v"] != null||json["__v"]!="") {
      v = json["__v"];
    }
    if (json["type"] != null||json["type"]!="") {
      type = json["type"];
    }
  }

  String? name;
  String? type;
  String? id;
  int? v;
  String? key;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    map['_key'] = key;
    map['_id'] = id;
    map['__v'] = v;
    map['type'] = type;
    return map;
  }
}
