import 'contacts.dart';


class LeavingApplication {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  List<ItemsLeavingApplication>? items;

  LeavingApplication({this.pageIndex, this.pageSize, this.totalPages, this.totalItems, this.items});

  LeavingApplication.fromJson(dynamic json) {
    pageIndex = json["pageIndex"];
    pageSize = json["pageSize"];
    totalPages = json["totalPages"];
    totalItems = json["totalItems"];
    if (json['items'] != null) {
      items = [];
      json['items'].forEach((v) {
        items?.add(ItemsLeavingApplication?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["pageIndex"] = pageIndex;
    _data["pageSize"] = pageSize;
    _data["totalPages"] = totalPages;
    _data["totalItems"] = totalItems;
    if(items != null) {
      _data["items"] = items?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class ItemsLeavingApplication {
  String? id;
  Teacher? teacher;
  StudentbyLeavingApplication? student;
  ClazzLeavingApplication? clazz;
  int? fromDate;
  int? toDate;
  String? reason;
  String? note;
  String? assure;
  int? createdAt;
  CreatedBy? createdBy;
  int? v;
  String? status;
  String? feedback;
  Parent? parent;
  List<LeaveDetail>? leaveDetail;

  ItemsLeavingApplication({this.id, this.teacher, this.student, this.clazz, this.fromDate, this.toDate, this.reason, this.note, this.assure, this.createdAt, this.createdBy, this.v, this.status,this.leaveDetail});

  ItemsLeavingApplication.fromJson(dynamic json) {
    id = json["_id"];
    teacher = json["teacher"] == null ? null : Teacher.fromJson(json["teacher"]);
    student = json["student"] == null ? null : StudentbyLeavingApplication.fromJson(json["student"]);
    clazz = json["clazz"] == null ? null : ClazzLeavingApplication.fromJson(json["clazz"]);
    fromDate = json["fromDate"];
    toDate = json["toDate"];
    reason = json["reason"];
    note = json["note"];
    assure = json["assure"];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"] == null ? null : CreatedBy.fromJson(json["teacher"]);
    v = json["__v"];
    status = json["status"]??"";
    feedback = json["feedback"]??"";
    parent = json["parent"] == null ? null : Parent.fromJson(json["parent"]);
    if (json['leaveDetail'] != []) {
      leaveDetail = [];
      json['leaveDetail'].forEach((v) {
        leaveDetail?.add(LeaveDetail?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(teacher != null) {
      _data["teacher"] = teacher?.toJson();
    }
    if(student != null) {
      _data["student"] = student?.toJson();
    }
    if(clazz != null) {
      _data["clazz"] = clazz?.toJson();
    }
    _data["fromDate"] = fromDate;
    _data["toDate"] = toDate;
    _data["reason"] = reason;
    _data["note"] = note;
    _data["assure"] = assure;
    _data["createdAt"] = createdAt;
    _data["createdBy"] = createdBy;
    _data["__v"] = v;
    _data["status"] = status;
    _data["feedback"] = feedback;
    _data["parent"] = parent;
    final leaveDetail = this.leaveDetail;
    if (leaveDetail != null) {
      _data['leaveDetail'] = leaveDetail.map((v) => v.toJson()).toList();
    }
    return _data;
  }
}

class StudentbyLeavingApplication{
  String? id;
  String? fullName;
  String? image;

  StudentbyLeavingApplication({this.id, this.fullName,this.image});

  StudentbyLeavingApplication.fromJson(dynamic json) {
    id = json["_id"];
    fullName = json["fullName"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map["_id"] = id;
    map["fullName"] = fullName;
    map["image"] = image;
    return map;
  }
}


class ClazzLeavingApplication {
  String? id;
  ClassCategoryLeavingApplication? classCategory;

  ClazzLeavingApplication({this.id, this.classCategory});

  ClazzLeavingApplication.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    classCategory = json["classCategory"] == null ? null : ClassCategoryLeavingApplication.fromJson(json["classCategory"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(classCategory != null) {
      _data["classCategory"] = classCategory?.toJson();
    }
    return _data;
  }
}

class ClassCategoryLeavingApplication {
  String? id;
  String? name;

  ClassCategoryLeavingApplication({this.id, this.name});

  ClassCategoryLeavingApplication.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["name"] = name;
    return _data;
  }
}


class CreatedBy {
  String? id;
  String? name;

  CreatedBy({this.id, this.name});

  CreatedBy.fromJson(dynamic json) {
    id = json["_id"];
    name = json["fullName"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = name;
    return _data;
  }
}



class LeaveDetail {
  int? date;
  String? sessionDay;

  LeaveDetail({this.date, this.sessionDay});

  LeaveDetail.fromJson(dynamic json) {
    date = json["date"];
    sessionDay = json["sessionDay"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["date"] = date;
    if(sessionDay != null) {
      _data["sessionDay"] = sessionDay;
    }
    return _data;
  }
}