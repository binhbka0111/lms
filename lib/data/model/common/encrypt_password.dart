class cryptoSecret {
  String? secret;

  cryptoSecret({this.secret});

  cryptoSecret.fromJson(dynamic json) {
    secret = json["secret"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["secret"] = secret;
    return _data;
  }
}