// To parse this JSON data, do
//
//     final eventNewsCategory = eventNewsCategoryFromJson(jsonString);

import 'dart:convert';

EventNewsCategory eventNewsCategoryFromJson(String str) => EventNewsCategory.fromJson(json.decode(str));

String eventNewsCategoryToJson(EventNewsCategory data) => json.encode(data.toJson());

class EventNewsCategory {
  String? id;
  String? code;
  String? name;
  String? school;
  int? createdAt;
  String? createdBy;
  int? v;
  bool? isDisplay;
  int? updatedAt;
  String? updatedBy;

  EventNewsCategory({
    this.id,
    this.code,
    this.name,
    this.school,
    this.createdAt,
    this.createdBy,
    this.v,
    this.isDisplay,
    this.updatedAt,
    this.updatedBy,
  });

  factory EventNewsCategory.fromJson(Map<String, dynamic> json) => EventNewsCategory(
    id: json["_id"],
    code: json["code"],
    name: json["name"],
    school: json["school"],
    createdAt: json["createdAt"],
    createdBy: json["createdBy"],
    v: json["__v"],
    isDisplay: json["isDisplay"],
    updatedAt: json["updatedAt"],
    updatedBy: json["updatedBy"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "code": code,
    "name": name,
    "school": school,
    "createdAt": createdAt,
    "createdBy": createdBy,
    "__v": v,
    "isDisplay": isDisplay,
    "updatedAt": updatedAt,
    "updatedBy": updatedBy,
  };
}
