class UserGroupByApp {
  Page? page;
  List<String>? features;

  UserGroupByApp({this.page, this.features});

  UserGroupByApp.fromJson(Map<String, dynamic> json) {
    page = json["page"] == null ? null : Page.fromJson(json["page"]);
    features = json["features"] == null ? null : List<String>.from(json["features"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if(page != null) {
      _data["page"] = page?.toJson();
    }
    if(features != null) {
      _data["features"] = features;
    }
    return _data;
  }
}

class Page {
  String? id;
  String? code;
  String? name;
  dynamic parentId;
  List<UserGroupByApp>? children;

  Page({this.id, this.code, this.name, this.parentId, this.children});

  Page.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
    parentId = json["parentId"];
    if (json['children'] != null) {
      children = [];
      json['children'].forEach((v) {
        children?.add(UserGroupByApp?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    _data["parentId"] = parentId;
    if(children != null) {
      _data["children"] = children;
    }
    return _data;
  }
}