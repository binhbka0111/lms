import '../res/class/block.dart';

class   Schedule {
  String? id;
  Subject? subject;
  int? startTime;
  int? endTime;
  int? v;

  Schedule({this.id, this.subject, this.startTime, this.endTime, this.v});

  Schedule.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    subject = json["subject"] == null ? null : Subject.fromJson(json["subject"]);
    startTime = json["startTime"];
    endTime = json["endTime"];
    v = json["__v"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(subject != null) {
      _data["subject"] = subject?.toJson();
    }
    _data["startTime"] = startTime;
    _data["endTime"] = endTime;
    _data["__v"] = v;
    return _data;
  }
}

class Subject {
  String? id;
  SubjectCategory? subjectCategory;
  String? schoolYearId;
  Clazz? clazz;
  int? v;
  Teacher? teacher;

  Subject({this.id, this.subjectCategory, this.schoolYearId, this.clazz, this.v, this.teacher});

  Subject.fromJson( dynamic json) {
    id = json["_id"];
    subjectCategory = json["subjectCategory"] == null ? null : SubjectCategory.fromJson(json["subjectCategory"]);
    schoolYearId = json["schoolYearId"];
    clazz = json["clazz"] == null ? null : Clazz.fromJson(json["clazz"]);
    v = json["__v"];
    teacher = json["teacher"] == null ? null : Teacher.fromJson(json["teacher"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(subjectCategory != null) {
      _data["subjectCategory"] = subjectCategory?.toJson();
    }
    _data["schoolYearId"] = schoolYearId;
    if(clazz != null) {
      _data["clazzId"] = clazz?.toJson();
    }
    _data["__v"] = v;
    if(teacher != null) {
      _data["teacher"] = teacher?.toJson();
    }
    return _data;
  }
}

class Teacher {
  String? id;
  String? fullName;

  Teacher({this.id, this.fullName});

  Teacher.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    return _data;
  }
}

class Clazz {
  String? id;
  ClassCategory? classCategory;
  List<Students>? students;

  Clazz({this.id, this.classCategory, this.students});

  Clazz.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    classCategory = json["classCategory"] == null ? null : ClassCategory.fromJson(json["classCategory"]);
    students = json["students"] == null ? null : (json["students"] as List).map((e) => Students.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(classCategory != null) {
      _data["classCategory"] = classCategory?.toJson();
    }
    if(students != null) {
      _data["students"] = students?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class Students {
  String? id;
  String? fullName;
  String? image;
  dynamic birthday;

  Students({this.id, this.fullName, this.image});

  Students.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    image = json["image"];
    birthday = json["birthday"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["image"] = image;
    _data["birthday"] = birthday;
    return _data;
  }
}



class SubjectCategory {
  String? id;
  String? name;

  SubjectCategory({this.id, this.name});

  SubjectCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["name"] = name;
    return _data;
  }
}