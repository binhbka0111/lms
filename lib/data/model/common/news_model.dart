class Item {
  String? id;
  String? image;
  String? title;
  String? excerpt;
  String? content;
  NewsCategory? newsCategory;
  School? school;
  int? createdAt;
  CreatedBy? createdBy;
  int? v;
  bool? isActive;
  int? updatedAt;
  String? updatedBy;
  String? url;

  Item({
    this.id,
    this.image,
    this.title,
    this.excerpt,
    this.content,
    this.newsCategory,
    this.school,
    this.createdAt,
    this.createdBy,
    this.v,
    this.isActive,
    this.updatedAt,
    this.updatedBy,
    this.url,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
    id: json["_id"],
    image: json["image"],
    title: json["title"],
    excerpt: json["excerpt"],
    content: json["content"],
    newsCategory: json["newsCategory"] == null ? null : NewsCategory.fromJson(json["newsCategory"]),
    school: json["school"] == null ? null : School.fromJson(json["school"]),
    createdAt: json["createdAt"],
    createdBy: json["createdBy"] == null ? null : CreatedBy.fromJson(json["createdBy"]),
    v: json["__v"],
    isActive: json["isActive"],
    updatedAt: json["updatedAt"],
    updatedBy: json["updatedBy"],
    url: json["url"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "image": image,
    "title": title,
    "excerpt": excerpt,
    "content": content,
    "newsCategory": newsCategory?.toJson(),
    "school": school?.toJson(),
    "createdAt": createdAt,
    "createdBy": createdBy?.toJson(),
    "__v": v,
    "isActive": isActive,
    "updatedAt": updatedAt,
    "updatedBy": updatedBy,
    "url": url,
  };
}

class CreatedBy {
  String? id;
  String? fullName;

  CreatedBy({
    this.id,
    this.fullName,
  });

  factory CreatedBy.fromJson(Map<String, dynamic> json) => CreatedBy(
    id: json["_id"],
    fullName: json["fullName"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "fullName": fullName,
  };
}

class NewsCategory {
  String? id;
  String? code;
  String? name;
  String? school;
  bool? isDisplay;
  int? createdAt;
  String? createdBy;
  int? v;

  NewsCategory({
    this.id,
    this.code,
    this.name,
    this.school,
    this.isDisplay,
    this.createdAt,
    this.createdBy,
    this.v,
  });

  factory NewsCategory.fromJson(Map<String, dynamic> json) => NewsCategory(
    id: json["_id"],
    code: json["code"],
    name: json["name"],
    school: json["school"],
    isDisplay: json["isDisplay"],
    createdAt: json["createdAt"],
    createdBy: json["createdBy"],
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "code": code,
    "name": name,
    "school": school,
    "isDisplay": isDisplay,
    "createdAt": createdAt,
    "createdBy": createdBy,
    "__v": v,
  };
}

class School {
  String? id;
  String? name;

  School({
    this.id,
    this.name,
  });

  factory School.fromJson(Map<String, dynamic> json) => School(
    id: json["_id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "name": name,
  };
}
