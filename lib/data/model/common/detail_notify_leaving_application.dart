
import '../res/class/School.dart';
import 'diligence.dart';

class DetailNotifyLeavingApplication {
  String? id;
  TeacherNotifyLeavingApplication? teacher;
  StudentNotifiLeavingApplication? student;
  ParentNotifiLeavingApplication? parent;
  Clazz? clazz;
  int? fromDate;
  int? toDate;
  String? reason;
  String? note;
  String? assure;
  String? status;
  String? schoolYear;
  int? createdAt;
  CreatedBy? createdBy;
  int? v;
  String? html;
  String? feedback;

  DetailNotifyLeavingApplication({this.id, this.teacher, this.student, this.parent, this.clazz, this.fromDate, this.toDate, this.reason, this.note, this.assure, this.status, this.schoolYear, this.createdAt, this.createdBy, this.v, this.html,this.feedback});

  DetailNotifyLeavingApplication.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    teacher = json["teacher"] == null ? null : TeacherNotifyLeavingApplication.fromJson(json["teacher"]);
    student = json["student"] == null ? null : StudentNotifiLeavingApplication.fromJson(json["student"]);
    parent = json["parent"] == null ? null : ParentNotifiLeavingApplication.fromJson(json["parent"]);
    clazz = json["clazz"] == null ? null : Clazz.fromJson(json["clazz"]);
    fromDate = json["fromDate"];
    toDate = json["toDate"];
    reason = json["reason"];
    note = json["note"];
    assure = json["assure"];
    status = json["status"];
    schoolYear = json["schoolYear"];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"] == null ? null : CreatedBy.fromJson(json["createdBy"]);
    v = json["__v"];
    html = json["html"];
    feedback = json["feedback"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(teacher != null) {
      _data["teacher"] = teacher?.toJson();
    }
    if(student != null) {
      _data["student"] = student?.toJson();
    }
    if(parent != null) {
      _data["parent"] = parent?.toJson();
    }
    if(clazz != null) {
      _data["clazz"] = clazz?.toJson();
    }
    _data["fromDate"] = fromDate;
    _data["toDate"] = toDate;
    _data["reason"] = reason;
    _data["note"] = note;
    _data["assure"] = assure;
    _data["status"] = status;
    _data["schoolYear"] = schoolYear;
    _data["createdAt"] = createdAt;
    if(createdBy != null) {
      _data["createdBy"] = createdBy?.toJson();
    }
    _data["__v"] = v;
    _data["html"] = html;
    _data["feedback"] = feedback;
    return _data;
  }
}

class CreatedBy {
  String? id;
  String? fullName;
  String? address;

  CreatedBy({this.id, this.fullName, this.address});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    address = json["address"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["address"] = address;
    return _data;
  }
}


class ParentNotifiLeavingApplication {
  String? id;
  String? fullName;
  String? phone;
  String? address;

  ParentNotifiLeavingApplication({this.id, this.fullName, this.phone, this.address});

  ParentNotifiLeavingApplication.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    phone = json["phone"];
    address = json["address"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["phone"] = phone;
    _data["address"] = address;
    return _data;
  }
}

class StudentNotifiLeavingApplication {
  String? id;
  String? fullName;
  String? image;
  String? address;

  StudentNotifiLeavingApplication({this.id, this.fullName, this.image, this.address});

  StudentNotifiLeavingApplication.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    image = json["image"];
    address = json["address"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["image"] = image;
    _data["address"] = address;
    return _data;
  }
}

class TeacherNotifyLeavingApplication {
  String? id;
  String? fullName;
  SchoolData? school;

  TeacherNotifyLeavingApplication({this.id, this.fullName, this.school});

  TeacherNotifyLeavingApplication.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    school = json["school"] == null ? null : SchoolData.fromJson(json["school"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    if(school != null) {
      _data["school"] = school?.toJson();
    }
    return _data;
  }
}

