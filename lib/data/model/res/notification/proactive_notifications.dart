
class ProactiveNotify {
  List<Files>? files;
  String? title;
  String? body;
  String? typeNotify;
  String? schoolYear;
  bool? isDelete;
  int? createdAt;
  CreatedBy? createdBy;
  Clazz? clazz;



  ProactiveNotify({this.clazz, this.files, this.title, this.body, this.typeNotify, this.schoolYear, this.isDelete, this.createdAt, this.createdBy});

  ProactiveNotify.fromJson(Map<String, dynamic> json) {
    files = json["files"] == null ? null : (json["files"] as List).map((e) => Files.fromJson(e)).toList();
    title = json["title"];
    body = json["body"];
    typeNotify = json["typeNotify"];
    schoolYear = json["schoolYear"];
    isDelete = json["isDelete"];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"] == null ? null : CreatedBy.fromJson(json["createdBy"]);
    clazz = json["clazz"] == null ? null : Clazz.fromJson(json["clazz"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};

    if(files != null) {
      _data["files"] = files?.map((e) => e.toJson()).toList();
    }
    _data["title"] = title;
    _data["body"] = body;
    _data["typeNotify"] = typeNotify;
    _data["schoolYear"] = schoolYear;
    _data["isDelete"] = isDelete;
    _data["createdAt"] = createdAt;
    if(createdBy != null) {
      _data["createdBy"] = createdBy?.toJson();
    }
    return _data;
  }
}

class CreatedBy {
  String? id;
  String? fullName;
  String? image;

  CreatedBy({this.id, this.fullName, this.image});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["image"] = image;
    return _data;
  }
}

class Files {
  String? originalFileName;
  String? link;
  String? tmpFolderUpload;
  String? name;
  String? ext;
  String? size;

  Files({this.originalFileName, this.link, this.tmpFolderUpload, this.name, this.ext, this.size});

  Files.fromJson(Map<String, dynamic> json) {
    originalFileName = json["originalFileName"];
    link = json["link"];
    tmpFolderUpload = json["tmpFolderUpload"];
    name = json["name"];
    ext = json["ext"];
    size = json["size"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["originalFileName"] = originalFileName;
    _data["link"] = link;
    _data["tmpFolderUpload"] = tmpFolderUpload;
    _data["name"] = name;
    _data["ext"] = ext;
    _data["size"] = size;
    return _data;
  }
}

class Clazz {
  String? id;
  ClassCategory? classCategory;
  String? school;
  String? schoolYear;
  String? block;
  List<String>? subjects;
  String? homeroomTeacher;
  List<String>? students;
  int? ordinal;
  SchoolLevel? schoolLevel;
  int? v;
  String? monitorStudent;

  Clazz({this.id, this.classCategory, this.school, this.schoolYear, this.block, this.subjects, this.homeroomTeacher, this.students, this.ordinal, this.schoolLevel, this.v, this.monitorStudent});

  Clazz.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    classCategory = json["classCategory"] == null ? null : ClassCategory.fromJson(json["classCategory"]);
    school = json["school"];
    schoolYear = json["schoolYear"];
    block = json["block"];
    subjects = json["subjects"] == null ? null : List<String>.from(json["subjects"]);
    homeroomTeacher = json["homeroomTeacher"];
    students = json["students"] == null ? null : List<String>.from(json["students"]);
    ordinal = json["ordinal"];
    schoolLevel = json["schoolLevel"] == null ? null : SchoolLevel.fromJson(json["schoolLevel"]);
    v = json["__v"];
    monitorStudent = json["monitorStudent"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(classCategory != null) {
      _data["classCategory"] = classCategory?.toJson();
    }
    _data["school"] = school;
    _data["schoolYear"] = schoolYear;
    _data["block"] = block;
    if(subjects != null) {
      _data["subjects"] = subjects;
    }
    _data["homeroomTeacher"] = homeroomTeacher;
    if(students != null) {
      _data["students"] = students;
    }
    _data["ordinal"] = ordinal;
    if(schoolLevel != null) {
      _data["schoolLevel"] = schoolLevel?.toJson();
    }
    _data["__v"] = v;
    _data["monitorStudent"] = monitorStudent;
    return _data;
  }
}

class SchoolLevel {
  String? name;
  int? ordinal;
  String? description;
  String? id;

  SchoolLevel({this.name, this.ordinal, this.description, this.id});

  SchoolLevel.fromJson(Map<String, dynamic> json) {
    name = json["name"];
    ordinal = json["ordinal"];
    description = json["description"];
    id = json["_id"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["name"] = name;
    _data["ordinal"] = ordinal;
    _data["description"] = description;
    _data["_id"] = id;
    return _data;
  }
}

class ClassCategory {
  String? id;
  String? name;

  ClassCategory({this.id, this.name});

  ClassCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["name"] = name;
    return _data;
  }
}

class Users {
  String? id;
  String? fullName;
  String? type;
  String? phone;
  String? image;
  List<dynamic>? students;
  List<dynamic>? parents;
  List<dynamic>? subjects;
  String? positionName;

  Users({this.id, this.fullName, this.type, this.phone, this.image, this.students, this.parents, this.subjects, this.positionName});

  Users.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    type = json["type"];
    phone = json["phone"];
    image = json["image"];
    students = json["students"] ?? [];
    parents = json["parents"] ?? [];
    subjects = json["subjects"] ?? [];
    positionName = json["positionName"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["type"] = type;
    _data["phone"] = phone;
    _data["image"] = image;
    if(students != null) {
      _data["students"] = students;
    }
    if(parents != null) {
      _data["parents"] = parents;
    }
    if(subjects != null) {
      _data["subjects"] = subjects;
    }
    _data["positionName"] = positionName;
    return _data;
  }
}