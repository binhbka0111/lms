
import '../../common/learning_managerment.dart';
import '../../common/subject.dart';

class Notify {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  ItemsNotification? items;

  Notify({this.pageIndex, this.pageSize, this.totalPages, this.totalItems, this.items});

  Notify.fromJson(Map<String, dynamic> json) {
    pageIndex = json["pageIndex"];
    pageSize = json["pageSize"];
    totalPages = json["totalPages"];
    totalItems = json["totalItems"];
    items = json["items"] == null ? null : ItemsNotification.fromJson(json["items"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["pageIndex"] = pageIndex;
    data["pageSize"] = pageSize;
    data["totalPages"] = totalPages;
    data["totalItems"] = totalItems;
    if(items != null) {
      data["items"] = items?.toJson();
    }
    return data;
  }
}

class ItemsNotification {
  List<NotifyList>? todayNotifyList;
  List<NotifyList>? beforeNotifyList;

  ItemsNotification({this.todayNotifyList, this.beforeNotifyList});

  ItemsNotification.fromJson(Map<String, dynamic> json) {
    if (json['todayNotifyList'] != null) {
      todayNotifyList = [];
      json['todayNotifyList'].forEach((v) {
        todayNotifyList?.add(NotifyList?.fromJson(v));
      });
    }
    if (json['beforeNotifyList'] != null) {
      beforeNotifyList = [];
      json['beforeNotifyList'].forEach((v) {
        beforeNotifyList?.add(NotifyList?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if(todayNotifyList != null) {
      data["todayNotifyList"] = todayNotifyList?.map((e) => e.toJson()).toList();
    }
    if(beforeNotifyList != null) {
      data["beforeNotifyList"] = beforeNotifyList?.map((e) => e.toJson()).toList();
    }
    return data;
  }
}

class NotifyList {
  String? id;
  String? userId;
  String? tranId;
  String? title;
  String? body;
  String? type;
  String? typeNotify;
  String? status;
  String? schoolYear;
  int? createdAt;
  CreatedBy? createdBy;
  int? v;
  DetailNotify? detail;

  NotifyList({this.id, this.userId, this.tranId, this.title, this.body, this.type, this.status, this.schoolYear,
    this.createdAt, this.createdBy, this.v,this.detail,this.typeNotify });

  NotifyList.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    userId = json["userId"];
    tranId = json["tranId"];
    title = json["title"];
    body = json["body"];
    type = json["type"];
    typeNotify = json["typeNotify"];
    status = json["status"];
    schoolYear = json["schoolYear"];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"] == null ? null : CreatedBy.fromJson(json["createdBy"]);
    detail = json["detail"] == null ? null : DetailNotify.fromJson(json["detail"]);
    v = json["__v"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["_id"] = id;
    data["userId"] = userId;
    data["tranId"] = tranId;
    data["title"] = title;
    data["body"] = body;
    data["type"] = type;
    data["status"] = status;
    data["schoolYear"] = schoolYear;
    data["createdAt"] = createdAt;
    if(createdBy != null) {
      data["createdBy"] = createdBy?.toJson();
    }
    if(detail != null) {
      data["detail"] = detail?.toJson();
    }
    data["__v"] = v;
    return data;
  }
}

class CreatedBy {
  String? id;
  String? fullName;
  String? image;

  CreatedBy({this.id, this.fullName, this.image});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["_id"] = id;
    data["fullName"] = fullName;
    data["image"] = image;
    return data;
  }
}


class TotalNotifiNotSeen {
  int? total;

  TotalNotifiNotSeen({this.total});

  TotalNotifiNotSeen.fromJson(Map<String, dynamic> json) {
    total = json["total"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["total"] = total;
    return data;
  }
}


class DetailNotify {
  String? tranId;
  dynamic fromDate;
  dynamic toDate;
  dynamic status;
  String? classId;
  String? subjectId;
  String? type;
  dynamic startTime;
  ItemsExercise? itemExercise;
  ItemsExam? itemExam;
  Student ? student;
  SubjectRes? subject;

  DetailNotify({this.tranId, this.startTime, this.fromDate,
    this.toDate, this.status, this.classId, this.subjectId,
    this.type,this.itemExercise, this.itemExam, this.student,
    this.subject});

  DetailNotify.fromJson(Map<String, dynamic> json) {
    tranId = json["tranId"];
    startTime = json["startTime"];
    fromDate = json["fromDate"];
    toDate = json["toDate"];
    status = json["status"];
    classId = json["classId"];
    subjectId = json["subjectId"];
    type = json["type"];
    itemExercise = json["item"] == null ? null : ItemsExercise.fromJson(json["item"]);
    itemExam = json["item"] == null ? null : ItemsExam.fromJson(json["item"]);
    student = json["studentId"] == null ? null : Student.fromJson(json["studentId"]);
    subject = json["subject"] == null ? null : SubjectRes.fromJson(json["subject"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["tranId"] = tranId;
    data["startTime"] = startTime;
    data["fromDate"] = fromDate;
    data["toDate"] = toDate;
    data["status"] = status;
    data["classId"] = classId;
    data["subjectId"] = subjectId;
    data["type"] = type;
    return data;
  }
}