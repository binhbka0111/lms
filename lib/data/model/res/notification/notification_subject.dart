import '../file/response_file.dart';

class NotificationSubject {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  List<ItemsNotificationSubject>? items;

  NotificationSubject({this.pageIndex, this.pageSize, this.totalPages, this.totalItems, this.items});

  NotificationSubject.fromJson(Map<String, dynamic> json) {
    pageIndex = json["pageIndex"];
    pageSize = json["pageSize"];
    totalPages = json["totalPages"];
    totalItems = json["totalItems"];
    items = json["items"] == null ? null : (json["items"] as List).map((e) => ItemsNotificationSubject.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["pageIndex"] = pageIndex;
    _data["pageSize"] = pageSize;
    _data["totalPages"] = totalPages;
    _data["totalItems"] = totalItems;
    if(items != null) {
      _data["items"] = items?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class ItemsNotificationSubject {
  String? id;
  String? subject;
  String? clazz;
  String? title;
  String? content;
  String? status;
  List<ResponseFileUpload>? files;
  int? createdAt;
  int? updatedAt;
  String? createdBy;
  String? updatedBy;
  int? v;

  ItemsNotificationSubject({this.id, this.subject, this.clazz, this.title, this.content, this.status, this.files, this.createdAt, this.updatedAt, this.createdBy, this.updatedBy, this.v});

  ItemsNotificationSubject.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    subject = json["subject"];
    clazz = json["clazz"];
    title = json["title"];
    content = json["content"];
    status = json["status"];
    files = json["files"] == null ? null : (json["files"] as List).map((e) => ResponseFileUpload.fromJson(e)).toList();
    createdAt = json["createdAt"];
    updatedAt = json["updatedAt"];
    createdBy = json["createdBy"];
    updatedBy = json["updatedBy"];
    v = json["__v"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["subject"] = subject;
    _data["clazz"] = clazz;
    _data["title"] = title;
    _data["content"] = content;
    _data["status"] = status;
    if(files != null) {
      _data["files"] = files?.map((e) => e.toJson()).toList();
    }
    _data["createdAt"] = createdAt;
    _data["updatedAt"] = updatedAt;
    _data["createdBy"] = createdBy;
    _data["updatedBy"] = updatedBy;
    _data["__v"] = v;
    return _data;
  }
}