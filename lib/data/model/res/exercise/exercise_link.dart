
class ExerciseLink {
  String? title;
  int? scoreFactor;
  int? deadline;
  String? description;
  String? link;
  String? typeExercise;
  String? subjectId;
  String? teacherId;
  String? classId;

  ExerciseLink({this.title, this.scoreFactor, this.deadline, this.description, this.link, this.typeExercise, this.subjectId, this.teacherId, this.classId});

  ExerciseLink.fromJson(Map<String, dynamic> json) {
    title = json["title"];
    scoreFactor = json["scoreFactor"];
    deadline = json["deadline"];
    description = json["description"];
    link = json["link"];
    typeExercise = json["typeExercise"];
    subjectId = json["subjectId"];
    teacherId = json["teacherId"];
    classId = json["classId"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["title"] = title;
    _data["scoreFactor"] = scoreFactor;
    _data["deadline"] = deadline;
    _data["description"] = description;
    _data["link"] = link;
    _data["typeExercise"] = typeExercise;
    _data["subjectId"] = subjectId;
    _data["teacherId"] = teacherId;
    _data["classId"] = classId;
    return _data;
  }
}