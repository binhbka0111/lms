
class TeacherCommentAnswer {
  String? questionAnswerId;
  double? point;
  String? teacherComment;

  TeacherCommentAnswer({this.questionAnswerId, this.point, this.teacherComment});

  TeacherCommentAnswer.fromJson(Map<String, dynamic> json) {
    questionAnswerId = json["questionAnswerId"];
    point = json["point"];
    teacherComment = json["teacherComment"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["questionAnswerId"] = questionAnswerId;
    _data["point"] = point;
    _data["teacherComment"] = teacherComment;
    return _data;
  }
}