
import '../../common/learning_managerment.dart';

class ListStudentSubmitted {
  List<StudentSubmitted>? studentSubmitted;
  List<StudentUnSubmitted>? studentUnSubmitted;

  ListStudentSubmitted({this.studentSubmitted, this.studentUnSubmitted});

  ListStudentSubmitted.fromJson(Map<String, dynamic> json) {
    studentSubmitted = json["studentSubmitted"] == null ? null : (json["studentSubmitted"] as List).map((e) => StudentSubmitted.fromJson(e)).toList();
    studentUnSubmitted = json["studentUnSubmitted"] == null ? null : (json["studentUnSubmitted"] as List).map((e) => StudentUnSubmitted.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if(studentSubmitted != null) {
      _data["studentSubmitted"] = studentSubmitted?.map((e) => e.toJson()).toList();
    }
    if(studentUnSubmitted != null) {
      _data["studentUnSubmitted"] = studentUnSubmitted?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class StudentUnSubmitted {
  String? id;
  String? fullName;
  int? birthday;
  String? image;
  String? statusExerciseByStudent;

  StudentUnSubmitted({this.id, this.fullName, this.birthday, this.image, this.statusExerciseByStudent});

  StudentUnSubmitted.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    birthday = json["birthday"];
    image = json["image"];
    statusExerciseByStudent = json["statusExerciseByStudent"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["birthday"] = birthday;
    _data["image"] = image;
    _data["statusExerciseByStudent"] = statusExerciseByStudent;
    return _data;
  }
}

class StudentSubmitted {
  String? id;
  Student? student;
  dynamic scoreOfExercise;
  dynamic scoreOfExam;
  String? statusExerciseByStudent;
  String? isGrade;
  String? isPublicScore;
  int? createdAt;
  dynamic score;

  StudentSubmitted({this.id, this.student, this.scoreOfExercise, this.statusExerciseByStudent, this.isGrade, this.isPublicScore, this.createdAt, this.score});

  StudentSubmitted.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    student = json["student"] == null ? null : Student.fromJson(json["student"]);
    scoreOfExercise = json["scoreOfExercise"];
    statusExerciseByStudent = json["statusExerciseByStudent"];
    isGrade = json["isGrade"];
    isPublicScore = json["isPublicScore"];
    createdAt = json["createdAt"];
    score = json["score"];
    scoreOfExercise = json["scoreOfExercise"]??0;
    scoreOfExam = json["scoreOfExam"]??0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(student != null) {
      _data["student"] = student?.toJson();
    }
    _data["statusExerciseByStudent"] = statusExerciseByStudent;
    _data["isGrade"] = isGrade;
    _data["isPublicScore"] = isPublicScore;
    _data["createdAt"] = createdAt;
    _data["score"] = score;
    return _data;
  }
}
