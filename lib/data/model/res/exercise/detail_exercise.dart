
import '../../common/learning_managerment.dart';
import '../file/response_file.dart';
import 'exercise.dart';

class DetailExercise {
  int? index;
  String? id;
  String? title;
  Teacher? teacher;
  Subject? subject;
  Clazz? clazz;
  List<Questions>? questions;
  String? typeExercise;
  int? deadline;
  String? isPublic;
  String? status;
  String? description;
  List<ResponseFileUpload>? files;
  int? createdAt;
  int? updatedAt;
  String? createdBy;
  String? updatedBy;
  int? v;
  num? scoreOfExercise;
  DetailExercise({this.id,this.index, this.title, this.teacher, this.subject,this.scoreOfExercise, this.clazz, this.questions, this.typeExercise, this.deadline, this.isPublic, this.status, this.description, this.files, this.createdAt, this.updatedAt, this.createdBy, this.updatedBy, this.v});

  DetailExercise.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    title = json["title"];
    teacher = json["teacher"] == null ? null : Teacher.fromJson(json["teacher"]);
    subject = json["subject"] == null ? null : Subject.fromJson(json["subject"]);
    clazz = json["clazz"] == null ? null : Clazz.fromJson(json["clazz"]);
    questions = json["questions"] == null ? null : (json["questions"] as List).map((e) => Questions.fromJson(e)).toList();
    typeExercise = json["typeExercise"];
    deadline = json["deadline"];
    isPublic = json["isPublic"];
    status = json["status"];
    description = json["description"];
    files = json["files"] == null ? null : (json["files"] as List).map((e) => ResponseFileUpload.fromJson(e)).toList();
    createdAt = json["createdAt"];
    updatedAt = json["updatedAt"];
    createdBy = json["createdBy"];
    updatedBy = json["updatedBy"];
    v = json["__v"];
    scoreOfExercise = json["scoreOfExercise"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["title"] = title;
    if(teacher != null) {
      _data["teacher"] = teacher?.toJson();
    }
    if(subject != null) {
      _data["subject"] = subject?.toJson();
    }
    if(clazz != null) {
      _data["clazz"] = clazz?.toJson();
    }
    if(questions != null) {
      _data["questions"] = questions?.map((e) => e.toJson()).toList();
    }
    _data["typeExercise"] = typeExercise;
    _data["deadline"] = deadline;
    _data["isPublic"] = isPublic;
    _data["status"] = status;
    _data["description"] = description;
    if(files != null) {
      _data["files"] = files;
    }
    _data["createdAt"] = createdAt;
    _data["updatedAt"] = updatedAt;
    _data["createdBy"] = createdBy;
    _data["updatedBy"] = updatedBy;
    _data["__v"] = v;
    return _data;
  }
}


