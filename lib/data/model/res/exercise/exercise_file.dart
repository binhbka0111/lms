import '../file/response_file.dart';
class ExerciseFile {
  String? title;
  int? scoreFactor;
  int? deadline;
  String? description;
  List<ResponseFileUpload>? filesUpload;
  String? typeExercise;
  String? subjectId;
  String? teacherId;
  String? classId;

  ExerciseFile({this.title, this.scoreFactor, this.deadline, this.description, this.filesUpload, this.typeExercise, this.subjectId, this.teacherId, this.classId});

  ExerciseFile.fromJson(Map<String, dynamic> json) {
    title = json["title"];
    scoreFactor = json["scoreFactor"];
    deadline = json["deadline"];
    description = json["description"];
    filesUpload = json["filesUpload"] == null ? null : (json["filesUpload"] as List).map((e) => ResponseFileUpload.fromJson(e)).toList();
    typeExercise = json["typeExercise"];
    subjectId = json["subjectId"];
    teacherId = json["teacherId"];
    classId = json["classId"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["title"] = title;
    _data["scoreFactor"] = scoreFactor;
    _data["deadline"] = deadline;
    _data["description"] = description;
    if(filesUpload != null) {
      _data["filesUpload"] = filesUpload?.map((e) => e.toJson()).toList();
    }
    _data["typeExercise"] = typeExercise;
    _data["subjectId"] = subjectId;
    _data["teacherId"] = teacherId;
    _data["classId"] = classId;
    return _data;
  }
}
