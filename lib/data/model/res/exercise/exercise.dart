import 'package:flutter/cupertino.dart';
import '../file/response_file.dart';

class ExerciseQuestion {
  String? title;
  int? scoreFactor;
  int? deadline;
  String? description;
  List<Questions>? questions;
  String? typeExercise;
  String? subjectId;
  String? teacherId;
  String? classId;

  ExerciseQuestion({this.title, this.scoreFactor, this.deadline, this.description, this.questions, this.typeExercise, this.subjectId, this.teacherId, this.classId});

  ExerciseQuestion.fromJson(Map<String, dynamic> json) {
    title = json["title"];
    scoreFactor = json["scoreFactor"];
    deadline = json["deadline"];
    description = json["description"];
    questions = json["questions"] == null ? null : (json["questions"] as List).map((e) => Questions.fromJson(e)).toList();
    typeExercise = json["typeExercise"];
    subjectId = json["subjectId"];
    teacherId = json["teacherId"];
    classId = json["classId"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["title"] = title;
    _data["scoreFactor"] = scoreFactor;
    _data["deadline"] = deadline;
    _data["description"] = description;
    if(questions != null) {
      _data["questions"] = questions?.map((e) => e.toJson()).toList();
    }
    _data["typeExercise"] = typeExercise;
    _data["subjectId"] = subjectId;
    _data["teacherId"] = teacherId;
    _data["classId"] = classId;
    return _data;
  }
}

class IsAdd{
  String? id;
  bool? isAdd;
  IsAdd({this.id,this.isAdd});
}

class Questions {
  String? content;
  bool? errorContentQuestion;
  bool? errorPointQuestion;
  num? point;
  String? typeQuestion;
  String? id;
  List<AnswerOption>? answerOption;
  List<ResponseFileUpload>? files;

  Questions({this.content,this.id, this.point, this.typeQuestion, this.answerOption, this.files,this.errorContentQuestion,this.errorPointQuestion});

  Questions.fromJson(Map<String, dynamic> json) {
    content = json["content"];
    point = json["point"];
    id = json["_id"];
    typeQuestion = json["typeQuestion"];
    if (json['answerOption'] != null) {
      answerOption = [];
      json['answerOption'].forEach((v) {
        answerOption?.add(AnswerOption?.fromJson(v));
      });
    }
    if (json['files'] != null) {
      files = [];
      json['files'].forEach((v) {
        files?.add(ResponseFileUpload?.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["content"] = content;
    _data["point"] = point;
    _data["typeQuestion"] = typeQuestion;
    if(answerOption != null) {
      _data["answerOption"] = answerOption?.map((e) => e.toJson()).toList();
    }
    if(files != null) {
      _data["filesUploadQuestion"] = files?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class AnswerOption {
  String? key;
  String? value;
  String? status;
  TextEditingController? textEditingController;
  FocusNode? focusNode;
  bool? validateAnswer;
  String? errorTextAnswer;

  AnswerOption({this.key, this.value, this.status,this.textEditingController,this.focusNode,this.validateAnswer,this.errorTextAnswer});

  AnswerOption.fromJson(dynamic json) {
    key = json["key"];
    value = json["value"];
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["key"] = key;
    _data["value"] = value;
    _data["status"] = status;
    return _data;
  }
}

class AnswerOptionExStudent{
  String? key;
  String? value;
  dynamic status;
  TextEditingController? textEditingController;
  FocusNode? focusNode;
  bool? validateAnswer;
  AnswerOptionExStudent({this.key, this.value, this.status,this.textEditingController,this.focusNode,this.validateAnswer});

  AnswerOptionExStudent.fromJson(Map<String, dynamic> json) {
    key = json["key"];
    value = json["value"];
    status = json["status"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["key"] = key;
    _data["value"] = value;
    _data["status"] = status;
    return _data;
  }
}