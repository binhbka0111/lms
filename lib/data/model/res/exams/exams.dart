import '../exercise/exercise.dart';


class ExamsQuestion {
  String? title;
  int? scoreFactor;
  int? startTime;
  int? endTime;
  String? description;

  List<Questions>? questions;
  String? typeExercise;
  String? subjectId;
  String? teacherId;
  String? classId;

  ExamsQuestion({this.title, this.scoreFactor,this.startTime, this.endTime, this.description, this.questions, this.typeExercise, this.subjectId, this.teacherId, this.classId});

  ExamsQuestion.fromJson(Map<String, dynamic> json) {
    title = json["title"];
    scoreFactor = json["scoreFactor"];
    startTime = json["startTime"];
    endTime = json["endTime"];
    description = json["description"];
    questions = json["questions"] == null ? null : (json["questions"] as List).map((e) => Questions.fromJson(e)).toList();
    typeExercise = json["typeExercise"];
    subjectId = json["subjectId"];
    teacherId = json["teacherId"];
    classId = json["classId"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["title"] = title;
    _data["scoreFactor"] = scoreFactor;
    _data["startTime"] = startTime;
    _data["endTime"] = endTime;
    _data["description"] = description;
    if(questions != null) {
      _data["questions"] = questions?.map((e) => e.toJson()).toList();
    }
    _data["typeExercise"] = typeExercise;
    _data["subjectId"] = subjectId;
    _data["teacherId"] = teacherId;
    _data["classId"] = classId;
    return _data;
  }
}