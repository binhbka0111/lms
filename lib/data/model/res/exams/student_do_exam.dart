import '../../common/learning_managerment.dart';
import '../../common/subject.dart';
import '../exercise/exercise.dart';
import '../file/response_file.dart';

class StudentDoExam {
  String? id;
  String? title;
  Teacher? teacher;
  Subject? subject;
  Clazz? clazz;
  List<dynamic>? questions;
  String? typeExercise;
  int? startTime;
  int? endTime;
  String? isPublic;
  String? status;
  String? description;
  List<Files>? files;
  int? createdAt;
  int? updatedAt;
  String? createdBy;
  String? updatedBy;
  int? v;
  String? statusExerciseByStudent;
  Student? student;
  List<QuestionAnswers>? questionAnswers;
  List<ResponseFileUpload>? filesAnswer;
  String? teacherComment;
  String? contentAnswer;
  String? isPublicScore;
  String? link;
  int? score;

  StudentDoExam({this.id, this.title, this.teacher, this.subject, this.clazz, this.questions, this.typeExercise, this.startTime, this.endTime, this.isPublic, this.status, this.description, this.files, this.createdAt, this.updatedAt, this.createdBy, this.updatedBy, this.v, this.statusExerciseByStudent, this.student, this.questionAnswers, this.filesAnswer, this.teacherComment, this.contentAnswer, this.isPublicScore, this.score});

  StudentDoExam.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    title = json["title"];
    teacher = json["teacher"] == null ? null : Teacher.fromJson(json["teacher"]);
    subject = json["subject"] == null ? null : Subject.fromJson(json["subject"]);
    clazz = json["clazz"] == null ? null : Clazz.fromJson(json["clazz"]);
    questions = json["questions"] ?? [];
    typeExercise = json["typeExercise"];
    startTime = json["startTime"];
    endTime = json["endTime"];
    isPublic = json["isPublic"];
    status = json["status"];
    description = json["description"];
    files = json["files"] == null ? null : (json["files"] as List).map((e) => Files.fromJson(e)).toList();
    createdAt = json["createdAt"];
    updatedAt = json["updatedAt"];
    createdBy = json["createdBy"];
    updatedBy = json["updatedBy"];
    v = json["__v"];
    statusExerciseByStudent = json["statusExerciseByStudent"];
    student = json["student"] == null ? null : Student.fromJson(json["student"]);
    questionAnswers = json["questionAnswers"] == null ? null : (json["questionAnswers"] as List).map((e) => QuestionAnswers.fromJson(e)).toList();
    filesAnswer = json["filesAnswer"] == null ? null : (json["filesAnswer"] as List).map((e) => ResponseFileUpload.fromJson(e)).toList();
    teacherComment = json["teacherComment"];
    contentAnswer = json["contentAnswer"];
    isPublicScore = json["isPublicScore"];
    score = json["score"];
    link = json["link"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["title"] = title;
    if(teacher != null) {
      _data["teacher"] = teacher?.toJson();
    }
    if(subject != null) {
      _data["subject"] = subject?.toJson();
    }
    if(clazz != null) {
      _data["clazz"] = clazz?.toJson();
    }
    if(questions != null) {
      _data["questions"] = questions;
    }
    _data["typeExercise"] = typeExercise;
    _data["isPublic"] = isPublic;
    _data["status"] = status;
    _data["description"] = description;
    if(files != null) {
      _data["files"] = files?.map((e) => e.toJson()).toList();
    }
    _data["createdAt"] = createdAt;
    _data["updatedAt"] = updatedAt;
    _data["createdBy"] = createdBy;
    _data["updatedBy"] = updatedBy;
    _data["__v"] = v;
    _data["statusExerciseByStudent"] = statusExerciseByStudent;
    if(student != null) {
      _data["student"] = student?.toJson();
    }
    if(questionAnswers != null) {
      _data["questionAnswers"] = questionAnswers;
    }
    if(filesAnswer != null) {
      _data["filesAnswer"] = filesAnswer?.map((e) => e.toJson()).toList();
    }
    _data["teacherComment"] = teacherComment;
    _data["contentAnswer"] = contentAnswer;
    _data["isPublicScore"] = isPublicScore;
    _data["score"] = score;
    return _data;
  }
}
class QuestionAnswers {
  String? id;
  String? exercise;
  Questions? question;
  String? exerciseAnswer;
  String? student;
  String? answer;
  int? point;
  List<ResponseFileUpload>? files;
  int? createdAt;
  int? v;
  String? typeQuestion;
  String? teacherComment;

  QuestionAnswers({this.id, this.exercise, this.question, this.exerciseAnswer, this.student, this.answer, this.point, this.files, this.createdAt, this.v, this.typeQuestion});

  QuestionAnswers.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    exercise = json["exercise"];
    question = json["question"] == null ? null : Questions.fromJson(json["question"]);
    exerciseAnswer = json["exerciseAnswer"];
    student = json["student"];
    answer = json["answer"];
    point = json["point"];
    files = json["files"] == null ? null : (json["files"] as List).map((e) => ResponseFileUpload.fromJson(e)).toList();
    createdAt = json["createdAt"];
    v = json["__v"];
    typeQuestion = json["typeQuestion"];
    teacherComment = json["teacherComment"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["exercise"] = exercise;
    if(question != null) {
      _data["question"] = question?.toJson();
    }
    _data["exerciseAnswer"] = exerciseAnswer;
    _data["student"] = student;
    _data["answer"] = answer;
    _data["point"] = point;
    if(files != null) {
      _data["files"] = files;
    }
    _data["createdAt"] = createdAt;
    _data["__v"] = v;
    _data["typeQuestion"] = typeQuestion;
    return _data;
  }
}