import '../../common/learning_managerment.dart';
import '../exercise/exercise.dart';
import '../file/response_file.dart';

class DetailExam {
  String? id;
  String? title;
  Teacher? teacher;
  Subject? subject;
  Clazz? clazz;
  List<Questions>? questions;
  String? typeExercise;
  int? startTime;
  int? endTime;
  String? isPublic;
  String? status;
  String? description;
  List<ResponseFileUpload>? files;
  int? createdAt;
  int? updatedAt;
  String? createdBy;
  String? updatedBy;
  int? v;
  num? scoreOfExam;

  DetailExam({this.id, this.title, this.teacher, this.subject, this.clazz, this.questions, this.typeExercise, this.startTime,this.endTime, this.isPublic, this.status, this.description, this.files, this.createdAt, this.updatedAt, this.createdBy, this.updatedBy, this.v,this.scoreOfExam});

  DetailExam.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    title = json["title"];
    teacher = json["teacher"] == null ? null : Teacher.fromJson(json["teacher"]);
    subject = json["subject"] == null ? null : Subject.fromJson(json["subject"]);
    clazz = json["clazz"] == null ? null : Clazz.fromJson(json["clazz"]);
    questions = json["questions"] == null ? null : (json["questions"] as List).map((e) => Questions.fromJson(e)).toList();
    typeExercise = json["typeExercise"];
    startTime = json["startTime"];
    endTime = json["endTime"];
    isPublic = json["isPublic"];
    status = json["status"];
    description = json["description"];
    files = json["files"] == null ? null : (json["files"] as List).map((e) => ResponseFileUpload.fromJson(e)).toList();
    createdAt = json["createdAt"];
    updatedAt = json["updatedAt"];
    createdBy = json["createdBy"];
    updatedBy = json["updatedBy"];
    v = json["__v"];
    scoreOfExam = json["scoreOfExam"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["title"] = title;
    if(teacher != null) {
      _data["teacher"] = teacher?.toJson();
    }
    if(subject != null) {
      _data["subject"] = subject?.toJson();
    }
    if(clazz != null) {
      _data["clazz"] = clazz?.toJson();
    }
    if(questions != null) {
      _data["questions"] = questions?.map((e) => e.toJson()).toList();
    }
    _data["typeExercise"] = typeExercise;
    _data["isPublic"] = isPublic;
    _data["status"] = status;
    _data["description"] = description;
    if(files != null) {
      _data["files"] = files;
    }
    _data["createdAt"] = createdAt;
    _data["updatedAt"] = updatedAt;
    _data["createdBy"] = createdBy;
    _data["updatedBy"] = updatedBy;
    _data["__v"] = v;
    return _data;
  }
}