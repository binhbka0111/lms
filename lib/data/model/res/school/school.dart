import '../../common/home_room_teacher.dart';

class School {
  String? id;
  String? name;
  List<SchoolLevel>? schoolLevel;

  School({this.id, this.name, this.schoolLevel});

  School.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    name = json["name"];
    schoolLevel = json["schoolLevel"] == null ? null : (json["schoolLevel"] as List).map((e) => SchoolLevel.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["name"] = name;
    if(schoolLevel != null) {
      _data["schoolLevel"] = schoolLevel?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}




class ClassBySchoolLevel {
  String? id;
  String? code;
  String? name;
  String? classId;
  bool? homeroomClass;

  ClassBySchoolLevel({this.id, this.code, this.name, this.classId, this.homeroomClass});

  ClassBySchoolLevel.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
    classId = json["classId"];
    homeroomClass = json["homeroomClass"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    _data["classId"] = classId;
    _data["homeroomClass"] = homeroomClass;
    return _data;
  }
}


