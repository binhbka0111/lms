class ResponseFileUpload {
  ResponseFileUpload({
    this.tmpFolderUpload,
    this.name,
    this.originalFileName,
    this.ext,
    this.size,
    this.link});

  ResponseFileUpload.fromJson(dynamic json) {
    tmpFolderUpload = json['tmpFolderUpload'];
    name = json['name'];
    originalFileName = json['originalFileName'];
    ext = json['ext'];
    size = json['size'];
    link = json['link'];
  }
  String? tmpFolderUpload;
  String? name;
  String? originalFileName;
  String? ext;
  String? size;
  String? link;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['tmpFolderUpload'] = tmpFolderUpload;
    map['name'] = name;
    map['originalFileName'] = originalFileName;
    map['ext'] = ext;
    map['size'] = size;
    map['link'] = link;
    return map;
  }

}