class OTPResponse {
  OTPResponse({
    this.transId,
  });

  OTPResponse.fromJson(dynamic json) {
    transId = json['transId'];
  }

  String? transId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['transId'] = transId;
    return map;
  }
}
