class LoginResponse {
  LoginResponse({
    this.accessToken,
    this.userType,
  });

  LoginResponse.fromJson(dynamic json) {
    accessToken = json['access_token'];
    userType = json['userType'];
  }

  String? accessToken;
  String? userType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['access_token'] = accessToken;
    map['userType'] = userType;
    return map;
  }
}