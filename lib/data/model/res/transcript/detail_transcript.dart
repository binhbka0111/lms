import 'package:slova_lms/data/model/res/transcript/semester.dart';

class DetailTranscript {
  String? id;
  String? name;
  Block? block;
  Clazz? clazz;
  Subject? subject;
  SchoolYear? schoolYear;
  Semester? semester;
  List<TableHeaders>? tableHeaders;
  List<TableRows>? tableRows;
  String? type;
  String? isPublic;

  DetailTranscript({this.id, this.name, this.block, this.clazz, this.subject, this.schoolYear, this.semester, this.tableHeaders, this.tableRows, this.type, this.isPublic});

  DetailTranscript.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    name = json["name"];
    block = json["block"] == null ? null : Block.fromJson(json["block"]);
    clazz = json["clazz"] == null ? null : Clazz.fromJson(json["clazz"]);
    subject = json["subject"] == null ? null : Subject.fromJson(json["subject"]);
    schoolYear = json["schoolYear"] == null ? null : SchoolYear.fromJson(json["schoolYear"]);
    semester = json["semester"] == null ? null : Semester.fromJson(json["semester"]);
    tableHeaders = json["tableHeaders"] == null ? null : (json["tableHeaders"] as List).map((e) => TableHeaders.fromJson(e)).toList();
    tableRows = json["tableRows"] == null ? null : (json["tableRows"] as List).map((e) => TableRows.fromJson(e)).toList();
    type = json["type"];
    isPublic = json["isPublic"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["name"] = name;
    if(block != null) {
      _data["block"] = block?.toJson();
    }
    if(clazz != null) {
      _data["clazz"] = clazz?.toJson();
    }
    if(subject != null) {
      _data["subject"] = subject?.toJson();
    }
    if(schoolYear != null) {
      _data["schoolYear"] = schoolYear?.toJson();
    }
    if(semester != null) {
      _data["semester"] = semester?.toJson();
    }
    if(tableHeaders != null) {
      _data["tableHeaders"] = tableHeaders?.map((e) => e.toJson()).toList();
    }
    if(tableRows != null) {
      _data["tableRows"] = tableRows?.map((e) => e.toJson()).toList();
    }
    _data["type"] = type;
    _data["isPublic"] = isPublic;
    return _data;
  }
}

class TableRows {
  String? student;
  Value? value;

  TableRows({this.student, this.value});

  TableRows.fromJson(Map<String, dynamic> json) {
    student = json["student"];
    value = json["value"] == null ? null : Value.fromJson(json["value"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["student"] = student;
    if(value != null) {
      _data["value"] = value?.toJson();
    }
    return _data;
  }
}

class Value {
  int? order;
  String? fullName;
  int? birthday;
  String? sex;
  dynamic description;

  Value({this.order, this.fullName, this.birthday, this.sex, this.description});

  Value.fromJson(Map<String, dynamic> json) {
    order = json["order"];
    fullName = json["fullName"];
    birthday = json["birthday"];
    sex = json["sex"];
    description = json["description"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["order"] = order;
    _data["fullName"] = fullName;
    _data["birthday"] = birthday;
    _data["sex"] = sex;
    _data["description"] = description;
    return _data;
  }
}

class TableHeaders {
  String? status;
  String? title;
  String? key;

  TableHeaders({this.status, this.title, this.key});

  TableHeaders.fromJson(Map<String, dynamic> json) {
    status = json["status"];
    title = json["title"];
    key = json["key"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["status"] = status;
    _data["title"] = title;
    _data["key"] = key;
    return _data;
  }
}


class SchoolYear {
  String? id;
  int? fromYear;
  int? toYear;

  SchoolYear({this.id, this.fromYear, this.toYear});

  SchoolYear.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fromYear = json["fromYear"];
    toYear = json["toYear"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fromYear"] = fromYear;
    _data["toYear"] = toYear;
    return _data;
  }
}

class Subject {
  String? id;
  SubjectCategory? subjectCategory;

  Subject({this.id, this.subjectCategory});

  Subject.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    subjectCategory = json["subjectCategory"] == null ? null : SubjectCategory.fromJson(json["subjectCategory"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(subjectCategory != null) {
      _data["subjectCategory"] = subjectCategory?.toJson();
    }
    return _data;
  }
}

class SubjectCategory {
  String? id;
  String? code;
  String? name;

  SubjectCategory({this.id, this.code, this.name});

  SubjectCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    return _data;
  }
}

class Clazz {
  String? id;
  ClassCategory? classCategory;

  Clazz({this.id, this.classCategory});

  Clazz.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    classCategory = json["classCategory"] == null ? null : ClassCategory.fromJson(json["classCategory"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(classCategory != null) {
      _data["classCategory"] = classCategory?.toJson();
    }
    return _data;
  }
}

class ClassCategory {
  String? id;
  String? code;
  String? name;

  ClassCategory({this.id, this.code, this.name});

  ClassCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    return _data;
  }
}

class Block {
  String? id;
  BlockCategory? blockCategory;

  Block({this.id, this.blockCategory});

  Block.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    blockCategory = json["blockCategory"] == null ? null : BlockCategory.fromJson(json["blockCategory"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(blockCategory != null) {
      _data["blockCategory"] = blockCategory?.toJson();
    }
    return _data;
  }
}

class BlockCategory {
  String? id;
  String? code;
  String? name;

  BlockCategory({this.id, this.code, this.name});

  BlockCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    return _data;
  }
}



class PointTranscript{
  String? key;
  String? title;
  dynamic point;
  PointTranscript({this.key, this.point,this.title});
}