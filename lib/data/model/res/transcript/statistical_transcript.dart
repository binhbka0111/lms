
import 'package:slova_lms/data/model/res/transcript/semester.dart';

import '../class/block.dart';

class StatisticalTranscript {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  List<ItemsStatisticalTranscript>? items;

  StatisticalTranscript({this.pageIndex, this.pageSize, this.totalPages, this.totalItems, this.items});

  StatisticalTranscript.fromJson(Map<String, dynamic> json) {
    pageIndex = json["pageIndex"];
    pageSize = json["pageSize"];
    totalPages = json["totalPages"];
    totalItems = json["totalItems"];
    items = json["items"] == null ? null : (json["items"] as List).map((e) => ItemsStatisticalTranscript.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["pageIndex"] = pageIndex;
    _data["pageSize"] = pageSize;
    _data["totalPages"] = totalPages;
    _data["totalItems"] = totalItems;
    if(items != null) {
      _data["items"] = items?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class ItemsStatisticalTranscript {
  Student? student;
  Value? value;
  String? school;
  String? transcriptId;
  Block? block;
  Clazz? clazz;
  Semester? semester;
  dynamic subject;
  SchoolYear? schoolYear;

  ItemsStatisticalTranscript({this.student, this.value, this.school,this.transcriptId, this.block, this.clazz, this.semester, this.subject, this.schoolYear});

  ItemsStatisticalTranscript.fromJson(Map<String, dynamic> json) {
    student = json["student"] == null ? null : Student.fromJson(json["student"]);
    value = json["value"] == null ? null : Value.fromJson(json["value"]);
    school = json["school"];
    transcriptId = json["transcriptId"];
    block = json["block"] == null ? null : Block.fromJson(json["block"]);
    clazz = json["clazz"] == null ? null : Clazz.fromJson(json["clazz"]);
    semester = json["semester"] == null ? null : Semester.fromJson(json["semester"]);
    subject = json["subject"];
    schoolYear = json["schoolYear"] == null ? null : SchoolYear.fromJson(json["schoolYear"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    if(student != null) {
      _data["student"] = student?.toJson();
    }
    if(value != null) {
      _data["value"] = value?.toJson();
    }
    _data["school"] = school;
    if(block != null) {
      _data["block"] = block?.toJson();
    }
    if(clazz != null) {
      _data["clazz"] = clazz?.toJson();
    }
    if(semester != null) {
      _data["semester"] = semester?.toJson();
    }
    _data["subject"] = subject;
    if(schoolYear != null) {
      _data["schoolYear"] = schoolYear?.toJson();
    }
    return _data;
  }
}

class SchoolYear {
  String? id;
  int? fromYear;
  int? toYear;

  SchoolYear({this.id, this.fromYear, this.toYear});

  SchoolYear.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fromYear = json["fromYear"];
    toYear = json["toYear"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fromYear"] = fromYear;
    _data["toYear"] = toYear;
    return _data;
  }
}


class Clazz {
  String? id;
  ClassCategory? classCategory;

  Clazz({this.id, this.classCategory});

  Clazz.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    classCategory = json["classCategory"] == null ? null : ClassCategory.fromJson(json["classCategory"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(classCategory != null) {
      _data["classCategory"] = classCategory?.toJson();
    }
    return _data;
  }
}

class ClassCategory {
  String? id;
  String? code;
  String? name;

  ClassCategory({this.id, this.code, this.name});

  ClassCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    return _data;
  }
}

class Value {
  int? order;
  String? fullName;
  int? birthday;
  String? sex;
  List<dynamic>? mediumScore;
  dynamic description;

  Value({this.order, this.fullName, this.birthday, this.sex, this.mediumScore, this.description});

  Value.fromJson(Map<String, dynamic> json) {
    order = json["order"];
    fullName = json["fullName"];
    birthday = json["birthday"];
    sex = json["sex"];

    mediumScore = json["mediumScore"] == null ? null : List<dynamic>.from(json["mediumScore"]);
    description = json["description"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["order"] = order;
    _data["fullName"] = fullName;
    _data["birthday"] = birthday;
    _data["sex"] = sex;
    if(mediumScore != null) {
      _data["mediumScore"] = mediumScore;
    }
    _data["description"] = description;
    return _data;
  }
}

class Student {
  String? id;
  String? fullName;
  int? birthday;
  String? image;

  Student({this.id, this.fullName, this.birthday, this.image});

  Student.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    birthday = json["birthday"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["birthday"] = birthday;
    _data["image"] = image;
    return _data;
  }
}




class StatisticalTranscriptPercent {
  String? id;
  int? ordinal;
  num? endScore;
  num? startScore;
  int? total;
  String? percent;
  String? color;

  StatisticalTranscriptPercent({this.id, this.ordinal, this.endScore, this.startScore, this.total, this.percent, this.color});

  StatisticalTranscriptPercent.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    ordinal = json["ordinal"];
    endScore = json["endScore"];
    startScore = json["startScore"];
    total = json["total"];
    percent = json["percent"];
    color = json["color"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["ordinal"] = ordinal;
    _data["endScore"] = endScore;
    _data["startScore"] = startScore;
    _data["total"] = total;
    _data["percent"] = percent;
    _data["color"] = color;
    return _data;
  }
}