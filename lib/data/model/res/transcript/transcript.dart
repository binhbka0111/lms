
import 'package:slova_lms/data/model/res/transcript/semester.dart';

import '../class/block.dart';

class Transcript {
  String? id;
  String? name;
  Block? block;
  Clazz? clazz;
  Subject? subject;
  SchoolYear? schoolYear;
  Semester? semester;
  String? type;
  String? isPublic;
  int? createdAt;
  CreatedBy? createdBy;

  Transcript({this.id, this.name, this.block, this.clazz, this.subject, this.schoolYear, this.semester, this.type, this.isPublic, this.createdAt, this.createdBy});

  Transcript.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    name = json["name"];
    block = json["block"] == null ? null : Block.fromJson(json["block"]);
    clazz = json["clazz"] == null ? null : Clazz.fromJson(json["clazz"]);
    subject = json["subject"] == null ? null : Subject.fromJson(json["subject"]);
    schoolYear = json["schoolYear"] == null ? null : SchoolYear.fromJson(json["schoolYear"]);
    semester = json["semester"] == null ? null : Semester.fromJson(json["semester"]);
    type = json["type"];
    isPublic = json["isPublic"];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"] == null ? null : CreatedBy.fromJson(json["createdBy"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["name"] = name;
    if(block != null) {
      _data["block"] = block?.toJson();
    }
    if(clazz != null) {
      _data["clazz"] = clazz?.toJson();
    }
    if(subject != null) {
      _data["subject"] = subject?.toJson();
    }
    if(schoolYear != null) {
      _data["schoolYear"] = schoolYear?.toJson();
    }
    if(semester != null) {
      _data["semester"] = semester?.toJson();
    }
    _data["type"] = type;
    _data["isPublic"] = isPublic;
    _data["createdAt"] = createdAt;
    if(createdBy != null) {
      _data["createdBy"] = createdBy?.toJson();
    }
    return _data;
  }
}

class CreatedBy {
  String? id;
  String? fullName;
  String? image;

  CreatedBy({this.id, this.fullName, this.image});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["image"] = image;
    return _data;
  }
}

class SemesterTranscript {
  String? id;
  String? name;

  SemesterTranscript({this.id, this.name});

  SemesterTranscript.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["name"] = name;
    return _data;
  }
}

class SchoolYear {
  String? id;
  int? fromYear;
  int? toYear;

  SchoolYear({this.id, this.fromYear, this.toYear});

  SchoolYear.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fromYear = json["fromYear"];
    toYear = json["toYear"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fromYear"] = fromYear;
    _data["toYear"] = toYear;
    return _data;
  }
}

class Subject {
  String? id;
  SubjectCategory? subjectCategory;

  Subject({this.id, this.subjectCategory});

  Subject.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    subjectCategory = json["subjectCategory"] == null ? null : SubjectCategory.fromJson(json["subjectCategory"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(subjectCategory != null) {
      _data["subjectCategory"] = subjectCategory?.toJson();
    }
    return _data;
  }
}

class SubjectCategory {
  String? id;
  String? code;
  String? name;

  SubjectCategory({this.id, this.code, this.name});

  SubjectCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    return _data;
  }
}

class Clazz {
  String? id;
  ClassCategory? classCategory;

  Clazz({this.id, this.classCategory});

  Clazz.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    classCategory = json["classCategory"] == null ? null : ClassCategory.fromJson(json["classCategory"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(classCategory != null) {
      _data["classCategory"] = classCategory?.toJson();
    }
    return _data;
  }
}

class ClassCategory {
  String? id;
  String? code;
  String? name;

  ClassCategory({this.id, this.code, this.name});

  ClassCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    return _data;
  }
}
