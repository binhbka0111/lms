import '../school/school.dart';

class Semester {
  String? id;
  String? code;
  String? name;
  String? description;
  String? status;
  School? school;
  int? createdAt;
  int? v;
  String? createdBy;

  Semester({this.id, this.code, this.name, this.description, this.status, this.school, this.createdAt, this.v, this.createdBy});

  Semester.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
    description = json["description"];
    status = json["status"];
    school = json["school"] == null ? null : School.fromJson(json["school"]);
    createdAt = json["createdAt"];
    v = json["__v"];
    createdBy = json["createdBy"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    _data["description"] = description;
    _data["status"] = status;
    if(school != null) {
      _data["school"] = school?.toJson();
    }
    _data["createdAt"] = createdAt;
    _data["__v"] = v;
    _data["createdBy"] = createdBy;
    return _data;
  }
}

