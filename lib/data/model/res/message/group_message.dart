import '../file/response_file.dart';

class GroupMessage {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  ItemsGroupMessage? items;

  GroupMessage({this.pageIndex, this.pageSize, this.totalPages, this.totalItems, this.items});

  GroupMessage.fromJson(Map<String, dynamic> json) {
    pageIndex = json["pageIndex"];
    pageSize = json["pageSize"];
    totalPages = json["totalPages"];
    totalItems = json["totalItems"];
    items = json["items"] == null ? null : ItemsGroupMessage.fromJson(json["items"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["pageIndex"] = pageIndex;
    _data["pageSize"] = pageSize;
    _data["totalPages"] = totalPages;
    _data["totalItems"] = totalItems;
    if(items != null) {
      _data["items"] = items?.toJson();
    }
    return _data;
  }
}

class ItemsGroupMessage {
  int? countNotSeen;
  List<DataGroupMessage>? data;

  ItemsGroupMessage({this.countNotSeen, this.data});

  ItemsGroupMessage.fromJson(Map<String, dynamic> json) {
    countNotSeen = json["countNotSeen"];
    data = json["data"] == null ? null : (json["data"] as List).map((e) => DataGroupMessage.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["countNotSeen"] = countNotSeen;
    if(data != null) {
      _data["data"] = data?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class DataGroupMessage {
  String? id;
  String? name;
  String? type;
  String? image;
  String? isNotify;
  String? createdBy;
  int? countUsers;
  Message? message;
  int? countNotSeen;

  DataGroupMessage({this.id, this.name, this.type,this.createdBy, this.image, this.isNotify, this.countUsers, this.message, this.countNotSeen});

  DataGroupMessage.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    name = json["name"];
    type = json["type"];
    image = json["image"];
    isNotify = json["isNotify"];
    countUsers = json["countUsers"];
    message = json["message"] == null ? null : Message.fromJson(json["message"]);
    countNotSeen = json["countNotSeen"];
    createdBy = json["createdBy"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["name"] = name;
    _data["type"] = type;
    _data["image"] = image;
    _data["isNotify"] = isNotify;
    _data["countUsers"] = countUsers;
    if(message != null) {
      _data["message"] = message?.toJson();
    }
    _data["countNotSeen"] = countNotSeen;
    return _data;
  }
}

class Message {
  String? id;
  String? content;
  List<ResponseFileUpload>? files;
  Sender? sender;
  String? chatRoomId;
  String? type;
  String? notifyType;
  List<Users>? user;
  int? createdAt;
  Message? messParent;



  Message({this.id, this.content, this.files, this.sender, this.chatRoomId, this.createdAt,this.type,this.user,this.notifyType});

  Message.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    content = json["content"];
    if (json['files'] != null) {
      files = [];
      json['files'].forEach((v) {
        files?.add(ResponseFileUpload?.fromJson(v));
      });
    }
    sender = json["sender"] == null ? null : Sender.fromJson(json["sender"]);
    chatRoomId = json["chatRoomId"];
    createdAt = json["createdAt"];
    type = json["type"];
    notifyType = json["notifyType"]??"";
    messParent = json["messParent"] == null ? null : Message.fromJson(json["messParent"]);
    if (json['users'] != null) {
      user = [];
      json['users'].forEach((v) {
        user?.add(Users?.fromJson(v));
      });
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["content"] = content;
    if(files != null) {
      _data["files"] = files;
    }
    if(sender != null) {
      _data["sender"] = sender?.toJson();
    }
    _data["chatRoomId"] = chatRoomId;

    _data["createdAt"] = createdAt;
    return _data;
  }
}

class SeenBys {
  String? user;
  int? seenAt;

  SeenBys({this.user, this.seenAt});

  SeenBys.fromJson(Map<String, dynamic> json) {
    user = json["user"];
    seenAt = json["seenAt"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["user"] = user;
    _data["seenAt"] = seenAt;
    return _data;
  }
}

class Sender {
  String? id;
  String? fullName;
  String? image;

  Sender({this.id, this.fullName, this.image});

  Sender.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["image"] = image;
    return _data;
  }
}
class Users {
  String? id;
  String? fullName;
  String? image;

  Users({this.id, this.fullName, this.image});

  Users.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["image"] = image;
    return _data;
  }
}

class TransId {
  String? id;

  TransId({this.id});

  TransId.fromJson(Map<String, dynamic> json) {
    id = json["_id"];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    return _data;
  }
}


class Message2 {
  String? id;
  String? content;
  List<ResponseFileUpload>? files;
  Sender? sender;
  String? chatRoomId;
  int? createdAt;
  Message? messParent;
  Session? position;
  String? type;
  String? notifyType;
  List<Users>? user;

  Message2({this.id, this.content, this.files, this.sender, this.chatRoomId, this.createdAt,this.position,this.messParent,this.type,this.user,this.notifyType});
}


enum Session{
  start,center,end,oneItem
}