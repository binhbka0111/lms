
import '../../common/contacts.dart';

class UserInGroupChat {
  String? pageIndex;
  String? pageSize;
  int? totalPages;
  int? totalItems;
  List<ItemContact>? items;

  UserInGroupChat({this.pageIndex, this.pageSize, this.totalPages, this.totalItems, this.items});

  UserInGroupChat.fromJson(Map<String, dynamic> json) {
    pageIndex = json["pageIndex"];
    pageSize = json["pageSize"];
    totalPages = json["totalPages"];
    totalItems = json["totalItems"];
    items = json["items"] == null ? null : (json["items"] as List).map((e) => ItemContact.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["pageIndex"] = pageIndex;
    _data["pageSize"] = pageSize;
    _data["totalPages"] = totalPages;
    _data["totalItems"] = totalItems;
    if(items != null) {
      _data["items"] = items?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}
