import '../../common/detail_notify_leaving_application.dart';


class DetailGroupChat {
  String? isNotify;
  String? id;
  String? name;
  String? type;
  String? image;
  List<Users>? users;
  int? countUsers;

  CreatedBy? createdBy;

  DetailGroupChat({this.createdBy,this.isNotify, this.id, this.name, this.type, this.image, this.users, this.countUsers});

  DetailGroupChat.fromJson(Map<String, dynamic> json) {
    isNotify = json["isNotify"];
    id = json["_id"];
    name = json["name"];
    type = json["type"];
    image = json["image"];
    users = json["users"] == null ? null : (json["users"] as List).map((e) => Users.fromJson(e)).toList();
    countUsers = json["countUsers"];
    createdBy = json["createdBy"] == null ? null : CreatedBy.fromJson(json["createdBy"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["isNotify"] = isNotify;
    _data["_id"] = id;
    _data["name"] = name;
    _data["type"] = type;
    _data["image"] = image;
    if(users != null) {
      _data["users"] = users?.map((e) => e.toJson()).toList();
    }
    _data["countUsers"] = countUsers;
    return _data;
  }
}

class Users {
  String? id;
  String? fullName;
  String? image;

  Users({this.id, this.fullName, this.image});

  Users.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    fullName = json["fullName"];
    image = json["image"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["image"] = image;
    return _data;
  }
}