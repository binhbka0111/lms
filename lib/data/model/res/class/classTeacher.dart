class ClassId {
  String? id;
  String? code;
  String? name;
  String? classId;
  bool? homeroomClass;
  bool checked = false;

  ClassId({this.id, this.code, this.name, this.classId, this.homeroomClass});

  ClassId.fromJson( dynamic json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
    classId = json["classId"];
    homeroomClass = json["homeroomClass"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    _data["classId"] = classId;
    _data["homeroomClass"] = homeroomClass;
    return _data;
  }
}