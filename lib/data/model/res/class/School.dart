class SchoolData {
  SchoolData({
      this.name, 
      this.id,
    this.address

  });

  SchoolData.fromJson(dynamic json) {

    if (json['name'] != null||json['name']!="") {
      name = json['name'];
    }else{
      name = "";
    }
    if (json['_id'] != null||json['_id']!="") {
      id = json['_id'];
    }else{
      id = "";
    }

    if (json['address'] != null||json['address']!="") {
      address = json['address'];
    }else{
      address = "";
    }
  }
  String? name;
  String? id;
  String ? address;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    map['_id'] = id;
    map['address'] = address;
    return map;
  }

}