import '../../common/home_room_teacher.dart';
class Block {
  String? id;
  BlockCategory? blockCategory;
  String? school;
  String? schoolYear;
  List<Class>? classes;
  int? ordinal;
  SchoolLevel? schoolLevel;
  int? v;

  Block({this.id, this.blockCategory, this.school, this.schoolYear, this.classes, this.ordinal, this.schoolLevel, this.v});

  Block.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    blockCategory = json["blockCategory"] == null ? null : BlockCategory.fromJson(json["blockCategory"]);
    school = json["school"];
    schoolYear = json["schoolYear"];
    classes = json["classes"] == null ? null : (json["classes"] as List).map((e) => Class.fromJson(e)).toList();
    ordinal = json["ordinal"];
    schoolLevel = json["schoolLevel"] == null ? null : SchoolLevel.fromJson(json["schoolLevel"]);
    v = json["__v"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(blockCategory != null) {
      _data["blockCategory"] = blockCategory?.toJson();
    }
    _data["school"] = school;
    _data["schoolYear"] = schoolYear;
    if(classes != null) {
      _data["classes"] = classes?.map((e) => e.toJson()).toList();
    }
    _data["ordinal"] = ordinal;
    if(schoolLevel != null) {
      _data["schoolLevel"] = schoolLevel?.toJson();
    }
    _data["__v"] = v;
    return _data;
  }
}



class Class {
  String? id;
  ClassCategory? classCategory;

  Class({this.id, this.classCategory});

  Class.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    classCategory = json["classCategory"] == null ? null : ClassCategory.fromJson(json["classCategory"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    if(classCategory != null) {
      _data["classCategory"] = classCategory?.toJson();
    }
    return _data;
  }
}

class ClassCategory {
  String? id;
  String? code;
  String? name;
  String? status;
  String? blockCategory;
  String? school;
  List<dynamic>? subjectTeacher;
  int? createdAt;
  int? v;
  int? updatedAt;
  String? updatedBy;
  int? ordinal;
  SchoolLevel? schoolLevel;

  ClassCategory({this.id, this.code, this.name, this.status, this.blockCategory, this.school, this.subjectTeacher, this.createdAt, this.v, this.updatedAt, this.updatedBy, this.ordinal, this.schoolLevel});

  ClassCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
    status = json["status"];
    blockCategory = json["blockCategory"];
    school = json["school"];
    subjectTeacher = json["subjectTeacher"] ?? [];
    createdAt = json["createdAt"];
    v = json["__v"];
    updatedAt = json["updatedAt"];
    updatedBy = json["updatedBy"];
    ordinal = json["ordinal"];
    schoolLevel = json["schoolLevel"] == null ? null : SchoolLevel.fromJson(json["schoolLevel"]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    _data["status"] = status;
    _data["blockCategory"] = blockCategory;
    _data["school"] = school;
    if(subjectTeacher != null) {
      _data["subjectTeacher"] = subjectTeacher;
    }
    _data["createdAt"] = createdAt;
    _data["__v"] = v;
    _data["updatedAt"] = updatedAt;
    _data["updatedBy"] = updatedBy;
    _data["ordinal"] = ordinal;
    if(schoolLevel != null) {
      _data["schoolLevel"] = schoolLevel?.toJson();
    }
    return _data;
  }
}



class BlockCategory {
  String? id;
  String? code;
  String? name;
  String? description;
  String? status;
  String? school;
  int? ordinal;
  String? level;
  int? createdAt;
  int? v;
  SchoolLevel? schoolLevel;
  int? updatedAt;
  String? updatedBy;

  BlockCategory({this.id, this.code, this.name, this.description, this.status, this.school, this.ordinal, this.level, this.createdAt, this.v, this.schoolLevel, this.updatedAt, this.updatedBy});

  BlockCategory.fromJson(Map<String, dynamic> json) {
    id = json["_id"];
    code = json["code"];
    name = json["name"];
    description = json["description"];
    status = json["status"];
    school = json["school"];
    ordinal = json["ordinal"];
    level = json["level"];
    createdAt = json["createdAt"];
    v = json["__v"];
    schoolLevel = json["schoolLevel"] == null ? null : SchoolLevel.fromJson(json["schoolLevel"]);
    updatedAt = json["updatedAt"];
    updatedBy = json["updatedBy"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["code"] = code;
    _data["name"] = name;
    _data["description"] = description;
    _data["status"] = status;
    _data["school"] = school;
    _data["ordinal"] = ordinal;
    _data["level"] = level;
    _data["createdAt"] = createdAt;
    _data["__v"] = v;
    if(schoolLevel != null) {
      _data["schoolLevel"] = schoolLevel?.toJson();
    }
    _data["updatedAt"] = updatedAt;
    _data["updatedBy"] = updatedBy;
    return _data;
  }
}


class HomeroomTeacher {
  String? id;
  String? fullName;
  String? password;
  String? phone;
  String? email;
  String? school;
  String? position;
  String? title;
  String? status;
  String? workingStatus;
  String? sex;
  int? birthday;
  String? address;
  String? type;
  String? description;
  int? dateStartWorked;
  int? createdAt;
  String? createdBy;
  int? v;
  int? updatedAt;
  String? updatedBy;

  HomeroomTeacher({this.id, this.fullName, this.password, this.phone, this.email, this.school, this.position, this.title, this.status, this.workingStatus, this.sex, this.birthday, this.address, this.type, this.description, this.dateStartWorked, this.createdAt, this.createdBy, this.v, this.updatedAt, this.updatedBy});

  HomeroomTeacher.fromJson(dynamic json) {
    id = json["_id"];
    fullName = json["fullName"];
    password = json["password"];
    phone = json["phone"];
    email = json["email"];
    school = json["school"];
    position = json["position"];
    title = json["title"];
    status = json["status"];
    workingStatus = json["workingStatus"];
    sex = json["sex"];
    birthday = json["birthday"];
    address = json["address"];
    type = json["type"];
    description = json["description"];
    dateStartWorked = json["dateStartWorked"];
    createdAt = json["createdAt"];
    createdBy = json["createdBy"];
    v = json["__v"];
    updatedAt = json["updatedAt"];
    updatedBy = json["updatedBy"];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["_id"] = id;
    _data["fullName"] = fullName;
    _data["password"] = password;
    _data["phone"] = phone;
    _data["email"] = email;
    _data["school"] = school;
    _data["position"] = position;
    _data["title"] = title;
    _data["status"] = status;
    _data["workingStatus"] = workingStatus;
    _data["sex"] = sex;
    _data["birthday"] = birthday;
    _data["address"] = address;
    _data["type"] = type;
    _data["description"] = description;
    _data["dateStartWorked"] = dateStartWorked;
    _data["createdAt"] = createdAt;
    _data["createdBy"] = createdBy;
    _data["__v"] = v;
    _data["updatedAt"] = updatedAt;
    _data["updatedBy"] = updatedBy;
    return _data;
  }
}



class Block1{
  String? id;
  String? name;

  Block1({this.id, this.name});
}

class Class1{
  String? id;
  String? name;

  Class1({this.id, this.name});
}

