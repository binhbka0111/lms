import '../../../commom/constants/api_constant.dart';
import '../../base_service/api_response.dart';
import '../../base_service/api_service.dart';
import '../../base_service/respone_data.dart';
import '../../model/base_model.dart';
import '../../model/common/learning_managerment.dart';
import '../../model/res/exercise/detail_exercise.dart';
import '../../model/res/exercise/exercise.dart';
import '../../model/res/exercise/list_student_exercise.dart';
import '../../model/res/exercise/student_do_exercise.dart';
import '../../model/res/exercise/teacherCommentAnswer.dart';
import '../../model/res/file/response_file.dart';

class ExerciseRepo {
  Future<ResponseData<BaseModel>> createExerciseQuestions(title,scoreFactor,deadline,description,typeExercise,subjectId,teacherId,classId,List<Questions> questions) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["title"] = title;
    param["scoreFactor"] = scoreFactor;
    param["deadline"] = deadline;
    param["description"] = description;
    param["typeExercise"] = typeExercise;
    param["subjectId"] = subjectId;
    param["teacherId"] = teacherId;
    param["classId"] = classId;
    param["questions"] = questions.map((v) => {
      "content": v.content,
      "point": v.point,
      "typeQuestion": v.typeQuestion,
      "answerOption": v.answerOption?.map((e) => {
        "key": e.key,
        "value":e.value,
        "status":e.status
      }).toList(),
      "files": v.files?.map((e) => {
        "name": e.name,
        "tmpFolderUpload":e.tmpFolderUpload,
        "size":e.size
      }).toList(),

    }).toList();
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_EXERCISES, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> updateExerciseQuestions(exerciseId,title,scoreFactor,deadline,description,typeExercise,subjectId,teacherId,classId,List<Questions> questions) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["title"] = title;
    param["scoreFactor"] = scoreFactor;
    param["deadline"] = deadline;
    param["description"] = description;
    param["typeExercise"] = typeExercise;
    param["subjectId"] = subjectId;
    param["teacherId"] = teacherId;
    param["classId"] = classId;
    param["questions"] = questions.map((v) => {
      "content": v.content,
      "point": v.point,
      "typeQuestion": v.typeQuestion,
      "answerOption": v.answerOption?.map((e) => {
        "key": e.key,
        "value":e.value,
        "status":e.status
      }).toList(),
      "files": v.files?.map((e) => {
        "name": e.name,
        "tmpFolderUpload":e.tmpFolderUpload,
        "size":e.size
      }).toList(),

    }).toList();
    var urlApi = "${ApiConstants.API_EXERCISES}/$exerciseId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> createExerciseFile(title,deadline,description,typeExercise,subjectId,teacherId,classId,List<ResponseFileUpload> files,scoreOfExercise) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["title"] = title;
    param["deadline"] = deadline;
    param["description"] = description;
    param["typeExercise"] = typeExercise;
    param["subjectId"] = subjectId;
    param["teacherId"] = teacherId;
    param["classId"] = classId;
    param["scoreOfExercise"] = scoreOfExercise;
    param["filesUpload"] = files.map((e) => {
      "name": e.name,
      "tmpFolderUpload": e.tmpFolderUpload,
      "size": e.size,
    }).toList();
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_EXERCISES, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> updateExerciseFile(exerciseId,title,deadline,description,typeExercise,subjectId,teacherId,classId,List<ResponseFileUpload> files,scoreOfExercise) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["title"] = title;
    param["deadline"] = deadline;
    param["description"] = description;
    param["typeExercise"] = typeExercise;
    param["subjectId"] = subjectId;
    param["teacherId"] = teacherId;
    param["classId"] = classId;
    param["scoreOfExercise"] = scoreOfExercise;
    param["filesUpload"] = files.map((e) => {
      "name": e.name,
      "tmpFolderUpload": e.tmpFolderUpload,
      "size": e.size,
    }).toList();
    var urlApi = "${ApiConstants.API_EXERCISES}/$exerciseId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> createExerciseLink(title,deadline,description,typeExercise,subjectId,teacherId,classId,link,scoreOfExercise) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["title"] = title;
    param["deadline"] = deadline;
    param["description"] = description;
    param["typeExercise"] = typeExercise;
    param["subjectId"] = subjectId;
    param["teacherId"] = teacherId;
    param["classId"] = classId;
    param["link"] = link;
    param["scoreOfExercise"] = scoreOfExercise;
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_EXERCISES, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> updateExerciseLink(exerciseId,title,deadline,description,typeExercise,subjectId,teacherId,classId,link,scoreOfExercise) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["title"] = title;
    param["deadline"] = deadline;
    param["description"] = description;
    param["typeExercise"] = typeExercise;
    param["subjectId"] = subjectId;
    param["teacherId"] = teacherId;
    param["classId"] = classId;
    param["link"] = link;
    param["scoreOfExercise"] = scoreOfExercise;
    var urlApi = "${ApiConstants.API_EXERCISES}/$exerciseId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<ExerciseStudent>> listExerciseTeacher(idSubject,status, fromDate,toDate,isPublic) async {
    ResponseData<ExerciseStudent> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_NONE}exercises/subjects/$idSubject?status=$status&fromDate=$fromDate&toDate=$toDate&isPublic=$isPublic";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      ExerciseStudent dataRes = ExerciseStudent.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> publicExercise(exerciseId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_EXERCISES}/public-exercise/$exerciseId";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> deleteExercise(exerciseId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_EXERCISES}/$exerciseId";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.DELETE);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }




  Future<ResponseData<DetailExercise>> detailExercise(exerciseId) async {
    ResponseData<DetailExercise> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_EXERCISES}/$exerciseId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      DetailExercise dataRes = DetailExercise.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<ListStudentSubmitted>> studentsSubmitted(exerciseId) async {
    ResponseData<ListStudentSubmitted> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_EXERCISES}/students-submitted/$exerciseId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      ListStudentSubmitted dataRes = ListStudentSubmitted.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<StudentDoExercise>> detailStudentDoExercise(exerciseId,studentId) async {
    ResponseData<StudentDoExercise> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_EXERCISES}/$exerciseId/students/$studentId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      StudentDoExercise dataRes = StudentDoExercise.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> teacherGrading(idExercise,idStudent,teacherComment,scoreExercise) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["teacherComment"] = teacherComment;
    param["scoreExercise"] = scoreExercise;
    var urlApi = "${ApiConstants.API_EXERCISES}/grading/$idExercise/students/$idStudent";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> teacherGradingQuestion(idExercise,idStudent,teacherComment,scoreExercise,List<TeacherCommentAnswer> teacherCommentAnswer) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["teacherComment"] = teacherComment;
    param["scoreExercise"] = scoreExercise;
    param["questionAnswer"] = teacherCommentAnswer.map((e) => {
      "questionAnswerId": e.questionAnswerId,
      "point": e.point,
      "teacherComment": e.teacherComment,
    }).toList();
    var urlApi = "${ApiConstants.API_EXERCISES}/grading/$idExercise/students/$idStudent";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> teacherPublicScore(idExercise,List<String> studentId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["studentId"] = studentId;
    var urlApi = "${ApiConstants.API_EXERCISES}/public-score/$idExercise";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }




}