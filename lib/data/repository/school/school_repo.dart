import 'package:slova_lms/commom/constants/api_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';

import '../../base_service/api_service.dart';
import '../../base_service/respone_data.dart';
import '../../model/base_model.dart';
import '../../model/res/school/school.dart';

class SchoolRepo {

  Future<ResponseData<School>> getListSchoolLevel(idSchool) async {
    ResponseData<School> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_SCHOOL}/$idSchool";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      School dataRes = School.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


}