import 'package:slova_lms/commom/constants/api_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/base_service/api_service.dart';
import 'package:slova_lms/data/base_service/respone_data.dart';
import 'package:slova_lms/data/model/base_model.dart';
import '../../../commom/app_cache.dart';
import '../../model/common/school_year.dart';


class SchoolYearRepo {
  Future<ResponseData<List<SchoolYear>>> getListSchoolYearByUser() async {
    ResponseData<List<SchoolYear>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_SCHOOL_ID_By_User, request: param).request(Request.GET);
    if (apiResponse.status == Status.SUCCESS) {
      List<SchoolYear> items = <SchoolYear>[];
      apiResponse.data?.data.forEach((v) {
        items.add(SchoolYear.fromJson(v));
      });
      if (items.isNotEmpty) {
        var schoolYearId = items[0].id ?? "";
        AppCache().setSchoolYearId(schoolYearId);
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      } else {
        var schoolYearId = "";
        AppCache().setSchoolYearId(schoolYearId);
      }
  }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<List<SchoolYear>>> getListSchoolYearByStudent(userId) async {
    ResponseData<List<SchoolYear>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_SCHOOL_ID_By_User}?userId=$userId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param).request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<SchoolYear> items = <SchoolYear>[];
      apiResponse.data?.data.forEach((v) {
        items.add(SchoolYear.fromJson(v));
      });
      if (items.isNotEmpty) {
        var schoolYearId = items[0].id ?? "";
        AppCache().setSchoolYearId(schoolYearId);
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      } else {
        var schoolYearId = "";
        AppCache().setSchoolYearId(schoolYearId);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

}
