import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:slova_lms/data/model/res/transcript/statistical_transcript.dart';
import '../../../commom/constants/api_constant.dart';
import '../../../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_transcripts_teacher/detail_transcript_teacher/detail_transcript_teacher_controller.dart';
import '../../../view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_transcripts_teacher/update_transcript_subject/update_transcript_teacher_controller.dart';
import '../../base_service/api_response.dart';
import '../../base_service/api_service.dart';
import '../../base_service/respone_data.dart';
import '../../model/base_model.dart';
import '../../model/res/transcript/detail_transcript.dart';
import '../../model/res/transcript/semester.dart';
import '../../model/res/transcript/transcript.dart';
import 'package:get/get.dart';
class TranscriptsRepo{
  Future<ResponseData<List<Transcript>>> getListTranscript(subjectId,isPublic,semesterId,fromDate,toDate,type,classId,userId) async {
    ResponseData<List<Transcript>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_TRANSCRIPTS}?subjectId=$subjectId&isPublic=$isPublic&fromDate=$fromDate&toDate=$toDate&semesterId=$semesterId&sort=createdAt:desc&type=$type&classId=$classId&userId=$userId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param).request(Request.GET);
    if (apiResponse.status == Status.SUCCESS) {
      List<Transcript> items = <Transcript>[];
      apiResponse.data?.data.forEach((v){
        items.add(Transcript.fromJson(v));
      });
      if(items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<List<Semester>>> getListSemester() async {
    ResponseData<List<Semester>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_NONE}semester-categories/list";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param).request(Request.GET);
    if (apiResponse.status == Status.SUCCESS) {
      List<Semester> items = <Semester>[];
      apiResponse.data?.data.forEach((v){
        items.add(Semester.fromJson(v));
      });
      if(items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<List<Semester>>> getListSemesterParent(userId) async {
    ResponseData<List<Semester>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_NONE}semester-categories/list?userId=$userId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param).request(Request.GET);
    if (apiResponse.status == Status.SUCCESS) {
      List<Semester> items = <Semester>[];
      apiResponse.data?.data.forEach((v){
        items.add(Semester.fromJson(v));
      });
      if(items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> publicTranscript(transcriptId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_TRANSCRIPTS}/$transcriptId/public";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param).request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> deleteTranscript(transcriptId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_TRANSCRIPTS}/$transcriptId";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.DELETE);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<DetailTranscript> detailTranscript(transcriptId) async {
    DetailTranscript dataRes = DetailTranscript();
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_TRANSCRIPTS}/$transcriptId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);
    if (apiResponse.status == Status.SUCCESS) {
       try{
         dataRes = DetailTranscript.fromJson(apiResponse.data?.data);
         Get.find<DetailTranscriptSubjectController>().detailTranscript.value = dataRes;
         Get.find<DetailTranscriptSubjectController>().listValue.value = [];
         for(int i = 0;i< Get.find<DetailTranscriptSubjectController>().detailTranscript.value.tableRows!.length;i++){
           var list = <PointTranscript>[];
           for(int j = 0;j< Get.find<DetailTranscriptSubjectController>().detailTranscript.value.tableHeaders!.length;j++){
                 list.add(PointTranscript(key: "",point: []));
                 list[j].key = Get.find<DetailTranscriptSubjectController>().detailTranscript.value.tableHeaders![j].key!;
                 list[j].point = apiResponse.data?.data['tableRows'][i]['value'][Get.find<DetailTranscriptSubjectController>().detailTranscript.value.tableHeaders![j].key!];
           }
           Get.find<DetailTranscriptSubjectController>().listValue.add(list);
         }
         Get.find<DetailTranscriptSubjectController>().listValue.refresh();
       }catch(e){
         if (kDebugMode) {
           print('13');
         }
       }
    }

    return dataRes;
  }
  Future<DetailTranscript> transcriptInScreenUpdate(transcriptId) async {
    DetailTranscript dataRes = DetailTranscript();
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_TRANSCRIPTS}/$transcriptId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);
    if (apiResponse.status == Status.SUCCESS) {
      try{
        dataRes = DetailTranscript.fromJson(apiResponse.data?.data);
        Get.find<UpdateTranscriptTeacherController>().detailTranscript.value = dataRes;
        Get.find<UpdateTranscriptTeacherController>().listValue.value = [];
        for(int i = 0;i< Get.find<UpdateTranscriptTeacherController>().detailTranscript.value.tableRows!.length;i++){
          var list = <PointTranscript>[];
          for(int j = 0;j<  Get.find<UpdateTranscriptTeacherController>().detailTranscript.value.tableHeaders!.length;j++){
            list.add(PointTranscript(key: "",point: []));
            list[j].key = Get.find<UpdateTranscriptTeacherController>().detailTranscript.value.tableHeaders![j].key!;
            list[j].point = apiResponse.data?.data['tableRows'][i]['value'][Get.find<UpdateTranscriptTeacherController>().detailTranscript.value.tableHeaders![j].key!];
          }
          Get.find<UpdateTranscriptTeacherController>().listValue.add(list);
        }
        Get.find<UpdateTranscriptTeacherController>().listValue.refresh();
      }catch(e){
        if (kDebugMode) {
          print('13');
        }
      }
    }
    return dataRes;
  }

  Future<ResponseData<BaseModel>> updateTranscript(transcriptId,semesterId,DetailTranscript detailTranscript,List<dynamic> listValue) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    var maps = <Map>[];
    var list =  <dynamic>[];
    for(int i = 0;i<listValue.length;i++){
      maps.add(Map());
      for(int j = 0;j<listValue[i].length;j++){
        maps[i]["${listValue[i][j].key}"] = listValue[i][j].point;
      }
      list.add(maps[i]);
    }
    param["tableHeaders"] = detailTranscript.tableHeaders?.map((e) => {
      "status": e.status,
      "key": e.key,
      "title": e.title,
    }).toList();
    param["semesterId"] = detailTranscript.semester?.id;
    param["tableRows"] = detailTranscript.tableRows!.mapIndexed((i,e) => {
        "student" : e.student,
        "value" : list[i]
    }).toList();
    var urlApi = "${ApiConstants.API_TRANSCRIPTS}/$transcriptId";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<StatisticalTranscript>> getTopStudentTranscript(page,size,blockId,classId,semesterId,numberTop,levelId) async {
    ResponseData<StatisticalTranscript> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_TRANSCRIPTS}/statistical"
        "?page=$page&size=$size&blockId=$blockId"
        "&classId=$classId&semesterId=$semesterId&numberTop=$numberTop&levelId=$levelId&type=SYNTHETIC";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      StatisticalTranscript dataRes = StatisticalTranscript.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<StatisticalTranscript>> getListStudentTranscriptStatisticalPercent(page,size,blockId,classId,semesterId,levelId,startScore,endScore) async {
    ResponseData<StatisticalTranscript> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_TRANSCRIPTS}/student-statistical"
        "?page=$page&size=$size&blockId=$blockId"
        "&classId=$classId&semesterId=$semesterId&levelId=$levelId&startScore=$startScore&endScore=$endScore&type=SYNTHETIC";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      StatisticalTranscript dataRes = StatisticalTranscript.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<List<StatisticalTranscriptPercent>>> getTranscriptSchoolPercent(levelId,semesterId,blockId,classId,) async {
    ResponseData<List<StatisticalTranscriptPercent>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_TRANSCRIPTS}/statistical-all-school?levelId=$levelId&blockId=$blockId"
        "&classId=$classId&semesterId=$semesterId&type=SYNTHETIC";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<StatisticalTranscriptPercent>? items = <StatisticalTranscriptPercent>[];
      apiResponse.data?.data.forEach((v){
        items.add(StatisticalTranscriptPercent.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

}