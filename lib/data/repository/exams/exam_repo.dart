

import '../../../commom/constants/api_constant.dart';
import '../../base_service/api_response.dart';
import '../../base_service/api_service.dart';
import '../../base_service/respone_data.dart';
import '../../model/base_model.dart';
import '../../model/common/learning_managerment.dart';
import '../../model/res/exams/detail_exam.dart';
import '../../model/res/exams/student_do_exam.dart';
import '../../model/res/exercise/exercise.dart';
import '../../model/res/exercise/list_student_exercise.dart';
import '../../model/res/exercise/teacherCommentAnswer.dart';
import '../../model/res/file/response_file.dart';

class ExamRepo{

  Future<ResponseData<BaseModel>> createExamQuestions(title,scoreFactor,startTime,endTime,description,typeExercise,subjectId,teacherId,classId,List<Questions> questions) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["title"] = title;
    param["scoreFactor"] = scoreFactor;
    param["startTime"] = startTime;
    param["endTime"] = endTime;
    param["description"] = description;
    param["typeExercise"] = typeExercise;
    param["subjectId"] = subjectId;
    param["teacherId"] = teacherId;
    param["classId"] = classId;
    param["questions"] = questions.map((v) => {
      "content": v.content,
      "point": v.point,
      "typeQuestion": v.typeQuestion,
      "answerOption": v.answerOption?.map((e) => {
        "key": e.key,
        "value":e.value,
        "status":e.status
      }).toList(),
      "files": v.files?.map((e) => {
        "name": e.name,
        "tmpFolderUpload":e.tmpFolderUpload,
        "size":e.size
      }).toList(),

    }).toList();
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_EXAMS, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> updateExamQuestions(exerciseId,title,scoreFactor,startTime,endTime,description,typeExercise,subjectId,teacherId,classId,List<Questions> questions) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["title"] = title;
    param["scoreFactor"] = scoreFactor;
    param["startTime"] = startTime;
    param["endTime"] = endTime;
    param["description"] = description;
    param["typeExercise"] = typeExercise;
    param["subjectId"] = subjectId;
    param["teacherId"] = teacherId;
    param["classId"] = classId;
    param["questions"] = questions.map((v) => {
      "content": v.content,
      "point": v.point,
      "typeQuestion": v.typeQuestion,
      "answerOption": v.answerOption?.map((e) => {
        "key": e.key,
        "value":e.value,
        "status":e.status
      }).toList(),
      "files": v.files?.map((e) => {
        "name": e.name,
        "tmpFolderUpload":e.tmpFolderUpload,
        "size":e.size
      }).toList(),

    }).toList();
    var urlApi = "${ApiConstants.API_EXAMS}/$exerciseId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> createExamFile(title,startTime,endTime,description,typeExercise,subjectId,teacherId,classId,List<ResponseFileUpload> files,scoreOfExam) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["title"] = title;
    param["startTime"] = startTime;
    param["endTime"] = endTime;
    param["description"] = description;
    param["typeExercise"] = typeExercise;
    param["subjectId"] = subjectId;
    param["teacherId"] = teacherId;
    param["classId"] = classId;
    param["scoreOfExam"] = scoreOfExam;
    param["filesUpload"] = files.map((e) => {
      "name": e.name,
      "tmpFolderUpload": e.tmpFolderUpload,
      "size": e.size,
    }).toList();
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_EXAMS, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> updateExamFile(exerciseId,title,startTime,endTime,description,typeExercise,subjectId,teacherId,classId,List<ResponseFileUpload> files,scoreOfExam) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["title"] = title;
    param["startTime"] = startTime;
    param["endTime"] = endTime;
    param["description"] = description;
    param["typeExercise"] = typeExercise;
    param["subjectId"] = subjectId;
    param["teacherId"] = teacherId;
    param["classId"] = classId;
    param["scoreOfExam"] = scoreOfExam;
    param["filesUpload"] = files.map((e) => {
      "name": e.name,
      "tmpFolderUpload": e.tmpFolderUpload,
      "size": e.size,
    }).toList();
    var urlApi = "${ApiConstants.API_EXAMS}/$exerciseId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> createExamLink(title,startTime,endTime,description,typeExercise,subjectId,teacherId,classId,link,scoreOfExam) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["title"] = title;
    param["startTime"] = startTime;
    param["endTime"] = endTime;
    param["description"] = description;
    param["typeExercise"] = typeExercise;
    param["subjectId"] = subjectId;
    param["teacherId"] = teacherId;
    param["classId"] = classId;
    param["link"] = link;
    param["scoreOfExam"] = scoreOfExam;
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_EXAMS, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> updateExamLink(exerciseId,title,startTime,endTime,description,typeExercise,subjectId,teacherId,classId,link,scoreOfExam) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["title"] = title;
    param["startTime"] = startTime;
    param["endTime"] = endTime;
    param["description"] = description;
    param["typeExercise"] = typeExercise;
    param["subjectId"] = subjectId;
    param["teacherId"] = teacherId;
    param["classId"] = classId;
    param["link"] = link;
    param["scoreOfExam"] = scoreOfExam;
    var urlApi = "${ApiConstants.API_EXAMS}/$exerciseId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<ExamStudent>> listExamTeacher(idSubject,status, fromDate,toDate,isPublic) async {
    ResponseData<ExamStudent> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_NONE}exams/subjects/$idSubject?status=$status&fromDate=$fromDate&toDate=$toDate&isPublic=$isPublic";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      ExamStudent dataRes = ExamStudent.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> publicExam(examId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_EXAMS}/public-exam/$examId";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> deleteExam(examId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_EXAMS}/$examId";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.DELETE);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<DetailExam>> detailExam(examId) async {
    ResponseData<DetailExam> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_EXAMS}/$examId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      DetailExam dataRes = DetailExam.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<ListStudentSubmitted>> studentsSubmitted(examId) async {
    ResponseData<ListStudentSubmitted> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_EXAMS}/students-submitted/$examId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      ListStudentSubmitted dataRes = ListStudentSubmitted.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<StudentDoExam>> detailStudentDoExam(exerciseId,studentId) async {
    ResponseData<StudentDoExam> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_EXAMS}/$exerciseId/students/$studentId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      StudentDoExam dataRes = StudentDoExam.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> teacherGrading(idExercise,idStudent,teacherComment,scoreExercise) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["teacherComment"] = teacherComment;
    param["scoreExercise"] = scoreExercise;
    var urlApi = "${ApiConstants.API_EXAMS}/grading/$idExercise/students/$idStudent";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> teacherGradingQuestion(idExercise,idStudent,teacherComment,scoreExercise,List<TeacherCommentAnswer> teacherCommentAnswer) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["teacherComment"] = teacherComment;
    param["scoreExercise"] = scoreExercise;
    param["questionAnswer"] = teacherCommentAnswer.map((e) => {
      "questionAnswerId": e.questionAnswerId,
      "point": e.point,
      "teacherComment": e.teacherComment,
    }).toList();
    var urlApi = "${ApiConstants.API_EXAMS}/grading/$idExercise/students/$idStudent";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> teacherPublicScore(idExercise,List<String> studentId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["studentId"] = studentId;
    var urlApi = "${ApiConstants.API_EXAMS}/public-score/$idExercise";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



}