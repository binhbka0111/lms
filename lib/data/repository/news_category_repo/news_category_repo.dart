import 'package:slova_lms/commom/constants/api_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/base_service/api_service.dart';
import 'package:slova_lms/data/base_service/respone_data.dart';
import 'package:slova_lms/data/model/base_model.dart';
import 'package:slova_lms/data/model/common/news_category.dart';

class NewsCategoryRepo {
  /// Method get category
  Future<ResponseData<List<EventNewsCategory>>> getNewsCategory() async {
    ResponseData<List<EventNewsCategory>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    ApiResponse<BaseModel> apiResponse = await ApiService(ApiConstants.API_EVENT_NEWS_CATEGORY,
            request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<EventNewsCategory> listItem = <EventNewsCategory>[];
      apiResponse.data?.data.forEach((element) {
        listItem.add(EventNewsCategory.fromJson(element));
      });

      if (listItem.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: listItem);
      } else {
        streamEvent = ResponseData(
            state: Status.ERROR,
            errorCode: apiResponse.error,
            message: "Không có dữ liệu danh mục tin tức sự kiện!");
      }
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }

    return streamEvent;
  }
}
