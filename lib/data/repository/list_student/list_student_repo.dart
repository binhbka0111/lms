import 'package:slova_lms/commom/constants/api_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/base_service/api_service.dart';
import 'package:slova_lms/data/base_service/respone_data.dart';
import 'package:slova_lms/data/model/base_model.dart';

import '../../model/common/list_student.dart';


class ListStudentRepo {
  Future<ResponseData<ListStudent>> detailListStudent() async {
    ResponseData<ListStudent> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_LIST_STUDENT, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      ListStudent dataRes = ListStudent.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<List<DetailItem>>> listStudentByClass(classId) async {
    ResponseData<List<DetailItem>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.API_LIST_STUDENT_BY_TEACHER}/$classId/students";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<DetailItem> items = <DetailItem>[];
      apiResponse.data?.data.forEach((v){
        items.add(DetailItem.fromJson(v));
      });
      if(items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      } else {
        streamEvent = ResponseData(
            state: Status.ERROR,
            errorCode: apiResponse.error,
            message: "Không có dữ liệu Lớp học");
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

}