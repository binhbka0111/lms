import 'dart:io';

import 'package:dio/dio.dart';
import 'package:path/path.dart';
import 'package:slova_lms/commom/constants/api_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/base_service/api_service.dart';
import 'package:slova_lms/data/base_service/respone_data.dart';
import 'package:slova_lms/data/model/base_model.dart';
import 'package:slova_lms/data/model/common/learning_managerment.dart';
import 'package:slova_lms/data/model/res/file/response_file.dart';

class FileRepo{

  Future<ResponseData<List<ResponseFileUpload>>> uploadFileAvatar(
      List<File>? files) async {
    ResponseData<List<ResponseFileUpload>> streamEvent =
    ResponseData(state: Status.LOADING);
    var multipartFiles = <MultipartFile>[];

    for (int i = 0; i < files!.length; i++) {
      var fileMultiPath = MultipartFile.fromFileSync(files[i].path,
          filename: basename(files[i].path));
      multipartFiles.add(fileMultiPath);
    }

    var formData = FormData.fromMap({'files': multipartFiles});

    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_UPLOAD_MULTI_FILE_AVATAR, fromData: formData)
        .request(Request.POST);
    if (apiResponse.status == Status.SUCCESS) {
      List<ResponseFileUpload> data = [];
      apiResponse.data?.data.map((e) {
        data.add(ResponseFileUpload.fromJson(e));
      }).toList();
      streamEvent = ResponseData(state: Status.SUCCESS, object: data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<List<ResponseFileUpload>>> uploadFile(
      List<File>? files) async {
    ResponseData<List<ResponseFileUpload>> streamEvent =
    ResponseData(state: Status.LOADING);
    var multipartFiles = <MultipartFile>[];

    for (int i = 0; i < files!.length; i++) {
      var fileMultiPath = MultipartFile.fromFileSync(files[i].path,
          filename: basename(files[i].path));
      multipartFiles.add(fileMultiPath);
    }

    var formData = FormData.fromMap({'files': multipartFiles});

    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_UPLOAD_MULTI_FILE, fromData: formData)
        .request(Request.POST);
    if (apiResponse.status == Status.SUCCESS) {
      List<ResponseFileUpload> data = [];
      apiResponse.data?.data.map((e) {
        data.add(ResponseFileUpload.fromJson(e));
      }).toList();
      streamEvent = ResponseData(state: Status.SUCCESS, object: data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<List<FilesExercise>>> uploadFileExercise(
      List<File>? files) async {
    ResponseData<List<FilesExercise>> streamEvent =
    ResponseData(state: Status.LOADING);
    var multipartFiles = <MultipartFile>[];

    for (int i = 0; i < files!.length; i++) {
      var fileMultiPath = MultipartFile.fromFileSync(files[i].path,
          filename: basename(files[i].path));
      multipartFiles.add(fileMultiPath);
    }

    var formData = FormData.fromMap({'files': multipartFiles});

    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_UPLOAD_MULTI_FILE_EX, fromData: formData)
        .request(Request.POST);
    if (apiResponse.status == Status.SUCCESS) {
      List<FilesExercise> data = [];
      apiResponse.data?.data.map((e) {
        data.add(FilesExercise.fromJson(e));
      }).toList();
      streamEvent = ResponseData(state: Status.SUCCESS, object: data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
}