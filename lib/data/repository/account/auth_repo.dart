import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/constants/api_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/base_service/api_service.dart';
import 'package:slova_lms/data/base_service/respone_data.dart';
import 'package:slova_lms/data/model/base_model.dart';
import 'package:slova_lms/data/model/res/auth/LoginResponse.dart';
import 'package:slova_lms/data/model/res/auth/OtpResponse.dart';
import 'package:slova_lms/view/mobile/account/login/login_controller.dart';
import '../../model/common/encrypt_password.dart';

class AuthRepo {
  Future<ResponseData<LoginResponse>> requestToken(
      String username, String password, isRememberPassword, fcmToken) async {
    ResponseData<LoginResponse> streamEvent =
        ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["isRememberPassword"] = isRememberPassword;
    param["fcmToken"] = fcmToken;
    param["password"] = password;
    param["username"] = username;
    ApiResponse<BaseModel> apiResponse =
        await ApiService(ApiConstants.API_LOGIN, request: param)
            .request(Request.POST) ;

    if (apiResponse.status == Status.SUCCESS) {
      LoginResponse dataRes = LoginResponse.fromJson(apiResponse.data?.data);
      var token = dataRes.accessToken ?? "";
      AppCache().setToken(token);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
      if(Get.isRegistered<LoginController>()){
        Get.find<LoginController>().statusCode = apiResponse.error!.code!;
      }

    }
    return streamEvent;
  }

  Future<ResponseData<OTPResponse>> requestOTP(String username) async {
    ResponseData<OTPResponse> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["username"] = username;
    ApiResponse<BaseModel> apiResponse =
        await ApiService(ApiConstants.API_SEND_OTP, request: param, )
            .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      OTPResponse dataRes = OTPResponse.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> verityRespone(
      String transId, String otp) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["transId"] = transId;
    param["otp"] = otp;
    ApiResponse<BaseModel> apiResponse =
        await ApiService(ApiConstants.API_VERIFY_OTP, request: param)
            .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> changePassRespone(
      String transId, String newPassword) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};
    param["transId"] = transId;
    param["newPassword"] = newPassword;
    ApiResponse<BaseModel> apiResponse =
        await ApiService(ApiConstants.API_CHANGE_PASS, request: param)
            .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> updatePassResponse(
      String oldPassword, String newPassword, userId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["oldPassword"] = oldPassword;
    param["newPassword"] = newPassword;

    var apiUpdatePassword = "${ApiConstants.API_BASE_USER}$userId/updatePassword";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUpdatePassword, request: param)
        .request(Request.PUT);
    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }

    return streamEvent;
  }

  Future<ResponseData<BaseModel>> logout() async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    ApiResponse<BaseModel> apiResponse =
        await ApiService(ApiConstants.API_LOGOUT, request: param)
            .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<cryptoSecret>> getSecret() async {
    ResponseData<cryptoSecret> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_CRPYPTO_SECRET, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      cryptoSecret dataRes = cryptoSecret.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }





// Future<ResponseData<CustomerModel>> getInfoCustomer() async {
//   ResponseData<CustomerModel> streamEvent;
//   ApiResponse<BaseModel> apiResponse =
//   await ApiService(ApiConstants.API_GET_INFO_CUS).request(Request.POST);
//   if (apiResponse != null) {
//     if (apiResponse.status == Status.SUCCESS) {
//       try{
//         CustomerModel customerModel = CustomerModel.fromJson(apiResponse.data.data);
//         AppCache().setCustomerInfo(customerModel);
//         return ResponseData(state: Status.SUCCESS, object: customerModel);
//       }catch (e) {
//   return ResponseData(state: Status.ERROR, errorCode: apiResponse.error,message:e.toString());
//
//   }
//
//     }
//     if (apiResponse.status == Status.ERROR) {
//       return ResponseData(state: Status.ERROR, errorCode: apiResponse.error,message: apiResponse.error.errorMsg);
//
//     }
//   }
//
//   return streamEvent;
// }

}
