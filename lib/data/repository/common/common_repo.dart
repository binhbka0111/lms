import 'package:slova_lms/commom/constants/api_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/base_service/api_service.dart';
import 'package:slova_lms/data/base_service/respone_data.dart';
import 'package:slova_lms/data/model/base_model.dart';

import '../../model/common/banner.dart';
import '../../model/common/static_page.dart';

class CommonRepo {
  Future<ResponseData<BannerHomePage>> getBanner() async {
    ResponseData<BannerHomePage> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_BANNER, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      BannerHomePage dataRes = BannerHomePage.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<ItemsStaticPage>> getListStaticPage(type) async {
    ResponseData<ItemsStaticPage> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_STATIC_PAGES}$type";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      ItemsStaticPage dataRes = ItemsStaticPage.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

}
