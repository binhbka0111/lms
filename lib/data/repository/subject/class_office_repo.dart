import 'package:slova_lms/commom/constants/api_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/base_service/api_service.dart';
import 'package:slova_lms/data/base_service/respone_data.dart';
import 'package:slova_lms/data/model/base_model.dart';
import '../../model/common/subject.dart';
import '../../model/res/class/block.dart';
import '../../model/res/class/classTeacher.dart';
import '../../model/res/school/school.dart';

class ClassOfficeRepo {
  Future<ResponseData<List<SubjectRes>>> listSubject(classId) async {
    ResponseData<List<SubjectRes>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_LIST_STUDENT_BY_TEACHER}/$classId/subjects";
    ApiResponse<BaseModel> apiResponse =
        await ApiService(apiUrl, request: param)
            .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<SubjectRes> items = <SubjectRes>[];
      apiResponse.data?.data.forEach((v){
        items.add(SubjectRes.fromJson(v));
      });
      if(items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }




  Future<ResponseData<List<SubjectRes>>> listSubjectTeacher(classId) async {
    ResponseData<List<SubjectRes>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_LIST_STUDENT_BY_TEACHER}/$classId/subjectsByTeacher";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<SubjectRes> items = <SubjectRes>[];
      apiResponse.data?.data.forEach((v){
        items.add(SubjectRes.fromJson(v));
      });
      if(items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<DetailSubject>> detailSubject(idSubject) async {
    ResponseData<DetailSubject> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_NONE}subjects/$idSubject";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      DetailSubject dataRes = DetailSubject.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<ShowNotification>> notificationSubject(idSubject,idClass) async {
    ResponseData<ShowNotification> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_NONE}subject-notifications?classId=$idClass&subjectId=$idSubject";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      ShowNotification dataRes = ShowNotification.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<ShowNotification>> notificationSubjectNotPin(idSubject, idClass) async {
    ResponseData<ShowNotification> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_NONE}subject-notifications?classId=$idClass&subjectId=$idSubject";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      ShowNotification dataRes = ShowNotification.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<DetailNotification>> detailNotificationSubject(idNotification) async {
    ResponseData<DetailNotification> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_NONE}subject-notifications/$idNotification";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      DetailNotification dataRes = DetailNotification.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<List<ClassId>>> listClassByTeacher(teacherId) async {
    ResponseData<List<ClassId>> streamEvent =
        ResponseData(state: Status.LOADING);

    var param = <String, dynamic>{};

    var apiURL = "${ApiConstants.API_LIST_CLASS_BY_USER}/class";

    ApiResponse<BaseModel> apiResponse =
        await ApiService(apiURL, request: param)
            .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<ClassId>? items = <ClassId>[];
      apiResponse.data?.data.forEach((v){
        items.add(ClassId.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<List<ClassId>>> listClassByParent(userId) async {
    ResponseData<List<ClassId>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_LIST_CLASS_BY_USER}/class?userId=$userId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<ClassId>? items = <ClassId>[];
      apiResponse.data?.data.forEach((v){
        items.add(ClassId.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<List<ClassId>>> listClassByStudent() async {
    ResponseData<List<ClassId>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_LIST_CLASS_BY_USER}/class";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<ClassId>? items = <ClassId>[];
      apiResponse.data?.data.forEach((v){
        items.add(ClassId.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<List<Block>>> listBlock(schoolYearId) async {
    ResponseData<List<Block>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}school-years/$schoolYearId/blocks";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<Block>? items = <Block>[];
      apiResponse.data?.data.forEach((v){
        items.add(Block.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<List<Block>>> listBlockBySchoolLevel(schoolYearId,levelId) async {
    ResponseData<List<Block>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}school-years/$schoolYearId/blocks?levelId=$levelId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<Block>? items = <Block>[];
      apiResponse.data?.data.forEach((v){
        items.add(Block.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<List<Class>>> listClassInBlock(schoolYearId,blockId) async {
    ResponseData<List<Class>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}blocks/$blockId/classes";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<Class>? items = <Class>[];
      apiResponse.data?.data.forEach((v){
        items.add(Class.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<List<ClassBySchoolLevel>>> listClassInBlockBySchoolLevel(schoolYearId,blockId,levelId) async {
    ResponseData<List<ClassBySchoolLevel>> streamEvent =
    ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_LIST_CLASS_BY_USER}/class?blockId=$blockId&levelId=$levelId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {

      List<ClassBySchoolLevel>? items = <ClassBySchoolLevel>[];
      apiResponse.data?.data.forEach((v){
        items.add(ClassBySchoolLevel.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
}
