
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import '../../../commom/constants/api_constant.dart';
import '../../base_service/api_response.dart';
import '../../base_service/api_service.dart';
import '../../base_service/respone_data.dart';
import '../../model/base_model.dart';
import '../../model/common/detail_notify_leaving_application.dart';
import '../../model/res/notification/notification.dart';
import '../../model/res/file/response_file.dart';
import '../../model/res/notification/notification_subject.dart';
import '../../model/res/notification/proactive_notifications.dart';

class NotificationRepo{
  Future<ResponseData<Notify>> getListNotify(classId,page,size) async {
    ResponseData<Notify> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_NOTIFICATION}?classId=$classId&page=$page&size=$size";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      Notify dataRes = Notify.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<TotalNotifiNotSeen>> getTotalNotifyNotSeen() async {
    ResponseData<TotalNotifiNotSeen> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_NOTIFICATION}/total-not-seen";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      TotalNotifiNotSeen dataRes = TotalNotifiNotSeen.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> seenNotification(RxList<String> listId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["Ids"] = listId.reduce((value, element) => '$value,$element');
    var apiUrl = ApiConstants.API_NOTIFICATION;

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> deleteNotification( RxList<String> listId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["Ids"] = listId.reduce((value, element) => '$value,$element');
    var apiUrl = ApiConstants.API_NOTIFICATION;
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.DELETE);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<DetailNotifyLeavingApplication>> getDetailNotifyLeavingApplication(transId) async {
    ResponseData<DetailNotifyLeavingApplication> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_LEAVING_APPLICATIONS}/$transId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      DetailNotifyLeavingApplication dataRes = DetailNotifyLeavingApplication.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> sendNotification(title,body,typeNotify,classId, List<String> listId,List<ResponseFileUpload> files) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["title"] = title;
    param["body"] = body;
    param["typeNotify"] = typeNotify;
    param["classId"] = classId;
    param["files"] = files.map((e) => {
      "name": e.name,
      "tmpFolderUpload": e.tmpFolderUpload,
      "size": e.size,
    }).toList();
    param["userIds"] = listId.reduce((value, element) => '$value,$element');
    var apiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}proactive-notifications";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> sendNotificationSubject(title,content,status,subjectId,List<ResponseFileUpload> files) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["title"] = title;
    param["content"] = content;
    param["status"] = status;
    param["subjectId"] = subjectId;
    param["files"] = files.map((e) => {
      "name": e.name,
      "tmpFolderUpload": e.tmpFolderUpload,
      "size": e.size,
    }).toList();
    var apiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}subject-notifications";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> updateNotificationSubject(notifyId,title,content,status,subjectId,List<ResponseFileUpload> files) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["title"] = title;
    param["content"] = content;
    param["status"] = status;
    param["subjectId"] = subjectId;
    param["files"] = files.map((e) => {
      "name": e.name,
      "tmpFolderUpload": e.tmpFolderUpload,
      "size": e.size,
    }).toList();
    var apiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}subject-notifications/$notifyId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<NotificationSubject>> getListNotifySubject(subjectId) async {
    ResponseData<NotificationSubject> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}subject-notifications?subjectId=$subjectId&sort=status:desc,createdAt:desc";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      NotificationSubject dataRes = NotificationSubject.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> pinNotifySubject(notifyId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}subject-notifications/$notifyId/pin";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<ProactiveNotify>> getDetailNotifyOther(transId) async {
    ResponseData<ProactiveNotify> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_NONE}proactive-notifications/$transId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      ProactiveNotify dataRes = ProactiveNotify.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

}