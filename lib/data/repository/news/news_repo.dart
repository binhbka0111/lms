import 'package:slova_lms/commom/constants/api_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/base_service/api_service.dart';
import 'package:slova_lms/data/base_service/respone_data.dart';
import 'package:slova_lms/data/model/base_model.dart';
import 'package:slova_lms/data/model/common/news_model.dart';

class NewsRepo {
  /// Method get list item by type
  Future<ResponseData<List<Item>>> getEventNews(
      int page, int size, String schoolId, String categoryId) async {
    ResponseData<List<Item>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    ApiResponse<BaseModel> apiResponse = await ApiService(
            '${ApiConstants.API_EVENT_NEWS}?page=$page&size=$size&schoolId=$schoolId&newsCategoryId=$categoryId',
            request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<Item> listItem = <Item>[];
      apiResponse.data?.data['items'].forEach((element) {
        listItem.add(Item.fromJson(element));
      });
      if (listItem.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: listItem);
      } else {
        streamEvent = ResponseData(
            state: Status.ERROR,
            errorCode: apiResponse.error,
            message: "Không có dữ liệu tin tức sự kiện");
      }
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }

    return streamEvent;
  }

  /// Method get detail item by id
  Future<ResponseData<Item>> getDetailItem(String id) async {
    ResponseData<Item> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    ApiResponse<BaseModel> apiResponse =
        await ApiService('${ApiConstants.API_EVENT_NEWS}/$id', request: param)
            .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      Item item = Item.fromJson(apiResponse.data?.data);

      streamEvent = ResponseData(state: Status.SUCCESS, object: item);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }

    return streamEvent;
  }
}
