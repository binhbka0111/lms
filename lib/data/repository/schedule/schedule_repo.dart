
import 'package:slova_lms/commom/constants/api_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/base_service/api_service.dart';
import 'package:slova_lms/data/base_service/respone_data.dart';
import 'package:slova_lms/data/model/base_model.dart';

import '../../model/common/schedule.dart';

class ScheduleRepo {
  Future<ResponseData<List<Schedule>>> getScheduleStudent(userId) async {
    ResponseData<List<Schedule>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}students/$userId/timetables";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<Schedule>? items = <Schedule>[];
      apiResponse.data?.data.forEach((v){
        items.add(Schedule.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<List<Schedule>>> getScheduleByClass(classId) async {
    ResponseData<List<Schedule>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}classes/$classId/timetables";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<Schedule>? items = <Schedule>[];
      apiResponse.data?.data.forEach((v){
        items.add(Schedule.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    return streamEvent;
  }

  Future<ResponseData<List<Schedule>>> getScheduleClassByTeacher(teacherId,classId) async {
    ResponseData<List<Schedule>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}teachers/$teacherId/timetables?classId=$classId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<Schedule>? items = <Schedule>[];
      apiResponse.data?.data.forEach((v){
        items.add(Schedule.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    return streamEvent;
  }

  Future<ResponseData<List<Schedule>>> getScheduleTeacher(idTeacher) async {
    ResponseData<List<Schedule>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.BASE_URL}${ApiConstants.ENDPOINT_URL}teachers/$idTeacher/timetables";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<Schedule>? items = <Schedule>[];
      apiResponse.data?.data.forEach((v){
        items.add(Schedule.fromJson(v));
      });

      if (items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
}