import 'package:slova_lms/commom/constants/api_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/base_service/api_service.dart';
import 'package:slova_lms/data/base_service/respone_data.dart';
import 'package:slova_lms/data/model/base_model.dart';

import '../../../commom/app_cache.dart';
import '../../model/common/contacts.dart';

class ContactRepo {
  Future<ResponseData<Contacts>> getContact(type) async {
    ResponseData<Contacts> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.API_LIST_CLASS_BY_USER}?type=$type";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      Contacts dataRes = Contacts.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<Contacts>> getContactParent(type,userId) async {
    ResponseData<Contacts> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.API_LIST_CLASS_BY_USER}?type=$type&userId=$userId";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      Contacts dataRes = Contacts.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<Contacts>> getClassContact(classId,type,isNotify) async {
    ResponseData<Contacts> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_LIST_CLASS_BY_USER}?classId=$classId&type=$type&isNotify=$isNotify";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      Contacts dataRes = Contacts.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<Contacts>> fillterClassContact(classId,type,fullName) async {
    ResponseData<Contacts> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_LIST_CLASS_BY_USER}?classId=$classId&type=$type&fullName=$fullName";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      Contacts dataRes = Contacts.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<Contacts>> getClassContactParent(classId,type,userId,isNotify) async {
    ResponseData<Contacts> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    String apiUrl;
    if(AppCache().userType == "PARENT"){
      apiUrl = "${ApiConstants.API_LIST_CLASS_BY_USER}?classId=$classId&type=$type&userId=$userId&isNotify=$isNotify";
    }else{
      apiUrl = "${ApiConstants.API_LIST_CLASS_BY_USER}?classId=$classId&type=$type&isNotify=$isNotify";
    }
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      Contacts dataRes = Contacts.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<Contacts>> findUserByClassContact(classId,type,fullName) async {
    ResponseData<Contacts> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.API_LIST_CLASS_BY_USER}?classId=$classId&type=$type&fullName=$fullName&sort=fullName:desc";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      Contacts dataRes = Contacts.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
}
