import 'package:slova_lms/data/base_service/respone_data.dart';
import 'package:slova_lms/data/model/base_model.dart';
import 'package:slova_lms/data/model/res/file/response_file.dart';
import '../../../commom/constants/api_constant.dart';
import '../../base_service/api_response.dart';
import '../../base_service/api_service.dart';
import '../../model/common/contacts.dart';
import '../../model/res/message/detail_group_chat.dart';
import '../../model/res/message/group_message.dart';
import '../../model/res/message/message.dart';
import '../../model/res/message/user_in_group_chat.dart';

class ChatRepo {
  Future<ResponseData<TransId>> createNewGroupChat(name,type, List<String> listId) async {
    ResponseData<TransId> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["name"] = name;
    param["type"] = type;
    param["userIds"] = listId.reduce((value, element) => '$value,$element');
    ApiResponse<BaseModel> apiResponse =
    await ApiService(ApiConstants.API_CHAT_ROOM, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      TransId dataRes = TransId.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> updateNameGroupChat(name,chatRoomId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["name"] = name;
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/$chatRoomId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> deleteGroupChat(chatRoomId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/$chatRoomId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.DELETE);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> updateImageGroupChat(ResponseFileUpload file,chatRoomId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["img"] = {
      "name": file.name,
      "tmpFolderUpload": file.tmpFolderUpload
    };
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/$chatRoomId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> updateNotifyGroupChat(chatRoomId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/$chatRoomId/notify";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> addUserToGroupChat(chatRoomId,List<String> listId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["userIds"] = listId.reduce((value, element) => '$value,$element');
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/$chatRoomId/users";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> deleteUserGroupChat(chatRoomId,userId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/$chatRoomId/users/$userId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.DELETE);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> addRecentSearch(userId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    param["userId"] = userId;
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/recent-search";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> deleteUserRecentSearch(userId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/recent-search/$userId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.DELETE);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<BaseModel>> deleteAllUserRecentSearch() async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/recent-search/delete-all";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.DELETE);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<UserInGroupChat>> getListUserInGroupChat(chatRoomId,page,size) async {
    ResponseData<UserInGroupChat> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/$chatRoomId/users?page=$page&size=$size";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      UserInGroupChat dataRes = UserInGroupChat.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<UserInGroupChat>> getListUserNotAdd(chatRoomId,page,size,fullName,userId) async {
    ResponseData<UserInGroupChat> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/$chatRoomId/users-not-add?page=$page&size=$size&fullName=$fullName&userId=$userId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      UserInGroupChat dataRes = UserInGroupChat.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<UserInGroupChat>> getListUserSearch(type,page,size,fullName,userId) async {
    ResponseData<UserInGroupChat> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/users-chat?type=$type&page=$page&size=$size&fullName=$fullName&userId=$userId";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      UserInGroupChat dataRes = UserInGroupChat.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<List<ItemContact>>> getListUserSearchSuggest() async {
    ResponseData<List<ItemContact>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/users-suggest";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<ItemContact> items = <ItemContact>[];
      apiResponse.data?.data.forEach((v){
        items.add(ItemContact.fromJson(v));
      });
      if(items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<GroupMessage>> getListGroupChat(type,page,size) async {
    ResponseData<GroupMessage> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.API_CHAT_ROOM}?type=$type&page=$page&size=$size";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      GroupMessage dataRes = GroupMessage.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
  Future<ResponseData<List<ItemContact>>> getListGroupChatRecentSearch() async {
    ResponseData<List<ItemContact>> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/recent-search";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      List<ItemContact> items = <ItemContact>[];
      apiResponse.data?.data.forEach((v){
        items.add(ItemContact.fromJson(v));
      });
      if(items.isNotEmpty) {
        streamEvent = ResponseData(state: Status.SUCCESS, object: items);
      }
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<AllMessage>> getAllMessageGroupChat(chatRoomId,page,size) async {
    ResponseData<AllMessage> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/$chatRoomId/messages?page=$page&size=$size";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      AllMessage dataRes = AllMessage.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> seenMessage(chatRoomId) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/$chatRoomId/seen-messages";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.PUT);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent =
          ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<AllMessage>> findMessageReply(chatRoomId,size,parentId) async {
    ResponseData<AllMessage> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/$chatRoomId/messages?size=$size&parentId=$parentId";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      AllMessage dataRes = AllMessage.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<DetailGroupChat>> detailGroupChat(chatRoomId,page,size) async {
    ResponseData<DetailGroupChat> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/$chatRoomId?page=$page&size=$size";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      DetailGroupChat dataRes = DetailGroupChat.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<DetailGroupChat>> getIdPrivateChat(userId) async {
    ResponseData<DetailGroupChat> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};

    var apiUrl = "${ApiConstants.API_CHAT_ROOM}/messages?userId=$userId";

    ApiResponse<BaseModel> apiResponse =
    await ApiService(apiUrl, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      DetailGroupChat dataRes = DetailGroupChat.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }
    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }
}