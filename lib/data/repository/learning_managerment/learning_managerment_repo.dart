import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/api_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/base_service/api_service.dart';
import 'package:slova_lms/data/base_service/respone_data.dart';
import 'package:slova_lms/data/model/base_model.dart';
import 'package:slova_lms/data/model/common/learning_managerment.dart';
import '../../../view/mobile/role/student/detail_subject/view_exam_student/detail_exam_student/student_doing_exam/student_doing_exam_controller.dart';

class LearningManagermentRepo{

  Future<ResponseData<ExerciseStudent>> listExerciseStudent(idSubject,status, fromDate,toDate,studentId,statusEx,page, size) async {
    ResponseData<ExerciseStudent> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_NONE}exercises/subjects/$idSubject?status=$status&fromDate=$fromDate&toDate=$toDate&studentId=$studentId&statusExerciseByStudent=$statusEx&page=$page&size=$size";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      ExerciseStudent dataRes = ExerciseStudent.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

  Future<ResponseData<ExerciseStudent>> listExamStudent(idSubject,status, fromDate,toDate,studentId,statusEx,page, size) async {
    ResponseData<ExerciseStudent> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_NONE}exams/subjects/$idSubject?status=$status&fromDate=$fromDate&toDate=$toDate&studentId=$studentId&statusExerciseByStudent=$statusEx&page=$page&size=$size";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      ExerciseStudent dataRes = ExerciseStudent.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);

    }
    return streamEvent;
  }



  Future<ResponseData<DetailExerciser>> detailExerciseStudent(idExercise,idStudent) async {
    ResponseData<DetailExerciser> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_NONE}exercises/$idExercise/students/$idStudent";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      DetailExerciser dataRes = DetailExerciser.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }


  Future<ResponseData<DetailExerciser>> detailExamsStudent(idExam,idStudent) async {
    ResponseData<DetailExerciser> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{};
    var urlApi = "${ApiConstants.API_NONE}exams/$idExam/students/$idStudent";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.GET);

    if (apiResponse.status == Status.SUCCESS) {
      DetailExerciser dataRes = DetailExerciser.fromJson(apiResponse.data?.data);
      streamEvent = ResponseData(state: Status.SUCCESS, object: dataRes);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
      Get.find<StudentDoingExamController>().statusCode.value = apiResponse.error!.code!;


    }
    return streamEvent;
  }


  Future<ResponseData<BaseModel>> submitSubject(List<QuestionAnswers> questionAnswers, List<FilesExercise> files , String isSubmit, String contentAnswer, idExercise,idStudent) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{
    };
    param["questionAnswers"] = questionAnswers.map((v) => {
      "questionId": v.id,
      "answer": v.answer,
      "files": v.files?.map((e) => {
        "name": e.name,
        "tmpFolderUpload": e.tmpFolderUpload,
        "size": e.size,

      }).toList()
    }).toList();

    param["files"] = files.map((v) => {
      "name": v.name,
      "tmpFolderUpload": v.tmpFolderUpload,
      "size": v.size,
    }).toList();

    param["isSubmit"] = isSubmit;
    param["contentAnswer"] = contentAnswer;


    var urlApi = "${ApiConstants.API_NONE}exercise-answers/$idExercise/students/$idStudent";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent = ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }



  Future<ResponseData<BaseModel>> submitExam(List<QuestionAnswers> questionAnswers, List<FilesExercise> files , String isSubmit, String contentAnswer, idExams,idStudent) async {
    ResponseData<BaseModel> streamEvent = ResponseData(state: Status.LOADING);
    var param = <String, dynamic>{
    };
    param["questionAnswers"] = questionAnswers.map((v) => {
      "questionId": v.id,
      "answer": v.answer,
      "files": v.files?.map((e) => {
        "name": e.name,
        "tmpFolderUpload": e.tmpFolderUpload,
        "size": e.size,

      }).toList()
    }).toList();

    param["files"] = files.map((v) => {
      "name": v.name,
      "tmpFolderUpload": v.tmpFolderUpload,
      "size": v.size,
    }).toList();

    param["isSubmit"] = isSubmit;
    param["contentAnswer"] = contentAnswer;


    var urlApi = "${ApiConstants.API_NONE}exercise-answers/exams/$idExams/students/$idStudent";
    ApiResponse<BaseModel> apiResponse =
    await ApiService(urlApi, request: param)
        .request(Request.POST);

    if (apiResponse.status == Status.SUCCESS) {
      streamEvent = ResponseData(state: Status.SUCCESS, object: apiResponse.data);
    }

    if (apiResponse.status == Status.ERROR) {
      streamEvent = ResponseData(
          state: Status.ERROR,
          errorCode: apiResponse.error,
          message: apiResponse.error?.message);
    }
    return streamEvent;
  }

}