import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:socket_io_client/socket_io_client.dart';
import 'package:slova_lms/commom/app_cache.dart';
import '../../commom/constants/api_constant.dart';
import 'package:get/get.dart';
import '../../view/mobile/message/list_all_message/list_all_message_controller.dart';
import '../../view/mobile/message/list_message_un_read/list_message_un_read_controller.dart';
import '../../view/mobile/message/message_content/list_user_in_group_chat/detail_message_private/detail_message_private_controller.dart';
import '../../view/mobile/message/message_content/message_content_controller.dart';
import '../../view/mobile/message/message_controller.dart';
import '../model/res/file/response_file.dart';
import '../model/res/message/group_message.dart';

class SocketIo {
  IO.Socket? socket;
  initSocket() {
    socket = IO.io(ApiConstants.socketServerURL, <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
      'force new connection': true,
      'extraHeaders':{'Authorization': "Bearer ${AppCache().token}", "channel": "APP_MOBILE"}
    });

    socket?.connect();
    socket?.onConnect((_) {
      if (kDebugMode) {
        print('Đã kết nối với Socket');
      }
    });

    socket?.on('message', (data) {
      getMessage(data);
    });
    socket?.on('notify', (data) {
      getMessage(data);
    });

    socket?.on('seen', (data) {
      if(Get.isRegistered<MessageController>()&&Get.isRegistered<DetailMessagePrivateController>()== false&&Get.isRegistered<MessageContentController>()== false){
        Get.find<MessageController>().getListGroupChat("ALL",0,20);
        Get.find<ListAllMessageController>().getListGroupChat("ALL",0,20);
        Get.find<ListMessageUnReadController>().getListGroupChat("NOT_SEEN",0,20);
      }
    });

    socket?.onDisconnect((_) => print('Đã ngắt kết nối với Socket'));
    socket?.onConnectError((err) => print(err));
    socket?.onError((err) => print(err));
  }




  getMessage(data){
    var message = Message. fromJson(data);

    if(Get.isRegistered<MessageContentController>()){
      if(message.chatRoomId == Get.find<MessageContentController>().detailGroupChat.id){
        Get.find<MessageContentController>().messageList = [message,...Get.find<MessageContentController>().messageList];
        Get.find<MessageContentController>().getListParagraph();
        Get.find<MessageContentController>().seenMessage();
      }
    }
    if(Get.isRegistered<DetailMessagePrivateController>()){
      if(message.chatRoomId == Get.find<DetailMessagePrivateController>().tmpChatRoomId){
        Get.find<DetailMessagePrivateController>().messageList = [message,...Get.find<DetailMessagePrivateController>().messageList];
        Get.find<DetailMessagePrivateController>().getListParagraph();
        Get.find<DetailMessagePrivateController>().seenMessage();
      }
    }
    if(Get.isRegistered<MessageController>()&&Get.isRegistered<DetailMessagePrivateController>()== false&&Get.isRegistered<MessageContentController>()== false){
      Get.find<MessageController>().getListGroupChat("ALL",0,20);
      Get.find<ListAllMessageController>().getListGroupChat("ALL",0,20);
      Get.find<ListMessageUnReadController>().getListGroupChat("NOT_SEEN",0,20);
    }
  }


  sendMessage(message,chatRoomId,List<ResponseFileUpload> list,parentId) {
    socket?.emit("message", {
      'content': message,
      'chatRoomId': chatRoomId,
      'parentId':parentId,
      "files": list.map((e) => {
        "name": e.name,
        "tmpFolderUpload":e.tmpFolderUpload,
        "size" : e.size
      }).toList()
    });

  }

  deleteUserGroupChat(chatRoomId,userId) {
    socket?.emit("notify", {
      'chatRoomId': chatRoomId,
      "notifyType": 'DELETE_USER',
      "userIds": userId
    });
  }
  createGroupChat(chatRoomId) {
    socket?.emit("notify", {
      'chatRoomId': chatRoomId,
      "notifyType": 'CREATE_GROUP',
    });
  }

  addUserGroupChat(chatRoomId,listUserId) {
    socket?.emit("notify", {
      'chatRoomId': chatRoomId,
      "notifyType": 'CREATE_USER',
      "userIds": listUserId.reduce((value, element) => '$value,$element')
    });
  }

  seenMessage(chatRoomId) {
    socket?.emit("seen", {
      'chatRoomId': chatRoomId,
    });
  }

}


class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}