import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/loading_custom.dart';
import 'package:slova_lms/commom/widget/logout.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  var controller = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    DateTime oldTime = DateTime.now();
    DateTime newTime = DateTime.now();
    return WillPopScope(
      onWillPop: () async {
        if(controller.userGroupByApp.isNotEmpty) {
          return false;
        } else {
          newTime = DateTime.now();
          int difference = newTime
              .difference(oldTime)
              .inMilliseconds;
          oldTime = newTime;
          if (difference < 2000) {
            return true;
          } else {
            AppUtils.shared.showToast('touch_again_to_exit'.tr,
                backgroundColor: ColorUtils.COLOR_BLACK.withOpacity(0.5),
                textColor: ColorUtils.COLOR_WHITE);
            return false;
          }
        }
      },
      child: GetBuilder<HomeController>(builder: (controller) {
        if (controller.isReady) {
          if(controller.userGroupByApp.isNotEmpty) {
            return SafeArea(
                top: false,
                bottom: true,
                child: Scaffold(
                    backgroundColor: Colors.transparent,
                    body: TabBarView(
                      physics: const NeverScrollableScrollPhysics(),
                      controller: controller.tabController,
                      children: controller.listTabView,
                    ),
                    bottomNavigationBar: ClipRRect(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10.r),
                          topLeft: Radius.circular(10.r)),
                      child: Container(
                        height: 60,
                        color: Colors.white,
                        child: TabBar(
                          controller: controller.tabController,
                          physics: const NeverScrollableScrollPhysics(),
                          indicatorWeight: 3,
                          indicator: MaterialIndicator(
                            color: ColorUtils.PRIMARY_COLOR,
                            topRightRadius: 10,
                            topLeftRadius: 10,
                            paintingStyle: PaintingStyle.fill,
                          ),
                          labelColor: Colors.black,
                          unselectedLabelColor: Colors.grey,
                          indicatorPadding: const EdgeInsets.fromLTRB(4, 0, 4, 0),
                          onTap: (index) {
                            controller.setIndex(index);
                            controller.update();
                          },
                          tabs: controller.listTabBar,
                        ),
                      ),
                    )));
          } else {
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
              showDialogRole();
            });
            return Container();
          }
        } else {
          return const LoadingCustom();
        }
      } ,),
    );
  }

  void showDialogRole(){
    Get.dialog(
        barrierDismissible: false,
        WillPopScope(
          onWillPop: () async => false,
          child: Material(
            color: Colors.transparent,
            child: Center(
              child: Wrap(
                children: [
                  Container(
                    width: double
                        .infinity,
                    decoration: BoxDecoration(
                        borderRadius:
                        BorderRadius.circular(16)),
                    child:
                    Stack(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                              top: 40.h,
                              right: 50.w,
                              left: 50.w),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(16)),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(padding: EdgeInsets.only(top: 32.h)),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 16.w),
                                  child: Text(
                                    "Bạn không có quyền truy cập!",
                                    style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontSize: 14.sp),
                                  ),
                                ),
                                Container(
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16),bottomRight: Radius.circular(16)),
                                  ),
                                    padding: EdgeInsets.all(16.h),
                                    child: SizedBox(
                                      width: double.infinity,
                                      height: 46,
                                      child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                                          onPressed: () {
                                          logout();
                                        },
                                        child: Text(
                                          'Đăng xuất',
                                          style: TextStyle(color: Colors.white, fontSize: 16.sp),
                                        ),
                                      ),
                                  ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                            alignment:
                            Alignment.center,
                            margin: const EdgeInsets.only(top: 10),
                            height: 80,
                            child: Image.asset("assets/images/image_app_logo.png"),
                        ),
                      ],
                  ),
                ),
              ],
              ),
    ),
          ),
        ));
  }
}
