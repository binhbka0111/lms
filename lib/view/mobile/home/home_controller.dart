import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/action/role_action.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/view/mobile/message/list_all_message/list_all_message_controller.dart';
import 'package:slova_lms/view/mobile/role/manager/manager_home_controller.dart';
import 'package:slova_lms/view/mobile/role/manager/schedule_manager/schedule_manager_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/teacher_home_controller.dart';
import '../../../commom/utils/color_utils.dart';
import '../../../commom/utils/textstyle.dart';
import '../../../data/base_service/api_response.dart';
import '../../../data/model/common/user_group_by_app.dart';
import '../../../data/repository/notification_repo/notification_repo.dart';
import '../../../data/repository/person/personal_info_repo.dart';
import '../message/list_message_un_read/list_message_un_read_controller.dart';
import '../message/message_controller.dart';
import '../message/message_page.dart';
import '../notification/notification_controller.dart';
import '../notification/notification_page.dart';
import '../phonebook/phone_book_controller.dart';
import '../phonebook/phone_book_page.dart';
import '../schedule/schedule_controller.dart';
import 'package:badges/src/badge.dart' as badge;

class HomeController extends GetxController with GetSingleTickerProviderStateMixin  {
  TabController? tabController;

  final PersonalInfoRepo personalInfoRepo = PersonalInfoRepo();
  var userGroupByApp = <UserGroupByApp>[];
  var listTabBar = <Widget>[];
  var listTabView = <Widget>[];
  var isReady = false;
  final NotificationRepo _notificationRepo = NotificationRepo();
  var colorDashboard = ColorUtils.PRIMARY_COLOR.obs;
  Rx<Color> colorSchedule = const Color.fromRGBO(177, 177, 177, 1).obs;
  RxString imagePhoneBook = "assets/images/icon_phonebook_nofocus.png".obs;
  RxString imageNotify = "assets/images/icon_notification_nofocus.png".obs;
  RxString imageMessage = "assets/images/icon_message_nofocus.png".obs;
  Widget dashboard = RoleAction().getDashboardUIByRole(
    currentRole: AppCache().userType,
  );
  Widget schedule = RoleAction().getScheduleUIByRole(
    currentRole: AppCache().userType,
  );
  Widget phonebook = PhoneBookPage();
  Widget notify = NotificationPage();
  Widget message = MessagePage();


  @override
  void onInit() {
    getUserGroupByApp();
    getCountNotifyNotSeen();
    super.onInit();
  }

  getUserGroupByApp() async{
    await personalInfoRepo.getUserGroupByApp().then((value) async{
      if (value.state == Status.SUCCESS) {
        userGroupByApp = value.object!;
        update();
        await addListTabBar(dashboard,schedule,phonebook,notify,message);
        if(AppCache().userType == "TEACHER"){
          Get.find<TeacherHomeController>().getListClassroomManagement();
          Get.find<TeacherHomeController>().getListInformationFromSchool();
        }
        if(AppCache().userType == "MANAGER"){
          Get.find<ManagerHomeController>().getListClassroomManagement();
          Get.find<ManagerHomeController>().getListInformationFromSchool();
        }
      }
    });


    tabController = TabController(length: listTabView.length, vsync: this)..addListener(() {
      if(!tabController!.indexIsChanging){
        if(tabController?.index  != listTabView.indexOf(message)){
          if(Get.isRegistered<MessageController>()){
            Get.find<MessageController>().socketIo.socket?.disconnect();
            Get.find<MessageController>().socketIo.socket?.dispose();
          }
          imageMessage.value = "assets/images/icon_message_nofocus.png";
        }
        if(tabController?.index  == listTabView.indexOf(message)){
          Get.find<MessageController>().socketIo.initSocket();
          if(Get.isRegistered<ListMessageUnReadController>()){
            Get.find<ListMessageUnReadController>().getListGroupChat("NOT_SEEN",0,20);
          }
          if(Get.isRegistered<ListAllMessageController>()){
            Get.find<ListAllMessageController>().getListGroupChat("ALL",0,20);
          }
          if(Get.isRegistered<MessageController>()){
            Get.find<MessageController>().getListGroupChat("ALL",0,20);
          }
          imageMessage.value = "assets/images/icon_message.png";
        }
        if(tabController?.index == listTabView.indexOf(schedule)){
          if(AppCache().userType == "MANAGER"){
            Get.find<ScheduleManagerController>().getListBlock(AppCache().schoolYearId);
          }else{
            Get.find<ScheduleController>().queryTimeCalendar(AppCache().userType);
            Get.find<ScheduleController>().onSelectDateTime();
            Get.find<ScheduleController>().weekController.value.selectedDate = Get.find<ScheduleController>().selectedDay.value;
          }
          colorSchedule.value = ColorUtils.PRIMARY_COLOR;
        }
        else{
          colorSchedule.value = const Color.fromRGBO(177, 177, 177, 1);
        }
        if(tabController?.index  == listTabView.indexOf(phonebook)){
          Get.find<PhoneBookController>().isExpandParents.value = false;
          Get.find<PhoneBookController>().isExpandStudent.value = false;
          imagePhoneBook.value = "assets/images/icon_phonebook.png";
        }
        else{
          imagePhoneBook.value = "assets/images/icon_phonebook_nofocus.png";
        }
        if(tabController?.index == listTabView.indexOf(notify)){
          Get.find<NotificationController>().isChooseManyToday.value = false;
          Get.find<NotificationController>().isChooseManyBefore.value = false;
          Get.find<NotificationController>().listIdNotifyBefore.value = [];
          Get.find<NotificationController>().listIdNotifyToday.value = [];
          Get.find<NotificationController>().getListNotification();
          if(AppCache().userType == "TEACHER"){
            Get.find<NotificationController>().classOfTeacher.clear();
            Get.find<NotificationController>().getListClassIdByTeacher();
            Get.find<NotificationController>().classOfTeacher.refresh();
          }
          Get.find<NotificationController>().isAllClass.value = true;
          Get.find<NotificationController>().isAllClass.refresh();
          Get.find<NotificationController>().indexPageToday.value = 1;
          Get.find<NotificationController>().indexPageBefore.value = 1;
          Get.find<NotificationController>().getListItemPopupMenu();

          imageNotify.value = "assets/images/icon_notification.png";
        }
        else{
          imageNotify.value = "assets/images/icon_notification_nofocus.png";
        }

        if(tabController?.index == listTabView.indexOf(dashboard)){
          colorDashboard.value = ColorUtils.PRIMARY_COLOR;
        }else{
          colorDashboard.value = const Color.fromRGBO(177, 177, 177, 1);
        }
      }
      update();
    });
    isReady = true;
    if(AppCache().isClickNotifyWhenKillApp == true){
      Get.find<HomeController>().comeNotification();
      AppCache().setIsClickNotifyWhenKillApp(false);
    }
  }



  addListTabBar(dashboard,schedule,phonebook,notify,message){
    listTabBar = [];
    listTabView = [];
    for(int i = 0 ; i<userGroupByApp.length;i++){
      if(userGroupByApp[i].page?.code == StringConstant.PAGE_HOME){
        listTabView.add(dashboard);
        listTabBar.add( Obx(() => Tab(
          icon: Image.asset(
            "assets/images/icon_dasbroads.png",
            height: 18.h,
            width: 18.h,
            color: colorDashboard.value,
          ),
        )));
      }
      if(userGroupByApp[i].page?.code == StringConstant.PAGE_LESSON){
        listTabView.add(schedule);
        listTabBar.add( Obx(() => Tab(
            icon: Image.asset(
              width: 18.h,
              height: 18.h,
              "assets/images/icon_schedule.png",
              color: colorSchedule.value,
            ))));
      }
      if(userGroupByApp[i].page?.code == StringConstant.PAGE_DIRECTORY){
        listTabView.add(phonebook);
        listTabBar.add( Obx(() => Tab(
            icon:
            Image.asset(
              getItemTabBar(),
              width: 18.h,
              height: 18.h,
            )
        )));
      }
      if(userGroupByApp[i].page?.code ==StringConstant.PAGE_NOTIFY){
        listTabView.add(notify);
        listTabBar.add(Tab(
            icon: Obx(() => badge.Badge(
              badgeContent: Text(
                AppCache().notificationCount.value >
                    99
                    ? " 99+"
                    : "${AppCache().notificationCount.value}",
                style: TextStyleUtils
                    .sizeText9Weight400()
                    ?.copyWith(
                  fontFamily: "Montserrat",
                  color: ColorUtils.COLOR_WHITE,
                ),
              ),
              badgeColor: ColorUtils.colorF62727,
              animationType: BadgeAnimationType.scale,
              showBadge: AppCache()
                  .notificationCount
                  .value !=
                  0,
              position: BadgePosition.topEnd(
                  top: -6.h, end: -6.w),
              child: Image.asset(
                imageNotify.value,
                width: 18.h,
                height: 18.h,
                fit: BoxFit.contain,
              ),
            ))
        ));
      }
      if(userGroupByApp[i].page?.code == StringConstant.PAGE_MESSAGE){
        listTabView.add(message);
        listTabBar.add( Obx(() => Tab(
            icon:  Image.asset(
                width: 18.h,
                height: 18.h,
                imageMessage.value,
                )
                )));
      }
    }
  }


  void setIndex(int index) {
    tabController?.animateTo(index);
  }

  getCountNotifyNotSeen(){
    _notificationRepo.getTotalNotifyNotSeen().then((value) {
      if (value.state == Status.SUCCESS) {
        if(AppCache().isSaveInfoLogin == true){
          FlutterAppBadger.updateBadgeCount(value.object?.total ?? 0);
        }
        AppCache().notificationCount.value = value.object?.total ?? 0;
      }
    });
  }


  updateProfile(){
    RoleAction().getActionByRole(
        currentRole: AppCache().userType,
        teacherAction: () {
          Get.find<TeacherHomeController>().getDetailProfile();
        },
        managerAction: () {
          Get.find<ManagerHomeController>().getDetailProfile();
        },
        studentAction: () {
          Get.find<StudentHomeController>().getDetailProfile();
        },
        parentAction: () {
          Get.find<ParentHomeController>().getDetailProfile();
        });
  }

  getItemTabBar() {
    RoleAction().getActionByRole(
        currentRole: AppCache().userType,
        teacherAction: () {
          return "assets/images/phonebook_teacher.png";
        },
        managerAction: () {
          return "assets/images/phonebook_teacher.png";
        },
        studentAction: () {
          return tabController?.index  == listTabView.indexOf(schedule)
              ? "assets/images/icon_phonebook.png"
              : "assets/images/icon_phonebook_nofocus.png";
        },
        parentAction: () {
          return tabController?.index  == listTabView.indexOf(schedule)
              ? "assets/images/icon_phonebook.png"
              : "assets/images/icon_phonebook_nofocus.png";
        });
    return imagePhoneBook.value;
  }





  comeBackHome() {
    tabController?.animateTo(listTabView.indexOf(dashboard));
  }

  comeCalendar() {
    tabController?.animateTo(listTabView.indexOf(schedule));
  }

  comeNotification() {
    tabController?.animateTo(listTabView.indexOf(notify));
  }

  comeMessage(){
    tabController?.animateTo(listTabView.indexOf(message));
  }
  @override
  dispose() {
    tabController?.dispose();
    super.dispose();
  }



}
