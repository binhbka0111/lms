import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';
import '../../../data/base_service/api_response.dart';
import '../../../data/model/common/contacts.dart';
import '../../../data/model/common/student_by_parent.dart';
import '../../../data/model/res/class/classTeacher.dart';
import '../../../data/repository/contact/contact_repo.dart';
import '../../../data/repository/subject/class_office_repo.dart';
import '../../../routes/app_pages.dart';
import '../role/teacher/teacher_home_controller.dart';

class PhoneBookController extends GetxController {
  var isExpandStudent = false.obs;
  var indexItemStudent = (-1).obs;
  var isExpandParents = false.obs;
  var indexItemParents = (-1).obs;
  var currentRole = "".obs;
  final ClassOfficeRepo _subjectRepo = ClassOfficeRepo();
  RxList<ClassId> classOfUser = <ClassId>[].obs;
  Rx<ClassId> currentClass = ClassId().obs;
  final ContactRepo _contactRepo = ContactRepo();
  var contacts = Contacts().obs;
  RxList<ItemContact> itemContact = <ItemContact>[].obs;
  var currentStudentProfile = StudentByParent().obs;
  @override
  void onInit() {
    super.onInit();
    currentRole.value = AppCache().userType;
  }

  getClassContact(classId, type, index) async {
    _contactRepo.getClassContact(classId, type,"").then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        itemContact.value = contacts.value.item!;
        toDetailPhonebook(type, index);
      }
    });
  }

  getUserId(){
    switch (AppCache().userType) {
      case "PARENT":
        return Get.find<ParentHomeController>().currentStudentProfile.value.id;
      case "STUDENT":
        return  Get.find<StudentHomeController>().userProfile.value.id;
      case "TEACHER":
        return Get.find<TeacherHomeController>().userProfile.value.id;
    }
  }

  getClassContactParent(classId, type, index) async {
    _contactRepo.getClassContactParent(classId, type, getUserId(),"").then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        itemContact.value = contacts.value.item!;
        toDetailPhonebook(type, index);
      }
    });
  }


  getContact(type,userId) async {
    if(AppCache().userType == "PARENT"){
      _contactRepo.getContactParent(type,userId).then((value) {
        if (value.state == Status.SUCCESS) {
          contacts.value = value.object!;
          itemContact.value = contacts.value.item!;
          toDetailPhonebookTeacherAndAll(type);
        }
      });

    }else{
      _contactRepo.getContactParent(type,userId).then((value) {
        if (value.state == Status.SUCCESS) {
          contacts.value = value.object!;
          itemContact.value = contacts.value.item!;
          toDetailPhonebookTeacherAndAll(type);
        }
      });
    }

  }

  getUserIdStudentByParents(){
    if(AppCache().userType == "PARENT"){
     return Get.find<ParentHomeController>().currentStudentProfile.value.id;
    }else{
      return "";
    }
  }

  toDetailPhonebook(type, index) {
    Get.toNamed(Routes.detailPhonebook, arguments: [
      itemContact,
      type,
      classOfUser[index].name,
      classOfUser[index].id
    ]);
  }

  toDetailPhonebookTeacherAndAll(type) {
    Get.toNamed(Routes.detailPhonebook,
        arguments: [itemContact, type, "", ""]);
  }

  queryListClassByParent(userId) async {
    _subjectRepo.listClassByParent(userId).then((value) {
      if (value.state == Status.SUCCESS) {
        classOfUser.value = value.object!;
      }
    });
  }

  getListClassByStudent() async {
    _subjectRepo.listClassByStudent().then((value) {
      if (value.state == Status.SUCCESS) {
        classOfUser.value = value.object!;
      }
    });
  }
}
