import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/action/role_action.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/phonebook/phone_book_controller.dart';

import '../role/parent/parent_home_controller.dart';
class PhoneBookPage extends GetWidget<PhoneBookController> {
  @override
  final controller = Get.put(PhoneBookController());

  PhoneBookPage({super.key});

  getViewStudent(role) {
    switch (role) {
      case RoleAction.STUDENT:
        return true;
      case RoleAction.PARENT:
        return false;
      case RoleAction.TEACHER:
        return true;
      case RoleAction.MANAGER:
        return true;
    }
  }

  getViewParents(role) {
    switch (role) {
      case RoleAction.STUDENT:
        return false;
      case RoleAction.PARENT:
        return true;
      case RoleAction.TEACHER:
        return true;
      case RoleAction.MANAGER:
        return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title: Text(
            "Danh Bạ",
            style: TextStyle(color: Colors.white, fontSize: 16.sp),
          ),
          actions: [
            InkWell(
              onTap: () {
                Get.find<HomeController>().comeBackHome();
              },
              child: const Icon(
                Icons.home,
                color: Colors.white,
              ),
            ),
            const Padding(padding: EdgeInsets.only(right: 16))
          ],
          automaticallyImplyLeading: false,
          elevation: 0,
        ),
        body: Obx(() => RefreshIndicator(
            color:ColorUtils.PRIMARY_COLOR,
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Container(
                margin: const EdgeInsets.only(left: 16, right: 16, top: 24),
                child: Column(
                  children: [
                    Visibility(
                        visible: getViewStudent(controller.currentRole.value),
                        child: InkWell(
                          onTap: () {
                            controller.isExpandStudent.value =
                            !controller.isExpandStudent.value;
                            controller.getListClassByStudent();
                          },
                          child: Card(
                            elevation: 1,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)),
                            child: Container(
                              padding: const EdgeInsets.all(16),
                              child: Row(
                                children: [
                                  Text(
                                    "Học Sinh",
                                    style: TextStyle(
                                        fontSize: 14.sp,
                                        color: const Color.fromRGBO(26, 26, 26, 1),
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Expanded(child: Container()),
                                  Visibility(
                                    visible: controller.isExpandStudent.value ==
                                        false,
                                    child: const Icon(
                                      Icons.keyboard_arrow_right,
                                      color: ColorUtils.PRIMARY_COLOR,
                                      size: 28,
                                    ),
                                  ),
                                  Visibility(
                                    visible: controller.isExpandStudent.value ==
                                        true,
                                    child: const Icon(
                                      Icons.keyboard_arrow_up,
                                      color: ColorUtils.PRIMARY_COLOR,
                                      size: 28,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )),
                    Container(
                      margin: const EdgeInsets.only(right: 6, left: 6),
                      child: Visibility(
                          visible: controller.isExpandStudent.value == true,
                          child: ListView.builder(
                              itemCount: controller.classOfUser.length,
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return GetBuilder<PhoneBookController>(builder: (controller) {
                                  return InkWell(
                                    onTap: () {
                                      controller.indexItemStudent.value = index;
                                      controller.getClassContact(
                                          controller
                                              .classOfUser[index].id,
                                          "STUDENT",
                                          index);
                                      controller.update();

                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: index ==
                                              controller
                                                  .indexItemStudent.value
                                              ? ColorUtils.PRIMARY_COLOR
                                              : Colors.white,
                                          borderRadius:
                                          BorderRadius.circular(12)),
                                      margin: const EdgeInsets.only(
                                          top: 8, bottom: 8),
                                      padding: const EdgeInsets.only(
                                          left: 21, top: 13, bottom: 13),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            "assets/images/icon_class.png",width: 18.w,height: 18.h,
                                            color: index == controller.indexItemStudent.value
                                                ? Colors.white
                                                : const Color.fromRGBO(
                                                90, 90, 90, 1),
                                          ),
                                          const Padding(
                                              padding:
                                              EdgeInsets.only(left: 8)),
                                          Text(
                                            "${controller.classOfUser[index].name}",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: index ==
                                                    controller
                                                        .indexItemStudent
                                                        .value
                                                    ? Colors.white
                                                    : const Color.fromRGBO(
                                                    90, 90, 90, 1)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },);
                              })),
                    ),
                    Visibility(
                      visible: getViewParents(controller.currentRole.value),
                      child: const Padding(padding: EdgeInsets.only(bottom: 8)),
                    ),
                    Visibility(
                        visible: getViewParents(controller.currentRole.value),
                        child: InkWell(
                          onTap: () {
                            controller.isExpandParents.value =
                            !controller.isExpandParents.value;
                            if(AppCache().userType == "PARENT"){
                              controller.queryListClassByParent(Get.put(ParentHomeController()).currentStudentProfile.value.id);
                            }else{
                              controller.getListClassByStudent();
                            }
                          },
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)),
                            child: Container(
                              padding: const EdgeInsets.all(16),
                              child: Row(
                                children: [
                                  Text(
                                    "Phụ Huynh Học Sinh",
                                    style: TextStyle(
                                        fontSize: 14.sp,
                                        color: const Color.fromRGBO(26, 26, 26, 1),
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Expanded(child: Container()),
                                  Visibility(
                                    visible: controller.isExpandParents.value ==
                                        false,
                                    child: const Icon(
                                      Icons.keyboard_arrow_right,
                                      color: ColorUtils.PRIMARY_COLOR,
                                      size: 28,
                                    ),
                                  ),
                                  Visibility(
                                    visible: controller.isExpandParents.value ==
                                        true,
                                    child: const Icon(
                                      Icons.keyboard_arrow_up,
                                      color: ColorUtils.PRIMARY_COLOR,
                                      size: 28,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )),
                    Container(
                      margin: const EdgeInsets.only(right: 6, left: 6),
                      child: Visibility(
                          visible: controller.isExpandParents.value == true,
                          child: ListView.builder(
                              itemCount: controller.classOfUser.length,
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return itemClass(index);
                              })),
                    ),
                    const Padding(padding: EdgeInsets.only(bottom: 8)),
                    InkWell(
                      onTap: () {
                        controller.getContact("TEACHER", controller.getUserIdStudentByParents());
                      },
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)),
                        child: Container(
                          padding: const EdgeInsets.all(16),
                          child: Row(
                            children: [
                              Text(
                                "Giáo Viên",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontWeight: FontWeight.bold),
                              ),
                              Expanded(child: Container()),
                              const Icon(
                                Icons.keyboard_arrow_right,
                                color: ColorUtils.PRIMARY_COLOR,
                                size: 28,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    const Padding(padding: EdgeInsets.only(bottom: 8)),
                    InkWell(
                      onTap: () {
                        controller.getContact(
                            "", controller.getUserIdStudentByParents());
                      },
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)),
                        child: Container(
                          padding: const EdgeInsets.all(16),
                          child: Row(
                            children: [
                              Text(
                                "Tất Cả",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontWeight: FontWeight.bold),
                              ),
                              Expanded(child: Container()),
                              const Icon(
                                Icons.keyboard_arrow_right,
                                color: ColorUtils.PRIMARY_COLOR,
                                size: 28,
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            onRefresh: () async {
              await controller.getListClassByStudent();
            })));
  }

  itemClass(int index) {
    return Obx(() => InkWell(
          onTap: () {
            controller.indexItemParents.value = index;
            // controller.getClassContact(controller.classOfUser.value[index].id,"PARENT",index);
            controller.getClassContactParent(
                controller.classOfUser[index].id, 'PARENT', index);
          },
          child: Container(
            decoration: BoxDecoration(
                color: index == controller.indexItemParents.value
                    ? ColorUtils.PRIMARY_COLOR
                    : Colors.white,
                borderRadius: BorderRadius.circular(12)),
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            padding: const EdgeInsets.only(left: 21, top: 13, bottom: 13),
            child: Row(
              children: [
                Image.asset(
                  "assets/images/icon_class.png",width: 18.w,height: 18.h,
                  color: index ==
                      controller
                          .indexItemParents
                          .value
                      ? Colors.white
                      : const Color.fromRGBO(
                      90, 90, 90, 1),
                ),
                const Padding(padding: EdgeInsets.only(left: 8)),
                Text(
                  "${controller.classOfUser[index].name}",
                  style: TextStyle(
                      fontSize: 14.sp,
                      color: index == controller.indexItemParents.value
                          ? Colors.white
                          : const Color.fromRGBO(90, 90, 90, 1)),
                ),
              ],
            ),
          ),
        ));
  }
}
