import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/phonebook/detail_phonebook/Detail_information_by_phonebook/detail_information_phonebook_controller.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:get/get.dart';
//ignore: must_be_immutable
class DetailInformationPhoneBookPage extends GetView<DetailInformationPhoneBookController> {
  @override
  final controller = Get.put(DetailInformationPhoneBookController());

  DetailInformationPhoneBookPage({super.key});


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text("Chi tiết"),
        leading: BackButton(
          color: Colors.white,
          onPressed: () {
            Get.delete<DetailInformationPhoneBookController>();
            Get.back();
          },
        ),
        backgroundColor: ColorUtils.PRIMARY_COLOR,
      ),
      backgroundColor: const Color.fromRGBO(249, 249, 249, 1),
      body: Container(
        margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
        child: Column(
          children: [profileAvatarWidget(), informationWidget(size)],
        ),
      ),
    );
  }

  informationWidget(Size size) {
    return Obx(() => controller.isReady.value
        ? Container(
            margin: EdgeInsets.only(top: 16.h),
            padding: EdgeInsets.only(left: 6.w, right: 6.w),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              color: const Color.fromRGBO(255, 255, 255, 1),
            ),
            child: Column(
              children: [
                SizedBox(height: 8.h,),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Thông Tin Cá Nhân',
                        style: styleTitle,
                      ),
                    ],
                  ),
                ),
                Container(
                    margin: const EdgeInsets.all(9),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              'Họ Và Tên: ',
                              style: styleTitle,
                            ),
                            Expanded(child: Container()),
                            Text(
                              controller.userProfile.value.fullName ?? "",
                              style: TextStyle(
                                  color: ColorUtils.PRIMARY_COLOR,
                                  fontSize: 14.sp,
                                  fontFamily: 'static/Inter-Medium.ttf',
                                  fontWeight: FontWeight.w500),
                            )
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 10.h)),
                        Row(
                          children: [
                            Text(
                              "${controller.getTitleClassDetail(controller.item.value.type)}",
                              style: styleTitle,
                            ),
                            Expanded(
                                child: controller.item.value.type == "TEACHER"
                                    ? getPositionTeacher(
                                        controller.getValueTitleDetail(
                                            controller.item.value.type),
                                        controller.item.value.type)
                                    : getClassOfStudentOrPositionParent(
                                        controller.getValueTitleDetail(
                                            controller.item.value.type),
                                        controller.item.value.type)),
                            // getPossitionAll()
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 10.h)),
                        Row(
                          children: [
                            Text(
                              'Số Điện Thoại: ',
                              style: styleTitle,
                            ),
                            Expanded(child: Container()),
                            InkWell(
                              onTap: () async {
                                await launchUrl(Uri(
                                  scheme: 'tel',
                                  path: controller.userProfile.value.phone,
                                ));
                              },
                              child: Text(
                                controller.userProfile.value.phone ?? "",
                                style: styleValue,
                              ),
                            )
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 10.h)),
                        Row(
                          children: [
                            Text(
                              'Email: ',
                              style: styleTitle,
                            ),
                            Expanded(child: Container()),
                            Text(
                              controller.userProfile.value.email ?? "",
                              style: styleValue,
                            )
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 10.h)),
                        controller.getTitleSchoolDetail(
                                    controller.item.value.type) ==
                                ""
                            ? Container()
                            : Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    '${controller.getTitleSchoolDetail(controller.item.value.type)}',
                                    style: styleTitle,
                                  ),
                                 const Spacer(),
                                  SizedBox(
                                    width: size.width*0.5,
                                    child: Text(
                                      '${controller.getValueTitleSchoolDetail(controller.item.value.type)}',
                                      style: styleValue,
                                      textAlign: TextAlign.right,
                                    ),
                                  )
                                ],
                              ),
                        Padding(padding: EdgeInsets.only(top: 10.h)),
                      ],
                    ))
              ],
            ),
          )
        : Container());
  }

  profileAvatarWidget() {
    return Obx(() => Container(
          width: 400.w,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6.r),
            color: const Color.fromRGBO(255, 255, 255, 1),
          ),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 10.h),
                child: SizedBox(
                    width: 80.h,
                    height: 80.h,
                    child: InkWell(
                        onTap: () {
                          controller.goToDetailAvatar();
                        },
                        child:
                        CacheNetWorkCustom(urlImage: '${controller.userProfile.value.image}',)
                    )),
              ),
              SizedBox(
                height: 8.h,
              ),
              Text(
                controller.userProfile.value.fullName ?? "",
                style: TextStyle(
                    color: const Color.fromRGBO(26, 26, 26, 1),
                    fontSize: 18.sp,
                    fontFamily: 'static/Inter-Medium.ttf',
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 8.h,
              ),
              Text(
                controller.userProfile.value.email ?? "",
                style: TextStyle(
                  color: const Color.fromRGBO(133, 133, 133, 1),
                  fontSize: 10.sp,
                ),
              ),
              SizedBox(
                height: 16.h,
              ),
            ],
          ),
        ));
  }

  var styleValue = TextStyle(
      color: const Color.fromRGBO(26, 26, 26, 1),
      fontSize: 14.sp,
      fontFamily: 'static/Inter-Medium.ttf',
      fontWeight: FontWeight.w500);
  var styleTitle = TextStyle(
      color: const Color.fromRGBO(133, 133, 133, 1),
      fontSize: 12.sp,
      fontFamily: 'static/Inter-Medium.ttf',
      fontWeight: FontWeight.w500);

  RichText getClassOfStudentOrPositionParent(List<dynamic> list, role) {
    return RichText(
      textAlign: TextAlign.end,
      text: TextSpan(
        text: "",
        children: list.map((e) {
          var index = list.indexOf(e);
          var showSplit = ", ";
          if (index == list.length - 1) {
            showSplit = "";
          }
          if (controller.item.value.type == "STUDENT") {
            return TextSpan(
                text: "${e.name}$showSplit",
                style: TextStyle(
                    color: ColorUtils.PRIMARY_COLOR,
                    fontSize: 14.sp,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'static/Inter-Medium.ttf'));
          }
          return TextSpan(
              text: "${e.fullName}$showSplit",
              style: TextStyle(
                  color: ColorUtils.PRIMARY_COLOR,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf'));
        }).toList(),
      ),
    );
  }

  RichText getPositionTeacher(List<dynamic> list, role) {
    return RichText(
      textAlign: TextAlign.end,
      text: TextSpan(
        text:"",
       children:
         list.map((e) {
           var index = list.indexOf(e);
           var showSplit = ", ";
           if (index == list.length - 1) {
             showSplit = "";
           }
           if (controller.item.value.type == "TEACHER") {
             if(e.type == "HOME_ROOM_TEACHER"){
               return TextSpan(
                 text: "Giáo viên chủ nhiệm, ${e.subjectName != null? 'Môn ${e.subjectName}' : '' } ${e.subjectName != null ? showSplit : '' } ",
                 style: TextStyle(
                     color: ColorUtils.PRIMARY_COLOR,
                     fontSize: 14.sp,
                     fontWeight: FontWeight.w500,
                     fontFamily: 'static/Inter-Medium.ttf'),);
             }else{
             return TextSpan(
                 text: "Môn ${e.subjectName ?? ""}$showSplit",
                 style: TextStyle(
                     color: ColorUtils.PRIMARY_COLOR,
                     fontSize: 14.sp,
                     fontWeight: FontWeight.w500,
                     fontFamily: 'static/Inter-Medium.ttf'),);
             }
           }

           return TextSpan(
               text: "${e.subjectName}$showSplit",
               style: TextStyle(
                   color: ColorUtils.PRIMARY_COLOR,
                   fontSize: 14.sp,
                   fontWeight: FontWeight.w500,
                   fontFamily: 'static/Inter-Medium.ttf'));
         }).toList(),
      ),
    );
  }
}
