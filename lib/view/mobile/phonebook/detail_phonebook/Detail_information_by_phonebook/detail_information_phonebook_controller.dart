import 'package:get/get.dart';
import 'package:slova_lms/data/model/common/Position.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/common/contacts.dart';
import '../../../../../data/model/common/student_by_parent.dart';
import '../../../../../data/model/common/user_profile.dart';
import '../../../../../data/model/res/class/School.dart';
import '../../../../../data/repository/person/personal_info_repo.dart';
import '../../../../../routes/app_pages.dart';

class DetailInformationPhoneBookController extends GetxController {
  var userProfile = UserProfile().obs;
  var school = SchoolData().obs;
  var id = "".obs;
  var type = "".obs;
  var clasName = ''.obs;
  var student = <Student>[].obs;
  var parent = <Parent>[].obs;
  var subject = <SubjectItemContact>[].obs;
  var position = Position().obs;
  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();
  RxList<ItemContact> itemContact = <ItemContact>[].obs;
  RxList<ItemUserProfile> itemuser = <ItemUserProfile>[].obs;
  var typeInList = "".obs;
  var listStudent = <StudentByParent>[].obs;
  var item = ItemContact().obs;
  var isReady = false.obs;
  var teacher = <Teachers>[].obs;

  @override
  void onInit() {
    super.onInit();
    var data = Get.arguments;
    if (data != null) {
      item.value = data;
    }
    _personalInfoRepo.getUserProfileById(item.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        if (userProfile.value.school == null) {
          school.value.name = "";
        } else {
          school.value = userProfile.value.school!;
        }
        if(userProfile.value.item?.teachers!= null ){
          teacher.value = userProfile.value.item!.teachers!;
        }
        if (userProfile.value.position == null) {
          position.value.name = "";
        } else {
          position.value = userProfile.value.position!;
        }
      }
      isReady.value = true;
    });

    _personalInfoRepo.listStudentByParent(item.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudent.value = value.object!;
      }
    });
  }

  void goToDetailAvatar() {
    Get.toNamed(Routes.getavatar, arguments: userProfile.value.image);
  }





  getTitleClassDetail(role) {
    switch (role) {
      case "STUDENT":
        return "Lớp: ";
      case "PARENT":
        return "Phụ huynh em: ";
      case "TEACHER":
        return "Chức danh: ";
      default:
        return "";
    }
  }

  Future<void> makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  getTitleSchoolDetail(role) {
    switch (role) {
      case "STUDENT":
        return "Trường: ";
      case "PARENT":
        return "";
      case "TEACHER":
        return "Trường: ";
      default:
        return "";
    }
  }

  getValueTitleSchoolDetail(role) {
    switch (role) {
      case "STUDENT":
        return school.value.name;
      case "TEACHER":
        return school.value.name;
      case "PARENT":
        return "";
      default:
        return "";
    }
  }

  getValueTitleDetail(role) {
    switch (role) {
      case "STUDENT":
        return userProfile.value.item!.clazzs;
      case "TEACHER":
        return userProfile.value.item!.teachers;
      case "PARENT":
        return listStudent;
      default:
        return "";
    }
  }


}
