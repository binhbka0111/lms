import 'package:flutter/material.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:get/get.dart';
import '../../home/home_controller.dart';
import 'detail_phonebook_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class DetailPhoneBookPage extends GetView<DetailPhoneBookController> {
  @override
  final controller = Get.put(DetailPhoneBookController());

  DetailPhoneBookPage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return 
     Obx(() =>  Scaffold(
         appBar: AppBar(
           elevation: 0,
           backgroundColor: ColorUtils.PRIMARY_COLOR,
           title: Text(controller.getTitleAppbar(controller.type.value),
               style: TextStyle(color: Colors.white, fontSize: 16.sp)),
         ),
         body:
         RefreshIndicator(
             color: ColorUtils.PRIMARY_COLOR,
             child: SingleChildScrollView(
                 physics: const AlwaysScrollableScrollPhysics(),
                 child:Column(
                   children: [
                     Container(
                       color: Colors.white,
                       padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                       child:  SizedBox(
                         width: double.infinity,
                         height: 40,
                         child: TextFormField(
                           onChanged: (text){
                             // controller.findUserByClassContact(controller.classId.value, controller.getRole(controller.type.value),text.trim());
                             if (controller.controllerName.value.text.trim().isEmpty)
                             {
                               controller.getClassContact(controller.classId.value, controller.getRole(controller.type.value));
                             }
                             else if(controller.controllerName.value.text.trim().isNotEmpty)
                             {
                               controller.findUserByClassContact(controller.classId.value, controller.getRole(controller.type.value),text.trim());
                             }else{
                               controller.itemContact.value = [];
                             }
                           },
                           onFieldSubmitted: (value) => {
                             if (controller.controllerName.value.text.trim().isEmpty)
                               {
                                 controller.getClassContact(controller.classId.value, controller.getRole(controller.type.value))
                               }
                             else
                               {
                                 controller.findUserByClassContact(controller.classId.value, controller.getRole(controller.type.value), controller.controllerName.value.text.trim()),
                               }
                           },
                           controller: controller.controllerName.value,
                           decoration: InputDecoration(
                               border: OutlineInputBorder(
                                   borderSide: const BorderSide(
                                       color:
                                       Color.fromRGBO(177, 177, 177, 1)),
                                   borderRadius: BorderRadius.circular(6)),
                               prefixIcon: const Icon(
                                 Icons.search,
                                 color: Color.fromRGBO(177, 177, 177, 1),
                               ),
                               hintText: "Tìm kiếm",
                               isCollapsed: true,
                               hintStyle: TextStyle(
                                   fontSize: 14.sp,
                                   color: const Color.fromRGBO(
                                       177, 177, 177, 1)),
                               focusedBorder: const OutlineInputBorder(
                                   borderSide: BorderSide(
                                       color: ColorUtils.PRIMARY_COLOR))),
                           textAlignVertical: TextAlignVertical.center,
                           cursorColor: ColorUtils.PRIMARY_COLOR,
                         ),
                       )
                     ),
                     Padding(padding: EdgeInsets.only(top: 16.h)),
                     Container(
                       margin: EdgeInsets.only(left: 16.w),
                       child: Row(
                         children: [
                           Text(
                             controller.getLabel(controller.type.value),
                             style:  TextStyle(
                                 color: const Color.fromRGBO(133, 133, 133, 1),
                                 fontSize: 12.sp),
                           ),
                           Visibility(
                             visible:
                             controller.getTitleClass(controller.type.value),
                             child: Text(
                               " ${controller.className.value}",
                               style:  TextStyle(
                                   color: ColorUtils.PRIMARY_COLOR,
                                   fontSize: 12.sp),
                             ),
                           )
                         ],
                       ),
                     ),
                     Padding(padding: EdgeInsets.only(top: 8.h)),
                     controller.itemContact.isEmpty
                         ? Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: [
                         SizedBox(
                           width: 100.w,
                           height: 100.h,
                           child: Image.asset("assets/images/noFind.png"),
                         ),
                         Padding(padding: EdgeInsets.only(top: 4.h)),
                         Text(
                           'Không có tìm kiếm nào trùng khớp',
                           style: TextStyle(
                               color: const Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w500,
                               fontSize: 12.sp),
                         ),
                         Padding(padding: EdgeInsets.only(top: 4.h)),
                         InkWell(
                           onTap: () {
                             controller.findUserByClassContact(
                                 controller.classId.value,
                                 controller.getRole(controller.type.value),
                                 controller.controllerName.value.text.trim());
                           },
                           child: Text(
                             "Nhấn để thử lại",
                             style: TextStyle(
                                 color: Colors.red,
                                 fontSize: 10.sp,
                                 fontWeight: FontWeight.w400),
                           ),
                         )
                       ],
                     )
                         :
                     ListView.builder(
                         shrinkWrap: true,
                         physics: const ScrollPhysics(),
                         itemCount: controller.itemContact.length,
                         itemBuilder: (context, index) {
                           controller.student.value =
                           controller.itemContact[index].student!;
                           controller.parent.value =
                           controller.itemContact[index].parent!;
                           controller.subject.value =
                           controller.itemContact[index].subject!;
                           controller.typeInList.value =
                           controller.itemContact[index].type!;
                           return
                             Container(
                               decoration: BoxDecoration(
                                 color: Colors.white,
                                 borderRadius: BorderRadius.circular(6),
                               ),
                               margin: const EdgeInsets.fromLTRB(16, 0, 16, 12),
                               child:  InkWell(
                                 onTap: () {
                                   checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> controller
                                       .goToDetailInformationPhoneBook(
                                       index),StringConstant.FEATURE_DIRECTORY_DETAIL);
                                 },
                                 child: Row(
                                   children: [
                                     Container(
                                       margin: const EdgeInsets.only(
                                           left: 8, top: 8),
                                       child: SizedBox(
                                           width: 40.h,
                                           height: 40.h,
                                           child:
                                           CacheNetWorkCustom(urlImage: '${controller.itemContact
                                           [index].image }',)

                                       ),
                                     ),
                                     const Padding(
                                         padding:
                                         EdgeInsets.only(left: 8)),
                                     Expanded(
                                       child: Column(
                                         crossAxisAlignment:
                                         CrossAxisAlignment.start,
                                         children: [
                                           const Padding(
                                               padding: EdgeInsets.only(
                                                   top: 8)),
                                           Text(
                                             "${controller.itemContact[index].fullName}",
                                             style: TextStyle(
                                                 color: Colors.black,
                                                 fontSize: 16.sp,
                                                 fontWeight:
                                                 FontWeight.bold),
                                           ),
                                           Padding(
                                               padding: EdgeInsets.only(
                                                   top: 4.h)),
                                           getRole(
                                               controller.getListRole(controller.typeInList.value, index),
                                               controller.typeInList.value, index),
                                           Padding(
                                               padding: EdgeInsets.only(
                                                   top: 4.h)),
                                           Row(
                                             children: [
                                               Text(
                                                 "Số điện thoại: ",
                                                 style: TextStyle(
                                                     fontSize: 14.sp,
                                                     color: const Color
                                                         .fromRGBO(133,
                                                         133, 133, 1)),
                                               ),
                                               InkWell(
                                                 onTap: () async {
                                                   await checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> callDirectory(index),StringConstant.FEATURE_DIRECTORY_CALL);
                                                 },
                                                 child: Text(
                                                   controller
                                                       .itemContact
                                                   [index]
                                                       .phone ??
                                                       "",
                                                   style: TextStyle(
                                                       color: Colors.black,
                                                       fontSize: 14.sp),
                                                 ),
                                               )
                                             ],
                                           ),
                                           Padding(
                                               padding: EdgeInsets.only(
                                                   bottom: 8.h)),
                                         ],
                                       ),
                                     ),
                                     const Padding(
                                         padding:
                                         EdgeInsets.only(left: 8)),
                                     Container(
                                       margin: EdgeInsets.only(bottom: 40.h),
                                       child: InkWell(
                                         onTap: () {
                                           checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> controller.getIdPrivateChat(controller.itemContact[index].id),StringConstant.FEATURE_DIRECTORY_MESSAGE);
                                         },
                                         child: Image.asset('assets/images/chatphonebook.png', width: 24, height: 24,),
                                       ),
                                     ),
                                     const SizedBox(width: 8,)
                                   ],
                                 ),
                               ),
                             );
                         })
                   ],
                 )

             ),
             onRefresh: () async {
               controller.onRefresh();
             })

     ));
  }

  callDirectory(index){
     launchUrl(Uri(scheme: 'tel', path: controller.itemContact[index].phone,
    ));
  }

  RichText getRole(List<dynamic> list, role, index) {
    return RichText(
      textAlign: TextAlign.start,
      text: TextSpan(
          text: "${controller.getTitle(role)}",
          style: TextStyle(
              color: const Color.fromRGBO(173, 173, 173, 1),
              fontSize: 14.sp,
              fontWeight: FontWeight.w500,
              fontFamily: 'static/Inter-Medium.ttf'),
          children: [
            TextSpan(
              text:
                  "${controller.getTextHomeRoomTeacher(controller.itemContact[index].positionName, index)}",
              style: controller.getColorTextHomeRoomTeacher(
                      controller.itemContact[index].positionName)
                  ? TextStyle(
                      color: const Color.fromRGBO(173, 173, 173, 1),
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'static/Inter-Medium.ttf')
                  : TextStyle(
                      color: ColorUtils.PRIMARY_COLOR,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'static/Inter-Medium.ttf'),
              children: list.map((e) {
                var index = list.indexOf(e);
                var showSplit = ", ";
                if (index == list.length - 1) {
                  showSplit = "";
                }
                if (controller.getRole(controller.typeInList.value) ==
                    "TEACHER") {
                  return TextSpan(
                      text: "Môn ${e.fullName}$showSplit",
                      style: TextStyle(
                          color: ColorUtils.PRIMARY_COLOR,
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'static/Inter-Medium.ttf'));
                }

                return TextSpan(
                    text: "${e.fullName}$showSplit",
                    style: TextStyle(
                        color: ColorUtils.PRIMARY_COLOR,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'static/Inter-Medium.ttf'));
              }).toList(),
            ),
          ]),
    );
  }
}
