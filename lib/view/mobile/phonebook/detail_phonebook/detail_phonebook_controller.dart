import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import '../../../../commom/utils/app_utils.dart';
import '../../../../commom/utils/color_utils.dart';
import '../../../../data/base_service/api_response.dart';
import '../../../../data/model/common/contacts.dart';
import '../../../../data/model/common/user_profile.dart';
import '../../../../data/model/res/message/detail_group_chat.dart';
import '../../../../data/repository/chat/chat_repo.dart';
import '../../../../data/repository/contact/contact_repo.dart';
import '../../../../routes/app_pages.dart';
import '../../message/message_controller.dart';

class DetailPhoneBookController extends GetxController {
  var type = "".obs;
  var className = "".obs;
  var classId = "".obs;
  var userProfile = UserProfile().obs;
  var student = <Student>[].obs;
  var parent = <Parent>[].obs;
  var subject = <SubjectItemContact>[].obs;
  final ContactRepo _contactRepo = ContactRepo();
  var contacts = Contacts().obs;
  RxList<ItemContact> itemContact = <ItemContact>[].obs;
  var controllerName = TextEditingController().obs;
  var typeInList = "".obs;
  var colorText = false.obs;
  final ChatRepo _chatRepo = ChatRepo();
  var itemDetailGroupChat = DetailGroupChat().obs;
  @override
  void onInit() {
    super.onInit();
    var data = Get.arguments;

    if (data != null && data.isNotEmpty) {
      itemContact.value = data[0];
      type.value = data[1];
      className.value = data[2];
      classId.value = data[3];
    }
  }


  getIdPrivateChat(userId) async{
    await _chatRepo.getIdPrivateChat(userId).then((value) async{
      if (value.state == Status.SUCCESS) {
        itemDetailGroupChat.value = value.object!;
        await openSocket();
        Get.toNamed(Routes.detailMessagePrivatePage,arguments: itemDetailGroupChat.value.id);
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lỗi",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


  openSocket(){
    if(Get.find<HomeController>().userGroupByApp.isEmpty){
      Get.find<MessageController>().socketIo.initSocket();
    }else{
      var list = Get.find<HomeController>().userGroupByApp.where((element) => element.page?.code == "PAGE_MESSAGE");
      if(list.isNotEmpty){
        Get.find<MessageController>().socketIo.initSocket();
      }
    }
  }

  getTitle(role) {
    switch (role) {
      case "PARENT":
        return "Phụ huynh hs: ";
      case "STUDENT":
        return "Phụ huynh: ";
      case "TEACHER":
        return "Giáo viên: ";
      default:
        return "";
    }
  }

  getTextHomeRoomTeacher(isHomeRoom, index) {
    var split = "";
    switch (isHomeRoom) {
      case "":
        return "";
      case null:
        return "";
      case "Giáo viên chủ nhiệm":
        if (itemContact[index].subject!.isEmpty) {
          split = "";
        } else {
          split = ",";
        }
        return "Giáo viên chủ nhiệm$split ";
      default:
        return "";
    }
  }


  getColorTextHomeRoomTeacher(isHomeRoom) {
    switch (isHomeRoom) {
      case "":
        return colorText.value == false;
      case null:
        return colorText.value == false;
      case "Giáo viên chủ nhiệm":
        return colorText.value == true;
      default:
        return colorText.value == false;
    }
  }

  getItemCount(role) {
    switch (role) {
      case "PARENT":
        return student.length;
      case "STUDENT":
        return parent.length;
      case "TEACHER":
        return subject.length;
      default:
        return 0;
    }
  }

  getFindDefault() {
    return AppUtils.shared.showToast("Tìm kiếm thành công!");
  }

  getListRole(role, index) {
    switch (role) {
      case "PARENT":
        return student;
      case "STUDENT":
        return parent;
      case "TEACHER":
        return itemContact[index].subject;
    }
  }

  getTitleClass(role) {
    if (role == "STUDENT" || role == "PARENT") {
      return true;
    } else {
      return false;
    }
  }

  getLabel(role) {
    switch (role) {
      case "PARENT":
        return "Thông Tin Phụ Huynh";
      case "STUDENT":
        return "Thông Tin Học Sinh";
      case "TEACHER":
        return "Thông Tin Giáo Viên";
      default:
        return "Gợi ý";
    }
  }

  getTitleAppbar(role) {
    switch (role) {
      case "STUDENT":
        return "Danh Bạ Học Sinh";
      case "PARENT":
        return "Danh Bạ Phụ Huynh";
      case "TEACHER":
        return "Danh Bạ Giáo Viên";
      default:
        return "Tất Cả";
    }
  }



  getRole(role) {
    switch (role) {
      case "STUDENT":
        return "STUDENT";
      case "TEACHER":
        return "TEACHER";
      case "PARENT":
        return "PARENT";
      case "MANAGER":
        return "MANAGER";
      default:
        return "";
    }
  }

  onRefresh() {
    getClassContact(classId.value, getRole(type.value));
  }

  findUserByClassContact(classId, type, fullName) async {
    _contactRepo.findUserByClassContact(classId, type, fullName).then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        itemContact.value = contacts.value.item!;
      }
    });
  }

  getClassContact(classId, type) async {
    _contactRepo.getClassContact(classId, type,"").then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        itemContact.value = contacts.value.item!;
      }
    });
  }

  void goToDetailAvatar(index) {
    Get.toNamed(Routes.getavatar, arguments: itemContact[index].image);
  }

  goToDetailInformationPhoneBook(index) {
    Get.toNamed(Routes.infoInPhoneBook, arguments: itemContact[index]);
  }
}
