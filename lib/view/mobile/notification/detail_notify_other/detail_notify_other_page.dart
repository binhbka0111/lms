import 'package:dotted_border/dotted_border.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:html/parser.dart';
import 'package:slova_lms/commom/utils/date_time_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import '../../../../../../../commom/utils/ViewPdf.dart';
import '../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../commom/utils/global.dart';
import '../../../../../../../commom/utils/open_url.dart';
import 'detail_notify_other_controller.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';



class DetailNotifyOtherPage extends GetView<DetailNotifyOtherController> {
  @override
  final controller = Get.put(DetailNotifyOtherController());

  DetailNotifyOtherPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GetBuilder<DetailNotifyOtherController>(builder: (controller) {
      return Center(
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {
                  comeToHome();
                },
                icon: const Icon(Icons.home),
                color: Colors.white,
              )
            ],
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            title: controller.isReady?Text(
              'Xem chi tiết thông báo ${controller.proactiveNotify.value.clazz?.classCategory?.name}',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ):Container(),
          ),
          body: RefreshIndicator(
              color: ColorUtils.PRIMARY_COLOR,
              child: GetBuilder<DetailNotifyOtherController>(builder: (controller) {
                return controller.isReady?Container(
                  margin: EdgeInsets.all(16.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets
                            .only(
                            left: 4.w),
                        child: Column(
                          mainAxisAlignment:
                          MainAxisAlignment
                              .start,
                          crossAxisAlignment:
                          CrossAxisAlignment
                              .start,
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 4.w),
                              child: SizedBox(
                                width: 270.w,
                                child: RichText(
                                    text: TextSpan(children: [
                                      TextSpan(
                                          text:
                                          '[${controller.proactiveNotify.value.typeNotify}] ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color:ColorUtils.PRIMARY_COLOR,
                                              fontSize: 14.sp)),
                                      TextSpan(
                                          text:
                                          ': ${controller.proactiveNotify.value.title}',
                                          style: const TextStyle(
                                              color: Color.fromRGBO(26, 26, 26, 1),
                                              fontSize: 16)),
                                    ])),
                              ),
                            ),
                            SizedBox(
                              height: 8.h,
                            ),
                            Container(
                              margin:  EdgeInsets
                                  .only(
                                  left:
                                  4.w),
                              child: Text("${parse(parse(controller.proactiveNotify.value.body).body?.text).documentElement?.text}")
                            ),
                            SizedBox(
                              height: 8.h,
                            ),
                            Row(
                              children: [
                                SizedBox(
                                  width: 36.w,
                                  height: 26.w,
                                  child:
                                  CacheNetWorkCustom(urlImage: '${controller.proactiveNotify.value.createdBy?.image}',)

                                ),
                                Padding(padding: EdgeInsets.only(right: 8.w)),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("${controller.proactiveNotify.value.createdBy?.fullName}",style: TextStyle(color: Colors.black,fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                    Container(
                                      margin:  EdgeInsets
                                          .only(
                                          left:
                                          4.w),
                                      child: Text(
                                        DateTimeUtils.convertToAgo(DateTime.fromMillisecondsSinceEpoch(controller.proactiveNotify.value.createdAt ??
                                            0)
                                            .toLocal()),
                                        textAlign:
                                        TextAlign
                                            .left,
                                        style: const TextStyle(
                                            color: Color.fromRGBO(
                                                114,
                                                116,
                                                119,
                                                1),
                                            fontSize:
                                            12),
                                      ),
                                    ),
                                  ],
                                ),

                              ],
                            ),

                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      Visibility(
                          visible: controller.proactiveNotify.value.files!.where((element) =>
                          element.ext == "png" ||
                              element.ext == "jpg" ||
                              element.ext == "jpeg" ||
                              element.ext == "gif" ||
                              element.ext == "bmp")
                              .toList()
                              .isNotEmpty,
                          child: GridView.builder(
                              padding: const EdgeInsets.all(10),
                              gridDelegate:
                              SliverGridDelegateWithMaxCrossAxisExtent(
                                maxCrossAxisExtent: 250.w,
                                crossAxisSpacing: 10,
                                mainAxisSpacing: 10,
                              ),
                              scrollDirection: Axis.vertical, //chiều cuộn
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: controller.proactiveNotify.value.files!
                                  .where((element) =>
                              element.ext == "png" ||
                                  element.ext == "jpg" ||
                                  element.ext == "jpeg" ||
                                  element.ext == "gif" ||
                                  element.ext == "bmp")
                                  .toList()
                                  .length,
                              itemBuilder: (context, indexGrid) {
                                return InkWell(
                                  onTap: () {
                                    OpenUrl.openImageViewer(
                                        context,
                                        controller.proactiveNotify.value
                                            .files!
                                            .where((element) =>
                                        element.ext == "png" ||
                                            element.ext == "jpg" ||
                                            element.ext == "jpeg" ||
                                            element.ext == "gif" ||
                                            element.ext == "bmp")
                                            .toList()[indexGrid]
                                            .link!);
                                  },
                                  child: SizedBox(
                                    width: 200.w,
                                    height: 200.w,
                                    child: Image.network(
                                      controller.proactiveNotify.value.files!.where((element) =>
                                      element.ext == "png" ||
                                          element.ext == "jpg" ||
                                          element.ext == "jpeg" ||
                                          element.ext == "gif" ||
                                          element.ext == "bmp")
                                          .toList()[indexGrid]
                                          .link ??
                                          "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                      errorBuilder:
                                          (context, object, stackTrace) {
                                        return Image.asset(
                                          "assets/images/image_error_load.jpg",
                                        );
                                      },
                                    ),
                                  ),
                                );
                              })),
                      Visibility(
                          visible: controller.proactiveNotify.value.files!
                              .where((element) =>
                          element.ext != "png" &&
                              element.ext != "jpg" &&
                              element.ext != "jpeg" &&
                              element.ext != "gif" &&
                              element.ext != "bmp")
                              .toList()
                              .isNotEmpty,
                          child: DottedBorder(
                              dashPattern: const [5, 5],
                              radius: const Radius.circular(6),
                              borderType: BorderType.RRect,
                              padding: const EdgeInsets.only(
                                  top: 12, left: 12, right: 12, bottom: 12),
                              color: ColorUtils.PRIMARY_COLOR,
                              child: ListView.builder(
                                  shrinkWrap: true,
                                  physics:
                                  const NeverScrollableScrollPhysics(),
                                  itemCount: controller.proactiveNotify.value
                                      .files!
                                      .where((element) =>
                                  element.ext != "png" &&
                                      element.ext != "jpg" &&
                                      element.ext != "jpeg" &&
                                      element.ext != "gif" &&
                                      element.ext != "bmp")
                                      .toList()
                                      .length,
                                  itemBuilder: (context, indexNotImage) {
                                    return InkWell(
                                      onTap: () {
                                        var action = 0;
                                        var extension = controller.proactiveNotify.value
                                            .files!
                                            .where((element) =>
                                        element.ext != "png" &&
                                            element.ext != "jpg" &&
                                            element.ext != "jpeg" &&
                                            element.ext != "gif" &&
                                            element.ext != "bmp")
                                            .toList()[indexNotImage]
                                            .ext;
                                        if (extension == "png" ||
                                            extension == "jpg" ||
                                            extension == "jpeg" ||
                                            extension == "gif" ||
                                            extension == "bmp") {
                                          action = 1;
                                        } else if (extension == "pdf") {
                                          action = 2;
                                        } else {
                                          action = 0;
                                        }
                                        switch (action) {
                                          case 1:
                                            OpenUrl.openImageViewer(
                                                context,
                                                controller.proactiveNotify.value
                                                    .files!
                                                    .where((element) =>
                                                element.ext != "png" &&
                                                    element.ext !=
                                                        "jpg" &&
                                                    element.ext !=
                                                        "jpeg" &&
                                                    element.ext !=
                                                        "gif" &&
                                                    element.ext !=
                                                        "bmp")
                                                    .toList()[
                                                indexNotImage]
                                                    .link!);
                                            break;
                                          case 2:
                                            Get.to(ViewPdfPage(
                                                url: controller.proactiveNotify.value
                                                    .files!
                                                    .where((element) =>
                                                element.ext != "png" &&
                                                    element.ext !=
                                                        "jpg" &&
                                                    element.ext !=
                                                        "jpeg" &&
                                                    element.ext !=
                                                        "gif" &&
                                                    element.ext !=
                                                        "bmp")
                                                    .toList()[
                                                indexNotImage]
                                                    .link!));
                                            break;
                                          default:
                                            OpenUrl.openFile(controller.proactiveNotify.value
                                                .files!
                                                .where((element) =>
                                            element.ext != "png" &&
                                                element.ext !=
                                                    "jpg" &&
                                                element.ext !=
                                                    "jpeg" &&
                                                element.ext !=
                                                    "gif" &&
                                                element.ext != "bmp")
                                                .toList()[indexNotImage]
                                                .link!);
                                            break;
                                        }
                                      },
                                      child: Container(
                                        margin: const EdgeInsets.only(
                                            top: 12),
                                        padding: const EdgeInsets.all(12),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            BorderRadius.circular(6),
                                            color: const Color.fromRGBO(
                                                246, 246, 246, 1)),
                                        child: Row(
                                          children: [
                                            Image.network(
                                              controller.proactiveNotify.value
                                                  .files!
                                                  .where((element) =>
                                              element.ext != "png" &&
                                                  element.ext !=
                                                      "jpg" &&
                                                  element.ext !=
                                                      "jpeg" &&
                                                  element.ext !=
                                                      "gif" &&
                                                  element.ext !=
                                                      "bmp")
                                                  .toList()[indexNotImage]
                                                  .link!,
                                              errorBuilder: (
                                                  BuildContext context,
                                                  Object error,
                                                  StackTrace? stackTrace,
                                                  ) {
                                                return Image.asset(
                                                  getFileIcon(controller.proactiveNotify.value
                                                      .files!
                                                      .where((element) =>
                                                  element.ext != "png" &&
                                                      element.ext !=
                                                          "jpg" &&
                                                      element.ext !=
                                                          "jpeg" &&
                                                      element.ext !=
                                                          "gif" &&
                                                      element.ext !=
                                                          "bmp")
                                                      .toList()[
                                                  indexNotImage]
                                                      .name),
                                                  height: 35,
                                                  width: 30,
                                                );
                                              },
                                              height: 35,
                                              width: 30,
                                            ),
                                            const Padding(
                                                padding: EdgeInsets.only(
                                                    left: 8)),
                                            SizedBox(
                                              width: 220.w,
                                              child: Text(
                                                '${controller.proactiveNotify.value.files!.where((element) => element.ext != "png" && element.ext != "jpg" && element.ext != "jpeg" && element.ext != "gif" && element.ext != "bmp").toList()[indexNotImage].name}',
                                                style: TextStyle(
                                                    color: const Color
                                                        .fromRGBO(
                                                        26, 59, 112, 1),
                                                    fontSize: 14.sp,
                                                    fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf',
                                                    fontWeight:
                                                    FontWeight.w500),
                                              ),
                                            ),
                                            Expanded(child: Container()),
                                            Image.asset(
                                              'assets/images/icon_upfile_subject.png',
                                              height: 16,
                                              width: 16,
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  })
                          )),
                    ],
                  ),
                ):Container();
              },), onRefresh: ()async{
            controller.getDetailNotifyOther(controller.transID.value);
          }),
        ),
      );
    },);
  }

}
