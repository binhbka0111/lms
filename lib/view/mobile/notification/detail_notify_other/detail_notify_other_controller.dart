import 'package:get/get.dart';
import 'package:slova_lms/data/repository/notification_repo/notification_repo.dart';

import '../../../../commom/utils/app_utils.dart';
import '../../../../data/base_service/api_response.dart';
import '../../../../data/model/res/notification/proactive_notifications.dart';



class DetailNotifyOtherController extends GetxController {
  var transID = "".obs;
  final NotificationRepo _notificationRepo = NotificationRepo();
  var proactiveNotify = ProactiveNotify().obs;
  var isReady = false;
  @override
  void onInit() {
    var data = Get.arguments;
    if (data != null) {
      transID.value = data;
      getDetailNotifyOther(transID.value);
    }
    super.onInit();
  }


  getDetailNotifyOther(transId) async{
    await  _notificationRepo.getDetailNotifyOther(transId).then((value) {
      if (value.state == Status.SUCCESS) {
        proactiveNotify.value = value.object!;
        isReady = true;
        update();
      } else {
        AppUtils.shared.snackbarError("Lỗi", value.message ?? "");
      }
    });
  }


}
