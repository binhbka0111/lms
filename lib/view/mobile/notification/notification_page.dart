import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:html/parser.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/notification/notification_controller.dart';
import '../../../commom/utils/date_time_utils.dart';

class NotificationPage extends GetView<NotificationController> {
  @override
  final controller = Get.put(NotificationController());

  NotificationPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NotificationController>(builder: (controller) {
      return controller.isReady?
      Scaffold(
        backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title:  Text(
            'Thông báo',
            style: TextStyle(
                color: Colors.white,
                fontSize: 20.sp,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body: RefreshIndicator(
            color: ColorUtils.PRIMARY_COLOR,
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Obx(() => Container(
                margin: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 32.h),
                alignment: Alignment.centerLeft,
                child: Column(
                  children: [
                    Visibility(
                        visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_NOTIFY_CLASS),
                        child: Visibility(
                            visible: AppCache().userType == "TEACHER",
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(6.r),
                                  border: Border.all(
                                      color:
                                      const Color.fromRGBO(239, 239, 239, 1),
                                      width: 1)),
                              child: Row(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      controller.isAllClass.value = true;
                                      for (var f
                                      in controller.classOfTeacher) {
                                        f.checked = false;
                                      }
                                      controller.getListNotification();
                                    },
                                    child: Container(
                                      width: 100,
                                      height: 26,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        color: controller.isAllClass.value
                                            ? const Color.fromRGBO(
                                            249, 154, 81, 1)
                                            : const Color.fromRGBO(
                                            246, 246, 246, 1),
                                        borderRadius: BorderRadius.circular(6),
                                      ),
                                      margin: const EdgeInsets.only(left: 8),
                                      padding:
                                      const EdgeInsets.fromLTRB(12, 4, 12, 4),
                                      child: Text("Tất cả",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: controller.isAllClass.value
                                                  ? Colors.white
                                                  : const Color.fromRGBO(
                                                  90, 90, 90, 1))),
                                    ),
                                  ),
                                  Expanded(
                                      child: Container(
                                        height: 42,
                                        padding: const EdgeInsets.all(8),
                                        child: ListView.builder(
                                            itemCount: controller
                                                .classOfTeacher.length,
                                            scrollDirection: Axis.horizontal,
                                            itemBuilder: (context, index) {
                                              return itemClassId(index);
                                            }),
                                      ))
                                ],
                              ),
                            ))),
                    viewNotify(context,NotificationController.today),
                    viewNotify(context,NotificationController.before),
                  ],
                ),
              )),
            ),
            onRefresh: () async {
              controller.onRefresh();
            }),
      ):const Center(
        child: CircularProgressIndicator(
          color: ColorUtils.PRIMARY_COLOR,
        ),
      );
    },);
  }

  itemClassId(int index) {
    return InkWell(
        onTap: () {
          controller.classUId.value = controller.classOfTeacher[index];
          controller.getColorClass(controller.classUId.value, index);
          controller
              .getListNotificationByClass(controller.classUId.value.classId);
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              width: 100,
              height: 26,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: controller.classOfTeacher[index].checked
                    ? const Color.fromRGBO(249, 154, 81, 1)
                    : const Color.fromRGBO(246, 246, 246, 1),
                borderRadius: BorderRadius.circular(6),
              ),
              margin: const EdgeInsets.only(right: 8),
              padding: const EdgeInsets.fromLTRB(12, 4, 12, 4),
              child: Text(
                "${controller.classOfTeacher[index].name}",
                style: TextStyle(
                    fontSize: 12,
                    color: controller.classOfTeacher[index].checked
                        ? Colors.white
                        : const Color.fromRGBO(90, 90, 90, 1)),
              ),
            ),
            (controller.classOfTeacher[index].homeroomClass == true)
                ? Positioned(
                    right: 13,
                    top: 4,
                    child: Icon(
                      Icons.star,
                      size: 8,
                      color: controller.classOfTeacher[index].checked
                          ? Colors.white
                          : const Color.fromRGBO(90, 90, 90, 1),
                    ),
                  )
                : Container()
          ],
        ));
  }


  viewNotify(context,type){
    return Container(
      margin: EdgeInsets.only(top: 16.h),
      padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Obx(() => InkWell(
            onTap: () {
              switch(type){
                case NotificationController.today:
                  controller.isExpandToday.value =! controller.isExpandToday.value;
                  break;
                case NotificationController.before:
                  controller.isExpandBefore.value =! controller.isExpandBefore.value;
                  break;
              }
            },
            child: Row(
              children: [
                Padding(padding: EdgeInsets.only(left: 6.w)),
                Visibility(
                    visible: getIsExpand(type) == false,
                    child: Text(
                      getTextNotify(type),
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 12.sp,
                          color: const Color.fromRGBO(133, 133, 133, 1),
                          fontFamily: 'static/Inter-Medium.ttf',
                          fontWeight: FontWeight.bold),
                    )),
                Visibility(
                    visible: getIsExpand(type) == true,
                    child: Visibility(
                        visible: getIsChooseMany(type),
                        child: InkWell(
                          onTap: () {
                            switch(type){
                              case NotificationController.today:
                                controller.isCheckAllToday.value = !controller.isCheckAllToday.value;
                                controller.isChooseManyToday.refresh();
                                controller.checkAllToday();
                                break;
                              case NotificationController.before:
                                controller.isCheckAllBefore.value = !controller.isCheckAllBefore.value;
                                controller.isChooseManyBefore.refresh();
                                controller.checkAllBefore();
                                break;
                            }
                          },
                          child: Row(
                            children: [


                              Container(
                                  width: 18.h,
                                  height: 18.h,
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.all(2.h),
                                  decoration: ShapeDecoration(
                                    shape: CircleBorder(
                                        side: BorderSide(
                                            color: getIsCheckAll(type)
                                                ? ColorUtils.PRIMARY_COLOR
                                                : const Color.fromRGBO(235, 235, 235, 1))),
                                  ),
                                  child: getIsCheckAll(type)
                                      ? const Icon(Icons.circle, size: 12, color: ColorUtils.PRIMARY_COLOR,
                                  )
                                      : null),
                              const Padding(padding: EdgeInsets.only(left: 6)),
                              Text(
                                "Chọn hết",
                                style: TextStyle(
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.black),
                              ),
                            ],
                          ),
                        ))),
                Visibility(
                    visible: getIsExpand(type)== true,
                    child: Visibility(
                        visible: getIsChooseMany(type),
                        child: Padding(padding: EdgeInsets.only(left: 8.w)))),
                Visibility(
                    visible: getIsExpand(type) == true,
                    child: Visibility(
                        visible: getIsChooseMany(type),
                        child: TextButton(
                            onPressed: () {
                              switch(type){
                                case NotificationController.today:
                                  controller.listCheckBoxToday.value = [];
                                  for(int i = 0;i<controller.listNotifyToday.length;i++){
                                    controller.listCheckBoxToday.add(false) ;
                                  }
                                  controller.listCheckBoxToday.refresh();
                                  if(controller.listCheckBoxToday.contains(false)){
                                    controller.isCheckAllToday.value = false;
                                  }
                                  controller.isCheckAllToday.refresh();
                                  controller.isChooseManyToday.value = false;
                                  controller.listIdNotifyToday.value = [];
                                  break;
                                case NotificationController.before:
                                  controller.listCheckBoxBefore.value = [];
                                  for(int i = 0;i<controller.listBeforeNotify.length;i++){
                                    controller.listCheckBoxBefore.add(false) ;
                                  }
                                  controller.listCheckBoxBefore.refresh();
                                  if(controller.listCheckBoxBefore.contains(false)){
                                    controller.isCheckAllBefore.value = false;
                                  }
                                  controller.isCheckAllBefore.refresh();
                                  controller.isChooseManyBefore.value = false;
                                  controller.listIdNotifyBefore.value = [];
                                  break;
                              }
                            },
                            child: const Text("Hủy", style: TextStyle(color: Colors.red,),)))),
                Visibility(
                    visible: getIsExpand(type) == true,
                    child: Visibility(
                        visible: getIsChooseMany(type),
                        child: Padding(padding: EdgeInsets.only(left: 6.w)))),
                Visibility(
                    visible: getIsExpand(type) == true,
                    child: Text(
                      getTextNotify(type),
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 12.sp,
                          color: getIsChooseMany(type) == true
                              ? ColorUtils.PRIMARY_COLOR
                              : const Color.fromRGBO(133, 133, 133, 1),
                          fontFamily: 'static/Inter-Medium.ttf',
                          fontWeight: FontWeight.bold),
                    )),
                Expanded(child: Container()),
                Visibility(
                    visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_NOTIFY_SEEN),
                    child: Visibility(
                        visible: getIsExpand(type) == true,
                        child: Visibility(
                            visible: getIsChooseMany(type),
                            child: InkWell(
                              onTap: () {
                                switch(type){
                                  case NotificationController.today:
                                    controller.seenNotification(controller.listIdNotifyToday,type);
                                    controller.listIdNotifyToday.clear();
                                    if(controller.listCheckBoxToday.contains(false)){
                                      controller.isCheckAllToday.value = false;
                                    }
                                    controller.isCheckAllToday.refresh();
                                    break;
                                  case NotificationController.before:
                                    controller.seenNotification(controller.listIdNotifyBefore,type);
                                    controller.listIdNotifyBefore
                                        .clear();
                                    if(controller.listCheckBoxBefore.contains(false)){
                                      controller.isCheckAllBefore.value = false;
                                    }
                                    controller.isCheckAllBefore.refresh();
                                    break;
                                }
                                Get.find<HomeController>().getCountNotifyNotSeen();
                              },
                              child: Icon(
                                Icons.check,
                                size: 24,
                                color: controller.setColorIconCheck(type),
                              ),
                            )))),
                Visibility(
                    visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_NOTIFY_DELETE),
                    child: Visibility(
                        visible: getIsExpand(type) == true,
                        child: Visibility(
                            visible: getIsChooseMany(type),
                            child: InkWell(
                              onTap: () {
                                Get.dialog(Center(
                                  child: Wrap(children: [
                                    Container(
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          borderRadius: BorderRadius.circular(16.r)),
                                      child: Stack(
                                        children: [
                                          Container(
                                            margin: const EdgeInsets.only(top: 50, right: 50, left: 50),
                                            decoration: BoxDecoration(
                                                color: Colors.white, borderRadius: BorderRadius.circular(16.r)),
                                            child: Column(
                                              children: [
                                                Padding(padding: EdgeInsets.only(top: 32.h)),
                                                Container(
                                                  margin: EdgeInsets.symmetric(horizontal: 16.w),
                                                  child: Text(
                                                    "Bạn có chắc muốn xóa những thông báo này",
                                                    style: TextStyle(
                                                        color: const Color.fromRGBO(133, 133, 133, 1), fontSize: 14.sp),
                                                  ),
                                                ),
                                                Container(
                                                    color: Colors.white,
                                                    padding: EdgeInsets.all(16.h),
                                                    child: SizedBox(
                                                      width: double.infinity,
                                                      height: 46,
                                                      child: ElevatedButton(
                                                          style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                                                          onPressed: () {
                                                            switch(type){
                                                              case NotificationController.today:
                                                                controller.deleteNotification(controller.listIdNotifyToday,type);
                                                                controller.listIdNotifyToday.clear();
                                                                break;
                                                              case NotificationController.before:
                                                                controller.deleteNotification(controller.listIdNotifyBefore,type);
                                                                controller.listIdNotifyBefore.clear();
                                                                break;
                                                            }
                                                          },
                                                          child: Text('Xóa', style: TextStyle(color: Colors.white, fontSize: 16.sp),)),
                                                    )),
                                                TextButton(
                                                    onPressed: () {
                                                      Get.back();
                                                    },
                                                    child: Text("Hủy", style: TextStyle(color: Colors.red, fontSize: 16.sp),))
                                              ],
                                            ),
                                          ),
                                          Container(
                                              alignment: Alignment.center,
                                              height: 80,
                                              child: Image.asset("assets/images/image_app_logo.png"))
                                        ],
                                      ),
                                    ),
                                  ]),
                                ));
                              },
                              child: Icon(
                                Icons.delete,
                                size: 24,
                                color: controller.setColorIconDelete(type),
                              ),
                            )))),
                Row(
                  children: [
                    Text( getIsExpand(type) ? 'Thu Gọn': 'Mở Rộng',style: TextStyle(color: const Color.fromRGBO(26, 59, 112, 1),fontSize: 12.sp),),
                    Icon( getIsExpand(type) ?Icons.keyboard_arrow_up_outlined : Icons.keyboard_arrow_down_outlined,size: 24,color: const Color.fromRGBO(26, 59, 112, 1),),
                  ],
                ),
              ],
            ),

          )),
          getListNotify(type)!.isEmpty
              ? Visibility(
              visible:getIsExpand(type) == true,
              child: const Center(
                  child: Text("Bạn chưa có thông báo")))
              : Visibility(
              visible: getIsExpand(type) == true,
              child: SizedBox(
                height: MediaQuery.of(context).size.height/1.8,
                child:  ListView.builder(
                    shrinkWrap: true,
                    controller: getScrollController(type),
                    itemCount: getListNotify(type)!.length,
                    itemBuilder: (context, index) {
                      var showLine = index ==
                          getListNotify(type)!.length - 1
                          ? false
                          : true;
                      return Obx(() => Stack(
                        children: [
                          InkWell(
                            onTap: () {
                              if(getIsChooseMany(type)){
                                switch(type){
                                  case NotificationController.today:
                                    controller.checkBoxListView(index,type);
                                    if (controller.listCheckBoxToday.contains(false) == true) {
                                      controller.isCheckAllToday.value = false;
                                    } else {
                                      controller.isCheckAllToday.value = true;
                                    }
                                    break;
                                  case NotificationController.before:
                                    controller.checkBoxListView(index,type);
                                    if (controller.listCheckBoxBefore.contains(false) == true) {
                                      controller.isCheckAllBefore.value = false;
                                    } else {
                                      controller.isCheckAllBefore.value = true;
                                    }
                                    break;
                                }
                              }else{
                                checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> controller.clickNotify(getListNotify(type),index,type),StringConstant.FEATURE_NOTIFY_DETAIL);
                              }
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  color:
                                  const Color.fromRGBO(
                                      255, 255, 255, 1),
                                  margin: EdgeInsets.only(top: 0.h, bottom: 0.h, left: 8.w, right: 0.h),
                                  alignment:
                                  Alignment.center,
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Visibility(
                                          visible: getIsChooseMany(type),
                                          child: InkWell(
                                            onTap: () {
                                              switch(type){
                                                case NotificationController.today:
                                                  controller.checkBoxListView(index,type);
                                                  if (controller.listCheckBoxToday.contains(false) == true) {
                                                    controller.isCheckAllToday.value = false;
                                                  } else {
                                                    controller.isCheckAllToday.value = true;
                                                  }
                                                  break;
                                                case NotificationController.before:
                                                  controller.checkBoxListView(index,type);
                                                  if (controller.listCheckBoxBefore.contains(false) == true) {
                                                    controller.isCheckAllBefore.value = false;
                                                  } else {
                                                    controller.isCheckAllBefore.value = true;
                                                  }
                                                  break;
                                              }
                                            },
                                            child: Container(
                                                width: 18.h,
                                                height: 18.h,
                                                alignment: Alignment.center,
                                                padding: EdgeInsets.all(2.h),
                                                margin: const EdgeInsets.only(right: 8),
                                                decoration: ShapeDecoration(
                                                  shape: CircleBorder(
                                                      side: BorderSide(color: getListCheckBox(type)[index] ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(235, 235, 235, 1))),
                                                ),
                                                child: getListCheckBox(type)[index]
                                                    ? const Icon(Icons.circle, size: 12,
                                                  color:ColorUtils.PRIMARY_COLOR,
                                                )
                                                    : null),
                                          )),
                                      SizedBox(
                                        width: 36.h,
                                        height: 36.h,
                                        child: CacheNetWorkCustom(urlImage: '${ getListNotify(type)![index].createdBy!.image}'),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.only(left: 4.w),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                margin: EdgeInsets.only(left: 4.w),
                                                child: SizedBox(
                                                  width: 270.w,
                                                  child: RichText(
                                                      text: TextSpan(children: [
                                                        TextSpan(
                                                            text: '[${getListNotify(type)![index].typeNotify}] ',
                                                            style: TextStyle(fontWeight: FontWeight.bold, color:ColorUtils.PRIMARY_COLOR, fontSize: 14.sp)),
                                                        TextSpan(
                                                            text: ' ${getListNotify(type)![index].title!}',
                                                            style: const TextStyle(color: Color.fromRGBO(26, 26, 26, 1), fontSize: 16)),
                                                      ])),
                                                ),
                                              ),
                                              SizedBox(
                                                height: 4.h,
                                              ),
                                              Container(
                                                margin:  EdgeInsets.only(left: 4.w),
                                                child: Text("${parse(parse(getListNotify(type)![index].body).body?.text).documentElement?.text}"),
                                              ),
                                              SizedBox(
                                                height: 4.h,
                                              ),
                                              Container(
                                                width: 270.w,
                                                margin:  EdgeInsets.only(left: 4.w),
                                                child: Text(
                                                  DateTimeUtils.convertToAgo(DateTime.fromMillisecondsSinceEpoch(getListNotify(type)![index].createdAt ?? 0).toLocal()),
                                                  textAlign: TextAlign.left,
                                                  style: const TextStyle(color: Color.fromRGBO(114, 116, 119, 1), fontSize: 12),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin:
                                        EdgeInsets.only(
                                            bottom:
                                            32.h),
                                        child:
                                        PopupMenuButton(
                                            padding: EdgeInsets.zero,
                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(6.0.r))),
                                            icon: const Icon(Icons.more_horiz_outlined,
                                              color: Color.fromRGBO(177, 177, 177, 1),
                                            ),
                                            itemBuilder: (context) {
                                              return controller.getListItemPopupMenu();
                                            },
                                            onSelected:
                                                (value) {switch (value) {
                                                case 0:
                                                  switch(type){
                                                    case NotificationController.today:
                                                      controller.isChooseManyToday.value = true;
                                                      if (controller.listCheckBoxToday.contains(false) == true) {
                                                        controller.isCheckAllToday.value = false;
                                                      } else {
                                                        controller.isCheckAllToday.value = true;
                                                      }
                                                      break;
                                                    case NotificationController.before:
                                                      controller.isChooseManyBefore.value = true;
                                                      if (controller.listCheckBoxBefore.contains(false) == true) {
                                                        controller.isCheckAllBefore.value = false;
                                                      } else {
                                                        controller.isCheckAllBefore.value = true;
                                                      }
                                                      break;
                                                  }
                                                  break;
                                                case 1:
                                                  Get.dialog(Center(
                                                    child: Wrap(children: [
                                                      Container(
                                                        width: double.infinity,
                                                        decoration: BoxDecoration(color: Colors.transparent,
                                                            borderRadius: BorderRadius.circular(16)),
                                                        child: Stack(
                                                          children: [
                                                            Container(
                                                              margin:  EdgeInsets.only(top: 40.h, right: 50.w, left: 50.w),
                                                              decoration: BoxDecoration(
                                                                  color: Colors.white, borderRadius: BorderRadius.circular(16)),
                                                              child: Column(
                                                                children: [
                                                                  Padding(padding: EdgeInsets.only(top: 32.h)),
                                                                  Text(
                                                                    "Bạn có chắc muốn xóa Thông báo này",
                                                                    style: TextStyle(
                                                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                                                        fontSize: 14.sp),
                                                                  ),
                                                                  Container(
                                                                      color: Colors.white,
                                                                      padding:
                                                                      EdgeInsets.all(16.h),
                                                                      child: SizedBox(
                                                                        width: double.infinity,
                                                                        height: 46,
                                                                        child: ElevatedButton(
                                                                            style: ElevatedButton.styleFrom(backgroundColor:  ColorUtils.PRIMARY_COLOR),
                                                                            onPressed: () {
                                                                              switch(type){
                                                                                case NotificationController.today:
                                                                                  controller.listIdNotifyToday.add(controller.listNotifyToday[index].id!);
                                                                                  controller.deleteNotification(controller.listIdNotifyToday,type);
                                                                                  controller.listIdNotifyToday.clear();
                                                                                  break;
                                                                                case NotificationController.before:
                                                                                  controller.listIdNotifyBefore.add(controller.listBeforeNotify[index].id!);
                                                                                  controller.deleteNotification(controller.listIdNotifyBefore,type);
                                                                                  controller.listIdNotifyBefore.clear();
                                                                                  break;
                                                                              }
                                                                            },
                                                                            child: Text('Xóa', style: TextStyle(color: Colors.white, fontSize: 16.sp),
                                                                            )),
                                                                      )),
                                                                  TextButton(
                                                                      onPressed: () {
                                                                        Get.back();
                                                                      },
                                                                      child: Text("Hủy", style: TextStyle(color: Colors.red, fontSize: 16.sp),
                                                                      ))
                                                                ],
                                                              ),
                                                            ),
                                                            Container(
                                                                alignment: Alignment.center,
                                                                margin: const EdgeInsets.only(top: 10),
                                                                height: 80,
                                                                child: Image.asset("assets/images/image_app_logo.png"))
                                                          ],
                                                        ),
                                                      ),
                                                    ]),
                                                  ));
                                                  break;
                                                case 2:
                                                  switch(type){
                                                    case NotificationController.today:
                                                      controller.listIdNotifyToday.add(controller.listNotifyToday[index].id!);
                                                      controller.seenNotification(controller.listIdNotifyToday,type);
                                                      controller.listIdNotifyToday.clear();
                                                      break;
                                                    case NotificationController.before:
                                                      controller.listIdNotifyBefore.add(controller.listBeforeNotify[index].id!);
                                                      controller.seenNotification(controller.listIdNotifyBefore,type);
                                                      controller.listIdNotifyBefore.clear();
                                                      break;
                                                  }
                                                  break;
                                              }
                                            }),
                                      ),
                                    ],
                                  ),
                                ),
                                (showLine)
                                    ? const Divider(
                                  indent: 10,
                                  endIndent: 10,
                                )
                                    : Container()
                              ],
                            ),
                          ),
                          Visibility(
                              visible: getListNotify(type)![index].status == "NOTSEEN",
                              child: const Positioned(
                                  bottom: 32, right: 24,
                                  child: Icon(Icons.circle, color: ColorUtils.PRIMARY_COLOR, size: 10,)))
                        ],
                      ));
                    }),
              ))
        ],
      ),
    );
  }


  getIsExpand(type){
    switch(type){
      case NotificationController.today:
        return controller.isExpandToday.value;
      case NotificationController.before:
        return controller.isExpandBefore.value;
    }
  }

  getIsChooseMany(type){
    switch(type){
      case NotificationController.today:
        return controller.isChooseManyToday.value;
      case NotificationController.before:
        return controller.isChooseManyBefore.value;
    }
  }

  getIsCheckAll(type){
    switch(type){
      case NotificationController.today:
        return controller.isCheckAllToday.value;
      case NotificationController.before:
        return controller.isCheckAllBefore.value;
    }
  }

  getListNotify(type){
    switch(type){
      case NotificationController.today:
        return controller.listNotifyToday;
      case NotificationController.before:
        return controller.listBeforeNotify;
      default :
        return [];
    }
  }

  getListCheckBox(type){
    switch(type){
      case NotificationController.today:
        return controller.listCheckBoxToday;
      case NotificationController.before:
        return controller.listCheckBoxBefore;
      default :
        return [];
    }
  }

  getScrollController(type){
    switch(type){
      case NotificationController.today:
        return controller.controllerToday;
      case NotificationController.before:
        return controller.controllerBefore;
    }
  }

  getTextNotify(type){
    switch(type){
      case NotificationController.today:
        return "Hôm nay";
      case NotificationController.before:
        return "Trước đó";
    }
  }
}
