import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/view_exam_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/grading_exercise/list_student_submitted_exercise/list_student_submit_exercise_controller.dart';
import '../../../commom/utils/app_utils.dart';
import '../../../commom/utils/click_subject_teacher.dart';
import '../../../data/base_service/api_response.dart';
import '../../../data/model/common/subject.dart';
import '../../../data/model/res/notification/notification.dart';
import '../../../data/model/res/class/classTeacher.dart';
import '../../../data/repository/notification_repo/notification_repo.dart';
import '../../../data/repository/subject/class_office_repo.dart';
import '../home/home_controller.dart';
import '../role/parent/diligence_parent/approval_parent/approval_parent_controller.dart';
import '../role/parent/parent_home_controller.dart';
import '../role/student/student_home_controller.dart';
import '../role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/grading_exam/list_student_submitted_exam/list_student_submit_exam_controller.dart';
import '../role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/view_exercise_controller.dart';

class NotificationController extends GetxController {
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy hh:mm:ss');
  final NotificationRepo _notificationRepo = NotificationRepo();
  var notify = Notify().obs;
  var itemsNotification = ItemsNotification().obs;
  var listNotifyToday = <NotifyList>[].obs;
  var listBeforeNotify = <NotifyList>[].obs;
  var listNotifyTodayLoadMore = <NotifyList>[].obs;
  var listBeforeNotifyLoadMore = <NotifyList>[].obs;
  var listIdNotifyToday = <String>[].obs;
  var listIdNotifyBefore = <String>[].obs;
  var listCheckBoxToday = <bool>[].obs;
  var listCheckBoxBefore = <bool>[].obs;
  var isChooseManyBefore = false.obs;
  var isChooseManyToday = false.obs;
  var isCheckAllBefore = false.obs;
  var isCheckAllToday = false.obs;
  final ClassOfficeRepo _classRepo = ClassOfficeRepo();
  RxList<ClassId> classOfTeacher = <ClassId>[].obs;
  var classUId = ClassId().obs;
  var isAllClass = true.obs;
  var controllerBefore = ScrollController();
  var controllerToday = ScrollController();
  var indexPageToday = 1.obs;
  var indexPageBefore = 1.obs;
  var isExpandToday = true.obs;
  var isExpandBefore = true.obs;
  var listItemPopupMenu = <PopupMenuItem>[];
  var showDividerMultiple = false;
  var showDelete = false;
  var showSeen = false;
  var indexChooseMultiple = 0;
  var indexDeleteNotify = 0;
  var indexSeenNotify = 0;
  var heightChooseMultiple = 30.0;
  var heightDeleteNotify = 30.0;
  var heightSeenNotify = 30.0;
  var subjectId  = "";
  var isReady = false;
  var detailSubject = SubjectRes().obs;

  static const String today = "Today";
  static const String before = "Before";
  @override
  void onInit() {
    controllerBefore = ScrollController()..addListener(_scrollListenerBefore);
    controllerToday = ScrollController()..addListener(_scrollListenerToday);
    super.onInit();
  }


  @override
  dispose() {
    controllerBefore.dispose();
    controllerToday.dispose();
    super.dispose();
  }

  void _scrollListenerBefore() {
    String? classId;
    if(AppCache().userType == "TEACHER"){
      classId = "";
    }else if(AppCache().userType == "STUDENT"){
      classId = Get.find<StudentHomeController>().clazzs[0].id??"";
    }
    else if(AppCache().userType == "PARENT"){
      classId = Get.find<ParentHomeController>().currentStudentProfile.value.clazz![0].id??"";
    }
    if (controllerBefore.position.pixels == controllerBefore.position.maxScrollExtent) {
      _notificationRepo.getListNotify(classId, indexPageBefore.value, 20).then((value) {
        if (value.state == Status.SUCCESS) {
          listBeforeNotifyLoadMore.value = [];
          listBeforeNotifyLoadMore.value = value.object!.items!.beforeNotifyList!;
          if(listBeforeNotifyLoadMore.isNotEmpty){
            indexPageBefore.value++;
          }
          listBeforeNotifyLoadMore.refresh();
          listBeforeNotify.addAll(listBeforeNotifyLoadMore);
          listCheckBoxBefore.value = [];
          for (int i = 0; i < listBeforeNotify.length; i++) {
            if(listIdNotifyBefore.contains(listBeforeNotify[i].id)){
              listCheckBoxBefore.add(true);
            }else{
              listCheckBoxBefore.add(false);
            }
          }
          if (listCheckBoxBefore.contains(false) == true) {
            isCheckAllBefore.value = false;
          } else {
            isCheckAllBefore.value = true;
          }
          listBeforeNotify.refresh();
          listCheckBoxBefore.refresh();
        }
      });
    }
  }

  void _scrollListenerToday() {
    String? classId;
    if(AppCache().userType == "TEACHER"){
      classId = "";
    }else if(AppCache().userType == "STUDENT"){
      classId = Get.find<StudentHomeController>().clazzs[0].id??"";
    }
    else if(AppCache().userType == "PARENT"){
      classId = Get.find<ParentHomeController>().currentStudentProfile.value.clazz![0].id??"";
    }
    if (controllerToday.position.pixels == controllerToday.position.maxScrollExtent) {
      if(listNotifyTodayLoadMore.isNotEmpty){
        indexPageToday.value++;
      }
      listNotifyTodayLoadMore.value = [];
      _notificationRepo.getListNotify(classId, indexPageToday.value, 20).then((value) {
        if (value.state == Status.SUCCESS) {
          listNotifyTodayLoadMore.value = value.object!.items!.todayNotifyList!;
        }
      });
      listNotifyToday.addAll(listNotifyTodayLoadMore);
      listCheckBoxToday.value = [];
      for (int i = 0; i < listNotifyToday.length; i++) {
        listCheckBoxToday.add(false);
      }
      for (int i = 0; i < listNotifyToday.length; i++) {
        if(listIdNotifyToday.contains(listNotifyToday[i].id)){
          listCheckBoxToday.add(true);
        }else{
          listCheckBoxToday.add(false);
        }
      }
      if (listCheckBoxToday.contains(false) == true) {
        isCheckAllToday.value = false;
      } else {
        isCheckAllToday.value = true;
      }
      listNotifyToday.refresh();
    }
  }

  setTypeNotification(type) {
    switch (type) {
      case "LEAVE_APPLICATION":
        return "Đơn xin nghỉ phép";
      case "DILIGENT":
        return "CHUYÊN CẦN";
      case "STUDY":
        return "HỌC TẬP";
      default:
        return type;
    }
  }

  setStatusNotification(type) {
    switch (type) {
      case "NOTSEEN":
        return "Chưa xem";
      case "SEEN":
        return "Đã  xem";
    }
  }


  checkBoxListView(index,type){
    switch(type){
      case today:
        if (listCheckBoxToday[index] == false) {
          listCheckBoxToday[index] = true;
          listIdNotifyToday.add(listNotifyToday[index].id!);
        } else {
          listCheckBoxToday[index] = false;
          listIdNotifyToday.remove(listNotifyToday[index].id!);
        }
        break;

      case before:
        if (listCheckBoxBefore[index] == false) {
          listCheckBoxBefore[index] = true;
          listIdNotifyBefore.add(listBeforeNotify[index].id!);
        } else {
          listCheckBoxBefore[index] = false;
          listIdNotifyBefore.remove(listBeforeNotify[index].id!);
        }
        break;


    }
  }

  checkAllBefore() {
    if (isCheckAllBefore.value == true) {
      listCheckBoxBefore.value = [];
      for (int i = 0; i < listBeforeNotify.length; i++) {
        listCheckBoxBefore.add(true);
        listIdNotifyBefore.add(listBeforeNotify[i].id!);
      }
      listCheckBoxBefore.refresh();
    } else {
      listCheckBoxBefore.value = [];
      for (int i = 0; i < listBeforeNotify.length; i++) {
        listCheckBoxBefore.add(false);
      }
      listIdNotifyBefore.clear();
      listCheckBoxBefore.refresh();
    }
  }

  checkAllToday() {
    if (isCheckAllToday.value == true) {
      listCheckBoxToday.value = [];
      for (int i = 0; i < listNotifyToday.length; i++) {
        listCheckBoxToday.add(true);
        listIdNotifyToday.add(listNotifyToday[i].id!);
      }
      listCheckBoxToday.refresh();
    } else {
      listCheckBoxToday.value = [];
      for (int i = 0; i < listNotifyToday.length; i++) {
        listCheckBoxToday.add(false);
      }
      listIdNotifyToday.clear();
      listCheckBoxToday.refresh();
    }
  }

  setColorIconDelete(type) {
    switch(type){
      case today:
        if (listCheckBoxToday.contains(true)) {
          return const Color.fromRGBO(255, 69, 89, 1);
        } else {
          return const Color.fromRGBO(177, 177, 177, 1);
        }
      case before:
        if (listCheckBoxBefore.contains(true) == true) {
          return const Color.fromRGBO(255, 69, 89, 1);
        } else {
          return const Color.fromRGBO(177, 177, 177, 1);
        }
    }
  }

  setColorIconCheck(type) {
    switch(type){
      case today:
        if (listCheckBoxBefore.contains(true) == true) {
          return ColorUtils.PRIMARY_COLOR;
        } else {
          return const Color.fromRGBO(177, 177, 177, 1);
        }
      case before:
        if (listCheckBoxBefore.contains(true) == true) {
          return ColorUtils.PRIMARY_COLOR;
        } else {
          return const Color.fromRGBO(177, 177, 177, 1);
        }
    }
  }

  getListNotification() async {
    String? classId;
    if(AppCache().userType == "TEACHER"){
      classId = "";
    }else if(AppCache().userType == "STUDENT"){
      classId = Get.find<StudentHomeController>().clazzs[0].id??"";
    }
    else if(AppCache().userType == "PARENT"){
      classId = Get.find<ParentHomeController>().currentStudentProfile.value.clazz![0].id??"";
    }
    await _notificationRepo.getListNotify(classId, 0, 20).then((value) {
      if (value.state == Status.SUCCESS) {
        notify.value = value.object!;
        itemsNotification.value = notify.value.items!;
        listNotifyToday.value = itemsNotification.value.todayNotifyList!;
        listBeforeNotify.value = itemsNotification.value.beforeNotifyList!;
        listCheckBoxToday.value = [];
        for (int i = 0; i < listNotifyToday.length; i++) {
          listCheckBoxToday.add(false);
        }
        listCheckBoxBefore.value = [];
        for (int i = 0; i < listBeforeNotify.length; i++) {
          listCheckBoxBefore.add(false);
        }
      }
    });
    isReady = true;
    update();
  }

  getListNotificationByClass(classId) async {
    _notificationRepo.getListNotify(classId, 0, 20).then((value) {
      if (value.state == Status.SUCCESS) {
        notify.value = value.object!;
        itemsNotification.value = notify.value.items!;
        listNotifyToday.value = itemsNotification.value.todayNotifyList!;
        listBeforeNotify.value = itemsNotification.value.beforeNotifyList!;
        listCheckBoxToday.value = [];
        for (int i = 0; i < listNotifyToday.length; i++) {
          listCheckBoxToday.add(false);
        }
        listCheckBoxBefore.value = [];
        for (int i = 0; i < listBeforeNotify.length; i++) {
          listCheckBoxBefore.add(false);
        }
      }
    });
  }

  seenNotification(RxList<String> list,type) {
    _notificationRepo.seenNotification(list).then((value) {
      if (value.state == Status.SUCCESS) {
        getListNotification();
        Get.find<HomeController>().getCountNotifyNotSeen();
        switch(type){
          case today:
            isChooseManyToday.value = false;
            listCheckBoxToday.value = [];
            for (int i = 0; i < listNotifyToday.length; i++) {
              listCheckBoxToday.add(false);
            }
            break;
          case before:
            isChooseManyBefore.value = false;
            listCheckBoxBefore.value = [];
            for (int i = 0; i < listBeforeNotify.length; i++) {
              listCheckBoxBefore.add(false);
            }
            break;
        }
      } else {
        AppUtils.shared.snackbarError("Lỗi", value.message ?? "");
      }
    });
  }

  deleteNotification(RxList<String> list,type) {
    _notificationRepo.deleteNotification(list).then((value) {
      if (value.state == Status.SUCCESS) {
        getListNotification();
        Get.find<HomeController>().getCountNotifyNotSeen();
        switch(type){
          case today:
            isChooseManyToday.value = false;
            break;
          case before:
            isChooseManyBefore.value = false;
            break;
        }
        Get.back();
      } else {
        AppUtils.shared.snackbarError("Lỗi", value.message ?? "");
      }
    });
  }

  getListClassIdByTeacher() async {
    var teacherId = AppCache().userId ?? "";
    _classRepo.listClassByTeacher(teacherId).then((value) {
      if (value.state == Status.SUCCESS) {
        classOfTeacher.value = value.object!;
        for (var f in classOfTeacher) {
          f.checked = false;
        }

        classOfTeacher.refresh();
      }
    });
  }

  selectedClass(ClassId classId, index) {
    for (var f in classOfTeacher) {
      if (f.id != classId.id) {
        f.checked = false;
      } else {
        f.checked = true;
      }
    }
    classOfTeacher.refresh();
  }

  getColorClass(ClassId classId, index) {
    isAllClass.value = false;
    for (var f in classOfTeacher) {
      if (f.id != classId.id) {
        f.checked = false;
      } else {
        f.checked = true;
      }
    }
    classOfTeacher.refresh();
  }

  onRefresh() {
    indexPageToday.value = 1;
    indexPageBefore.value = 1;
    getListNotification();
    if(AppCache().userType == "TEACHER"){
      getListClassIdByTeacher();
    }
  }

  clickNotify(RxList<NotifyList> list,index,type){
    subjectId = list[index].detail!.subjectId??"";
    RxBool isChooseMany = false.obs;
    switch(type){
      case today:
        isChooseMany.value == isChooseManyToday.value;
        break;
      case before:
        isChooseMany.value == isChooseManyBefore.value;
        break;
    }
    if(isChooseMany.value == true){
      checkBoxListView(index,type);
      switch(type){
        case today:
          if (listCheckBoxToday.contains(false) == true) {
            isCheckAllToday.value = false;
          } else {
            isCheckAllToday.value = true;
          }
          break;
        case before:
          if (listCheckBoxBefore.contains(false) == true) {
            isCheckAllBefore.value = false;
          } else {
            isCheckAllBefore.value = true;
          }
          break;
      }

    }else{
      if (list[index].type == "LEAVE_APPLICATION") {
        if (AppCache().userType == "TEACHER") {
          if (list[index].detail?.status == "PENDING") {
            Get.find<HomeController>().comeBackHome();
            Get.toNamed(Routes.diligentManagementTeacher);
            Get.toNamed(Routes.managerLeaveApplicationTeacherPage);
            Get.toNamed(Routes.detailPendingLeaveApplicationTeacher,
                arguments:list[index].detail?.tranId);
          }
          else if (list[index].detail?.status == "CANCEL") {
            Get.find<HomeController>().comeBackHome();
            Get.toNamed(Routes.diligentManagementTeacher);
            Get.toNamed(Routes.managerLeaveApplicationTeacherPage);
            Get.toNamed(Routes.detailCancelLeaveApplicationTeacher, arguments: list[index].detail?.tranId);
          }
          else {
            Get.find<HomeController>().comeBackHome();
            Get.toNamed(Routes.diligentManagementTeacher);
            Get.toNamed(Routes.managerLeaveApplicationTeacherPage);
            Get.toNamed(Routes.detailApprovedLeaveApplicationTeacher,
                arguments: list[index].detail?.tranId);
          }
        }
        else if (AppCache().userType == "PARENT") {
          Get.find<HomeController>().comeBackHome();
          Get.toNamed(Routes.diligenceParentPage);
          Get.toNamed(Routes.approvalParent);
          if (Get.isRegistered<ApprovalParentController>()) {
            Get.find<ApprovalParentController>().onInit();
          }
          Get.toNamed(Routes.detailLeaveApplicationParentPage, arguments: list[index].tranId);
        }
      }
      else if (list[index].type == "DILIGENT") {
        if (AppCache().userType == "PARENT") {
          Get.dialog(Center(
            child: Wrap(children: [
              Container(
                height: 150,
                width: 400,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.white, borderRadius:
                    BorderRadius.circular(16)),
                child: Column(
                  children: [
                    Padding(padding: EdgeInsets.only(top: 32.h)),
                    Text("Thông tin chuyên cần", style: TextStyle(color: Colors.black, fontSize: 14.sp),
                    ),
                    Padding(padding: EdgeInsets.only(top: 16.h)),
                    Text(
                      "${list[index].title}",
                      style:  TextStyle(fontSize: 14.sp, color:ColorUtils.PRIMARY_COLOR, fontWeight: FontWeight.w400),
                    ),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    Text("${list[index].body}",
                      style:  TextStyle(fontSize: 14.sp,color: const Color.fromRGBO(26, 26, 26, 1), fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
            ]),
          ));
        }
      }
      else if (list[index].type == "TRANSCRIPT") {
        if (AppCache().userType == "STUDENT"){
          if(list[index].detail!.type == "SYNTHETIC"){
            Get.find<HomeController>().comeBackHome();
            Get.toNamed(Routes.transcriptsSyntheticStudentPage);
            Get.toNamed(Routes.detailTranscriptPage,arguments: list[index].detail!.tranId);
          }else{
            Get.find<HomeController>().comeBackHome();
            Get.toNamed(Routes.detailSubjectStudentPage);
            Get.toNamed(Routes.viewTranscriptSubjectStudentPage);
            Get.toNamed(Routes.detailTranscriptPage,arguments:list[index].detail!.tranId);
          }
        }else if (AppCache().userType == "PARENT"){
          if(list[index].detail!.type == "SYNTHETIC"){
            Get.find<HomeController>().comeBackHome();
            Get.toNamed(Routes.transcriptsSyntheticParentPage);
            Get.toNamed(Routes.detailTranscriptPage,arguments: list[index].detail!.tranId);
          }else{
            Get.find<HomeController>().comeBackHome();
            Get.toNamed(Routes.detailSubjectParentPage);
            Get.toNamed(Routes.viewTranscriptSubjectParentPage);
            Get.toNamed(Routes.detailTranscriptPage,arguments:list[index].detail!.tranId);
          }
        }
      }
      else if (list[index].type == "STUDY"){
        if(AppCache().userType == "STUDENT"){
          Get.find<HomeController>().comeBackHome();
          Get.toNamed(Routes.detailSubjectStudentPage);
          Get.toNamed(Routes.viewExerciseStudentPage);
          Get.toNamed(Routes.detailExerciseStudentPage,arguments: list[index].detail!.tranId);
        }else  if(AppCache().userType == "PARENT"){
          Get.find<HomeController>().comeBackHome();
          Get.toNamed(Routes.detailSubjectParentPage);
          Get.toNamed(Routes.viewExerciseParentPage);
          Get.toNamed(Routes.detailExerciseParentPage,arguments: list[index].detail!.tranId);
        }

      }
      else if(list[index].type == "EXAM" || list[index].type == "POINT_EXAM"){
        if(AppCache().userType == "STUDENT"){
          if(list[index].detail?.startTime != null && list[index].detail?.startTime > DateTime.now().millisecondsSinceEpoch){
            AppUtils.shared.showToast("Bài kiểm tra chưa đến giờ làm!");
          }else{
            Get.find<HomeController>().comeBackHome();
            Get.toNamed(Routes.detailSubjectStudentPage);
            Get.toNamed(Routes.viewExamStudentPage);
            Get.toNamed(Routes.detailExamsStudentPage,arguments: list[index].detail!.tranId);
          }

        }else  if(AppCache().userType == "PARENT"){
          if(list[index].detail?.startTime != null && list[index].detail?.startTime > DateTime.now().millisecondsSinceEpoch){
            AppUtils.shared.showToast("Bài kiểm tra chưa đến giờ làm!");
          }else{
            Get.find<HomeController>().comeBackHome();
            Get.toNamed(Routes.detailSubjectParentPage);
            Get.toNamed(Routes.viewExamParentPage);
            Get.toNamed(Routes.detailExamsParentPage,arguments: list[index].detail!.tranId);
          }
        }

      }
      else if(list[index].type == "POINT_EXERCISE"){
        if(AppCache().userType == "STUDENT"){
          Get.find<HomeController>().comeBackHome();
          Get.toNamed(Routes.detailSubjectStudentPage,arguments:list[index].detail!.subjectId);
          Get.toNamed(Routes.viewExerciseStudentPage);
          Get.toNamed(Routes.detailExerciseStudentPage,arguments: list[index].detail!.tranId);
        }else  if(AppCache().userType == "PARENT"){
          Get.find<HomeController>().comeBackHome();
          Get.toNamed(Routes.detailSubjectParentPage,arguments:list[index].detail!.subjectId);
          Get.toNamed(Routes.viewExerciseParentPage);
          Get.toNamed(Routes.detailExerciseParentPage,arguments: list[index].detail!.tranId);
        }

      }
      else if(list[index].type =="FINISH_EXERCISE"){
        detailSubject.value = list[index].detail!.subject!;
        Get.toNamed(Routes.subjectTeacherPage);
        checkClickFeature(Get.find<HomeController>().userGroupByApp, ()=> Get.toNamed(Routes.subjectLearningManagementTeacherPage)
            ,StringConstant.FEATURE_SUBJECT_DETAIL);
        clickSubjectTeacher();
        Get.toNamed(Routes.viewExerciseTeacherPage);
        WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
          Get.find<ViewExerciseController>().goToGradingExercise(list[index].detail!.itemExercise!);
          WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
            Get.find<ListStudentSubmitExerciseController>().clickStudentDoExercise(list[index].detail?.student!.id);
          });
        });
      }
      else if(list[index].type =="FINISH_EXAM"){
        detailSubject.value = list[index].detail!.subject!;
        Get.toNamed(Routes.subjectTeacherPage);
        checkClickFeature(Get.find<HomeController>().userGroupByApp, ()=> Get.toNamed(Routes.subjectLearningManagementTeacherPage)
            ,StringConstant.FEATURE_SUBJECT_DETAIL);
        clickSubjectTeacher();
        Get.toNamed(Routes.viewExamTeacherPage);
        WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
          Get.find<ViewExamController>().goToGradingExam(list[index].detail!.itemExam!);
          WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
            Get.find<ListStudentSubmitExamController>().clickStudentDoExam(list[index].detail?.student!.id);
          });
        });
      }
      else if(list[index].type == "SUBJECT_NOTIFY"){
        Get.find<HomeController>().comeBackHome();
        if(AppCache().userType == 'STUDENT'){
          Get.toNamed(Routes.detailSubjectStudentPage, );
          checkClickFeature(Get.find<HomeController>().userGroupByApp,
                  () {
                Get.toNamed(Routes.detailNotificationSubjectPage, arguments: list[index].tranId);
              }, StringConstant.FEATURE_NOTIFY_SUBJECT_PIN_DETAIL);
        }else if(AppCache().userType == 'PARENT'){
          Get.toNamed(Routes.detailSubjectParentPage);
          checkClickFeature(Get.find<HomeController>().userGroupByApp, () =>  Get.toNamed(Routes.detailNotificationSubjectPage, arguments:list[index].tranId), StringConstant.FEATURE_NOTIFY_SUBJECT_PIN_DETAIL);
        }
      }
      else if(list[index].type == "OTHER"){
        Get.toNamed(Routes.detailNotifyOtherPage,arguments: list[index].detail!.tranId);
      }
      if (list[index].status == "NOTSEEN") {
        switch(type){
          case today:
            listIdNotifyToday.add(list[index].id!);
            seenNotification(listIdNotifyToday,type);
            listIdNotifyToday.value = [];
            break;
          case before:
            listIdNotifyBefore.add(list[index].id!);
            seenNotification(listIdNotifyBefore,type);
            listIdNotifyBefore.value = [];
            break;
        }
      }
    }
  }

  getListItemPopupMenu(){
    listItemPopupMenu = [];
    var chooseMultiple =  PopupMenuItem<int>(
        value: 0,
        padding: EdgeInsets.zero,
        height: heightChooseMultiple,
        child: showDividerMultiple
            ?Container(
          padding: EdgeInsets.symmetric(vertical: 4.h),
          decoration:  const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black26))),
          child: Row(
            children: [
              Padding(padding: EdgeInsets.only(right: 8.w)),
              SizedBox(
                height: 20,
                width: 20,
                child: Image.asset("assets/images/image_pick_notify.png"),
              ),
              Padding(padding: EdgeInsets.only(right: 8.w)),
              Text(
                "Chọn nhiều thông báo",
                style: TextStyle(fontSize: 14.sp),
              )
            ],
          ),
        )
            :Row(
          children: [
            Padding(padding: EdgeInsets.only(right: 8.w)),
            SizedBox(
              height: 20,
              width: 20,
              child: Image.asset("assets/images/image_pick_notify.png"),
            ),
            Padding(padding: EdgeInsets.only(right: 8.w)),
            Text(
              "Chọn nhiều thông báo",
              style: TextStyle(fontSize: 14.sp),
            )
          ],
        )
    );
    var deleteNotify =  PopupMenuItem<int>(
      padding: EdgeInsets.zero,
      value: 1,
      height: heightDeleteNotify,
      child: showDelete
          ?Container(
        padding: EdgeInsets.symmetric(vertical: 4.h),
        decoration:  const BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.black26))),
        child: Row(
          children: [
            Padding(padding: EdgeInsets.only(right: 8.w)),
            const Icon(
              Icons.delete_rounded,
              color: Colors.black,
            ),
            Padding(padding: EdgeInsets.only(right: 8.w)),
            const Text("Xóa thông báo")
          ],
        ),
      )
          :Row(
        children: [
          Padding(padding: EdgeInsets.only(right: 8.w)),
          const Icon(
            Icons.delete_rounded,
            color: Colors.black,
          ),
          Padding(padding: EdgeInsets.only(right: 8.w)),
          const Text("Xóa thông báo")
        ],
      ),
    );
    var seenNotify =   PopupMenuItem<int>(
      value: 2,
      padding: EdgeInsets.zero,
      height: heightSeenNotify,
      child: showSeen
          ?Container(
        padding: EdgeInsets.symmetric(vertical: 4.h),
        decoration:  const BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.black26))),
        child: Row(
          children: [
            Padding(padding: EdgeInsets.only(right: 8.w)),
            const Icon(
              Icons.check,
              color: Colors.black,
            ),
            Padding(padding: EdgeInsets.only(right: 8.w)),
            const Text("Đánh dấu là đã đọc")
          ],
        ),
      )
          :Row(
        children: [
          Padding(padding: EdgeInsets.only(right: 8.w)),
          const Icon(
            Icons.check,
            color: Colors.black,
          ),
          Padding(padding: EdgeInsets.only(right: 8.w)),
          const Text("Đánh dấu là đã đọc")
        ],
      ),
    );
    var shareNotify =   PopupMenuItem<int>(
      value: 3,
      padding: EdgeInsets.zero,
      height: 30,
      child: Row(
        children: [
          Padding(padding: EdgeInsets.only(right: 8.w)),
          SizedBox(
            height: 20,
            width: 20,
            child: Image.asset("assets/images/image_share.png"),
          ),
          Padding(padding: EdgeInsets.only(right: 8.w)),
          const Text(
            "Chia sẻ thông báo",
          )
        ],
      ),
    );

    if(Get.find<HomeController>().userGroupByApp.isEmpty){
      listItemPopupMenu = [chooseMultiple,deleteNotify,seenNotify,shareNotify];
      return listItemPopupMenu;
    }else{
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_NOTIFY_CHOOSE)){
        listItemPopupMenu.add(chooseMultiple);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_NOTIFY_DELETE)){
        listItemPopupMenu.add(deleteNotify);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_NOTIFY_SEEN)){
        listItemPopupMenu.add(seenNotify);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_NOTIFY_SHARE)){
        listItemPopupMenu.add(shareNotify);
      }
      indexChooseMultiple = listItemPopupMenu.indexOf(chooseMultiple);
      indexDeleteNotify = listItemPopupMenu.indexOf(deleteNotify);
      indexSeenNotify = listItemPopupMenu.indexOf(seenNotify);

      showDividerMultiple = setVisiblePopupMenuItem(indexChooseMultiple,listItemPopupMenu);
      showDelete = setVisiblePopupMenuItem(indexDeleteNotify,listItemPopupMenu);
      showSeen = setVisiblePopupMenuItem(indexSeenNotify,listItemPopupMenu);

      heightChooseMultiple = setHeightPopupMenuItem(indexChooseMultiple,listItemPopupMenu);
      heightDeleteNotify = setHeightPopupMenuItem(indexDeleteNotify,listItemPopupMenu);
      heightSeenNotify = setHeightPopupMenuItem(indexSeenNotify,listItemPopupMenu);
      update();
      return listItemPopupMenu;
    }
  }
}
