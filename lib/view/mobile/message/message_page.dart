import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/message/message_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/view/mobile/message/search_message/message_personal/message_personal_controller.dart';
import '../home/home_controller.dart';
import 'list_all_message/list_all_message_page.dart';
import 'list_message_un_read/list_message_un_read_page.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
class MessagePage extends GetView<MessageController> {
  @override
  final controller = Get.put(MessageController());
  MessagePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Text(
          "Tin Nhắn",
          style: TextStyle(color: Colors.white, fontSize: 16.sp),
        ),
        actions: [
          Visibility(
              visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_MESSAGE_ADD),
              child:  IconButton(
                icon: Image.asset("assets/images/icon_edit_message.png", width: 25, height: 25,),
                onPressed: () {
                  Get.toNamed(Routes.sendNewMessagePage);
                },
              ))
        ],
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Obx(() => controller.isReady.value?Column(
          children: [
            Visibility(
                visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_MESSAGE_FIND),
                child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)),
                    color: const Color.fromRGBO(246, 246, 246, 1),
                    margin: EdgeInsets.only(top: 16.h, left: 16.w, right: 16.w),
                    child: TextFormField(
                      readOnly: true,
                      onTap: () {
                        if(Get.isRegistered<MessagePersonalController>()){
                          Get.find<MessagePersonalController>().isSharePost.value = false;
                        }else{
                          Get.put(MessagePersonalController());
                          Get.find<MessagePersonalController>().isSharePost.value = false;
                        }
                        Get.toNamed(Routes.searchMessagePage);
                        Get.find<MessagePersonalController>().update();
                      },
                      decoration: InputDecoration(
                        contentPadding:  EdgeInsets.symmetric(
                            vertical: 7.h, horizontal: 16.w),
                        hintText: "Tìm tin nhắn...",
                        hintStyle: TextStyle(
                            fontSize: 14.sp,
                            color: const Color.fromRGBO(177, 177, 177, 1)),
                        prefixIcon: const Icon(
                          Icons.search,
                          color: Color.fromRGBO(177, 177, 177, 1),
                        ),
                        border: InputBorder.none,
                      ),
                      textAlignVertical: TextAlignVertical.center,
                      cursorColor: ColorUtils.PRIMARY_COLOR,
                    ))),
            TabBar(
                physics: const NeverScrollableScrollPhysics(),
                indicatorColor: ColorUtils.PRIMARY_COLOR,
                indicatorWeight: 2,
                labelColor: Colors.black,
                unselectedLabelColor: Colors.grey,
                controller: controller.tabController,
                onTap: (index) {
                  controller.selectedPageIndex.value = index;
                  controller.tabController?.animateTo(controller.selectedPageIndex.value);
                  controller.update();
                },
                tabs: [
                  Container(
                    height: 60.h,
                    padding: EdgeInsets.only(top: 20.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Tất cả",
                          style: TextStyle(
                              color:
                              controller.selectedPageIndex.value == 0
                                  ? ColorUtils.PRIMARY_COLOR
                                  : const Color.fromRGBO(26, 26, 26, 1),
                              fontSize: 14.sp),
                        ),

                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 20.h),
                    height: 60.h,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Chưa đọc ",
                          style: TextStyle(
                              color:
                              controller.selectedPageIndex.value == 1
                                  ? ColorUtils.PRIMARY_COLOR
                                  : const Color.fromRGBO(26, 26, 26, 1),
                              fontSize: 14.sp),
                        ),
                        Visibility(
                            visible: controller.groupMessage.value.items!.countNotSeen != 0,
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                  color: controller.selectedPageIndex.value == 1
                                      ? ColorUtils.PRIMARY_COLOR
                                      : const Color.fromRGBO(177, 177, 177, 1)),
                              padding: EdgeInsets.fromLTRB(8.w, 4.h, 8.w, 4.h),
                              child: Text(
                                "${controller.groupMessage.value.items!.countNotSeen}",
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 10),
                              ),
                            ))
                      ],
                    ),
                  ),
                ]),
            Expanded(
              child: TabBarView(
                physics: const NeverScrollableScrollPhysics(),
                controller: controller.tabController,
                children: [ListAllMessagePage(),ListMessageUnRead()],
              ),
            )
          ],
        ):const Center(
          child: CircularProgressIndicator(
            color: ColorUtils.PRIMARY_COLOR,
          ),
        )),
      ),
    );
  }
}

