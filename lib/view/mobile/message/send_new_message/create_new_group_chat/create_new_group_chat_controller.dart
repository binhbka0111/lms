import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:slova_lms/data/repository/subject/class_office_repo.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/common/contacts.dart';
import '../../../../../data/model/common/student_by_parent.dart';
import '../../../../../data/model/res/class/classTeacher.dart';


class CreateNewGroupChatController extends GetxController {
  var isExpandStudent = false.obs;
  var indexItemStudent = -1;
  var isExpandParents = false.obs;
  var indexItemParents = -1;

  var isExpandTeacher = false.obs;
  var indexItemTeacher = -1;
  var isExpandAll= false.obs;
  var indexItemAll = -1;
  final ClassOfficeRepo _subjectRepo = ClassOfficeRepo();
  RxList<ClassId> classOfUser = <ClassId>[].obs;
  Rx<ClassId> currentClass = ClassId().obs;
  var currentStudentProfile = StudentByParent().obs;
  RxList<ItemContact> listUserInGroupChat = <ItemContact>[].obs;
  var listId = <String>[];

  var controllerNameGroupChat = TextEditingController();
  var focusNameGroupChat = FocusNode();
  var isShowErrorNameGroup = false;
  @override
  void onInit() {
    super.onInit();
  }


  queryListClassByParent(userId) async {
    classOfUser.value = [];
    _subjectRepo.listClassByParent(userId).then((value) {
      if (value.state == Status.SUCCESS) {
        classOfUser.value = value.object!;
      }
    });
    classOfUser.refresh();
  }

  getListClassByStudent() async {
    _subjectRepo.listClassByStudent().then((value) {
      if (value.state == Status.SUCCESS) {
        classOfUser.value = value.object!;
      }
    });
  }





}
