import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../../../../../commom/app_cache.dart';
import 'list_all_user/list_all_user_create_new_group_chat_controller.dart';
import 'list_parent/list_parent_create_new_group_chat_controller.dart';
import 'list_student/list_student_create_new_group_chat_controller.dart';
import 'list_teacher/list_teacher_create_new_group_chat_controller.dart';


class CreateNewGroupChatAllUserController extends GetxController {
  var indexClick = 0.obs;
  var click = <bool>[].obs;
  var dropDownSort = ''.obs;
  var dropDownDetailSort = <String>[].obs;
  var listSort = ["A", "Z"].obs;
  var listSort1 = ["Z", "A"].obs;
  var listTitle = <String>[].obs;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: false,
  );
  var classId = "".obs;






  onPageViewChange(int page) {
    indexClick.value = page;
    if (page != 0) {
      indexClick.value - 1;
    } else {
      indexClick.value = 0;
    }
  }





  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    for (int i = 0; i < getCountList(); i++) {
      click.add(false);
    }
    getListStatus();
    var tmpClassId = Get.arguments;
    if(tmpClassId!=null){
      classId.value = tmpClassId;
    }
  }

  @override
  dispose() {
    pageController.dispose();
    super.dispose();
  }


  getCount(index){
    if(AppCache().userType == "STUDENT"){
      switch(index){
        case 0:
          return Get.find<ListAllUserCreateNewGroupChatController>().listAll.length;
        case 1:
          return Get.find<ListStudentCreateNewGroupChatController>().listStudent.length;
        case 2:
          return Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.length;
      }
    }else if(AppCache().userType=="PARENT"){
      switch(index){
        case 0:
          return Get.find<ListAllUserCreateNewGroupChatController>().listAll.length;
        case 1:
          return Get.find<ListParentCreateNewGroupChatController>().listParent.length;
        case 2:
          return Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.length;
      }
    }else{
      switch(index){
        case 0:
          return Get.find<ListAllUserCreateNewGroupChatController>().listAll.length;
        case 1:
          return Get.find<ListStudentCreateNewGroupChatController>().listStudent.length;
        case 2:
          return Get.find<ListParentCreateNewGroupChatController>().listParent.length;
        case 3:
          return Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.length;
      }
    }
  }


  showColor(index) {
    for (int i = 0; i < getCountList(); i++) {
      click.add(false);
    }
    click[index] = true;
  }

  getCountList(){
    if(AppCache().userType == "STUDENT"||AppCache().userType =="PARENT"){
      return 2;
    }else{
      return 3;
    }
  }

  getListStatus(){
    if(AppCache().userType == "STUDENT"){
        listTitle.value= ["Tất cả","Học sinh","Giáo Viên"];
    }else if(AppCache().userType=="PARENT"){

      listTitle.value= ["Tất cả","Phụ huynh","Giáo Viên"];
    }else{
      listTitle.value= ["Tất cả","Học sinh", "Phụ huynh","Giáo Viên"];
    }
  }



}



