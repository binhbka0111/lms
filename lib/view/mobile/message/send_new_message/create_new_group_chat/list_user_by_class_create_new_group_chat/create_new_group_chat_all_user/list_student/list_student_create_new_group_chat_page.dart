import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../commom/utils/createGroupChat.dart';
import '../../../create_new_group_chat_controller.dart';
import '../list_all_user/list_all_user_create_new_group_chat_controller.dart';
import '../list_parent/list_parent_create_new_group_chat_controller.dart';
import '../list_teacher/list_teacher_create_new_group_chat_controller.dart';
import 'list_student_create_new_group_chat_controller.dart';


class ListStudentCreateNewGroupChatPage extends GetView<ListStudentCreateNewGroupChatController>{
  @override
  final controller = Get.put(ListStudentCreateNewGroupChatController());

  ListStudentCreateNewGroupChatPage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Obx(() => Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(6)),
      padding: const EdgeInsets.all(16),
      margin:  EdgeInsets.fromLTRB(16.w, 0, 16.w, 16.h),
      child: Column(
        children: [
          Row(
            children: [
              InkWell(
                onTap: () {
                  controller.isCheckAll.value = !controller.isCheckAll.value;
                  controller.checkAll();
                },
                child: Row(
                  children: [
                    Container(
                        width: 18.h,
                        height: 18.h,
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2.h),
                        margin: const EdgeInsets.only(right: 8),
                        decoration: ShapeDecoration(
                          shape: CircleBorder(
                              side: BorderSide(color: controller.isCheckAll.value ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(235, 235, 235, 1))),
                        ),
                        child: controller.isCheckAll.value
                            ? const Icon(Icons.circle, size: 12,
                          color:ColorUtils.PRIMARY_COLOR,
                        )
                            : null),
                    Padding(padding: EdgeInsets.only(left: 8.w)),
                    Text(
                      "Chọn hết",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w400,
                          color: Colors.black),
                    ),
                  ],
                ),
              ),
              Expanded(child: Container()),
              Text(
                "Đã chọn ${controller.listCheckBox.where((p0) => p0 == true).toList().length} /${controller.listStudentCopy.length}",
                style: TextStyle(
                    color: const Color.fromRGBO(177, 177, 177, 1),
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w500),
              )
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 16.h)),
          Expanded(
            child: ListView.builder(
                itemCount: controller.listStudent.length,
                shrinkWrap: true,
                physics: const ScrollPhysics(),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      controller.checkBoxListView(index);
                      if(controller.listCheckBox.contains(false) == true){
                        controller.isCheckAll.value = false;
                      }else{
                        controller.isCheckAll.value = true;
                      }

                    },
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                                width: 18.h,
                                height: 18.h,
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(2.h),
                                margin: const EdgeInsets.only(right: 8),
                                decoration: ShapeDecoration(
                                  shape: CircleBorder(
                                      side: BorderSide(color: controller.listCheckBox[index] ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(235, 235, 235, 1))),
                                ),
                                child: controller.listCheckBox[index]
                                    ? const Icon(Icons.circle, size: 12,
                                  color:ColorUtils.PRIMARY_COLOR,
                                )
                                    : null),
                            Padding(padding: EdgeInsets.only(left: 8.w)),
                            SizedBox(
                              width: 32.h,
                              height: 32.h,
                              child:
                              CacheNetWorkCustom(urlImage: '${controller.listStudent[index].image }',)

                            ),
                            Padding(padding: EdgeInsets.only(left: 8.w)),
                            Expanded(child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${controller.listStudent[index].fullName}",
                                  style: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black),
                                ),
                                SizedBox(height: 4.h,),
                                RichText(
                                  textAlign: TextAlign.left,
                                  text: TextSpan(
                                    text: 'Phụ huynh: ',
                                    style: TextStyle(
                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: 'static/Inter-Medium.ttf'),

                                    children: controller.listStudent[index].parent?.map((e) {
                                      var indexName = controller.listStudent[index].parent?.indexOf(e);
                                      var showSplit = ", ";
                                      if (indexName == controller.listStudent[index].parent!.length - 1) {
                                        showSplit = "";
                                      }
                                      return TextSpan(
                                          text: "${e.fullName??""}$showSplit",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w500,
                                              fontFamily: 'static/Inter-Medium.ttf'));
                                    }).toList(),
                                  ),
                                )
                              ],
                            ))
                          ],
                        ),
                        const Divider(),
                      ],
                    ),
                  );
                }),
          ),
          Visibility(
              visible:  Get.find<CreateNewGroupChatController>().listId.isNotEmpty,
              child: Row(
                children: [
                  Expanded(child: Container(
                    margin: EdgeInsets.only(top: 8.h),
                    decoration: BoxDecoration(
                      border: Border.all(color: const Color.fromRGBO(239, 239, 239, 1),),
                    ),
                    height: 86.h,
                    child: ListView.builder(
                        itemCount: Get.find<CreateNewGroupChatController>().listUserInGroupChat.length,
                        physics: const ScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context,index){
                          return Container(
                            margin: EdgeInsets.symmetric(horizontal: 8.w),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  color: Colors.white,
                                  child: Stack(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.all(8.h),
                                        width: 32.h,
                                        height: 32.h,
                                        child:
                                          CacheNetWorkCustom(urlImage: '${Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].image }',)
                                      ),
                                      Positioned(
                                          top: 4,
                                          right: 0,
                                          child: InkWell(
                                            onTap: () {

                                              controller.listId.remove(Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].id);
                                              Get.find<ListTeacherCreateNewGroupChatController>().listId.remove(Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].id);
                                              Get.find<ListParentCreateNewGroupChatController>().listId.remove(Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].id);
                                              Get.find<ListAllUserCreateNewGroupChatController>().listId.remove(Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].id);
                                              Get.find<CreateNewGroupChatController>().listId.remove(Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].id);

                                              controller.listCheckBox.value = [];
                                              Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.value = [];
                                              Get.find<ListParentCreateNewGroupChatController>().listCheckBox.value = [];
                                              Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox.value = [];

                                              for(int i = 0;i <controller.listStudent.length;i++){
                                                if(controller.listId.contains(controller.listStudent[i].id)){
                                                  controller.listCheckBox.add(true);
                                                }else{
                                                  controller.listCheckBox.add(false);
                                                }
                                              }

                                              for(int i = 0;i <Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.length;i++){
                                                if(Get.find<ListTeacherCreateNewGroupChatController>().listId.contains(Get.find<ListTeacherCreateNewGroupChatController>().listTeacher[i].id)){
                                                  Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.add(true);
                                                }else{
                                                  Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.add(false);
                                                }
                                              }
                                              for(int i = 0;i <Get.find<ListParentCreateNewGroupChatController>().listParent.length;i++){
                                                if(Get.find<ListParentCreateNewGroupChatController>().listId.contains(Get.find<ListParentCreateNewGroupChatController>().listParent[i].id)){
                                                  Get.find<ListParentCreateNewGroupChatController>().listCheckBox.add(true);
                                                }else{
                                                  Get.find<ListParentCreateNewGroupChatController>().listCheckBox.add(false);
                                                }
                                              }

                                              for(int i = 0;i <Get.find<ListAllUserCreateNewGroupChatController>().listAll.length;i++){
                                                if(Get.find<ListAllUserCreateNewGroupChatController>().listId.contains(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id)){
                                                  Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox.add(true);
                                                }else{
                                                  Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox.add(false);
                                                }
                                              }

                                              Get.find<CreateNewGroupChatController>().listUserInGroupChat.removeAt(index);
                                              if(controller.listCheckBox.contains(false) == true){
                                                controller.isCheckAll.value = false;
                                              }else{
                                                controller.isCheckAll.value = true;
                                              }
                                            },
                                            child: Container(
                                              padding: EdgeInsets.zero,
                                              decoration: const BoxDecoration(
                                                color: Colors.white,
                                                shape: BoxShape.circle,
                                              ),
                                              child: SvgPicture.asset("assets/images/icon_dangerous.svg",fit: BoxFit.cover,height: 18.h,width: 18.h,),
                                            ),
                                          ))
                                    ],
                                  ) ,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 8.h),
                                  child:  Text("${ Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].fullName}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.w400,fontSize: 14),),
                                )
                              ],
                            ),
                          );
                        }),
                  )),
                  SizedBox(
                    width: 8.w,
                  ),
                  InkWell(
                    onTap: () {
                      if(Get.find<CreateNewGroupChatController>().listId.length<2) {
                        AppUtils.shared.showToast("Vui lòng chọn nhiều hơn 1 người để tạo nhóm");
                      }else{
                        Get.dialog(_onDialogConfirm());
                      }
                    },
                    child: const Icon(Icons.send_rounded,color: ColorUtils.PRIMARY_COLOR,size: 30,),
                  )
                ],
              ))
        ],
      ),
    ));
  }

  _onDialogConfirm() {
    return GetBuilder<ListStudentCreateNewGroupChatController>(builder: (controller) {
      return Center(
        child: Wrap(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 32.w),
              child: Card(
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(8),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16.w,
                    vertical: 16.h,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            'Đổi Tên Nhóm',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                            ),
                          ),
                          IconButton(
                            padding: EdgeInsets.zero,
                            constraints: const BoxConstraints(),
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            onPressed: () {
                              Get.back();
                            },
                            icon: const Icon(
                              Icons.close_rounded,
                              size: 20.0,
                              color: Color(0xFF7D7E7E),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      FocusScope(
                        node: FocusScopeNode(),
                        child: Focus(
                            onFocusChange: (focus) {
                              controller.update();
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding: const EdgeInsets.all(2.0),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                      const BorderRadius.all(Radius.circular(6.0)),
                                      border: Border.all(
                                        width: 1,
                                        style: BorderStyle.solid,
                                        color: controller.focusNameGroupChat.hasFocus
                                            ? ColorUtils.PRIMARY_COLOR
                                            : const Color.fromRGBO(192, 192, 192, 1),
                                      )),
                                  child: TextFormField(
                                    cursorColor: ColorUtils.PRIMARY_COLOR,
                                    focusNode: controller.focusNameGroupChat,
                                    onChanged: (value) {
                                      if(controller.controllerNameGroupChat.text.trim() != ""){
                                        controller.isShowErrorNameGroup = false;
                                      }
                                      controller.update();
                                    },
                                    onFieldSubmitted: (value) {
                                      controller.update();
                                    },
                                    controller: controller.controllerNameGroupChat,
                                    decoration: InputDecoration(
                                      labelText: "Nhập tên nhóm",
                                      labelStyle: TextStyle(
                                          fontSize: 14.0.sp,
                                          color: const Color.fromRGBO(177, 177, 177, 1),
                                          fontWeight: FontWeight.w500,
                                          fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                                      contentPadding: const EdgeInsets.symmetric(
                                          vertical: 8, horizontal: 16),
                                      prefixIcon:  null,
                                      suffixIcon:  Visibility(
                                          visible:
                                          controller.controllerNameGroupChat.text.isNotEmpty ==
                                              true,
                                          child: InkWell(
                                            onTap: () {
                                              controller.controllerNameGroupChat.text = "";
                                            },
                                            child: const Icon(
                                              Icons.close_outlined,
                                              color: Colors.black,
                                              size: 20,
                                            ),
                                          )),
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      border: InputBorder.none,
                                      errorStyle: const TextStyle(height: 0),
                                      focusedErrorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      floatingLabelBehavior: FloatingLabelBehavior.auto,

                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: controller.isShowErrorNameGroup,
                                  child: Container(
                                      padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                      child: const Text(
                                        "Vui lòng nhập tên nhóm",
                                        style: TextStyle(color: Colors.red),
                                      )),
                                )
                              ],
                            )),
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          ElevatedButton.icon(
                            onPressed: () {
                              Get.back();
                              controller.controllerNameGroupChat.text = "";
                            },
                            style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0),
                                  side: const BorderSide(
                                      color: Color(0xFFC0C0C0), width: 0.75),
                                ),
                                backgroundColor: Colors.white),
                            icon:
                            const Icon(Icons.close_rounded, color: Colors.black),
                            label: const Text(
                              'Huỷ',
                              style: TextStyle(
                                color: Color(0xFF333333),
                                fontSize: 14,
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xFFF88125),
                            ),
                            onPressed: () {
                              if (controller.controllerNameGroupChat.text.trim() == "") {
                                controller.isShowErrorNameGroup = true;
                                controller.update();
                              } else {
                                createNewGroupChat(controller.controllerNameGroupChat.text.trim(), "GROUP",  Get.find<CreateNewGroupChatController>().listId);
                              }
                            },
                            icon: const Icon(Icons.check),
                            label: const Text('Xác nhận'),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    },);
  }

}