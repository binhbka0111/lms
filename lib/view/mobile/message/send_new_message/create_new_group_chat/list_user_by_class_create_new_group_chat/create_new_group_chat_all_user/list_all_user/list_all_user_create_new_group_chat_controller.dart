import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../../data/repository/contact/contact_repo.dart';
import '../../../../../../../../commom/app_cache.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../data/repository/chat/chat_repo.dart';
import '../../../../../../../../routes/app_pages.dart';
import '../../../../../../home/home_controller.dart';
import '../../../../../../role/parent/parent_home_controller.dart';
import '../../../../../message_controller.dart';
import '../../../create_new_group_chat_controller.dart';
import '../create_new_group_chat_all_user_controller.dart';
import '../list_parent/list_parent_create_new_group_chat_controller.dart';
import '../list_student/list_student_create_new_group_chat_controller.dart';
import '../list_teacher/list_teacher_create_new_group_chat_controller.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';

class ListAllUserCreateNewGroupChatController extends GetxController{
  var clickAll = <bool>[].obs;
  var listCheckBox = <bool>[].obs;
  var isCheckAll = false.obs;
  final ContactRepo _contactRepo = ContactRepo();
  var contacts = Contacts().obs;
  RxList<ItemContact> listAll= <ItemContact>[].obs;
  RxList<ItemContact> listAllCopy= <ItemContact>[].obs;
  var student = <Student>[].obs;
  var parent = <Parent>[].obs;
  var listId = <String>[];
  final ChatRepo _chatRepo = ChatRepo();
  var controllerNameGroupChat = TextEditingController();
  var focusNameGroupChat = FocusNode();
  var isShowErrorNameGroup = false;
  @override
  void onInit() {
    getClassContact();
    super.onInit();
  }

  createNewGroupChat(name, type, listId) {
    _chatRepo.createNewGroupChat(name, type, listId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Tạo nhóm chat thành công");
        Get.toNamed(Routes.home);
        Get.find<HomeController>().comeMessage();
        Get.find<MessageController>().onInit();
        checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> goToMessageContent(value.object?.id),StringConstant.FEATURE_MESSAGE_DETAIL);

      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tạo nhóm chat thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }
  void goToMessageContent(id){
    Get.toNamed(Routes.messageContent,arguments: id);
  }


  getClassContact() async {
    listAll.value = [];
    if(AppCache().userType == "PARENT"){
      _contactRepo.getClassContactParent(Get.find<CreateNewGroupChatAllUserController>().classId.value,
          "",Get.find<ParentHomeController>().currentStudentProfile.value.id,"TRUE").then((value) {
        if (value.state == Status.SUCCESS) {
          contacts.value = value.object!;
          listAll.value = contacts.value.item!;
          listAllCopy.value = contacts.value.item!;
          getListCheckBox();
        }
      });
    }else{
      _contactRepo.getClassContact(Get.find<CreateNewGroupChatAllUserController>().classId.value, "","TRUE").then((value) {
        if (value.state == Status.SUCCESS) {
          contacts.value = value.object!;
          listAll.value = contacts.value.item!;
          listAllCopy.value = contacts.value.item!;
          getListCheckBox();
        }
      });
    }
  }


  getListCheckBox(){
    listCheckBox.value = [];
    for (int i = 0; i < listAll.length; i++){
      if(Get.find<CreateNewGroupChatController>().listId.contains(listAll[i].id)){
        listCheckBox.add(true);
        listId.add(listAll[i].id!);
      }else{
        listCheckBox.add(false);
      }
    }
  }


  getListRole(role,index) {
    switch (role) {
      case "PARENT":
        return student;
      case "STUDENT":
        return parent;
      case "TEACHER":
        return listAll[index].subject??"";
    }
  }


  checkBoxListView(index) {
    if (listCheckBox[index] == false) {
      listCheckBox[index] = true;
      if(listId.contains(listAll[index].id!)== true){
      }else{
        listId.add(listAll[index].id!);
      }

      for(int i = 0 ;i<Get.find<ListParentCreateNewGroupChatController>().listParent.length;i++){
        if(Get.find<ListParentCreateNewGroupChatController>().listParent[i].id !=listAll[index].id!){
        }else{
          Get.find<ListParentCreateNewGroupChatController>().listId.add(Get.find<ListParentCreateNewGroupChatController>().listParent[i].id!);
          Get.find<ListParentCreateNewGroupChatController>().listCheckBox[i] = true;
        }
      }

      for(int i = 0 ;i<Get.find<ListStudentCreateNewGroupChatController>().listStudent.length;i++){
        if(Get.find<ListStudentCreateNewGroupChatController>().listStudent[i].id != listAll[index].id!){
        }else{
          Get.find<ListStudentCreateNewGroupChatController>().listId.add(Get.find<ListStudentCreateNewGroupChatController>().listStudent[i].id!);
          Get.find<ListStudentCreateNewGroupChatController>().listCheckBox[i] = true;
        }
      }
      for(int i = 0 ;i<Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.length;i++){
        if(Get.find<ListTeacherCreateNewGroupChatController>().listTeacher[i].id != listAll[index].id!){
        }else{
          Get.find<ListTeacherCreateNewGroupChatController>().listId.add(Get.find<ListTeacherCreateNewGroupChatController>().listTeacher[i].id!);
          Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox[i] = true;
        }
      }
      if(Get.find<CreateNewGroupChatController>().listId.contains(listAll[index].id!) == false){
        Get.find<CreateNewGroupChatController>().listId.add(listAll[index].id!);
        Get.find<CreateNewGroupChatController>().listUserInGroupChat.add(listAll[index]);
      }

    } else {
      listCheckBox[index] = false;
      listId.remove(listAll[index].id!);


      for(int i = 0 ;i<Get.find<ListParentCreateNewGroupChatController>().listParent.length;i++){
        if(Get.find<ListParentCreateNewGroupChatController>().listParent[i].id !=listAll[index].id!){
        }else{
          Get.find<ListParentCreateNewGroupChatController>().listCheckBox[i] = false;
          Get.find<ListParentCreateNewGroupChatController>().listId.remove(Get.find<ListParentCreateNewGroupChatController>().listParent[i].id!);
        }
      }


      for(int i = 0 ;i<Get.find<ListStudentCreateNewGroupChatController>().listStudent.length;i++){
        if(Get.find<ListStudentCreateNewGroupChatController>().listStudent[i].id !=listAll[index].id!){
        }else{
          Get.find<ListStudentCreateNewGroupChatController>().listCheckBox[i] = false;
          Get.find<ListStudentCreateNewGroupChatController>().listId.remove(Get.find<ListStudentCreateNewGroupChatController>().listStudent[i].id!);
        }
      }

      for(int i = 0 ;i<Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.length;i++){
        if(Get.find<ListTeacherCreateNewGroupChatController>().listTeacher[i].id !=listAll[index].id!){
        }else{
          Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox[i] = false;
          Get.find<ListTeacherCreateNewGroupChatController>().listId.remove(Get.find<ListTeacherCreateNewGroupChatController>().listTeacher[i].id!);
        }
      }
      Get.find<CreateNewGroupChatController>().listId.remove(listAll[index].id!);
      Get.find<CreateNewGroupChatController>().listUserInGroupChat.removeWhere((element) => element.id == listAll[index].id!);
    }
    if(Get.find<ListStudentCreateNewGroupChatController>().listCheckBox.contains(false) == true){
      Get.find<ListStudentCreateNewGroupChatController>().isCheckAll.value = false;
    }else{
      Get.find<ListStudentCreateNewGroupChatController>().isCheckAll.value = true;
    }
    if(Get.find<ListParentCreateNewGroupChatController>().listCheckBox.contains(false) == true){
      Get.find<ListParentCreateNewGroupChatController>().isCheckAll.value = false;
    }else{
      Get.find<ListParentCreateNewGroupChatController>().isCheckAll.value = true;
    }
    if(Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.contains(false) == true){
      Get.find<ListTeacherCreateNewGroupChatController>().isCheckAll.value = false;
    }else{
      Get.find<ListTeacherCreateNewGroupChatController>().isCheckAll.value = true;
    }
  }


  checkAll() {
    if (isCheckAll.value == true) {
      listCheckBox.value = [];
      Get.find<ListParentCreateNewGroupChatController>().listCheckBox.value = [];
      Get.find<ListStudentCreateNewGroupChatController>().listCheckBox.value = [];
      Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.value = [];
      for (int i = 0; i < listAll.length; i++){
        if(listId.contains(listAll[i].id!) == true){
        }else{
          listId.add(listAll[i].id!);
        }
      }
      for(int i = 0 ;i<Get.find<ListParentCreateNewGroupChatController>().listParent.length;i++){
        if(Get.find<ListParentCreateNewGroupChatController>().listId.contains(Get.find<ListParentCreateNewGroupChatController>().listParent[i].id) == false ){
          Get.find<ListParentCreateNewGroupChatController>().listId.add(Get.find<ListParentCreateNewGroupChatController>().listParent[i].id!);
        }
      }

      for(int i = 0 ;i<Get.find<ListStudentCreateNewGroupChatController>().listStudent.length;i++){
        if(Get.find<ListStudentCreateNewGroupChatController>().listId.contains(Get.find<ListStudentCreateNewGroupChatController>().listStudent[i].id) == false){
          Get.find<ListStudentCreateNewGroupChatController>().listId.add(Get.find<ListStudentCreateNewGroupChatController>().listStudent[i].id!);
        }
      }
      for(int i = 0 ;i<Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.length;i++){
        if(Get.find<ListTeacherCreateNewGroupChatController>().listId.contains(Get.find<ListTeacherCreateNewGroupChatController>().listTeacher[i].id) == false){
          Get.find<ListTeacherCreateNewGroupChatController>().listId.add(Get.find<ListTeacherCreateNewGroupChatController>().listTeacher[i].id!);
        }
      }
      for (int i = 0; i < listId.length; i++) {
        listCheckBox.add(true);
      }
      for (int i = 0; i < Get.find<ListStudentCreateNewGroupChatController>().listStudent.length; i++) {
        Get.find<ListStudentCreateNewGroupChatController>().listCheckBox.add(true);
      }
      for (int i = 0; i < Get.find<ListParentCreateNewGroupChatController>().listParent.length; i++) {
        Get.find<ListParentCreateNewGroupChatController>().listCheckBox.add(true);
      }
      for (int i = 0; i < Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.length; i++) {
        Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.add(true);
      }

      for (int i = 0; i < listAll.length; i++){
        if(Get.find<CreateNewGroupChatController>().listId.contains(listAll[i].id!) == false){
          Get.find<CreateNewGroupChatController>().listId.add(listAll[i].id!);
          Get.find<CreateNewGroupChatController>().listUserInGroupChat.add(listAll[i]);
        }
      }
    } else {
      listCheckBox.value = [];
      Get.find<ListParentCreateNewGroupChatController>().listCheckBox.value = [];
      Get.find<ListStudentCreateNewGroupChatController>().listCheckBox.value = [];
      Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.value = [];
      for (int i = 0; i < listAll.length; i++) {
        listCheckBox.add(false);
        if(listId.contains(listAll[i].id!) == true){
          listId.remove(listAll[i].id!);
        }
      }

      for (int i = 0; i < Get.find<ListParentCreateNewGroupChatController>().listParent.length; i++) {
        Get.find<ListParentCreateNewGroupChatController>().listCheckBox.add(false);
        if(Get.find<ListParentCreateNewGroupChatController>().listId.contains(Get.find<ListParentCreateNewGroupChatController>().listParent[i].id!) == true){
          Get.find<ListParentCreateNewGroupChatController>().listId.remove(Get.find<ListParentCreateNewGroupChatController>().listParent[i].id!);
        }
      }

      for (int i = 0; i < Get.find<ListStudentCreateNewGroupChatController>().listStudent.length; i++) {
        Get.find<ListStudentCreateNewGroupChatController>().listCheckBox.add(false);
        if(Get.find<ListStudentCreateNewGroupChatController>().listId.contains(Get.find<ListStudentCreateNewGroupChatController>().listStudent[i].id!) == true){
          Get.find<ListStudentCreateNewGroupChatController>().listId.remove(Get.find<ListStudentCreateNewGroupChatController>().listStudent[i].id!);
        }
      }
      for (int i = 0; i < Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.length; i++) {
        Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.add(false);
        if(Get.find<ListTeacherCreateNewGroupChatController>().listId.contains(Get.find<ListTeacherCreateNewGroupChatController>().listTeacher[i].id!) == true){
          Get.find<ListTeacherCreateNewGroupChatController>().listId.remove(Get.find<ListTeacherCreateNewGroupChatController>().listTeacher[i].id!);
        }
      }

      for (int i = 0; i < listAll.length; i++) {
        if(Get.find<CreateNewGroupChatController>().listId.contains(listAll[i].id!) == true){
          Get.find<CreateNewGroupChatController>().listId.remove(listAll[i].id!);
          Get.find<CreateNewGroupChatController>().listUserInGroupChat.removeWhere((element) => element.id == listAll[i].id);
        }
      }

    }
    if(Get.find<ListStudentCreateNewGroupChatController>().listCheckBox.contains(false) == true){
      Get.find<ListStudentCreateNewGroupChatController>().isCheckAll.value = false;
    }else{
      Get.find<ListStudentCreateNewGroupChatController>().isCheckAll.value = true;
    }
    if(Get.find<ListParentCreateNewGroupChatController>().listCheckBox.contains(false) == true){
      Get.find<ListParentCreateNewGroupChatController>().isCheckAll.value = false;
    }else{
      Get.find<ListParentCreateNewGroupChatController>().isCheckAll.value = true;
    }
    if(Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.contains(false) == true){
      Get.find<ListTeacherCreateNewGroupChatController>().isCheckAll.value = false;
    }else{
      Get.find<ListTeacherCreateNewGroupChatController>().isCheckAll.value = true;
    }
  }



  getTitle(role) {
    switch (role) {
      case "PARENT":
        return "Phụ huynh hs: ";
      case "STUDENT":
        return "Phụ huynh: ";
      case "TEACHER":
        return "Giáo viên: ";
      default:
        return "";
    }
  }


}