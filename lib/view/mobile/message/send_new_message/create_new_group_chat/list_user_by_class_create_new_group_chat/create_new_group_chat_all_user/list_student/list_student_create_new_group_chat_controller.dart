import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../../data/repository/contact/contact_repo.dart';
import '../../../../../../../../commom/app_cache.dart';
import '../../../../../../role/parent/parent_home_controller.dart';
import '../../../create_new_group_chat_controller.dart';
import '../create_new_group_chat_all_user_controller.dart';
import '../list_all_user/list_all_user_create_new_group_chat_controller.dart';
class ListStudentCreateNewGroupChatController extends GetxController{
  var clickAll = <bool>[].obs;
  var listCheckBox = <bool>[].obs;
  var isCheckAll = false.obs;
  final ContactRepo _contactRepo = ContactRepo();
  var contacts = Contacts().obs;
  RxList<ItemContact>   listStudent = <ItemContact>[].obs;
  RxList<ItemContact> listStudentCopy = <ItemContact>[].obs;
  var listId = <String>[];
  var controllerNameGroupChat = TextEditingController();
  var focusNameGroupChat = FocusNode();
  var isShowErrorNameGroup = false;
  @override
  void onInit() {
    getStudentClassContact();
    super.onInit();
  }




  getStudentClassContact() async {
    if(AppCache().userType == "PARENT"){
      _contactRepo.getClassContactParent(Get.find<CreateNewGroupChatAllUserController>().classId.value,
          "STUDENT",Get.find<ParentHomeController>().currentStudentProfile.value.id,"TRUE").then((value) {
        if (value.state == Status.SUCCESS) {
          contacts.value = value.object!;
          listStudent.value = contacts.value.item!;
          listStudentCopy.value = contacts.value.item!;
          getListCheckBox();
        }
      });
    }else{
      _contactRepo.getClassContact(Get.find<CreateNewGroupChatAllUserController>().classId.value, "STUDENT","TRUE").then((value) {
        if (value.state == Status.SUCCESS) {
          contacts.value = value.object!;
          listStudent.value = contacts.value.item!;
          listStudentCopy.value = contacts.value.item!;
          getListCheckBox();
        }
      });
    }
  }


  getListCheckBox(){
    listCheckBox.value = [];
    for (int i = 0; i < listStudent.length; i++){
      if(Get.find<CreateNewGroupChatController>().listId.contains(listStudent[i].id)){
        listCheckBox.add(true);
        listId.add(listStudent[i].id!);
      }else{
        listCheckBox.add(false);
      }
    }
  }


  checkBoxListView(index) {
    if (listCheckBox[index] == false) {
      listCheckBox[index] = true;
      if(listId.contains(listStudent[index].id!)== true){

      }else{
        listId.add(listStudent[index].id!);
      }


      for(int i = 0 ;i<Get.find<ListAllUserCreateNewGroupChatController>().listAll.length;i++){
        if(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id !=listStudent[index].id!){
        }else{
          Get.find<ListAllUserCreateNewGroupChatController>().listId.add(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id!);
          Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[i] = true;
        }
      }
      if(Get.find<CreateNewGroupChatController>().listId.contains(listStudent[index].id!)== false){
        Get.find<CreateNewGroupChatController>().listId.add(listStudent[index].id!);
        Get.find<CreateNewGroupChatController>().listUserInGroupChat.add(listStudent[index]);
      }

    } else {
      listCheckBox[index] = false;
      listId.remove(listStudent[index].id!);

      for(int i = 0 ;i<Get.find<ListAllUserCreateNewGroupChatController>().listAll.length;i++){
        if(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id !=listStudent[index].id!){
        }else{
          Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[i] = false;
          Get.find<ListAllUserCreateNewGroupChatController>().listId.remove(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id!);
        }
      }
      Get.find<CreateNewGroupChatController>().listId.remove(listStudent[index].id!);
      Get.find<CreateNewGroupChatController>().listUserInGroupChat.removeWhere((element) => element.id == listStudent[index].id!);

    }

    if(Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox.contains(false) == true){
      Get.find<ListAllUserCreateNewGroupChatController>().isCheckAll.value = false;
    }else{
      Get.find<ListAllUserCreateNewGroupChatController>().isCheckAll.value = true;
    }
  }


  checkAll() {
    if (isCheckAll.value == true) {
      listCheckBox.value = [];
      for (int i = 0; i < listStudent.length; i++){
        if(listId.contains(listStudent[i].id!) == true){
        }else{
          listId.add(listStudent[i].id!);
        }
      }

      for(int i = 0 ;i < listStudent.length;i++){
        if(Get.find<ListAllUserCreateNewGroupChatController>().listId.contains(listStudent[i].id) == false){
          Get.find<ListAllUserCreateNewGroupChatController>().listId.add(listStudent[i].id!);
        }
      }

      for (int i = 0; i < Get.find<ListAllUserCreateNewGroupChatController>().listAll.length; i++) {
        if(Get.find<ListAllUserCreateNewGroupChatController>().listId.contains(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id) == true){
          Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[i] = true;
        }else{
          Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[i] = false;
        }
      }
      for (int i = 0; i < listId.length; i++) {
        listCheckBox.add(true);
      }
      for (int i = 0; i < listStudent.length; i++){
        if(Get.find<CreateNewGroupChatController>().listId.contains(listStudent[i].id!) == false){
          Get.find<CreateNewGroupChatController>().listId.add(listStudent[i].id!);
          Get.find<CreateNewGroupChatController>().listUserInGroupChat.add(listStudent[i]);
        }
      }
    } else {
      listCheckBox.value = [];
      for (int i = 0; i < listStudent.length; i++) {
        listCheckBox.add(false);
        if(listId.contains(listStudent[i].id!) == true){
          listId.remove(listStudent[i].id!);
        }
      }

      for (int i = 0; i < listStudent.length; i++) {
        if(Get.find<ListAllUserCreateNewGroupChatController>().listId.contains(listStudent[i].id!) == true){
          Get.find<ListAllUserCreateNewGroupChatController>().listId.remove(listStudent[i].id!);
        }
      }
      for (int i = 0; i < Get.find<ListAllUserCreateNewGroupChatController>().listAll.length; i++) {
        if(Get.find<ListAllUserCreateNewGroupChatController>().listId.contains(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id) == true){
          Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[i] = true;
        }else{
          Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[i] = false;
        }
      }
      for (int i = 0; i < listStudent.length; i++) {
        if(Get.find<CreateNewGroupChatController>().listId.contains(listStudent[i].id!) == true){
          Get.find<CreateNewGroupChatController>().listId.remove(listStudent[i].id!);
          Get.find<CreateNewGroupChatController>().listUserInGroupChat.removeWhere((element) => element.id == listStudent[i].id);
        }
      }
    }
    if(Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox.contains(false) == true){
      Get.find<ListAllUserCreateNewGroupChatController>().isCheckAll.value = false;
    }else{
      Get.find<ListAllUserCreateNewGroupChatController>().isCheckAll.value = true;
    }

  }





}