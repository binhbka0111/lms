import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';
import 'package:tiengviet/tiengviet.dart';
import '../../../../../../../commom/app_cache.dart';
import '../../create_new_group_chat_controller.dart';
import 'create_new_group_chat_all_user_controller.dart';
import 'list_all_user/list_all_user_create_new_group_chat_controller.dart';
import 'list_all_user/list_all_user_create_new_group_chat_page.dart';
import 'list_parent/list_parent_create_new_group_chat_controller.dart';
import 'list_parent/list_parent_create_new_group_chat_page.dart';
import 'list_student/list_student_create_new_group_chat_controller.dart';
import 'list_student/list_student_create_new_group_chat_page.dart';
import 'list_teacher/list_teacher_create_new_group_chat_controller.dart';
import 'list_teacher/list_teacher_create_new_group_chat_page.dart';
class CreateNewGroupChatAllUserPage extends GetWidget<CreateNewGroupChatAllUserController>{
  @override
  final controller = Get.put(CreateNewGroupChatAllUserController());

  CreateNewGroupChatAllUserPage({super.key});

  @override
  Widget build(BuildContext context) {
    return
      Obx(() =>  WillPopScope(child: SafeArea(
          child: GestureDetector(
            onTap: (){
              FocusManager.instance.primaryFocus?.unfocus();
            },

            child: Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: AppBar(
                backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
                elevation: 0,
                title: Text(
                  "Tạo nhóm chat mới",
                  style: TextStyle(
                      color: Colors.white, fontSize: 16.sp, fontWeight: FontWeight.w500),
                ),
                actions: [
                  InkWell(
                    onTap: () {
                      comeToHome();

                    },
                    child: const Icon(
                      Icons.home,
                      color: Colors.white,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(right: 16.w))
                ],
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 8.h),
                    child: Row(
                      children: [
                        Expanded(
                          child: SizedBox(
                            height: 40.h,
                            child: TextFormField(
                              onChanged: (value) {
                                Get.find<ListAllUserCreateNewGroupChatController>().listAll.value = [];
                                Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox.value = [];
                                Get.find<ListAllUserCreateNewGroupChatController>().listAll.value = Get.find<ListAllUserCreateNewGroupChatController>().listAllCopy.where((element) => TiengViet.parse(element.fullName!.toLowerCase()).contains(TiengViet.parse(value.toLowerCase().trim())) == true).toList();
                                for(int i= 0; i <Get.find<ListAllUserCreateNewGroupChatController>().listAll.length; i++){
                                  Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox.add(false);
                                }
                                for(int j = 0; j <Get.find<ListAllUserCreateNewGroupChatController>().listAll.length; j++){
                                  if(Get.find<ListAllUserCreateNewGroupChatController>().listId.contains(Get.find<ListAllUserCreateNewGroupChatController>().listAll[j].id) ){
                                    Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[j] = true;
                                  }else{
                                    Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[j] = false;
                                  }
                                }

                                if(Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox.isNotEmpty){
                                  if(Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox.contains(false) == true){
                                    Get.find<ListAllUserCreateNewGroupChatController>().isCheckAll.value = false;
                                  }else{
                                    Get.find<ListAllUserCreateNewGroupChatController>().isCheckAll.value = true;
                                  }
                                }

                                Get.find<ListAllUserCreateNewGroupChatController>().listAll.refresh();
                                Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox.refresh();
                                Get.find<ListStudentCreateNewGroupChatController>().listStudent.value = [];
                                Get.find<ListStudentCreateNewGroupChatController>().listCheckBox.value = [];
                                Get.find<ListStudentCreateNewGroupChatController>().listStudent.value = Get.find<ListStudentCreateNewGroupChatController>().listStudentCopy.where((element) => TiengViet.parse(element.fullName!.toLowerCase()).contains(TiengViet.parse(value.toLowerCase().trim())) == true).toList();
                                for(int i= 0; i <Get.find<ListStudentCreateNewGroupChatController>().listStudent.length; i++){
                                  Get.find<ListStudentCreateNewGroupChatController>().listCheckBox.add(false);
                                }
                                for(int j = 0; j <Get.find<ListStudentCreateNewGroupChatController>().listStudent.length; j++){
                                  if(Get.find<ListStudentCreateNewGroupChatController>().listId.contains(Get.find<ListStudentCreateNewGroupChatController>().listStudent[j].id) ){
                                    Get.find<ListStudentCreateNewGroupChatController>().listCheckBox[j] = true;
                                  }else{
                                    Get.find<ListStudentCreateNewGroupChatController>().listCheckBox[j] = false;
                                  }
                                }

                                if(Get.find<ListStudentCreateNewGroupChatController>().listCheckBox.isNotEmpty){
                                  if(Get.find<ListStudentCreateNewGroupChatController>().listCheckBox.contains(false) == true){
                                    Get.find<ListStudentCreateNewGroupChatController>().isCheckAll.value = false;
                                  }else{
                                    Get.find<ListStudentCreateNewGroupChatController>().isCheckAll.value = true;
                                  }
                                }

                                Get.find<ListStudentCreateNewGroupChatController>().listStudent.refresh();
                                Get.find<ListStudentCreateNewGroupChatController>().listCheckBox.refresh();
                                Get.find<ListParentCreateNewGroupChatController>().listParent.value = [];
                                Get.find<ListParentCreateNewGroupChatController>().listCheckBox.value = [];
                                Get.find<ListParentCreateNewGroupChatController>().listParent.value = Get.find<ListParentCreateNewGroupChatController>().listParentCopy.where((element) => TiengViet.parse(element.fullName!.toLowerCase()).contains(TiengViet.parse(value.toLowerCase().trim())) == true).toList();
                                for(int i= 0; i <Get.find<ListParentCreateNewGroupChatController>().listParent.length; i++){
                                  Get.find<ListParentCreateNewGroupChatController>().listCheckBox.add(false);
                                }
                                for(int j = 0; j <Get.find<ListParentCreateNewGroupChatController>().listParent.length; j++){
                                  if(Get.find<ListParentCreateNewGroupChatController>().listId.contains(Get.find<ListParentCreateNewGroupChatController>().listParent[j].id) ){
                                    Get.find<ListParentCreateNewGroupChatController>().listCheckBox[j] = true;
                                  }else{
                                    Get.find<ListParentCreateNewGroupChatController>().listCheckBox[j] = false;
                                  }
                                }

                                if(Get.find<ListParentCreateNewGroupChatController>().listCheckBox.isNotEmpty){
                                  if(Get.find<ListParentCreateNewGroupChatController>().listCheckBox.contains(false) == true){
                                    Get.find<ListParentCreateNewGroupChatController>().isCheckAll.value = false;
                                  }else{
                                    Get.find<ListParentCreateNewGroupChatController>().isCheckAll.value = true;
                                  }
                                }

                                Get.find<ListParentCreateNewGroupChatController>().listParent.refresh();
                                Get.find<ListParentCreateNewGroupChatController>().listCheckBox.refresh();
                                Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.value = [];
                                Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.value = [];
                                Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.value = Get.find<ListTeacherCreateNewGroupChatController>().listTeacherCopy.where((element) => TiengViet.parse(element.fullName!.toLowerCase()).contains(TiengViet.parse(value.toLowerCase().trim())) == true).toList();
                                for(int i= 0; i <Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.length; i++){
                                  Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.add(false);
                                }
                                for(int j = 0; j <Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.length; j++){
                                  if(Get.find<ListTeacherCreateNewGroupChatController>().listId.contains(Get.find<ListTeacherCreateNewGroupChatController>().listTeacher[j].id) ){
                                    Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox[j] = true;
                                  }else{
                                    Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox[j] = false;
                                  }
                                }
                                if(Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.isNotEmpty){
                                  if(Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.contains(false) == true){
                                    Get.find<ListTeacherCreateNewGroupChatController>().isCheckAll.value = false;
                                  }else{
                                    Get.find<ListTeacherCreateNewGroupChatController>().isCheckAll.value = true;
                                  }
                                }

                                Get.find<ListTeacherCreateNewGroupChatController>().listTeacher.refresh();
                                Get.find<ListTeacherCreateNewGroupChatController>().listCheckBox.refresh();

                              },
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Color.fromRGBO(177, 177, 177, 1)),
                                      borderRadius: BorderRadius.circular(6)),
                                  prefixIcon: const Icon(
                                    Icons.search,
                                    color: Color.fromRGBO(177, 177, 177, 1),
                                  ),
                                  hintText: "Tìm kiếm",
                                  isCollapsed: true,
                                  hintStyle: TextStyle(
                                      fontSize: 14.sp,
                                      color: const Color.fromRGBO(177, 177, 177, 1)),
                                  focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color.fromRGBO(248, 129, 37, 1)))),
                              textAlignVertical: TextAlignVertical.center,
                              cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(left: 8.w)),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 16.w),
                    height: 40,
                    child: ListView.builder(
                        itemCount: controller.listTitle.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return GetBuilder<CreateNewGroupChatAllUserController>(

                              builder: (controller) {
                                return TextButton(
                                    onPressed: () {
                                      controller.showColor(index);
                                      controller.pageController.animateToPage(index,
                                          duration:
                                          const Duration(milliseconds: 500),
                                          curve: Curves.linear);
                                    },
                                    child: Obx(() => Row(
                                      children: [
                                        Text(
                                          controller.listTitle[index],
                                          style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12.sp,
                                              color: controller.indexClick.value == index
                                                  ? const Color.fromRGBO(
                                                  248, 129, 37, 1)
                                                  : const Color.fromRGBO(
                                                  177, 177, 177, 1)),
                                        ),
                                        Visibility(
                                            visible: Get.isRegistered<ListAllUserCreateNewGroupChatController>()&&Get.isRegistered<ListParentCreateNewGroupChatController>()&&Get.isRegistered<ListStudentCreateNewGroupChatController>()&&Get.isRegistered<ListTeacherCreateNewGroupChatController>(),
                                            child: Text(
                                              " (${controller.getCount(index).toString()})",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 12.sp,
                                                  color: controller.indexClick.value == index
                                                      ? const Color.fromRGBO(
                                                      248, 129, 37, 1)
                                                      : const Color.fromRGBO(
                                                      177, 177, 177, 1)),
                                            ))
                                      ],
                                    )));
                              });
                        }),
                  ),
                  Visibility(
                      visible: AppCache().userType != "STUDENT"&& AppCache().userType!="PARENT",
                      child: Expanded(
                          child: PageView(
                            onPageChanged: (value) {
                              controller.onPageViewChange(value);
                            },
                            controller: controller.pageController,
                            physics: const ScrollPhysics(),
                            children: [
                              ListAllUserCreateNewGroupChatPage(),
                              ListStudentCreateNewGroupChatPage(),
                              ListParentCreateNewGroupChatPage(),
                              ListTeacherCreateNewGroupChatPage(),
                            ],
                          ))),
                  Visibility(
                      visible: AppCache().userType == "STUDENT",
                      child: Expanded(
                          child: PageView(
                            onPageChanged: (value) {
                              controller.onPageViewChange(value);
                            },
                            controller: controller.pageController,
                            physics: const ScrollPhysics(),
                            children: [
                              ListAllUserCreateNewGroupChatPage(),
                              ListStudentCreateNewGroupChatPage(),
                              ListTeacherCreateNewGroupChatPage(),
                            ],
                          ))),
                  Visibility(
                      visible:AppCache().userType=="PARENT",
                      child: Expanded(
                          child: PageView(
                            onPageChanged: (value) {
                              controller.onPageViewChange(value);
                            },
                            controller: controller.pageController,
                            physics: const ScrollPhysics(),
                            children: [
                              ListAllUserCreateNewGroupChatPage(),
                              ListParentCreateNewGroupChatPage(),
                              ListTeacherCreateNewGroupChatPage(),
                            ],
                          ))),
                ],
              ),
            ),
          )), onWillPop: () async{
        Get.find<CreateNewGroupChatController>().indexItemAll = -1;
        Get.find<CreateNewGroupChatController>().update();
        return true;
      }));
  }

}