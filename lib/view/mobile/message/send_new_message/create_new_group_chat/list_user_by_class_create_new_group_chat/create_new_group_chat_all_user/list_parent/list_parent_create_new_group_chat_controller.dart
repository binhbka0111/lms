import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../../data/repository/contact/contact_repo.dart';
import '../../../../../../../../commom/app_cache.dart';
import '../../../../../../role/parent/parent_home_controller.dart';
import '../../../create_new_group_chat_controller.dart';
import '../create_new_group_chat_all_user_controller.dart';
import '../list_all_user/list_all_user_create_new_group_chat_controller.dart';

class ListParentCreateNewGroupChatController extends GetxController{
  var clickAll = <bool>[].obs;
  var listCheckBox = <bool>[].obs;
  var isCheckAll = false.obs;
  final ContactRepo _contactRepo = ContactRepo();
  var contacts = Contacts().obs;
  RxList<ItemContact> listParent = <ItemContact>[].obs;
  RxList<ItemContact> listParentCopy = <ItemContact>[].obs;
  var students = <Student>[].obs;
  var listId = <String>[];
  var controllerNameGroupChat = TextEditingController();
  var focusNameGroupChat = FocusNode();
  var isShowErrorNameGroup = false;
  @override
  void onInit() {
    getStudentClassContact();


    super.onInit();
  }



  getStudentClassContact() async {
    if(AppCache().userType == "PARENT"){
      _contactRepo.getClassContactParent(
          Get.find<CreateNewGroupChatAllUserController>().classId.value,
          "PARENT",
          Get.find<ParentHomeController>().currentStudentProfile.value.id,"TRUE").then((value) {
        if (value.state == Status.SUCCESS) {
          contacts.value = value.object!;
          listParent.value = contacts.value.item!;
          listParentCopy.value = contacts.value.item!;
          getListCheckBox();
        }
      });
    }else{
      _contactRepo.getClassContact(Get.find<CreateNewGroupChatAllUserController>().classId.value, "PARENT","TRUE").then((value) {
        if (value.state == Status.SUCCESS) {
          contacts.value = value.object!;
          listParent.value = contacts.value.item!;
          listParentCopy.value = contacts.value.item!;
          getListCheckBox();
        }
      });
    }

  }


  getListCheckBox(){
    listCheckBox.value = [];
    for (int i = 0; i < listParent.length; i++){
      if(Get.find<CreateNewGroupChatController>().listId.contains(listParent[i].id)){
        listCheckBox.add(true);
        listId.add(listParent[i].id!);
      }else{
        listCheckBox.add(false);
      }
    }
  }



  checkBoxListView(index) {
    if (listCheckBox[index] == false) {
      listCheckBox[index] = true;
      if(listId.contains(listParent[index].id!)== true){
      }else{
        listId.add(listParent[index].id!);
      }

      if(listId.contains(listParent[index].id!)== true){
      }else{
        listId.add(listParent[index].id!);
      }


      for(int i = 0 ;i<Get.find<ListAllUserCreateNewGroupChatController>().listAll.length;i++){
        if(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id !=listParent[index].id!){
        }else{
          Get.find<ListAllUserCreateNewGroupChatController>().listId.add(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id!);
          Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[i] = true;
        }
      }
      if(Get.find<CreateNewGroupChatController>().listId.contains(listParent[index].id!)== false){
        Get.find<CreateNewGroupChatController>().listId.add(listParent[index].id!);
        Get.find<CreateNewGroupChatController>().listUserInGroupChat.add(listParent[index]);
      }
    } else {
      listCheckBox[index] = false;
      listId.remove(listParent[index].id!);

      for(int i = 0 ;i<Get.find<ListAllUserCreateNewGroupChatController>().listAll.length;i++){
        if(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id !=listParent[index].id!){
        }else{
          Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[i] = false;
          Get.find<ListAllUserCreateNewGroupChatController>().listId.remove(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id!);
        }
      }
      Get.find<CreateNewGroupChatController>().listId.remove(listParent[index].id!);
      Get.find<CreateNewGroupChatController>().listUserInGroupChat.removeWhere((element) => element.id == listParent[index].id!);
    }

    if(Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox.contains(false) == true){
      Get.find<ListAllUserCreateNewGroupChatController>().isCheckAll.value = false;
    }else{
      Get.find<ListAllUserCreateNewGroupChatController>().isCheckAll.value = true;
    }
  }


  checkAll() {
    if (isCheckAll.value == true) {
      listCheckBox.value = [];
      for (int i = 0; i < listParent.length; i++){
        if(listId.contains(listParent[i].id!) == true){
        }else{
          listId.add(listParent[i].id!);
        }
      }

      for(int i = 0 ;i < listParent.length;i++){
        if(Get.find<ListAllUserCreateNewGroupChatController>().listId.contains(listParent[i].id) == false){
          Get.find<ListAllUserCreateNewGroupChatController>().listId.add(listParent[i].id!);
        }
      }

      for (int i = 0; i < Get.find<ListAllUserCreateNewGroupChatController>().listAll.length; i++) {
        if(Get.find<ListAllUserCreateNewGroupChatController>().listId.contains(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id) == true){
          Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[i] = true;
        }else{
          Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[i] = false;
        }
      }
      for (int i = 0; i < listId.length; i++) {
        listCheckBox.add(true);
      }
      for (int i = 0; i < listParent.length; i++){
        if(Get.find<CreateNewGroupChatController>().listId.contains(listParent[i].id!) == false){
          Get.find<CreateNewGroupChatController>().listId.add(listParent[i].id!);
          Get.find<CreateNewGroupChatController>().listUserInGroupChat.add(listParent[i]);
        }
      }
    } else {
      listCheckBox.value = [];
      for (int i = 0; i < listParent.length; i++) {
        listCheckBox.add(false);
        if(listId.contains(listParent[i].id!) == true){
          listId.remove(listParent[i].id!);
        }
      }

      for (int i = 0; i < listParent.length; i++) {
        if(Get.find<ListAllUserCreateNewGroupChatController>().listId.contains(listParent[i].id!) == true){
          Get.find<ListAllUserCreateNewGroupChatController>().listId.remove(listParent[i].id!);
        }
      }
      for (int i = 0; i < Get.find<ListAllUserCreateNewGroupChatController>().listAll.length; i++) {
        if(Get.find<ListAllUserCreateNewGroupChatController>().listId.contains(Get.find<ListAllUserCreateNewGroupChatController>().listAll[i].id) == true){
          Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[i] = true;
        }else{
          Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox[i] = false;
        }
      }
      for (int i = 0; i < listParent.length; i++) {
        if(Get.find<CreateNewGroupChatController>().listId.contains(listParent[i].id!) == true){
          Get.find<CreateNewGroupChatController>().listId.remove(listParent[i].id!);
          Get.find<CreateNewGroupChatController>().listUserInGroupChat.removeWhere((element) => element.id == listParent[i].id);
        }
      }
    }
    if(Get.find<ListAllUserCreateNewGroupChatController>().listCheckBox.contains(false) == true){
      Get.find<ListAllUserCreateNewGroupChatController>().isCheckAll.value = false;
    }else{
      Get.find<ListAllUserCreateNewGroupChatController>().isCheckAll.value = true;
    }

  }





}