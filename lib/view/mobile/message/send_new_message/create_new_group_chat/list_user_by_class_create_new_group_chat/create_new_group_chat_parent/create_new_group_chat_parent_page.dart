import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:tiengviet/tiengviet.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../commom/utils/createGroupChat.dart';
import '../../create_new_group_chat_controller.dart';
import 'create_new_group_chat_parent_controller.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';

class CreateNewGroupChatParentPage extends GetView<CreateNewGroupChatParentController>{
  @override
  final controller = Get.put(CreateNewGroupChatParentController());

  CreateNewGroupChatParentPage({super.key});


  @override
  Widget build(BuildContext context) {
    return
      WillPopScope(child: SafeArea(
          child: GestureDetector(
            onTap: (){
              FocusManager.instance.primaryFocus?.unfocus();
            },

            child: Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: AppBar(
                backgroundColor: ColorUtils.PRIMARY_COLOR,
                elevation: 0,
                title: Text(
                  "Tạo nhóm chat mới",
                  style: TextStyle(
                      color: Colors.white, fontSize: 16.sp, fontWeight: FontWeight.w500),
                ),
                actions: [
                  InkWell(
                    onTap: () {
                     comeToHome();
                    },
                    child: const Icon(
                      Icons.home,
                      color: Colors.white,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(right: 16.w))
                ],
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 8.h),
                    child: Row(
                      children: [
                        Expanded(
                          child: SizedBox(
                            height: 40.h,
                            child: TextFormField(
                              onChanged: (value) {
                                controller.listParent.value = [];
                                controller.listCheckBox.value = [];
                                controller.listParent.value = controller.listParentCopy.where((element) => TiengViet.parse(element.fullName!.toLowerCase()).contains(TiengViet.parse(value.toLowerCase().trim())) == true).toList();
                                for(int i= 0; i <controller.listParent.length; i++){
                                  controller.listCheckBox.add(false);
                                }
                                for(int j = 0; j <controller.listParent.length; j++){
                                  if(controller.listId.contains(controller.listParent[j].id) ){
                                    controller.listCheckBox[j] = true;
                                  }else{
                                    controller.listCheckBox[j] = false;
                                  }
                                }
                                if(controller.listCheckBox.isNotEmpty){
                                  if(controller.listCheckBox.contains(false) == true){
                                    controller.isCheckAll.value = false;
                                  }else{
                                    controller.isCheckAll.value = true;
                                  }
                                }
                                controller.listParent.refresh();
                                controller.listCheckBox.refresh();
                              },
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Color.fromRGBO(177, 177, 177, 1)),
                                      borderRadius: BorderRadius.circular(6)),
                                  prefixIcon: const Icon(
                                    Icons.search,
                                    color: Color.fromRGBO(177, 177, 177, 1),
                                  ),
                                  hintText: "Tìm kiếm",
                                  isCollapsed: true,
                                  hintStyle: TextStyle(
                                      fontSize: 14.sp,
                                      color: const Color.fromRGBO(177, 177, 177, 1)),
                                  focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color:ColorUtils.PRIMARY_COLOR))),
                              textAlignVertical: TextAlignVertical.center,
                              cursorColor: ColorUtils.PRIMARY_COLOR,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 16.h)),
                  Expanded(
                      child: Obx(() => Container(
                        decoration: BoxDecoration(
                            color: Colors.white, borderRadius: BorderRadius.circular(6)),
                        padding: const EdgeInsets.all(16),
                        margin:  EdgeInsets.fromLTRB(16.w, 0, 16.w, 16.h),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                InkWell(
                                  onTap: () {
                                    controller.isCheckAll.value = !controller.isCheckAll.value;
                                    controller.checkAll();
                                  },
                                  child: Row(
                                    children: [
                                      Container(
                                          width: 18.h,
                                          height: 18.h,
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.all(2.h),
                                          margin: const EdgeInsets.only(right: 8),
                                          decoration: ShapeDecoration(
                                            shape: CircleBorder(
                                                side: BorderSide(color: controller.isCheckAll.value ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(235, 235, 235, 1))),
                                          ),
                                          child: controller.isCheckAll.value
                                              ? const Icon(Icons.circle, size: 12,
                                            color:ColorUtils.PRIMARY_COLOR,
                                          )
                                              : null),
                                      Padding(padding: EdgeInsets.only(left: 8.w)),
                                      Text(
                                        "Chọn hết",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.black),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(child: Container()),
                                Text(
                                  "Đã chọn ${controller.selectedQuantity.value} /${controller.listParentCopy.length}",
                                  style: TextStyle(
                                      color: const Color.fromRGBO(177, 177, 177, 1),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500),
                                )
                              ],
                            ),
                            Padding(padding: EdgeInsets.only(top: 16.h)),
                            Expanded(
                              child: ListView.builder(
                                  itemCount: controller.listParent.length,
                                  shrinkWrap: true,
                                  physics: const ScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    controller.students.value = controller.listParent[index].student!;
                                    return InkWell(
                                      onTap: () {
                                        controller.checkBoxListView(index);
                                        if(controller.listCheckBox.contains(false) == true){
                                          controller.isCheckAll.value = false;
                                        }else{
                                          controller.isCheckAll.value = true;
                                        }

                                      },
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                  width: 18.h,
                                                  height: 18.h,
                                                  alignment: Alignment.center,
                                                  padding: EdgeInsets.all(2.h),
                                                  margin: const EdgeInsets.only(right: 8),
                                                  decoration: ShapeDecoration(
                                                    shape: CircleBorder(
                                                        side: BorderSide(color: controller.listCheckBox[index] ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(235, 235, 235, 1))),
                                                  ),
                                                  child: controller.listCheckBox[index]
                                                      ? const Icon(Icons.circle, size: 12,
                                                    color:ColorUtils.PRIMARY_COLOR,
                                                  )
                                                      : null),
                                              Padding(padding: EdgeInsets.only(left: 8.w)),
                                              SizedBox(
                                                width: 32.h,
                                                height: 32.h,
                                                child:
                                                CacheNetWorkCustom(urlImage: '${controller.listParent[index].image}',)

                                              ),
                                              Padding(padding: EdgeInsets.only(left: 8.w)),
                                              Expanded(child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "${controller.listParent[index].fullName}",
                                                    style: TextStyle(
                                                        fontSize: 16.sp,
                                                        fontWeight: FontWeight.w500,
                                                        color: Colors.black),
                                                  ),
                                                  SizedBox(
                                                    height: 4.h,
                                                  ),
                                                  controller.students.isNotEmpty?
                                                  RichText(
                                                    textAlign: TextAlign.left,
                                                    text: TextSpan(
                                                      text: 'Phụ huynh em: ',
                                                      style: TextStyle(
                                                          color: const Color.fromRGBO(133, 133, 133, 1),
                                                          fontSize: 12.sp,
                                                          fontWeight: FontWeight.w500,
                                                          fontFamily: 'static/Inter-Medium.ttf'),

                                                      children: controller.students.map((e) {
                                                        var indexName = controller.students.indexOf(e);
                                                        var showSplit = ", ";
                                                        if (indexName == controller.students.length - 1) {
                                                          showSplit = "";
                                                        }
                                                        return TextSpan(
                                                            text: "${e.fullName}$showSplit",
                                                            style: TextStyle(
                                                                color: Colors.black,
                                                                fontSize: 12.sp,
                                                                fontWeight: FontWeight.w500,
                                                                fontFamily: 'static/Inter-Medium.ttf'));
                                                      }).toList(),
                                                    ),
                                                  ):Container()
                                                ],
                                              ))
                                            ],
                                          ),
                                          const Divider(),
                                        ],
                                      ),
                                    );
                                  }),
                            ),

                            Visibility(
                                visible:  Get.find<CreateNewGroupChatController>().listId.isNotEmpty,
                                child: Row(
                                  children: [
                                    Expanded(child: Container(
                                      margin: EdgeInsets.only(top: 8.h),
                                      decoration: BoxDecoration(
                                        border: Border.all(color: const Color.fromRGBO(239, 239, 239, 1),),
                                      ),
                                      height: 86.h,
                                      child: ListView.builder(
                                          itemCount: Get.find<CreateNewGroupChatController>().listUserInGroupChat.length,
                                          physics: const ScrollPhysics(),
                                          scrollDirection: Axis.horizontal,
                                          itemBuilder: (context,index){
                                            return Container(
                                              margin: EdgeInsets.symmetric(horizontal: 8.w),
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    color: Colors.white,
                                                    child: Stack(
                                                      children: [
                                                        Container(
                                                          margin: EdgeInsets.all(8.w),
                                                          width: 32.h,
                                                          height: 32.h,
                                                          child:

                                                          CacheNetWorkCustom(urlImage: '${Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].image }',)

                                                        ),
                                                        Positioned(
                                                            top: 4,
                                                            right: 0,
                                                            child: InkWell(
                                                              onTap: () {
                                                                controller.selectedQuantity.value = 0;
                                                                controller.listId.remove(Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].id);
                                                                Get.find<CreateNewGroupChatController>().listId.remove(Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].id);
                                                                controller.listCheckBox.value = [];
                                                                for(int i = 0;i <controller.listParent.length;i++){
                                                                  if(controller.listId.contains(controller.listParent[i].id)){
                                                                    controller.listCheckBox.add(true);
                                                                    controller.selectedQuantity++;
                                                                  }else{
                                                                    controller.listCheckBox.add(false);
                                                                  }
                                                                }
                                                                Get.find<CreateNewGroupChatController>().listUserInGroupChat.removeAt(index);
                                                                if(controller.listCheckBox.contains(false) == true){
                                                                  controller.isCheckAll.value = false;
                                                                }else{
                                                                  controller.isCheckAll.value = true;
                                                                }
                                                              },
                                                              child: Container(
                                                                padding: EdgeInsets.zero,
                                                                decoration: const BoxDecoration(
                                                                  color: Colors.white,
                                                                  shape: BoxShape.circle,
                                                                ),
                                                                child: SvgPicture.asset("assets/images/icon_dangerous.svg",fit: BoxFit.cover,height: 18.h,width: 18.h,),
                                                              ),
                                                            ))
                                                      ],
                                                    ) ,
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(top: 8.h),
                                                    child:  Text("${ Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].fullName}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.w400,fontSize: 14),),
                                                  )
                                                ],
                                              ),
                                            );
                                          }),
                                    )),
                                    SizedBox(
                                      width: 8.w,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        if(Get.find<CreateNewGroupChatController>().listId.length<2) {
                                          AppUtils.shared.showToast("Vui lòng chọn nhiều hơn 1 người để tạo nhóm");
                                        }else{
                                          Get.dialog(_onDialogConfirm());
                                        }
                                      },
                                      child: const Icon(Icons.send_rounded,color: ColorUtils.PRIMARY_COLOR,size: 30,),
                                    )
                                  ],
                                ))
                          ],
                        ),
                      ))),
                ],
              ),
            ),
          )), onWillPop: () async{
            Get.find<CreateNewGroupChatController>().indexItemParents = -1;
            Get.find<CreateNewGroupChatController>().update();
            return true;
          },);
  }
  _onDialogConfirm() {
    return GetBuilder<CreateNewGroupChatParentController>(builder: (controller) {
      return Center(
        child: Wrap(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 32.w),
              child: Card(
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(8),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16.w,
                    vertical: 16.h,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            'Đổi Tên Nhóm',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                            ),
                          ),
                          IconButton(
                            padding: EdgeInsets.zero,
                            constraints: const BoxConstraints(),
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            onPressed: () {
                              Get.back();
                            },
                            icon: const Icon(
                              Icons.close_rounded,
                              size: 20.0,
                              color: Color(0xFF7D7E7E),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      FocusScope(
                        node: FocusScopeNode(),
                        child: Focus(
                            onFocusChange: (focus) {
                              controller.update();
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding: const EdgeInsets.all(2.0),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                      const BorderRadius.all(Radius.circular(6.0)),
                                      border: Border.all(
                                        width: 1,
                                        style: BorderStyle.solid,
                                        color: controller.focusNameGroupChat.hasFocus
                                            ? ColorUtils.PRIMARY_COLOR
                                            : const Color.fromRGBO(192, 192, 192, 1),
                                      )),
                                  child: TextFormField(
                                    cursorColor: ColorUtils.PRIMARY_COLOR,
                                    focusNode: controller.focusNameGroupChat,
                                    onChanged: (value) {
                                      if(controller.controllerNameGroupChat.text.trim() != ""){
                                        controller.isShowErrorNameGroup = false;
                                      }
                                      controller.update();
                                    },
                                    onFieldSubmitted: (value) {
                                      controller.update();
                                    },
                                    controller: controller.controllerNameGroupChat,
                                    decoration: InputDecoration(
                                      labelText: "Nhập tên nhóm",
                                      labelStyle: TextStyle(
                                          fontSize: 14.0.sp,
                                          color: const Color.fromRGBO(177, 177, 177, 1),
                                          fontWeight: FontWeight.w500,
                                          fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                                      contentPadding: const EdgeInsets.symmetric(
                                          vertical: 8, horizontal: 16),
                                      prefixIcon:  null,
                                      suffixIcon:  Visibility(
                                          visible:
                                          controller.controllerNameGroupChat.text.isNotEmpty ==
                                              true,
                                          child: InkWell(
                                            onTap: () {
                                              controller.controllerNameGroupChat.text = "";
                                            },
                                            child: const Icon(
                                              Icons.close_outlined,
                                              color: Colors.black,
                                              size: 20,
                                            ),
                                          )),
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      border: InputBorder.none,
                                      errorStyle: const TextStyle(height: 0),
                                      focusedErrorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      floatingLabelBehavior: FloatingLabelBehavior.auto,

                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: controller.isShowErrorNameGroup,
                                  child: Container(
                                      padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                      child: const Text(
                                        "Vui lòng nhập tên nhóm",
                                        style: TextStyle(color: Colors.red),
                                      )),
                                )
                              ],
                            )),
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          ElevatedButton.icon(
                            onPressed: () {
                              controller.controllerNameGroupChat.text = "";
                              Get.back();
                            },
                            style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0),
                                  side: const BorderSide(
                                      color: Color(0xFFC0C0C0), width: 0.75),
                                ),
                                backgroundColor: Colors.white),
                            icon:
                            const Icon(Icons.close_rounded, color: Colors.black),
                            label: const Text(
                              'Huỷ',
                              style: TextStyle(
                                color: Color(0xFF333333),
                                fontSize: 14,
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: ColorUtils.PRIMARY_COLOR,
                            ),
                            onPressed: () {
                              if (controller.controllerNameGroupChat.text.trim() == "") {
                                controller.isShowErrorNameGroup = true;
                                controller.update();
                              } else {
                                createNewGroupChat(controller.controllerNameGroupChat.text.trim(), "GROUP",  Get.find<CreateNewGroupChatController>().listId);
                              }
                            },
                            icon: const Icon(Icons.check),
                            label: const Text('Xác nhận'),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    },);
  }

}