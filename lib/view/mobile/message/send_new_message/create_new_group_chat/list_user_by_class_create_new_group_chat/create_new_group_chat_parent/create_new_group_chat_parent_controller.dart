import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../../data/repository/contact/contact_repo.dart';
import '../../../../../../../commom/app_cache.dart';
import '../../create_new_group_chat_controller.dart';

class CreateNewGroupChatParentController extends GetxController{
  var selectedQuantity = 0.obs;
  var clickAll = <bool>[].obs;
  var listCheckBox = <bool>[].obs;
  var isCheckAll = false.obs;
  final ContactRepo _contactRepo = ContactRepo();
  var contacts = Contacts().obs;
  RxList<ItemContact> listParent = <ItemContact>[].obs;
  RxList<ItemContact> listParentCopy = <ItemContact>[].obs;
  var students = <Student>[].obs;
  var classId = "".obs;
  var listId = <String>[];
  var controllerNameGroupChat = TextEditingController();
  var focusNameGroupChat = FocusNode();
  var isShowErrorNameGroup = false;
  @override
  void onInit() {
    var tmpClassId = Get.arguments;
    if(tmpClassId!=null){
      classId.value = tmpClassId;
    }
    getStudentClassContact();
    super.onInit();
  }




  getStudentClassContact() async {
    if(AppCache().userType == "PARENT"){
      _contactRepo.getClassContactParent(classId, "PARENT",Get.find<ParentHomeController>().currentStudentProfile.value.id,"TRUE").then((value) {
        if (value.state == Status.SUCCESS) {
          contacts.value = value.object!;
          listParent.value = contacts.value.item!;
          listParentCopy.value = contacts.value.item!;
          getListCheckBox();
        }
      });
    }else{
      _contactRepo.getClassContact(classId, "PARENT","TRUE").then((value) {
        if (value.state == Status.SUCCESS) {
          contacts.value = value.object!;
          listParent.value = contacts.value.item!;
          listParentCopy.value = contacts.value.item!;
          getListCheckBox();
        }
      });
    }

  }


  getListCheckBox(){
    listCheckBox.value = [];
    for (int i = 0; i < listParent.length; i++){
      if(Get.find<CreateNewGroupChatController>().listId.contains(listParent[i].id)){
        listCheckBox.add(true);
        listId.add(listParent[i].id!);
        selectedQuantity.value++;
      }else{
        listCheckBox.add(false);
      }
    }
  }


  checkBoxListView(index) {
    if (listCheckBox[index] == false) {
      listCheckBox[index] = true;
      selectedQuantity++;
      if(listId.contains(listParent[index].id!)== true){

      }else{
        listId.add(listParent[index].id!);
      }
      if(Get.find<CreateNewGroupChatController>().listId.contains(listParent[index].id!)== false){
        Get.find<CreateNewGroupChatController>().listId.add(listParent[index].id!);
        Get.find<CreateNewGroupChatController>().listUserInGroupChat.add(listParent[index]);
      }

    } else {
      listCheckBox[index] = false;
      selectedQuantity--;
      listId.remove(listParent[index].id!);
      Get.find<CreateNewGroupChatController>().listId.remove(listParent[index].id!);
      Get.find<CreateNewGroupChatController>().listUserInGroupChat.removeWhere((element) => element.id == listParent[index].id!);
    }
  }


  checkAll() {
    if (isCheckAll.value == true) {
      listCheckBox.value = [];
      for (int i = 0; i < listParent.length; i++){
        if(listId.contains(listParent[i].id!) == true){
        }else{
          listId.add(listParent[i].id!);
        }
      }

      for (int i = 0; i < listParent.length; i++){
        if(Get.find<CreateNewGroupChatController>().listId.contains(listParent[i].id!) == false){
          Get.find<CreateNewGroupChatController>().listId.add(listParent[i].id!);
          Get.find<CreateNewGroupChatController>().listUserInGroupChat.add(listParent[i]);
        }
      }

      for (int i = 0; i < listId.length; i++) {
        listCheckBox.add(true);
      }
      selectedQuantity.value = listParent.length;
    } else {
      listCheckBox.value = [];
      for (int i = 0; i < listParent.length; i++) {
        listCheckBox.add(false);
        if(listId.contains(listParent[i].id!) == true){
          listId.remove(listParent[i].id!);
        }
      }
      for (int i = 0; i < listParent.length; i++) {
        if(Get.find<CreateNewGroupChatController>().listId.contains(listParent[i].id!) == true){
          Get.find<CreateNewGroupChatController>().listId.remove(listParent[i].id!);
          Get.find<CreateNewGroupChatController>().listUserInGroupChat.removeWhere((element) => element.id == listParent[i].id);
        }
      }
      selectedQuantity.value = 0;
    }
  }





}