import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/action/role_action.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import '../../../../../commom/app_cache.dart';
import '../../../../../commom/utils/app_utils.dart';
import '../../../../../commom/utils/color_utils.dart';
import '../../../../../commom/utils/createGroupChat.dart';
import '../../../../../routes/app_pages.dart';
import '../../../role/parent/parent_home_controller.dart';
import 'create_new_group_chat_controller.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';


class CreateNewGroupChatPage extends GetWidget<CreateNewGroupChatController> {
  @override
  final controller = Get.put(CreateNewGroupChatController());

  CreateNewGroupChatPage({super.key});

  getViewStudent(role) {
    switch (role) {
      case RoleAction.STUDENT:
        return true;
      case RoleAction.PARENT:
        return false;
      case RoleAction.TEACHER:
        return true;
      case RoleAction.MANAGER:
        return true;
    }
  }

  getViewParents(role) {
    switch (role) {
      case RoleAction.STUDENT:
        return false;
      case RoleAction.PARENT:
        return true;
      case RoleAction.TEACHER:
        return true;
      case RoleAction.MANAGER:
        return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title: const Text(
            "Tạo nhóm mới",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
          actions: [
            InkWell(
              onTap: () {
                comeToHome();

              },
              child: const Icon(
                Icons.home,
                color: Colors.white,
              ),
            ),
            const Padding(padding: EdgeInsets.only(right: 16))
          ],
          elevation: 0,
        ),
        body: Obx(() => RefreshIndicator(
            color: ColorUtils.PRIMARY_COLOR,
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Container(
                margin:  EdgeInsets.only(left: 16.w, right: 16.w, top: 8.h),
                child: Column(
                  children: [
                    Visibility(
                        visible:  Get.find<CreateNewGroupChatController>().listId.isNotEmpty,
                        child: Row(
                          children: [
                            Expanded(child: Container(
                              margin: EdgeInsets.only(top: 8.h),
                              decoration: BoxDecoration(
                                border: Border.all(color: const Color.fromRGBO(239, 239, 239, 1),),
                              ),
                              height: 86.h,
                              child: ListView.builder(
                                  itemCount: Get.find<CreateNewGroupChatController>().listUserInGroupChat.length,
                                  physics: const ScrollPhysics(),
                                  padding: EdgeInsets.zero,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context,index){
                                    return Container(
                                      margin: EdgeInsets.symmetric(horizontal: 8.w),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Stack(
                                            children: [
                                              Container(
                                                margin: EdgeInsets.all(8.h),
                                                width: 32.h,
                                                height: 32.h,
                                                child:
                                                CacheNetWorkCustom(urlImage: '${  Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].image}',)
                                              ),
                                              Positioned(
                                                  top: 4,
                                                  right: 0,
                                                  child: InkWell(
                                                    onTap: () {
                                                      controller.listId.remove(Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].id);
                                                      Get.find<CreateNewGroupChatController>().listUserInGroupChat.removeAt(index);

                                                    },
                                                    child: Container(
                                                      padding: EdgeInsets.zero,
                                                      decoration: const BoxDecoration(
                                                        color: Colors.white,
                                                        shape: BoxShape.circle,
                                                      ),
                                                      child: SvgPicture.asset("assets/images/icon_dangerous.svg",fit: BoxFit.cover,height: 18.h,width: 18.h,),
                                                    ),
                                                  ))
                                            ],
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 8.h),
                                            child:  Text("${ Get.find<CreateNewGroupChatController>().listUserInGroupChat[index].fullName}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.w400,fontSize: 14),),
                                          )
                                        ],
                                      ),
                                    );
                                  }),
                            )),
                            SizedBox(
                              width: 8.w,
                            ),
                            InkWell(
                              onTap: () {
                                if(controller.listId.length<2) {
                                  AppUtils.shared.showToast("Vui lòng chọn nhiều hơn 1 người để tạo nhóm");
                                }else{
                                  Get.dialog(_onDialogConfirm());
                                }
                              },
                              child: const Icon(Icons.send_rounded,color: ColorUtils.PRIMARY_COLOR,size: 30,),
                            )
                          ],
                        )),
                    SizedBox(
                      height: 8.h,
                    ),
                    Visibility(
                        visible: getViewStudent(AppCache().userType),
                        child: InkWell(
                          onTap: () {
                            controller.isExpandStudent.value =
                            !controller.isExpandStudent.value;
                            controller.indexItemStudent = -1;
                            if(controller.classOfUser.isEmpty){
                              controller.getListClassByStudent();
                            }
                          },
                          child: Card(
                            elevation: 1,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)),
                            child: Container(
                              padding: const EdgeInsets.all(16),
                              child: Row(
                                children: [
                                  const Text(
                                    "Học Sinh",
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: Color.fromRGBO(26, 26, 26, 1),
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Expanded(child: Container()),
                                  Visibility(
                                    visible: controller.isExpandStudent.value ==
                                        false,
                                    child: const Icon(
                                      Icons.keyboard_arrow_right,
                                      color: ColorUtils.PRIMARY_COLOR,
                                      size: 28,
                                    ),
                                  ),
                                  Visibility(
                                    visible: controller.isExpandStudent.value ==
                                        true,
                                    child: const Icon(
                                      Icons.keyboard_arrow_up,
                                      color: ColorUtils.PRIMARY_COLOR,
                                      size: 28,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )),
                    Container(
                      margin: EdgeInsets.only(right: 8.w, left: 8.w),
                      child: Visibility(
                          visible: controller.isExpandStudent.value == true,
                          child: ListView.builder(
                              itemCount: controller.classOfUser.length,
                              shrinkWrap: true,
                              padding: EdgeInsets.zero,
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return GetBuilder<CreateNewGroupChatController>(builder: (controller) {
                                  return InkWell(
                                    onTap: () {
                                      controller.indexItemStudent = index;
                                      Get.toNamed(Routes.createNewGroupChatStudentPage,arguments: controller.classOfUser[index].id);
                                      controller.update();
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: index == controller.indexItemStudent ? const Color.fromRGBO(
                                              249, 154, 81, 1)
                                              : Colors.white,
                                          borderRadius:
                                          BorderRadius.circular(12)),
                                      margin: const EdgeInsets.only(
                                          top: 8, bottom: 8),
                                      padding: const EdgeInsets.only(
                                          left: 21, top: 13, bottom: 13),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            "assets/images/icon_class.png",width: 18.w,height: 18.h,
                                            color: index == controller.indexItemStudent
                                                ? Colors.white
                                                : const Color.fromRGBO(
                                                90, 90, 90, 1),
                                          ),
                                          const Padding(
                                              padding:
                                              EdgeInsets.only(left: 8)),
                                          Text(
                                            "${controller.classOfUser[index].name}",
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: index ==
                                                    controller
                                                        .indexItemStudent
                                                    ? Colors.white
                                                    : const Color.fromRGBO(
                                                    90, 90, 90, 1)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },);
                              })),
                    ),
                    Visibility(
                      visible: getViewParents(AppCache().userType),
                      child: Padding(padding: EdgeInsets.only(bottom: 8.h)),
                    ),
                    Visibility(
                        visible: getViewParents(AppCache().userType),
                        child: InkWell(
                          onTap: () {
                            controller.isExpandParents.value =
                            !controller.isExpandParents.value;
                            if(AppCache().userType == "PARENT"){
                              controller.queryListClassByParent(Get.find<ParentHomeController>().currentStudentProfile.value.id);
                            }else{
                              controller.getListClassByStudent();
                            }
                          },
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)),
                            child: Container(
                              padding: const EdgeInsets.all(16),
                              child: Row(
                                children: [
                                  const Text(
                                    "Phụ Huynh Học Sinh",
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: Color.fromRGBO(26, 26, 26, 1),
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Expanded(child: Container()),
                                  Visibility(
                                    visible: controller.isExpandParents.value ==
                                        false,
                                    child: const Icon(
                                      Icons.keyboard_arrow_right,
                                      color: ColorUtils.PRIMARY_COLOR,
                                      size: 28,
                                    ),
                                  ),
                                  Visibility(
                                    visible: controller.isExpandParents.value ==
                                        true,
                                    child: const Icon(
                                      Icons.keyboard_arrow_up,
                                      color: ColorUtils.PRIMARY_COLOR,
                                      size: 28,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        )),
                    Container(
                      margin: const EdgeInsets.only(right: 6, left: 6),
                      child: Visibility(
                          visible: controller.isExpandParents.value == true,
                          child: ListView.builder(
                              itemCount: controller.classOfUser.length,
                              shrinkWrap: true,
                              padding: EdgeInsets.zero,
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return GetBuilder<CreateNewGroupChatController>(builder: (controller) {
                                  return itemClass(index);
                                },);
                              })),
                    ),
                    const Padding(padding: EdgeInsets.only(bottom: 8)),
                    InkWell(
                      onTap: () {
                        controller.isExpandTeacher.value =
                        !controller.isExpandTeacher.value;
                        if(controller.classOfUser.isEmpty){
                          if(AppCache().userType == "PARENT"){
                            controller.queryListClassByParent(Get.find<ParentHomeController>().currentStudentProfile.value.id);
                          }else{
                            controller.getListClassByStudent();
                          }
                        }
                      },
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)),
                        child: Container(
                          padding: const EdgeInsets.all(16),
                          child: Row(
                            children: [
                              const Text(
                                "Giáo Viên",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Color.fromRGBO(26, 26, 26, 1),
                                    fontWeight: FontWeight.bold),
                              ),
                              Expanded(child: Container()),
                              const Icon(
                                Icons.keyboard_arrow_right,
                                color: ColorUtils.PRIMARY_COLOR,
                                size: 28,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 6, left: 6),
                      child: Visibility(
                          visible: controller.isExpandTeacher.value == true,
                          child: ListView.builder(
                              itemCount: controller.classOfUser.length,
                              shrinkWrap: true,
                              padding: EdgeInsets.zero,
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return GetBuilder<CreateNewGroupChatController>(builder: (controller) {
                                  return InkWell(
                                    onTap: () {
                                      controller.indexItemTeacher = index;
                                      controller.update();
                                      Get.toNamed(Routes.createNewGroupChatTeacherPage,arguments: controller.classOfUser[index].id);
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: index ==
                                              controller
                                                  .indexItemTeacher
                                              ? const Color.fromRGBO(
                                              249, 154, 81, 1)
                                              : Colors.white,
                                          borderRadius:
                                          BorderRadius.circular(12)),
                                      margin: const EdgeInsets.only(
                                          top: 8, bottom: 8),
                                      padding: const EdgeInsets.only(
                                          left: 21, top: 13, bottom: 13),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            "assets/images/icon_class.png",width: 18.w,height: 18.h,
                                            color: index == controller.indexItemTeacher
                                                ? Colors.white
                                                : const Color.fromRGBO(
                                                90, 90, 90, 1),
                                          ),
                                          const Padding(
                                              padding:
                                              EdgeInsets.only(left: 8)),
                                          Text(
                                            "${controller.classOfUser[index].name}",
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: index ==
                                                    controller
                                                        .indexItemTeacher
                                                    ? Colors.white
                                                    : const Color.fromRGBO(
                                                    90, 90, 90, 1)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },);
                              })),
                    ),
                    const Padding(padding: EdgeInsets.only(bottom: 8)),
                    InkWell(
                      onTap: () {
                        controller.isExpandAll.value =
                        !controller.isExpandAll.value;
                        if(controller.classOfUser.isEmpty){
                          if(AppCache().userType == "PARENT"){
                            controller.queryListClassByParent(Get.find<ParentHomeController>().currentStudentProfile.value.id);
                          }else{
                            controller.getListClassByStudent();
                          }
                        }

                      },
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)),
                        child: Container(
                          padding: const EdgeInsets.all(16),
                          child: Row(
                            children: [
                              const Text(
                                "Tất Cả",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Color.fromRGBO(26, 26, 26, 1),
                                    fontWeight: FontWeight.bold),
                              ),
                              Expanded(child: Container()),
                              const Icon(
                                Icons.keyboard_arrow_right,
                                color: ColorUtils.PRIMARY_COLOR,
                                size: 28,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 6, left: 6),
                      child: Visibility(
                          visible: controller.isExpandAll.value == true,
                          child: ListView.builder(
                              itemCount: controller.classOfUser.length,
                              shrinkWrap: true,
                              padding: EdgeInsets.zero,
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return GetBuilder<CreateNewGroupChatController>(builder: (controller) {
                                  return InkWell(
                                    onTap: () {
                                      controller.indexItemAll = index;
                                      Get.toNamed(Routes.createNewGroupChatAllUserPage,arguments: controller.classOfUser[index].id);
                                      controller.update();
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: index ==
                                              controller
                                                  .indexItemAll
                                              ? const Color.fromRGBO(
                                              249, 154, 81, 1)
                                              : Colors.white,
                                          borderRadius:
                                          BorderRadius.circular(12)),
                                      margin: const EdgeInsets.only(
                                          top: 8, bottom: 8),
                                      padding: const EdgeInsets.only(
                                          left: 21, top: 13, bottom: 13),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            "assets/images/icon_class.png",width: 18.w,height: 18.h,
                                            color: index == controller.indexItemAll
                                                ? Colors.white
                                                : const Color.fromRGBO(
                                                90, 90, 90, 1),
                                          ),
                                          const Padding(
                                              padding:
                                              EdgeInsets.only(left: 8)),
                                          Text(
                                            "${controller.classOfUser[index].name}",
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: index ==
                                                    controller
                                                        .indexItemAll
                                                    ? Colors.white
                                                    : const Color.fromRGBO(
                                                    90, 90, 90, 1)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },);
                              })),
                    ),
                  ],
                ),
              ),
            ),
            onRefresh: () async {
              if(AppCache().userType == "PARENT"){
                controller.queryListClassByParent(Get.find<ParentHomeController>().currentStudentProfile.value.id);
              }else{
                controller.getListClassByStudent();
              }
            })));
  }

  itemClass(int index) {
    return InkWell(
      onTap: () async{
        controller.indexItemParents = index;
        controller.update();
        Get.toNamed(Routes.createNewGroupChatParentPage,arguments: controller.classOfUser[index].id);

      },
      child: Container(
        decoration: BoxDecoration(
            color: index == controller.indexItemParents
                ? const Color.fromRGBO(249, 154, 81, 1)
                : Colors.white,
            borderRadius: BorderRadius.circular(12)),
        margin: const EdgeInsets.only(top: 8, bottom: 8),
        padding: const EdgeInsets.only(left: 21, top: 13, bottom: 13),
        child: Row(
          children: [
            Image.asset(
              "assets/images/icon_class.png",width: 18.w,height: 18.h,
              color: index ==
                  controller
                      .indexItemParents
                  ? Colors.white
                  : const Color.fromRGBO(
                  90, 90, 90, 1),
            ),
            const Padding(padding: EdgeInsets.only(left: 8)),
            Text(
              "${controller.classOfUser[index].name}",
              style: TextStyle(
                  fontSize: 14,
                  color: index == controller.indexItemParents
                      ? Colors.white
                      : const Color.fromRGBO(90, 90, 90, 1)),
            ),
          ],
        ),
      ),
    );
  }



  _onDialogConfirm() {
    return GetBuilder<CreateNewGroupChatController>(builder: (controller) {
      return Center(
        child: Wrap(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 32.w),
              child: Card(
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(8),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16.w,
                    vertical: 16.h,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            'Đổi Tên Nhóm',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                            ),
                          ),
                          IconButton(
                            padding: EdgeInsets.zero,
                            constraints: const BoxConstraints(),
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            onPressed: () {
                              Get.back();
                            },
                            icon: const Icon(
                              Icons.close_rounded,
                              size: 20.0,
                              color: Color(0xFF7D7E7E),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      FocusScope(
                        node: FocusScopeNode(),
                        child: Focus(
                            onFocusChange: (focus) {
                              controller.update();
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding: const EdgeInsets.all(2.0),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                      const BorderRadius.all(Radius.circular(6.0)),
                                      border: Border.all(
                                        width: 1,
                                        style: BorderStyle.solid,
                                        color: controller.focusNameGroupChat.hasFocus
                                            ? ColorUtils.PRIMARY_COLOR
                                            : const Color.fromRGBO(192, 192, 192, 1),
                                      )),
                                  child: TextFormField(
                                    cursorColor: ColorUtils.PRIMARY_COLOR,
                                    focusNode: controller.focusNameGroupChat,
                                    onChanged: (value) {
                                      if(controller.controllerNameGroupChat.text.trim() != ""){
                                        controller.isShowErrorNameGroup = false;
                                      }
                                      controller.update();
                                    },
                                    onFieldSubmitted: (value) {
                                      controller.update();
                                    },
                                    controller: controller.controllerNameGroupChat,
                                    decoration: InputDecoration(
                                      labelText: "Nhập tên nhóm",
                                      labelStyle: TextStyle(
                                          fontSize: 14.0.sp,
                                          color: const Color.fromRGBO(177, 177, 177, 1),
                                          fontWeight: FontWeight.w500,
                                          fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                                      contentPadding: const EdgeInsets.symmetric(
                                          vertical: 8, horizontal: 16),
                                      prefixIcon:  null,
                                      suffixIcon:  Visibility(
                                          visible:
                                          controller.controllerNameGroupChat.text.isNotEmpty ==
                                              true,
                                          child: InkWell(
                                            onTap: () {
                                              controller.controllerNameGroupChat.text = "";
                                            },
                                            child: const Icon(
                                              Icons.close_outlined,
                                              color: Colors.black,
                                              size: 20,
                                            ),
                                          )),
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      border: InputBorder.none,
                                      errorStyle: const TextStyle(height: 0),
                                      focusedErrorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      floatingLabelBehavior: FloatingLabelBehavior.auto,

                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: controller.isShowErrorNameGroup,
                                  child: Container(
                                      padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                      child: const Text(
                                        "Vui lòng nhập tên nhóm",
                                        style: TextStyle(color: Colors.red),
                                      )),
                                )
                              ],
                            )),
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          ElevatedButton.icon(
                            onPressed: () {
                              controller.controllerNameGroupChat.text = "";
                              Get.back();
                            },
                            style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0),
                                  side: const BorderSide(
                                      color: Color(0xFFC0C0C0), width: 0.75),
                                ),
                                backgroundColor: Colors.white),
                            icon:
                            const Icon(Icons.close_rounded, color: Colors.black),
                            label: const Text(
                              'Huỷ',
                              style: TextStyle(
                                color: Color(0xFF333333),
                                fontSize: 14,
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xFFF88125),
                            ),
                            onPressed: () {
                              if (controller.controllerNameGroupChat.text.trim() == "") {
                                controller.isShowErrorNameGroup = true;
                                controller.update();
                              } else {
                                createNewGroupChat(controller.controllerNameGroupChat.text.trim(), "GROUP", controller.listId);
                              }
                            },
                            icon: const Icon(Icons.check),
                            label: const Text('Xác nhận'),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    },);
  }

}
