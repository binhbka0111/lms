import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/message/send_new_message/send_new_message_controller.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';
import '../../home/home_controller.dart';
class SendNewMessagePage extends GetView<SendNewMessageController>{
  @override
  final controller = Get.put(SendNewMessageController());

  SendNewMessagePage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        leading: const BackButton(
          color: Colors.white,
        ),
        title: const Text("Gửi tin nhắn mới",style: TextStyle(color: Colors.white,fontSize: 16),),
        actions: [
          IconButton(
            icon: const Icon(Icons.home,color: Colors.white,),
            onPressed: () {
              comeToHome();
            },
          )
        ],
      ),
      body: Obx(() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
                color: const Color.fromRGBO(246, 246, 246, 1),
                borderRadius: BorderRadius.circular(6)
            ),
            margin: const EdgeInsets.fromLTRB(16, 8, 16, 8),
            child: TextFormField(
              onTap: () {

              },
              onChanged: (text) {
                if(text.trim()  == ""){
                  controller.listUserSearch.value = [];
                }else{
                  controller.getListUserSearch(0, 20, text.trim());
                }
              },
              controller: controller.textEditingController,
              decoration: InputDecoration(
                prefixIcon: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: const <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Text('Đến: ',style: TextStyle(color: Color.fromRGBO(26, 26, 26, 1),fontSize: 16),),
                    )
                  ],),
                border: InputBorder.none,
              ),

              textAlignVertical: TextAlignVertical.center,
              cursorColor: const Color.fromRGBO(248, 129, 37, 1),
            ),
          ),
          Visibility(
              visible: controller.listUserSearch.isNotEmpty,
              child: Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    controller: controller.controllerLoadMoreSearch,
                    itemCount: controller.listUserSearch.length,
                    padding: EdgeInsets.zero,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> clickItemUserSearch(index),StringConstant.FEATURE_MESSAGE_DETAIL);
                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 16.w),
                          child: Column(
                            children: [Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  width: 36.w,
                                  height: 36.w,
                                  child:
                                  CacheNetWorkCustom(urlImage: '${controller.listUserSearch[index].image}',)

                                ),
                                Padding(padding: EdgeInsets.only(left: 8.w)),
                                Text(
                                  "${controller.listUserSearch[index].fullName}",
                                  style: TextStyle(
                                      fontSize: 16.sp,
                                      color: const Color.fromRGBO(0, 0, 0, 1)),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),const Divider()],
                          ),
                        ),
                      );
                    }),
              )),
          Visibility(
            visible: controller.textEditingController.text=="",
            child: Container(
              margin: const EdgeInsets.fromLTRB(16, 16, 16, 8),
              child: InkWell(
                onTap: () {
                  Get.toNamed(Routes.createNewGroupChatPage);
                },
                child: Row(
                  children: [
                    const Icon(Icons.groups_outlined),
                    const Padding(padding: EdgeInsets.only(left: 16)),
                    const Text("Tạo nhóm mới",style: TextStyle(color: Color.fromRGBO(26, 26, 26, 1),fontSize: 16)),
                    Expanded(child: Container()),
                    const Icon(Icons.arrow_forward_ios,color: Color.fromRGBO(248, 129, 37, 1),),
                    const Padding(padding: EdgeInsets.only(right: 16)),
                  ],
                ),
              ),
            ),),
          Visibility(
            visible: controller.textEditingController.text=="",
            child: Container(
              margin: const EdgeInsets.only(top: 20, left: 16),
              child: const Text(
                "Gợi ý",
                style: TextStyle(
                    fontSize: 12,
                    color: Color.fromRGBO(177, 177, 177, 1)),
              ),
            ),),
          Visibility(
              visible: controller.textEditingController.text == "",
              child: Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: controller.listUserMessageSuggest.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      controller.getIdPrivateChat(controller.listUserMessageSuggest[index].id);
                    },
                    child: Container(
                      margin: const EdgeInsets.all(16),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 36.w,
                            height: 36.w,
                            child:
                            CacheNetWorkCustom(urlImage: '${controller.listUserMessageSuggest[index].image}',)

                          ),
                          Padding(padding: EdgeInsets.only(left: 8.h)),
                          Text(
                            "${controller.listUserMessageSuggest[index].fullName}",
                            style: TextStyle(
                                fontSize: 16.sp,
                                color: const Color.fromRGBO(0, 0, 0, 1)),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  );
                }),
          )),

        ],
      )),
    );
  }


  clickItemUserSearch(index){
    controller.getIdPrivateChat(controller.listUserSearch[index].id);
    controller.addRecentSearch(controller.listUserSearch[index].id);
  }

}