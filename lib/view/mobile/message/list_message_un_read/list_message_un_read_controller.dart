import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import 'package:slova_lms/routes/app_pages.dart';
import '../../../../commom/utils/app_utils.dart';
import '../../../../commom/utils/check_user_group_permission.dart';
import '../../../../commom/utils/color_utils.dart';
import '../../../../commom/utils/file_device.dart';
import '../../../../commom/utils/textstyle.dart';
import '../../../../commom/widget/cache_network_custom.dart';
import '../../../../data/base_service/api_response.dart';
import '../../../../data/model/common/req_file.dart';
import '../../../../data/model/common/user_group_by_app.dart';
import '../../../../data/model/res/file/response_file.dart';
import '../../../../data/model/res/message/group_message.dart';
import '../../../../data/repository/chat/chat_repo.dart';
import '../../../../data/repository/file/file_repo.dart';
import 'dart:io';
import '../../home/home_controller.dart';


class ListMessageUnReadController extends GetxController{
  var focusedDay = DateTime.now().obs;
  final ChatRepo _chatRepo = ChatRepo();
  var dataGroupMessage = <DataGroupMessage>[].obs;
  var dataGroupMessageLoadMore = <DataGroupMessage>[].obs;
  var indexPage = 1.obs;
  var controllerLoadMore = ScrollController();
  var controllerNameGroupChat = TextEditingController();
  var focusNameGroupChat = FocusNode();
  var isShowErrorNameGroup = false;
  final FileRepo fileRepo = FileRepo();
  var files = <ReqFile>[].obs;
  var imageGroupChatUpload = <ResponseFileUpload>[].obs;
  var listCustomSlidableAction = <CustomSlidableAction>[];
  var listFeatureInMenu = <String>[];

  @override
  void onInit() {
    getListGroupChat("NOT_SEEN",0,20);
    controllerLoadMore = ScrollController()..addListener(_scrollListenerLoadMore);
    super.onInit();
  }


  @override
  dispose() {
    controllerLoadMore.dispose();
    super.dispose();
  }

  getListFeatureInMenu(index){
    listFeatureInMenu = [];
    List<UserGroupByApp> listPageMessage = Get.find<HomeController>().userGroupByApp.where((element) => element.page?.code  == StringConstant.PAGE_MESSAGE).toList();
    if(listPageMessage.isNotEmpty){
      if(dataGroupMessage[index].createdBy == AppCache().userId){
        if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_MESSAGE_USER_LIST)){
          listFeatureInMenu.add("Thành viên");
        }
        if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_MESSAGE_IMAGE_EDIT)){
          listFeatureInMenu.add("Thay đổi ảnh nhóm");
        }
        if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_MESSAGE_NAME_EDIT)){
          listFeatureInMenu.add("Thay đổi tên nhóm");
        }
        if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_MESSAGE_USER_ADD)){
          listFeatureInMenu.add("Thêm thành viên");
        }
      }else{
        if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_MESSAGE_USER_LIST)){
          listFeatureInMenu.add("Thành viên");
        }
      }
    }

    return listFeatureInMenu;
  }


  setVisibleDivider(List<String> list,index){
    if(list.length <= 1){
      return false;
    }else{
      if(index == 0){
        return true;
      }else if(index == list.length -1){
        return false;
      }else{
        return true;
      }
    }
  }
  seenMessage(index) async {
    await _chatRepo
        .seenMessage(dataGroupMessage[index].id)
        .then((value) {
      if (value.state == Status.SUCCESS) {
      }
    });
  }

  getListCustomSlidableAction(index){
    listCustomSlidableAction = [];
    var list =  getListFeatureInMenu(index);
    var customSlidableActionMenu = CustomSlidableAction(
        autoClose: true,
        flex: 1,
        onPressed: (context) => {

        },
        foregroundColor: Colors.white,
        child:InkWell(
          onTap: () {
            Get.bottomSheet(
                Wrap(
                  children: [
                    Column(
                      children: [
                        Container(
                          decoration: ShapeDecoration(
                            // color: const Color.fromRGBO(238, 236, 237, 1),
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)
                              )
                          ),
                          margin:  EdgeInsets.symmetric(horizontal: 8.w),
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(vertical: 8.h),
                                alignment: Alignment.center,
                                child:  Text('Tùy chọn', style:  TextStyle(color:  ColorUtils.colorGray, fontSize: 12.sp, fontWeight: FontWeight.w400),),
                              ),
                              const Divider(),
                              Container(
                                constraints: const BoxConstraints(
                                ),
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  padding: EdgeInsets.zero,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: list.length,
                                  itemBuilder: (context, indexMenu) {
                                    return Column(
                                      children: [
                                        InkWell(
                                          onTap: () async{
                                            Get.back();
                                            if(indexMenu == list.indexOf("Thành viên")){
                                              Get.toNamed(Routes.listUserInGroupChatPage,arguments: [dataGroupMessage[index].id,dataGroupMessage[index].createdBy]);
                                            }
                                            if(indexMenu == list.indexOf("Thay đổi ảnh nhóm")){
                                              Get.dialog(GetBuilder<ListMessageUnReadController>(builder: (controller) {
                                                return Center(
                                                  child: Container(
                                                    margin: EdgeInsets.symmetric(horizontal: 32.w),
                                                    child: Wrap(
                                                      children: [
                                                        Card(
                                                          shape: const RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.all(
                                                              Radius.circular(8.0),
                                                            ),
                                                          ),
                                                          child: Padding(
                                                            padding: EdgeInsets.symmetric(
                                                              horizontal: 16.w,
                                                              vertical: 16.h,
                                                            ),
                                                            child: Column(
                                                              mainAxisAlignment:
                                                              MainAxisAlignment.center,
                                                              children: [
                                                                Row(
                                                                  mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                                  children: [
                                                                    Text(
                                                                      'Đổi Ảnh Nhóm',
                                                                      style: TextStyleUtils
                                                                          .sizeText16Weight500()
                                                                          ?.copyWith(
                                                                          color: Colors.black),
                                                                    ),
                                                                    IconButton(
                                                                      padding: EdgeInsets.zero,
                                                                      constraints:
                                                                      const BoxConstraints(),
                                                                      splashColor:
                                                                      Colors.transparent,
                                                                      highlightColor:
                                                                      Colors.transparent,
                                                                      onPressed: () {
                                                                        Get.back();
                                                                        controller.imageGroupChatUpload.value = [];
                                                                        controller.files.value = [];
                                                                        controller.update();
                                                                      },
                                                                      icon: const Icon(
                                                                        Icons.close_rounded,
                                                                        size: 20.0,
                                                                        color: ColorUtils
                                                                            .colorGray,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                                SizedBox(
                                                                  height: 16.h,
                                                                ),
                                                                SizedBox(
                                                                    width: 48,
                                                                    height: 48.h,
                                                                    child:
                                                                    CacheNetWorkCustom(urlImage: '${ controller.dataGroupMessage[index].image }',)

                                                                ),
                                                                SizedBox(
                                                                  height: 16.h,
                                                                ),
                                                                InkWell(
                                                                  onTap: () {
                                                                    if(imageGroupChatUpload.isEmpty){
                                                                      pickerImage();
                                                                    }
                                                                  },
                                                                  child: Container(
                                                                    color: const Color.fromRGBO(246, 246, 246, 1),
                                                                    child: DottedBorder(
                                                                      dashPattern: const [5, 5],
                                                                      radius:
                                                                      const Radius.circular(6),
                                                                      borderType: BorderType.RRect,
                                                                      color: const Color.fromRGBO(
                                                                          192, 192, 192, 1),
                                                                      padding: EdgeInsets.all(16.h),
                                                                      child: SizedBox(
                                                                        width: double.infinity,
                                                                        height: 80.h,
                                                                        child: controller.imageGroupChatUpload.isEmpty
                                                                            ?Column(
                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                          crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                          children: [
                                                                            Padding(
                                                                                padding:
                                                                                EdgeInsets.only(
                                                                                    top: 16.h)),
                                                                            Icon(
                                                                              Icons
                                                                                  .drive_folder_upload_rounded,
                                                                              color: Colors.grey,
                                                                              size: 40.w,
                                                                            ),
                                                                            Padding(
                                                                                padding:
                                                                                EdgeInsets.only(
                                                                                    top: 4.h)),
                                                                            Text(
                                                                              "Tải lên Ảnh",
                                                                              style: TextStyleUtils
                                                                                  .sizeText14Weight500()
                                                                                  ?.copyWith(
                                                                                  color: Colors
                                                                                      .grey),
                                                                            ),
                                                                          ],
                                                                        ):Center(
                                                                          child: buildAvatarSelect(),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                                SizedBox(
                                                                  height: 16.h,
                                                                ),
                                                                Row(
                                                                  mainAxisAlignment:
                                                                  MainAxisAlignment.end,
                                                                  children: [
                                                                    ElevatedButton.icon(
                                                                      onPressed: () {
                                                                        Get.back();
                                                                        controller.imageGroupChatUpload.value = [];
                                                                        controller.files.value = [];
                                                                        controller.update();
                                                                      },
                                                                      style: ElevatedButton
                                                                          .styleFrom(
                                                                          shape:
                                                                          RoundedRectangleBorder(
                                                                            borderRadius:
                                                                            BorderRadius
                                                                                .circular(
                                                                                4.0),
                                                                            side: BorderSide(
                                                                                color: Colors.grey,
                                                                                width:
                                                                                0.75.w),
                                                                          ),
                                                                          backgroundColor:
                                                                          Colors.white),
                                                                      icon: Icon(
                                                                        Icons.close_rounded,
                                                                        color: Colors.black,
                                                                        size: 20.w,
                                                                      ),
                                                                      label: Text(
                                                                        'Huỷ',
                                                                        style: TextStyleUtils
                                                                            .sizeText14Weight500()
                                                                            ?.copyWith(
                                                                            color: Colors
                                                                                .black),
                                                                      ),
                                                                    ),
                                                                    SizedBox(
                                                                      width: 10.w,
                                                                    ),
                                                                    ElevatedButton.icon(
                                                                      style: ElevatedButton
                                                                          .styleFrom(
                                                                        backgroundColor: ColorUtils.PRIMARY_COLOR,
                                                                      ),
                                                                      onPressed: () {
                                                                        controller.updateImageGroupChat(controller.dataGroupMessage[index].id);
                                                                      },
                                                                      icon: Icon(
                                                                        Icons.check,
                                                                        size: 20.w,
                                                                      ),
                                                                      label: Text(
                                                                        'Xác nhận',
                                                                        style: TextStyleUtils
                                                                            .sizeText14Weight500()
                                                                            ?.copyWith(
                                                                            color: Colors
                                                                                .white),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        )],
                                                    ),
                                                  ),
                                                );
                                              },),);
                                            }
                                            if(indexMenu == list.indexOf("Thay đổi tên nhóm")){
                                              Get.dialog(Center(
                                                child: Wrap(
                                                  children: [Container(
                                                    margin: EdgeInsets.symmetric(horizontal: 32.w),
                                                    child: Card(
                                                      shape: const RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.all(
                                                          Radius.circular(8),
                                                        ),
                                                      ),
                                                      child: Padding(
                                                        padding: EdgeInsets.symmetric(
                                                          horizontal: 16.w,
                                                          vertical: 16.h,
                                                        ),
                                                        child: Column(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: [
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                              children: [
                                                                const Text(
                                                                  'Đổi Tên Nhóm',
                                                                  style: TextStyle(
                                                                    fontWeight: FontWeight.w500,
                                                                    fontSize: 16,
                                                                  ),
                                                                ),
                                                                IconButton(
                                                                  padding: EdgeInsets.zero,
                                                                  constraints: const BoxConstraints(),
                                                                  splashColor: Colors.transparent,
                                                                  highlightColor: Colors.transparent,
                                                                  onPressed: () {
                                                                    Get.back();
                                                                    controllerNameGroupChat.text = "";
                                                                    update();
                                                                  },
                                                                  icon: const Icon(
                                                                    Icons.close_rounded,
                                                                    size: 20.0,
                                                                    color: Color(0xFF7D7E7E),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                            SizedBox(
                                                              height: 16.h,
                                                            ),
                                                            FocusScope(
                                                              node: FocusScopeNode(),
                                                              child: Focus(
                                                                  onFocusChange: (focus) {
                                                                    update();
                                                                  },
                                                                  child: Column(
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                                    children: [
                                                                      Container(
                                                                        alignment: Alignment.centerLeft,
                                                                        padding: const EdgeInsets.all(2.0),
                                                                        decoration: BoxDecoration(
                                                                            color: Colors.white,
                                                                            borderRadius:
                                                                            const BorderRadius.all(Radius.circular(6.0)),
                                                                            border: Border.all(
                                                                              width: 1,
                                                                              style: BorderStyle.solid,
                                                                              color: focusNameGroupChat.hasFocus
                                                                                  ? ColorUtils.PRIMARY_COLOR
                                                                                  : const Color.fromRGBO(192, 192, 192, 1),
                                                                            )),
                                                                        child: TextFormField(
                                                                          maxLines: null,
                                                                          cursorColor: ColorUtils.PRIMARY_COLOR,
                                                                          focusNode: focusNameGroupChat,
                                                                          onChanged: (value) {
                                                                            if(controllerNameGroupChat.text.trim() != ""){
                                                                              isShowErrorNameGroup = false;
                                                                            }
                                                                            update();
                                                                          },
                                                                          onFieldSubmitted: (value) {
                                                                            update();
                                                                          },
                                                                          controller: controllerNameGroupChat,
                                                                          decoration: InputDecoration(
                                                                            labelText: "Nhập tên nhóm",
                                                                            labelStyle: TextStyle(
                                                                                fontSize: 14.0.sp,
                                                                                color: const Color.fromRGBO(177, 177, 177, 1),
                                                                                fontWeight: FontWeight.w500,
                                                                                fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                                                                            contentPadding: const EdgeInsets.symmetric(
                                                                                vertical: 8, horizontal: 16),
                                                                            prefixIcon:  null,
                                                                            suffixIcon:  Visibility(
                                                                                visible:
                                                                                controllerNameGroupChat.text.isNotEmpty ==
                                                                                    true,
                                                                                child: InkWell(
                                                                                  onTap: () {
                                                                                    controllerNameGroupChat.text = "";
                                                                                  },
                                                                                  child: const Icon(
                                                                                    Icons.close_outlined,
                                                                                    color: Colors.black,
                                                                                    size: 20,
                                                                                  ),
                                                                                )),
                                                                            enabledBorder: InputBorder.none,
                                                                            errorBorder: InputBorder.none,
                                                                            border: InputBorder.none,
                                                                            errorStyle: const TextStyle(height: 0),
                                                                            focusedErrorBorder: InputBorder.none,
                                                                            disabledBorder: InputBorder.none,
                                                                            focusedBorder: InputBorder.none,
                                                                            floatingLabelBehavior: FloatingLabelBehavior.auto,

                                                                          ),
                                                                        ),
                                                                      ),
                                                                      Visibility(
                                                                        visible: isShowErrorNameGroup,
                                                                        child: Container(
                                                                            padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                                                            child: const Text(
                                                                              "Vui lòng nhập tên nhóm",
                                                                              style: TextStyle(color: Colors.red),
                                                                            )),
                                                                      )
                                                                    ],
                                                                  )),
                                                            ),
                                                            SizedBox(
                                                              height: 16.h,
                                                            ),
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.end,
                                                              children: [
                                                                ElevatedButton.icon(
                                                                  onPressed: () {
                                                                    Get.back();
                                                                    controllerNameGroupChat.text = "";
                                                                    update();
                                                                  },
                                                                  style: ElevatedButton.styleFrom(
                                                                      shape: RoundedRectangleBorder(
                                                                        borderRadius: BorderRadius.circular(4.0),
                                                                        side: const BorderSide(
                                                                            color: Color(0xFFC0C0C0), width: 0.75),
                                                                      ),
                                                                      backgroundColor: Colors.white),
                                                                  icon:
                                                                  const Icon(Icons.close_rounded, color: Colors.black),
                                                                  label: const Text(
                                                                    'Huỷ',
                                                                    style: TextStyle(
                                                                      color: Color(0xFF333333),
                                                                      fontSize: 14,
                                                                    ),
                                                                  ),
                                                                ),
                                                                const SizedBox(
                                                                  width: 10,
                                                                ),
                                                                ElevatedButton.icon(
                                                                  style: ElevatedButton.styleFrom(
                                                                    backgroundColor: const Color(0xFFF88125),
                                                                  ),
                                                                  onPressed: () {
                                                                    if (controllerNameGroupChat.text.trim() == "") {
                                                                      isShowErrorNameGroup = true;
                                                                      update();
                                                                    } else {
                                                                      updateNameGroupChat(controllerNameGroupChat.text.trim(), dataGroupMessage[index].id);
                                                                    }
                                                                  },
                                                                  icon: const Icon(Icons.check),
                                                                  label: const Text('Xác nhận'),
                                                                ),
                                                              ],
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  )],
                                                ),
                                              ));
                                            }
                                            if(indexMenu == list.indexOf("Thêm thành viên")){
                                              Get.toNamed(Routes.addUserGroupChatPage,arguments: dataGroupMessage[index].id);
                                            }

                                          },
                                          child: Container(
                                              height: 32.h,
                                              alignment: Alignment.center,
                                              child: Text(list[indexMenu], style:  TextStyle(color:  ColorUtils.BG_STATUS_MATCH, fontSize: 14.sp, fontWeight: FontWeight.w400),)),
                                        ),
                                        Visibility(
                                          visible: setVisibleDivider(list,indexMenu),
                                          child: const Divider(),)
                                      ],
                                    );
                                  },),),
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Get.back();
                          },
                          child: Container(
                            decoration: const ShapeDecoration(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(8))
                                )),
                            margin: const EdgeInsets.symmetric(horizontal: 8,vertical: 8),
                            alignment: Alignment.center,
                            padding:  EdgeInsets.symmetric(vertical: 16.h),
                            width: double.infinity,
                            child: Text('Hủy', style:  TextStyle(color:  ColorUtils.BG_STATUS_MATCH, fontSize: 14.sp, fontWeight: FontWeight.w500),),
                          ),
                        ),
                      ],
                    ),],
                )
            );
          },
          child: SizedBox(
            height: 40.h,
            width: 40,
            child: Container(
              decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color.fromRGBO(235, 235, 235, 1)
              ),
              child: const Icon(Icons.menu,color: Colors.black),
            ),
          ),
        ));
    var customSlidableActionNotify = CustomSlidableAction(
        autoClose: true,
        flex: 1,
        onPressed: (context) => {

        },
        foregroundColor: Colors.white,
        child: InkWell(
          onTap: () {
            updateNotifyGroupChat(dataGroupMessage[index].id, index);
          },
          child: SizedBox(
            height: 40.h,
            width: 40,
            child: Container(
              decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color.fromRGBO(235, 235, 235, 1)
              ),
              child: Icon(dataGroupMessage[index].isNotify == "FALSE"?Icons.notifications:Icons.notifications_off_outlined,color: Colors.black,),
            ),
          ),
        ));
    var customSlidableActionDelete =  CustomSlidableAction(
        autoClose: true,
        flex: 1,
        onPressed: (context) => {

        },
        foregroundColor: Colors.white,
        child: InkWell(
          onTap: () {
            Get.dialog(Center(
              child: Wrap(
                  children: [
                    Container(
                      width: double
                          .infinity,
                      decoration: BoxDecoration(
                          color: Colors
                              .transparent,
                          borderRadius:
                          BorderRadius.circular(16)),
                      child:
                      Stack(
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                                top: 40.h,
                                right: 50.w,
                                left: 50.w),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(16)),
                            child:
                            Column(
                              children: [
                                Padding(padding: EdgeInsets.only(top: 40.h)),
                                Center(
                                  child: Container(
                                    alignment: Alignment.center,
                                    margin: EdgeInsets.symmetric(horizontal: 16.w),
                                    child: Text(
                                      "Bạn có chắc muốn xoá cuộc trò chuyện này?",
                                      style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontSize: 14.sp),
                                    ),
                                  ),
                                ),
                                Container(
                                    color: Colors.white,
                                    padding: EdgeInsets.all(16.h),
                                    child: SizedBox(
                                      width: double.infinity,
                                      height: 46,
                                      child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                                          onPressed: () {
                                            deleteGroupChat(dataGroupMessage[index].id);
                                          },
                                          child: Text(
                                            'Xóa',
                                            style: TextStyle(color: Colors.white, fontSize: 16.sp),
                                          )),
                                    )),
                                TextButton(
                                    onPressed: () {
                                      Get.back();
                                    },
                                    child: Text(
                                      "Hủy",
                                      style: TextStyle(color: Colors.red, fontSize: 16.sp),
                                    ))
                              ],
                            ),
                          ),
                          Container(
                              alignment:
                              Alignment.center,
                              margin: const EdgeInsets.only(top: 10),
                              height: 80,
                              child: Image.asset("assets/images/image_app_logo.png"))
                        ],
                      ),
                    ),
                  ]),
            ));
          },
          child: SizedBox(
            height: 40.h,
            width: 40,
            child: Container(
              decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color.fromRGBO(255, 69, 89, 1)
              ),
              child: const Icon(Icons.delete,color: Colors.white,),
            ),
          )),
        );

    List<UserGroupByApp> listPageMessage = Get.find<HomeController>().userGroupByApp.where((element) => element.page?.code  == StringConstant.PAGE_MESSAGE).toList();
    if(listPageMessage.isNotEmpty){
      var listMenu = listPageMessage[0].features?.where((element) =>
      element == StringConstant.FEATURE_MESSAGE_USER_LIST
          ||element == StringConstant.FEATURE_MESSAGE_IMAGE_EDIT
          ||element == StringConstant.FEATURE_MESSAGE_NAME_EDIT
          ||element == StringConstant.FEATURE_MESSAGE_USER_ADD ).toList();
      var listDelete =listPageMessage[0].features?.where((element) => element == StringConstant.FEATURE_MESSAGE_DELETE).toList();
      var listNotify =listPageMessage[0].features?.where((element) => element == StringConstant.FEATURE_MESSAGE_NOTIFY).toList();
      if(dataGroupMessage[index].type =="GROUP"){
        if(listMenu!.isNotEmpty){
          listCustomSlidableAction.add(customSlidableActionMenu);
        }
        if(listNotify!.isNotEmpty){
          listCustomSlidableAction.add(customSlidableActionNotify);
        }
        if(listDelete!.isNotEmpty){
          listCustomSlidableAction.add(customSlidableActionDelete);
        }
      }else{
        if(listNotify!.isNotEmpty){
          listCustomSlidableAction.add(customSlidableActionNotify);
        }
        if(listDelete!.isNotEmpty){
          listCustomSlidableAction.add(customSlidableActionDelete);
        }
      }
    }
    return listCustomSlidableAction;
  }




  void _scrollListenerLoadMore() {
    if (controllerLoadMore.position.pixels == controllerLoadMore.position.maxScrollExtent) {
      if(dataGroupMessageLoadMore.isNotEmpty){
        indexPage.value++;
      }
      _chatRepo.getListGroupChat("NOT_SEEN",indexPage.value,20).then((value) {
        if (value.state == Status.SUCCESS) {
          dataGroupMessageLoadMore.value = value.object!.items!.data!;
          dataGroupMessage.addAll(dataGroupMessageLoadMore);
          dataGroupMessage.refresh();
        }
      });
    }

  }


  getListGroupChat(type,page,size) {
    _chatRepo.getListGroupChat(type,page,size).then((value) {
      if (value.state == Status.SUCCESS) {
        dataGroupMessage.value = value.object!.items!.data!;
        dataGroupMessage.refresh();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lỗi",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }
  deleteGroupChat(chatRoomId) async {
    await _chatRepo.deleteGroupChat( chatRoomId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Xóa nhóm chat thành công");
        Get.back();
        onInit();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(
            value.message ?? "Xóa nhóm chat thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

  uploadFile(file) async {
    showDialogUploadFile();
    var fileList = <File>[];
    for (var element in file) {
      fileList.add(element.file!);
    }
  await  fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;
        files.clear();
        files.addAll(file);
        files.refresh();
        imageGroupChatUpload.clear();
        imageGroupChatUpload.addAll(listResFile);
        imageGroupChatUpload.refresh();
        listResFile.clear();
        update();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại",
            backgroundColor: ColorUtils.COLOR_GREEN_BOLD);
      }
    });
    Get.back();
  }

  updateNameGroupChat(name, chatRoomId) async {
    await _chatRepo.updateNameGroupChat(name, chatRoomId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật tên nhóm chat thành công");
        Get.back();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(
            value.message ?? "Cập nhật tên nhóm chat thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

  updateImageGroupChat(chatRoomId) async {
    await _chatRepo
        .updateImageGroupChat(imageGroupChatUpload[0], chatRoomId)
        .then((value) {
      Get.back();
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật ảnh nhóm chat thành công");
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(
            value.message ?? "Cập nhật ảnh nhóm chat thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


  updateNotifyGroupChat(chatRoomId,index) async {
    await _chatRepo.updateNotifyGroupChat(chatRoomId).then((value) {
      if (value.state == Status.SUCCESS) {
        if (dataGroupMessage[index].isNotify == "TRUE") {
          AppUtils.shared.showToast("Tắt thông báo nhóm chat thành công");
        } else {
          AppUtils.shared.showToast("Bật thông báo nhóm chat thành công");
        }
      } else {
        AppUtils.shared.hideLoading();
        if (dataGroupMessage[index].isNotify == "FALSE") {
          AppUtils.shared.showToast(
              value.message ?? "Tắt thông báo nhóm chat thất bại",
              backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        } else {
          AppUtils.shared.showToast(
              value.message ?? "Bật thông báo nhóm chat thất bại",
              backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        }
      }
    });
  }

  buildAvatarSelect() {
    var file = files[0].file;
    return Container(
      height: 80,
      margin: EdgeInsets.only(top: 10.h),
      child: Stack(
        children: [
          InkWell(
            onTap: (){

            },
            child:    SizedBox(
              width: 80,
              height: 80.h,
              child: CircleAvatar(
                backgroundImage: Image.file(
                  file!,
                ).image,
              ),
            ),
          ),
          Positioned(
              right: 0,
              bottom: 0,
              child: InkWell(
                onTap: () {
                  pickerImage();
                },
                child: Image.asset(
                  'assets/images/img_cam.png',width: 24,height: 24,),
              ))
        ],
      ),
    );
  }


  Future<void> pickerImage() async {
    var file = await FileDevice.showSelectFileV2(Get.context! ,image: true);
    if (file.isNotEmpty) {
      uploadFile(file);
    }
  }

}