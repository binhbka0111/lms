import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import '../../../../commom/utils/color_utils.dart';
import '../../../../commom/utils/date_time_utils.dart';
import '../../../../routes/app_pages.dart';
import '../message_controller.dart';
import 'list_message_un_read_controller.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';

class ListMessageUnRead extends GetView<ListMessageUnReadController> {
  ListMessageUnRead({super.key});
  @override
  final controller = Get.put(ListMessageUnReadController());
  @override
  Widget build(BuildContext context) {
    return Obx(() => controller.dataGroupMessage.isNotEmpty?ListView.builder(
        shrinkWrap: true,
        controller: controller.controllerLoadMore,
        itemCount: controller.dataGroupMessage.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> goToMessageContent(index),StringConstant.FEATURE_MESSAGE_DETAIL);
            },
            child: Slidable(
                closeOnScroll: true,
                endActionPane: ActionPane(
                    motion: const ScrollMotion(),
                    extentRatio: 0.5,
                    children: controller.getListCustomSlidableAction(index)
                ),
                child: Container(
                  margin: const EdgeInsets.all(16),
                  child: Row(
                    children: [
                      SizedBox(
                          width: 36.h,
                          height: 36.h,
                          child:
                          CacheNetWorkCustom(urlImage: '${  controller.dataGroupMessage[index].image}',)

                      ),
                      Padding(padding: EdgeInsets.only(left: 16.w)),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            child: Text(
                              "${controller.dataGroupMessage[index].name}",
                              style: TextStyle(
                                  fontSize: 16.sp,
                                  color: const Color.fromRGBO(0, 0, 0, 1)),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Visibility(
                              visible: controller.dataGroupMessage[index].message != null,
                              child: Row(
                                children: [
                                  Container(
                                    constraints: BoxConstraints(
                                      maxWidth: 120.w,
                                    ),
                                    child: Text(
                                      controller.dataGroupMessage[index].message?.content == ""?"Tệp đính kèm":"${controller.dataGroupMessage[index].message?.content}",
                                      style: TextStyle(
                                        color:
                                        controller.dataGroupMessage[index].countNotSeen! > 0
                                            ? const Color.fromRGBO(26, 26, 26, 1)
                                            : const Color.fromRGBO(133, 133, 133, 1),
                                        fontSize: 14.sp,
                                      ),
                                      overflow: TextOverflow.ellipsis,),
                                  ),
                                  Text(
                                    "    • ${DateTimeUtils.convertToAgo(DateTime.fromMillisecondsSinceEpoch(controller.dataGroupMessage[index].message?.createdAt ??
                                        0)
                                        .toLocal())}",
                                    textAlign:
                                    TextAlign
                                        .left,
                                    style: TextStyle(
                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                        fontSize:
                                        14.sp),
                                  ),
                                ],
                              )),
                        ],
                      ),
                      const Spacer(),
                      Visibility(
                          visible: controller.dataGroupMessage[index].isNotify == "TRUE",
                          child: const Icon(Icons.notifications_none_outlined,color: ColorUtils.PRIMARY_COLOR,size: 18,)),
                      Visibility(
                          visible: controller.dataGroupMessage[index].isNotify == "FALSE",
                          child: const Icon(Icons.notifications_off_outlined,color: Color.fromRGBO(133, 133, 133, 1),size: 18,)),
                      SizedBox(
                        width: 8.w,
                      ),
                      Icon(
                        Icons.circle,
                        color: controller.dataGroupMessage[index].countNotSeen! > 0
                            ? ColorUtils.PRIMARY_COLOR
                            : Colors.white,
                        size: 14,
                      )
                    ],
                  ),
                )),
          );
        }): Center(
      child: Text("Không có tin nhắn chưa đọc",style: TextStyle(fontSize: 16.sp)),
    ));
  }

  void goToMessageContent(index){
    Get.toNamed(Routes.messageContent,arguments: controller.dataGroupMessage[index].id);
    controller.seenMessage(index);
    Get.find<MessageController>().socketIo.seenMessage(controller.dataGroupMessage[index].id);
    Get.find<ListMessageUnReadController>().onInit();
  }
}