import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/open_url.dart';
import '../../../../commom/app_cache.dart';
import '../../../../commom/constants/string_constant.dart';
import '../../../../commom/utils/app_utils.dart';
import '../../../../commom/utils/color_utils.dart';
import '../../../../commom/widget/dialog_upload_file.dart';
import '../../../../data/base_service/api_response.dart';
import '../../../../data/model/common/req_file.dart';
import '../../../../data/model/common/user_group_by_app.dart';
import '../../../../data/model/res/file/response_file.dart';
import '../../../../data/model/res/message/detail_group_chat.dart';
import '../../../../data/model/res/message/group_message.dart';
import '../../../../data/repository/chat/chat_repo.dart';
import '../../../../data/repository/file/file_repo.dart';
import '../../home/home_controller.dart';
import '../list_all_message/list_all_message_controller.dart';
import '../message_controller.dart';

class MessageContentController extends GetxController {
  var textEditingController = TextEditingController();
  var isShowSticker = false;
  var hasFocus = false;
  final imgPicker = ImagePicker();
  File? imageSelect;
  FocusNode focusNode = FocusNode();
  var showCursor = true;
  var tmpChatRoomId = "";
  final ChatRepo _chatRepo = ChatRepo();
  var controllerNameGroupChat = TextEditingController();
  var focusNameGroupChat = FocusNode();
  var isShowErrorNameGroup = false;
  var detailGroupChat = DetailGroupChat();
  var isReady = false;
  final FileRepo fileRepo = FileRepo();
  var files = <ReqFile>[];
  var imageGroupChatUpload = <ResponseFileUpload>[];
  var fileUploadSendMessage = <ResponseFileUpload>[];
  var messageList = <Message>[];
  var messageList2 = <Message2>[];
  var messageListAbove = <Message>[];
  var messageListBottom = <Message>[];
  AutoScrollController scrollListMessage = AutoScrollController();
  var outputDateFormat = DateFormat('hh:mm A');
  var outputDateFormat2 = DateFormat('dd/MM/yyyy HH:mm');
  List<List<Message>> listParagraphChat = <List<Message>>[];
  var indexPageBottom = (-1);
  var indexPageAbove = 1;
  var isShowClickEnd = false;
  final countShowTime = 30;
  final double runSpacing = 4;
  final double spacing = 4;
  final columns = 4;
  var pageSize = 30;
  late final BuildContext contextController;

  var listItemPopupMenu = <PopupMenuItem>[];

  var heightAddMember = 30.0;
  var heightViewMember = 30.0;
  var heightChangeAvatarGroup = 30.0;
  var heightChangeNameGroup = 30.0;

  var showAddMember = true;
  var showViewMember = false;
  var showChangeAvatarGroup = false;
  var showChangeNameGroup = false;

  var indexAddMember = 0;
  var indexViewMember = 0;
  var indexChangeAvatarGroup = 0;
  var indexChangeNameGroup = 0;

  var nameUserReply = "";
  var contentReply = "";
  var isShowReply = false;
  var isShowImage = false;
  var imageReply = "";
  var messParentId = "";
  var listColor = <Color>[];
  var parentId = "";
  Timer? timer;
  int _start = 2;
  var isTop = false;
  var isLoadingMore = false;
  double lastOffset = 0.0;
  var indexPageCurrent = 0;
  bool isScrollEnabled = true;
  double scrollOffset = 0.0;

  @override
  void onInit() {


    scrollListMessage.addListener(() {
      if (isScrollEnabled) {
        scrollOffset = scrollListMessage.offset;
        if (scrollListMessage.offset <= 0) {
          isShowClickEnd = false;
          update();
        } else {
          isShowClickEnd = true;
        }
        if (scrollListMessage.position.extentBefore < 200) {
          isShowClickEnd = false;
        }
        if (scrollListMessage.position.atEdge) {
          if (scrollListMessage.position.pixels == 0) {
            if (indexPageBottom >= 0) {
              scrollListenerBottom();
            }
          }
        }
        if (scrollListMessage.position.pixels == scrollListMessage.position.maxScrollExtent) {
          isTop = true;
          scrollListenerAbove();
        } else {
          isTop = false;
        }
        update();
      }
    });
    super.onInit();
  }

  startTimer(index) {
    const oneSec = Duration(seconds: 1);
    timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          listColor[index] = Colors.white;
          update();
          timer.cancel();
          _start = 2;
        } else {
          _start--;
          listColor[index] = const Color.fromRGBO(254, 230, 211, 1);
          update();
        }
      },
    );
  }

  getAlignment(index) {
    if (messageList2[index].messParent != null) {
      return Alignment.centerLeft;
    } else {
      if (messageList2[index].content!.trim().length < 3) {
        return Alignment.center;
      } else {
        return Alignment.centerLeft;
      }
    }
  }

  getListPopupMenuItem() {
    listItemPopupMenu = [];
    var viewMember = PopupMenuItem<int>(
        value: 0,
        padding: EdgeInsets.zero,
        height: heightViewMember,
        child: showViewMember
            ? Container(
                padding: EdgeInsets.symmetric(vertical: 4.h),
                decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: Colors.black26))),
                child: Row(
                  children: [
                    Padding(padding: EdgeInsets.only(right: 8.w)),
                    const Icon(
                      Icons.groups_outlined,
                      color: Colors.black,
                    ),
                    Padding(padding: EdgeInsets.only(right: 8.w)),
                    const Text("Thành viên"),
                  ],
                ),
              )
            : Row(
                children: [
                  Padding(padding: EdgeInsets.only(right: 8.w)),
                  const Icon(
                    Icons.groups_outlined,
                    color: Colors.black,
                  ),
                  Padding(padding: EdgeInsets.only(right: 8.w)),
                  const Text("Thành viên"),
                ],
              ));
    var changeAvatarGroup = PopupMenuItem<int>(
      padding: EdgeInsets.zero,
      value: 1,
      height: heightChangeAvatarGroup,
      child: showChangeAvatarGroup
          ? Container(
              padding: EdgeInsets.symmetric(vertical: 4.h),
              decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: Colors.black26))),
              child: Row(
                children: [
                  Padding(padding: EdgeInsets.only(right: 8.w)),
                  const Icon(
                    Icons.image_outlined,
                    color: Colors.black,
                  ),
                  Padding(padding: EdgeInsets.only(right: 8.w)),
                  const Text("Thay đổi ảnh nhóm"),
                ],
              ),
            )
          : Row(
              children: [
                Padding(padding: EdgeInsets.only(right: 8.w)),
                const Icon(
                  Icons.image_outlined,
                  color: Colors.black,
                ),
                Padding(padding: EdgeInsets.only(right: 8.w)),
                const Text("Thay đổi ảnh nhóm"),
              ],
            ),
    );
    var changeNameGroup = PopupMenuItem<int>(
      value: 2,
      padding: EdgeInsets.zero,
      height: heightChangeNameGroup,
      child: showChangeNameGroup
          ? Container(
              padding: EdgeInsets.symmetric(vertical: 4.h),
              decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: Colors.black26))),
              child: Row(
                children: [
                  Padding(padding: EdgeInsets.only(right: 8.w)),
                  const Icon(
                    Icons.edit_square,
                    color: Colors.black,
                  ),
                  Padding(padding: EdgeInsets.only(right: 8.w)),
                  const Text("Đổi tên Nhóm"),
                ],
              ),
            )
          : Row(
              children: [
                Padding(padding: EdgeInsets.only(right: 8.w)),
                const Icon(
                  Icons.edit_square,
                  color: Colors.black,
                ),
                Padding(padding: EdgeInsets.only(right: 8.w)),
                const Text("Đổi tên Nhóm"),
              ],
            ),
    );
    var addMember = PopupMenuItem<int>(
      value: 3,
      padding: EdgeInsets.zero,
      height: heightAddMember,
      child: showAddMember
          ? Container(
              padding: EdgeInsets.symmetric(vertical: 4.h),
              decoration: const BoxDecoration(border: Border(bottom: BorderSide(color: Colors.black26))),
              child: Row(
                children: [
                  Padding(padding: EdgeInsets.only(right: 8.w)),
                  const Icon(
                    Icons.person_add_alt_1_outlined,
                    color: Colors.black,
                  ),
                  Padding(padding: EdgeInsets.only(right: 8.w)),
                  const Text("Thêm thành viên"),
                ],
              ),
            )
          : Row(
              children: [
                Padding(padding: EdgeInsets.only(right: 8.w)),
                const Icon(
                  Icons.person_add_alt_1_outlined,
                  color: Colors.black,
                ),
                Padding(padding: EdgeInsets.only(right: 8.w)),
                const Text("Thêm thành viên"),
              ],
            ),
    );
    var changeStatusNotify = PopupMenuItem<int>(
      value: 4,
      padding: EdgeInsets.zero,
      height: 30,
      child: Row(
        children: [
          Padding(padding: EdgeInsets.only(right: 8.w)),
          detailGroupChat.isNotify == "TRUE"
              ? const Icon(
                  Icons.notifications_off_outlined,
                  color: Colors.black,
                )
              : const Icon(
                  Icons.notifications_none_outlined,
                  color: Colors.black,
                ),
          Padding(padding: EdgeInsets.only(right: 8.w)),
          detailGroupChat.isNotify == "TRUE" ? const Text("Tắt thông báo") : const Text("Bật thông báo")
        ],
      ),
    );
    List<UserGroupByApp> listPageMessage = Get.find<HomeController>().userGroupByApp.where((element) => element.page?.code == StringConstant.PAGE_MESSAGE).toList();
    if (listPageMessage.isNotEmpty) {
      var listViewMember = listPageMessage[0].features?.where((element) => element == StringConstant.FEATURE_MESSAGE_USER_LIST).toList();
      var listChangeAvatarGroup = listPageMessage[0].features?.where((element) => element == StringConstant.FEATURE_MESSAGE_IMAGE_EDIT).toList();
      var listChangeNameGroup = listPageMessage[0].features?.where((element) => element == StringConstant.FEATURE_MESSAGE_NAME_EDIT).toList();
      var listAddMember = listPageMessage[0].features?.where((element) => element == StringConstant.FEATURE_MESSAGE_USER_ADD).toList();
      var listChangeStatusNotify = listPageMessage[0].features?.where((element) => element == StringConstant.FEATURE_MESSAGE_NOTIFY).toList();
      if (detailGroupChat.type == "GROUP") {
        if (detailGroupChat.createdBy?.id != AppCache().userId) {
          if (listViewMember!.isNotEmpty) {
            listItemPopupMenu.add(viewMember);
          }
          if (listChangeStatusNotify!.isNotEmpty) {
            listItemPopupMenu.add(changeStatusNotify);
          }
        } else {
          if (listViewMember!.isNotEmpty) {
            listItemPopupMenu.add(viewMember);
          }
          if (listChangeAvatarGroup!.isNotEmpty) {
            listItemPopupMenu.add(changeAvatarGroup);
          }
          if (listChangeNameGroup!.isNotEmpty) {
            listItemPopupMenu.add(changeNameGroup);
          }
          if (listAddMember!.isNotEmpty) {
            listItemPopupMenu.add(addMember);
          }
          if (listChangeStatusNotify!.isNotEmpty) {
            listItemPopupMenu.add(changeStatusNotify);
          }
        }
      } else {
        if (listChangeStatusNotify!.isNotEmpty) {
          listItemPopupMenu.add(changeStatusNotify);
        }
      }
    }

    indexViewMember = listItemPopupMenu.indexOf(viewMember);
    indexAddMember = listItemPopupMenu.indexOf(addMember);
    indexChangeAvatarGroup = listItemPopupMenu.indexOf(changeAvatarGroup);
    indexChangeNameGroup = listItemPopupMenu.indexOf(changeNameGroup);

    showViewMember = setVisiblePopupMenuItem(indexViewMember, listItemPopupMenu);
    showChangeAvatarGroup = setVisiblePopupMenuItem(indexChangeAvatarGroup, listItemPopupMenu);
    showChangeNameGroup = setVisiblePopupMenuItem(indexChangeNameGroup, listItemPopupMenu);
    showAddMember = setVisiblePopupMenuItem(indexAddMember, listItemPopupMenu);

    heightViewMember = setHeightPopupMenuItem(indexViewMember, listItemPopupMenu);
    heightChangeAvatarGroup = setHeightPopupMenuItem(indexChangeAvatarGroup, listItemPopupMenu);
    heightChangeNameGroup = setHeightPopupMenuItem(indexChangeNameGroup, listItemPopupMenu);
    heightAddMember = setHeightPopupMenuItem(indexAddMember, listItemPopupMenu);

    update();
    return listItemPopupMenu;
  }

  clickLink(myString) {
    if (Uri.tryParse(myString)!.hasAbsolutePath) {
      OpenUrl.openLaunch(myString);
    }
  }

  isLink(myString) {
    if (Uri.tryParse(myString) == null) {
      return false;
    } else {
      if (Uri.tryParse(myString)!.hasAbsolutePath) {
        return true;
      } else {
        return false;
      }
    }
  }

  scrollListenerAbove() async {
    if (messageListAbove.isNotEmpty) {
      indexPageAbove++;
    }
    await Future.delayed(const Duration(milliseconds: 300));
    await _chatRepo.getAllMessageGroupChat(tmpChatRoomId, indexPageAbove, pageSize).then((value) {
      if (value.state == Status.SUCCESS) {
        messageListAbove = [];
        messageListAbove = value.object!.items!;
        messageList = [...messageList, ...messageListAbove];
        getListParagraph();
      }
    });
    isTop = false;
    update();
  }

  scrollListenerBottom() async {
    isLoadingMore = true;
    isScrollEnabled = false;

    if (indexPageBottom >= 0) {
      await _chatRepo.getAllMessageGroupChat(tmpChatRoomId, indexPageBottom, pageSize).then((value) async {
        if (value.state == Status.SUCCESS) {
          indexPageBottom--;
          messageListBottom = [];
          messageListBottom = value.object!.items!;
          messageListBottom = messageListBottom.reversed.toList();
          scrollListMessage.scrollToIndex(pageSize);
          for (int i = 0; i < messageListBottom.length; i++) {
            messageList.insert(0, messageListBottom[i]);
          }
          getListParagraph();
          scrollListMessage.jumpTo(scrollOffset);
          Future.delayed(const Duration(milliseconds: 500), () {
            isScrollEnabled = true;
          });
        }
      });
    }
    isLoadingMore = false;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      update();
    });
  }

  scrollToItemReply(messageId) async {
    isScrollEnabled = false;
    messageList = [];
    await _chatRepo.findMessageReply(tmpChatRoomId, pageSize, messageId).then((value) async {
      if (value.state == Status.SUCCESS) {
        messageList = value.object!.items!;
        indexPageAbove = value.object!.pageIndex! + 1;
        indexPageBottom = value.object!.pageIndex! - 1;
        indexPageCurrent = value.object!.pageIndex!;
        await getListParagraph();
        await checkMessageInList(messageId);
        parentId = messageId;
      }
    });
    update();
  }

  checkMessageInList(messageId) async {
    for (int i = 0; i < messageList2.length; i++) {
      if (messageList2[i].id == messageId) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          scrollListMessage.scrollToIndex(i, preferPosition: AutoScrollPosition.begin);
        });
        if (scrollListMessage.isAutoScrolling == false) {
          await startTimer(i);
        }
        if (scrollListMessage.isAutoScrolling == false) {
          isScrollEnabled = true;
        }
      }
    }
  }

  getDiffTime(i) {
    if (i > 0) {
      if (i == messageList2.length - 1) {
        return true;
      }
      if ((messageList2[i].createdAt! - messageList2[i + 1].createdAt!) > 1000 * 60 * 60 * 24) {
        return true;
      } else {
        if ((DateTime.now().millisecondsSinceEpoch - messageList2[i].createdAt!) > 1000 * 60 * countShowTime) {
          return true;
        } else {
          if (messageList2[i].files!.isNotEmpty) {
            return true;
          } else {
            return false;
          }
        }
      }
    } else {
      if (messageList2.length > 1) {
        if ((messageList2[i].createdAt! - messageList2[i + 1].createdAt!) > 1000 * 60 * 60 * 24) {
          return true;
        } else {
          if ((DateTime.now().millisecondsSinceEpoch - messageList2[i].createdAt!) > 1000 * 60 * countShowTime) {
            return true;
          } else {
            return false;
          }
        }
      } else {
        if ((DateTime.now().millisecondsSinceEpoch - messageList2[i].createdAt!) > 1000 * 60 * countShowTime) {
          return true;
        } else {
          return false;
        }
      }
    }
  }

  getBorerRadiusBottomRightSelf(Session session) {
    if (session == Session.end) {
      return 4.r;
    } else if (session == Session.oneItem) {
      return 12.r;
    } else if (session == Session.start) {
      return 12.r;
    } else {
      return 4.r;
    }
  }

  getBorerRadiusTopRightSelf(Session session) {
    if (session == Session.end) {
      return 12.r;
    } else if (session == Session.oneItem) {
      return 12.r;
    } else if (session == Session.start) {
      return 4.r;
    } else {
      return 4.r;
    }
  }

  getBorerRadiusBottomLeftGuess(Session session) {
    if (session == Session.end) {
      return 4.r;
    } else if (session == Session.oneItem) {
      return 12.r;
    } else if (session == Session.start) {
      return 12.r;
    } else {
      return 4.r;
    }
  }

  getBorerRadiusTopLeftGuess(Session session) {
    if (session == Session.end) {
      return 12.r;
    } else if (session == Session.oneItem) {
      return 12.r;
    } else if (session == Session.start) {
      return 4.r;
    } else {
      return 4.r;
    }
  }

  getListParagraph() {
    var count = 0;
    listParagraphChat = [];
    messageList2.clear();
    listColor = [];
    for (int i = 0; i < messageList.length; i++) {
      if (i > 0) {
        if (messageList[i].sender?.id == messageList[i - 1].sender?.id) {
          if (messageList[i].type == "NOTIFY" || messageList[i - 1].type == "NOTIFY") {
            var listUserChat = <Message>[];
            listParagraphChat.add(listUserChat);
            count++;
            listParagraphChat[count].add(messageList[i]);
          } else {
            if ((messageList[i - 1].createdAt! - messageList[i].createdAt!) > 1000 * 60 * 60 * 24) {
              var listUserChat = <Message>[];
              listParagraphChat.add(listUserChat);
              count++;
              listParagraphChat[count].add(messageList[i]);
            } else {
              if (messageList[i].files!.isEmpty && messageList[i - 1].files!.isEmpty) {
                listParagraphChat[count].add(messageList[i]);
              } else {
                var listUserChat = <Message>[];
                listParagraphChat.add(listUserChat);
                count++;
                listParagraphChat[count].add(messageList[i]);
              }
            }
          }
        } else {
          var listUserChat = <Message>[];
          listParagraphChat.add(listUserChat);
          count++;
          listParagraphChat[count].add(messageList[i]);
        }
      } else {
        var listUserChat = <Message>[];
        listParagraphChat.add(listUserChat);
        listParagraphChat[count].add(messageList[i]);
      }
    }
    for (int i = 0; i < listParagraphChat.length; i++) {
      for (int j = 0; j < listParagraphChat[i].length; j++) {
        if (listParagraphChat[i].length == 1) {
          messageList2.add(Message2(
              id: listParagraphChat[i][j].id,
              chatRoomId: listParagraphChat[i][j].chatRoomId,
              sender: listParagraphChat[i][j].sender,
              files: listParagraphChat[i][j].files ?? [],
              content: listParagraphChat[i][j].content,
              messParent: listParagraphChat[i][j].messParent,
              createdAt: listParagraphChat[i][j].createdAt,
              type: listParagraphChat[i][j].type,
              notifyType: listParagraphChat[i][j].notifyType,
              user: listParagraphChat[i][j].user,
              position: Session.oneItem));
          isScrollEnabled = false;
          update();
        } else {
          if (j == 0 && j != listParagraphChat[i].length - 1) {
            messageList2.add(Message2(
                id: listParagraphChat[i][j].id,
                chatRoomId: listParagraphChat[i][j].chatRoomId,
                sender: listParagraphChat[i][j].sender,
                files: listParagraphChat[i][j].files,
                content: listParagraphChat[i][j].content,
                messParent: listParagraphChat[i][j].messParent,
                createdAt: listParagraphChat[i][j].createdAt,
                type: listParagraphChat[i][j].type,
                notifyType: listParagraphChat[i][j].notifyType,
                user: listParagraphChat[i][j].user,
                position: Session.start));
            isScrollEnabled = false;
            update();
          } else if (j == listParagraphChat[i].length - 1) {
            messageList2.add(Message2(
              id: listParagraphChat[i][j].id,
              chatRoomId: listParagraphChat[i][j].chatRoomId,
              sender: listParagraphChat[i][j].sender,
              files: listParagraphChat[i][j].files,
              content: listParagraphChat[i][j].content,
              messParent: listParagraphChat[i][j].messParent,
              createdAt: listParagraphChat[i][j].createdAt,
              type: listParagraphChat[i][j].type,
              notifyType: listParagraphChat[i][j].notifyType,
              user: listParagraphChat[i][j].user,
              position: Session.end,
            ));
            isScrollEnabled = false;
            update();
          } else {
            messageList2.add(Message2(
                id: listParagraphChat[i][j].id,
                chatRoomId: listParagraphChat[i][j].chatRoomId,
                sender: listParagraphChat[i][j].sender,
                files: listParagraphChat[i][j].files,
                content: listParagraphChat[i][j].content,
                messParent: listParagraphChat[i][j].messParent,
                createdAt: listParagraphChat[i][j].createdAt,
                type: listParagraphChat[i][j].type,
                notifyType: listParagraphChat[i][j].notifyType,
                user: listParagraphChat[i][j].user,
                position: Session.center));
            isScrollEnabled = false;
            update();
          }
        }
      }
    }

    WidgetsBinding.instance.addPostFrameCallback((_) {
      isScrollEnabled = true;
      update();
    });
    for (int i = 0; i < messageList2.length; i++) {
      listColor.add(Colors.white);
    }
  }

  getDetailGroupChat(chatRoomId, page, size) async {
    await _chatRepo.detailGroupChat(chatRoomId, page, size).then((value) async {
      if (value.state == Status.SUCCESS) {
        detailGroupChat = value.object!;
        await getListPopupMenuItem();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lỗi", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
    update();
  }

  getAllMessageGroupChat(chatRoomId, page, size) async {
    messageList = [];
    indexPageBottom = -1;
    indexPageAbove = 1;
    await _chatRepo.getAllMessageGroupChat(chatRoomId, page, size).then((value) {
      if (value.state == Status.SUCCESS) {
        messageList = value.object!.items!;
        getListParagraph();
      }
    });
    isReady = true;
    update();
  }

  seenMessage() async {
    await _chatRepo.seenMessage(tmpChatRoomId).then((value) {
      if (value.state == Status.SUCCESS) {}
    });
  }

  setVisibleMessage(index) {
    if (messageList2[index].messParent != null) {
      return true;
    } else {
      if (messageList2[index].content?.trim() != "") {
        return true;
      } else {
        return false;
      }
    }
  }

  uploadFileImageGroup(file) {
    var fileList = <File>[];
    for (var element in file) {
      fileList.add(element.file!);
    }
    fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;
        files.clear();
        files.addAll(file);
        imageGroupChatUpload.clear();
        imageGroupChatUpload.addAll(listResFile);
        listResFile.clear();
        update();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_GREEN_BOLD);
      }
    });
  }

  uploadFileImageMessage(file) async {
    showDialogUploadFile();
    var fileList = <File>[];
    for (var element in file) {
      fileList.add(element.file!);
    }
    await fileRepo.uploadFileAvatar(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;
        fileUploadSendMessage.addAll(listResFile);
        listResFile.clear();
        update();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_GREEN_BOLD);
      }
    });
    Get.back();
  }

  updateNameGroupChat(name, chatRoomId) async {
    await _chatRepo.updateNameGroupChat(name, chatRoomId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật tên nhóm chat thành công");
        Get.back();
        getDetailGroupChat(chatRoomId, 0, 20);
        Get.find<MessageController>().getListGroupChat("ALL", 0, 20);
        Get.find<ListAllMessageController>().getListGroupChat("ALL", 0, 20);
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Cập nhật tên nhóm chat thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
    update();
  }

  updateImageGroupChat(chatRoomId) async {
    await _chatRepo.updateImageGroupChat(imageGroupChatUpload[0], chatRoomId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật ảnh nhóm chat thành công");
        imageGroupChatUpload = [];
        files = [];
        Get.back();
        getDetailGroupChat(chatRoomId, 0, 20);
        Get.find<MessageController>().getListGroupChat("ALL", 0, 20);
        Get.find<ListAllMessageController>().getListGroupChat("ALL", 0, 20);
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Cập nhật ảnh nhóm chat thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
    update();
  }

  updateNotifyGroupChat(chatRoomId) async {
    await _chatRepo.updateNotifyGroupChat(chatRoomId).then((value) {
      if (value.state == Status.SUCCESS) {
        if (detailGroupChat.isNotify == "TRUE") {
          AppUtils.shared.showToast("Tắt thông báo nhóm chat thành công");
        } else {
          AppUtils.shared.showToast("Bật thông báo nhóm chat thành công");
        }
        getDetailGroupChat(chatRoomId, 0, pageSize);
      } else {
        AppUtils.shared.hideLoading();
        if (detailGroupChat.isNotify == "TRUE") {
          AppUtils.shared.showToast(value.message ?? "Tắt thông báo nhóm chat thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        } else {
          AppUtils.shared.showToast(value.message ?? "Bật thông báo nhóm chat thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        }
      }
    });
    update();
  }

  getImage(list) {
    if (list.length == 0) {
      return "";
    } else {
      return list[0].link;
    }
  }

  getContent(list, content) {
    if (list.length == 0) {
      return content;
    } else {
      return "photo";
    }
  }

  @override
  void dispose() {
    scrollListMessage.dispose();
    super.dispose();
  }

  @override
  void onClose() {
    scrollListMessage.dispose();
    super.onClose();
  }

  getStartRichText(type) {
    switch (type) {
      case "CREATE_GROUP":
        return "đã thêm nhóm mới";
      case "CREATE_USER":
        return "đã thêm ";
      case "DELETE_USER":
        return "đã xoá ";
    }
  }

  getEndRichText(type) {
    switch (type) {
      case "CREATE_GROUP":
        return "";
      case "CREATE_USER":
        return " vào nhóm";
      case "DELETE_USER":
        return " khỏi nhóm";
    }
  }
}
