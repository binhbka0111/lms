import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/contacts.dart';
import '../../../../../commom/app_cache.dart';
import '../../../../../commom/utils/app_utils.dart';
import '../../../../../commom/utils/color_utils.dart';
import '../../../../../data/model/res/message/detail_group_chat.dart';
import '../../../../../data/repository/chat/chat_repo.dart';
import '../../../role/parent/parent_home_controller.dart';
import '../../message_controller.dart';
import '../list_user_in_group_chat/list_user_in_group_chat_controller.dart';



class AddUserGroupChatController extends GetxController{
  var selectedQuantity = 0.obs;
  var clickAll = <bool>[].obs;
  var listCheckBox = <bool>[].obs;
  var contacts = Contacts().obs;
  RxList<ItemContact> listAll= <ItemContact>[].obs;
  RxList<ItemContact> listAllLoadMore = <ItemContact>[].obs;
  var listId = <String>[];
  final ChatRepo _chatRepo = ChatRepo();
  var indexPage = 1.obs;
  var controllerLoadMore = ScrollController();
  var itemDetailGroupChat = DetailGroupChat().obs;
  var controllerFilter = TextEditingController();
  RxList<ItemContact> listUserAddToGroupChat = <ItemContact>[].obs;
  var chatRoomId = "";
  @override
  void onInit() {
    var argument = Get.arguments;
    if (argument != null) {
      chatRoomId = argument;
    }
    getListUserNotAdd(chatRoomId,0,20);
    controllerLoadMore = ScrollController()..addListener(_scrollListenerLoadMore);
    super.onInit();
  }


  @override
  dispose() {
    controllerLoadMore.dispose();
    super.dispose();
  }


  void _scrollListenerLoadMore() async{
    if (controllerLoadMore.position.pixels == controllerLoadMore.position.maxScrollExtent) {
      if(listAllLoadMore.isNotEmpty){
        indexPage.value++;
      }
      var userId = "";
      if(AppCache().userType == "PARENT"){
        userId = Get.find<ParentHomeController>().currentStudentProfile.value.id!;
      }else{
        userId = "";
      }
      await _chatRepo.getListUserNotAdd(chatRoomId,indexPage.value,20,controllerFilter.text.trim(),userId).then((value) {
        if (value.state == Status.SUCCESS) {
          listAllLoadMore.value = value.object!.items!;
          listAll.addAll(listAllLoadMore);
          listAll.refresh();
          listCheckBox.value = [];
          for (int i = 0; i < listAll.length; i++) {
            listCheckBox.add(false);
          }
        }
      });
    }

  }
  getListUserNotAdd(chatRoomId,page,size) async{
    var userId = "";
    if(AppCache().userType == "PARENT"){
      userId = Get.find<ParentHomeController>().currentStudentProfile.value.id!;
    }else{
      userId = "";
    }
    await _chatRepo.getListUserNotAdd(chatRoomId,page,size,controllerFilter.text.trim(),userId).then((value) {
      if (value.state == Status.SUCCESS) {
        listAll.value = [];
        listAll.value = value.object!.items!;
        listCheckBox.value = [];
        for (int i = 0; i < listAll.length; i++) {
          listCheckBox.add(false);
        }
        for (int i = 0; i < listAll.length; i++){
          if(listId.contains(listAll[i].id)){
            listCheckBox[i] = true;
          }
        }
      }
    });
  }


  addUserToGroupChat() async{
    await _chatRepo.addUserToGroupChat(chatRoomId,listId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Thêm thành viên vào nhóm chat thành công");
        Get.find<MessageController>().socketIo.addUserGroupChat(chatRoomId,listId);
        if(Get.isRegistered<ListUserInGroupChatController>()){
          Get.find<ListUserInGroupChatController>().getDetailGroupChat(chatRoomId, 0, 20);
        }
        Get.back();

      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Thêm thành viên vào nhóm chat thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

  checkBoxListView(index) {
    if (listCheckBox[index] == false) {
      listCheckBox[index] = true;
      selectedQuantity++;
      if(listId.contains(listAll[index].id!)== true){
      }else{
        listId.add(listAll[index].id!);
      }
      if(listId.contains(listAll[index].id!)== true){
        listUserAddToGroupChat.add(listAll[index]);
      }
    } else {
      listCheckBox[index] = false;
      selectedQuantity--;
      listId.remove(listAll[index].id!);
      listUserAddToGroupChat.removeWhere((element) => element.id == listAll[index].id!);
    }
  }


  getListRole(role,index) {
    switch (role) {
      case "PARENT":
        return listAll[index].student;
      case "STUDENT":
        return listAll[index].parent;
      case "TEACHER":
        return [];
      case "MANAGER":
        return [];
    }
  }

  getTextHomeRoomTeacher(isHomeRoom, index) {
    var split = "";
    switch (isHomeRoom) {
      case "":
        return "";
      case null:
        return "";
      case "Giáo viên chủ nhiệm":
        if (listAll[index].subject!.isEmpty) {
          split = "";
        } else {
          split = ",";
        }
        return "Giáo viên chủ nhiệm$split ";
      default:
        return "";
    }
  }


  getTitle(role) {
    switch (role) {
      case "PARENT":
        return "Phụ huynh hs: ";
      case "STUDENT":
        return "Phụ huynh: ";
      case "TEACHER":
        return "Giáo viên";
      case "MANAGER":
        return "Cán bộ quản lý";
      default:
        return "";
    }
  }





}