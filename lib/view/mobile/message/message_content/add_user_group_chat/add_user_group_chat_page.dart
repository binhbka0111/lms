import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';

import '../message_content_controller.dart';
import 'add_user_group_chat_controller.dart';


class AddUserGroupChatPage extends GetView<AddUserGroupChatController>{
  @override
  final controller = Get.put(AddUserGroupChatController());

  AddUserGroupChatPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
            leading: const BackButton(
              color: Colors.white,
            ),
            title: const Text("Thêm thành viên",style: TextStyle(color: Colors.white,fontSize: 16),),
          ),
          body: Obx(() =>  Column(
            children: [
              Card(
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6)),
                  color: const Color.fromRGBO(246, 246, 246, 1),
                  margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
                  child: TextFormField(
                    autofocus: true,
                    onTap: () {},
                    onChanged: (text) {
                      controller.getListUserNotAdd(Get.find<MessageContentController>().tmpChatRoomId,0,20);
                    },
                    controller: controller.controllerFilter,
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 7, horizontal: 16),
                      hintText: "Tìm tin nhắn...",
                      hintStyle: TextStyle(
                          fontSize: 14,
                          color: Color.fromRGBO(177, 177, 177, 1)),
                      prefixIcon: Icon(
                        Icons.search,
                        color: Color.fromRGBO(177, 177, 177, 1),
                      ),
                      border: InputBorder.none,
                    ),
                    textAlignVertical: TextAlignVertical.center,
                    cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                  )
              ),
              Visibility(
                  visible: controller.listUserAddToGroupChat.isNotEmpty,
                  child: Container(
                    margin: EdgeInsets.only(top: 8.h,right: 16.w,left: 16.w),
                    decoration: BoxDecoration(
                      border: Border.all(color: const Color.fromRGBO(239, 239, 239, 1),),
                    ),
                    height: 86.h,
                    child: ListView.builder(
                        itemCount: controller.listUserAddToGroupChat.length,
                        physics: const ScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context,index){
                          return Container(
                            margin: EdgeInsets.symmetric(horizontal: 8.w),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Stack(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.all(8.w),
                                      width: 32.h,
                                      height: 32.h,
                                      child:
                                      CacheNetWorkCustom(urlImage: '${controller.listUserAddToGroupChat[index].image}',)

                                    ),
                                    Positioned(
                                        top: 4,
                                        right: 0,
                                        child: InkWell(
                                          onTap: () {
                                            controller.listId.remove(controller.listUserAddToGroupChat[index].id);
                                            controller.listUserAddToGroupChat.removeAt(index);
                                            for(int i = 0;i<controller.listAll.length;i++){
                                              if(controller.listId.contains(controller.listAll[i].id)){
                                                controller.listCheckBox[i] = true;
                                              }else{
                                                controller.listCheckBox[i] = false;
                                              }
                                            }
                                          },
                                          child: Container(
                                            padding: EdgeInsets.zero,
                                            decoration: const BoxDecoration(
                                              color: Colors.white,
                                              shape: BoxShape.circle,
                                            ),
                                            child: SvgPicture.asset("assets/images/icon_dangerous.svg",fit: BoxFit.cover,height: 18.w,width: 18.w,),
                                          ),
                                        ))
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 8.h),
                                  child:  Text("${ controller.listUserAddToGroupChat[index].fullName}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.w400,fontSize: 14),),
                                )
                              ],
                            ),
                          );
                        }),
                  )),
              Expanded(child: Container(
                decoration: BoxDecoration(
                    color: Colors.white, borderRadius: BorderRadius.circular(6)),
                margin:  EdgeInsets.only( top:16.h,left: 16.w,right: 16.w),
                child: ListView.builder(
                    itemCount: controller.listAll.length,
                    shrinkWrap: true,
                    controller: controller.controllerLoadMore,
                    itemBuilder: (context, index) {
                      return Obx(() => InkWell(
                        onTap: () {
                          controller.checkBoxListView(index);
                        },
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Container(
                                    alignment: Alignment.center,
                                    decoration: ShapeDecoration(
                                      shape: CircleBorder(
                                          side: BorderSide(
                                              color:
                                              controller.listCheckBox[index] ?
                                              const Color.fromRGBO(
                                                  248, 129, 37, 1) : const Color.fromRGBO(
                                                  235, 235, 235, 1)
                                          )
                                      ),
                                    ),
                                    child:
                                    controller.listCheckBox[index]
                                        ?  Container(
                                        margin: const EdgeInsets.all(4),
                                        child: Icon(
                                          Icons.circle,
                                          size: ScreenUtil().setSp(16),
                                          color: ColorUtils.PRIMARY_COLOR,
                                        ))
                                        : Container(
                                        margin: const EdgeInsets.all(4),
                                        child: Icon(
                                          Icons.circle,
                                          size: ScreenUtil().setSp(16),
                                          color: Colors.transparent,
                                        ))
                                ),
                                Padding(padding: EdgeInsets.only(left: 8.w)),
                                SizedBox(
                                  width: 32.w,
                                  height: 32.w,
                                  child:
                                  CacheNetWorkCustom(urlImage: '${controller.listAll[index].image }',)

                                ),
                                Padding(padding: EdgeInsets.only(left: 8.w)),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${controller.listAll[index].fullName}",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                    ),
                                    SizedBox(
                                      width: 230.w,
                                      child: controller.listAll[index].type!="TEACHER"?getRole(
                                          controller.getListRole(controller.listAll[index].type,index),
                                          controller.listAll[index].type, index):getRoleTeacher(
                                          controller.getListRole(controller.listAll[index].type,index),
                                          controller.listAll[index].type, index),
                                    )
                                  ],
                                )
                              ],
                            ),
                            const Divider(),
                          ],
                        ),
                      ));
                    }),
              )),
              Container(
                color: const Color.fromRGBO(255, 255, 255, 1),
                height: 78,
                padding: const EdgeInsets.all(16),
                child: SizedBox(
                  width: double.infinity,
                  height: 46,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor:
                        controller.listId.isNotEmpty?const Color.fromRGBO(248, 129, 37, 1):const Color.fromRGBO(246, 246, 246, 1),
                      ),
                      onPressed: () {
                        controller.addUserToGroupChat();
                      },
                      child:  Text(
                        'Thêm',
                        style: TextStyle(
                            color:controller.listId.isNotEmpty? const Color.fromRGBO(255, 255, 255, 1):const Color.fromRGBO(177, 177, 177, 1),
                            fontSize: 16),
                      )),
                ),
              ),
            ],
          )),
        )
    );
  }
  RichText getRole(List<dynamic> list, role, index) {
    return RichText(
      textAlign: TextAlign.left,
      text: TextSpan(
        text: '${controller.getTitle(role)}',
        style: TextStyle(
            color: const Color.fromRGBO(133, 133, 133, 1),
            fontSize: 12.sp,
            fontWeight: FontWeight.w500,
            fontFamily: 'static/Inter-Medium.ttf'),

        children: list.map((e) {
          var index = list.indexOf(e);
          var showSplit = ", ";
          if (index == list.length - 1) {
            showSplit = "";
          }
          return TextSpan(
              text: "${e.fullName}$showSplit",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf'));
        }).toList(),
      ),
    );
  }
  RichText getRoleTeacher(List<dynamic> list, role, index) {
    return RichText(
      textAlign: TextAlign.left,
      text: TextSpan(
          text: '${controller.getTitle(role)}',
          style: TextStyle(
              color: const Color.fromRGBO(133, 133, 133, 1),
              fontSize: 12.sp,
              fontWeight: FontWeight.w500,
              fontFamily: 'static/Inter-Medium.ttf'),

          children: [
            TextSpan(
              text:
              "${controller.getTextHomeRoomTeacher(controller.listAll[index].positionName, index)}",
              style: TextStyle(
                  color: const Color.fromRGBO(26, 26, 26, 1),
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf'),
              children: controller.listAll[index].subject?.map((e) {
                var indexSubject = controller.listAll[index].subject?.indexOf(e);
                var showSplit = ", ";
                if (indexSubject == controller.listAll[index].subject!.length - 1) {
                  showSplit = "";
                }
                if (controller.listAll[index].type == "TEACHER") {
                  return TextSpan(
                      text: "Môn ${e.fullName}$showSplit",
                      style: TextStyle(
                          color: const Color.fromRGBO(26, 26, 26, 1),
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'static/Inter-Medium.ttf'));
                }

                return TextSpan(
                    text: "${e.fullName}$showSplit",
                    style: TextStyle(
                        color: const Color.fromRGBO(26, 26, 26, 1),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'static/Inter-Medium.ttf'));
              }).toList(),
            ),
          ]
      ),
    );
  }


}