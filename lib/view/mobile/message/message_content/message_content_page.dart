import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/routes/app_pages.dart';
import '../../../../commom/app_cache.dart';
import '../../../../commom/utils/file_device.dart';
import '../../../../commom/utils/global.dart';
import '../../../../commom/utils/open_url.dart';
import '../../../../commom/utils/textstyle.dart';
import '../../../../data/model/res/message/group_message.dart';
import '../list_all_message/list_all_message_controller.dart';
import '../list_message_un_read/list_message_un_read_controller.dart';
import '../message_controller.dart';
import 'message_content_controller.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:swipe_to/swipe_to.dart';

class MessageContent extends GetView<MessageContentController> {
  MessageContent({super.key});
  @override
  final controller = Get.put(MessageContentController());

  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return WillPopScope(child: GestureDetector(
      onTap: () {
        dismissKeyboard();
        controller.isShowSticker = false;
        controller.update();
      },
      child: ScreenUtilInit(
        useInheritedMediaQuery: true,
        builder: (BuildContext context, Widget? child) {
          return SafeArea(
              child: Scaffold(
                backgroundColor: Colors.white,
                resizeToAvoidBottomInset: true,
                body: GetBuilder<MessageContentController>(builder: (controller) {
                  return controller.isReady?Column(
                    children: [
                      Expanded(child:Column(
                        children: [
                          Container(
                            decoration: const BoxDecoration(
                              border: Border(
                                bottom: BorderSide(color: Colors.black26)
                              )
                            ),
                            child: Card(
                              margin: EdgeInsets.zero,
                              child: Container(
                                margin: const EdgeInsets.all(6),
                                child: Row(
                                  children: [
                                    const BackButton(),
                                    SizedBox(
                                        width: 40,
                                        height:40.h,
                                        child:
                                        CacheNetWorkCustom(urlImage: '${controller.detailGroupChat.image}',)
                                    ),
                                    Padding(padding: EdgeInsets.only(left: 8.w)),
                                    Text(
                                      "${controller.detailGroupChat.name?.trim()}",
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          color: const Color.fromRGBO(0, 0, 0, 1)),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    const Spacer(),
                                    popupMenuGroupChat(),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Visibility(
                              visible: controller.isTop,
                              child:  Container(
                                height: 16.h,
                                width: 16.h,
                                margin: EdgeInsets.symmetric(vertical: 8.h),
                                child: const Center(
                                  child: CircularProgressIndicator(
                                    color: ColorUtils.PRIMARY_COLOR,
                                  ),
                                ),
                              )),
                          Expanded(child: Stack(
                            alignment: Alignment.topCenter,
                            children: [
                              ListView.builder(
                                reverse:true,
                                shrinkWrap: true,
                                controller: controller.scrollListMessage,
                                itemCount: controller.messageList2.length,
                                itemBuilder: (context, index) {
                                  return Column(
                                    children: [
                                      showTimeMessage(controller, index),
                                      showTimeTypeNotify(index),
                                      showTextTypeNotify(controller, index),
                                      (controller.messageList2[index].sender?.id == AppCache().userId
                                          ? messageSelf(index, controller, context)
                                          : messageGuess(index, controller))
                                    ],
                                  );
                                },),
                              Visibility(
                                  visible: controller.isShowClickEnd,
                                  child: Positioned(
                                      bottom: 8.h,
                                      child: InkWell(
                                        onTap: () {
                                          controller.timer?.cancel();
                                          controller.getAllMessageGroupChat(controller.tmpChatRoomId, 0, controller.pageSize);
                                          controller.scrollListMessage.jumpTo(0.0);
                                          controller.isShowClickEnd = false;
                                          controller.update();
                                        },
                                        child: Container(
                                          width: 32,
                                          height: 32.h,
                                          decoration: const ShapeDecoration(
                                              shape: CircleBorder(
                                              ),
                                              color: ColorUtils.PRIMARY_COLOR
                                          ),
                                          child: const Icon(Icons.arrow_downward_outlined,color: Colors.white,),
                                        ),
                                      )))
                            ],
                          )),
                          Visibility(
                              visible: controller.isLoadingMore,
                              child:  Container(
                                height: 16.h,
                                width: 16.h,
                                margin: EdgeInsets.symmetric(vertical: 8.h),
                                child: const Center(
                                  child: CircularProgressIndicator(
                                    color: ColorUtils.PRIMARY_COLOR,
                                  ),
                                ),
                              )),
                        ],
                      )),
                      showReply(),
                      importContentMessage(),
                      showEmoji(),
                      showImageSendMessage(),
                    ],
                  )
                      :const Center(
                    child: CircularProgressIndicator(
                      color: ColorUtils.PRIMARY_COLOR,
                    ),
                  );
                },),
              ));
        },

      ),
    ), onWillPop: () async{
      if(Get.isRegistered<MessageController>()){
        Get.find<MessageController>().getListGroupChat("ALL", 0, 20);
      }
      if(Get.isRegistered<ListAllMessageController>()){
        Get.find<ListAllMessageController>().getListGroupChat("ALL", 0, 20);
      }
      if(Get.isRegistered<ListMessageUnReadController>()){
        Get.find<ListMessageUnReadController>().getListGroupChat("NOT_SEEN",0,20);
      }
      Get.back();
      return true;
    },);
  }

  showReply(){
    return Visibility(
        visible: controller.isShowReply,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 8.h),
          color: Colors.black12,
          child: IntrinsicHeight(
            child: Row(
              children: [
                const Icon(
                  Icons.reply_outlined,
                  color: ColorUtils.PRIMARY_COLOR,
                ),
                const VerticalDivider(
                  color: ColorUtils.PRIMARY_COLOR,
                  thickness: 2,
                ),
                Visibility(
                  visible: controller.isShowImage,
                  child: SizedBox(
                      width: 30.w,
                      height: 30.w,
                      child: CacheNetWorkCustomBanner(urlImage: controller.imageReply,)
                  ),),
                SizedBox(
                  width: 4.w,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Trả lời đến ${controller.nameUserReply}",style: const TextStyle(
                        color:  ColorUtils.PRIMARY_COLOR
                    ),),
                    Container(
                      constraints: BoxConstraints(
                          maxWidth: 250.w
                      ),
                      child: Text(controller.contentReply,overflow: TextOverflow.ellipsis),
                    ),
                  ],
                ),
                const Spacer(),
                InkWell(
                  onTap: (){
                    controller.isShowReply = false;
                    controller.update();
                  },
                  child: const Icon(Icons.close,color: ColorUtils.PRIMARY_COLOR,),
                )
              ],
            ),
          ),
        ));
  }

  importContentMessage(){
    return Container(
      margin: EdgeInsets.only(right: 16.w),
      child: Row(
        children: [
          Expanded(child: Container(
            margin: EdgeInsets.symmetric(horizontal: 16.w,vertical: 8.h),
            padding: EdgeInsets.only(left: 8.w),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              color: const Color.fromRGBO(246, 246, 246, 1),
            ),
            child: FocusScope(
              node: FocusScopeNode(),
              onFocusChange: (focus) {
                controller.hasFocus = focus;
                if (controller.hasFocus == true) {
                  controller.isShowSticker = false;
                }
                controller.update();
              },
              child: Focus(
                child: TextFormField(
                  onTap: () {
                    controller.isShowSticker = false;
                    controller.showCursor = true;
                    controller.update();
                  },
                  maxLines: null,
                  showCursor: controller.showCursor,
                  focusNode: controller.focusNode,
                  textAlignVertical: TextAlignVertical.center,
                  controller: controller.textEditingController,
                  onFieldSubmitted: (value) {

                  },
                  cursorColor: ColorUtils.PRIMARY_COLOR,
                  decoration: InputDecoration(
                      hintText: "Type your message here...",
                      hintStyle: TextStyle(
                          color: const Color.fromRGBO(177, 177, 177, 1),
                          fontSize: 12.sp),
                      border: InputBorder.none,
                      suffixIcon: SizedBox(
                        width: 80.w,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            InkWell(
                              onTap: () {
                                pickerImageSendMessage();
                                dismissKeyboard();
                              },
                              child: const Icon(Icons.image, color: ColorUtils.PRIMARY_COLOR,size: 25,),
                            ),
                            const SizedBox(width:8,),
                            InkWell(
                              onTap: () {
                                controller.isShowSticker =
                                !controller.isShowSticker;
                                SystemChannels.textInput
                                    .invokeMethod('TextInput.hide');
                                controller.showCursor = false;
                                controller.update();
                              },
                              child: const Icon(CupertinoIcons.smiley_fill, color: ColorUtils.PRIMARY_COLOR,size: 25,),
                            ),
                            Padding(padding: EdgeInsets.only(right: 8.w)),
                          ],
                        ),
                      )),
                ),
              ),
            ),
          ),),
          InkWell(
            onTap: () async{
              if(controller.isShowReply == true){
                sendMessage(controller.messParentId);
              }else{
                sendMessage("");
              }
              controller.update();
            },
            child: const Icon(Icons.send,color: ColorUtils.PRIMARY_COLOR,),
          )
        ],
      ),
    );
  }


  showEmoji(){
    return Offstage(
      offstage: !controller.isShowSticker,
      child: SizedBox(
          height: 250,
          child: EmojiPicker(
            textEditingController: controller.textEditingController,
            config: Config(
              columns: 7,
              emojiSizeMax: 32 * (foundation.defaultTargetPlatform == TargetPlatform.iOS ? 1.30 : 1.0),
              verticalSpacing: 0,
              horizontalSpacing: 0,
              gridPadding: EdgeInsets.zero,
              initCategory: Category.RECENT,
              bgColor: const Color(0xFFF2F2F2),
              indicatorColor: ColorUtils.PRIMARY_COLOR,
              iconColor: Colors.grey,
              iconColorSelected: Colors.blue,
              backspaceColor: Colors.blue,
              skinToneDialogBgColor: Colors.white,
              skinToneIndicatorColor: Colors.grey,
              enableSkinTones: true,
              recentsLimit: 28,
              replaceEmojiOnLimitExceed: false,
              noRecents: const Text(
                'No Recents',
                style: TextStyle(fontSize: 20, color: Colors.black26),
                textAlign: TextAlign.center,
              ),
              loadingIndicator: const SizedBox.shrink(),
              tabIndicatorAnimDuration: kTabScrollDuration,
              categoryIcons: const CategoryIcons(),
              buttonMode: ButtonMode.MATERIAL,
              checkPlatformCompatibility: true,
            ),
          )),
    );
  }

  showImageSendMessage(){
    return Offstage(
      offstage: controller.fileUploadSendMessage.isEmpty,
      child: SizedBox(
          height: 250,
          child: buildImageSendMessage()),
    );
  }

  AutoScrollTag messageGuess(int index, MessageContentController controller) {
    return AutoScrollTag(key: ValueKey(index),
      controller: controller.scrollListMessage,
      index: index,
      child: SwipeTo(
          onRightSwipe: () {
            controller.isShowReply = true;
            controller.nameUserReply =  controller.messageList2[index].sender!.fullName!;
            controller.messParentId =  controller.messageList2[index].id!;
            if( controller.messageList2[index].files!.isNotEmpty){
              controller.contentReply = "photo";
              controller.isShowImage = true;
              controller.imageReply=  controller.messageList2[index].files![0].link!;
            }else{
              controller.contentReply =  controller.messageList2[index].content!;
              controller.isShowImage = false;
            }
            controller.update();
            },
          child: Container(
            color: controller.listColor[index],
            margin: EdgeInsets.only(left: 8.w,top: 2.h),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                controller.messageList2[index].position == Session.start ||controller.messageList2[index].position == Session.oneItem
                    ?SizedBox(
                    width: 32.h,
                    height: 32.h,
                    child: CacheNetWorkCustom(urlImage: '${  controller.messageList2[index].sender?.image}',)
                ):
                SizedBox(
                  width: 32.h,
                  height: 32.h,),
                SizedBox(
                  width: 4.w,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        if( controller.messageList2[index].messParent != null){
                          controller.scrollToItemReply(controller.messageList2[index].messParent!.id);
                        }
                        },
                      child:controller.setVisibleMessage(index)? Container(
                        decoration:  BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(12.r),
                              topLeft: Radius.circular(controller.getBorerRadiusTopLeftGuess( controller.messageList2[index].position!)),
                              bottomRight: Radius.circular(12.r),
                              bottomLeft: Radius.circular(controller.getBorerRadiusBottomLeftGuess( controller.messageList2[index].position!))),
                          color: const Color.fromRGBO(246, 246, 246, 1),
                        ),
                        padding:  EdgeInsets.symmetric(horizontal: 12.w,vertical: 8.h),
                        child: Container(
                          constraints: BoxConstraints(
                            maxWidth: 220.w,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              showTextMessageReplyGuess(index),
                              InkWell(
                                  onTap: (){controller.clickLink( controller.messageList2[index].content?.trim());},
                                  child: Text(
                                    "${ controller.messageList2[index].content?.trim()}",
                                    style: TextStyle(
                                      color: controller.isLink( controller.messageList2[index].content?.trim()??"")
                                          ?ColorUtils.PRIMARY_COLOR:const Color.fromRGBO(26, 26, 26, 1),
                                      fontSize: 12.sp,
                                    ),

                                  )
                              ),
                              SizedBox(height: 4.h,),
                              showImageMessageReplyGuess(index),
                            ],
                          ),
                        ),
                      ):const SizedBox(),
                    ),
                    Visibility(
                        visible:  controller.messageList2[index].files!.isNotEmpty,
                        child: Padding(padding: EdgeInsets.only(bottom: 4.h))),
                    Container(
                      constraints: BoxConstraints(
                        maxWidth: 200.w
                      ),
                      child: showImageMessageGuess(index),
                    )
                  ],
                )
              ],
            ),)),);
  }

  showImageMessageGuess(index){
    return Visibility(
        visible:  controller.messageList2[index].messParent == null,
        child: Visibility(
            visible:  controller.messageList2[index].files!.isNotEmpty,
            child: Wrap(
              runSpacing: controller.runSpacing,
              spacing: controller.spacing,
              alignment: WrapAlignment.start,
              children: List.generate( controller.messageList2[index].files!.length, (indexFile) {
                return InkWell(
                  onLongPress: () {
                    controller.isShowReply = true;
                    controller.nameUserReply = controller.messageList2[index].sender!.fullName!;
                    controller.messParentId = controller.messageList2[index].id!;
                    controller.contentReply = "photo";
                    controller.isShowImage = true;
                    controller.imageReply = controller.messageList2[index].files![indexFile].link!;
                    controller.update();
                  },
                  onTap: () {
                    OpenUrl.openImageViewer(Get.context, controller.messageList2[index].files![indexFile].link);
                  },
                  child: Container(
                      width: 60.w,
                      height: 60.w,
                      padding:const EdgeInsets.all(2),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(6.r)
                      ),
                      child:
                      CacheNetWorkCustomBanner(urlImage: '${controller.messageList2[index].files![indexFile].link}',)
                  ),
                );
              }),
            )));
  }

  showTextMessageReplyGuess(index){
    return Visibility(
        visible: controller.messageList2[index].messParent != null,
        child: IntrinsicHeight(
          child: IntrinsicWidth(
            child: Row(
              children: [
                const VerticalDivider(
                  color: Colors.black,
                  thickness: 2,
                  width: 0,
                ),
                SizedBox(
                  width: 4.w,
                ),
                Visibility(
                  visible: controller.messageList2[index].messParent != null &&  controller.messageList2[index].messParent!.files!.isNotEmpty ,
                  child: SizedBox(
                      width: 30.w,
                      height: 30.w,
                      child:
                      CacheNetWorkCustomBanner(urlImage:  controller.messageList2[index].messParent != null?
                      controller.getImage( controller.messageList2[index].messParent!.files) :"",)
                  ),),
                SizedBox(
                  width: 4.w,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      constraints: BoxConstraints(
                          maxWidth: 150.w
                      ),
                      child: Text("${ controller.messageList2[index].messParent == null
                          ?""
                          : controller.messageList2[index].messParent!.sender!.fullName}"
                        ,style: TextStyle(
                            color:  Colors.black54,fontSize: 10.sp
                        ),),
                    ),
                    Container(
                      constraints: BoxConstraints(
                          maxWidth: 150.w
                      ),
                      child: Text( controller.messageList2[index].messParent == null
                          ?""
                          :controller.getContent( controller.messageList2[index].messParent!.files,
                          controller.messageList2[index].messParent!.content!)
                        ,overflow: TextOverflow.ellipsis
                        ,style:  TextStyle(color: Colors.black54,fontSize: 10.sp),),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ));
  }

  showImageMessageReplyGuess(index){
    return Visibility(
        visible:  controller.messageList2[index].messParent != null,
        child: Visibility(
            visible:  controller.messageList2[index].files!.isNotEmpty,
            child: Wrap(
              runSpacing: controller.runSpacing,
              spacing: controller.spacing,
              alignment: WrapAlignment.start,
              children: List.generate( controller.messageList2[index].files!.length, (indexFile) {
                return InkWell(
                  onLongPress: () {
                    controller.isShowReply = true;
                    controller.nameUserReply = controller.messageList2[index].sender!.fullName!;
                    controller.messParentId = controller.messageList2[index].id!;
                    controller.contentReply = "photo";
                    controller.isShowImage = true;
                    controller.imageReply = controller.messageList2[index].files![indexFile].link!;
                    controller.update();
                  },
                  onTap: () {
                    OpenUrl.openImageViewer(Get.context, controller.messageList2[index].files![indexFile].link);
                  },
                  child: Container(
                      width: 60.w,
                      height: 60.w,
                      padding:const EdgeInsets.all(2),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey),
                          borderRadius: BorderRadius.circular(6.r)
                      ),
                      child:
                      CacheNetWorkCustomBanner(urlImage: '${  controller.messageList2[index].files![indexFile].link}',)
                  ),
                );
              }),
            )));
  }

  AutoScrollTag messageSelf(int index, MessageContentController controller, BuildContext context) {
    return AutoScrollTag(
      key: ValueKey(index),
      controller: controller.scrollListMessage,
      index: index,child:  Container(
      padding: EdgeInsets.zero,
      width: Get.width,
      alignment: Alignment.centerRight,
      color: controller.listColor[index],
      margin: EdgeInsets.only(right: 4.w,top: 2.h),
      child: Row(
        children: [
          const Spacer(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              controller.setVisibleMessage(index)
                  ?SwipeTo(
                  iconSize: 16,
                  onLeftSwipe: () {
                    controller.isShowReply = true;
                    controller.nameUserReply = controller.messageList2[index].sender!.fullName!;
                    controller.messParentId = controller.messageList2[index].id!;
                    controller.contentReply = controller.messageList2[index].content!;
                    controller.isShowImage = false;
                    controller.update();
                    },
                  child: InkWell(
                    onTap: () {
                      if(controller.messageList2[index].messParent != null){
                        controller.scrollToItemReply(controller.messageList2[index].messParent!.id);
                      }
                      },
                    child:controller.messageList2[index].type != "NOTIFY"?IntrinsicWidth(
                      child: Container(
                        margin: EdgeInsets.zero,
                        alignment: Alignment.centerRight,
                        constraints: BoxConstraints(
                            maxWidth: 200.w
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(controller.getBorerRadiusTopRightSelf(controller.messageList2[index].position!)),
                              topLeft: Radius.circular(12.r),
                              bottomRight: Radius.circular(controller.getBorerRadiusBottomRightSelf(controller.messageList2[index].position!)),
                              bottomLeft: Radius.circular(12.r)),
                          color: ColorUtils.PRIMARY_COLOR,
                        ),
                        padding:  EdgeInsets.symmetric(horizontal: 12.w,vertical: 8.h),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            showTextMessageReplySelf(index),
                            InkWell(
                                onTap: (){
                                  controller.clickLink(controller.messageList2[index].content?.trim());
                                  },
                                child: Align(
                                  alignment: controller.getAlignment(index),
                                  child: Text(
                                    "${controller.messageList2[index].content?.trim()}",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12.sp,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                )
                            ),
                            showImageMessageReplySelf(index)
                          ],
                        ),

                      ),
                    ):Container(),
                  ))
                  :Container(),
              Visibility(
                  visible: controller.messageList2[index].files!.isNotEmpty,
                  child: Padding(padding: EdgeInsets.only(bottom: 4.h))),
              showImageMessageSelf(index,controller),
            ],
          ),
          SizedBox(width: 4.w)
        ],
      ),
    ),);
  }

  showTextMessageReplySelf(index){
    return Visibility(
        visible:controller.messageList2[index].messParent != null,
        child: IntrinsicHeight(
          child:IntrinsicWidth(
            child:  Row(
              children: [
                const VerticalDivider(
                  color: Colors.white,
                  thickness: 2,
                  width: 0,
                ),
                SizedBox(
                  width: 4.w,
                ),
                Visibility(
                  visible:controller.messageList2[index].messParent != null && controller.messageList2[index].messParent!.files!.isNotEmpty ,
                  child: SizedBox(
                      width: 30.w,
                      height: 30.w,
                      child:
                      CacheNetWorkCustomBanner(urlImage: controller.messageList2[index].messParent != null?
                      controller.getImage(controller.messageList2[index].messParent!.files) :"",)
                  ),),
                SizedBox(
                  width: 4.w,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      constraints: BoxConstraints(
                          maxWidth: 150.w
                      ),
                      child: Text("${controller.messageList2[index].messParent == null?"":controller.messageList2[index].messParent!.sender!.fullName}",style: TextStyle(
                          color:  Colors.white60,
                          fontSize: 10.sp
                      ),overflow: TextOverflow.ellipsis,),
                    ),
                    Container(
                      constraints: BoxConstraints(
                        maxWidth: 150.w
                      ),
                      child: Text(controller.messageList2[index].messParent == null?"":controller.getContent(controller.messageList2[index].messParent!.files,controller.messageList2[index].messParent!.content!),overflow: TextOverflow.ellipsis,
                        style:  TextStyle(color: Colors.white60,fontSize: 10.sp),),
                    )
                  ],
                ),
              ],
            ),
          ),
        ));
  }

  showImageMessageSelf(index,context){
    return Visibility(
        visible:controller.messageList2[index].messParent == null,
        child:controller.messageList2[index].files!.isNotEmpty? SwipeTo(
            onLeftSwipe: () {
              controller.isShowReply = true;
              controller.nameUserReply =controller.messageList2[index].sender!.fullName!;
              controller.messParentId = controller.messageList2[index].id!;
              controller.contentReply = "photo";
              controller.isShowImage = true;
              controller.imageReply = controller.messageList2[index].files![0].link!;
              controller.update();
            },
            child: Container(
              width: 250.w,
              margin: EdgeInsets.only(bottom: 4.h),
              alignment: Alignment.topRight,
              child:  Wrap(
                runSpacing: controller.runSpacing,
                spacing: controller.spacing,
                alignment: WrapAlignment.end,
                children: List.generate( controller.messageList2[index].files!.length, (indexFile) {
                  return InkWell(
                    onLongPress: () {
                      controller.isShowReply = true;
                      controller.nameUserReply = controller.messageList2[index].sender!.fullName!;
                      controller.messParentId = controller.messageList2[index].id!;
                      controller.contentReply = "photo";
                      controller.isShowImage = true;
                      controller.imageReply = controller.messageList2[index].files![indexFile].link!;
                      controller.update();
                    },
                    onTap: () {
                      OpenUrl.openImageViewer(Get.context, controller.messageList2[index].files![indexFile].link);
                    },
                    child: Container(
                        width: 60.w,
                        height: 60.w,
                        padding:const EdgeInsets.all(2),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(6.r)
                        ),
                        child:
                        CacheNetWorkCustomBanner(urlImage: '${  controller.messageList2[index].files![indexFile].link}',)
                    ),
                  );
                }),
              ),
            )):Container());
  }

  showImageMessageReplySelf(index){
    return Visibility(
        visible:controller.messageList2[index].messParent != null,
        child: Visibility(
            visible: controller.messageList2[index].files!.isNotEmpty,
            child: SwipeTo(
                onLeftSwipe: () {
                  controller.isShowReply = true;
                  controller.nameUserReply = controller.messageList2[index].sender!.fullName!;
                  controller.messParentId = controller.messageList2[index].id!;
                  controller.contentReply = "photo";
                  controller.isShowImage = true;
                  controller.imageReply = controller.messageList2[index].files![0].link!;
                  controller.update();
                },
                child: Container(
                  margin: EdgeInsets.only(bottom: 4.h,top: 4.h),
                  alignment: Alignment.topLeft,
                  child:  Wrap(
                    runSpacing: controller.runSpacing,
                    spacing: controller.spacing,
                    alignment: WrapAlignment.start,
                    children: List.generate(
                        controller.messageList2[index].files!.length, (indexFile) {
                      return InkWell(
                        onLongPress: () {
                          controller.isShowReply = true;
                          controller.nameUserReply = controller.messageList2[index].sender!.fullName!;
                          controller.messParentId = controller.messageList2[index].id!;
                          controller.contentReply = "photo";
                          controller.isShowImage = true;
                          controller.imageReply = controller.messageList2[index].files![indexFile].link!;
                          controller.update();
                        },
                        onTap: () {
                          OpenUrl.openImageViewer(Get.context, controller.messageList2[index].files![indexFile].link);
                        },
                        child: Container(
                            width: 60.w,
                            height: 60.w,
                            padding:const EdgeInsets.all(2),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.white),
                                borderRadius: BorderRadius.circular(6.r)
                            ),
                            child: CacheNetWorkCustomBanner(urlImage: '${ controller.messageList2[index].files![indexFile].link}',)
                        ),
                      );
                    }),
                  ),))));
  }

  Visibility showTimeMessage(MessageContentController controller, int index) {
    return Visibility(
        visible:controller.messageList2[index].type != "NOTIFY",
        child: Visibility(visible: controller.messageList2[index].position == Session.end||controller.messageList2[index].position == Session.oneItem||controller.messageList2[index].files!.isNotEmpty,
            child: controller.getDiffTime(index)
                ? Center(
              child: Container(
                margin: EdgeInsets.fromLTRB(8.w,8.h,0.w,4.h),
                child: Text(controller.outputDateFormat2.format(DateTime.fromMillisecondsSinceEpoch(controller.messageList2[index].createdAt!)),style: TextStyle(fontSize: 12.sp)),
              ),)
                :const SizedBox()));
  }

  showTimeTypeNotify(index){
    return controller.messageList2[index].type == "NOTIFY"
        ?Center(
      child: Container(
        margin: EdgeInsets.fromLTRB(8.w,8.h,0.w,4.h),
        child: Text(controller.outputDateFormat2.format(DateTime.fromMillisecondsSinceEpoch(controller.messageList2[index].createdAt!)),style: TextStyle(fontSize: 12.sp)),
      ),):const SizedBox();
  }

  Visibility showTextTypeNotify(MessageContentController controller, int index) {
    return Visibility(
        visible: controller.messageList2[index].type == "NOTIFY",
        child: Center(child: Container(
          constraints: BoxConstraints(maxWidth: 250.w
          ),
          child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  children: [
                    TextSpan(
                      text: '${controller.detailGroupChat.createdBy?.fullName} ',
                      style: TextStyle(
                          fontSize: 12.sp,
                          color: ColorUtils.PRIMARY_COLOR
                      ),
                    ),
                    TextSpan(
                        text: '${controller.getStartRichText(controller.messageList2[index].notifyType)}',
                        style: TextStyle(
                            fontSize: 12.sp,
                            color: const Color.fromRGBO(177, 177, 177, 1)
                        ),
                        children: controller.messageList2[index].user?.map((e) {
                          var indexUser = controller.messageList2[index].user?.indexOf(e);
                          var showSplit = ", ";
                          if (indexUser == controller.messageList2[index].user!.length - 1) {
                            showSplit = "";
                          }
                          return TextSpan(
                              text: "${e.fullName}$showSplit",
                              style: TextStyle(
                                  color: const Color.fromRGBO(177, 177, 177, 1),
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'static/Inter-Medium.ttf'));
                        }).toList()
                    ),
                    TextSpan(
                      text: '${controller.getEndRichText(controller.messageList2[index].notifyType)} ',
                      style: TextStyle(
                          fontSize: 12.sp,
                          color: const Color.fromRGBO(177, 177, 177, 1)
                      ),
                    ),
                  ]
              )),),));
  }

  buildAvatarSelect() {
    var file = controller.files[0].file;
    return Container(
      height: 80,
      margin: EdgeInsets.only(top: 10.h),
      child: Stack(
        children: [
          InkWell(
            onTap: (){

            },
            child:  SizedBox(
              width: 80.w,
              height: 80.w,
              child: CircleAvatar(
                backgroundImage: Image.file(
                  file!,
                ).image,
              ),
            ),
          ),
          Positioned(
              right: 0,
              bottom: 0,
              child: InkWell(
                onTap: () {
                  pickerImage();
                },
                child: Image.asset(
                  'assets/images/img_cam.png',width: 24.w,height: 24.h,),
              ))
        ],
      ),
    );
  }

  Future<void> pickerImage() async {
    var file = await FileDevice.showSelectFileV2(Get.context!,image: true);
    if (file.isNotEmpty) {
      controller.uploadFileImageGroup(file);
    }
  }

  buildImageSendMessage() {
    return GridView.builder(
        padding: const EdgeInsets.all(10),
        gridDelegate:
        SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 80.w,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,

        ),
        scrollDirection: Axis.vertical, //chiều cuộn
        shrinkWrap: true,
        itemCount: controller.fileUploadSendMessage.length,
        itemBuilder: (context, indexGrid) {
          return InkWell(
            onTap: () {
              OpenUrl.openImageViewer(context, controller.fileUploadSendMessage[indexGrid].link);
            },
            child: Stack(
              children: [
                Container(
                  width: 80.w,
                  height: 80.w,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.grey
                        ),
                        borderRadius: BorderRadius.circular(6.r)
                    ),
                  child:
                  CacheNetWorkCustomBanner(urlImage: '${  controller.fileUploadSendMessage[indexGrid].link }',)
                ),
                Positioned(
                    top: 0,
                    right: 4,
                    child: InkWell(
                      onTap: () {
                        controller.fileUploadSendMessage.removeAt(indexGrid);
                        controller.update();
                      },
                      child: Container(
                        padding: EdgeInsets.zero,
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        child: SvgPicture.asset("assets/images/icon_dangerous.svg",fit: BoxFit.cover,height: 18.w,width: 18.w,),
                      ),
                    ))
              ],
            ),
          );
        });
  }

  sendMessage(id){
    if(controller.textEditingController.text.trim() != "" || controller.fileUploadSendMessage.isNotEmpty) {
      Get.find<MessageController>().socketIo.sendMessage(controller.textEditingController.text.trim(), controller.tmpChatRoomId,controller.fileUploadSendMessage,id);
      controller.textEditingController.text = "";
      controller.timer?.cancel();
      if(controller.indexPageCurrent !=0 &&controller.scrollListMessage.position.extentBefore>1000){
        controller.getAllMessageGroupChat(controller.tmpChatRoomId, 0, controller.pageSize);
      }
      controller.scrollListMessage.jumpTo(0.0);
      controller.isShowClickEnd = false;
      controller.update();
      controller.fileUploadSendMessage = [];
    }
    controller.isShowReply = false;
    controller.update();
  }

  Future<void> pickerImageSendMessage() async {
    var file = await FileDevice.showSelectFileV2(Get.context! ,image: true,mutilpleImage: true);
    if (file.isNotEmpty) {
      controller.uploadFileImageMessage(file);
    }
  }

  dialogChangeAvatarGroupChat(){
    return  Get.dialog(GetBuilder<MessageContentController>(builder: (controller) {
      return Center(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 32.w),
          child: Wrap(
            children: [
              Card(
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(8.0),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 16.w,
                    vertical: 16.h,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Đổi Ảnh Nhóm',
                            style: TextStyleUtils.sizeText16Weight500()?.copyWith(color: Colors.black),
                          ),
                          IconButton(
                            padding: EdgeInsets.zero,
                            constraints: const BoxConstraints(),
                            splashColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            onPressed: () {
                              Get.back();
                              controller.imageGroupChatUpload = [];
                              controller.files = [];
                              controller.update();
                            },
                            icon: const Icon(
                              Icons.close_rounded,
                              size: 20.0,
                              color: ColorUtils.colorGray,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      SizedBox(
                          width: 48,
                          height: 48.h,
                          child: CacheNetWorkCustom(urlImage: '${ controller.detailGroupChat.image }',)
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      InkWell(
                        onTap: () {
                          if(controller.imageGroupChatUpload.isEmpty){
                            pickerImage();
                          }
                        },
                        child: Container(
                          color: const Color.fromRGBO(246, 246, 246, 1),
                          child: DottedBorder(
                            dashPattern: const [5, 5],
                            radius: const Radius.circular(6),
                            borderType: BorderType.RRect,
                            color: const Color.fromRGBO(
                                192, 192, 192, 1),
                            padding: EdgeInsets.all(16.h),
                            child: SizedBox(
                              width: double.infinity,
                              height: 80.w,
                              child: controller.imageGroupChatUpload.isEmpty
                                  ?Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                      padding:
                                      EdgeInsets.only(
                                          top: 16.h)),
                                  Icon(
                                    Icons.drive_folder_upload_rounded,
                                    color: Colors.grey,
                                    size: 40.w,
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(top: 4.h)),
                                  Text(
                                    "Tải lên Ảnh",
                                    style: TextStyleUtils.sizeText14Weight500()?.copyWith(color: Colors.grey),
                                  ),
                                ],
                              ):Center(
                                child: buildAvatarSelect(),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 16.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          ElevatedButton.icon(
                            onPressed: () {
                              Get.back();
                              controller.imageGroupChatUpload = [];
                              controller.files = [];
                              controller.update();
                            },
                            style: ElevatedButton
                                .styleFrom(
                                shape:
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0),
                                  side: BorderSide(
                                      color: Colors.grey,
                                      width: 0.75.w),
                                ),
                                backgroundColor:
                                Colors.white),
                            icon: Icon(
                              Icons.close_rounded,
                              color: Colors.black,
                              size: 20.w,
                            ),
                            label: Text(
                              'Huỷ',
                              style: TextStyleUtils.sizeText14Weight500()?.copyWith(color: Colors.black),
                            ),
                          ),
                          SizedBox(
                            width: 10.w,
                          ),
                          ElevatedButton.icon(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: ColorUtils.PRIMARY_COLOR,
                            ),
                            onPressed: () {
                              controller.updateImageGroupChat(controller.tmpChatRoomId);
                            },
                            icon: Icon(
                              Icons.check,
                              size: 20.w,
                            ),
                            label: Text(
                              'Xác nhận',
                              style: TextStyleUtils.sizeText14Weight500()?.copyWith(color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )],
          ),
        ),
      );
    },),);
  }

  dialogChangeNameGroup(){
    return Get.dialog(Center(
      child: Wrap(
        children: [Container(
          margin: EdgeInsets.symmetric(horizontal: 32.w),
          child: Card(
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(8),
              ),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 16.w,
                vertical: 16.h,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'Đổi Tên Nhóm',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                        ),
                      ),
                      IconButton(
                        padding: EdgeInsets.zero,
                        constraints: const BoxConstraints(),
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onPressed: () {
                          Get.back();
                          controller.controllerNameGroupChat.text = "";
                          controller.update();
                        },
                        icon: const Icon(
                          Icons.close_rounded,
                          size: 20.0,
                          color: Color(0xFF7D7E7E),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  FocusScope(
                    node: FocusScopeNode(),
                    child: Focus(
                        onFocusChange: (focus) {
                          controller.update();
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              padding: const EdgeInsets.all(2.0),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                  const BorderRadius.all(Radius.circular(6.0)),
                                  border: Border.all(
                                    width: 1,
                                    style: BorderStyle.solid,
                                    color: controller.focusNameGroupChat.hasFocus
                                        ? ColorUtils.PRIMARY_COLOR
                                        : const Color.fromRGBO(192, 192, 192, 1),
                                  )),
                              child: TextFormField(
                                maxLines: null,
                                cursorColor: ColorUtils.PRIMARY_COLOR,
                                focusNode: controller.focusNameGroupChat,
                                onChanged: (value) {
                                  if(controller.controllerNameGroupChat.text.trim() != ""){
                                    controller.isShowErrorNameGroup = false;
                                  }
                                  controller.update();
                                },
                                onFieldSubmitted: (value) {
                                  controller.update();
                                },
                                controller: controller.controllerNameGroupChat,
                                decoration: InputDecoration(
                                  labelText: "Nhập tên nhóm",
                                  labelStyle: TextStyle(
                                      fontSize: 14.0.sp,
                                      color: const Color.fromRGBO(177, 177, 177, 1),
                                      fontWeight: FontWeight.w500,
                                      fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                                  contentPadding: const EdgeInsets.symmetric(
                                      vertical: 8, horizontal: 16),
                                  prefixIcon:  null,
                                  suffixIcon:  Visibility(
                                      visible:
                                      controller.controllerNameGroupChat.text.isNotEmpty ==
                                          true,
                                      child: InkWell(
                                        onTap: () {
                                          controller.controllerNameGroupChat.text = "";
                                        },
                                        child: const Icon(
                                          Icons.close_outlined,
                                          color: Colors.black,
                                          size: 20,
                                        ),
                                      )),
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  border: InputBorder.none,
                                  errorStyle: const TextStyle(height: 0),
                                  focusedErrorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  floatingLabelBehavior: FloatingLabelBehavior.auto,

                                ),
                              ),
                            ),
                            Visibility(
                              visible: controller.isShowErrorNameGroup,
                              child: Container(
                                  padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                  child: const Text(
                                    "Vui lòng nhập tên nhóm",
                                    style: TextStyle(color: Colors.red),
                                  )),
                            )
                          ],
                        )),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ElevatedButton.icon(
                        onPressed: () {
                          Get.back();
                          controller.controllerNameGroupChat.text = "";
                          controller.update();
                        },
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4.0),
                              side: const BorderSide(
                                  color: Color(0xFFC0C0C0), width: 0.75),
                            ),
                            backgroundColor: Colors.white),
                        icon:
                        const Icon(Icons.close_rounded, color: Colors.black),
                        label: const Text(
                          'Huỷ',
                          style: TextStyle(
                            color: Color(0xFF333333),
                            fontSize: 14,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      ElevatedButton.icon(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color(0xFFF88125),
                        ),
                        onPressed: () {
                          if (controller.controllerNameGroupChat.text.trim() == "") {
                            controller.isShowErrorNameGroup = true;
                            controller.update();
                          } else {
                            controller.updateNameGroupChat(controller.controllerNameGroupChat.text.trim(), controller.detailGroupChat.id);
                          }
                        },
                        icon: const Icon(Icons.check),
                        label: const Text('Xác nhận'),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        )],
      ),
    ));
  }

  popupMenuGroupChat(){
    return GetBuilder<MessageContentController>(builder: (controller) {
      return Visibility(
          visible: controller.listItemPopupMenu.isNotEmpty,
          child: PopupMenuButton(
              padding: EdgeInsets.zero,
              shape: const RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.all(
                      Radius.circular(
                          6.0))),
              icon: const Icon(
                Icons.more_horiz_outlined,
                color: Colors.black,
              ),
              itemBuilder: (context) {
                return controller.getListPopupMenuItem();
              },
              onSelected: (value) {
                switch (value) {
                  case 0:
                    Get.toNamed(Routes.listUserInGroupChatPage,arguments: [controller.detailGroupChat.id,controller.detailGroupChat.createdBy?.id]);
                    break;
                  case 1:
                    dialogChangeAvatarGroupChat();
                    break;
                  case 2:
                    dialogChangeNameGroup();
                    break;
                  case 3:
                    Get.toNamed(Routes.addUserGroupChatPage,arguments: controller.tmpChatRoomId);
                    break;
                  case 4:
                    controller.updateNotifyGroupChat(controller.tmpChatRoomId);
                    break;

                }
              }));
    },);
  }



}
