import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import '../../../../../commom/utils/color_utils.dart';
import '../../../../../routes/app_pages.dart';
import '../../../home/home_controller.dart';
import 'list_user_in_group_chat_controller.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
class ListUserInGroupChatPage extends GetView<ListUserInGroupChatController>{
  @override
  final controller = Get.put(ListUserInGroupChatController());

  ListUserInGroupChatPage({super.key});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
            title: const Text("Thành viên",style: TextStyle(color: Colors.white,fontSize: 16),),
            actions: [
              Visibility(
                  visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_MESSAGE_USER_ADD),
                  child:  IconButton(
                    icon: const Icon(Icons.person_add_alt_1,color: Colors.white,),
                    onPressed: () {
                      Get.toNamed(Routes.addUserGroupChatPage,arguments: controller.chatRoomId);
                    },
                  ))
            ],
          ),
          body: Obx(() => Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(6)),
            margin:  EdgeInsets.only( top:16.h),
            child: ListView.builder(
                itemCount: controller.listAll.length,
                shrinkWrap: true,
                controller: controller.controllerLoadMore,
                itemBuilder: (context, index) {
                  controller.getListPopupMenuItem(index);
                  return Column(
                    children: [
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.only(left: 8.w)),
                          SizedBox(
                              width: 32.h,
                              height: 32.h,
                              child:
                              CacheNetWorkCustom(urlImage: '${controller.listAll[index].image}',)
                          ),
                          Padding(padding: EdgeInsets.only(left: 8.w)),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    "${controller.listAll[index].fullName}",
                                    style: TextStyle(
                                        fontSize: 16.sp,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black),
                                  ),
                                  Visibility(
                                    visible: controller.listAll[index].id == controller.createdById,
                                    child: Text(
                                      " (quản trị viên)",
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w500,
                                          color: const Color.fromRGBO(133, 133, 133, 1)),
                                    ),)
                                ],
                              ),
                              SizedBox(
                                width: 230.w,
                                child: getRole(
                                    controller.getListRole(controller.listAll[index].type,index),
                                    controller.listAll[index].type, index),
                              )
                            ],
                          ),
                          const Spacer(),
                          Visibility(
                              visible: controller.listItemPopupMenu.isNotEmpty,
                              child: PopupMenuButton(
                                  padding: EdgeInsets.zero,
                                  shape: const RoundedRectangleBorder(
                                      borderRadius:
                                      BorderRadius.all(
                                          Radius.circular(
                                              6.0))),
                                  icon: const Icon(
                                    Icons.more_horiz_outlined,
                                    color: Color.fromRGBO(125, 126, 126, 1),
                                  ),
                                  itemBuilder: (context) {
                                    return controller.getListPopupMenuItem(index);
                                  },
                                  onSelected: (value) {
                                    switch (value) {
                                      case 0:
                                        controller.getIdPrivateChat(controller.listAll[index].id);
                                        break;
                                      case 1:
                                        Get.dialog(Center(
                                          child: Wrap(
                                              children: [
                                                Container(
                                                  width: double
                                                      .infinity,
                                                  decoration: BoxDecoration(
                                                      color: Colors
                                                          .transparent,
                                                      borderRadius:
                                                      BorderRadius.circular(16)),
                                                  child:
                                                  Stack(
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 40.h,
                                                            right: 50.w,
                                                            left: 50.w),
                                                        decoration: BoxDecoration(
                                                            color: Colors.white,
                                                            borderRadius: BorderRadius.circular(16)),
                                                        child:
                                                        Column(
                                                          children: [
                                                            Padding(padding: EdgeInsets.only(top: 40.h)),
                                                            Container(
                                                              margin: EdgeInsets.symmetric(horizontal: 16.w),
                                                              child: Text(
                                                                "Bạn có chắc muốn xoá người này khỏi nhóm?",
                                                                style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontSize: 14.sp),
                                                              ),
                                                            ),
                                                            Container(
                                                                color: Colors.white,
                                                                padding: EdgeInsets.all(16.h),
                                                                child: SizedBox(
                                                                  width: double.infinity,
                                                                  height: 46,
                                                                  child: ElevatedButton(
                                                                      style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                                                                      onPressed: () {
                                                                        controller.deleteUserGroupChat(controller.listAll[index].id);
                                                                      },
                                                                      child: Text(
                                                                        'Xóa',
                                                                        style: TextStyle(color: Colors.white, fontSize: 16.sp),
                                                                      )),
                                                                )),
                                                            TextButton(
                                                                onPressed: () {
                                                                  Get.back();
                                                                },
                                                                child: Text(
                                                                  "Hủy",
                                                                  style: TextStyle(color: Colors.red, fontSize: 16.sp),
                                                                ))
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                          alignment:
                                                          Alignment.center,
                                                          margin: const EdgeInsets.only(top: 10),
                                                          height: 80,
                                                          child: Image.asset("assets/images/image_app_logo.png"))
                                                    ],
                                                  ),
                                                ),
                                              ]),
                                        ));
                                        break;
                                      case 2:
                                        Get.toNamed(Routes.informationUserPage, arguments: controller.listAll[index]);
                                        break;


                                    }
                                  }))

                        ],
                      ),
                      const Divider(),
                    ],
                  );
                }),
          )),
        )
    ), onWillPop: () async{

      return true;
    },);
  }
  RichText getRole(List<dynamic> list, role, index) {
    return RichText(
      textAlign: TextAlign.left,
      text: TextSpan(
        text: '${controller.getTitle(role)}',
        style: TextStyle(
            color: const Color.fromRGBO(133, 133, 133, 1),
            fontSize: 12.sp,
            fontWeight: FontWeight.w500,
            fontFamily: 'static/Inter-Medium.ttf'),

        children: list.map((e) {
          var index = list.indexOf(e);
          var showSplit = ", ";
          if (index == list.length - 1) {
            showSplit = "";
          }
          return TextSpan(
              text: "${e.fullName}$showSplit",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf'));
        }).toList(),
      ),
    );
  }
  RichText getRoleTeacher(List<dynamic> list, role, index) {
    return RichText(
      textAlign: TextAlign.left,
      text: TextSpan(
          text: '${controller.getTitle(role)}',
          style: TextStyle(
              color: const Color.fromRGBO(133, 133, 133, 1),
              fontSize: 12.sp,
              fontWeight: FontWeight.w500,
              fontFamily: 'static/Inter-Medium.ttf'),

          children: [
            TextSpan(
              text:
              "${controller.getTextHomeRoomTeacher(controller.listAll[index].positionName, index)}",
              style: TextStyle(
                  color: const Color.fromRGBO(26, 26, 26, 1),
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf'),
              children: controller.listAll[index].subject?.map((e) {
                var indexSubject = controller.listAll[index].subject?.indexOf(e);
                var showSplit = ", ";
                if (indexSubject == controller.listAll[index].subject!.length - 1) {
                  showSplit = "";
                }
                if (controller.listAll[index].type == "TEACHER") {
                  return TextSpan(
                      text: "Môn ${e.fullName}$showSplit",
                      style: TextStyle(
                          color: const Color.fromRGBO(26, 26, 26, 1),
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'static/Inter-Medium.ttf'));
                }

                return TextSpan(
                    text: "${e.fullName}$showSplit",
                    style: TextStyle(
                        color: const Color.fromRGBO(26, 26, 26, 1),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'static/Inter-Medium.ttf'));
              }).toList(),
            ),
          ]
      ),
    );
  }


}