import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/data/model/common/user_group_by_app.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/message/message_controller.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/contacts.dart';
import '../../../../../commom/app_cache.dart';
import '../../../../../commom/constants/string_constant.dart';
import '../../../../../commom/utils/app_utils.dart';
import '../../../../../commom/utils/color_utils.dart';
import '../../../../../data/model/res/message/detail_group_chat.dart';
import '../../../../../data/repository/chat/chat_repo.dart';
import '../../../../../routes/app_pages.dart';



class ListUserInGroupChatController extends GetxController {
  var selectedQuantity = 0.obs;
  var clickAll = <bool>[].obs;
  var listCheckBox = <bool>[].obs;
  var isCheckAll = false.obs;
  var contacts = Contacts().obs;
  RxList<ItemContact> listAll = <ItemContact>[].obs;
  RxList<ItemContact> listAllLoadMore = <ItemContact>[].obs;
  var listId = <String>[];
  final ChatRepo _chatRepo = ChatRepo();
  var indexPage = 1.obs;
  var controllerLoadMore = ScrollController();
  var itemDetailGroupChat = DetailGroupChat().obs;
  var chatRoomId = "";
  var createdById = "";

  var listItemPopupMenu = <PopupMenuItem>[];

  var heightMessage= 30.0;
  var heightDeleteMember = 30.0;

  var showMessage= true;
  var showDeleteMember = false;

  var indexMessage = 0;
  var indexDeleteMember  = 0;


  @override
  void onInit() {
    var argument = Get.arguments;
    if (argument != null) {
      chatRoomId = argument[0];
      createdById = argument[1];
    }
    getDetailGroupChat(chatRoomId, 0, 20);
    controllerLoadMore = ScrollController()..addListener(_scrollListenerLoadMore);
    super.onInit();
  }


  getListPopupMenuItem(index){
    listItemPopupMenu = [];
    var message =  PopupMenuItem<int>(
        value: 0,
        padding: EdgeInsets.zero,
        height: heightMessage,
        child: showMessage
            ?Container(
          padding: EdgeInsets.symmetric(vertical: 4.h),
          decoration:  const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black26))),
          child: Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Icon(
                Icons
                    .messenger_outline_outlined,
                color: Colors
                    .black,
              ),
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Text(
                  "Nhắn tin"),

            ],
          ),
        )
            :Row(
          children: [
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Icon(
              Icons
                  .messenger_outline_outlined,
              color: Colors
                  .black,
            ),
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Text(
                "Nhắn tin"),

          ],
        )
    );
    var deleteMember =  PopupMenuItem<int>(
      padding: EdgeInsets.zero,
      value: 1,
      height: heightDeleteMember,
      child: showDeleteMember
          ?Container(
        padding: EdgeInsets.symmetric(vertical: 4.h),
        decoration:  const BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.black26))),
        child: Row(
          children: [
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Icon(
              Icons
                  .person_remove_outlined,
              color: Colors
                  .black,
            ),
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Text(
                "Xóa thành viên"),

          ],
        ),
      )
          :Row(
        children: [
          Padding(
              padding: EdgeInsets.only(
                  right:
                  8.w)),
          const Icon(
            Icons
                .person_remove_outlined,
            color: Colors
                .black,
          ),
          Padding(
              padding: EdgeInsets.only(
                  right:
                  8.w)),
          const Text(
              "Xóa thành viên"),

        ],
      ),
    );
    var detailInformation =   PopupMenuItem<int>(
      value: 2,
      padding: EdgeInsets.zero,
      height: 30,
      child: Row(
        children: [
          Padding(
              padding: EdgeInsets.only(
                  right:
                  8.w)),
          const Icon(
            Icons.person_outline_outlined,
            color: Colors
                .black,
          ),
          Padding(
              padding: EdgeInsets.only(
                  right:
                  8.w)),
          const Text("Thông tin cá nhân")
        ],
      ),
    );
    List<UserGroupByApp> listPageMessage = Get.find<HomeController>().userGroupByApp.where((element) => element.page?.code  == StringConstant.PAGE_MESSAGE).toList();
    if(listPageMessage.isNotEmpty){
      List<UserGroupByApp> listPageMessageUser = listPageMessage[0].page!.children!.where((element) => element.page?.code == StringConstant.PAGE_MESSAGE_USER).toList();
      if(listPageMessageUser.isNotEmpty){
        var listMessage = listPageMessageUser[0].features?.where((element) => element == StringConstant.FEATURE_SEND_MESSAGE);
        var listDeleteMember = listPageMessageUser[0].features?.where((element) => element == StringConstant.FEATURE_MESSAGE_USER_DELETE);
        var listViewProfile = listPageMessageUser[0].features?.where((element) => element == StringConstant.FEATURE_VIEW_PROFILE);

        if(listAll[index].id != AppCache().userId){
          if(createdById == AppCache().userId){
            if(listMessage!.isNotEmpty){
              listItemPopupMenu.add(message);
            }
            if(listDeleteMember!.isNotEmpty){
              listItemPopupMenu.add(deleteMember);
            }
            if(listViewProfile!.isNotEmpty){
              listItemPopupMenu.add(detailInformation);
            }
          }else{
            if(listMessage!.isNotEmpty){
              listItemPopupMenu.add(message);
            }
            if(listViewProfile!.isNotEmpty){
              listItemPopupMenu.add(detailInformation);
            }
          }
        }
      }
    }




    indexMessage = listItemPopupMenu.indexOf(message);
    indexDeleteMember = listItemPopupMenu.indexOf(deleteMember);

    showMessage = setVisiblePopupMenuItem(indexMessage,listItemPopupMenu);
    showDeleteMember =setVisiblePopupMenuItem(indexDeleteMember,listItemPopupMenu);

    heightMessage = setHeightPopupMenuItem(indexMessage,listItemPopupMenu);
    heightDeleteMember = setHeightPopupMenuItem(indexDeleteMember,listItemPopupMenu);


    update();

    return listItemPopupMenu;
  }




  void _scrollListenerLoadMore() async {
    if (controllerLoadMore.position.pixels == controllerLoadMore.position.maxScrollExtent) {
      if (listAllLoadMore.isNotEmpty) {
        indexPage.value++;
      }
      await _chatRepo.getListUserInGroupChat(
          chatRoomId,
              indexPage.value,
              20)
          .then((value) {
        if (value.state == Status.SUCCESS) {
          listAllLoadMore.value = [];
          listAllLoadMore.value = value.object!.items!;
          listAll.addAll(listAllLoadMore);
          listAll.refresh();
        }
      });
    }
  }

  getDetailGroupChat(chatRoomId, page, size) async {
    listAll.value = [];
    await _chatRepo
        .getListUserInGroupChat(chatRoomId, page, size)
        .then((value) {
      if (value.state == Status.SUCCESS) {
        listAll.value = value.object!.items!;
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lỗi",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

  getIdPrivateChat(userId) async {
    await _chatRepo.getIdPrivateChat(userId).then((value) {
      if (value.state == Status.SUCCESS) {
        itemDetailGroupChat.value = value.object!;
        Get.toNamed(Routes.detailMessagePrivatePage,
            arguments: itemDetailGroupChat.value.id);
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lỗi",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

  deleteUserGroupChat(userId) async {
    await _chatRepo
        .deleteUserGroupChat(
        chatRoomId, userId)
        .then((value) {
      Get.back();
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Xóa người dùng khỏi nhóm chat thành công");
        Get.find<MessageController>().socketIo.deleteUserGroupChat(chatRoomId,userId);
        onInit();

      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(
            value.message ?? "Xóa người dùng khỏi nhóm chat thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

  getListRole(role, index) {
    switch (role) {
      case "PARENT":
        return listAll[index].student;
      case "STUDENT":
        return listAll[index].parent;
      case "TEACHER":
        return [];
      case "MANAGER":
        return [];
    }
  }

  getTextHomeRoomTeacher(isHomeRoom, index) {
    var split = "";
    switch (isHomeRoom) {
      case "":
        return "";
      case null:
        return "";
      case "Giáo viên chủ nhiệm":
        if (listAll[index].subject!.isEmpty) {
          split = "";
        } else {
          split = ",";
        }
        return "Giáo viên chủ nhiệm$split ";
      default:
        return "";
    }
  }

  getTitle(role) {
    switch (role) {
      case "PARENT":
        return "Phụ huynh hs: ";
      case "STUDENT":
        return "Phụ huynh: ";
      case "TEACHER":
        return "Giáo viên";
      case "MANAGER":
        return "Cán bộ quản lý";
      default:
        return "";
    }
  }
}
