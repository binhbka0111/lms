import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:get/get.dart';

import 'information_user_controller.dart';
//ignore: must_be_immutable
class InformationUserPage extends GetView<InformationUserController> {
  @override
  final controller = Get.put(InformationUserController());

  InformationUserPage({super.key});


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text("Chi tiết"),
        leading: BackButton(
          color: Colors.white,
          onPressed: () {
            Get.back();
          },
        ),
        backgroundColor: ColorUtils.PRIMARY_COLOR,
      ),
      backgroundColor: const Color.fromRGBO(249, 249, 249, 1),
      body: Container(
        margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
        child: Column(
          children: [profileAvatarWidget(), informationWidget(size)],
        ),
      ),
    );
  }

  informationWidget(Size size) {
    return Obx(() => controller.isReady.value
        ? Container(
      margin: EdgeInsets.only(top: 16.h),
      padding: EdgeInsets.only(left: 6.w, right: 6.w),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: const Color.fromRGBO(255, 255, 255, 1),
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(
                top: 16.h, bottom: 8.h, right: 9.w, left: 9.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'Thông Tin Cá Nhân',
                  style: styleTitle,
                ),
              ],
            ),
          ),
          Container(
              margin: const EdgeInsets.all(9),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        'Họ Và Tên: ',
                        style: styleTitle,
                      ),
                      Expanded(child: Container()),
                      Text(
                        controller.userProfile.value.fullName ?? "",
                        style: TextStyle(
                            color: ColorUtils.PRIMARY_COLOR,
                            fontSize: 14.sp,
                            fontFamily: 'static/Inter-Medium.ttf',
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 10.h)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${controller.getTitleClassDetail(controller.item.value.type)}",
                        style: styleTitle,
                      ),
                      controller.getPosition(),

                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 10.h)),
                  Row(
                    children: [
                      Text(
                        'Số Điện Thoại: ',
                        style: styleTitle,
                      ),
                      Expanded(child: Container()),
                      InkWell(
                        onTap: () async {
                          await launchUrl(Uri(
                            scheme: 'tel',
                            path: controller.userProfile.value.phone,
                          ));
                        },
                        child: Text(
                          controller.userProfile.value.phone ?? "",
                          style: styleValue,
                        ),
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 10.h)),
                  Row(
                    children: [
                      Text(
                        'Email: ',
                        style: styleTitle,
                      ),
                      Expanded(child: Container()),
                      Text(
                        controller.userProfile.value.email ?? "",
                        style: styleValue,
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 10.h)),
                  controller.getTitleSchoolDetail(
                      controller.item.value.type) ==
                      ""
                      ? Container()
                      : Row(
                    mainAxisAlignment:
                    MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '${controller.getTitleSchoolDetail(controller.item.value.type)}',
                        style: styleTitle,
                      ),
                      const Spacer(),
                      SizedBox(
                        width: size.width*0.5,
                        child: Text(
                          '${controller.getValueTitleSchoolDetail(controller.item.value.type)}',
                          style: styleValue,
                          textAlign: TextAlign.right,
                        ),
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 10.h)),
                ],
              ))
        ],
      ),
    )
        : Container());
  }

  profileAvatarWidget() {
    return Obx(() => Container(
      width: 400.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.r),
        color: const Color.fromRGBO(255, 255, 255, 1),
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 10.h),
            child: SizedBox(
                width: 80.h,
                height: 80.h,
                child: InkWell(
                  onTap: () {
                    controller.goToDetailAvatar();
                  },
                  child:
                  CacheNetWorkCustom(urlImage: '${controller.userProfile.value.image}',)

                )),
          ),
          SizedBox(
            height: 8.h,
          ),
          Text(
            controller.userProfile.value.fullName ?? "",
            style: TextStyle(
                color: const Color.fromRGBO(26, 26, 26, 1),
                fontSize: 18.sp,
                fontFamily: 'static/Inter-Medium.ttf',
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 8.h,
          ),
          Text(
            controller.userProfile.value.email ?? "",
            style: TextStyle(
              color: const Color.fromRGBO(133, 133, 133, 1),
              fontSize: 10.sp,
            ),
          ),
          SizedBox(
            height: 16.h,
          ),
        ],
      ),
    ));
  }

  var styleValue = TextStyle(
      color: const Color.fromRGBO(26, 26, 26, 1),
      fontSize: 14.sp,
      fontFamily: 'static/Inter-Medium.ttf',
      fontWeight: FontWeight.w500);
  var styleTitle = TextStyle(
      color: const Color.fromRGBO(133, 133, 133, 1),
      fontSize: 12.sp,
      fontFamily: 'static/Inter-Medium.ttf',
      fontWeight: FontWeight.w500);



}
