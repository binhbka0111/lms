import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import '../../../../commom/utils/color_utils.dart';
import '../../../../commom/utils/date_time_utils.dart';
import '../../../../routes/app_pages.dart';
import '../list_message_un_read/list_message_un_read_controller.dart';
import '../message_controller.dart';
import 'list_all_message_controller.dart';



class ListAllMessagePage extends GetView<ListAllMessageController> {
  ListAllMessagePage({super.key});

  @override
  final controller = Get.put(ListAllMessageController());
  @override
  Widget build(BuildContext context) {
    return Obx(() => controller.isReady.value?GestureDetector(
      onTap: () {
        Slidable.of(context)?.close(duration: const Duration(milliseconds: 500));
      },
      child: ListView.builder(
          shrinkWrap: true,
          controller: controller.controllerLoadMore,
          itemCount: controller.dataGroupMessage.length,
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> goToMessageContent(index),StringConstant.FEATURE_MESSAGE_DETAIL);
              },
              child:  Slidable(
                  closeOnScroll: false,

                  endActionPane: ActionPane(
                    motion: const ScrollMotion(),
                    extentRatio: 0.5,
                    children: controller.getListCustomSlidableAction(index),
                  ),
                  child: Container(
                    margin: const EdgeInsets.all(16),
                    child: Row(
                      children: [
                        SizedBox(
                            width: 40,
                            height: 40.h,
                            child:
                            CacheNetWorkCustom(urlImage: '${ controller.dataGroupMessage[index].image }',)

                        ),
                        Padding(padding: EdgeInsets.only(left: 16.w)),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              constraints: BoxConstraints(
                                  maxWidth: 230.w
                              ),
                              child: Text(
                                "${controller.dataGroupMessage[index].name?.trim()}",
                                style: TextStyle(
                                    fontSize: 16.sp,
                                    color: const Color.fromRGBO(0, 0, 0, 1)),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),

                            Visibility(
                                visible: controller.dataGroupMessage[index].message != null,
                                child: Row(
                                  children: [
                                    SizedBox(
                                      width: 90.w,
                                      child: Text(
                                        controller.dataGroupMessage[index].message?.content?.trim() == ""?"Tệp đính kèm":"${controller.dataGroupMessage[index].message?.content?.trim()}",
                                        style: TextStyle(
                                          color:
                                          controller.dataGroupMessage[index].countNotSeen! > 0
                                              ? const Color.fromRGBO(26, 26, 26, 1)
                                              : const Color.fromRGBO(133, 133, 133, 1),
                                          fontSize: 14.sp,
                                        ),
                                        overflow: TextOverflow.ellipsis,),
                                    ),
                                    Text(
                                      "    • ${DateTimeUtils.convertToAgo(DateTime.fromMillisecondsSinceEpoch(controller.dataGroupMessage[index].message?.createdAt ??
                                          0)
                                          .toLocal())}",
                                      textAlign:
                                      TextAlign
                                          .left,
                                      style: TextStyle(
                                          color: const Color.fromRGBO(133, 133, 133, 1),
                                          fontSize:
                                          14.sp),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                        const Spacer(),
                        Visibility(
                            visible: controller.dataGroupMessage[index].isNotify == "TRUE",
                            child: const Icon(Icons.notifications_none_outlined,color: ColorUtils.PRIMARY_COLOR,size: 18,)),
                        Visibility(
                            visible: controller.dataGroupMessage[index].isNotify == "FALSE",
                            child: const Icon(Icons.notifications_off_outlined,color: Color.fromRGBO(133, 133, 133, 1),size: 18,)),
                        SizedBox(
                          width: 8.w,
                        ),
                        Icon(
                          Icons.circle,
                          color: controller.dataGroupMessage[index].countNotSeen! > 0
                              ? ColorUtils.PRIMARY_COLOR
                              : Colors.white,
                          size: 14,
                        )
                      ],
                    ),
                  )),
            );
          }),
    ):const Center(
      child: CircularProgressIndicator(color: ColorUtils.PRIMARY_COLOR,),
    ));
  }

  void goToMessageContent(index){
    Get.toNamed(Routes.messageContent,arguments: controller.dataGroupMessage[index].id);
    controller.seenMessage(index);
    Get.find<MessageController>().socketIo.seenMessage(controller.dataGroupMessage[index].id);
    Get.find<ListMessageUnReadController>().onInit();
  }

}