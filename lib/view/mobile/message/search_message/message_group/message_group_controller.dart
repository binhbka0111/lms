import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import '../../../../../commom/utils/app_utils.dart';
import '../../../../../commom/utils/color_utils.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/common/contacts.dart';
import '../../../../../data/model/res/message/group_message.dart';
import '../../../../../data/repository/chat/chat_repo.dart';
import '../../../role/parent/parent_home_controller.dart';
import '../search_message_controller.dart';


class MessageGroupController extends GetxController {
  var selectedPageIndex = 0.obs;
  FocusNode searchFocus = FocusNode();
  final ChatRepo _chatRepo = ChatRepo();
  var groupMessage = GroupMessage().obs;
  var isReady = false;
  var indexPage = 1.obs;
  var controllerLoadMoreSearch = ScrollController();

  var listGroupMessageSearch = <ItemContact>[].obs;
  var listGroupMessageLoadMoreSearch = <ItemContact>[].obs;

  @override
  void onInit() {
    getListUserSearch(0,20,"");
    controllerLoadMoreSearch = ScrollController()..addListener(_scrollListenerLoadMoreSearch);
    super.onInit();
  }



  void _scrollListenerLoadMoreSearch() {
    if (controllerLoadMoreSearch.position.pixels == controllerLoadMoreSearch.position.maxScrollExtent) {
      if(listGroupMessageLoadMoreSearch.isNotEmpty){
        indexPage.value++;
      }
      var userId = "";
      if(AppCache().userType == "PARENT"){
        userId = Get.find<ParentHomeController>().currentStudentProfile.value.id!;
      }else{
        userId = "";
      }
      _chatRepo.getListUserSearch("GROUP",indexPage.value,20,Get.find<SearchMessageController>().textEditingController.text,userId).then((value) {
        if (value.state == Status.SUCCESS) {
          listGroupMessageLoadMoreSearch.value = [];
          listGroupMessageLoadMoreSearch.value = value.object!.items!;
          listGroupMessageLoadMoreSearch.refresh();
          listGroupMessageSearch.addAll(listGroupMessageLoadMoreSearch);
          listGroupMessageSearch.refresh();
        }
      });
    }

  }


  getListUserSearch(page,size,fullName) {
    var userId = "";
    if(AppCache().userType == "PARENT"){
      userId = Get.find<ParentHomeController>().currentStudentProfile.value.id!;
    }else{
      userId = "";
    }
    _chatRepo.getListUserSearch("GROUP",page,size,fullName,userId).then((value) {
      if (value.state == Status.SUCCESS) {
        listGroupMessageSearch.value = value.object!.items!;
        Get.find<SearchMessageController>().countGroupMessage = listGroupMessageSearch.length;
        Get.find<SearchMessageController>().update();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lỗi",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }




}
