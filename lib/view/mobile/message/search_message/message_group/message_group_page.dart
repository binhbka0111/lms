import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/message/message_controller.dart';
import 'package:slova_lms/view/mobile/message/search_message/message_personal/message_personal_controller.dart';
import '../../../../../routes/app_pages.dart';
import '../../../home/home_controller.dart';
import '../search_message_controller.dart';
import 'message_group_controller.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';

class MessageGroupPage extends GetView<MessageGroupController> {
  MessageGroupPage({super.key});
  @override
  final controller = Get.put(MessageGroupController());
  final controllerPersonal = Get.find<MessagePersonalController>();
  @override
  Widget build(BuildContext context) {
    return Obx(() => Column(
      children: [
        Visibility(
          visible: Get.find<SearchMessageController>().textEditingController.text.trim() != "",
            child: Row(
          children: [
            SizedBox(
              width: 16.w,
            ),
            const Icon(Icons.search,color: Color.fromRGBO(133, 133, 133, 1),),
            SizedBox(
              width: 8.w,
            ),
            const Text("Tìm thấy ",style: TextStyle(color: Color.fromRGBO(133, 133, 133, 1)),),
            Text("${controller.listGroupMessageSearch.length}",style: const TextStyle(color: ColorUtils.PRIMARY_COLOR),),
            const Text(" tin nhắn",style: TextStyle(color: Color.fromRGBO(133, 133, 133, 1)),),
          ],
        )),
        SizedBox(
          height: 8.h,
        ),
        Expanded(
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: controller.listGroupMessageSearch.length,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> clickItemGroupSearch(index),StringConstant.FEATURE_MESSAGE_DETAIL);
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 16.w),
                    child: Column(
                      children: [Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 36.h,
                            height: 36.h,
                            child:
                            CacheNetWorkCustom(urlImage: '${ controller.listGroupMessageSearch[index].image}',)

                          ),
                          Padding(padding: EdgeInsets.only(left: 8.w)),
                          Text(
                            "${controller.listGroupMessageSearch[index].fullName?.trim()}",
                            style: TextStyle(
                                fontSize: 16.sp,
                                color: const Color.fromRGBO(0, 0, 0, 1)),
                            overflow: TextOverflow.ellipsis,
                          ),
                          if(controllerPersonal.isSharePost.value) const Spacer(),
                          if(controllerPersonal.isSharePost.value) InkWell(
                            onTap: (){
                              Get.find<MessageController>().socketIo.initSocket();
                              controllerPersonal.listUserSearch[index].id;
                              Get.find<MessageController>().socketIo.sendMessage(controllerPersonal.contentSharePost.value, controller.listGroupMessageSearch[index].id,[],"");
                              AppUtils.shared.showToast("Chia sẻ tin tức thành công");
                               Get.back();
                            },
                            child: Container(
                              padding: const EdgeInsets.all(5),
                              decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(6)),
                                  color: ColorUtils.COLOR_PRIMARY
                              ),
                              child: Text('Gửi',style: TextStyle(fontSize: 16.sp,color: Colors.white),),
                            ),
                          )
                        ],
                      ),const Divider()],
                    ),
                  ),
                );
              }),
        ),
      ],
    ));
  }


  clickItemGroupSearch(index){
    Get.toNamed(Routes.messageContent,
        arguments: controller.listGroupMessageSearch[index].id);
  }

}
