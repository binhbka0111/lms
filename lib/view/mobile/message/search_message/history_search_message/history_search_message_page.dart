import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';
import '../../../../../commom/utils/color_utils.dart';
import 'history_search_message_controller.dart';

class HistorySearchMessagePage extends GetView<HistorySearchMessageController> {
  @override
  final controller = Get.put(HistorySearchMessageController());

  HistorySearchMessagePage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        leading: const BackButton(
          color: Colors.white,
        ),
        title: const Text(
          "Chỉnh sửa lịch sử tìm kiếm",
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
        actions: [
          IconButton(
            icon: const Icon(
              Icons.home,
              color: Colors.white,
            ),
            onPressed: () {
              comeToHome();

            },
          )
        ],
      ),
      body: GetBuilder<HistorySearchMessageController>(builder: (controller) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 16.h,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16.w),
              child: Row(
                children:  [
                  const Text("Tìm kiếm gần đây"),
                  const Spacer(),
                  InkWell(
                    onTap: () {
                      Get.dialog(Center(
                        child: Wrap(
                            children: [
                              Container(
                                width: double
                                    .infinity,
                                decoration: BoxDecoration(
                                    color: Colors
                                        .transparent,
                                    borderRadius:
                                    BorderRadius.circular(16)),
                                child:
                                Stack(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                          top: 40.h,
                                          right: 50.w,
                                          left: 50.w),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(16)),
                                      child:
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Padding(padding: EdgeInsets.only(top: 32.h)),
                                          Container(
                                            margin: EdgeInsets.symmetric(horizontal: 16.w),
                                            child: Text(
                                              "Bạn có chắc muốn xóa hết lịch sử tìm kiếm?",
                                              style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontSize: 14.sp),
                                            ),
                                          ),
                                          Container(
                                              color: Colors.white,
                                              padding: EdgeInsets.all(16.h),
                                              child: SizedBox(
                                                width: double.infinity,
                                                height: 46,
                                                child: ElevatedButton(
                                                    style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                                                    onPressed: () {
                                                      controller.deleteAllUserRecentSearch();

                                                    },
                                                    child: Text(
                                                      'Xóa',
                                                      style: TextStyle(color: Colors.white, fontSize: 16.sp),
                                                    )),
                                              )),
                                          TextButton(
                                              onPressed: () {
                                                Get.back();
                                              },
                                              child: Text(
                                                "Hủy",
                                                style: TextStyle(color: Colors.red, fontSize: 16.sp),
                                              ))
                                        ],
                                      ),
                                    ),
                                    Container(
                                        alignment:
                                        Alignment.center,
                                        margin: const EdgeInsets.only(top: 10),
                                        height: 80,
                                        child: Image.asset("assets/images/image_app_logo.png"))
                                  ],
                                ),
                              ),
                            ]),
                      ));
                    },
                    child: const Text(
                      "Xóa tất cả",
                      style: TextStyle(color: ColorUtils.PRIMARY_COLOR),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 16.h,
            ),
            ListView.builder(
                shrinkWrap: true,
                itemCount: controller.listUserRecentSearch.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      controller.getIdPrivateChat(controller.listUserRecentSearch[index].id);
                    },
                    child: Container(
                      margin: EdgeInsets.only(bottom: 8.h,right: 16.w,left: 16.w),
                      child: Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 36.w,
                                height: 36.w,
                                child:
                                CacheNetWorkCustom(urlImage: '${controller.listUserRecentSearch[index].image}',)

                              ),
                              Padding(padding: EdgeInsets.only(left: 8.h)),
                              Text(
                                "${controller.listUserRecentSearch[index].fullName}",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: const Color.fromRGBO(0, 0, 0, 1)),
                                overflow: TextOverflow.ellipsis,
                              ),
                              const Spacer(),
                              InkWell(
                                onTap: () {
                                  Get.dialog(Center(
                                    child: Wrap(
                                        children: [
                                          Container(
                                            width: double
                                                .infinity,
                                            decoration: BoxDecoration(
                                                color: Colors
                                                    .transparent,
                                                borderRadius:
                                                BorderRadius.circular(16)),
                                            child:
                                            Stack(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.only(
                                                      top: 40.h,
                                                      right: 50.w,
                                                      left: 50.w),
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius: BorderRadius.circular(16)),
                                                  child:
                                                  Column(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Padding(padding: EdgeInsets.only(top: 32.h)),
                                                      Container(
                                                        margin: EdgeInsets.symmetric(horizontal: 16.w),
                                                        child: Text(
                                                          "Bạn có chắc muốn xóa người dùng này khỏi lịch sử tìm kiếm?",
                                                          style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontSize: 14.sp),
                                                        ),
                                                      ),
                                                      Container(
                                                          color: Colors.white,
                                                          padding: EdgeInsets.all(16.h),
                                                          child: SizedBox(
                                                            width: double.infinity,
                                                            height: 46,
                                                            child: ElevatedButton(
                                                                style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                                                                onPressed: () {
                                                                  controller.deleteUserRecentSearch(controller.listUserRecentSearch[index].id,index);
                                                                },
                                                                child: Text(
                                                                  'Xóa',
                                                                  style: TextStyle(color: Colors.white, fontSize: 16.sp),
                                                                )),
                                                          )),
                                                      TextButton(
                                                          onPressed: () {
                                                            Get.back();
                                                          },
                                                          child: Text(
                                                            "Hủy",
                                                            style: TextStyle(color: Colors.red, fontSize: 16.sp),
                                                          ))
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                    alignment:
                                                    Alignment.center,
                                                    margin: const EdgeInsets.only(top: 10),
                                                    height: 80,
                                                    child: Image.asset("assets/images/image_app_logo.png"))
                                              ],
                                            ),
                                          ),
                                        ]),
                                  ));
                                },
                                child: Container(
                                  padding: EdgeInsets.zero,
                                  decoration: const BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle,
                                  ),
                                  child: SvgPicture.asset("assets/images/icon_dangerous.svg",fit: BoxFit.cover,height: 18.w,width: 18.w,),
                                ),
                              ),
                            ],
                          ),
                          const Divider()
                        ],
                      ),
                    ),
                  );
                })
          ],
        );
      },),
    ));
  }
}
