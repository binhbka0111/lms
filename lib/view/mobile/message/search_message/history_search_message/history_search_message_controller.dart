import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../commom/utils/app_utils.dart';
import '../../../../../commom/utils/color_utils.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/common/contacts.dart';
import '../../../../../data/model/res/message/detail_group_chat.dart';
import '../../../../../data/repository/chat/chat_repo.dart';
import '../../../../../routes/app_pages.dart';
import '../message_personal/message_personal_controller.dart';


class HistorySearchMessageController extends GetxController {
  var selectedPageIndex = 0.obs;
  FocusNode searchFocus = FocusNode();
  final ChatRepo _chatRepo = ChatRepo();
  var isReady = false;
  var indexPage = 1.obs;
  var listUserRecentSearch = <ItemContact>[].obs;
  var itemDetailGroupChat = DetailGroupChat().obs;
  @override
  void onInit() {
    getListGroupChatRecentSearch();
    super.onInit();
  }


  getListGroupChatRecentSearch() {
    _chatRepo.getListGroupChatRecentSearch().then((value) {
      if (value.state == Status.SUCCESS) {
        listUserRecentSearch.value = [];
        listUserRecentSearch.value = value.object!;
        listUserRecentSearch.refresh();
        isReady = true;
        update();
      }
    });
  }








  addRecentSearch(userId) {
    _chatRepo.addRecentSearch(userId).then((value) {
    });
  }

  deleteAllUserRecentSearch() async{
    await _chatRepo.deleteAllUserRecentSearch().then((value) async{
      if (value.state == Status.SUCCESS){
        listUserRecentSearch.value = [];
        Get.find<MessagePersonalController>().listUserRecentSearch.value = [];
        update();
        Get.back();
      }
    });
  }
  deleteUserRecentSearch(userId,index) async{
    await _chatRepo.deleteUserRecentSearch(userId).then((value) async{
      if (value.state == Status.SUCCESS){
        listUserRecentSearch.removeAt(index);
        Get.find<MessagePersonalController>().listUserRecentSearch.removeAt(index);
        update();
        Get.back();
      }
    });
  }


  getIdPrivateChat(userId) async{
    await _chatRepo.getIdPrivateChat(userId).then((value) {
      if (value.state == Status.SUCCESS) {
        itemDetailGroupChat.value = value.object!;
        Get.toNamed(Routes.detailMessagePrivatePage,arguments: itemDetailGroupChat.value.id);
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lỗi",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


}
