import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:slova_lms/view/mobile/message/search_message/search_message_controller.dart';
import '../../../../commom/utils/color_utils.dart';
import 'message_group/message_group_controller.dart';
import 'message_group/message_group_page.dart';
import 'message_personal/message_personal_controller.dart';
import 'message_personal/message_personal_page.dart';


class SearchMessagePage extends GetView<SearchMessageController>{
  @override
  final controller = Get.put(SearchMessageController());

  SearchMessagePage({super.key});
  final isSharePost = Get.find<MessagePersonalController>().isSharePost.value;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
        child: DefaultTabController(
          length: 2,
          child: Scaffold(
            backgroundColor: Colors.white,
            body: GetBuilder<SearchMessageController>(builder: (controller) {
              return Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Card(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)),
                            color: const Color.fromRGBO(246, 246, 246, 1),
                            margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
                            child: TextFormField(
                              onTap: () {},
                              onChanged: (text) {
                                Get.find<MessageGroupController>().getListUserSearch(0, 20, text.trim());
                                if(text.trim() == ""){
                                  Get.find<MessagePersonalController>().listUserSearch.value = [];
                                }else{
                                  Get.find<MessagePersonalController>().getListUserSearch(0, 20, text.trim());
                                }

                              },
                              controller: controller.textEditingController,
                              decoration:  InputDecoration(
                                contentPadding: const EdgeInsets.symmetric(
                                    vertical: 7, horizontal: 16),
                                hintText: !isSharePost ? "Tìm tin nhắn...": "Tìm kiếm",
                                hintStyle: const TextStyle(
                                    fontSize: 14,
                                    color: Color.fromRGBO(177, 177, 177, 1)),
                                prefixIcon: const Icon(
                                  Icons.search,
                                  color: Color.fromRGBO(177, 177, 177, 1),
                                ),
                                border: InputBorder.none,
                              ),
                              textAlignVertical: TextAlignVertical.center,
                              cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                            )
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.fromLTRB(8, 6, 16, 0),
                        child: TextButton(
                            onPressed: () {
                              controller.textEditingController.text ="";
                              Get.find<MessagePersonalController>().listUserSearch.value = [];
                              controller.update();
                              Get.back();
                            },
                            child:  Text(
                              "Hủy",
                              style: TextStyle(
                                  color: const Color.fromRGBO(255, 69, 89, 1),
                                  fontSize: 12.sp),
                            )),
                      ),
                    ],
                  ),
                  TabBar(
                      physics: const NeverScrollableScrollPhysics(),
                      indicatorColor: const Color.fromRGBO(248, 129, 37, 1),
                      indicatorWeight: 2,
                      controller: controller.tabController,
                      labelColor: Colors.black,
                      unselectedLabelColor: Colors.grey,
                      onTap: (index) {
                        controller.selectedPageIndex = index;
                        controller.update();
                      },
                      tabs: [
                        Container(
                          height: 56,
                          padding: const EdgeInsets.only(top: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Cá nhân ",
                                style: TextStyle(
                                    color: controller.selectedPageIndex == 0
                                        ? const Color.fromRGBO(248, 129, 37, 1)
                                        : const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 14),
                              ),
                             Visibility(
                                 child:  Visibility(
                                 visible: controller.textEditingController.text.trim() != "",
                                 child: Container(
                                   decoration: BoxDecoration(
                                       borderRadius: BorderRadius.circular(16),
                                       color: controller.selectedPageIndex == 0
                                           ? ColorUtils.PRIMARY_COLOR
                                           : const Color.fromRGBO(177, 177, 177, 1)),
                                   padding: EdgeInsets.fromLTRB(8.w, 4.h, 8.w, 4.h),
                                   child: Text(
                                     "${controller.countPersonalMessage}",
                                     style: TextStyle(
                                         color: Colors.white, fontSize: 10.sp),
                                   ),
                                 )))
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 20),
                          height: 56,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Nhóm  ",
                                style: TextStyle(
                                    color: controller.selectedPageIndex == 1
                                        ? const Color.fromRGBO(248, 129, 37, 1)
                                        : const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 14),
                              ),
                              Visibility(
                                  child: Visibility(
                                      visible: controller.textEditingController.text.trim() != "",
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: controller.selectedPageIndex == 1
                                            ? ColorUtils.PRIMARY_COLOR
                                            : const Color.fromRGBO(177, 177, 177, 1)),
                                    padding: EdgeInsets.fromLTRB(8.w, 4.h, 8.w, 4.h),
                                    child: Text(
                                      "${controller.countGroupMessage}",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 10.sp),
                                    ),
                                  )))
                            ],
                          ),
                        ),
                      ]),
                  Expanded(child: TabBarView(
                      physics: const NeverScrollableScrollPhysics(),
                      children: [MessagePersonalPage(),MessageGroupPage()]))
                ],
              );
            },),
          ),
        ));
  }
}

