import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/view/mobile/message/message_controller.dart';
import '../../../../../commom/utils/color_utils.dart';
import '../../../../../commom/widget/cache_network_custom.dart';
import '../../../../../routes/app_pages.dart';
import '../../../home/home_controller.dart';
import '../search_message_controller.dart';
import 'message_personal_controller.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
class MessagePersonalPage extends GetView<MessagePersonalController> {
  MessagePersonalPage({super.key});
  @override
  final controller = Get.put(MessagePersonalController());
  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
      margin: EdgeInsets.symmetric(vertical: 16.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Visibility(
              visible: Get.find<SearchMessageController>().textEditingController.text.trim() != "",
              child: Row(
                children: [
                  SizedBox(
                    width: 16.w,
                  ),
                  const Icon(Icons.search,color: Color.fromRGBO(133, 133, 133, 1),),
                  SizedBox(
                    width: 8.w,
                  ),
                  const Text("Tìm thấy ",style: TextStyle(color: Color.fromRGBO(133, 133, 133, 1)),),
                  Text("${controller.listUserSearch.length}",style: const TextStyle(color: ColorUtils.PRIMARY_COLOR),),
                   Text(controller.isSharePost.value ?" tin nhắn": " người",style: const TextStyle(color: Color.fromRGBO(133, 133, 133, 1)),),
                ],
              )),
          Visibility(
              visible: Get.find<SearchMessageController>().textEditingController.text.trim() != "",
              child: const Divider()),
          Visibility(
              visible: Get.find<SearchMessageController>().textEditingController.text.trim() != "" || controller.listUserSearch.isNotEmpty,
              child: Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                controller: controller.controllerLoadMoreSearch,
                itemCount: controller.listUserSearch.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      if(!controller.isSharePost.value) {
                        checkClickFeature(Get.find<HomeController>().userGroupByApp,() => clickItemUserSearch(index),
                            StringConstant.FEATURE_MESSAGE_DETAIL);
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 16.w),
                      child: Column(
                        children: [Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 36.w,
                              height: 36.w,
                              child:
                              CacheNetWorkCustom(urlImage: '${ controller.listUserSearch[index].image}',)
                            ),
                            Padding(padding: EdgeInsets.only(left: 8.w)),
                            Text(
                              "${controller.listUserSearch[index].fullName?.trim()}",
                              style: TextStyle(
                                  fontSize: 16.sp,
                                  color: const Color.fromRGBO(0, 0, 0, 1)),
                              overflow: TextOverflow.ellipsis,
                            ),
                            if(controller.isSharePost.value) const Spacer(),
                            if(controller.isSharePost.value) InkWell(
                              onTap: () async{
                                Get.find<MessageController>().socketIo.initSocket();
                                await controller.getIdSharePost(controller.listUserSearch[index].id!);
                                controller.itemDetailGroupChat;
                                Get.find<MessageController>().socketIo.sendMessage(controller.contentSharePost.value, controller.itemDetailGroupChat.value.id,[],"");
                                AppUtils.shared.showToast("Chia sẻ tin tức thành công");
                                Get.back();
                              },
                              child: Container(
                                padding: const EdgeInsets.all(5),
                                decoration: const BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(6)),
                                  color: ColorUtils.COLOR_PRIMARY
                                ),
                                child: Text('Gửi',style: TextStyle(fontSize: 16.sp,color: Colors.white),),
                              ),
                            )
                          ],
                        ),const Divider()],
                      ),
                    ),
                  );
                }),
          )),
          Visibility(
              visible: Get.find<SearchMessageController>().textEditingController.text.trim() == "" && !controller.isSharePost.value,
              child: Visibility(
              visible: controller.listUserRecentSearch.isNotEmpty,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 16.w),
                child: Row(
                  children:  [
                    const Text("Tìm kiếm gần đây"),
                    const Spacer(),
                    InkWell(
                      onTap: () {
                        Get.toNamed(Routes.historySearchMessagePage);
                      },
                      child: const Text("Chỉnh sửa",style: TextStyle(color: ColorUtils.PRIMARY_COLOR),),
                    )
                  ],
                ),
              ))),
          SizedBox(
            height: 16.h,
          ),
          Visibility(
              visible: Get.find<SearchMessageController>().textEditingController.text.trim() == "" && !controller.isSharePost.value,
              child: Visibility(
              visible: controller.listUserRecentSearch.isNotEmpty,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 16.w),
                height: 86.h,
                child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: controller.listUserRecentSearch.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> controller.getIdPrivateChat(controller.listUserRecentSearch[index].id),StringConstant.FEATURE_MESSAGE_DETAIL);
                        },
                        child: Container(
                          margin: EdgeInsets.only(right: 16.w),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 36.w,
                                height: 36.w,
                                child:
                                CacheNetWorkCustom(urlImage: '${ controller.listUserRecentSearch[index].image}',)
                              ),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              Text(
                                "${controller.listUserRecentSearch[index].fullName?.trim()}",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: const Color.fromRGBO(0, 0, 0, 1)),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
              ))),
          SizedBox(
            height: 16.h,
          ),
          Visibility(
              visible: Get.find<SearchMessageController>().textEditingController.text.trim() == "" && !controller.isSharePost.value,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 16.w),
                child: const Text("Gợi ý"),
              )),
          SizedBox(
            height: 8.h,
          ),
          Visibility(
              visible: Get.find<SearchMessageController>().textEditingController.text.trim() == "" && !controller.isSharePost.value,
              child: Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: controller.listUserMessageSuggest.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      checkClickFeature(Get.find<HomeController>().userGroupByApp,()=>  controller.getIdPrivateChat(controller.listUserMessageSuggest[index].id),StringConstant.FEATURE_MESSAGE_DETAIL);
                    },
                    child: Container(
                      margin: const EdgeInsets.all(16),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 36.w,
                            height: 36.w,
                            child:
                            CacheNetWorkCustom(urlImage: '${ controller.listUserMessageSuggest[index].image }',)
                          ),
                          Padding(padding: EdgeInsets.only(left: 8.h)),
                          Text(
                            "${controller.listUserMessageSuggest[index].fullName?.trim()}",
                            style: TextStyle(
                                fontSize: 16.sp,
                                color: const Color.fromRGBO(0, 0, 0, 1)),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  );
                }),
          )),
        ],
      ),
    ));
  }

  clickItemUserSearch(index){
    controller.getIdPrivateChat(controller.listUserSearch[index].id);
    controller.addRecentSearch(controller.listUserSearch[index].id);
  }

}
