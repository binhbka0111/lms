import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../commom/app_cache.dart';
import '../../../../../commom/utils/app_utils.dart';
import '../../../../../commom/utils/color_utils.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/common/contacts.dart';
import '../../../../../data/model/res/message/detail_group_chat.dart';
import '../../../../../data/repository/chat/chat_repo.dart';
import '../../../../../routes/app_pages.dart';
import '../../../role/parent/parent_home_controller.dart';
import '../search_message_controller.dart';


class MessagePersonalController extends GetxController {
  var selectedPageIndex = 0.obs;
  FocusNode searchFocus = FocusNode();
  final ChatRepo _chatRepo = ChatRepo();
  var isReady = false;
  var indexPage = 1.obs;
  var controllerLoadMoreSearch = ScrollController();
  var listUserRecentSearch = <ItemContact>[].obs;
  var listUserMessageSuggest = <ItemContact>[].obs;
  var listUserSearch = <ItemContact>[].obs;
  var listUserLoadMoreSearch = <ItemContact>[].obs;
  var itemDetailGroupChat = DetailGroupChat().obs;
  RxBool isSharePost = false.obs;
  RxString contentSharePost = ''.obs;
  @override
  void onInit() {
    getListUserSearchSuggest();
    getListGroupChatRecentSearch();
    controllerLoadMoreSearch = ScrollController()..addListener(_scrollListenerLoadMoreSearch);
    super.onInit();
  }


  getListGroupChatRecentSearch() {
    _chatRepo.getListGroupChatRecentSearch().then((value) {
      if (value.state == Status.SUCCESS) {
        listUserRecentSearch.value = value.object!;
        isReady = true;
        update();
      }
    });
  }




  getListUserSearchSuggest() {
    _chatRepo.getListUserSearchSuggest().then((value) {
      if (value.state == Status.SUCCESS) {
        listUserMessageSuggest.value = value.object!;
      }
    });
  }


  void _scrollListenerLoadMoreSearch() {
    if (controllerLoadMoreSearch.position.pixels == controllerLoadMoreSearch.position.maxScrollExtent) {
      if(listUserLoadMoreSearch.isNotEmpty){
        indexPage.value++;
      }
      var userId = "";
      if(AppCache().userType == "PARENT"){
        userId = Get.find<ParentHomeController>().currentStudentProfile.value.id!;
      }else{
        userId = "";
      }
      _chatRepo.getListUserSearch("PRIVATE",indexPage.value,20,Get.find<SearchMessageController>().textEditingController.text,userId).then((value) {
        if (value.state == Status.SUCCESS) {
          listUserLoadMoreSearch.value = [];
          listUserLoadMoreSearch.value = value.object!.items!;
          listUserLoadMoreSearch.refresh();
          listUserSearch.addAll(listUserLoadMoreSearch);
          listUserSearch.refresh();
        }
      });
    }

  }


  getListUserSearch(page,size,fullName) {
    var userId = "";
    if(AppCache().userType == "PARENT"){
      userId = Get.find<ParentHomeController>().currentStudentProfile.value.id!;
    }else{
      userId = "";
    }
    _chatRepo.getListUserSearch("PRIVATE",page,size,fullName,userId).then((value) {
      if (value.state == Status.SUCCESS) {
        listUserSearch.value = value.object!.items!;
        Get.find<SearchMessageController>().countPersonalMessage = listUserSearch.length;
        Get.find<SearchMessageController>().update();
      }
    });
  }

  addRecentSearch(userId) {
    _chatRepo.addRecentSearch(userId).then((value) {
    });
  }


  getIdPrivateChat(userId) async{
    await _chatRepo.getIdPrivateChat(userId).then((value) {
      if (value.state == Status.SUCCESS) {
        itemDetailGroupChat.value = value.object!;
        Get.toNamed(Routes.detailMessagePrivatePage,arguments: itemDetailGroupChat.value.id);
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lỗi",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

  getIdSharePost(String userId) async{
    await _chatRepo.getIdPrivateChat(userId).then((value) {
      if (value.state == Status.SUCCESS) {
        itemDetailGroupChat.value = value.object!;
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lỗi",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


}
