import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../commom/utils/app_utils.dart';
import '../../../commom/utils/color_utils.dart';
import '../../../data/base_service/api_response.dart';
import '../../../data/base_service/socket_io.dart';
import '../../../data/model/res/message/group_message.dart';
import '../../../data/repository/chat/chat_repo.dart';



class MessageController extends GetxController with GetSingleTickerProviderStateMixin{
  var selectedPageIndex = 0.obs;
  FocusNode searchFocus = FocusNode();
  final ChatRepo _chatRepo = ChatRepo();
  var groupMessage = GroupMessage().obs;
  var isReady = false.obs;
  final SocketIo socketIo = SocketIo();
  TabController? tabController;
  @override
  void onInit() {
    tabController = TabController(length: 2, vsync: this);
    getListGroupChat("ALL",0,20);
    super.onInit();
  }


  getListGroupChat(type,page,size) {
    _chatRepo.getListGroupChat(type,page,size).then((value) {
      if (value.state == Status.SUCCESS) {
        groupMessage.value = value.object!;
        groupMessage.refresh();
      } else {
        AppUtils.shared.showToast(value.message ?? "Lỗi",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
    isReady.value = true;
  }


  @override
  dispose() {
    tabController?.dispose();
    super.dispose();
  }
}
