import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pinput/pinput.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/repository/account/auth_repo.dart';
import 'package:slova_lms/routes/app_pages.dart';
import '../../../../commom/utils/global.dart';
// ignore_for_file: constant_identifier_names
class VerifyCodeController extends GetxController {
  final AuthRepo _loginRepo = AuthRepo();

  static const NUM_BOXES = 4;
  final controllers = <TextEditingController>[];

  // final boxFocusNodes = <FocusNode>[];
  List<String> characters = <String>[];
  List<Widget> pinWidget = <Widget>[];
  RxInt indexOTP = 0.obs;
  RxString userName = "".obs;
  RxBool ready = false.obs;
  RxBool enableSubmit = false.obs;
  String transId = "";
  final FocusScopeNode node = FocusScopeNode();
  var pinController = TextEditingController().obs;
  @override
  void onInit() async {
    super.onInit();
    var tmpUserName = Get.arguments[0];
    var tmpTransId = Get.arguments[1];
    if (tmpUserName != null && tmpUserName.isNotEmpty) {
      userName.value = tmpUserName;
    }
    if (tmpTransId != null && tmpTransId.isNotEmpty) {
      transId = tmpTransId;
    }
    initializing();
  }

  initializing() {
    characters = List.generate(NUM_BOXES, (index) {
      final controller = TextEditingController();
      controllers.add(controller);
      pinWidget.add(pinNumberWidget(index, controller));
      showPinPut();
      return "";
    });
    ready.value = true;
  }
  final defaultPinTheme = PinTheme(
    width: 56,
    height: 56,
    textStyle: TextStyle(fontSize: 16.sp, color: const Color.fromRGBO(26, 26, 26, 1), fontWeight: FontWeight.w500),
    decoration: BoxDecoration(
      border: Border.all(color: const Color.fromRGBO(133, 133, 133, 1)),
      borderRadius: BorderRadius.circular(8.r),
    ),
  );

  pinNumberWidget(currentIndex, controller) {
    return Obx(() =>
        Container(
          alignment: Alignment.center,
          width: 50.w,
          height: 50.w,
          margin: const EdgeInsets.only(left: 16),
          padding: const EdgeInsets.all(12),
          decoration: BoxDecoration(
            border: Border.all(
                color: indexOTP.value == currentIndex
                    ? ColorUtils.PRIMARY_COLOR
                    : const Color.fromRGBO(133, 133, 133, 1)),
            borderRadius: BorderRadius.circular(10),
          ),
          child: AbsorbPointer(
            child:
            TextFormField(
              enableInteractiveSelection: false,
              textInputAction: (currentIndex == (NUM_BOXES - 1))
                  ? TextInputAction.done
                  : TextInputAction.next,
              // move to the next field
              onEditingComplete: node.nextFocus,
              onChanged: (value) {
                _onTextChanged(currentIndex, value);
              },
              controller: controller,
              autofocus: true,
              decoration: InputDecoration(
                  border: indexOTP.value == currentIndex
                      ? const UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Color.fromRGBO(51, 51, 51, 1)))
                      : InputBorder.none,
                  focusedBorder: const UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: ColorUtils.PRIMARY_COLOR))),
              cursorColor: ColorUtils.PRIMARY_COLOR,
              textAlign: TextAlign.center,
              keyboardType: TextInputType.number,
              inputFormatters: [
                LengthLimitingTextInputFormatter(1),
                FilteringTextInputFormatter.digitsOnly,
              ],
            ),
          ),
        ));
  }

  void confirmOTP() async {
    dismissKeyboard();
    _verityResponse(transId, pinController.value.text.toString());
  }

  _verityResponse(transId, otp) async {
    dismissKeyboard();
    AppUtils.shared.showLoading();
    _loginRepo.verityRespone(transId, otp).then((value) {
      if (value.state == Status.SUCCESS) {
        toResetPass();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Mã xác nhận không hợp lệ", value.message ?? "");
      }
    });
  }

  void _onTextChanged(int index, String value) {
    if (value.isEmpty) {
      // request focus for the previous "box"
      if (index > 0) {
        indexOTP.value--;
        node.previousFocus();
        enableSubmit.value = false;
      }
      return;
    } else {
      characters[index] = value;
      // request focus for the next "box"
      if (index < NUM_BOXES - 1) {
        indexOTP.value++;
        node.nextFocus();
      } else {
        enableSubmit.value = true;
      }
    }
  }

  void toResetPass() {
    Get.toNamed(Routes.resetPass, arguments: [transId]);
    AppUtils.shared.hideLoading();
  }

  getPinView() {
    return pinWidget;
  }

  showPinPut(){
    return Pinput(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      defaultPinTheme: defaultPinTheme,
      focusedPinTheme: defaultPinTheme.copyDecorationWith(
        border: Border.all(color: ColorUtils.PRIMARY_COLOR),
        borderRadius: BorderRadius.circular(8),

      ),
      submittedPinTheme: defaultPinTheme.copyWith(
          decoration: defaultPinTheme.decoration?.copyWith(
            color: const Color.fromRGBO(255, 255, 255, 1),
          )
      ),
      controller: pinController.value,
      onChanged: (index) => {
        if(pinController.value.length == 4){
          enableSubmit.value= true
        }else{
          enableSubmit.value= false
    }
      },
      autofocus: true,
      cursor: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 9),
            width: 22,
            height: 1,
            color: ColorUtils.PRIMARY_COLOR,
          ),
        ],
      ),
      androidSmsAutofillMethod:  AndroidSmsAutofillMethod.smsRetrieverApi,
      isCursorAnimationEnabled: true,
      pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
      showCursor: true,
    );
  }
}
