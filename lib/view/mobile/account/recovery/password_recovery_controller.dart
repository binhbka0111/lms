import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/global.dart';
import 'package:slova_lms/commom/widget/text_field_custom.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/repository/account/auth_repo.dart';
import 'package:slova_lms/routes/app_pages.dart';

class PasswordRecoveryController extends GetxController {
  final AuthRepo _loginRepo = AuthRepo();

  TextEditingController controllerUserName = TextEditingController();
  Rx<StateType> stateInputUser = StateType.DEFAULT.obs;
  RxBool enableSubmit = false.obs;

  @override
  void onInit() async {
    super.onInit();
    controllerUserName.addListener(() {
      if (controllerUserName.text.trim().isEmpty) {
        stateInputUser.value = StateType.DEFAULT;
        enableSubmit.value = false;
      } else {
        stateInputUser.value = StateType.SUCCESS;
        enableSubmit.value = true;
      }
    });
  }

  void submitOTP() async {
    dismissKeyboard();
    stateInputUser.value = StateType.DEFAULT;
    if (controllerUserName.text.trim().isEmpty) {
      stateInputUser.value = StateType.ERROR;
    }
    requestOTP(controllerUserName.text);
  }

  requestOTP(username) async {
    dismissKeyboard();
    AppUtils.shared.showLoading();
    _loginRepo.requestOTP(username).then((value) {
      if (value.state == Status.SUCCESS) {
        var transId = value.object?.transId ?? "";
        stateInputUser.value = StateType.SUCCESS;
        AppUtils.shared.showToast("Đã gửi mã OTP");
        toConfirmCode(transId);
      } else {
        AppUtils.shared.snackbarError(
            "Tài khoản không hợp lệ", value.message ?? "");
      }
    });
  }

  void toConfirmCode(String transId) {
    Get.toNamed(Routes.retrievingCode, arguments: [controllerUserName.text, transId]);
    AppUtils.shared.hideLoading();
  }
}
