import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/global.dart';
import 'package:slova_lms/commom/widget/LabeledCheckbox.dart';
import 'package:slova_lms/commom/widget/text_field_custom.dart';
import 'package:slova_lms/view/mobile/account/login/login_controller.dart';

class LoginPage extends StatelessWidget {
  final controller = Get.put(LoginController());

  LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: GestureDetector(
          onTap: () {
            dismissKeyboard();
          },
          child: SafeArea(
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.transparent,
                elevation: 0,
                leading: IconButton(
                    onPressed: () {
                      Get.back();
                    },
                    icon: const Icon(
                      Icons.arrow_back_outlined,
                      color: Colors.black,
                    )),
              ),
              body:
              SingleChildScrollView(
                child: Obx(() => Container(
                  margin: const EdgeInsets.all(10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            alignment: Alignment.center,
                            height: 44,
                            child: const Text(
                              'Đăng Nhập',
                              style: TextStyle(
                                  fontSize: 32,
                                  color: Color.fromRGBO(26, 26, 26, 1),
                                  fontFamily: 'static/Inter-Medium.ttf',
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                          Container(
                              alignment: Alignment.center,
                              margin: const EdgeInsets.only(top: 8),
                              height: 15,
                              child: const Text(
                                'Vui lòng nhập tài khoản và mật khẩu ở bên dưới',
                                style: TextStyle(
                                    color: Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12,
                                    fontFamily: 'static/Inter-Regular.ttf',
                                    fontWeight: FontWeight.w400),
                              )),
                          Container(
                            margin: const EdgeInsets.only(
                                top: 32, left: 0, right: 0, bottom: 0),
                            child: LabelOutSideTextFormField(
                              state: controller.stateInputUser.value,
                              hintText: "Nhập Email hoặc số điện thoại",
                              controller: controller.controllerUserName,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              autofocus: false,
                              functionOnChange: ()=>setDefaultState(),
                              enable: true,
                              showHelperText: true,
                              iconPrefix: "assets/images/icon_username.png",
                              iconSuffix: getIconSuffix(
                                  controller.stateInputUser.value),
                              helperText: "",
                              obscureText: false,
                              errorText: controller.submitErrorUserName(),
                              labelText: "Email hoặc số điện thoại ",
                              showIconHideShow: false,
                              focusNode: controller.focusUserName,
                              functionOnTap: ()=>onTapUserName(),
                              functionOnSubmit: ()=> onSubmitUserName(),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.zero,
                            child: Column(
                              children: [
                                LabelOutSideTextFormField(
                                  state: controller.stateInputPassword.value,
                                  hintText: "Nhập mật khẩu",
                                  controller: controller.controllerPassword,
                                  keyboardType: TextInputType.text,
                                  textInputAction: TextInputAction.done,
                                  autofocus: false,
                                  functionOnChange: ()=>setDefaultState(),
                                  enable: true,
                                  showHelperText: true,
                                  iconPrefix:
                                  "assets/images/icon_password.png",
                                  iconSuffix: getIconSuffix(
                                      controller.stateInputPassword.value),
                                  helperText: "Mật khẩu có ít nhất 8 kí tự",
                                  obscureText: controller.obscureText.value,
                                  errorText: controller.submitErrorPassword(),
                                  labelText: "Mật khẩu ",
                                  showIconHideShow: true,
                                  focusNode: controller.focusUserPassword,
                                  functionOnTap: ()=>onTapPassword(),
                                  functionOnSubmit: ()=> onSubmitPassword(),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin:
                            const EdgeInsets.only(left: 10, right: 10),
                            child: Row(
                              children: [
                                Expanded(
                                    child: LabeledCheckbox(
                                      activeColor:
                                      ColorUtils.PRIMARY_COLOR,
                                      value: controller.isChecked.value,
                                      onTap: (value) {
                                        if (value != null) {
                                          controller.isChecked.value = value;
                                        }
                                      },
                                      label: 'Nhớ mật khẩu',
                                      contentPadding:
                                      EdgeInsets.symmetric(horizontal: 2.w),
                                      fontSize: 14.sp,
                                    )),
                                TextButton(
                                    onPressed: () {
                                      controller.toRecoveryPassword();
                                    },
                                    child: const Text(
                                      'Quên mật khẩu?',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color:
                                          ColorUtils.PRIMARY_COLOR,
                                          fontFamily:
                                          'static/Inter-Regular.ttf',
                                          fontWeight: FontWeight.w400),
                                    ))
                              ],
                            ),
                          )
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 16.w),
                          child: Center(
                            child: SizedBox(
                              width: double.infinity,
                              height: 46,
                              child: ElevatedButton(
                                  onPressed: () {
                                    controller.submitLogin();
                                  },
                                  style: ElevatedButton.styleFrom(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(6)),
                                      backgroundColor:
                                      ColorUtils.PRIMARY_COLOR),
                                  child: const Text(
                                    'Đăng Nhập',
                                    style: TextStyle(fontSize: 16),
                                  )),
                            ),
                          )),
                    ],
                  ),
                )),
              ),
            ),
          ),
        ),
        onWillPop: () async {
          Get.back();
          return false;
        });
  }


  setDefaultState(){
    controller.stateInputUser.value = StateType.DEFAULT;
    controller.stateInputPassword.value = StateType.DEFAULT;
  }

  onTapUserName(){
    controller.stateInputUser.value = StateType.DEFAULT;
    controller.stateInputPassword.value = StateType.DEFAULT;
    controller.focusUserPassword.unfocus();
  }
  onTapPassword(){
    controller.stateInputUser.value = StateType.DEFAULT;
    controller.stateInputPassword.value = StateType.DEFAULT;
    controller.focusUserName.unfocus();
  }

  onSubmitUserName(){
    controller.focusUserPassword.requestFocus();
  }
  onSubmitPassword(){
    controller.focusUserPassword.unfocus();
    controller.submitLogin();
  }
}
