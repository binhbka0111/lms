import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/global.dart';
import 'package:slova_lms/commom/utils/textstyle.dart';

class DialogNotifyAuthen extends StatelessWidget {
  const DialogNotifyAuthen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          dismissKeyboard();
        },
        child: Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(7),
          ),
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(7),
              boxShadow: const [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 7,
                  offset: Offset(0.0, 10.0),
                ),
              ],
            ),
            child: SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 16.h),
                    Icon(Icons.error_rounded,
                        color: ColorUtils.BG_BUTTON_SELL, size: 40.sp),
                    SizedBox(height: 16.h),
                    Padding(
                      padding: EdgeInsets.only(left: 8.w, right: 8.w),
                      child: Text(
                        "please_login".tr,
                        textAlign: TextAlign.center,
                        style: TextStyleUtils.sizeText15Weight700()
                            ?.copyWith(color: ColorUtils.COLOR_BLACK),
                      ),
                    ),
                    SizedBox(height: 21.h),
                    InkWell(
                      onTap: () {
                        AppUtils.shared.popView(context);
                      },
                      child: Container(
                        alignment: Alignment.center,
                        height: 30.h,
                        width: 104.w,
                        decoration: BoxDecoration(
                            color: ColorUtils.BG_COLOR,
                            borderRadius: BorderRadius.circular(8)),
                        child: Text("close".tr,
                            textAlign: TextAlign.center,
                            style: TextStyleUtils.sizeText13Weight500()
                                ?.copyWith(color: ColorUtils.BG_BASE1)),
                      ),
                    ),
                    SizedBox(
                      height: 21.h,
                    ),
                  ]),
            ),
          ),
        ));
  }
}
