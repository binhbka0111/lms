import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/global.dart';
import 'package:slova_lms/commom/utils/validate_utils.dart';
import 'package:slova_lms/commom/widget/text_field_custom.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/repository/account/auth_repo.dart';
import 'package:slova_lms/routes/app_pages.dart';

class LoginController extends GetxController {
  final AuthRepo _loginRepo = AuthRepo();

  TextEditingController controllerUserName = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();
  FocusNode focusUserName = FocusNode();
  FocusNode focusUserPassword = FocusNode();
  RxBool obscureText = true.obs;
  Rx<StateType> stateInputUser = StateType.DEFAULT.obs;
  Rx<StateType> stateInputPassword = StateType.DEFAULT.obs;
  RxBool isChecked = false.obs;

  String language = 'vi';
  var supportState = (_SupportState.unknown).obs;
  int statusCode = 0;

  @override
  void onInit() async {
    getInfoLoginSaved();
    super.onInit();
  }

  void toggle() {
    obscureText.value = !obscureText.value;
    update();
  }

  getInfoLoginSaved() async {
    isChecked.value = AppCache().isSaveInfoLogin;
    if (isChecked.value) {
      String? usernameSaved = await AppCache().username;
      String? passwordSaved = await AppCache().password;
      controllerUserName.text = usernameSaved ?? "";
      controllerPassword.text = passwordSaved ?? "";
    } else {
      controllerUserName.text = "";
      controllerPassword.text = "";
    }
  }

  void saveInfoLogin() {
    if (isChecked.value) {
      AppCache().saveInfoLogin(isChecked.value);
      AppCache().saveLogin(controllerUserName.text, controllerPassword.text);
    } else {
      if (!AppCache().isSaveLoginAuthen) {}
      AppCache().saveInfoLogin(isChecked.value);
    }
  }

  submitErrorUserName() {
    if (controllerUserName.value.text.isEmpty) {
      return "(Vui lòng nhập Email/SĐT)";
    } else {
      return "(Sai Email/ Số điện thoại)";
    }
  }

  submitErrorPassword() {
    if (controllerPassword.value.text.isEmpty) {
      return "(Vui lòng nhập mật khẩu)";
    } else {
      return "(Mật khẩu không chính xác)";
    }
  }

  void submitLogin() {
    dismissKeyboard();
    stateInputUser.value = StateType.DEFAULT;
    stateInputPassword.value = StateType.DEFAULT;

    if (controllerUserName.text.trim().isEmpty) {
      //User name empty
      stateInputUser.value = StateType.ERROR;
      return;
    }
    if (controllerUserName.text.trim() == "") {
      //User name empty
      stateInputUser.value = StateType.ERROR;
      return;
    } else {
      stateInputUser.value = StateType.DEFAULT;
    }
    if (controllerPassword.text.trim().isEmpty) {
      //Password empty
      stateInputPassword.value = StateType.ERROR;
      return;
    }
    if (ValidateUtils.isPassword(controllerPassword.text)) {
      //Not valid password
      stateInputPassword.value = StateType.ERROR;
      return;
    } else {
      stateInputPassword.value = StateType.DEFAULT;
    }
    _requestToken(controllerUserName.text, controllerPassword.text, isChecked.value);

  }

  _requestToken(username, password, isCheck) async {
    var fcmKey = AppCache().fcmToken ?? "";
    if (fcmKey.isEmpty) {
      await getAndSaveFcmToken();
      fcmKey = await AppCache().getFcmToken;
    }
    _loginRepo.requestToken(username, password, isCheck, fcmKey).then((value) {
      if (value.state == Status.SUCCESS) {
        var userType = value.object?.userType ?? "";
        AppCache().setUserType(userType);
        stateInputUser.value = StateType.SUCCESS;
        stateInputPassword.value = StateType.SUCCESS;
        AppUtils.shared.showToast("Đăng nhập thành công!");
        saveInfoLogin();
        pushToHome();
      } else {
        if(statusCode == 400){
          stateInputUser.value = StateType.ERROR;
          stateInputPassword.value = StateType.ERROR;
        }
        AppUtils.shared.hideLoading();
        AppUtils.shared
            .snackbarError("Đăng nhập thất bại", value.message ?? "");
      }
    });
  }

  pushToHome() {
    Get.offAllNamed(Routes.home);
  }

  void toRecoveryPassword() {
    Get.toNamed(Routes.recoveryPass);
  }
}

enum _SupportState {
  unknown,
  supported,
  unsupported,
}

Future<void> getAndSaveFcmToken() async {
  AppCache appCache = AppCache();
  try {
    String fcmTokenCache = await appCache.getFcmToken;
    final fcmTokenFirebase = await FirebaseMessaging.instance.getToken();
    if (fcmTokenFirebase != null) {
      if (fcmTokenCache == fcmTokenFirebase) {
      } else {
        await appCache.saveFcmToken(fcmToken: fcmTokenFirebase);
      }
    } else {
      await getAndSaveFcmToken();
    }
  } catch (e) {
    await getAndSaveFcmToken();
  }
}
