import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/global.dart';
import 'package:slova_lms/commom/widget/text_field_custom.dart';
import 'package:slova_lms/view/mobile/account/changePassword/change_pass_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class ChangePassPage extends GetWidget<ChangePassController> {
  final controller = Get.put(ChangePassController());

  ChangePassPage({super.key});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        dismissKeyboard();
      },
      child:
      SafeArea(
        child:
        Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: const Icon(
                    Icons.arrow_back_outlined,
                    color: Colors.black,
                  )),
            ),
            body: SingleChildScrollView(
              reverse: true,
              physics: const BouncingScrollPhysics(),
              child:     Obx(() => Center(
                  child: Container(
                    margin: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Column(
                          children: [
                            Container(
                              alignment: Alignment.center,
                              width: 300,
                              height: 44,
                              child: const Text(
                                'Đổi Mật Khẩu',
                                style: TextStyle(
                                    fontSize: 32,
                                    color: Color.fromRGBO(26, 26, 26, 1),
                                    fontFamily: 'static/Inter-Medium.ttf',
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(top: 8.h),
                                width: 280,
                                height: 15,
                                child: const Text(
                                  'Mật khẩu mới phải khác so với mật khẩu cũ',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color.fromRGBO(133, 133, 133, 1),
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      fontFamily: 'static/Inter-Regular.ttf'),
                                ))
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 32.h),
                          child: Column(
                            children: [
                              LabelOutSideTextFormField(
                                state: controller.stateInputOldPass.value,
                                hintText: "Nhập Mật Khẩu Cũ",
                                controller: controller.controllerOldPassword,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                autofocus: false,
                                validation: (textToValidate) {
                                  // return getTempIFSCValidation(textToValidate);
                                },
                                enable: true,
                                showHelperText: true,
                                iconPrefix: "assets/images/icon_password.png",
                                iconSuffix:
                                getIconSuffix(controller.stateInputOldPass.value),
                                helperText: "",
                                obscureText: true,
                                errorText: controller.checvalidateOldpass()??"",
                                labelText: "Mật Khẩu Cũ",
                                showIconHideShow: true, focusNode: FocusNode(),
                              ),
                            ],
                          ),
                        ),
                        Column(
                          children: [
                            LabelOutSideTextFormField(
                              state: controller.stateInputNewPass.value,
                              hintText: "Nhập Mật Khẩu Mới",
                              controller: controller.controllerNewPassword,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              autofocus: false,
                              validation: (textToValidate) {
                                // return getTempIFSCValidation(textToValidate);
                              },
                              enable: true,
                              showHelperText: true,
                              iconPrefix: "assets/images/icon_password.png",
                              iconSuffix:
                              getIconSuffix(controller.stateInputNewPass.value),
                              helperText: "",
                              obscureText: true,
                              errorText: controller.checkValidateNewPass(),
                              labelText: "Mật Khẩu Mới",
                              showIconHideShow: true, focusNode:  FocusNode(),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            LabelOutSideTextFormField(
                              state: controller.stateInputRepass.value,
                              hintText: "Nhập Lại Mật Khẩu",
                              controller: controller.controllerRePassword,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              autofocus: false,
                              validation: (textToValidate) {
                                // return getTempIFSCValidation(textToValidate);
                              },
                              enable: true,
                              showHelperText: true,
                              iconPrefix: "assets/images/icon_password.png",
                              iconSuffix:
                              getIconSuffix(controller.stateInputRepass.value),
                              helperText: "Mật khẩu có ít nhất 8 kí tự",
                              obscureText: true,
                              errorText: controller.checkValidateRepass() ?? "Mật khẩu không hợp lệ",
                              labelText: "Nhập Lại Mật Khẩu",
                              showIconHideShow: true, focusNode: FocusNode(),
                            ),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 95.h)),
                        SizedBox(
                          width: 360,
                          height: 46,
                          child: ElevatedButton(
                              onPressed: (controller.enableSubmit.value)
                                  ? () {
                                controller.submitUpdatePass();
                              }
                                  : null,
                              style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(6)),
                                  backgroundColor: ColorUtils.PRIMARY_COLOR),
                              child: const Text(
                                'Xác Nhận',
                                style: TextStyle(fontSize: 16),
                              )),
                        ),
                      ],
                    ),
                  ))),
            )
        )
      ),
    );
  }
}
