import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/global.dart';
import 'package:slova_lms/commom/widget/text_field_custom.dart';
import 'package:slova_lms/view/mobile/account/resetPass/reset_pass_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../commom/utils/app_utils.dart';

class ResetPassPage extends StatelessWidget {
  final controller = Get.put(ResetPassController());

  ResetPassPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        dismissKeyboard();
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: const Icon(
                  Icons.arrow_back_outlined,
                  color: Colors.black,
                )),
          ),
          body: Obx(() => SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              reverse: true,
              child: Container(
                margin: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          width: 300,
                          height: 44,
                          child: const Text(
                            'Đặt Lại Mật Khẩu',
                            style: TextStyle(
                                fontSize: 32,
                                color: Color.fromRGBO(26, 26, 26, 1),
                                fontFamily: 'static/Inter-Medium.ttf',
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 8.h),
                            width: 280,
                            height: 15,
                            child: const Text(
                              'Mật khẩu mới phải khác so với mật khẩu cũ',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: 'static/Inter-Regular.ttf'),
                            ))
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 32.h),
                      child: Column(
                        children: [
                          LabelOutSideTextFormField(
                            focusNode: controller.focusPassword,
                            state: controller.statePassword.value,
                            hintText: "Nhập Mật Khẩu Mới",
                            controller: controller.controllerPassword.value,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            autofocus: false,
                            validation: (textToValidate) {
                              // return getTempIFSCValidation(textToValidate);
                            },
                            enable: true,
                            showHelperText: true,
                            iconPrefix: "assets/images/icon_password.png",
                            iconSuffix:
                                getIconSuffix(controller.statePassword.value),
                            helperText: "",
                            obscureText: true,
                            errorText: controller.checkValidateNewPassword(),
                            labelText: "Mật Khẩu Mới",
                            showIconHideShow: true,
                            functionOnSubmit: ()=>onSubmitPassWord(),
                            functionOnTap: ()=>onTapPassword(),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: [
                        LabelOutSideTextFormField(
                          state: controller.stateRePassword.value,
                          hintText: "Nhập Lại Mật Khẩu",
                          controller: controller.controllerRePassword.value,
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          autofocus: false,
                          validation: (textToValidate) {
                            // return getTempIFSCValidation(textToValidate);
                          },
                          enable: true,
                          showHelperText: true,
                          iconPrefix: "assets/images/icon_password.png",
                          iconSuffix:
                              getIconSuffix(controller.stateRePassword.value),
                          helperText: "Mật khẩu có ít nhất 8 kí tự",
                          obscureText: true,
                          errorText: controller.checkValidateRePassword() ?? "",
                          labelText: "Nhập Lại Mật Khẩu",
                          showIconHideShow: true,
                          focusNode: controller.focusRePassword,
                          functionOnTap: ()=>onTapRePassword(),
                          functionOnSubmit: ()=>onSubmitRePassWord(),
                        ),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 95.h)),
                    SizedBox(
                      width: 360,
                      height: 46,
                      child: ElevatedButton(
                          onPressed: (controller.enableSubmit.value)
                              ? () {
                                  if (RegExp(r"\s").hasMatch(controller.controllerPassword.value.text)) {
                                    if(RegExp(r"\s").hasMatch(controller.controllerRePassword.value.text)){
                                      AppUtils.shared.snackbarError(
                                          "Mật khẩu nhập lại không được  chứa dấu cách","");
                                      controller.statePassword.value = StateType.ERROR;
                                      controller.stateRePassword.value = StateType.ERROR;
                                    }else{
                                      controller.statePassword.value = StateType.ERROR;
                                      controller.stateRePassword.value = StateType.DEFAULT;
                                      AppUtils.shared.snackbarError(
                                          "Mật khẩu nhập lại không được  chứa dấu cách","");
                                    }
                                  }else{
                                    if(RegExp(r"\s").hasMatch(controller.controllerRePassword.value.text)){
                                      controller.stateRePassword.value = StateType.ERROR;
                                      controller.statePassword.value = StateType.DEFAULT;
                                      AppUtils.shared.snackbarError(
                                          "Mật khẩu nhập lại không được  chứa dấu cách","");
                                    }else{
                                      controller.submitChangePass();
                                    }
                                  }

                                  controller.stateRePassword.refresh();
                                  controller.statePassword.refresh();
                                  controller.controllerRePassword.refresh();
                                  controller.controllerPassword.refresh();
                                }
                              : null,
                          style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6)),
                              backgroundColor:
                              ColorUtils.PRIMARY_COLOR),
                          child: const Text(
                            'Xác Nhận',
                            style: TextStyle(fontSize: 16),
                          )),
                    ),
                  ],
                ),
              ))),
        ),
      ),
    );
  }


  setDefaultState(){
    controller.statePassword.value = StateType.DEFAULT;
    controller.stateRePassword.value = StateType.DEFAULT;
  }

  onSubmitPassWord(){
    controller.focusRePassword.requestFocus();
    setDefaultState();
  }

  onSubmitRePassWord(){
    controller.focusRePassword.unfocus();
    setDefaultState();
  }



  onTapPassword(){
    setDefaultState();
  }
  onTapRePassword(){
    setDefaultState();
  }
}
