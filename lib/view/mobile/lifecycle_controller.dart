import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'home/home_controller.dart';
import 'notification/notification_controller.dart';

class LifeCycleController extends SuperController {
  @override
  void onDetached() {
    getCountNotify();
    if(AppCache().isSaveInfoLogin == false){
      FlutterAppBadger.removeBadge();
    }
    AppUtils.shared.printLog("App state : onDetached");
  }

  @override
  void onInactive() {
    AppUtils.shared.printLog("App state : onInactive");
  }

  @override
  void onPaused() {
    AppUtils.shared.printLog("App state : onPaused");
  }

  @override
  void onResumed() {
    AppUtils.shared.printLog("App state : onResumed");
    getCountNotify();
  }

  @override
  void onHidden() {
    // TODO: implement onHidden
  }

}

getCountNotify(){
  if (Get.isRegistered<HomeController>()) {
    Get.find<HomeController>().getCountNotifyNotSeen();
    Get.find<HomeController>().addListTabBar(Get.find<HomeController>().dashboard,
        Get.find<HomeController>().schedule, Get.find<HomeController>().phonebook,
        Get.find<HomeController>().notify, Get.find<HomeController>().message);
    Get.find<HomeController>().update();
  }
  if (Get.isRegistered<NotificationController>()) {
    Get.find<NotificationController>().onRefresh();
  }
}
