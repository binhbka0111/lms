import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/staticPage/static_page_controller.dart';
class StaticPage extends GetWidget<StaticPageController> {
  @override
  final controller = Get.put(StaticPageController());
  StaticPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          elevation: 0,
          title: Text("${controller.itemsStaticPage.value.title}",style: const TextStyle(color: Colors.white)),
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(
                Icons.arrow_back_outlined,
                color: Colors.white,
              )),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 8.w,vertical: 16.h),
            child: Html(data: controller.itemsStaticPage.value.content),
          ),
        ),
      );
  }
}
