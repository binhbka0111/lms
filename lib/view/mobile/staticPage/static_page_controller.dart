import 'dart:ui';
import 'package:get/get.dart';
import '../../../../commom/utils/app_utils.dart';
import '../../../../commom/utils/color_utils.dart';
import '../../../../data/model/common/static_page.dart';
import 'package:webview_flutter/webview_flutter.dart';

class StaticPageController extends GetxController {
  var itemsStaticPage = ItemsStaticPage().obs;
  WebViewController? controllerWebView;
  String url = "";
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    var tmpStaticPage = Get.arguments;
    if (tmpStaticPage != null) {
      itemsStaticPage.value = tmpStaticPage;
      url = "https://cms.lms.xteldev.com/${itemsStaticPage.value.url}";
      controllerWebView = WebViewController()
        ..setJavaScriptMode(JavaScriptMode.unrestricted)
        ..setBackgroundColor(const Color(0x00000000))
        ..setNavigationDelegate(
          NavigationDelegate(
            onProgress: (int progress) {
              // Update loading bar.
            },
            onPageStarted: (String url) {},
            onPageFinished: (String url) {},
            onWebResourceError: (WebResourceError error) {},

          ),

        )..loadHtmlString("${itemsStaticPage.value.content}");
    } else {
      AppUtils.shared.showToast("Không có dữ liệu trang tĩnh!",
          backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
    }
  }

  @override
  dispose() {

    super.dispose();
  }

}
