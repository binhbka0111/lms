import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/appointment_builder.dart';
import 'package:slova_lms/commom/widget/hexColor.dart';
import 'package:slova_lms/commom/widget/loading_custom.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/schedule/schedule_controller.dart';
import '../../../commom/widget/meeting_data_source.dart';
import '../role/manager/schedule_manager/list_class_in_block_manager/list_class_in_block_manager_controller.dart';

class SchedulePage extends GetView<ScheduleController> {

  final controller = AppCache().userType != "MANAGER" ?Get.find<ScheduleController>():Get.put(ScheduleController());

  SchedulePage({super.key});

  @override
  Widget build(BuildContext context) {
if(AppCache().userType=="MANAGER"){
  controller.queryTimeTableByClass(Get.find<ListClassInBlockManagerController>().classId.value);
  controller.queryTimeCalendar(AppCache().userType);
  controller.onSelectDateTime();
  controller.weekController.value.selectedDate = Get.find<ScheduleController>().selectedDay.value;
}
    return Scaffold(
      appBar: AppBar(
        title: Text("${controller.getTitleSchedule(AppCache().userType)}"),
        leading: BackButton(
          color: Colors.white,
          onPressed: () {
            if (AppCache().userType == "MANAGER") {
              Get.back();
              controller.timeTable.clear();
              controller.selectedDay.refresh();
            } else {
              Get.find<HomeController>().comeBackHome();
            }
          },
        ),
        backgroundColor: ColorUtils.PRIMARY_COLOR,
      ),
      body: Obx(
        () => (controller.isReady.value)
            ? RefreshIndicator(
            color: ColorUtils.PRIMARY_COLOR,
                child: Column(
                  children: [
                    SingleChildScrollView(
                      physics: const AlwaysScrollableScrollPhysics(),
                      child: Container(
                        margin: const EdgeInsets.only(right: 16, left: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(top: 16),
                              child: Text(
                                "Thời gian",
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp),
                              ),
                            ),
                            dateTimeSelectV2(),
                        Visibility(
                            visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_LESSON_LIST),
                            child: Container(
                          margin: const EdgeInsets.only(top: 16),
                          child:  Text(
                            "Thời khóa biểu",
                            style: TextStyle(
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontSize: 12.sp),
                          ),
                        )),
                          ],
                        ),
                      ),
                    ),
                 Visibility(
                     visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_LESSON_LIST),
                     child:
                 Expanded(child: Container(
                   margin: EdgeInsets.symmetric(horizontal: 16.w),
                   child: timelineView(),
                 )))
                  ],
                ),
                onRefresh: () async {
                  await controller.onRefresh(AppCache().userType);
                })
            : const LoadingCustom(),
      ),
    );
  }

  Card timelineView() {
    return Card(
      child: SfCalendar(
        headerHeight: 0,
        timeSlotViewSettings: const TimeSlotViewSettings(
          timeFormat: "h:mm aa",
          numberOfDaysInView: 1,
          startHour: 0,
          endHour: 24,
          // nonWorkingDays: <int>[DateTime.sunday, DateTime.saturday],
          timeIntervalHeight: 56,
        ),
        controller: controller.timelineController.value,
        onViewChanged: (ViewChangedDetails details) {},
        view: CalendarView.day,
        selectionDecoration: const BoxDecoration(),
        weekNumberStyle:  WeekNumberStyle(
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          textStyle: TextStyle(color: Colors.white, fontSize: 15.sp),
        ),
        viewNavigationMode: ViewNavigationMode.none,
        allowViewNavigation: false,
        showCurrentTimeIndicator: true,
        viewHeaderStyle: const ViewHeaderStyle(
            dateTextStyle: TextStyle(color: Color.fromRGBO(26, 26, 26, 1))),
        dataSource: MeetingDataSource(controller.timeTable),
        todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),
        appointmentBuilder: appointmentBuilder,
      ),
    );
  }


  Card dateTimeSelectV2() {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 8.h, left: 8.h),
            child: Text(
              "${controller.selectedDay.value.year}",
              style: TextStyle(
                  color: const Color.fromRGBO(248, 129, 37, 1),
                  fontSize: 13.sp),
            ),
          ),
          Row(
            children: [
              Expanded(
                  child: Container(
                    margin: EdgeInsets.only(top: 4.h, left: 8.w, bottom: 4.h),
                    child: Text(
                      "Tháng ${controller.focusedDay.value.month}",
                      style: TextStyle(color: Colors.black87, fontSize: 14.sp),
                    ),
                  )),
              InkWell(
                child: (controller.weekNumber.value == 1)
                    ? const Icon(
                  Icons.expand_more,
                  size: 24,
                )
                    : const Icon(
                  Icons.expand_less,
                  size: 24,
                ),
                onTap: () {
                  controller.changeShowFull();
                },
              ),
              const SizedBox(
                width: 16,
              )
            ],
          ),
          SizedBox(
            height: controller.heightOffset.value,
            child: SfDateRangePicker(
              viewSpacing: 10,
              todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),
              controller: controller.weekController.value,
              view: DateRangePickerView.month,
              enablePastDates: true,
              selectionMode: DateRangePickerSelectionMode.single,
              showNavigationArrow: true,
              selectionRadius: 15,
              monthFormat: "MMM",
              headerHeight: 0,
              selectionColor: const Color.fromRGBO(249, 154, 81, 1),
              selectionShape: DateRangePickerSelectionShape.circle,
              selectionTextStyle: const TextStyle(
                  color: ColorUtils.COLOR_WHITE, fontWeight: FontWeight.w700),
              monthCellStyle: const DateRangePickerMonthCellStyle(
                todayTextStyle: TextStyle(
                    color: Color.fromRGBO(249, 154, 81, 1),
                    fontWeight: FontWeight.w700),
              ),
              headerStyle: DateRangePickerHeaderStyle(
                  textAlign: TextAlign.left,
                  textStyle: TextStyle(
                    fontStyle: FontStyle.normal,
                    fontSize: 12.sp,
                    color: Colors.black87,
                  )),
              monthViewSettings: DateRangePickerMonthViewSettings(
                  numberOfWeeksInView: controller.weekNumber.value,
                  firstDayOfWeek: 1,
                  showTrailingAndLeadingDates: true,
                  viewHeaderHeight: 16.h,
                  dayFormat: 'EEE',
                  viewHeaderStyle: DateRangePickerViewHeaderStyle(
                      textStyle: TextStyle(
                          color: HexColor("#B1B1B1"),
                          fontWeight: FontWeight.w600,
                          fontSize: 10.sp))),
              onViewChanged: (DateRangePickerViewChangedArgs args) {
                Future.delayed(const Duration(milliseconds: 400), () {
                  var visibleDates = args.visibleDateRange;
                  controller.focusedDay.value = visibleDates.startDate ?? DateTime.now();
                });
              },
              onSelectionChanged: (DateRangePickerSelectionChangedArgs args) {
                // var dateTime = args.value
                if (args.value is PickerDateRange) {
                  final DateTime rangeStartDate = args.value.startDate;

                  controller.selectedDay.value = rangeStartDate;
                  controller.timelineController.value.displayDate =
                      rangeStartDate;
                } else if (args.value is DateTime) {
                  final DateTime selectedDate = args.value;
                  controller.timelineController.value.displayDate =
                      selectedDate;

                  controller.selectedDay.value = selectedDate;
                } else if (args.value is List<DateTime>) {
                  final List<DateTime> selectedDates = args.value;
                  controller.timelineController.value.displayDate =
                  selectedDates[0];
                  controller.selectedDay.value = selectedDates[0];
                } else {

                }
               controller.onSelectDateTime();
              },
            ),
          )
        ],
      ),
    );
  }
}
