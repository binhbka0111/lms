import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:slova_lms/data/model/common/user_group_by_app.dart';
import 'package:slova_lms/view/mobile/role/manager/manager_home_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/teacher_home_controller.dart';
import '../../../commom/app_cache.dart';
import '../../../data/base_service/api_response.dart';
import '../../../data/model/common/schedule.dart';
import '../../../data/model/common/student_by_parent.dart';
import '../../../data/model/common/user_profile.dart';
import '../../../data/model/res/class/classTeacher.dart';
import '../../../data/repository/schedule/schedule_repo.dart';
import '../role/manager/schedule_manager/list_class_in_block_manager/list_class_in_block_manager_controller.dart';
import '../role/parent/parent_home_controller.dart';
import '../role/student/student_home_controller.dart';

class ScheduleController extends GetxController {
  var format = CalendarFormat.month.obs;
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;

  var timelineController = CalendarController().obs;

  var weekController = DateRangePickerController().obs;
  RxList<Appointment> timeTable = <Appointment>[].obs;
  RxList<Schedule> schedule = <Schedule>[].obs;
  RxList<Schedule> scheduleStudent = <Schedule>[].obs;
  RxList<Schedule> scheduleTeacher = <Schedule>[].obs;

  var userProfile = UserProfile().obs;
  var studentByParent = StudentByParent().obs;
  var classId = ClassId().obs;
  var subject = Subject().obs;
  final ScheduleRepo _scheduleRepo = ScheduleRepo();
 var isReady = false.obs;
 var showFull = false;

  var quarterTurns = 270.obs;
  var weekNumber = 1.obs;
  var heightOffset = (Get.width/5).obs;

  UserGroupByApp userGroupByApp = UserGroupByApp();

  @override
  void onInit() {
    super.onInit();
    Future.delayed(const Duration(seconds: 1), (){
      isReady.value = true;
    });
    changeCalendar();


  }


  onRefresh(role) {
    switch(role){
      case "STUDENT":
        return queryTimeTableByStudent(Get.find<StudentHomeController>().userProfile.value.id);
      case "PARENT":
        return queryTimeTableByStudent(Get.find<ParentHomeController>().currentStudentProfile.value.id);
      case "TEACHER":
        return queryTimeTableClassByTeacher(Get.find<TeacherHomeController>().userProfile.value.id,Get.find<TeacherHomeController>().classUId.value.classId);
      case "MANAGER":
        return queryTimeTableByClass(Get.find<ListClassInBlockManagerController>().classId.value);
    }
  }




  getTitleSchedule(type){
    switch(type){
      case "STUDENT":
        return "Thời khóa biểu";
      case "PARENT":{
        return "Thời khóa biểu";
      }
      case "TEACHER":{
        return "Công việc";
      }
      case "MANAGER":{
        return "Công việc";
      }
    }

  }


  String? setTimeTable(type){
    String? controller;
    switch(type){
      case "STUDENT":
        controller = "StudentHomeController";
        return controller;
      case "PARENT":{
        controller = "ParentHomeController";
        return controller;
      }
      case "TEACHER":{
        controller = "TeacherHomeController";
        return controller;
      }
      case "MANAGER":{
        controller = "ManagerHomeController";
        return controller;
      }
    }
    return controller;

  }


  void queryTimeCalendar(role){

    switch(role){
      case "STUDENT":
        if(DateTime.now().month<=12&&DateTime.now().month>9){
          timelineController.value.selectedDate = DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
          focusedDay.value = DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
          weekController.value.selectedDate = DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
          selectedDay.value = DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
        }else{
          timelineController.value.selectedDate = DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
          focusedDay.value = DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
          selectedDay.value = DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
          weekController.value.selectedDate = DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
        }
        break;
      case "PARENT":
        if(DateTime.now().month<=12&&DateTime.now().month>9){
          timelineController.value.selectedDate = DateTime(Get.find<ParentHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
          focusedDay.value = DateTime(Get.find<ParentHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
          selectedDay.value = DateTime(Get.find<ParentHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
          weekController.value.selectedDate = DateTime(Get.find<ParentHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
        }else{
          timelineController.value.displayDate = DateTime(Get.find<ParentHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
          focusedDay.value = DateTime(Get.find<ParentHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
          selectedDay.value = DateTime(Get.find<ParentHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
          weekController.value.displayDate = DateTime(Get.find<ParentHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
        }
        break;
      case "TEACHER":
        if(DateTime.now().month<=12&&DateTime.now().month>9){
          timelineController.value.selectedDate = DateTime(Get.find<TeacherHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
          selectedDay.value = DateTime(Get.find<TeacherHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
          weekController.value.selectedDate = DateTime(Get.find<TeacherHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
          focusedDay.value = DateTime(Get.find<TeacherHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
        }else{
          timelineController.value.selectedDate = DateTime(Get.find<TeacherHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
          focusedDay.value = DateTime(Get.find<TeacherHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
          selectedDay.value = DateTime(Get.find<TeacherHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
          weekController.value.selectedDate = DateTime(Get.find<TeacherHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
        }
        break;
      case "MANAGER":
        if(DateTime.now().month<=12&&DateTime.now().month>9){
          timelineController.value.selectedDate = DateTime(Get.find<ManagerHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
          focusedDay.value = DateTime(Get.find<ManagerHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
          selectedDay.value = DateTime(Get.find<ManagerHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
          weekController.value.selectedDate = DateTime(Get.find<ManagerHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);

        }else{
          timelineController.value.selectedDate = DateTime(Get.find<ManagerHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
          focusedDay.value = DateTime(Get.find<ManagerHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
          selectedDay.value = DateTime(Get.find<ManagerHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
          weekController.value.selectedDate = DateTime(Get.find<ManagerHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
        }
    }

  }

  onSelectDateTime(){
    switch(AppCache().userType){
      case "STUDENT":
        if(DateTime.now().month<=12&&DateTime.now().month>9){
          timelineController.value.displayDate = DateTime(Get.find<StudentHomeController>().fromYearPresent.value,selectedDay.value.month,selectedDay.value.day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
        }else{
          timelineController.value.displayDate = DateTime(Get.find<StudentHomeController>().toYearPresent.value,selectedDay.value.month,selectedDay.value.day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
        }
        break;
      case "PARENT":
        if(DateTime.now().month<=12&&DateTime.now().month>9){
         timelineController.value.displayDate = DateTime(Get.find<ParentHomeController>().fromYearPresent.value,selectedDay.value.month,selectedDay.value.day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
        }else{
         timelineController.value.displayDate = DateTime(Get.find<ParentHomeController>().toYearPresent.value,selectedDay.value.month,selectedDay.value.day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
        }
        break;
      case "TEACHER":
        if(DateTime.now().month<=12&&DateTime.now().month>9){
          timelineController.value.displayDate = DateTime(Get.find<TeacherHomeController>().fromYearPresent.value,selectedDay.value.month,selectedDay.value.day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
        }else{
          timelineController.value.displayDate = DateTime(Get.find<TeacherHomeController>().toYearPresent.value,selectedDay.value.month,selectedDay.value.day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
        }
        break;
      case "MANAGER":
        if(DateTime.now().month<=12&&DateTime.now().month>9){
         timelineController.value.displayDate = DateTime(Get.find<ManagerHomeController>().fromYearPresent.value,selectedDay.value.month,selectedDay.value.day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
        }else{
         timelineController.value.displayDate = DateTime(Get.find<ManagerHomeController>().toYearPresent.value,selectedDay.value.month,selectedDay.value.day,(DateTime.now().hour>3)?DateTime.now().hour-3:DateTime.now().hour);
        }
        break;
    }
  }

  getAppointments() {
    timeTable.clear();
    for (int i = 0; i < schedule.length; i++) {
      timeTable.add(Appointment(
          startTime:
              DateTime.fromMillisecondsSinceEpoch(schedule[i].startTime!),
          endTime:
              DateTime.fromMillisecondsSinceEpoch(schedule[i].endTime!),
          subject: schedule[i].subject!.subjectCategory!.name!,
          notes: schedule[i].subject?.clazz?.classCategory?.name!.toString(), // tên lớp
          resourceIds: schedule[i].subject!.clazz!.students,
          recurrenceId: schedule[i].subject?.teacher?.fullName!.toString(),
          // color: HexColor(schedule.value[i].color!)
      ));

    }
    timeTable.refresh();
  }


  getAppointmentsStudent() {
    timeTable.clear();
    for (int i = 0; i < scheduleStudent.length; i++) {
      timeTable.add(Appointment(
        startTime:
        DateTime.fromMillisecondsSinceEpoch(scheduleStudent[i].startTime!),
        endTime:
        DateTime.fromMillisecondsSinceEpoch(scheduleStudent[i].endTime!),
        subject: scheduleStudent[i].subject!.subjectCategory!.name!,
        notes: scheduleStudent[i].subject?.clazz?.classCategory?.name!.toString(), // tên lớp
        resourceIds: scheduleStudent[i].subject!.clazz!.students,
        recurrenceId: scheduleStudent[i].subject?.teacher?.fullName!.toString(),
        // color: HexColor(schedule.value[i].color!)
      ));
    }
    timeTable.refresh();
  }



  void queryTimeTableByClass(classId) {
    schedule.clear();
    _scheduleRepo.getScheduleByClass(classId).then((value) {
      if (value.state == Status.SUCCESS) {
        schedule.value = value.object!;
        getAppointments();
      }
    });
    schedule.refresh();
  }


  void queryTimeTableClassByTeacher(teacherId,classId){
    _scheduleRepo.getScheduleClassByTeacher(teacherId,classId).then((value) {
      if (value.state == Status.SUCCESS) {
        schedule.value = value.object!;
        getAppointments();
      }
    });
  }

  queryTimeTableByStudent(id) {
    scheduleStudent.value = [];
    _scheduleRepo.getScheduleStudent(id).then((value) {
      if (value.state == Status.SUCCESS) {
        scheduleStudent.value = value.object!;
        getAppointmentsStudent();
      }
    });
    scheduleStudent.refresh();
  }

  void queryTimeTableByTeacher(idTeacher) {
    _scheduleRepo.getScheduleTeacher(idTeacher).then((value) {
      if (value.state == Status.SUCCESS) {
        scheduleTeacher.value = value.object!;
        getAppointments();
      }
    });
  }


  void changeCalendar() {
    if(showFull){
      weekNumber.value = 4;
      heightOffset.value = (Get.width/2);
      quarterTurns.value = 90;
    } else {
      heightOffset.value = (Get.width/5);
      weekNumber.value = 1;
      quarterTurns.value = 270;
    }
  }

  void changeShowFull() {
    showFull = !showFull;
    changeCalendar();
  }
}
