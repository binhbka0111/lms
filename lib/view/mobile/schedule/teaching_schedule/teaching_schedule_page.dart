import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/schedule/teaching_schedule/teaching_schedule_controller.dart';

class TeachingSchedulePage extends GetView<TeachingScheduleController> {
  @override
  final controller = Get.put(TeachingScheduleController());


  TeachingSchedulePage({super.key});

  @override
  Widget build(BuildContext context) {
    return
     Scaffold(
       appBar: AppBar(
         backgroundColor: ColorUtils.PRIMARY_COLOR,
         elevation: 0,
         title:  Text(
           "${controller.getTitleDetailSchedule(AppCache().userType)}",
           style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w500),
         ),
       ),
       body: SingleChildScrollView(
         child: Container(
           margin: const EdgeInsets.all(16),
           child: Column(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: [
               Text(
                 "Môn ${controller.appoinment.value.subject}",
                 style: TextStyle(
                     fontWeight: FontWeight.w500,
                     fontSize: 12.sp,
                     color: const Color.fromRGBO(133, 133, 133, 1)),
               ),
               const Padding(padding: EdgeInsets.only(top: 8)),
               Container(
                 decoration: BoxDecoration(
                     borderRadius: BorderRadius.circular(6),
                     color: Colors.white),
                 padding: const EdgeInsets.all(16),
                 child: Column(
                   children: [
                     Row(
                       children: [
                         Text(
                           "Môn học:",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: const Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w400),
                         ),
                         Expanded(child: Container()),
                         Text(controller.appoinment.value.subject,
                             style: TextStyle(
                                 fontSize: 14.sp,
                                 color: const Color.fromRGBO(26, 26, 26, 1),
                                 fontWeight: FontWeight.w500))
                       ],
                     ),
                     const Padding(padding: EdgeInsets.only(top: 16)),
                     Row(
                       children: [
                         Text(
                           "Lớp:",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: const Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w400),
                         ),
                         Expanded(child: Container()),
                         Text("${controller.appoinment.value.notes}",
                             style: TextStyle(
                                 fontSize: 14.sp,
                                 color: ColorUtils.PRIMARY_COLOR,
                                 fontWeight: FontWeight.w500))
                       ],
                     ),
                     const Padding(padding: EdgeInsets.only(top: 16)),
                     Row(
                       children: [
                         Text(
                           "Thời gian:",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: const Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w400),
                         ),
                         Expanded(child: Container()),
                         Text("${controller.outputFormat.format(controller.appoinment.value.startTime)} - ${controller.outputFormat.format(controller.appoinment.value.endTime)}",
                             style: TextStyle(
                                 fontSize: 14.sp,
                                 color: const Color.fromRGBO(26, 26, 26, 1),
                                 fontWeight: FontWeight.w500))
                       ],
                     ),
                     const Padding(padding: EdgeInsets.only(top: 16)),
                     Row(
                       children: [
                          Text(
                           "Ngày:",
                           style: TextStyle(
                               fontSize: 14.sp,
                               color: const Color.fromRGBO(26, 26, 26, 1),
                               fontWeight: FontWeight.w400),
                         ),
                         Expanded(child: Container()),
                         Text(controller.outputDateFormat.format(controller.appoinment.value.startTime),
                             style: TextStyle(
                                 fontSize: 14.sp,
                                 color: const Color.fromRGBO(26, 26, 26, 1),
                                 fontWeight: FontWeight.w500))
                       ],
                     ),
                     const Padding(padding: EdgeInsets.only(top: 16)),

                  AppCache().userType != "TEACHER"? Row(
                      children: [
                         Text(
                          "Giáo viên: ",
                          style: TextStyle(
                              fontSize: 14.sp,
                              color: const Color.fromRGBO(26, 26, 26, 1),
                              fontWeight: FontWeight.w400),
                        ),
                        Expanded(child: Container()),
                        Text("${controller.appoinment.value.recurrenceId}",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontWeight: FontWeight.w500))
                      ],
                    )
                        : Container(),
                   ],
                 ),
               ),
               const Padding(padding: EdgeInsets.only(top: 16)),
               Row(
                 children:  [
                   Text(
                     "Danh Sách Học Sinh ",
                     style: TextStyle(
                         fontSize: 14.sp,
                         color: const Color.fromRGBO(26, 26, 26, 1),
                         fontWeight: FontWeight.w400),
                   ),
                   Text("${controller.appoinment.value.notes}",
                       style: TextStyle(
                           fontSize: 14.sp,
                           color: ColorUtils.PRIMARY_COLOR,
                           fontWeight: FontWeight.w500))
                 ],
               ),
               ListView.builder(
                   shrinkWrap: true,
                   physics: const NeverScrollableScrollPhysics(),
                   itemCount: controller.students.length,
                   itemBuilder: (context, index) {
                     return GestureDetector(
                       onTap: () {
                       },
                       child: Column(
                         children: [
                           Row(
                             children: [
                               Container(
                                 margin: const EdgeInsets.only(
                                     left: 16, right: 24),
                                 child: Text(
                                   '${index+1}',
                                   style: const TextStyle(
                                       color: Color.fromRGBO(26, 26, 26, 1),
                                       fontSize: 14),
                                 ),
                               ),
                               Container(
                                 margin: const EdgeInsets.only(
                                     top: 8, bottom: 8),
                                 child: Row(
                                   children: [

                                     SizedBox(
                                       width: 48,
                                       height: 48,
                                       child:
                                        CacheNetWorkCustom(urlImage: '${controller.students[index].image }',)


                                     ),
                                     const Padding(
                                         padding: EdgeInsets.only(right: 8)),
                                     Column(
                                       crossAxisAlignment: CrossAxisAlignment.start,
                                       children: [
                                         SizedBox(
                                           child: RichText(
                                               text: TextSpan(children: [
                                                 const TextSpan(
                                                     text: 'Học Sinh: ',
                                                     style: TextStyle(
                                                         color: ColorUtils.PRIMARY_COLOR,
                                                         fontSize: 14)),
                                                 TextSpan(
                                                     text:
                                                     '${controller.students[index].fullName}',
                                                     style: const TextStyle(
                                                         color: Color.fromRGBO(
                                                             26, 26, 26, 1),
                                                         fontSize: 14)),
                                               ])),
                                         ),
                                         const Padding(
                                             padding:
                                             EdgeInsets.only(top: 4)),
                                         SizedBox(
                                           width: 200,

                                           child:Visibility(
                                             visible: controller.students[index].birthday!= null,
                                             child:  Text(
                                             controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.students[index].birthday??0)),
                                             style: const TextStyle(
                                                 color: Color.fromRGBO(
                                                     133, 133, 133, 1),
                                                 fontSize: 14),
                                           ),)
                                         )
                                       ],
                                     )
                                   ],
                                 ),
                               )
                             ],
                           ),
                           const Divider()
                         ],
                       ),
                     );
                   })
             ],
           ),
         ),
       ),
     );
  }
}
