import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/appointment_builder_timelineday.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/commom/widget/meeting_data_source.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/teacher_home_controller.dart';
import '../../../../commom/widget/loading_custom.dart';
import '../../../../commom/widget/logout.dart';
import '../../../../routes/app_pages.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';

class TeacherHomePage extends GetWidget<TeacherHomeController> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  final controller = Get.put(TeacherHomeController());

  TeacherHomePage({super.key});



  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      drawerEdgeDragWidth: 0,
      key: _scaffoldKey,
      drawer: Container(
        margin: const EdgeInsets.only(right: 28),
        width: MediaQuery.of(context).size.width,
        child: Obx(() => Drawer(
              child: SingleChildScrollView(
                child: controller.isReadyDrawer.value?Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: ColorUtils.PRIMARY_COLOR,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      margin: const EdgeInsets.all(16),
                      padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                      child: Row(
                        children: [
                        InkWell(
                          onTap: (){
                            controller.goToDetailAvatar();
                          },
                          child:   SizedBox(
                            width: 40.h,
                            height: 40.h,
                            child:
                            CacheNetWorkCustom(urlImage:  '${controller.userProfile.value.image }',),

                          ),
                        ),
                          const Padding(padding: EdgeInsets.only(left: 16)),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${controller.userProfile.value.fullName}",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15.sp,
                                      fontWeight: FontWeight.w700),
                                ),
                                Text(
                                  "${controller.userProfile.value.email}",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w400),
                                )
                              ],
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              Get.back();
                            },
                            icon: const Icon(
                                Icons.keyboard_double_arrow_left_outlined),
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                    const Padding(padding: EdgeInsets.only(top: 16)),
                    Container(
                      margin: const EdgeInsets.only(left: 16, right: 16),
                      child: Row(
                        children: [
                          Text(
                            'Thông Tin Cá Nhân',
                            style: TextStyle(
                                color: const Color.fromRGBO(133, 133, 133, 1),
                                fontSize: 15.sp),
                          ),
                          Expanded(child: Container()),
                          InkWell(
                            onTap: () {
                              controller.goToUpdateInfoUser();
                            },
                            child:
                        Row(
                          children: [
                            Text('Chỉnh sửa ',
                                style: TextStyle(
                                    color: ColorUtils.PRIMARY_COLOR,
                                    fontSize: 15.sp,
                                    fontFamily:
                                    'static/Inter-Regular.ttf')),
                             Icon(
                              Icons.mode_edit_outline_outlined,
                              color: ColorUtils.PRIMARY_COLOR,
                              size: 16.sp,
                            )
                          ],
                        ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin:
                          const EdgeInsets.only(left: 16, right: 16, top: 16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                width: 150.w,
                                child: Text(
                                  'Họ và tên: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Text(
                                '${controller.userProfile.value.fullName}',
                                style: TextStyle(
                                    color: ColorUtils.PRIMARY_COLOR,
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              )
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 10.h)),
                          Row(
                            children: [
                              SizedBox(
                                width: 150.w,
                                child: Text(
                                  'Số Điện Thoại: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Text(
                                '${controller.userProfile.value.phone}',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 10)),
                          Row(
                            children: [
                              SizedBox(
                                width: 150.w,
                                child: Text(
                                  'Email: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Text(
                                '${controller.userProfile.value.email}',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 10)),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 150.w,
                                child: Text(
                                  'Trường : ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Flexible(child: Text(
                                '${controller.school.value.name}',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              ))
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 10)),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 150.w,
                                child: Text(
                                  'Chức vụ: ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14.sp,
                                      fontFamily: 'static/Inter-Medium.ttf'),
                                ),
                              ),
                              Expanded(child:
                                Obx(() => ListView.builder(
                                    shrinkWrap: true,
                                    itemCount:  controller.userProfile.value.item!.teachers?.length,
                                    itemBuilder: (context, index){
                                      if(controller.userProfile.value.item!.teachers!.isNotEmpty){
                                        controller.listclass.value = controller.userProfile.value.item!.teachers![index].clazz!;
                                      }
                                      return
                                        RichText(
                                          textAlign: TextAlign.start,
                                          text: TextSpan(
                                            text: "${controller.getType(controller.userProfile.value.item!.teachers?[index].type, controller.userProfile.value.item!.teachers?[index].subjectName)} ",
                                            style: TextStyle(
                                                color: const Color
                                                    .fromRGBO(26,
                                                    26, 26, 1),
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w500,
                                                fontFamily: 'static/Inter-Medium.ttf'),
                                            children:
                                            controller.listclass.map((e) {
                                              var index = controller.listclass.indexOf(e);
                                              var showSplit = ", ";
                                              if (index == controller.listclass.length - 1) {
                                                showSplit = "";
                                              }
                                              return TextSpan(
                                                  text: "${controller.listclass[index].name}$showSplit",
                                                  style: TextStyle(
                                                      color: ColorUtils.PRIMARY_COLOR,
                                                      fontSize: 14.sp,
                                                      fontWeight: FontWeight.w500,
                                                      fontFamily: 'static/Inter-Medium.ttf'));
                                            }).toList(),
                                          ),
                                        );
                                    }

                                )),
                              ),
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 10)),
                        ],
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(
                              left: 16, right: 16, top: 24),
                          child: Row(
                            children: [
                               Text(
                                'Cài Đặt',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 15.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'static/Inter-Medium.ttf'),
                              ),
                              Expanded(child: Container())
                            ],
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 12)),
                        Container(
                          margin: const EdgeInsets.only(left: 16),
                          child: Column(
                            children: [
                             InkWell(
                               onTap: () {
                                 controller.getStaticPage("SUPPORT");
                               },
                               child:  Row(
                               children: [
                                  Text(
                                   'Hỗ Trợ',
                                   style: TextStyle(
                                       color: const Color.fromRGBO(24, 29, 39, 1),
                                       fontSize: 14.sp,
                                       fontWeight: FontWeight.w500,
                                       fontFamily: 'static/Inter-Medium.ttf'),
                                 ),
                                 Expanded(child: Container()),
                                 IconButton(
                                     iconSize: 24.sp,
                                     onPressed: () {
                                       controller.getStaticPage("SUPPORT");
                                     },
                                     splashColor: Colors.transparent,
                                     icon: const Icon(
                                       Icons.navigate_next,
                                       color: Colors.black,
                                     ))
                               ],
                             ),),
                             InkWell(
                               onTap: (){
                                 controller.goToChangePassPage();
                               },
                               child:  Row(
                                 children: [
                                    Text(
                                     'Đổi Mật Khẩu ',
                                     style: TextStyle(
                                         color: const Color.fromRGBO(24, 29, 39, 1),
                                         fontSize: 14.sp,
                                         fontWeight: FontWeight.w500,
                                         fontFamily: 'static/Inter-Medium.ttf'),
                                   ),
                                   Expanded(child: Container()),
                                   IconButton(
                                       iconSize: 24.sp,
                                       onPressed: () {
                                         controller.goToChangePassPage();
                                       },
                                       splashColor: Colors.transparent,
                                       icon: const Icon(
                                         Icons.navigate_next,
                                         color: Colors.black,
                                       ))
                                 ],
                               ),
                             ),
                             InkWell(
                               onTap: () {
                                 controller.getStaticPage("RULES");
                               },
                               child:  Row(
                               children: [
                                  Text(
                                   'Chính Sách Bảo Mật',
                                   style: TextStyle(
                                       color: const Color.fromRGBO(24, 29, 39, 1),
                                       fontSize: 14.sp,
                                       fontWeight: FontWeight.w500,
                                       fontFamily: 'static/Inter-Medium.ttf'),
                                 ),
                                 Expanded(child: Container()),
                                 IconButton(
                                     iconSize: 24.sp,
                                     onPressed: () {
                                       controller.getStaticPage("RULES");
                                     },
                                     splashColor: Colors.transparent,
                                     icon: const Icon(
                                       Icons.navigate_next,
                                       color: Colors.black,
                                     ))
                               ],
                             ),),
                             InkWell(
                               onTap: () {
                                 controller.getStaticPage("SECURITY");
                               },
                               child:  Row(
                               children: [
                                  Text(
                                   'Điều Khoản Sử Dụng',
                                   style: TextStyle(
                                       color: const Color.fromRGBO(24, 29, 39, 1),
                                       fontSize: 14.sp,
                                       fontWeight: FontWeight.w500,
                                       fontFamily: 'static/Inter-Medium.ttf'),
                                 ),
                                 Expanded(child: Container()),
                                 IconButton(
                                     iconSize: 24.sp,
                                     onPressed: () {
                                       controller.getStaticPage("SECURITY");
                                     },
                                     splashColor: Colors.transparent,
                                     icon: const Icon(
                                       Icons.navigate_next,
                                       color: Colors.black,
                                     ))
                               ],
                             ),),
                              InkWell(
                                onTap: () {
                                  Get.bottomSheet(const Logout());
                                },
                                child: Row(
                                  children: [
                                     Text(
                                      'Đăng Xuất',
                                      style: TextStyle(
                                          color: const Color.fromRGBO(24, 29, 39, 1),
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w500,
                                          fontFamily:
                                              'static/Inter-Medium.ttf'),
                                    ),
                                    Expanded(child: Container()),
                                    IconButton(
                                        iconSize: 24.sp,
                                        onPressed: () {
                                          Get.bottomSheet(const Logout());
                                        },
                                        splashColor: Colors.transparent,
                                        icon: const Icon(
                                          Icons.navigate_next,
                                          color: Colors.black,
                                        ))
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ):Container(),
              ),
            )),
      ),

      body: Obx(() => controller.isReady.value?RefreshIndicator(
          color: ColorUtils.PRIMARY_COLOR,
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child:  Column(
              children: [
                Container(
                  color: Colors.white,
                  padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {
                          _scaffoldKey.currentState?.openDrawer();
                        },
                        child: SizedBox(
                          height: 32.h,
                          width: 32.w,
                          child: Image.asset(
                              "assets/images/icon_more_teacher.png"),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(left: 8.w)),
                      Visibility(
                          visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_CLASS_LIST),
                          child: buildClassOffice()),
                    ],
                  ),
                ),
                Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(padding: EdgeInsets.only(top: 16)),
                      Container(
                        margin: const EdgeInsets.only(left: 16),
                        child: const Text(
                          "Quản Lý Lớp Học",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                              color: Colors.black),
                        ),
                      ),
                      Visibility(
                          visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_SCHOOL_YEARS_LIST),
                          child: Visibility(
                              visible: controller.schoolYears.isNotEmpty,
                              child: InkWell(
                                onTap: () {
                                  Get.bottomSheet(
                                      StatefulBuilder(builder: (context,state){
                                        return  Wrap(
                                          children: [
                                            Container(
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    topRight: Radius.circular(32.r),
                                                    topLeft: Radius.circular(32.r)),
                                                color: Colors.white,
                                              ),
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(6.r),
                                                        color: const Color.fromRGBO(210, 212, 216, 1)),
                                                    height: 6.h,
                                                    width: 48.w,
                                                    alignment: Alignment.topCenter,
                                                    margin: EdgeInsets.only(top: 16.h),
                                                  ),
                                                  Padding(padding: EdgeInsets.only(top: 12.h)),
                                                  Text("Năm học",style: TextStyle(color: const Color.fromRGBO(23, 32, 63, 1,),fontSize: 16.sp,fontWeight: FontWeight.w500),),
                                                  controller.fromYearPresent.value != 0?Container(
                                                    margin: EdgeInsets.symmetric(vertical: 16.h, horizontal: 8.w),
                                                    child: ListView.builder(
                                                        physics: const ScrollPhysics(),
                                                        itemCount: controller.schoolYears.length,
                                                        shrinkWrap: true,
                                                        itemBuilder: (context,index) {
                                                          return InkWell(
                                                            child: Container(
                                                              color:  controller.clickSchoolYear[index] == true?const Color.fromRGBO(254, 230, 211, 1):Colors.white,
                                                              child: Column(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                children: [
                                                                  Padding(padding: EdgeInsets.only(top: 16.h)),
                                                                  Row(
                                                                    children: [
                                                                      Text(
                                                                        "Năm học ",
                                                                        style: TextStyle(
                                                                            color: controller.clickSchoolYear[index] == true? ColorUtils.PRIMARY_COLOR:Colors.black,
                                                                            fontSize: 14.sp,
                                                                            fontWeight: FontWeight.w500),
                                                                      ),
                                                                      Text(
                                                                        "${controller.schoolYears[index].fromYear}",
                                                                        style: TextStyle(
                                                                            color: controller.clickSchoolYear[index] == true? ColorUtils.PRIMARY_COLOR:Colors.black,
                                                                            fontSize: 14.sp,
                                                                            fontWeight: FontWeight.w500),
                                                                      ),
                                                                      Text(
                                                                        "-",
                                                                        style: TextStyle(
                                                                            color: controller.clickSchoolYear[index] == true? ColorUtils.PRIMARY_COLOR:Colors.black,
                                                                            fontSize: 14.sp,
                                                                            fontWeight: FontWeight.w500),
                                                                      ),
                                                                      Text(
                                                                        "${controller.schoolYears[index].toYear}",
                                                                        style: TextStyle(
                                                                            color: controller.clickSchoolYear[index] == true? ColorUtils.PRIMARY_COLOR:Colors.black,
                                                                            fontSize: 14.sp,
                                                                            fontWeight: FontWeight.w500),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  Padding(padding: EdgeInsets.only(top: 16.h)),
                                                                  Visibility(visible: index != 3, child: const Divider(
                                                                    height: 0,
                                                                    indent: 0,
                                                                    thickness: 1,
                                                                  )),
                                                                ],
                                                              ),
                                                            ),
                                                            onTap: () {
                                                              updated(state,index);
                                                              controller.onSelectedSchoolYears(index);

                                                            },
                                                          );
                                                        }),
                                                  ):Container()
                                                ],
                                              ),
                                            )
                                          ],
                                        );
                                      }));
                                },
                                child: Container(
                                  margin: const EdgeInsets.only(left: 16),
                                  child: Row(
                                    children: [
                                      Text(
                                        "Năm học ",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      Text(
                                        "${controller.fromYearPresent.value}",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            color: controller.clickSchoolYear.contains(true) == true? ColorUtils.PRIMARY_COLOR:Colors.black,
                                            fontWeight: FontWeight.w400),
                                      ),
                                      Text(
                                        "-",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            color: controller.clickSchoolYear.contains(true) == true? ColorUtils.PRIMARY_COLOR:Colors.black,
                                            fontWeight: FontWeight.w400),
                                      ),
                                      Text(
                                        "${controller.toYearPresent.value}",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            color: controller.clickSchoolYear.contains(true) == true? ColorUtils.PRIMARY_COLOR:Colors.black,
                                            fontWeight: FontWeight.w400),
                                      ),
                                      Padding(padding: EdgeInsets.only(left: 8.w)),
                                      Icon(
                                        Icons.keyboard_arrow_down_outlined,
                                        size: 18.h,
                                        color: Colors.black,
                                      )
                                    ],
                                  ),
                                ),
                              ))),
                      const Padding(padding: EdgeInsets.only(top: 16)),
                      Container(
                        margin:  EdgeInsets.only(left: 16.w, right: 16.w,bottom: 16.h),
                        child: GridView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: controller.listPageClassroomManagement.length,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 2/2,
                              crossAxisCount: !Device.get().isTablet ? 2 : 4,crossAxisSpacing: 16,mainAxisSpacing: 16),
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                if(index == controller.listPageClassroomManagement.indexWhere((element) => element.name == TeacherHomeController.diligentManagement)){
                                  controller.clickDiligentManagement();
                                }
                                if(index == controller.listPageClassroomManagement.indexWhere((element) => element.name == TeacherHomeController.learningManagement)){
                                  controller.clickLearningManagement();
                                }
                                if(index == controller.listPageClassroomManagement.indexWhere((element) => element.name == TeacherHomeController.sendNotify)){
                                  controller.goToSendNotificationPage();
                                }
                                if(index == controller.listPageClassroomManagement.indexWhere((element) => element.name == TeacherHomeController.listStudentInClass)){
                                  controller.goToStudentListPage();
                                }

                              },
                              child: Card(
                                color: Colors.white,
                                margin: EdgeInsets.zero,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6)),
                                elevation: 3,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: 100,
                                        height: 100,
                                        child: Image.asset("${controller.listPageClassroomManagement[index].image}"),
                                      ),
                                      Container(
                                        margin:const EdgeInsets.only(top:6),
                                        height: 27.sp,
                                        child: Text("${controller.listPageClassroomManagement[index].name}",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12.sp,
                                                color: Colors.black)),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },),
                      ),
                    ],
                  ),
                ),
                const Padding(padding: EdgeInsets.only(top: 16)),
                Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(padding: EdgeInsets.only(top: 16)),
                      Container(
                        margin: const EdgeInsets.only(left: 16),
                        child: const Text(
                          "Thông tin từ nhà trường",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                              color: Colors.black),
                        ),
                      ),
                      const Padding(padding: EdgeInsets.only(top: 16)),
                      Visibility(
                          visible: controller.listPageInformationFromSchool.isNotEmpty,
                          child: Container(
                            margin:  EdgeInsets.only(left: 16.w, right: 16.w),
                            child: GridView.builder(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: controller.listPageInformationFromSchool.length,
                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: !Device.get().isTablet ? 2 : 4,crossAxisSpacing: 16,mainAxisSpacing: 16),
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {
                                    if(index == controller.listPageInformationFromSchool.indexWhere((element) => element.name == TeacherHomeController.newAndEvent)){
                                      Get.toNamed(Routes.listEventNewsPage,);
                                    }
                                    if(index == controller.listPageInformationFromSchool.indexWhere((element) => element.name == TeacherHomeController.notification)){
                                      Get.find<HomeController>().comeNotification();
                                    }
                                  },
                                  child: Card(
                                    color: Colors.white,
                                    margin: EdgeInsets.zero,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(6)),
                                    elevation: 3,
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            width: 100,
                                            height: 100,
                                            child: Image.asset("${controller.listPageInformationFromSchool[index].image}"),
                                          ),
                                          const Padding(
                                              padding: EdgeInsets.only(top: 8)),
                                          Text("${controller.listPageInformationFromSchool[index].name}",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 12.sp,
                                                  color: Colors.black))
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },),
                          )),
                      const Padding(padding: EdgeInsets.only(bottom: 16)),
                    ],
                  ),
                ),
                Visibility(
                    visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_LESSON_DASHBROAD_LIST),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: Colors.white,
                      ),
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.fromLTRB(16, 20, 16, 6),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const SizedBox(
                                      child: Text("Thời khóa biểu",
                                          style: TextStyle(
                                              fontSize: 20,
                                              color:
                                              Color.fromRGBO(26, 26, 26, 1))),
                                    ),
                                    const Padding(padding: EdgeInsets.only(top: 6)),
                                    Visibility(
                                      visible: controller.isSubjectToday.value,
                                      child:
                                      SizedBox(
                                        child: Text(
                                          "Hôm nay bạn có ${controller.scheduleToday.length} tiết dạy",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color:
                                              const Color.fromRGBO(133, 133, 133, 1)),
                                        ),
                                      ),)
                                  ],
                                )),
                                InkWell(
                                  onTap: (){
                                    Get.find<HomeController>().comeCalendar();
                                  },
                                  child:
                                  SizedBox(
                                    height: 30,
                                    child: Text(
                                      "Xem Tất cả",
                                      style: TextStyle(
                                          fontSize: 12.sp,
                                          color: ColorUtils.PRIMARY_COLOR),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 16.h)),
                          Card(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                            margin: const EdgeInsets.only(left: 16, right: 16, bottom: 20),
                            child: Obx(() => SfCalendar(
                              view: CalendarView.timelineDay,
                              firstDayOfWeek: 1,
                              onViewChanged: (viewChangedDetails) {

                              },
                              controller: controller.calendarController.value,
                              allowViewNavigation: false,
                              dataSource: MeetingDataSource(controller.timeTable),
                              todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),

                              selectionDecoration: BoxDecoration(
                                border: Border.all(color: const Color.fromRGBO(249, 154, 81, 1), width: 2), // Sử dụng màu sắc của border
                              ),
                              appointmentBuilder: appointmentBuilderTimeLineDay,
                              timeSlotViewSettings: TimeSlotViewSettings(
                                timeFormat: "H a",
                                timeIntervalWidth: 80.w,
                                timelineAppointmentHeight: 50.h,

                              ),
                              scheduleViewSettings: const ScheduleViewSettings(

                                  appointmentItemHeight: 20, hideEmptyScheduleWeek: true)
                              ,
                            )),
                          ),
                        ],
                      ),
                    ))
              ],
            ),
          ), onRefresh: () async{
        await controller.onRefresh();
      }):const LoadingCustom())
        ));
  }

  Future<void> updated(StateSetter updateState, int index) async {
    updateState(() {
      controller.onClickSchoolYears(index);
    });
  }

  Expanded buildClassOffice() {
    return Expanded(
        child: Container(
      height: 42.h,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(6.r),
          border: Border.all(
              color: const Color.fromRGBO(239, 239, 239, 1), width: 1)),
      padding:  EdgeInsets.all(8.h),
      child: ListView.builder(
          itemCount: controller.classOfTeacher.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return itemClassId(index);
          }),
    ));
  }

  itemClassId(int index) {
    return InkWell(
        onTap: () {
          controller.classUId.value = controller.classOfTeacher[index];
          controller.selectedClass(controller.classUId.value, index);
          controller.getTablePlan();
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              width: 100.w,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: controller.classOfTeacher[index].checked
                    ? const Color.fromRGBO(249, 154, 81, 1)
                    : const Color.fromRGBO(246, 246, 246, 1),
                borderRadius: BorderRadius.circular(6),
              ),
              margin: const EdgeInsets.only(right: 8),
              padding: const EdgeInsets.fromLTRB(12, 4, 12, 4),
              child: Text(
                "${controller.classOfTeacher[index].name}",
                style: TextStyle(
                    fontSize: 12.sp,
                    color: controller.classOfTeacher[index].checked
                        ? Colors.white
                        : const Color.fromRGBO(90, 90, 90, 1)),
              ),
            ),
            (controller.classOfTeacher[index].homeroomClass == true)
                ? Positioned(
                    right: 13,
                    top: 4,
                    child: Icon(
                      Icons.star,
                      size: 10,
                      color: controller.classOfTeacher[index].checked
                          ? Colors.white
                          : const Color.fromRGBO(90, 90, 90, 1),
                    ),
                  )
                : Container()
          ],
        ));
  }

}
