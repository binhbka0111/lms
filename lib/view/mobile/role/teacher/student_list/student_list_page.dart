import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/commom/widget/loading_custom.dart';
import 'package:slova_lms/view/mobile/role/teacher/student_list/student_list_controller.dart';
import '../../../../../commom/utils/date_time_utils.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';


class StudentListPage extends GetWidget<StudentListController> {
  @override
  final controller = Get.put(StudentListController());

  StudentListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromRGBO(239, 239, 239, 1),
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                comeToHome();
              },
              icon: const Icon(Icons.home),
              color: Colors.white,
            )
          ],
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title: Text(
            'Danh Sách Học Sinh',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body: Obx(() => controller.isReady.value?RefreshIndicator(
              color: ColorUtils.PRIMARY_COLOR,
              onRefresh: () async {
                await controller.onRefresh();
              },
              child: SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: Center(
                  child: Column(
                    children: [
                      Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.only(
                              top: 16.h, left: 16.w, bottom: 8.h),
                          child: RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                  text: 'Danh Sách Học Sinh ',
                                  style: TextStyle(
                                      color: const Color.fromRGBO(
                                          133, 133, 133, 1),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500)),
                              TextSpan(
                                  text: controller.currentClass.value.name??"",
                                  style: TextStyle(
                                      color: ColorUtils.PRIMARY_COLOR,
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500))
                            ]),
                          )),
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: const Color.fromRGBO(255, 255, 255, 1)),
                        margin: EdgeInsets.only(left: 16.w, right: 16.w),
                        padding:  EdgeInsets.symmetric(vertical: 8.h,horizontal: 16.w),
                        child: controller.detailItem.isNotEmpty
                            ?ListView.builder(
                            shrinkWrap: true,
                            physics: const ScrollPhysics(),
                            padding: EdgeInsets.zero,
                            itemCount: controller.detailItem.length,
                            itemBuilder: (context, index) {
                              var showLine = index ==
                                  controller.detailItem.length - 1
                                  ? false
                                  : true;
                              return buildItemListStudent(index, showLine);
                            })
                            :Container(
                          alignment: Alignment.center,
                          width: double.infinity,
                          child: Text("Không có dữ liệu", style: TextStyle(fontSize: 12.sp),),
                        ),
                      ),
                    ],
                  ),
                ),
              )):const LoadingCustom(),
        ),
    );
  }

  buildItemListStudent(int index, bool showLine) {
    return Obx(() => GestureDetector(
          onTap: () {},
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 16.w, right: 16.w),
                    child: Text(
                      '${index + 1}',
                      style: TextStyle(
                          color: const Color.fromRGBO(26, 26, 26, 1),
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  SizedBox(
                    width: 48.h,
                    height: 48.h,
                    child:
                    CacheNetWorkCustom(urlImage:  '${ controller.detailItem[index].image }',),

                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                          text: TextSpan(children: [
                        TextSpan(
                            text: 'Học Sinh: ',
                            style: TextStyle(
                                color: ColorUtils.PRIMARY_COLOR,
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'static/Inter-Regular.ttf')),
                        TextSpan(
                            text:
                                '${controller.detailItem[index].fullName}',
                            style: TextStyle(
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                fontFamily: 'static/Inter-Regular.ttf')),
                      ])),
                      Padding(padding: EdgeInsets.only(top: 4.h)),
                      Text(
                        controller.detailItem[index].birthday != null
                            ? DateTimeUtils.convertLongToStringTime(
                                controller.detailItem[index].birthday)
                            : "",
                        style: TextStyle(
                            color: const Color.fromRGBO(133, 133, 133, 1),
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w400,
                            fontFamily: 'static/Inter-Regular.ttf'),
                      )
                    ],
                  ))
                ],
              ),
              (showLine) ? const Divider() : Container()
            ],
          ),
        ));
  }
}
