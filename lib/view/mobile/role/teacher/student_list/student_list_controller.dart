import 'package:get/get.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/model/common/list_student.dart';
import '../../../../../data/model/res/class/classTeacher.dart';
import '../../../../../data/repository/list_student/list_student_repo.dart';

class StudentListController extends GetxController{
  final ListStudentRepo _listStudent = ListStudentRepo();
  var listStudent = ListStudent().obs;
  RxList<DetailItem> detailItem = <DetailItem>[].obs;
  Rx<ClassId> currentClass = ClassId().obs;
  var isReady = false.obs;
  @override
  void onInit() {
    super.onInit();
    var tmpClass = Get.arguments;
    if(tmpClass!=null){
      currentClass.value = tmpClass;
      getDetailListStudent();
    }
  }


    getDetailListStudent() async{
    var classId = currentClass.value.classId;
    await _listStudent.listStudentByClass(classId).then((value) {
      if (value.state == Status.SUCCESS) {
        detailItem.value = value.object!;
      }
    });
    isReady.value = true;
  }

  onRefresh() {
    getDetailListStudent();
  }

}