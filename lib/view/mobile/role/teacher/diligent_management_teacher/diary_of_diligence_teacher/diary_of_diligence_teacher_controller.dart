import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/model/common/diligence.dart';
import 'package:slova_lms/data/repository/diligence/diligence_repo.dart';
import 'package:slova_lms/view/mobile/role/teacher/teacher_home_controller.dart';

class DiaryOfDiligenceTeacherController extends GetxController{
  final category = ''.obs;
  var focusDateStart = FocusNode().obs;
  var controllerDateStart = TextEditingController().obs;
  var focusDateEnd = FocusNode().obs;
  var controllerDateEnd = TextEditingController().obs;
  var dateStart ="".obs;
  var dateEnd ="".obs;
  var outputDateToDateFormat = DateFormat('dd/MM/yyyy');
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiaryDiligenceTeacher().obs;
  RxList<ItemDiaryDiligence> items = <ItemDiaryDiligence>[].obs;
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;


  @override
  void onInit() {
    super.onInit();
    setDateSchoolYears();
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      controllerDateStart.value.text =   outputDateToDateFormat.format(DateTime(Get.find<TeacherHomeController>().fromYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateToDateFormat.format(DateTime(Get.find<TeacherHomeController>().fromYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      dateEnd.value =outputDateToDateFormat.format(DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      dateStart.value =outputDateToDateFormat.format(DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));

    }else{
      controllerDateStart.value.text =   outputDateToDateFormat.format(DateTime(Get.find<TeacherHomeController>().toYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateToDateFormat.format(DateTime(Get.find<TeacherHomeController>().toYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      dateStart.value =   outputDateToDateFormat.format(DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      dateEnd.value = outputDateToDateFormat.format(DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));

    }

  }

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }

  getDiaryDiligenceTeacherClickDate(fromDate,toDate){
    var classId = Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromDate = DateTime(int.parse(dateStart.value.substring(6,10)), int.parse(dateStart.value.substring(3,5)),int.parse(dateStart.value.substring(0,2))).millisecondsSinceEpoch;
    var toDate = DateTime(int.parse(dateEnd.value.substring(6,10)), int.parse(dateEnd.value.substring(3,5)),int.parse(dateEnd.value.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.getListDiaryDiligenceTeacher(classId, fromDate, toDate).then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value= value.object!;
        items.value = diligenceClass.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  getDiaryDiligenceTeacherToday(){
    var classId = Get.find<TeacherHomeController>().currentClass.value.classId;
    var toDate = DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var fromdate = DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.getListDiaryDiligenceTeacher(classId, fromdate, toDate).then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value= value.object!;
        items.value = diligenceClass.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<TeacherHomeController>().fromYearPresent.value == DateTime.now().year) {
        getDiaryDiligenceTeacherToday();
        return;
      } else {
        fromYearSelect.value = "${Get.find<TeacherHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<TeacherHomeController>().fromYearPresent}";
        getDiaryDiligenceTeacherToYear(fromYearSelect.value, toYearSelect.value);
      }
    } else {
      if (Get.find<TeacherHomeController>().toYearPresent.value == DateTime.now().year) {
        getDiaryDiligenceTeacherToday();
        return;
      } else {
        fromYearSelect.value = "${Get.find<TeacherHomeController>().toYearPresent}";
        toYearSelect.value = "${Get.find<TeacherHomeController>().toYearPresent}";
        getDiaryDiligenceTeacherToYear(fromYearSelect.value, toYearSelect.value);
      }
    }
  }

  getDiaryDiligenceTeacherToYear(fromdate,todate){
    var classId = Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromdate = DateTime(int.parse(toYearSelect.value.substring(0,4)), findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.getListDiaryDiligenceTeacher(classId, fromdate, todate).then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value= value.object!;
        items.value = diligenceClass.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  onRefresh(){
    items.clear();
    getDiaryDiligenceTeacherToday();
    items.refresh();
  }

}