import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/role/teacher/diligent_management_teacher/dedicated_statistics_teacher/dedicated_statistics_page.dart';
import 'package:slova_lms/view/mobile/role/teacher/diligent_management_teacher/diary_of_diligence_teacher/diary_of_diligence_teacher_page.dart';
import '../../../home/home_controller.dart';
import 'attendance_teacher/attendance_teacher_controller.dart';

import 'diligent_management_teacher_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'list_leave_application_teacher/list_leave_application_teacher_controller.dart';

import 'manager_leave_application_teacher/approved_leave_application_teacher/approved_teacher_leave_application_controller.dart';
import 'manager_leave_application_teacher/cancel_leave_application_teacher/cancel_leave_application_teacher_controller.dart';
import 'manager_leave_application_teacher/manager_leave_application_teacher_controller.dart';
import 'manager_leave_application_teacher/pending_teacher/pending_teacher_controller.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';

class DiligentManagementTeacherPage extends GetView<DiligenceManagementTeacherController> {
  @override
  final controller = Get.put(DiligenceManagementTeacherController(), permanent: true);

  DiligentManagementTeacherPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(child: Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        elevation: 0,
        actions: [
         GetBuilder<DiligenceManagementTeacherController>(builder: (controller) {
           return Visibility(
               visible: controller.listItemPopupMenu.isNotEmpty,
               child:  Visibility(
                 visible: controller.isShowPopupMenu.value,
                 child: PopupMenuButton(
                   icon: const Icon(Icons.more_vert),
                     padding: EdgeInsets.zero,
                     shape: RoundedRectangleBorder(
                         borderRadius:
                         BorderRadius.all(Radius.circular(6.0
                             .r))),
                     itemBuilder: (context) {
                       return controller.getListItemPopupMenu();
                     }, onSelected: (value) {
                   if (value == 0) {
                     Get.toNamed(Routes.managerLeaveApplicationTeacherPage);
                     if(Get.isRegistered<ManagerLeaveApplicationTeacherController>()){
                       Get.find<ManagerLeaveApplicationTeacherController>().indexClick = 0;
                       Get.find<ManagerLeaveApplicationTeacherController>().update();
                     }
                   }else if(value== 1){
                     Get.to(DedicatedStatisticsPage());
                   }else if(value== 2){
                     Get.to(DiaryOfDiligenceTeacherPage());
                   }
                 }),
               ));
         },)
        ],
        leading: InkWell(
          onTap: () {
            Get.back();
            Get.delete<DiligenceManagementTeacherController>(force: true);
            Get.delete<ManagerLeaveApplicationTeacherController>(force: true);
            Get.delete<PendingTeacherController>(force: true);
            Get.delete<CancelLeaveApplicationTeacherController>(force: true);
            Get.delete<ApprovedLeaveApplicationTeacherController>(force: true);
            Get.delete<ListLeaveApplicationTeacherController>(force: true);
            Get.delete<AttendanceTeacherController>(force: true);
          },
          child: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        title: Text(
          'Quản Lý Chuyên Cần',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RefreshIndicator(
            color:const Color.fromRGBO(248, 129, 37, 1) ,
            child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  child: Obx(() => Container(
                    margin: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 8.h),
                    padding: EdgeInsets.all(16.h),
                    decoration: BoxDecoration(
                        color: Colors.white, borderRadius: BorderRadius.circular(6)),
                    child:
                    InkWell(
                      onTap: () {
                        showModalBottomSheet(
                            context: context,
                            builder: (context) {
                              return Wrap(
                                children: [
                                  Row(
                                    children: [
                                      TextButton(
                                          onPressed: () {
                                            Get.back();
                                          },
                                          style: ElevatedButton.styleFrom(
                                              backgroundColor: Colors.white),
                                          child: const Text(
                                            "Hủy",
                                            style: TextStyle(
                                                color:
                                                Color.fromRGBO(123, 123, 123, 1)),
                                          )),
                                      Expanded(child: Container()),
                                      TextButton(
                                          onPressed: () {
                                            controller.selectedDate.value = controller.cupertinoDatePicker.value;
                                            controller.onSelectedDateDiligence();
                                            controller.selectedDate.refresh();
                                            Get.back();
                                          },
                                          style: ElevatedButton.styleFrom(
                                              backgroundColor: Colors.white),
                                          child: const Text(
                                            "Xong",
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    248, 129, 37, 1)),
                                          )),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 300, // Just as an example
                                    child: CupertinoDatePicker(
                                      mode: CupertinoDatePickerMode.date,
                                      initialDateTime: controller.cupertinoDatePicker.value,
                                      onDateTimeChanged: (DateTime dateTime) {
                                        controller.cupertinoDatePicker.value = dateTime;
                                      },
                                    ),
                                  ),
                                ],
                              );
                            });
                      },
                      child:  Row(
                        children: [
                          Text(
                            DateFormat('d').format(controller.selectedDate.value),
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 44.sp),
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 4.w),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Thứ ${controller.selectedDate.value.weekday+1}",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14.sp),
                              ),
                              Text(
                                "Tháng ${controller.selectedDate.value.month.toString()}, ${controller.selectedDate.value.year.toString()}",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14.sp),
                              ),
                            ],
                          ),
                          Expanded(child: Container()),
                          Visibility(
                              visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_DETAIL_DILIGENCE),
                              child: InkWell(
                            onTap: () {
                              Get.toNamed(Routes.detailDiligentManagementTeacherPage);
                            },
                            child: Container(
                              padding:
                              EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                              decoration: BoxDecoration(
                                  color: const Color.fromRGBO(253, 221, 196, 1),
                                  borderRadius: BorderRadius.circular(6.r)),
                              child: Text(
                                "Chi Tiết",
                                style: TextStyle(
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w500,
                                    color: const Color.fromRGBO(248, 129, 37, 1)),
                              ),
                            ),
                          ))
                        ],
                      ),
                    ),
                  )),
                ),
                Container(
                  margin: EdgeInsets.only(left: 16.w),
                  height: 40.sp,
                  child: ListView.builder(
                      itemCount: controller.listStatus.length,
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context,index){
                        return GetBuilder<DiligenceManagementTeacherController>(builder: (controller){
                          return TextButton(
                              onPressed: () {
                                controller.indexClick.value = index;
                                controller.showColor(controller.indexClick.value);
                                controller.pageController.animateToPage(index,
                                    duration: const Duration(seconds: 1),
                                    curve: Curves.easeOutBack);
                                controller.update();
                              },
                              child: Obx(() => Text(
                                "${controller.listStatus[index]} (${controller.getListStatus(index) ?? 0})",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14.sp,
                                    color: controller.indexClick.value == index
                                        ? const Color.fromRGBO(248, 129, 37, 1)
                                        : const Color.fromRGBO(177, 177, 177, 1)),
                              )));
                        });
                      }),
                ),
              ],
            ),
          ), onRefresh: () async{
              if(Get.isRegistered<AttendanceTeacherController>()){
                Get.find<AttendanceTeacherController>().onInit();
              }
              if(Get.isRegistered<ListLeaveApplicationTeacherController>()){
                Get.find<ListLeaveApplicationTeacherController>().onInit();
              }
            controller.onInit();
          },),
          Expanded(
              child: PageView(

                onPageChanged: (value) {
                  controller.onPageViewChange(value);
                },
                controller: controller.pageController,
                physics: const ScrollPhysics(),
                children: controller.listPageView,
              )),
          Visibility(
              visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_ATTENDANCE_CLASS),
              child: Obx(() => Visibility(
                visible: Get.isRegistered<AttendanceTeacherController>(),
                  child: Visibility(
                  visible:Get.find<AttendanceTeacherController>().isShowSwitchAttendance.value,
                  child: controller.indexClick.value == 0?Container(
                      color: Colors.white,
                      padding: EdgeInsets.all(16.h),
                      child: SizedBox(
                        width: double.infinity,
                        height: 46,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                backgroundColor: controller.color.value),
                            onPressed: () {
                              controller.confirmAttendance();
                            },
                            child: Text(
                              'Điểm Danh Lớp Học',
                              style: TextStyle(
                                  color: controller.colorText.value, fontSize: 16),
                            )),
                      )):Container()))))
        ],
      ),
    ), onWillPop: ()async{
      Get.back();
      Get.delete<DiligenceManagementTeacherController>(force: true);
      Get.delete<ManagerLeaveApplicationTeacherController>(force: true);
      Get.delete<PendingTeacherController>(force: true);
      Get.delete<CancelLeaveApplicationTeacherController>(force: true);
      Get.delete<ApprovedLeaveApplicationTeacherController>(force: true);
      Get.delete<ListLeaveApplicationTeacherController>(force: true);
      Get.delete<AttendanceTeacherController>(force: true);
      return true;
    });
  }
}
