import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../commom/widget/loading_custom.dart';
import '../../../../home/home_controller.dart';
import 'attendance_teacher_controller.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
class AttendanceTeacherPage extends GetView<AttendanceTeacherController> {
  @override
  final controller = Get.put(AttendanceTeacherController(), permanent: true);

  AttendanceTeacherPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Obx(() =>  SafeArea(
        child: Scaffold(
            body: controller.isReady.value
                ? Column(
              children: [
                Visibility(
                    visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_START_ATTENDANCE),
                    child: Visibility(
                      visible: controller.listStudentDiligence.isNotEmpty,
                      child:  controller.showButtonAttendance(controller.status.value)
                          ? Container(
                        margin: EdgeInsets.only(
                            right: 16.w, top: 8.h, bottom: 8.h),
                        child: Row(
                          children: [
                            Expanded(child: Container()),
                            Text("Đã điểm danh ${controller.listStudentDiligence.length-controller.listStudentNotAttendance.length}/${controller.listStudentDiligence.length}")
                          ],
                        ),
                      )
                          : Container(
                        margin: EdgeInsets.only(right: 16.w),
                        child: Row(
                          children: [
                            Expanded(child: Container()),
                            Visibility(
                              visible: controller.isShowSwitchAttendance.value,
                              child: controller.isAttendance.value
                                  ?InkWell(
                                onTap: () {
                                  controller.setStatusLockAttendance();
                                },
                                child:  Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16.w, vertical: 8.h),
                                  decoration: BoxDecoration(
                                      color: const Color.fromRGBO(255, 69, 89, 1),
                                      borderRadius: BorderRadius.circular(8.r)),
                                  child: Text(
                                    "Kết thúc điểm danh",
                                    style: TextStyle(
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                ),)
                                  : InkWell(
                                onTap: () {
                                  controller.setStatusLockAttendance();
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 16.w, vertical: 8.h),
                                  decoration: BoxDecoration(
                                      color: ColorUtils.PRIMARY_COLOR,
                                      borderRadius: BorderRadius.circular(6.r)),
                                  child: Text(
                                    "Bắt đầu điểm danh",
                                    style: TextStyle(
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )),
                const Padding(padding: EdgeInsets.only(bottom: 8)),
                Expanded(
                    child:
                    controller.listStudentDiligence.isNotEmpty
                        ? ListView.builder(
                        itemCount: controller.listStudentDiligence.length,
                        shrinkWrap: true,
                        physics: const ScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Container(
                            margin: EdgeInsets.only(left: 16.w, right: 16.w, bottom: 8.h),
                            padding: EdgeInsets.only(top: 12.h, bottom: 8.h, right: 8.w, left: 8.w),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.3),
                                      blurRadius: 1,
                                      offset:  const Offset(0.3,0.5)
                                  )
                                ]
                            ),
                            child: Obx(() => controller.groupValue.isNotEmpty?Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(right: 8.w, left: 8.w),
                                  child: Row(
                                    children: [
                                      SizedBox(
                                        width: 32.h,
                                        height: 32.h,
                                        child:
                                        CacheNetWorkCustom(urlImage: '${ controller.listStudentDiligence[index].student!.image }'),

                                      ),
                                      Padding(
                                          padding:
                                          EdgeInsets.only(
                                              right: 8.w)),
                                      Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            controller.listStudentDiligence[index].student!.fullName!,
                                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500, fontSize: 14.sp),
                                          ),
                                          Padding(
                                              padding: EdgeInsets.only(top: 4.h)),
                                          Visibility(
                                              visible: controller.listStudentDiligence[index].student!.birthday!=null,
                                              child: Text(
                                                controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.listStudentDiligence[index].student!.birthday??0)),
                                                style: TextStyle(
                                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 12.sp),
                                              ))
                                        ],
                                      ),
                                      Expanded(
                                          child: Container()),
                                      Visibility(
                                          visible: controller.getStatusConfirmAttendance(controller.status.value),
                                          child: controller.isShowButtonEdit[index]
                                              ? InkWell(
                                            onTap: () {
                                              controller.isShowButtonEdit[index] = false;
                                              controller.colorItem(index);
                                              controller.isShowButtonEdit.refresh();
                                            },
                                            child:
                                            Container(
                                              padding:
                                              const EdgeInsets.all(8),
                                              decoration: BoxDecoration(color: ColorUtils.PRIMARY_COLOR,
                                                  borderRadius:
                                                  BorderRadius.circular(6.r)),
                                              child: const Icon(
                                                Icons.edit,
                                                color: Colors.white,
                                                size: 14,
                                              ),
                                            ),
                                          )
                                              : Row(
                                            children: [
                                              InkWell(
                                                onTap:
                                                    () {
                                                  controller.isShowButtonEdit[index] = true;
                                                  controller.colorItem(index);
                                                  controller.groupValue[index] = controller.getStatusDiligence(controller.listStudentDiligence[index].statusDiligent);
                                                  controller.isShowButtonEdit.refresh();
                                                },
                                                child:
                                                Container(
                                                  padding:
                                                  const EdgeInsets.all(8),
                                                  decoration: BoxDecoration(
                                                      color:
                                                      Colors.red,
                                                      borderRadius: BorderRadius.circular(6.r)),
                                                  child:
                                                  const Icon(
                                                    Icons
                                                        .clear,
                                                    color:
                                                    Colors.white,
                                                    size:
                                                    14,
                                                  ),
                                                ),
                                              ),
                                              Padding(padding: EdgeInsets.only(right: 8.w)),
                                              InkWell(
                                                onTap: () {
                                                  controller.updateAttendance(index,controller.groupValue[index]);
                                                  controller.isShowButtonEdit[index] = true;
                                                  controller.colorItem(index);
                                                  controller.isShowButtonEdit.refresh();
                                                  AppUtils.shared.showToast("Cập nhật thông tin điểm danh thành công!");
                                                },
                                                child:  Container(
                                                  padding: const EdgeInsets.all(8),
                                                  decoration: BoxDecoration(
                                                      color: Colors.green,
                                                      borderRadius: BorderRadius.circular(6.r)),
                                                  child:
                                                  const Icon(Icons.check, color: Colors.white,size: 14,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ))
                                    ],
                                  ),
                                ),
                                Padding(
                                    padding: EdgeInsets.only(
                                        top: 16.h)),
                                Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment
                                          .start,
                                      children: [
                                        Expanded(
                                          flex: 1,
                                          child: SizedBox(
                                            child:
                                            RadioListTile(
                                                title: Transform
                                                    .translate(
                                                  offset: const Offset(
                                                      -8,
                                                      0),
                                                  child:
                                                  Text(
                                                    "Đi học đúng giờ",
                                                    style: TextStyle(
                                                        fontSize: 14.sp,
                                                        fontWeight: FontWeight.w400,
                                                        color: controller.colorTextRadioListTile[index]),
                                                  ),
                                                ),
                                                value:
                                                1,
                                                dense:
                                                true,
                                                visualDensity:
                                                const VisualDensity(
                                                  horizontal:
                                                  VisualDensity.minimumDensity,
                                                  vertical:
                                                  VisualDensity.minimumDensity,
                                                ),
                                                contentPadding:
                                                EdgeInsets
                                                    .zero,
                                                activeColor: ColorUtils.PRIMARY_COLOR,
                                                groupValue:
                                                controller.groupValue[index],
                                                onChanged:
                                                    (int?
                                                value) {
                                                  controller.onclickAttendance(
                                                      index,
                                                      value);
                                                }),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: SizedBox(
                                            child:
                                            RadioListTile(
                                                title: Transform
                                                    .translate(
                                                  offset: const Offset(
                                                      -8,
                                                      0),
                                                  child:
                                                  Text(
                                                    "Đi học muộn",
                                                    style: TextStyle(
                                                        fontSize: 14.sp,
                                                        fontWeight: FontWeight.w400,
                                                        color: controller.colorTextRadioListTile[index]),
                                                  ),
                                                ),
                                                value:
                                                2,
                                                dense:
                                                true,
                                                visualDensity:
                                                const VisualDensity(
                                                  horizontal:
                                                  VisualDensity.minimumDensity,
                                                  vertical:
                                                  VisualDensity.minimumDensity,
                                                ),
                                                contentPadding:
                                                EdgeInsets
                                                    .zero,
                                                activeColor: ColorUtils.PRIMARY_COLOR,
                                                groupValue:
                                                controller.groupValue[
                                                index],
                                                onChanged:
                                                    (int?
                                                value) {
                                                  controller.onclickAttendance(
                                                      index,
                                                      value);
                                                }),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Padding(
                                        padding:
                                        EdgeInsets.only(
                                            top: 8.h)),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment
                                          .start,
                                      children: [
                                        Expanded(
                                          flex: 1,
                                          child: SizedBox(
                                            child:
                                            RadioListTile(
                                                title: Transform
                                                    .translate(
                                                  offset: const Offset(
                                                      -8,
                                                      0),
                                                  child:
                                                  Text(
                                                    "Nghỉ có phép",
                                                    style: TextStyle(
                                                        fontSize: 14.sp,
                                                        fontWeight: FontWeight.w400,
                                                        color: controller.colorTextRadioListTile[index]),
                                                  ),
                                                ),
                                                value:
                                                3,
                                                dense:
                                                true,
                                                contentPadding:
                                                EdgeInsets
                                                    .zero,
                                                visualDensity:
                                                const VisualDensity(
                                                  horizontal:
                                                  VisualDensity.minimumDensity,
                                                  vertical:
                                                  VisualDensity.minimumDensity,
                                                ),
                                                activeColor: ColorUtils.PRIMARY_COLOR,
                                                groupValue:
                                                controller.groupValue[
                                                index],
                                                onChanged:
                                                    (int?
                                                value) {
                                                  controller.onclickAttendance(
                                                      index,
                                                      value);
                                                }),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: SizedBox(
                                            child:
                                            RadioListTile(
                                                title: Transform
                                                    .translate(
                                                  offset: const Offset(
                                                      -8,
                                                      0),
                                                  child:
                                                  Text(
                                                    "Nghỉ không phép",
                                                    style: TextStyle(
                                                        fontSize: 14.sp,
                                                        fontWeight: FontWeight.w400,
                                                        color: controller.colorTextRadioListTile[index]),
                                                  ),
                                                ),
                                                dense:
                                                true,
                                                contentPadding:
                                                EdgeInsets
                                                    .zero,
                                                visualDensity:
                                                const VisualDensity(
                                                  horizontal:
                                                  VisualDensity.minimumDensity,
                                                  vertical:
                                                  VisualDensity.minimumDensity,
                                                ),
                                                activeColor: ColorUtils.PRIMARY_COLOR,
                                                value:
                                                4,
                                                groupValue:
                                                controller.groupValue[
                                                index],
                                                onChanged:
                                                    (int?
                                                value) {
                                                  controller.onclickAttendance(
                                                      index,
                                                      value);
                                                }),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ):Container()),
                          );
                        })
                        : Container())
              ],
            )
                :  const LoadingCustom(),)));
  }
}
