import 'dart:ui';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/app_cache.dart';
import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../data/base_service/api_response.dart';
import '../../../../../../data/model/common/diligence.dart';
import '../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../teacher_home_controller.dart';
import '../diligent_management_teacher_controller.dart';

class AttendanceTeacherController extends GetxController {
  DateTime now = DateTime.now();
  RxInt indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  var groupValue = <int>[].obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudentDiligence = <Diligents>[].obs;
  var student = StudentDiligent().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var colorTextRadioListTile = <Color>[].obs;
  var isAttendance = false.obs;
  var isReady = false.obs;
  var isConfirmAttendance = false.obs;
  var isShowSwitchAttendance = false.obs;
  var isShowButtonEdit = <bool>[].obs;
  var status = "".obs;
  var selectedDay = DateTime.now().obs;
  var statusCode = 0.obs;
  var listStudentNotAttendance = <int>[].obs;

  @override
  void onInit() {
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      selectedDay.value = DateTime(Get.find<TeacherHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
    }else{
      selectedDay.value = DateTime(Get.find<TeacherHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
    }
    getListStudentDiligence();
    checkHomeRoomTeacher();
    super.onInit();
  }

  showColor(index) {
    click.value = [];
    for (int i = 0; i < 3; i++) {
      click.add(false);
    }
    click[index] = true;
  }

  getListStudentDiligence() async{
    var classId = Get.find<TeacherHomeController>().currentClass.value.classId;
    groupValue.value = [];
    listStudentNotAttendance.value = [];
    var fromDate = DateTime(selectedDay.value.year, selectedDay.value.month,
            selectedDay.value.day, 00, 00)
        .millisecondsSinceEpoch;
    var toDate = DateTime(selectedDay.value.year, selectedDay.value.month,
            selectedDay.value.day, 23, 59)
        .millisecondsSinceEpoch;

    await _diligenceRepo
        .getListStudentDiligence(classId, fromDate, toDate, "")
        .then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudentDiligence.value = diligenceClass.value.studentDiligent!;
        if (diligenceClass.value.status != null) {
          status.value = diligenceClass.value.status!;
        } else {
          status.value = "";
        }
        isAttendance.value = getStatusLock(diligenceClass.value.isLock);
        for (int i = 0; i < listStudentDiligence.length; i++) {
          groupValue.add(getStatusDiligence(listStudentDiligence[i].statusDiligent ?? ""));
          if(status.value == "CONFIRM"){
            colorTextRadioListTile.add(const Color.fromRGBO(133, 133, 133, 1));
          }else{
            if (isAttendance.value == false) {
              colorTextRadioListTile.add( const Color.fromRGBO(133, 133, 133, 1));
            } else {
              colorTextRadioListTile.add(const Color.fromRGBO(26, 26, 26, 1));
            }
          }
          isShowButtonEdit.add(true);
        }
        if(groupValue.isNotEmpty){
          for (int i = 0; i < groupValue.length; i++){
            if(groupValue[i] == 0){
              listStudentNotAttendance.add(groupValue[i]);
            }
          }
        }
        if(diligenceClass.value.lockedByUser?.id != AppCache().userId){
          isAttendance.value = false;
        }
        listStudentNotAttendance.refresh();
        groupValue.refresh();
        Get.find<DiligenceManagementTeacherController>()
            .setColorButtonAttendance(groupValue);
        Get.find<DiligenceManagementTeacherController>()
            .setColorTextButtonAttendance(groupValue);
      }
    });
    isReady.value = true;
    diligenceClass.refresh();
  }

  getStatusLock(status) {
    switch (status) {
      case "TRUE":
        return true;
      case "FALSE":
        return false;
    }
  }

  checkHomeRoomTeacher() {
    if(selectedDay.value.year == DateTime.now().year &&selectedDay.value.month == DateTime.now().month
        &&selectedDay.value.day == DateTime.now().day){
      if (Get.find<TeacherHomeController>().userProfile.value.classesOfHomeroomTeacher?.contains(Get.find<TeacherHomeController>().currentClass.value.classId) == true) {
        isShowSwitchAttendance.value = true;
      } else {
        isShowSwitchAttendance.value = false;
      }
    }else{
      isShowSwitchAttendance.value = false;
    }
  }

  getStatusConfirmAttendance(status) {
    if (Get.find<TeacherHomeController>().userProfile.value.classesOfHomeroomTeacher?.contains(Get.find<TeacherHomeController>().currentClass.value.classId) == true) {
      switch (status) {
        case "DRAFT":
          return false;
        case "CONFIRM":
          return true;
        default:
          return false;
      }
    } else {
      return false;
    }
  }


  showButtonAttendance(status){
    switch (status) {
      case "DRAFT":
        return false;
      case "CONFIRM":
        return true;
      default:
        return false;
    }
  }

  getStatusDiligence(status) {
    switch (status) {
      case "ON_TIME":
        return 1;
      case "NOT_ON_TIME":
        return 2;
      case "EXCUSED_ABSENCE":
        return 3;
      case "ABSENT_WITHOUT_LEAVE":
        return 4;
      default:
        return 0;
    }
  }

  openAttendance() {
    var id = diligenceClass.value.id!;
    _diligenceRepo.toggleAttendance(id, "TRUE").then((value) {
      if (value.state == Status.SUCCESS) {
        isAttendance.value = true;
        colorAttendance(isAttendance.value);
        getListStudentDiligence();
      }else{
        if(statusCode.value == 400){
          AppUtils().snackbarError("Thông báo", "Có người đang điểm danh chuyên cần");
          isAttendance.value = false;
        }
      }
    });
  }

  closeAttendance() {
    var id = diligenceClass.value.id!;
    _diligenceRepo.toggleAttendance(id, "FALSE").then((value) {
      if (value.state == Status.SUCCESS) {
        isAttendance.value = false;
        colorAttendance(isAttendance.value);
        getListStudentDiligence();
      }
      else{
        if(statusCode.value == 400){
          AppUtils().snackbarError("Thông báo", "Có người đang điểm danh chuyên cần");
        }
      }
    });
  }



  setStatusLockAttendance() {
    if(isAttendance.value == true){
      closeAttendance();
      Get.find<DiligenceManagementTeacherController>().setColorButtonAttendance(groupValue);
      Get.find<DiligenceManagementTeacherController>().setColorTextButtonAttendance(groupValue);
    }else{
      openAttendance();
      Get.find<DiligenceManagementTeacherController>()
          .setColorButtonAttendance(groupValue);
      Get.find<DiligenceManagementTeacherController>()
          .setColorTextButtonAttendance(groupValue);
    }
  }

  colorAttendance(isOpen) {
    if (isOpen != true) {
      for (int i = 0; i < listStudentDiligence.length; i++) {
        colorTextRadioListTile[i] = const Color.fromRGBO(133, 133, 133, 1);
      }
      colorTextRadioListTile.refresh();
    } else {
      for (int i = 0; i < listStudentDiligence.length; i++) {
        colorTextRadioListTile[i] = const Color.fromRGBO(26, 26, 26, 1);
      }
      colorTextRadioListTile.refresh();
    }
  }

  colorItem(index) {
    if (isShowButtonEdit[index] == false) {
      colorTextRadioListTile[index] = const Color.fromRGBO(26, 26, 26, 1);
      colorTextRadioListTile.refresh();
    } else {
      colorTextRadioListTile[index] = const Color.fromRGBO(133, 133, 133, 1);
      colorTextRadioListTile.refresh();
    }
  }

  attendanceStudent(statusDiligent, index,valueCheck) async {
    var id = diligenceClass.value.id;
    _diligenceRepo.attendanceStudent(id, statusDiligent, listStudentDiligence[index].id).then((value) {
      if (value.state == Status.SUCCESS) {
        groupValue[index] = valueCheck;
        groupValue.refresh();
        Get.find<DiligenceManagementTeacherController>()
            .setColorButtonAttendance(groupValue);
        Get.find<DiligenceManagementTeacherController>()
            .setColorTextButtonAttendance(groupValue);
      }
      else{
        if(statusCode.value == 4092){
          AppUtils().snackbarError("Thông báo", "Điểm danh chuyên cần đã bị khóa bởi người khác");
          getListStudentDiligence();
          Get.find<DiligenceManagementTeacherController>()
              .setColorButtonAttendance(groupValue);
          Get.find<DiligenceManagementTeacherController>()
              .setColorTextButtonAttendance(groupValue);
        }
      }
    });
  }

  updateAttendanceStudent(statusDiligent, index) async {
    var id = diligenceClass.value.id;
    _diligenceRepo
        .updateAttendanceStudent(
            id, statusDiligent, listStudentDiligence[index].id)
        .then((value) {
      if (value.state == Status.SUCCESS) {}
    });
  }

  onclickAttendance(index, value) {
    if(selectedDay.value.year == DateTime.now().year &&selectedDay.value.month == DateTime.now().month
        &&selectedDay.value.day == DateTime.now().day){
      if (isShowSwitchAttendance.value) {
        if (diligenceClass.value.status == "CONFIRM") {
          if (isShowButtonEdit[index] == false) {
            groupValue[index] = value!;
            groupValue.refresh();
          }
        } else {
          if (diligenceClass.value.lockedByUser?.id == AppCache().userId || diligenceClass.value.lockedByUser?.id == ""|| diligenceClass.value.lockedByUser?.id == null){
            if (isAttendance.value == true) {
              switch (value) {
                case 1:
                  attendanceStudent("ON_TIME", index,value);
                  break;
                case 2:
                  attendanceStudent("NOT_ON_TIME", index,value);
                  break;
                case 3:
                  attendanceStudent("EXCUSED_ABSENCE", index,value);
                  break;
                case 4:
                  attendanceStudent("ABSENT_WITHOUT_LEAVE", index,value);
                  break;
              }
              Get.find<DiligenceManagementTeacherController>()
                  .setColorButtonAttendance(groupValue);
              Get.find<DiligenceManagementTeacherController>()
                  .setColorTextButtonAttendance(groupValue);
              Get.find<DiligenceManagementTeacherController>().update();
            }
          }else{
            AppUtils().showToast("${diligenceClass.value.lockedByUser?.fullName??""} đang điểm danh");
          }

        }
      }
    }
  }

  updateAttendance(index, value) {
    switch (value) {
      case 1:
        updateAttendanceStudent("ON_TIME", index);
        break;
      case 2:
        updateAttendanceStudent("NOT_ON_TIME", index);
        break;
      case 3:
        updateAttendanceStudent("EXCUSED_ABSENCE", index);
        break;
      case 4:
        updateAttendanceStudent("ABSENT_WITHOUT_LEAVE", index);
        break;
    }
    groupValue.refresh();
    Get.find<DiligenceManagementTeacherController>()
        .setColorButtonAttendance(groupValue);
    Get.find<DiligenceManagementTeacherController>()
        .setColorTextButtonAttendance(groupValue);
  }
}
