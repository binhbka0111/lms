import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../teacher_home_controller.dart';


class ListLeaveApplicationTeacherController extends GetxController{
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var leavingApplication = LeavingApplication().obs;
  var itemsLeavingApplications = <ItemsLeavingApplication>[].obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var selectedDay = DateTime.now().obs;
  var parents = <String>[].obs;
  var listApprove = <ItemsLeavingApplication>[].obs;
  var listRefuse = <ItemsLeavingApplication>[].obs;
  @override
  void onInit() {
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      selectedDay.value = DateTime(Get.find<TeacherHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
    }else{
      selectedDay.value = DateTime(Get.find<TeacherHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
    }

    getListLeavingApplicationTeacher();
    super.onInit();

  }

  getListLeavingApplicationTeacher() async{
    itemsLeavingApplications.clear();
    listApprove.clear();
    listRefuse.clear();
    var classId =Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromDate = DateTime(selectedDay.value.year,selectedDay.value.month,selectedDay.value.day,00,00,00).toLocal().millisecondsSinceEpoch;
    var toDate = DateTime(selectedDay.value.year,selectedDay.value.month,selectedDay.value.day,23,59,59).toLocal().millisecondsSinceEpoch;
    _diligenceRepo.getListLeavingApplicationTeacher(classId,fromDate,toDate,"","TIMELEAVE","").then((value) {
      if (value.state == Status.SUCCESS) {
        itemsLeavingApplications.value = value.object!;
        listApprove.value = itemsLeavingApplications.where((element) => element.status == "APPROVE").toList();
        listRefuse.value = itemsLeavingApplications.where((element) => element.status == "REFUSE").toList();
        listApprove.addAll(listRefuse);
      }
    });
    itemsLeavingApplications.refresh();
    listApprove.refresh();
    listRefuse.refresh();
  }


  getStatus(status){
    switch(status){
      case "CANCEL":
        return "Đã hủy";
      case "PENDING":
        return "Chờ duyệt";
      case "REFUSE":
        return "Từ chối";
      case "APPROVE":
        return "Đã duyệt";
    }
  }


  getColorTextStatus(status){
    switch(status){
      case "PENDING":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "APPROVE":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "REFUSE":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "CANCEL":
        return const Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getColorBackgroundStatus(status){
    switch(status){
      case "PENDING":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "APPROVE":
        return const Color.fromRGBO(192, 242, 220, 1);
      case "REFUSE":
        return const Color.fromRGBO(252, 211, 215, 1);
      case "CANCEL":
        return const Color.fromRGBO(252, 211, 215, 1);
    }
  }







}