import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../../../../routes/app_pages.dart';
import '../../../teacher_home_controller.dart';
import '../../diligent_management_teacher_controller.dart';
import '../approved_leave_application_teacher/approved_teacher_leave_application_controller.dart';
import '../manager_leave_application_teacher_controller.dart';

class PendingTeacherController extends GetxController {
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var leavingApplication = LeavingApplication().obs;
  var itemsLeavingApplications = <ItemsLeavingApplication>[].obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var selectedDay = DateTime.now().obs;
  var isHomeRoomTeacher = false.obs;
  var parents = <String>[].obs;
  var controllerDateStart = TextEditingController().obs;
  var controllerDateEnd = TextEditingController().obs;
  var controllerFeedback = TextEditingController().obs;
  var studentId = "".obs;
  var typeTimeLeaveApplication = "Thời gian nghỉ".obs;
  @override
  void onInit() {
    super.onInit();
    if(Get.isRegistered<DiligenceManagementTeacherController>()){
      selectedDay.value = DateTime(
          Get.find<DiligenceManagementTeacherController>().selectedDate.value.year,
          Get.find<DiligenceManagementTeacherController>().selectedDate.value.month,
          Get.find<DiligenceManagementTeacherController>().selectedDate.value.day);
    }

    if(Get.isRegistered<ManagerLeaveApplicationTeacherController>()){
      controllerDateStart.value.text = Get.find<ManagerLeaveApplicationTeacherController>().controllerDateStart.value.text;
      controllerDateEnd.value.text = Get.find<ManagerLeaveApplicationTeacherController>().controllerDateEnd.value.text;
      studentId.value = Get.find<ManagerLeaveApplicationTeacherController>().studentId.value;
      typeTimeLeaveApplication.value = Get.find<ManagerLeaveApplicationTeacherController>().typeTimeLeaveApplication.value;
    }else{
      studentId.value = "";
      typeTimeLeaveApplication.value = "Thời gian tạo";
      controllerDateStart.value.text =   outputDateFormat.format(DateTime(findFirstDateOfTheWeek(selectedDay.value).year, findFirstDateOfTheWeek(selectedDay.value).month,findFirstDateOfTheWeek(selectedDay.value).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(findLastDateOfTheWeek(selectedDay.value).year, findLastDateOfTheWeek(selectedDay.value).month,findLastDateOfTheWeek(selectedDay.value).day));
    }
    getListLeavingApplicationPendingTeacher();
    checkHomeRoomTeacher();

  }

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }

  getListLeavingApplicationPendingTeacher() async {
    itemsLeavingApplications.clear();
    var classId = Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromdate = DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2),),00,00,00).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2)),23,59,59).millisecondsSinceEpoch;
    _diligenceRepo
        .getListLeavingApplicationTeacher(classId, fromdate, todate, "PENDING",getTypeTimeLeaveApplication(typeTimeLeaveApplication.value),studentId.value)
        .then((value) {
      if (value.state == Status.SUCCESS) {
        itemsLeavingApplications.value = value.object!;
      }
    });
    itemsLeavingApplications.refresh();
  }


  getTypeTimeLeaveApplication(type){
    switch(type){
      case "Thời gian tạo":
        return "CREATEAT";
      case "Thời gian nghỉ":
        return "TIMELEAVE";
    }
  }

  approveLeaveApplication(index) async {
    _diligenceRepo
        .approveLeaveApplication(itemsLeavingApplications[index].id, controllerFeedback.value.text.trim())
        .then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Duyệt đơn thành công");
        Get.find<ApprovedLeaveApplicationTeacherController>().onInit();
      }
    });
  }

  refuseLeaveApplication(index) async {
    _diligenceRepo
        .refuseLeaveApplication(itemsLeavingApplications[index].id, controllerFeedback.value.text.trim())
        .then((value) {
      if (value.state == Status.SUCCESS) {
        if(Get.isRegistered<DiligenceManagementTeacherController>()){
          Get.find<DiligenceManagementTeacherController>().onInit();
        }
        AppUtils.shared.showToast("Từ chối đơn thành công");
      }
    });
  }

  checkHomeRoomTeacher() {
    if (Get.find<TeacherHomeController>()
            .userProfile
            .value
            .classesOfHomeroomTeacher
            ?.contains(
                Get.find<TeacherHomeController>().currentClass.value.classId) ==
        true) {
      isHomeRoomTeacher.value = true;
    } else {
      isHomeRoomTeacher.value = false;
    }
  }

  getColorTextStatus(status) {
    switch (status) {
      case "PENDING":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "APPROVE":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "REFUSE":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "CANCEL":
        return const Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getColorBackgroundStatus(status) {
    switch (status) {
      case "PENDING":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "APPROVE":
        return const Color.fromRGBO(192, 242, 220, 1);
      case "REFUSE":
        return const Color.fromRGBO(252, 211, 215, 1);
      case "CANCEL":
        return const Color.fromRGBO(252, 211, 215, 1);
    }
  }

  getStatus(status) {
    switch (status) {
      case "CANCEL":
        return "Đã hủy";
      case "PENDING":
        return "Chờ duyệt";
      case "REFUSE":
        return "Từ chối";
      case "APPROVE":
        return "Đã duyệt";
    }
  }

  setTextDialog(status){
    switch (status) {
      case "REFUSE":
        return "Xác nhận từ chối";
      case "APPROVE":
        return "Xác nhận duyệt đơn";
    }
  }


  goToDetailPendingLeaveApplication(index){
    Get.toNamed(Routes.detailPendingLeaveApplicationTeacher,arguments: itemsLeavingApplications[index].id);
  }
}
