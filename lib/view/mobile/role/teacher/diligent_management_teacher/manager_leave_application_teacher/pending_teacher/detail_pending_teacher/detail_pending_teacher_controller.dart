import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../../../../../data/model/common/detail_notify_leaving_application.dart';
import '../../../../../../../../data/repository/notification_repo/notification_repo.dart';
import '../../../../teacher_home_controller.dart';
import '../../approved_leave_application_teacher/approved_teacher_leave_application_controller.dart';

class DetailPendingTeacherController extends GetxController{
  var focusFeedback = FocusNode();
  var controllerFeedback = TextEditingController();
  final DiligenceRepo _diligenceRepo = DiligenceRepo();

  var itemsLeavingApplication = ItemsLeavingApplication().obs;
  String getTempIFSCValidation(String text) {
    return text.length > 7 ? "This line is helper text" : "";
  }
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var isHomeRoomTeacher = false.obs;
  var transId = "".obs;
  final NotificationRepo _notificationRepo = NotificationRepo();
  var detailNotifyLeavingApplication = DetailNotifyLeavingApplication().obs;
  WebViewController? controllerWebView;
  var isReady = false.obs;
  @override
  void onInit() {
    var tmpTransId = Get.arguments;
    if (tmpTransId != null) {
      transId.value = tmpTransId;
      getDetailNotify();
    }
    checkHomeRoomTeacher();
    super.onInit();
  }


  getDetailNotify() async{
    await _notificationRepo.getDetailNotifyLeavingApplication(transId.value).then((value) {
      if (value.state == Status.SUCCESS) {
        detailNotifyLeavingApplication.value = value.object!;
      }
    });
    isReady.value = true;
  }


  approveLeaveApplication() async{
    _diligenceRepo.approveLeaveApplication(detailNotifyLeavingApplication.value.id,controllerFeedback.value.text).then((value) {
      if (value.state == Status.SUCCESS) {
        Get.find<ApprovedLeaveApplicationTeacherController>().onInit();
        AppUtils.shared.showToast("Duyệt đơn thành công");
      }
    });
  }


  refuseLeaveApplication() async{
    _diligenceRepo.refuseLeaveApplication(detailNotifyLeavingApplication.value.id,controllerFeedback.value.text).then((value) {
      if (value.state == Status.SUCCESS) {
        Get.find<ApprovedLeaveApplicationTeacherController>().onInit();
        AppUtils.shared.showToast("Từ chối đơn thành công");
      }
    });
  }


  checkHomeRoomTeacher(){
    if(Get.find<TeacherHomeController>().userProfile.value.classesOfHomeroomTeacher?.contains(Get.find<TeacherHomeController>().currentClass.value.classId) == true){
      isHomeRoomTeacher.value = true;
    }else{
      isHomeRoomTeacher.value = false;
    }
  }

  getColorTextStatus(status) {
    switch (status) {
      case "PENDING":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "APPROVE":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "REFUSE":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "CANCEL":
        return const Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getColorBackgroundStatus(status) {
    switch (status) {
      case "PENDING":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "APPROVE":
        return const Color.fromRGBO(192, 242, 220, 1);
      case "REFUSE":
        return const Color.fromRGBO(252, 211, 215, 1);
      case "CANCEL":
        return const Color.fromRGBO(252, 211, 215, 1);
    }
  }

  getStatus(status) {
    switch (status) {
      case "CANCEL":
        return "Đã hủy";
      case "PENDING":
        return "Chờ duyệt";
      case "REFUSE":
        return "Từ chối";
      case "APPROVE":
        return "Đã duyệt";
    }
  }



}