import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import '../../../../../../../../commom/utils/global.dart';
import '../../../../../../../../commom/widget/loading_custom.dart';
import '../../../attendance_teacher/attendance_teacher_controller.dart';
import '../../../diligent_management_teacher_controller.dart';
import '../../../list_leave_application_teacher/list_leave_application_teacher_controller.dart';
import '../../approved_leave_application_teacher/approved_teacher_leave_application_controller.dart';
import '../../manager_leave_application_teacher_controller.dart';
import '../../pending_teacher/pending_teacher_controller.dart';
import '../cancel_leave_application_teacher_controller.dart';
import 'detail_cancel_leave_application_teacher_controller.dart';
class DetailCancelLeaveApplicationTeacherPage
    extends GetWidget<DetailCancelLeaveApplicationTeacherController> {
  @override
  final controller = Get.put(DetailCancelLeaveApplicationTeacherController());

  DetailCancelLeaveApplicationTeacherPage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Obx(() => WillPopScope(
        child:controller.isReady.value
            ? SafeArea(
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: ColorUtils.PRIMARY_COLOR,
                elevation: 0,
                actions: [
                  InkWell(
                    onTap: () {
                      comeToHome();
                      Get.delete<DiligenceManagementTeacherController>(force: true);
                      Get.delete<ManagerLeaveApplicationTeacherController>(force: true);
                      Get.delete<PendingTeacherController>(force: true);
                      Get.delete<CancelLeaveApplicationTeacherController>(force: true);
                      Get.delete<ApprovedLeaveApplicationTeacherController>(force: true);
                      Get.delete<ListLeaveApplicationTeacherController>(force: true);
                      Get.delete<AttendanceTeacherController>(force: true);
                    },
                    child: const Icon(
                      Icons.home,
                      color: Colors.white,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(right: 16.w))
                ],
                title: const Text(
                  'Chi Tiết Đơn Nghỉ Phép',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: 'static/Inter-Medium.ttf'),
                ),
              ),
              body: GestureDetector(
                onTap: () {
                  dismissKeyboard();
                },
                child: Column(
                  children: [
                    Expanded(
                        child:
                        SingleChildScrollView(
                          physics: const BouncingScrollPhysics(),
                          child: Column(
                            children: [
                              Visibility(
                                  visible: controller.detailNotifyLeavingApplication.value.html != null,
                                  child: Container(
                                    decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(6)),
                                    margin: EdgeInsets.symmetric(horizontal: 16.w,vertical: 16.h),
                                    padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 16.h),
                                    child: Html(data: controller.detailNotifyLeavingApplication.value.html??"",
                                    ),
                                  )),
                              Container(
                                decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(6)),
                                margin: EdgeInsets.symmetric(horizontal: 16.w,vertical: 8.h),
                                padding: EdgeInsets.all(16.h),
                                child:  Column(
                                  children: [
                                    Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            width: 80.w,
                                            child: Text(
                                              "Trạng thái:",
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 4.h),
                                            decoration: BoxDecoration(
                                                color: controller.getColorBackgroundStatus(controller.detailNotifyLeavingApplication.value.status),
                                                borderRadius: BorderRadius.circular(6)
                                            ),
                                            child: Text(
                                              "${controller.getStatus(controller.detailNotifyLeavingApplication.value.status)??""}",
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  color: controller.getColorTextStatus(controller.detailNotifyLeavingApplication.value.status),
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                          Expanded(child: Container()),
                                        ]),
                                    Padding(padding: EdgeInsets.only(top: 8.h)),
                                    Row(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            width: 100.w,
                                            child: Text(
                                              "Lý do hủy đơn:",
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                          Visibility(visible: controller.detailNotifyLeavingApplication.value.feedback != ""||controller.detailNotifyLeavingApplication.value.feedback != null,
                                            child: Text(
                                              controller.detailNotifyLeavingApplication.value.feedback??"",
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontWeight: FontWeight.w400),
                                            ),)
                                        ]),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )),

                  ],
                ),
              ),
            ))
            :const LoadingCustom(),
        onWillPop: ()async{
          Get.back();
          if(Get.isRegistered<ManagerLeaveApplicationTeacherController>()){
            Get.find<ManagerLeaveApplicationTeacherController>().onRefresh();
            Get.find<ManagerLeaveApplicationTeacherController>().update();
          }
          return true;
        }));
  }
}
