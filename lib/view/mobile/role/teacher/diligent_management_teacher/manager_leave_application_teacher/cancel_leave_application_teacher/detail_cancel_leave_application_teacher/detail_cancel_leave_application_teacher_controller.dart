import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/detail_notify_leaving_application.dart';
import '../../../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../../../data/repository/notification_repo/notification_repo.dart';
class DetailCancelLeaveApplicationTeacherController extends GetxController {
  var itemsLeavingApplication = ItemsLeavingApplication().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var isHomeRoomTeacher = false.obs;
  var transId = "".obs;
  final NotificationRepo _notificationRepo = NotificationRepo();
  var detailNotifyLeavingApplication = DetailNotifyLeavingApplication().obs;
  var isReady = false.obs;
  @override
  void onInit() {
    var tmpTransId = Get.arguments;
    if (tmpTransId != null) {
      transId.value = tmpTransId;
      getDetailNotify();
    }
    super.onInit();
  }


  getDetailNotify() async{
   await _notificationRepo.getDetailNotifyLeavingApplication(transId.value).then((
        value) {
      if (value.state == Status.SUCCESS) {
        detailNotifyLeavingApplication.value = value.object!;
      }
    });
    isReady.value = true;
  }


  getColorTextStatus(status) {
    switch (status) {
      case "PENDING":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "APPROVE":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "REFUSE":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "CANCEL":
        return const Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getColorBackgroundStatus(status) {
    switch (status) {
      case "PENDING":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "APPROVE":
        return const Color.fromRGBO(192, 242, 220, 1);
      case "REFUSE":
        return const Color.fromRGBO(252, 211, 215, 1);
      case "CANCEL":
        return const Color.fromRGBO(252, 211, 215, 1);
    }
  }

  getStatus(status) {
    switch (status) {
      case "CANCEL":
        return "Đã hủy";
      case "PENDING":
        return "Chờ duyệt";
      case "REFUSE":
        return "Từ chối";
      case "APPROVE":
        return "Đã duyệt";
    }
  }
}