import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../teacher_home_controller.dart';
import '../../diligent_management_teacher_controller.dart';
import '../manager_leave_application_teacher_controller.dart';
import '../../../../../../../routes/app_pages.dart';
class CancelLeaveApplicationTeacherController extends GetxController{

  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var leavingApplication = LeavingApplication().obs;
  var itemsLeavingApplications = <ItemsLeavingApplication>[].obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var selectedDay = DateTime.now().obs;
  var controllerDateStart = TextEditingController().obs;
  var controllerDateEnd = TextEditingController().obs;
  var studentId = "".obs;
  var typeTimeLeaveApplication = "Thời gian nghỉ".obs;
  @override
  void onInit() {
    if(Get.isRegistered<DiligenceManagementTeacherController>()){
      selectedDay.value = DateTime(
          Get.find<DiligenceManagementTeacherController>()
              .selectedDate
              .value
              .year,
          Get.find<DiligenceManagementTeacherController>()
              .selectedDate
              .value
              .month,
          Get.find<DiligenceManagementTeacherController>()
              .selectedDate
              .value
              .day);
    }
    if(Get.isRegistered<ManagerLeaveApplicationTeacherController>()){
      controllerDateStart.value.text = Get.find<ManagerLeaveApplicationTeacherController>().controllerDateStart.value.text;
      controllerDateEnd.value.text = Get.find<ManagerLeaveApplicationTeacherController>().controllerDateEnd.value.text;
      studentId.value = Get.find<ManagerLeaveApplicationTeacherController>().studentId.value;
      typeTimeLeaveApplication.value = Get.find<ManagerLeaveApplicationTeacherController>().typeTimeLeaveApplication.value;
    }else{
      studentId.value = "";
      typeTimeLeaveApplication.value = "Thời gian tạo";
      controllerDateStart.value.text =   outputDateFormat.format(DateTime(findFirstDateOfTheWeek(selectedDay.value).year, findFirstDateOfTheWeek(selectedDay.value).month,findFirstDateOfTheWeek(selectedDay.value).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(findLastDateOfTheWeek(selectedDay.value).year, findLastDateOfTheWeek(selectedDay.value).month,findLastDateOfTheWeek(selectedDay.value).day));
    }

    getListLeavingApplicationCancelTeacher();
    super.onInit();
  }

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }

  getListLeavingApplicationCancelTeacher() async{
    itemsLeavingApplications.clear();
    var fromDate = DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2),),00,00,00).millisecondsSinceEpoch;
    var toDate = DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2)),23,59,59).millisecondsSinceEpoch;
    var classId =Get.find<TeacherHomeController>().currentClass.value.classId;
    _diligenceRepo.getListLeavingApplicationTeacher(classId,fromDate,toDate,"CANCEL",getTypeTimeLeaveApplication(typeTimeLeaveApplication.value),studentId.value).then((value) {
      if (value.state == Status.SUCCESS) {
        itemsLeavingApplications.value = value.object!;
      }
    });
    itemsLeavingApplications.refresh();
  }
  getTypeTimeLeaveApplication(type){
    switch(type){
      case "Thời gian tạo":
        return "CREATEAT";
      case "Thời gian nghỉ":
        return "TIMELEAVE";
    }
  }


  getImageStatus(status){
    switch(status){
      case "PENDING":
        return "";
      case "APPROVE":
        return "assets/images/image_approved.png";
      case "REFUSE":
        return "assets/images/image_rejected.png";
    }
  }


  getColorTextStatus(status){
    switch(status){
      case "PENDING":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "APPROVE":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "REFUSE":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "CANCEL":
        return const Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getColorBackgroundStatus(status){
    switch(status){
      case "PENDING":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "APPROVE":
        return const Color.fromRGBO(192, 242, 220, 1);
      case "REFUSE":
        return const Color.fromRGBO(252, 211, 215, 1);
      case "CANCEL":
        return const Color.fromRGBO(252, 211, 215, 1);
    }
  }


  getStatus(status){
    switch(status){
      case "CANCEL":
        return "Đã hủy";
      case "PENDING":
        return "Chờ duyệt";
      case "REFUSE":
        return "Từ chối";
      case "APPROVE":
        return "Đã duyệt";
    }
  }
  goToDetailCancelLeaveApplication(index){
    Get.toNamed(Routes.detailCancelLeaveApplicationTeacher,arguments: itemsLeavingApplications[index].id);
  }

}