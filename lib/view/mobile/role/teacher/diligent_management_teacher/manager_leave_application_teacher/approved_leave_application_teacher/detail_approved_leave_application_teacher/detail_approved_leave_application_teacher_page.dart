import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/loading_custom.dart';
import 'package:slova_lms/view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/cancel_leave_application_teacher/cancel_leave_application_teacher_controller.dart';
import '../../../attendance_teacher/attendance_teacher_controller.dart';
import '../../../diligent_management_teacher_controller.dart';
import '../../../list_leave_application_teacher/list_leave_application_teacher_controller.dart';
import '../../manager_leave_application_teacher_controller.dart';
import '../../pending_teacher/pending_teacher_controller.dart';
import '../approved_teacher_leave_application_controller.dart';
import 'detail_approved_leave_application_teacher_controller.dart';

class DetailApprovedTeacherPage
    extends GetWidget<DetailApprovedTeacherController> {
  @override
  final controller = Get.put(DetailApprovedTeacherController());

  DetailApprovedTeacherPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ScreenUtilInit(
      useInheritedMediaQuery: true,
      builder: (context, child) {
      return Obx(() => WillPopScope(
          child: controller.isReady.value
              ?SafeArea(
              child: Scaffold(
                resizeToAvoidBottomInset: true,
                appBar: AppBar(
                  backgroundColor: ColorUtils.PRIMARY_COLOR,
                  elevation: 0,
                  actions: [
                    InkWell(
                      onTap: () {
                        comeToHome();
                        Get.delete<DiligenceManagementTeacherController>(force: true);
                        Get.delete<ManagerLeaveApplicationTeacherController>(force: true);
                        Get.delete<PendingTeacherController>(force: true);
                        Get.delete<CancelLeaveApplicationTeacherController>(force: true);
                        Get.delete<ApprovedLeaveApplicationTeacherController>(force: true);
                        Get.delete<ListLeaveApplicationTeacherController>(force: true);
                        Get.delete<AttendanceTeacherController>(force: true);
                      },
                      child: const Icon(
                        Icons.home,
                        color: Colors.white,
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(right: 16.w))
                  ],
                  title: Text(
                    'Chi Tiết Đơn Nghỉ Phép',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.sp,
                        fontFamily: 'static/Inter-Medium.ttf'),
                  ),
                ),
                body: SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.only(left: 8.w, right: 8.w, top: 8.h),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Chi Tiết Đơn",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 12.sp,
                              color: const Color.fromRGBO(133, 133, 133, 1)),
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        controller.detailNotifyLeavingApplication.value.html != null
                            ? Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(6)),
                          margin: EdgeInsets.symmetric(
                              horizontal: 8.w, vertical: 16.h),
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.w, vertical: 16.h),
                          child: Html(

                              data: controller.detailNotifyLeavingApplication
                                  .value.html ??
                                  ""),
                        )
                            : Container(),
                        Container(
                          decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(6)),
                          margin: EdgeInsets.symmetric(horizontal: 8.w,vertical: 8.h),
                          padding: EdgeInsets.all(16.h),
                          child:  Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: 80.w,
                                      child: Text(
                                        "Trạng thái:",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            color: const Color.fromRGBO(
                                                133, 133, 133, 1),
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                    Container(
                                      padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                                      decoration: BoxDecoration(
                                          color: controller.getColorBackgroundStatus(controller.detailNotifyLeavingApplication.value.status),
                                          borderRadius: BorderRadius.circular(6)
                                      ),
                                      child: Text(
                                        "${controller.getStatus(controller.detailNotifyLeavingApplication.value.status)??""}",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            color: controller.getColorTextStatus(controller.detailNotifyLeavingApplication.value.status),
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                    Expanded(child: Container()),
                                  ]),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      width: controller.setWidth(controller.detailNotifyLeavingApplication.value.status)??80.0,
                                      child: Text(
                                        controller.setTextFeedback(controller.detailNotifyLeavingApplication.value.status)??"",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            color: const Color.fromRGBO(
                                                133, 133, 133, 1),
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                    Visibility(visible: controller.detailNotifyLeavingApplication.value.feedback != ""||controller.detailNotifyLeavingApplication.value.feedback != null,
                                      child: Text(
                                        controller.detailNotifyLeavingApplication.value.feedback??"",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            color: const Color.fromRGBO(
                                                133, 133, 133, 1),
                                            fontWeight: FontWeight.w400),
                                      ),)
                                  ]),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ))
              :const LoadingCustom(),
          onWillPop: () async{
            Get.back();
            if(Get.isRegistered<ManagerLeaveApplicationTeacherController>()){
              Get.find<ManagerLeaveApplicationTeacherController>().onRefresh();
              Get.find<ManagerLeaveApplicationTeacherController>().update();
            }
            return true;
          }));
    },);
  }
}
