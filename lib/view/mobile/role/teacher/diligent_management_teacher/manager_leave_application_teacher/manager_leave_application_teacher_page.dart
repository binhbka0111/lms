import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/pending_teacher/pending_teacher_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/pending_teacher/pending_teacher_page.dart';
import '../../../../../../commom/constants/date_format.dart';
import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../commom/utils/date_time_picker.dart';
import '../../../../../../commom/utils/time_utils.dart';
import '../../../../../../data/model/common/contacts.dart';
import '../attendance_teacher/attendance_teacher_controller.dart';
import '../diligent_management_teacher_controller.dart';
import '../list_leave_application_teacher/list_leave_application_teacher_controller.dart';
import 'approved_leave_application_teacher/approved_teacher_leave_application_controller.dart';
import 'approved_leave_application_teacher/approved_teacher_leave_application_page.dart';
import 'cancel_leave_application_teacher/cancel_leave_application_teacher_controller.dart';
import 'cancel_leave_application_teacher/cancel_leave_application_teacher_page.dart';
import 'manager_leave_application_teacher_controller.dart';

class ManagerLeaveApplicationTeacherPage
    extends GetView<ManagerLeaveApplicationTeacherController> {
  @override
  final controller =
      Get.put(ManagerLeaveApplicationTeacherController(), permanent: true);

  ManagerLeaveApplicationTeacherPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        elevation: 0,
        actions: [
          InkWell(
            onTap: () {
              comeToHome();
              Get.delete<DiligenceManagementTeacherController>(force: true);
              Get.delete<ManagerLeaveApplicationTeacherController>(force: true);
              Get.delete<PendingTeacherController>(force: true);
              Get.delete<CancelLeaveApplicationTeacherController>(force: true);
              Get.delete<ApprovedLeaveApplicationTeacherController>(force: true);
              Get.delete<ListLeaveApplicationTeacherController>(force: true);
              Get.delete<AttendanceTeacherController>(force: true);
            },
            child: Icon(
              Icons.home,
              color: Colors.white,
              size: 20.sp,
            ),
          ),
          Padding(padding: EdgeInsets.only(right: 16.w))
        ],
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        title: Text(
          'Quản Lý đơn xin nghỉ',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() => RefreshIndicator(
        color: ColorUtils.PRIMARY_COLOR,
          child: Column(
            children: [
              SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child:  Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                        color: Colors.white,
                      ),
                      padding: const EdgeInsets.all(8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 16.w),
                            height: 40,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                    color: const Color.fromRGBO(192, 192, 192, 1)),
                                borderRadius: BorderRadius.circular(8)),
                            child: IntrinsicHeight(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  IntrinsicWidth(
                                      child: Theme(
                                        data: Theme.of(context).copyWith(
                                          canvasColor: Colors.white,
                                        ),
                                        child: DropdownButtonHideUnderline(
                                            child: DropdownButton(
                                              isExpanded: true,
                                              iconSize: 0,
                                              icon: const Visibility(
                                                  visible: false,
                                                  child: Icon(Icons.arrow_downward)),
                                              elevation: 0,
                                              borderRadius: BorderRadius.circular(10.r),
                                              hint:
                                              controller.typeTimeLeaveApplication.value ==
                                                  ""
                                                  ? Row(
                                                children: [
                                                  Text(
                                                    'chọn kiểu thời gian',
                                                    style: TextStyle(
                                                        fontSize: 14.sp,
                                                        fontWeight: FontWeight.w400,
                                                        color: ColorUtils.PRIMARY_COLOR),
                                                  ),
                                                  const Icon(
                                                    Icons.keyboard_arrow_down,
                                                    color: Colors.black,
                                                    size: 18,
                                                  )
                                                ],
                                              )
                                                  : Row(
                                                children: [
                                                  Text(
                                                    '${controller.typeTimeLeaveApplication.value} ',
                                                    style: TextStyle(
                                                        fontSize: 14.sp,
                                                        fontWeight: FontWeight.w400,
                                                        color: Colors.black),
                                                  ),
                                                  const Icon(
                                                    Icons.keyboard_arrow_down,
                                                    color: Colors.black,
                                                    size: 18,
                                                  )
                                                ],
                                              ),
                                              items: ['Thời gian tạo', 'Thời gian nghỉ'].map(
                                                    (value) {
                                                  return DropdownMenuItem<String>(
                                                    value: value,
                                                    child: Text(
                                                      value,
                                                      style: TextStyle(
                                                          fontSize: 14.sp,
                                                          fontWeight: FontWeight.w400,
                                                          color: ColorUtils.PRIMARY_COLOR),
                                                    ),
                                                  );
                                                },
                                              ).toList(),
                                              onChanged: (String? value) {
                                                controller.typeTimeLeaveApplication.value =
                                                value!;
                                                controller.fillLeaveApplication();
                                              },
                                            )),
                                      )),
                                  const SizedBox(
                                      height: 30,
                                      child: VerticalDivider(color: Colors.black)),
                                  InkWell(
                                    onTap: () {
                                      Get.dialog(Dialog(
                                        child: IntrinsicHeight(
                                            child: Container(
                                              margin: const EdgeInsets.symmetric(
                                                  horizontal: 16, vertical: 16),
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(6)),
                                              child: Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(bottom: 8.h),
                                                    child: Text(
                                                      "Vui lòng chọn ngày",
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight: FontWeight.w500,
                                                          fontSize: 12.sp),
                                                    ),
                                                  ),
                                                  Row(
                                                    children: [
                                                      Expanded(
                                                        child: Container(
                                                          alignment: Alignment.centerLeft,
                                                          padding:
                                                          EdgeInsets.only(left: 8.w),
                                                          decoration: BoxDecoration(
                                                              color: Colors.white,
                                                              borderRadius:
                                                              const BorderRadius.all(
                                                                  Radius.circular(
                                                                      6.0)),
                                                              border: Border.all(
                                                                width: 1,
                                                                style: BorderStyle.solid,
                                                                color:
                                                                const Color.fromRGBO(
                                                                    192, 192, 192, 1),
                                                              )),
                                                          child: TextFormField(
                                                            keyboardType:
                                                            TextInputType.multiline,
                                                            maxLines: null,
                                                            style: TextStyle(
                                                              fontSize: 10.0.sp,
                                                              color: const Color.fromRGBO(
                                                                  26, 26, 26, 1),
                                                            ),
                                                            onTap: () {
                                                              selectDateTimeStart(
                                                                  controller
                                                                      .controllerDateStart
                                                                      .value
                                                                      .text,
                                                                  DateTimeFormat
                                                                      .formatDateShort,
                                                                  context);

                                                            },
                                                            cursorColor: ColorUtils.PRIMARY_COLOR,
                                                            controller: controller
                                                                .controllerDateStart
                                                                .value,
                                                            readOnly: true,
                                                            decoration: InputDecoration(
                                                              suffixIcon: SizedBox(
                                                                height: 1,
                                                                width: 1,
                                                                child: SvgPicture.asset(
                                                                  "assets/images/icon_date_picker.svg",
                                                                  fit: BoxFit.scaleDown,
                                                                ),
                                                              ),
                                                              labelText: "Từ ngày",
                                                              border: InputBorder.none,
                                                              labelStyle: TextStyle(
                                                                  color: const Color
                                                                      .fromRGBO(
                                                                      177, 177, 177, 1),
                                                                  fontSize: 12.sp,
                                                                  fontWeight:
                                                                  FontWeight.w500,
                                                                  fontFamily:
                                                                  'assets/font/static/Inter-Medium.ttf'),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Padding(
                                                          padding: EdgeInsets.only(
                                                              right: 16.w)),
                                                      Expanded(
                                                        child: Container(
                                                          alignment: Alignment.centerLeft,
                                                          padding:
                                                          EdgeInsets.only(left: 8.w),
                                                          decoration: BoxDecoration(
                                                              color: Colors.white,
                                                              borderRadius:
                                                              const BorderRadius.all(
                                                                  Radius.circular(
                                                                      6.0)),
                                                              border: Border.all(
                                                                width: 1,
                                                                style: BorderStyle.solid,
                                                                color:
                                                                const Color.fromRGBO(
                                                                    192, 192, 192, 1),
                                                              )),
                                                          child: TextFormField(
                                                            keyboardType:
                                                            TextInputType.multiline,
                                                            maxLines: null,
                                                            style: TextStyle(
                                                              fontSize: 10.0.sp,
                                                              color: const Color.fromRGBO(
                                                                  26, 26, 26, 1),
                                                            ),
                                                            onTap: () {
                                                              selectDateTimeEnd(
                                                                  controller
                                                                      .controllerDateEnd
                                                                      .value
                                                                      .text,
                                                                  DateTimeFormat
                                                                      .formatDateShort,
                                                                  context);
                                                            },
                                                            readOnly: true,
                                                            cursorColor: ColorUtils.PRIMARY_COLOR,
                                                            controller: controller
                                                                .controllerDateEnd.value,
                                                            decoration: InputDecoration(
                                                              suffixIcon: SizedBox(
                                                                height: 1,
                                                                width: 1,
                                                                child: SvgPicture.asset(
                                                                  "assets/images/icon_date_picker.svg",
                                                                  fit: BoxFit.scaleDown,
                                                                ),
                                                              ),
                                                              label:
                                                              const Text("Đến ngày"),
                                                              border: InputBorder.none,
                                                              labelStyle: TextStyle(
                                                                  color: const Color
                                                                      .fromRGBO(
                                                                      177, 177, 177, 1),
                                                                  fontSize: 12.sp,
                                                                  fontWeight:
                                                                  FontWeight.w500,
                                                                  fontFamily:
                                                                  'assets/font/static/Inter-Medium.ttf'),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const Padding(
                                                      padding: EdgeInsets.only(top: 16)),
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Expanded(
                                                          child: SizedBox(
                                                            height: 24.h,
                                                            child: ElevatedButton(
                                                                style:
                                                                ElevatedButton.styleFrom(
                                                                    backgroundColor:
                                                                    const Color.fromRGBO(
                                                                        255,
                                                                        255,
                                                                        255,
                                                                        1),
                                                                    side:
                                                                    const BorderSide(
                                                                        color: ColorUtils.PRIMARY_COLOR,
                                                                        width: 1),
                                                                    shape:
                                                                    RoundedRectangleBorder(
                                                                      borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                          6),
                                                                    )),
                                                                onPressed: () {
                                                                  Get.back();
                                                                  controller.getDay();
                                                                },
                                                                child: Text(
                                                                  'Hủy',
                                                                  style: TextStyle(
                                                                      color: ColorUtils.PRIMARY_COLOR,
                                                                      fontSize: 12.sp,
                                                                      fontWeight:
                                                                      FontWeight.w400,
                                                                      fontFamily:
                                                                      'assets/font/static/Inter-Regular.ttf'),
                                                                )),
                                                          )),
                                                      Padding(
                                                          padding: EdgeInsets.only(
                                                              right: 8.w)),
                                                      Expanded(
                                                          child: SizedBox(
                                                            height: 24.h,
                                                            child: ElevatedButton(
                                                                style:
                                                                ElevatedButton.styleFrom(
                                                                    backgroundColor: ColorUtils.PRIMARY_COLOR,
                                                                    side:
                                                                    const BorderSide(
                                                                        color: ColorUtils.PRIMARY_COLOR,
                                                                        width: 1),
                                                                    shape:
                                                                    RoundedRectangleBorder(
                                                                      borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                          6),
                                                                    )),
                                                                onPressed: () {
                                                                  Get.back();
                                                                  controller.fillLeaveApplication();
                                                                },
                                                                child: Text(
                                                                  'Xác Nhận',
                                                                  style: TextStyle(
                                                                      color: const Color
                                                                          .fromRGBO(
                                                                          255, 255, 255, 1),
                                                                      fontSize: 12.sp,
                                                                      fontWeight:
                                                                      FontWeight.w400,
                                                                      fontFamily:
                                                                      'assets/font/static/Inter-Regular.ttf'),
                                                                )),
                                                          ))
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            )),
                                      ));
                                    },
                                    child: Row(
                                      children: [
                                        Text(
                                            "${controller.controllerDateStart.value.text.substring(0,10)} - ${controller.controllerDateEnd.value.text.substring(0,10)}",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    26, 26, 26, 1),
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w500,
                                                fontFamily:
                                                'assets/font/static/Inter-Medium.ttf')),
                                        Padding(padding: EdgeInsets.only(right: 4.w)),
                                        SizedBox(
                                          child: Image.asset(
                                            "assets/images/icon_calenda.png",
                                            height: 16,
                                            width: 16,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 16.h)),
                         Obx(() =>  Container(
                           margin: EdgeInsets.symmetric(horizontal: 16.w),
                           padding: EdgeInsets.symmetric(horizontal: 16.w),
                           height: 40,
                           decoration: BoxDecoration(
                               color: Colors.white,
                               border: Border.all(
                                   color: const Color.fromRGBO(192, 192, 192, 1)),
                               borderRadius: BorderRadius.circular(8)),
                           child: Theme(
                             data: Theme.of(context).copyWith(
                               canvasColor: Colors.white,
                             ),
                             child: DropdownButtonHideUnderline(
                                 child: DropdownButton(
                                   isExpanded: true,
                                   iconSize: 0,
                                   icon: const Visibility(
                                       visible: false,
                                       child: Icon(Icons.arrow_downward)),
                                   elevation: 16,
                                   hint: controller.selectStudent.value != ""
                                       ? Row(
                                     children: [
                                       Text(
                                         controller.selectStudent.value,
                                         style: TextStyle(
                                             fontSize: 14.sp,
                                             fontWeight: FontWeight.w400,
                                             color: ColorUtils.PRIMARY_COLOR),
                                       ),
                                       Expanded(child: Container()),
                                       const Icon(
                                         Icons.keyboard_arrow_down,
                                         color: Colors.black,
                                         size: 18,
                                       )
                                     ],
                                   )
                                       : Row(
                                     children: [
                                       Text(
                                         'Chọn học sinh',
                                         style: TextStyle(
                                             fontSize: 14.sp,
                                             fontWeight: FontWeight.w400,
                                             color: Colors.black),
                                       ),
                                       Expanded(child: Container()),
                                       const Icon(
                                         Icons.keyboard_arrow_down,
                                         color: Colors.black,
                                         size: 18,
                                       )
                                     ],
                                   ),
                                   items: controller.listStudentByClass.map(
                                         (value) {
                                       return DropdownMenuItem<Student>(
                                         value: value,
                                         child: Text(
                                           value.fullName!,
                                           style: TextStyle(
                                               fontSize: 14.sp,
                                               fontWeight: FontWeight.w400,
                                               color: ColorUtils.PRIMARY_COLOR),
                                         ),
                                       );
                                     },
                                   ).toList(),
                                   onChanged: (Student? value) {
                                     controller.selectStudent.value = value!.fullName!;
                                     controller.studentId.value = value.id!;
                                     controller.fillLeaveApplication();
                                   },
                                 )),
                           ),
                         ))
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16.w),
                      height: 40,
                      child: ListView.builder(
                          itemCount: controller.listStatus.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return GetBuilder<ManagerLeaveApplicationTeacherController>(

                                builder: (controller) {
                                  return TextButton(
                                      onPressed: () {
                                        controller.setUpdate(index);
                                        controller.showColor(index);
                                        controller.pageController.animateToPage(index,
                                            duration:
                                            const Duration(milliseconds: 500),
                                            curve: Curves.linear);
                                      },
                                      child: Obx(() => Text(
                                        "${controller.listStatus[index]} (${controller.getCountLeaveApplication(index)})",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12.sp,
                                            color: controller.indexClick == index
                                                ? ColorUtils.PRIMARY_COLOR
                                                : const Color.fromRGBO(
                                                177, 177, 177, 1)),
                                      )));
                                });
                          }),
                    ),

                  ],
                ),
              ),
              Expanded(
                  child:  PageView(
                    physics: const NeverScrollableScrollPhysics(),
                    onPageChanged: (value) {
                      controller.onPageViewChange(value);
                    },
                    controller: controller.pageController,
                    children: [
                      PendingTeacherPage(),
                      ApprovedLeaveApplicationTeacherPage(),
                      CancelLeaveApplicationTeacherPage()
                    ],
                  )),
            ],
          ), onRefresh: () async{
          controller.onRefresh();
      })),
    );
  }

  selectDateTimeStart(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
            initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy')
                .parse(controller.controllerDateEnd.value.text)
                .millisecondsSinceEpoch) {
          controller.controllerDateStart.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian Từ ngày nhỏ hơn thời gian Đến ngày");
        }
      } else {
        controller.controllerDateStart.value.text = date;
      }

    }
  }

  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
            initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy')
            .parse(controller.controllerDateStart.value.text)
            .millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch) {
          controller.controllerDateEnd.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian Đến ngày lớn hơn thời gian Từ ngày");
        }
      } else {
        controller.controllerDateEnd.value.text = date;
      }

    }
  }
}
