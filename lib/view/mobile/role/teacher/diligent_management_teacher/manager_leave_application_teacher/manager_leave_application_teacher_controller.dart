import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/diligence_parent/approval_parent/cancel_leaving_application/cancel_leave_application_page.dart';
import 'package:slova_lms/view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/pending_teacher/pending_teacher_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/diligent_management_teacher/manager_leave_application_teacher/pending_teacher/pending_teacher_page.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/diligence.dart';
import '../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../../../commom/constants/string_constant.dart';
import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../commom/utils/check_user_group_permission.dart';
import '../../../../../../data/model/common/contacts.dart';
import '../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../data/model/common/list_student.dart';
import '../../../../../../data/repository/list_student/list_student_repo.dart';
import '../../teacher_home_controller.dart';
import '../diligent_management_teacher_controller.dart';
import 'approved_leave_application_teacher/approved_teacher_leave_application_controller.dart';
import 'approved_leave_application_teacher/approved_teacher_leave_application_page.dart';
import 'cancel_leave_application_teacher/cancel_leave_application_teacher_controller.dart';

class ManagerLeaveApplicationTeacherController extends GetxController{
  DateTime now = DateTime.now();
  var indexClick = 0;
  RxList<bool> click = <bool>[].obs;
  var countOnTime = 0.obs;

  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudent = <Diligents>[].obs;
  var student = StudentDiligent().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');

  var itemsLeavingApplications = <ItemsLeavingApplication>[].obs;
  var listPending = <ItemsLeavingApplication>[].obs;
  var listApprove = <ItemsLeavingApplication>[].obs;
  var listRefuse = <ItemsLeavingApplication>[].obs;
  var listCancel = <ItemsLeavingApplication>[].obs;

  var selectedDay = DateTime.now().obs;
  var controllerDateStart = TextEditingController().obs;
  var controllerDateEnd = TextEditingController().obs;
  var typeTimeLeaveApplication = "".obs;
  final ListStudentRepo _listStudent = ListStudentRepo();
  RxList<DetailItem> detailItem = <DetailItem>[].obs;
  var listStudentByClass = <Student>[].obs;
  var detailStudent = Student().obs;
  var selectStudent = "".obs;
  var studentId = "".obs;

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: false,
  );
  var listStatus = <String>[].obs;
  var pending = PendingTeacherPage();
  var cancel = CancelLeaveApplicationPage();
  var approved = ApprovedLeaveApplicationTeacherPage();
  var listPage = <Widget>[];
  void setUpdate(int index){
    indexClick= index;
    update();
  }


  @override
  void onInit() {
    super.onInit();
    selectStudent.value = "Tất cả";
    studentId.value = "";
    typeTimeLeaveApplication.value = "Thời gian tạo";
    if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_PENDING_LIST)){
      listStatus.add("Chờ duyệt");
      listPage.add(pending);
    }
    if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_APPROVED_LIST)){
      listStatus.add("Đã Duyệt");
      listPage.add(approved);
    }
    if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_CANCEL_LIST)){
      listStatus.add("Đã Hủy");
      listPage.add(cancel);
    }



    selectedDay.value = DateTime(
        Get.find<DiligenceManagementTeacherController>().selectedDate.value.year,
        Get.find<DiligenceManagementTeacherController>().selectedDate.value.month,
        Get.find<DiligenceManagementTeacherController>().selectedDate.value.day);
    getDay();
    getListLeavingApplicationTeacher();
    getDetailListStudent();
  }

  @override
  dispose() {
    pageController.dispose();
    super.dispose();
  }

  getDay(){
    controllerDateStart.value.text =   outputDateFormat.format(DateTime(findFirstDateOfTheWeek(selectedDay.value).year, findFirstDateOfTheWeek(selectedDay.value).month,findFirstDateOfTheWeek(selectedDay.value).day));
    controllerDateEnd.value.text = outputDateFormat.format(DateTime(findLastDateOfTheWeek(selectedDay.value).year, findLastDateOfTheWeek(selectedDay.value).month,findLastDateOfTheWeek(selectedDay.value).day));
  }


  getTypeTimeLeaveApplication(type){
    switch(type){
      case "Thời gian tạo":
        return "CREATEAT";
      case "Thời gian nghỉ":
        return "TIMELEAVE";
    }
  }

   getDetailListStudent() {
    listStudentByClass.value = [];
    listStudentByClass.add(Student(id: "",fullName: "Tất cả"));
    var classId = Get.find<TeacherHomeController>().currentClass.value.classId;
    _listStudent.listStudentByClass(classId).then((value) {
      if (value.state == Status.SUCCESS) {
        detailItem.value = value.object!;
        for(int i =0;i< detailItem.length ;i++){
          listStudentByClass.add(Student(id: "",fullName: ""));
          listStudentByClass[i+1].fullName = detailItem[i].fullName!;
          listStudentByClass[i+1].id = detailItem[i].id!;
        }
      }
    });
    listStudentByClass.refresh();
  }


  fillLeaveApplication(){
    if(controllerDateStart.value.text == ""){
      AppUtils().showToast("Vui lòng chọn ngày bắt đầu");
    }else{
      if(controllerDateEnd.value.text == ""){
        AppUtils().showToast("Vui lòng chọn ngày kết thúc");
      }else{
        if(typeTimeLeaveApplication.value == ""){
          AppUtils().showToast("Vui lòng chọn kiểu thời gian");
        }else{
          if(selectStudent.value == ""){
            AppUtils().showToast("Vui lòng chọn học sinh");
          }else{
            getListLeavingApplicationTeacher();
            Get.find<PendingTeacherController>().onInit();
            Get.find<CancelLeaveApplicationTeacherController>().onInit();
            Get.find<ApprovedLeaveApplicationTeacherController>().onInit();
          }
        }
      }
    }

  }


  getListLeavingApplicationTeacher() async{
    itemsLeavingApplications.clear();
    listPending.clear();
    listApprove.clear();
    listRefuse.clear();
    listCancel.clear();
    listApprove.clear();
    var classId =Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromDate = DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2),),00,00,00).millisecondsSinceEpoch;
    var toDate = DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2)),23,59,59).millisecondsSinceEpoch;
    _diligenceRepo.getListLeavingApplicationTeacher(classId,fromDate,toDate,"",getTypeTimeLeaveApplication(typeTimeLeaveApplication.value),studentId.value).then((value) {
      if (value.state == Status.SUCCESS) {
        itemsLeavingApplications.value = value.object!;
        listPending.value = itemsLeavingApplications.where((element) => element.status == "PENDING").toList();
        listApprove.value = itemsLeavingApplications.where((element) => element.status == "APPROVE").toList();
        listCancel.value = itemsLeavingApplications.where((element) => element.status == "CANCEL").toList();
        listRefuse.value = itemsLeavingApplications.where((element) => element.status == "REFUSE").toList();
        listApprove.addAll(listRefuse);
      }
    });
    itemsLeavingApplications.refresh();
    listPending.refresh();
    listApprove.refresh();
    listCancel.refresh();
    listRefuse.refresh();
    listApprove.refresh();
    update();
  }


  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }

  getCountLeaveApplication(index){
    switch(index){
      case 0:
        return listPending.length;
      case 1:
        return listApprove.length;
      case 2:
        return listCancel.length;

    }
  }


  comePending() {
    pageController.animateTo(0, duration: const Duration(seconds: 1), curve: Curves.bounceIn);
  }


  comeApproved() {
    pageController.animateTo(1, duration: const Duration(seconds: 1), curve: Curves.bounceIn);
  }

  comeCancel() {
    pageController.animateTo(2, duration: const Duration(seconds: 1), curve: Curves.bounceIn);
  }

  onRefresh(){
    selectStudent.value = "Tất cả";
    studentId.value = "";
    controllerDateStart.value.text =   outputDateFormat.format(DateTime(findFirstDateOfTheWeek(selectedDay.value).year, findFirstDateOfTheWeek(selectedDay.value).month,findFirstDateOfTheWeek(selectedDay.value).day,00,00));
    controllerDateEnd.value.text = outputDateFormat.format(DateTime(findLastDateOfTheWeek(selectedDay.value).year, findLastDateOfTheWeek(selectedDay.value).month,findLastDateOfTheWeek(selectedDay.value).day,23,59));
    getListLeavingApplicationTeacher();
    getDetailListStudent();
    Get.find<PendingTeacherController>().onInit();
    Get.find<CancelLeaveApplicationTeacherController>().onInit();
    Get.find<ApprovedLeaveApplicationTeacherController>().onInit();
  }






  onPageViewChange(int page) {
    indexClick = page;
    if(page != 0) {
      indexClick-1;
    } else {
      indexClick = 0;
    }

  }


  showColor(index) {
    click.value = [];
    for (int i = 0; i < 3; i++) {
      click.add(false);
    }
    click[index] = true;
  }
}



