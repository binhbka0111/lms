import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import '../../../../../commom/constants/string_constant.dart';
import '../../../../../commom/utils/app_utils.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/common/diligence.dart';
import '../../../../../data/model/common/leaving_application.dart';
import '../../../../../data/repository/diligence/diligence_repo.dart';
import 'list_leave_application_teacher/list_leave_application_teacher_page.dart';
import '../teacher_home_controller.dart';
import 'attendance_teacher/attendance_teacher_controller.dart';
import 'list_leave_application_teacher/list_leave_application_teacher_controller.dart';
import 'manager_leave_application_teacher/approved_leave_application_teacher/approved_teacher_leave_application_controller.dart';
import 'manager_leave_application_teacher/cancel_leave_application_teacher/cancel_leave_application_teacher_controller.dart';
import 'manager_leave_application_teacher/manager_leave_application_teacher_controller.dart';
import 'manager_leave_application_teacher/pending_teacher/pending_teacher_controller.dart';
import 'attendance_teacher/attendance_teacher_page.dart';

class DiligenceManagementTeacherController extends GetxController {
  DateTime now = DateTime.now();
  RxInt indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  var colorText = const Color.fromRGBO(177, 177, 177, 1).obs;
  var color = const Color.fromRGBO(246, 246, 246, 1).obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var selectedDate = DateTime.now().obs;
  var cupertinoDatePicker = DateTime.now().obs;
  var offset = 0.0.obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  var itemsLeavingApplications = <ItemsLeavingApplication>[].obs;
  var diligenceClass = DiligenceClass().obs;
  var listStudentDiligence = <Diligents>[].obs;
  var listStatus = <String>[].obs;
  var isShowPopupMenu = false.obs;
  var listItemPopupMenu = <PopupMenuItem>[];
  var listPageView = <Widget>[].obs;

  var heightManagerLeaveApplication= 30.0;
  var heightStatisticalDiligence = 30.0;


  var showManagerLeaveApplication = true;
  var showStatisticalDiligence = false;


  var indexManagerLeaveApplication = 0;
  var indexStatisticalDiligence = 0;
  var attendance = AttendanceTeacherPage();
  var listLeaveApplicationTeacherPage = ListLeaveApplicationTeacherPage();


  @override
  void onInit() {

    if(DateTime.now().month<=12&&DateTime.now().month>9){
     selectedDate.value = DateTime(Get.find<TeacherHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
     cupertinoDatePicker.value = DateTime(Get.find<TeacherHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
    }else{
     selectedDate.value = DateTime(Get.find<TeacherHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
     cupertinoDatePicker.value = DateTime(Get.find<TeacherHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
    }
    pageController.addListener(() {
      if(indexClick.value == 0){
        Get.put(AttendanceTeacherController());
      }
    });
    getListStudentDiligence();
    getListLeavingApplicationTeacher();
    checkHomeRoomTeacher();
    indexClick.value = 0;
    if(checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_ATTENDANCE)){
      listStatus.add("Điểm danh");
      listPageView.add(attendance);
    }
    if(checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_LIST_LEAVE_APPLICATION)){
      listStatus.add("Danh sách đơn xin nghỉ");
      listPageView.add(listLeaveApplicationTeacherPage);
    }
    super.onInit();
  }

  @override
  dispose() {
    pageController.dispose();
    super.dispose();
  }

  getListItemPopupMenu(){
    listItemPopupMenu = [];
    var managerLeaveApplication =   PopupMenuItem<int>(
      value: 0,
      padding: EdgeInsets.zero,
      height: heightManagerLeaveApplication,
      child: showManagerLeaveApplication
          ?Container(
        padding: EdgeInsets.symmetric(vertical: 4.h),
        decoration:  const BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.black26))),
        child: const Center(
          child: Text("Quản lý đơn xin phép"),
        ),
      )
          :const Center(
        child: Text("Quản lý đơn xin phép"),
      ),
    );
    var statisticalDiligence =   PopupMenuItem<int>(
      value: 1,
      padding: EdgeInsets.zero,
      height: heightStatisticalDiligence,
      child: showStatisticalDiligence
          ?Container(
        padding: EdgeInsets.symmetric(vertical: 4.h),
        decoration:  const BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.black26))),
        child:const Center(
          child: Text("Thống kê chuyên cần"),
        ) ,
      )
          : const Center(
        child: Text("Thống kê chuyên cần"),
      ),
    );
    var diaryDiligence =   const PopupMenuItem<int>(
      value: 2,
      padding: EdgeInsets.zero,
      height: 30,
      child: Center(

        child: Text("Nhật ký chuyên cần"),
      ),
    );


      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_MANAGE_LEAVE_APPLICATION)){
        listItemPopupMenu.add(managerLeaveApplication);
      }
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_STATISTICAL_DILIGENCE)){
        listItemPopupMenu.add(statisticalDiligence);
      }
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_DIARY_DILIGENCE)){
        listItemPopupMenu.add(diaryDiligence);
      }

    indexManagerLeaveApplication = listItemPopupMenu.indexOf(managerLeaveApplication);
    indexStatisticalDiligence = listItemPopupMenu.indexOf(statisticalDiligence);

    showManagerLeaveApplication = setVisiblePopupMenuItem(indexManagerLeaveApplication,listItemPopupMenu);
    showStatisticalDiligence =setVisiblePopupMenuItem(indexStatisticalDiligence,listItemPopupMenu);

    heightManagerLeaveApplication = setHeightPopupMenuItem(indexManagerLeaveApplication,listItemPopupMenu);
    heightStatisticalDiligence = setHeightPopupMenuItem(indexStatisticalDiligence,listItemPopupMenu);


    update();

    return listItemPopupMenu;

  }




  checkHomeRoomTeacher() {
    if (Get.find<TeacherHomeController>()
        .userProfile
        .value
        .classesOfHomeroomTeacher
        ?.contains(
        Get.find<TeacherHomeController>().currentClass.value.classId) ==
        true) {
      isShowPopupMenu.value = true;
    } else {
      isShowPopupMenu.value = false;
    }
  }


  onPageViewChange(int page) {
    indexClick.value = page;
    if (page != 0) {
      indexClick.value - 1;
    } else {
      indexClick.value = 0;
    }
  }

  showColor(index) {
    click.value = [];
    for (int i = 0; i < 2; i++) {
      click.add(false);
    }
    click[index] = true;
  }

  setColorButtonAttendance(RxList<int> groupValue) {
    if(groupValue.isEmpty){
      colorText.value = const Color.fromRGBO(177, 177, 177, 1);
    }else{
      if (groupValue.contains(0) == true) {
        color.value = const Color.fromRGBO(246, 246, 246, 1);
      } else {
        color.value = const Color.fromRGBO(248, 129, 37, 1);
        if (Get.find<AttendanceTeacherController>().status.value == "CONFIRM") {
          color.value = const Color.fromRGBO(246, 246, 246, 1);
        }
      }
    }

  }

  setColorTextButtonAttendance(RxList<int> groupValue) {
    if(groupValue.isEmpty){
      colorText.value = const Color.fromRGBO(177, 177, 177, 1);
    }else{
      if (groupValue.contains(0)) {
        colorText.value = const Color.fromRGBO(177, 177, 177, 1);
      } else {
        colorText.value = const Color.fromRGBO(255, 255, 255, 1);
        if (Get.find<AttendanceTeacherController>().status.value == "CONFIRM") {
          colorText.value = const Color.fromRGBO(177, 177, 177, 1);
        }
      }
    }
  }

  confirmAttendance() {
    var id = Get.find<AttendanceTeacherController>().diligenceClass.value.id;
    if(Get.find<AttendanceTeacherController>().listStudentDiligence.isNotEmpty){
      if (Get.find<AttendanceTeacherController>().groupValue.contains(0)) {

      } else {
        if (Get.find<AttendanceTeacherController>().status.value != "CONFIRM") {
          if(Get.find<AttendanceTeacherController>().diligenceClass.value.isLock == "FALSE"){
            _diligenceRepo.confirmAttendance(id).then((value) {
              if (value.state == Status.SUCCESS) {
                AppUtils.shared.showToast("Điểm danh thành công!");
                Get.find<AttendanceTeacherController>().colorAttendance(false);
                Get.find<AttendanceTeacherController>().isAttendance.value = false;
                Get.find<AttendanceTeacherController>().onInit();
                color.value = const Color.fromRGBO(246, 246, 246, 1);
                colorText.value = const Color.fromRGBO(177, 177, 177, 1);
              } else {
                AppUtils.shared.hideLoading();
                AppUtils.shared
                    .snackbarError("Điểm danh thất bại", value.message ?? "");
              }
            });
          }else{
            if(Get.find<AttendanceTeacherController>().diligenceClass.value.lockedByUser?.id == AppCache().userId){
              _diligenceRepo.confirmAttendance(id).then((value) {
                if (value.state == Status.SUCCESS) {
                  AppUtils.shared.showToast("Điểm danh thành công!");
                  Get.find<AttendanceTeacherController>().colorAttendance(false);
                  Get.find<AttendanceTeacherController>().isAttendance.value = false;
                  Get.find<AttendanceTeacherController>().onInit();
                  color.value = const Color.fromRGBO(246, 246, 246, 1);
                  colorText.value = const Color.fromRGBO(177, 177, 177, 1);
                } else {
                  AppUtils.shared.hideLoading();
                  AppUtils.shared
                      .snackbarError("Điểm danh thất bại", value.message ?? "");
                }
              });
            }else{
              AppUtils.shared.showToast("Lớp học đang được điểm danh bởi ${Get.find<AttendanceTeacherController>().diligenceClass.value.lockedByUser?.fullName}!");
            }
          }
        }
      }
    }
  }

  getListStudentDiligence() async{
    var classId = Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromDate = DateTime(selectedDate.value.year, selectedDate.value.month,
        selectedDate.value.day, 00, 00)
        .millisecondsSinceEpoch;
    var toDate = DateTime(selectedDate.value.year, selectedDate.value.month,
        selectedDate.value.day, 23, 59)
        .millisecondsSinceEpoch;

    await _diligenceRepo
        .getListStudentDiligence(classId, fromDate, toDate, "")
        .then((value) async{
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudentDiligence.value = diligenceClass.value.studentDiligent!;

      }
    });
    await getListItemPopupMenu();
  }
  getListLeavingApplicationTeacher() async{
    itemsLeavingApplications.clear();
    var classId =Get.find<TeacherHomeController>().currentClass.value.classId;
    var fromDate = DateTime(selectedDate.value.year,selectedDate.value.month,selectedDate.value.day,00,00,00).millisecondsSinceEpoch;
    var toDate = DateTime(selectedDate.value.year,selectedDate.value.month,selectedDate.value.day,23,59,59).millisecondsSinceEpoch;
    _diligenceRepo.getListLeavingApplicationTeacher(classId,fromDate,toDate,"","TIMELEAVE","").then((value) {
      if (value.state == Status.SUCCESS) {
        itemsLeavingApplications.value = value.object!;

      }
    });
    itemsLeavingApplications.refresh();
  }



  getListStatus(index){
    switch(index){
      case 0:
        return  Get.find<AttendanceTeacherController>().listStudentDiligence.length;
      case 1:
        return Get.find<ListLeaveApplicationTeacherController>().listApprove.length;
    }
  }


  onSelectedDateDiligence(){
    Get.find<AttendanceTeacherController>().selectedDay.value = selectedDate.value;
    Get.find<ListLeaveApplicationTeacherController>().selectedDay.value = selectedDate.value;
    Get.find<AttendanceTeacherController>().onInit();
    Get.find<ListLeaveApplicationTeacherController>().onInit();
    if(selectedDate.value.year == DateTime.now().year&&selectedDate.value.month == DateTime.now().month&&selectedDate.value.day == DateTime.now().day){
      Get.find<AttendanceTeacherController>().isShowSwitchAttendance.value = true;
    }else{
      Get.find<AttendanceTeacherController>().isShowSwitchAttendance.value = false;
    }
    if(Get.isRegistered<ManagerLeaveApplicationTeacherController>()){
      Get.find<ManagerLeaveApplicationTeacherController>().onInit();
      Get.find<ManagerLeaveApplicationTeacherController>().update();
    }

    if(Get.isRegistered<CancelLeaveApplicationTeacherController>()){
      Get.find<CancelLeaveApplicationTeacherController>().onInit();
    }
    if(Get.isRegistered<PendingTeacherController>()){
      Get.find<PendingTeacherController>().onInit();
    }
    if(Get.isRegistered<ApprovedLeaveApplicationTeacherController>()){
      Get.find<ApprovedLeaveApplicationTeacherController>().onInit();
      Get.find<ApprovedLeaveApplicationTeacherController>().itemsLeavingApplications.refresh();
    }

    if(Get.isRegistered<AttendanceTeacherController>()){
      Get.find<AttendanceTeacherController>().groupValue.clear();
      Get.find<AttendanceTeacherController>().getListStudentDiligence();
      Get.find<AttendanceTeacherController>().groupValue.refresh();
    }

    if(Get.isRegistered<ListLeaveApplicationTeacherController>()){
      Get.find<ListLeaveApplicationTeacherController>().itemsLeavingApplications.clear();
      Get.find<ListLeaveApplicationTeacherController>().getListLeavingApplicationTeacher();
      Get.find<ListLeaveApplicationTeacherController>().itemsLeavingApplications.refresh();
    }


    update();

  }


  comeBackAttendance() {
    //Move tab to first
    pageController.animateTo(0, duration: const Duration(seconds: 1), curve: Curves.bounceIn);
  }

}
