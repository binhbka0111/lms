import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/diligence.dart';
import '../../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../teacher_home_controller.dart';
import '../../diligent_management_teacher_controller.dart';
import '../detail_diligent_management_teacher_controller.dart';


class ExcusedAbsenceController extends GetxController{
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudentExcusedAbsence = <Diligents>[].obs;
  var listStudent = <Diligents>[].obs;
  var student = StudentDiligent().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var selectedDate = DateTime.now().obs;
  @override
  void onInit() {
    getListStudentExcusedAbsence();
    super.onInit();
  }



  getListStudentExcusedAbsence(){
    var classId =Get.find<TeacherHomeController>().currentClass.value.classId;
    if(Get.isRegistered<DetailDiligenceManagementTeacherController>()){
      selectedDate.value = Get.find<DetailDiligenceManagementTeacherController>().selectedDay.value;
    }else{
      selectedDate.value = DateTime(
          Get.find<DiligenceManagementTeacherController>().selectedDate.value.year,
          Get.find<DiligenceManagementTeacherController>().selectedDate.value.month,
          Get.find<DiligenceManagementTeacherController>().selectedDate.value.day);
    }
    var fromDate = DateTime(selectedDate.value.year, selectedDate.value.month, selectedDate.value.day,00,00).millisecondsSinceEpoch;
    var toDate = DateTime(selectedDate.value.year, selectedDate.value.month, selectedDate.value.day,23,59).millisecondsSinceEpoch;

    _diligenceRepo.getListStudentDetailDiligence(classId, fromDate, toDate, "EXCUSED_ABSENCE").then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudent.value = diligenceClass.value.studentDiligent!;
        listStudentExcusedAbsence.value = listStudent.where((element) => element.statusDiligent == "EXCUSED_ABSENCE").toList();
      }
    });
  }
}