import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/subjects_teacher_controller.dart';
import '../../../../../commom/widget/loading_custom.dart';
import '../../../../../routes/app_pages.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';

import '../../../home/home_controller.dart';
class SubjectTeacherPage extends GetView<SubjectTeacherController> {

  const SubjectTeacherPage({super.key});


  @override
  Widget build(BuildContext context) {
    if (Get.find<HomeController>().tabController?.index != Get.find<HomeController>().listTabView.indexOf(Get.find<HomeController>().notify)) {
      Get.put(SubjectTeacherController(),permanent: true);
    }
    // TODO: implement build
    return WillPopScope(child: Scaffold(
        backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
        appBar: AppBar(
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title: Text(
            'Môn Học',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body:
        Obx(() => controller.isReady.value?SingleChildScrollView(child: Container(
          margin: EdgeInsets.only(top: 16.h),
          child: Column(
            children: [
              Visibility(
                  visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_TRANSCRIPT_SYNTHETIC),
                  child: Visibility(
                      visible: Get.find<SubjectTeacherController>().checkHomeRoomTeacher(),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            onTap: () {
                              Get.toNamed(Routes.transcriptsSyntheticTeacherPage);
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.w, vertical: 8.h),
                              decoration: BoxDecoration(
                                  color: ColorUtils.PRIMARY_COLOR,
                                  borderRadius: BorderRadius.circular(6.r)),
                              child: Text(
                                "Bảng điểm tổng hợp",
                                style: TextStyle(
                                    fontSize: 13.sp,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(right: 16.w))
                        ],
                      ))),
              Visibility(
                  visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_LIST_SUBJECT),
                  child: GridView.builder(
                      padding: const EdgeInsets.all(10),
                      gridDelegate:
                      const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        crossAxisSpacing: 5, //khoảng cách theo chiều dọc
                        mainAxisSpacing: 5, //khoảng cách teho chiều ngang
                      ),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true, //chiều cuộn
                      physics: const ClampingScrollPhysics(),
                      itemCount: Get.find<SubjectTeacherController>().subjects.length,
                      itemBuilder: (context, index) {
                        return
                          InkWell(
                            onTap: (){
                              checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> Get.find<SubjectTeacherController>().onClickSubject(index),StringConstant.FEATURE_SUBJECT_DETAIL);
                            },
                            child:  SizedBox(
                              height: 96,
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      height: 50.w,
                                      width: 50.w,
                                      child:
                                      CacheNetWorkCustomBanner(urlImage:   "${Get.find<SubjectTeacherController>().subjects[index].subjectCategory?.image}",),
                                    ),
                                    const Padding(padding: EdgeInsets.only(top: 8)),
                                    Container(
                                      margin: EdgeInsets.symmetric(horizontal: 8.w),
                                      child: Text(
                                        textAlign: TextAlign.center,
                                        "${Get.find<SubjectTeacherController>().subjects[index].subjectCategory?.name}",
                                        style: TextStyle(
                                            color: const Color.fromRGBO(26, 26, 26, 1),
                                            fontSize: 12.sp),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                      }))
            ],
          ),
        ),):const LoadingCustom())
    ), onWillPop: () async{
      Get.back();
      Get.delete<SubjectTeacherController>(force: true);
      return true;
    },);
  }
}
