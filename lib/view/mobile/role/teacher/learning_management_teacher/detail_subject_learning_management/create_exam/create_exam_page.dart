import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/create_exam/question_exam/question_exam_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/create_exam/question_exam/question_exam_page.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/create_exam/upload_file_exam/file_upload_exam_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/create_exam/upload_file_exam/file_upload_exam_page.dart';
import '../../../../../../../commom/app_cache.dart';
import '../../../../../../../commom/constants/date_format.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../../commom/utils/date_time_picker.dart';
import '../../../../../../../commom/utils/time_utils.dart';
import '../../../../../../../commom/widget/text_field_custom.dart';
import '../../../teacher_home_controller.dart';
import '../learning_management_teacher_controller.dart';
import 'assign_link_exam/attach_link_exam_page.dart';
import 'create_exam_controller.dart';


class CreateExamPage extends GetWidget<CreateExamController> {
  @override
  final controller = Get.put(CreateExamController());

  CreateExamPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              comeToHome();
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),

        /// TODO Tạo Bài Tập
        title: Text(
          'Tạo bài kiểm tra',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body:Stack(
        children: [
          NestedScrollView(
            physics: const ClampingScrollPhysics(),
            scrollBehavior: const ScrollBehavior(),
            controller: controller.scrollController,
            headerSliverBuilder: (context, value) {
              return [
                SliverToBoxAdapter(
                  child: Obx(() => Container(
                    margin: EdgeInsets.only(top: 8.h),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 12.w),
                          child: GetBuilder<CreateExamController>(builder: (controller) {
                            return MyOutlineBorderTextFormFieldNhat(
                              enable: true,
                              focusNode: controller.focusNameExercise,
                              iconPrefix: null,
                              iconSuffix: null,
                              state: StateType.DEFAULT,
                              labelText: "Tên Bài Kiểm Tra",
                              autofocus: false,
                              controller: controller.controllerNameExam,
                              helperText: "Vui lòng nhập tên bài kiểm tra",
                              showHelperText: controller.isShowErrorTextNameExam,
                              textInputAction: TextInputAction.next,
                              ishowIconPrefix: false,
                              keyboardType: TextInputType.text,
                            );
                          },),
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 12.w),
                          child: GetBuilder<CreateExamController>(builder: (controller) {
                            return MyOutlineBorderTextFormFieldNhat(
                              enable: true,
                              focusNode: controller.focusDescribe,
                              iconPrefix: null,
                              iconSuffix: null,
                              state: StateType.DEFAULT,
                              labelText: "Mô Tả",
                              autofocus: false,
                              controller: controller.controllerDescribe,
                              helperText: "",
                              showHelperText: false,
                              textInputAction: TextInputAction.next,
                              ishowIconPrefix: false,
                              keyboardType: TextInputType.text,
                            );
                          },),
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 8.h),
                          margin: EdgeInsets.symmetric(horizontal: 12.w),
                          height: 56,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: const Color.fromRGBO(192, 192, 192, 1)),
                              borderRadius: BorderRadius.circular(8)),
                          child: Theme(
                            data: Theme.of(context).copyWith(
                              canvasColor: Colors.white,
                            ),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                isExpanded: true,
                                iconSize: 0,
                                icon: const Visibility(
                                    visible: false,
                                    child: Icon(Icons.arrow_downward)),
                                elevation: 16,
                                hint: controller.typeExam.value != ""
                                    ? Row(
                                  children: [
                                    Text(
                                      controller.typeExam.value,
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w400,
                                          color: ColorUtils.PRIMARY_COLOR),
                                    ),
                                    Expanded(child: Container()),
                                    const Icon(
                                      Icons.keyboard_arrow_down,
                                      color: Colors.black,
                                      size: 18,
                                    )
                                  ],
                                )
                                    : Row(
                                  children: [
                                    Text(
                                      'Loại bài kiểm tra',
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black),
                                    ),
                                    Expanded(child: Container()),
                                    const Icon(
                                      Icons.keyboard_arrow_down,
                                      color: Colors.black,
                                      size: 18,
                                    )
                                  ],
                                ),
                                items: [
                                  'Trắc nghiệm',
                                  'Tự luận',
                                  'Trắc nghiệm & tự luận',
                                ].map(
                                      (value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(
                                        value,
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w400,
                                            color: ColorUtils.PRIMARY_COLOR),
                                      ),
                                    );
                                  },
                                ).toList(),
                                onChanged: (String? value) {
                                  controller.typeExam.value = value!;
                                  switch(controller.typeExam.value){
                                    case "Trắc nghiệm":
                                      for(int i = 0;i<Get.find<QuestionExamController>().groupValue.length;i++){
                                        Get.find<QuestionExamController>().groupValue[i] = 0;
                                      }
                                      break;
                                    case "Tự luận":
                                      for(int i = 0;i<Get.find<QuestionExamController>().groupValue.length;i++){
                                        Get.find<QuestionExamController>().groupValue[i] = 1;
                                      }
                                      break;
                                    case "Trắc nghiệm & tự luận":
                                      for(int i = 0;i<Get.find<QuestionExamController>().groupValue.length;i++){
                                        Get.find<QuestionExamController>().groupValue[i] = 0;
                                      }
                                      break;
                                  }
                                },
                              ),
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        GetBuilder<CreateExamController>(builder: (controller) {
                          return Container(
                            margin: EdgeInsets.symmetric(horizontal: 12.w),
                            child: Column(

                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(right: 4.w),
                                            alignment: Alignment.centerLeft,
                                            padding: EdgeInsets.only(left: 8.w),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: const BorderRadius.all(
                                                    Radius.circular(6.0)),
                                                border: Border.all(
                                                  width: 1,
                                                  style: BorderStyle.solid,
                                                  color: const Color.fromRGBO(
                                                      192, 192, 192, 1),
                                                )),
                                            child: TextFormField(
                                              keyboardType: TextInputType.multiline,
                                              maxLines: null,
                                              style: TextStyle(
                                                fontSize: 12.0.sp,
                                                color:
                                                const Color.fromRGBO(26, 26, 26, 1),
                                              ),
                                              onTap: () {
                                                selectDateTimeStart(
                                                    controller
                                                        .controllerDateStart.value.text,
                                                    DateTimeFormat.formatDateShort,
                                                    context);
                                              },
                                              readOnly: true,
                                              cursorColor: ColorUtils.PRIMARY_COLOR,
                                              controller: controller.controllerDateStart,
                                              decoration: InputDecoration(
                                                suffixIcon: Container(
                                                  margin: EdgeInsets.only(bottom: 4.h),
                                                  child: SizedBox(
                                                    height: 14,
                                                    width: 14,
                                                    child: Container(
                                                      margin: EdgeInsets.zero,
                                                      child: SvgPicture.asset(
                                                        "assets/images/icon_date_picker.svg",
                                                        fit: BoxFit.scaleDown,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                label: const Text("Ngày bắt đầu"),
                                                border: InputBorder.none,
                                                labelStyle: TextStyle(
                                                    color: const Color.fromRGBO(
                                                        177, 177, 177, 1),
                                                    fontSize: 12.sp,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf'),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),),
                                    Expanded(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(left: 4.w),
                                            alignment: Alignment.centerLeft,
                                            padding: EdgeInsets.only(left: 8.w),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: const BorderRadius.all(
                                                    Radius.circular(6.0)),
                                                border: Border.all(
                                                  width: 1,
                                                  style: BorderStyle.solid,
                                                  color: const Color.fromRGBO(
                                                      192, 192, 192, 1),
                                                )),
                                            child: TextFormField(
                                              keyboardType: TextInputType.multiline,
                                              maxLines: null,
                                              style: TextStyle(
                                                fontSize: 12.0.sp,
                                                color:
                                                const Color.fromRGBO(26, 26, 26, 1),
                                              ),
                                              onTap: () {
                                                selectTimeStart(
                                                    controller
                                                        .controllerTimeStart.value.text,
                                                    DateTimeFormat.formatTime);
                                              },
                                              readOnly: true,
                                              cursorColor: ColorUtils.PRIMARY_COLOR,
                                              controller: controller.controllerTimeStart,
                                              decoration: InputDecoration(
                                                suffixIcon: Container(
                                                  margin: EdgeInsets.only(bottom: 4.h),
                                                  child: SizedBox(
                                                    height: 14,
                                                    width: 14,
                                                    child: Container(
                                                      margin: EdgeInsets.zero,
                                                      child: SvgPicture.asset(
                                                        "assets/images/icon_date_picker.svg",
                                                        fit: BoxFit.scaleDown,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                label: const Text("Giờ bắt đầu"),
                                                border: InputBorder.none,
                                                labelStyle: TextStyle(
                                                    color: const Color.fromRGBO(
                                                        177, 177, 177, 1),
                                                    fontSize: 12.sp,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf'),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Visibility(
                                        visible: controller.isShowErrorTextDateStart,
                                        child: Container(
                                            padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                            child: const Text(
                                              "Vui lòng nhập ngày nộp",
                                              style: TextStyle(color: Colors.red),
                                            )),
                                      ),),
                                    Expanded(
                                      flex: 1,
                                      child: Visibility(
                                        visible: controller.isShowErrorTextTimeStart,
                                        child: Container(
                                            padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                            child: const Text(
                                              "Vui lòng nhập giờ nộp",
                                              style: TextStyle(color: Colors.red),
                                            )),
                                      ),
                                    )
                                  ],
                                ),

                              ],
                            ),
                          );
                        },),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        GetBuilder<CreateExamController>(builder: (controller) {
                          return Container(
                            margin: EdgeInsets.symmetric(horizontal: 12.w),
                            child: Column(

                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(right: 4.w),
                                            alignment: Alignment.centerLeft,
                                            padding: EdgeInsets.only(left: 8.w),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: const BorderRadius.all(
                                                    Radius.circular(6.0)),
                                                border: Border.all(
                                                  width: 1,
                                                  style: BorderStyle.solid,
                                                  color: const Color.fromRGBO(
                                                      192, 192, 192, 1),
                                                )),
                                            child: TextFormField(
                                              keyboardType: TextInputType.multiline,
                                              maxLines: null,
                                              style: TextStyle(
                                                fontSize: 12.0.sp,
                                                color:
                                                const Color.fromRGBO(26, 26, 26, 1),
                                              ),
                                              onTap: () {
                                                selectDateTimeEnd(
                                                    controller
                                                        .controllerDateEnd.value.text,
                                                    DateTimeFormat.formatDateShort,
                                                    context);
                                              },
                                              readOnly: true,
                                              cursorColor: ColorUtils.PRIMARY_COLOR,
                                              controller: controller.controllerDateEnd,
                                              decoration: InputDecoration(
                                                suffixIcon: Container(
                                                  margin: EdgeInsets.only(bottom: 4.h),
                                                  child: SizedBox(
                                                    height: 14,
                                                    width: 14,
                                                    child: Container(
                                                      margin: EdgeInsets.zero,
                                                      child: SvgPicture.asset(
                                                        "assets/images/icon_date_picker.svg",
                                                        fit: BoxFit.scaleDown,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                label: const Text("Ngày kết thúc"),
                                                border: InputBorder.none,
                                                labelStyle: TextStyle(
                                                    color: const Color.fromRGBO(
                                                        177, 177, 177, 1),
                                                    fontSize: 12.sp,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf'),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),),
                                    Expanded(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(left: 4.w),
                                            alignment: Alignment.centerLeft,
                                            padding: EdgeInsets.only(left: 8.w),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: const BorderRadius.all(
                                                    Radius.circular(6.0)),
                                                border: Border.all(
                                                  width: 1,
                                                  style: BorderStyle.solid,
                                                  color: const Color.fromRGBO(
                                                      192, 192, 192, 1),
                                                )),
                                            child: TextFormField(
                                              keyboardType: TextInputType.multiline,
                                              maxLines: null,
                                              style: TextStyle(
                                                fontSize: 12.0.sp,
                                                color:
                                                const Color.fromRGBO(26, 26, 26, 1),
                                              ),
                                              onTap: () {
                                                selectTimeEnd(
                                                    controller
                                                        .controllerTimeEnd.value.text,
                                                    DateTimeFormat.formatTime);
                                              },
                                              readOnly: true,
                                              cursorColor: ColorUtils.PRIMARY_COLOR,
                                              controller: controller.controllerTimeEnd,
                                              decoration: InputDecoration(
                                                suffixIcon: Container(
                                                  margin: EdgeInsets.only(bottom: 4.h),
                                                  child: SizedBox(
                                                    height: 14,
                                                    width: 14,
                                                    child: Container(
                                                      margin: EdgeInsets.zero,
                                                      child: SvgPicture.asset(
                                                        "assets/images/icon_date_picker.svg",
                                                        fit: BoxFit.scaleDown,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                label: const Text("Giờ kết thúc"),
                                                border: InputBorder.none,
                                                labelStyle: TextStyle(
                                                    color: const Color.fromRGBO(
                                                        177, 177, 177, 1),
                                                    fontSize: 12.sp,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf'),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Visibility(
                                        visible: controller.isShowErrorTextDateEnd,
                                        child: Container(
                                            padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                            child: const Text(
                                              "Vui lòng nhập ngày nộp",
                                              style: TextStyle(color: Colors.red),
                                            )),
                                      ),),
                                    Expanded(
                                      flex: 1,
                                      child: Visibility(
                                        visible: controller.isShowErrorTextTimeEnd,
                                        child: Container(
                                            padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                            child: const Text(
                                              "Vui lòng nhập giờ nộp",
                                              style: TextStyle(color: Colors.red),
                                            )),
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          );
                        },),
                      ],
                    ),
                  )),
                ),
              ];
            },
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                TabBar(
                  padding: EdgeInsets.symmetric(horizontal: 16.h),
                  controller: controller.tabController,
                  labelColor: ColorUtils.PRIMARY_COLOR,
                  isScrollable: true,
                  indicatorColor: Colors.transparent,
                  unselectedLabelColor: const Color.fromRGBO(177, 177, 177, 1),
                  tabs: controller.myTabs,
                  onTap: (value) {
                    controller.indexTabbar = value;
                    controller.update();
                  },
                ),
                Expanded(child: TabBarView(
                  controller: controller.tabController,
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    QuestionExamPage(),
                    FileUploadExamPage(),
                    AttachLinkExamPage(),
                  ],
                ))
              ],
            ),
          ),
          GetBuilder<CreateExamController>(builder: (controller) {
            return controller.tabController?.index == 1?Positioned(
                bottom: 0.h,
                right: 0.w,
                left: 0.w,
                child: InkWell(
                  onTap: () {
                    if (Get.find<CreateExamController>().controllerNameExam.text.trim() == "") {
                      Get.find<CreateExamController>().isShowErrorTextNameExam = true;
                      Get.find<CreateExamController>().update();
                    } else {
                      Get.find<CreateExamController>().isShowErrorTextNameExam = false;
                      Get.find<CreateExamController>().update();
                      if (Get.find<CreateExamController>().controllerDateStart.text.trim() == "") {
                        Get.find<CreateExamController>().isShowErrorTextDateStart = true;
                        Get.find<CreateExamController>().update();
                      } else {
                        Get.find<CreateExamController>().isShowErrorTextDateStart = false;
                        Get.find<CreateExamController>().update();
                        if(Get.find<CreateExamController>().controllerTimeStart.text.trim() == ""){
                          Get.find<CreateExamController>().isShowErrorTextTimeStart = true;
                          Get.find<CreateExamController>().update();
                        }else{
                          Get.find<CreateExamController>().isShowErrorTextTimeStart = false;
                          Get.find<CreateExamController>().update();
                          if(Get.find<CreateExamController>().controllerDateEnd.text.trim() == ""){
                            Get.find<CreateExamController>().isShowErrorTextDateEnd = true;
                            Get.find<CreateExamController>().update();
                          }else{
                            Get.find<CreateExamController>().isShowErrorTextDateEnd = false;
                            Get.find<CreateExamController>().update();
                            if (Get.find<CreateExamController>().controllerTimeEnd.text.trim() == "") {
                              Get.find<CreateExamController>().isShowErrorTextTimeEnd = true;
                              Get.find<CreateExamController>().update();
                            }else{
                              Get.find<CreateExamController>().isShowErrorTextTimeEnd = false;
                              Get.find<CreateExamController>().update();
                              if(DateFormat("dd/MM/yyyy HH:mm").
                              parse("${Get.find<CreateExamController>().controllerDateStart.text.trim()} "
                                  "${Get.find<CreateExamController>().controllerTimeStart.text.trim()}")
                                  .millisecondsSinceEpoch >
                                  DateFormat("dd/MM/yyyy HH:mm").
                                  parse("${Get.find<CreateExamController>().controllerDateEnd.text.trim()} "
                                      "${Get.find<CreateExamController>().controllerTimeEnd.text.trim()}")
                                      .millisecondsSinceEpoch){
                                AppUtils.shared.showToast("Vui lòng chọn thời gian bắt đầu nhỏ hơn thời gian kết thúc");
                              }else{
                                if(Get.find<FileUploadExamController>().files.isEmpty){
                                  AppUtils.shared.showToast("Vui lòng thêm file");
                                }else{
                                  if (Get.find<FileUploadExamController>().controllerPoint.text.trim() == "") {
                                    Get.find<FileUploadExamController>().isShowErrorPoint = true;
                                    Get.find<FileUploadExamController>().errorTextPoint.value = "Vui lòng nhập điểm cho câu hỏi";
                                    Get.find<FileUploadExamController>().update();
                                  }else{
                                    Get.find<FileUploadExamController>().isShowErrorPoint= false;
                                    Get.find<FileUploadExamController>().update();
                                    if(Get.find<FileUploadExamController>().controllerPoint.text.trim().substring(Get.find<FileUploadExamController>().controllerPoint.text.trim().length - 1) == "."){
                                      Get.find<FileUploadExamController>().isShowErrorPoint = true;
                                      Get.find<FileUploadExamController>().errorTextPoint.value = "Vui lòng nhập đúng định dạng điểm";
                                      Get.find<FileUploadExamController>().update();
                                    }
                                    else if (double.tryParse(Get.find<FileUploadExamController>().controllerPoint.text.trim()) == null) {
                                      Get.find<FileUploadExamController>().isShowErrorPoint = true;
                                      Get.find<FileUploadExamController>().errorTextPoint.value = "Vui lòng nhập đúng định dạng điểm";
                                      Get.find<FileUploadExamController>().update();
                                    }else{
                                      Get.find<FileUploadExamController>().createExam(
                                          Get.find<CreateExamController>().controllerNameExam.text.trim()
                                          , DateFormat("dd/MM/yyyy HH:mm").parse("${Get.find<CreateExamController>().controllerDateStart.text.trim()} ${Get.find<CreateExamController>().controllerTimeStart.text.trim()}").millisecondsSinceEpoch
                                          , DateFormat("dd/MM/yyyy HH:mm").parse("${Get.find<CreateExamController>().controllerDateEnd.text.trim()} ${Get.find<CreateExamController>().controllerTimeEnd.text.trim()}").millisecondsSinceEpoch
                                          , Get.find<CreateExamController>().controllerDescribe.text.trim()
                                          , Get.find<FileUploadExamController>().setTypeExercise(Get.find<CreateExamController>().typeExam.value)
                                          , Get.find<LearningManagementTeacherController>().detailSubject.value.id
                                          , AppCache().userId
                                          , Get.find<TeacherHomeController>().currentClass.value.classId
                                          , Get.find<FileUploadExamController>().filesUploadExam
                                          , double.tryParse(Get.find<FileUploadExamController>().controllerPoint.text.trim()));
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }

                      }
                    }
                  },
                  child: GetBuilder<FileUploadExamController>(builder: (controller) {
                    return Container(
                      color: Colors.white,
                      margin: EdgeInsets.symmetric(horizontal: 16.w, vertical: 16.h),
                      child: Container(
                        height: 40.h,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          border: Border.all(
                              color:  Get.find<CreateExamController>().getColorButtonFileUpload()
                          ),
                        ),
                        alignment: Alignment.center,
                        child: Text("Xác nhận",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color:  Get.find<CreateExamController>().getColorButtonFileUpload()),
                            textAlign: TextAlign.center),
                      ),
                    );
                  },),
                )):const SizedBox();
          },)

        ],
      ),


    );
  }


  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10),
        isDisablePreviousDay: true)
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (int.parse(date.substring(6, 10)) <= DateTime.now().year &&
        int.parse(date.substring(3, 5)) <= DateTime.now().month &&
        int.parse(date.substring(0, 2)) < DateTime.now().day) {
      AppUtils.shared.showToast("Vui lòng không chọn ngày trong quá khứ");
      controller.controllerDateEnd.text = "";
    }else{
      controller.controllerDateEnd.text = date;
    }
    
  }


  selectTimeEnd(stringTime,format) async {
    var now = DateTime.now();
    if (!stringTime.isEmpty) {
      now = TimeUtils.convertStringToDate(stringTime, format);
    }
    var time = "";
    await DateTimePicker.showTimePicker(Get.context!, now).then((times) {
      time =
          TimeUtils.convertDateTimeToFormat(times, DateTimeFormat.formatTime);
    });

    if (time.isNotEmpty) {
      controller.controllerTimeEnd.text = time;
    }
  }



  selectDateTimeStart(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10),
        isDisablePreviousDay: true)
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (int.parse(date.substring(6, 10)) <= DateTime.now().year &&
        int.parse(date.substring(3, 5)) <= DateTime.now().month &&
        int.parse(date.substring(0, 2)) < DateTime.now().day) {
      AppUtils.shared.showToast("Vui lòng không chọn ngày trong quá khứ");
      controller.controllerDateStart.text = "";
    }else{
      controller.controllerDateStart.text = date;
    }
    
  }


  selectTimeStart(stringTime,format) async {
    var now = DateTime.now();
    if (!stringTime.isEmpty) {
      now = TimeUtils.convertStringToDate(stringTime, format);
    }
    var time = "";
    await DateTimePicker.showTimePicker(Get.context!, now).then((times) {
      time =
          TimeUtils.convertDateTimeToFormat(times, DateTimeFormat.formatTime);
    });

    if (time.isNotEmpty) {
      controller.controllerTimeStart.text = time;
    }
  }
}
