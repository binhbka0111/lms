import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/res/exercise/exercise_link.dart';
import '../../../../../../../../data/repository/exams/exam_repo.dart';



class AttachLinkExamController extends GetxController{
  final ExamRepo examRepo = ExamRepo();
  var focusAttachLink = FocusNode();
  var controllerAttachLink = TextEditingController();
  var textAttachLink = "".obs;
  var exerciseLink = ExerciseLink().obs;
  var isShowErrorLink = false;
  var isShowErrorPoint = false;
  var errorTextPoint = "".obs;
  var focusPoint = FocusNode();
  var controllerPoint = TextEditingController();


  createExam(title, startTime,endTime, description, typeExercise, subjectId, teacherId, classId, link,scoreOfExam) {
    examRepo.createExamLink(title,startTime,endTime, description, typeExercise, subjectId, teacherId, classId, link,scoreOfExam).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Tạo bài kiểm tra thành công");
        Get.back();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tạo bài kiểm tra thất bại",
            backgroundColor: ColorUtils.COLOR_GREEN_BOLD);
      }
    });
  }
  setTypeExercise(type){
    switch(type){
      case "Trắc nghiệm & tự luận":
        return "ALL";
      case "Trắc nghiệm":
        return "SELECTED_RESPONSE";
      case "Tự luận":
        return "CONSTRUCTED_RESPONSE";
      default:
        return "";
    }
  }


}