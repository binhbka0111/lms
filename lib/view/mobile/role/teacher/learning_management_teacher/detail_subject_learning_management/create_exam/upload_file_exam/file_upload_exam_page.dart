import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/global.dart';
import '../../../../../../../../commom/utils/file_device.dart';
import '../../../../../../../../commom/widget/file_widget.dart';
import '../../../../../../../../data/model/common/req_file.dart';
import '../create_exam_controller.dart';
import 'file_upload_exam_controller.dart';

class FileUploadExamPage extends GetWidget<FileUploadExamController> {
  @override
  final controller = Get.put(FileUploadExamController());

   FileUploadExamPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
      GestureDetector(
        onTap: (){
          dismissKeyboard();
          Get.find<CreateExamController>().scrollController?.animateTo(
            0,
            duration: const Duration(milliseconds: 500),
            curve: Curves.easeOut,
          );
        },
        child: GetBuilder<FileUploadExamController>(builder: (controller){
          return LayoutBuilder(builder: (context,constraints){
            return SingleChildScrollView(
              physics:const ClampingScrollPhysics(),
              child: Column(
                children: [
                  Obx(() =>   Column(
                    children: [
                      InkWell(
                        onTap: () {
                          FileDevice.showSelectFileV2(Get.context!).then((value) {
                            if (value.isNotEmpty) {
                              controller.uploadFile(value);
                            }
                          });
                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 16.w),
                          child: DottedBorder(
                              dashPattern: const [5, 5],
                              radius: Radius.circular(6.r),
                              borderType: BorderType.RRect,
                              color: const Color.fromRGBO(192, 192, 192, 1),
                              child: SizedBox(
                                width: double.infinity,
                                child: controller.files.isNotEmpty
                                    ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    getFileWidgets(controller.files),
                                    Padding(padding: EdgeInsets.only(top: 8.h)),
                                    TextButton(
                                        onPressed: () {
                                          FileDevice.showSelectFileV2(Get.context!)
                                              .then((value) {
                                            if (value.isNotEmpty) {
                                              controller.uploadFile(value);
                                            }
                                          });
                                        },
                                        child: Text(
                                          "Thêm tệp",
                                          style: TextStyle(
                                              color: const Color.fromRGBO(
                                                  72, 98, 141, 1),
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w400),
                                        )),
                                  ],
                                )
                                    : Column(
                                  children: [
                                    Padding(padding: EdgeInsets.only(top: 16.h)),
                                    const Icon(
                                      Icons.drive_folder_upload_rounded,
                                      color: ColorUtils.PRIMARY_COLOR,
                                      size: 40,
                                    ),
                                    Padding(padding: EdgeInsets.only(top: 4.h)),
                                    Text(
                                      "Tải lên Ảnh / Video",
                                      style: TextStyle(
                                          color: const Color.fromRGBO(
                                              133, 133, 133, 1),
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Padding(padding: EdgeInsets.only(top: 12.h)),
                                    Text(
                                      "File (Video, Ảnh, Zip,...) có dung lượng không quá 10Mb",
                                      style: TextStyle(
                                          color: const Color.fromRGBO(
                                              133, 133, 133, 1),
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w400),
                                    ),
                                    Padding(padding: EdgeInsets.only(bottom: 20.h)),
                                  ],
                                ),
                              )),
                        ),
                      ),
                      SizedBox(
                        height: 8.h,
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 16.w),
                        child: FocusScope(
                          node: FocusScopeNode(),
                          child: Focus(
                              onFocusChange: (focus) {
                                controller.update();
                              },
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    padding: const EdgeInsets.all(2.0),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                        const BorderRadius.all(Radius.circular(6.0)),
                                        border: Border.all(
                                          width: 1,
                                          style: BorderStyle.solid,
                                          color: controller.focusPoint.hasFocus
                                              ? ColorUtils.PRIMARY_COLOR
                                              : const Color.fromRGBO(192, 192, 192, 1),
                                        )),
                                    child: TextFormField(
                                      cursorColor: ColorUtils.PRIMARY_COLOR,
                                      focusNode: controller.focusPoint,
                                      inputFormatters: <TextInputFormatter> [
                                        FilteringTextInputFormatter.allow(RegExp('[0-9.,]')),
                                      ],
                                      onChanged: (value) {
                                        if(controller.controllerPoint.text.trim() != ""){
                                          controller.isShowErrorPoint = false;
                                        }
                                        controller.update();
                                      },
                                      onFieldSubmitted: (value) {
                                        controller.update();
                                        Get.find<CreateExamController>().scrollController?.animateTo(
                                          0,
                                          duration: const Duration(milliseconds: 500),
                                          curve: Curves.easeOut,
                                        );
                                      },
                                      controller: controller.controllerPoint,
                                      keyboardType: TextInputType.number,

                                      decoration: InputDecoration(
                                        labelText: "Nhập điểm bài tập",
                                        labelStyle: TextStyle(
                                            fontSize: 14.0.sp,
                                            color: const Color.fromRGBO(177, 177, 177, 1),
                                            fontWeight: FontWeight.w500,
                                            fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                                        contentPadding: const EdgeInsets.symmetric(
                                            vertical: 8, horizontal: 16),
                                        prefixIcon:  null,
                                        suffixIcon:  Visibility(
                                            visible:
                                            controller.controllerPoint.text.isNotEmpty ==
                                                true,
                                            child: InkWell(
                                              onTap: () {
                                                controller.controllerPoint.text = "";
                                              },
                                              child: const Icon(
                                                Icons.close_outlined,
                                                color: Colors.black,
                                                size: 20,
                                              ),
                                            )),
                                        enabledBorder: InputBorder.none,
                                        errorBorder: InputBorder.none,
                                        border: InputBorder.none,
                                        errorStyle: const TextStyle(height: 0),
                                        focusedErrorBorder: InputBorder.none,
                                        disabledBorder: InputBorder.none,
                                        focusedBorder: InputBorder.none,
                                        floatingLabelBehavior: FloatingLabelBehavior.auto,

                                      ),
                                    ),
                                  ),
                                  Visibility(
                                    visible: controller.isShowErrorPoint,
                                    child: Container(
                                        padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                        child: Text(
                                          controller.errorTextPoint.value,
                                          style: const TextStyle(color: Colors.red),
                                        )),
                                  )
                                ],
                              )),
                        ),
                      ),],
                  ),),

                ],
              ),
            );
          });



        }),
      )
      ;
  }

  Widget getFileWidgets(List<ReqFile> listFile) {
    return Card(
      child: ListView.builder(
        physics: const NeverScrollableScrollPhysics(),
          padding: EdgeInsets.zero,
          itemCount: listFile.length,
          shrinkWrap: true,
          itemBuilder: (context, i) {
            return FileWidget.itemFile(Get.context!,
                remove: true, file: listFile[i], function: () {
              controller.files.removeAt(i);
              controller.files.refresh();
              controller.filesUploadExam.removeAt(i);
              controller.filesUploadExam.refresh();
            });
          }),
    );
  }
}
