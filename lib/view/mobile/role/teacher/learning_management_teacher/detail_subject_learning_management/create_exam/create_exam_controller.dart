import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/create_exam/upload_file_exam/file_upload_exam_controller.dart';

import '../../../../../../../commom/utils/color_utils.dart';

class CreateExamController extends GetxController with GetSingleTickerProviderStateMixin{
  var controllerDateEnd = TextEditingController();
  var controllerTimeEnd = TextEditingController();
  var controllerDateStart = TextEditingController();
  var controllerTimeStart  = TextEditingController();
  var focusNameExercise = FocusNode();
  var controllerNameExam = TextEditingController();
  var focusAssure = FocusNode();
  var controllerAssure = TextEditingController();
  var focusDescribe = FocusNode();
  var controllerDescribe = TextEditingController();
  var isShowErrorTextNameExam = false;
  var isShowErrorTextDateEnd= false;
  var isShowErrorTextTimeEnd= false;
  var isShowErrorTextDateStart= false;
  var isShowErrorTextTimeStart= false;
  var errorTextDateStart = "Vui lòng nhập ngày bắt đầu";
  var errorTextTimeStart = "Vui lòng nhập giờ bắt đầu";
  var errorTextDateEnd = "Vui lòng nhập ngày kết thúc";
  var errorTextTimeEnd = "Vui lòng nhập giờ kết thúc";
  var coefficient = ''.obs;
  var isExpand = true.obs;
  var typeExam = 'Trắc nghiệm & tự luận'.obs;
  String getTempIFSCValidation(String text) {
    return text.length > 7 ? "This line is helper text" : "";
  }
  var outputDateFormat = DateFormat('dd/MM/yyyy');

  var listStatus = <String>[].obs;
  var indexClick = 0.obs;
  int indexTabbar = 0;
  RxList<bool> click = <bool>[].obs;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  TabController? tabController ;
  ScrollController? scrollController;
  bool fixedScroll = false;
  final List<Widget> myTabs = [
    const Tab(text: 'Câu hỏi',),
    const Tab(text: 'Đăng Tải Tệp'),
    const Tab(text: 'Gắn Link'),
  ];
  @override
  void onInit() {
    scrollController = ScrollController();
    tabController = TabController(length: 3, vsync: this);
    super.onInit();
  }


  @override
  dispose() {
    tabController?.dispose();
    scrollController?.dispose();
    super.dispose();
  }



  showColor(index) {
    click.value = [];
    for (int i = 0; i < 3; i++) {
      click.add(false);
    }
    click[index] = true;
  }


  onPageViewChange(int page) {
    indexClick.value = page;
    if (page != 0) {
      indexClick.value - 1;
    } else {
      indexClick.value = 0;
    }
  }

  getColorButtonFileUpload(){
    if(Get.isRegistered<FileUploadExamController>()){
      if(Get.find<FileUploadExamController>().files.isNotEmpty &&Get.find<FileUploadExamController>().controllerPoint.text != ""){
        return ColorUtils.PRIMARY_COLOR;
      }else{
        return const Color.fromRGBO(177, 177, 177, 1);
      }
    }else{
      return const Color.fromRGBO(177, 177, 177, 1);
    }
  }


}