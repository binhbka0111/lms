import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import 'dart:io';
import '../../../../../../../../commom/app_cache.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/req_file.dart';
import '../../../../../../../../data/model/res/exercise/exercise.dart';
import '../../../../../../../../data/repository/exams/exam_repo.dart';
import '../../../../../../../../data/repository/file/file_repo.dart';
import '../../../../teacher_home_controller.dart';
import '../../learning_management_teacher_controller.dart';
import '../create_exam_controller.dart';


class QuestionExamController extends GetxController {
  var groupValue = <int>[].obs;
  var focusScoreQuestion = <FocusNode>[];
  var controllerScoreQuestion = <TextEditingController>[];
  var focusQuestion = <FocusNode>[];
  var controllerQuestion = <TextEditingController>[];
  var listErrorTextPointQuestion = <String>[].obs;

  RxInt indexClick = 0.obs;
  final  listQuestion = <Questions>[].obs;
  var listStatusAnswer = <dynamic>[].obs;
  var question = Questions().obs;
  final FileRepo fileRepo = FileRepo();
  final ExamRepo examRepo = ExamRepo();
  var isShowNote = <bool>[].obs;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  var files = <ReqFile>[].obs;
  var listFile = <dynamic>[].obs;

  onPageViewChange(int page) {
    indexClick.value = page;
    if (page != 0) {
      indexClick.value - 1;
    } else {
      indexClick.value = 0;
    }
  }

  @override
  void onInit() {
    question.value.files = [];
    question.value.answerOption = [];
    question.value.answerOption?.add(AnswerOption());
    question.value.errorContentQuestion = false;
    question.value.errorPointQuestion = false;
    question.value.answerOption![0] = AnswerOption(
        key: "A",
        value: "",
        status: "FALSE",
        textEditingController: TextEditingController(),
        focusNode: FocusNode(),
        validateAnswer: false,
        errorTextAnswer: ""
        );
    listQuestion.add(question.value);

    for (int i = 0; i < listQuestion.length; i++) {
      groupValue.add(0);
      listFile.add(files);
    }
    isShowNote.add(true);
    listErrorTextPointQuestion.add("");
    controllerScoreQuestion = [];
    controllerQuestion = [];
    focusScoreQuestion = [];
    focusQuestion = [];
    super.onInit();
  }

  @override
  dispose() {
    pageController.dispose();
    super.dispose();
  }

  uploadFile(index,file) async  {
    showDialogUploadFile();
    var fileList = <File>[];
    file.forEach((element) {
      fileList.add(element.file!);
    });
   await fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;


        listQuestion[index].files?.addAll(listResFile);
        listQuestion.refresh();
        listFile[index].addAll(file);
        for(int i =0;i<files.length;i++){
          files[i].url = listQuestion[index].files?[i].link;
        }
        listFile.refresh();
        listResFile.clear();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại",
            backgroundColor: ColorUtils.COLOR_GREEN_BOLD);
      }
    });
   Get.back();
  }


  createExam(title,scoreFactor, startTime,endTime, description, typeExercise, subjectId, teacherId, classId, questions) {
    examRepo.createExamQuestions(title,scoreFactor,startTime,endTime, description, typeExercise, subjectId, teacherId, classId, questions).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Tạo bài kiểm tra thành công");
        Get.back();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tạo bài kiểm tra thất bại",
            backgroundColor: ColorUtils.COLOR_GREEN_BOLD);
      }
    });
  }


  setColorArrowForward(index){
    if(listQuestion.length==1 ){
      return const Color.fromRGBO(177, 177, 177, 1);
    }else{
      if(index == listQuestion.length -1){
        return const Color.fromRGBO(177, 177, 177, 1);
      }else{
        return Colors.black;
      }
    }
  }

  confirmCreteExam(){
    var scoreFactor = 0.0;
    for (int i = 0; i < listQuestion.length; i++) {
      listQuestion[i].point = double.tryParse(controllerScoreQuestion[i].text.trim());
      scoreFactor += listQuestion[i].point!;
    }
    for (int i = 0; i < listQuestion.length; i++) {
      listQuestion[i].content = controllerQuestion[i].text.trim();
      listQuestion[i].point = double.tryParse(controllerScoreQuestion[i].text.trim());
      if (groupValue[i] == 0) {
       listQuestion[i].typeQuestion = "SELECTED_RESPONSE";
      }
      if (groupValue[i] == 1) {
       listQuestion[i].typeQuestion = "CONSTRUCTED_RESPONSE";
        listQuestion[i].answerOption = [];
      }
      for (int j = 0; j < listQuestion[i].answerOption!.length; j++) {
        listQuestion[i].answerOption![j].value = listQuestion[i].answerOption![j].textEditingController?.text.trim();
      }
    }
    listQuestion.refresh();
   createExam(
        Get.find<CreateExamController>().controllerNameExam.text.trim(),
        scoreFactor,
       DateFormat("dd/MM/yyyy HH:mm").parse("${Get.find<CreateExamController>().controllerDateStart.text.trim()} ${Get.find<CreateExamController>().controllerTimeStart.text.trim()}").millisecondsSinceEpoch,
         DateFormat("dd/MM/yyyy HH:mm").parse("${Get.find<CreateExamController>().controllerDateEnd.text.trim()} ${Get.find<CreateExamController>().controllerTimeEnd.text.trim()}").millisecondsSinceEpoch,
        Get.find<CreateExamController>().controllerDescribe.text.trim(),
        setTypeExercise(Get.find<CreateExamController>().typeExam.value),
        Get.find<LearningManagementTeacherController>().detailSubject.value.id,
        AppCache().userId,
        Get.find<TeacherHomeController>().currentClass.value.classId,
        listQuestion);
  }



  setTypeExercise(type){
    switch(type){
      case "Trắc nghiệm & tự luận":
        return "ALL";
      case "Trắc nghiệm":
        return "SELECTED_RESPONSE";
      case "Tự luận":
        return "CONSTRUCTED_RESPONSE";
      default:
        return "";
    }
  }

}
