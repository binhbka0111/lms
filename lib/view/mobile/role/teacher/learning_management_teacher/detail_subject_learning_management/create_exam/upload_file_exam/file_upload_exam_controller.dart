import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/req_file.dart';
import 'dart:io';
import '../../../../../../../../data/model/res/exercise/exercise_file.dart';
import '../../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../../../data/repository/exams/exam_repo.dart';
import '../../../../../../../../data/repository/file/file_repo.dart';

class FileUploadExamController extends GetxController{
  final ExamRepo examRepo = ExamRepo();
  final FileRepo fileRepo = FileRepo();
  var exerciseFile = ExerciseFile().obs;
  var filesUploadExam = <ResponseFileUpload>[].obs;
  var files = <ReqFile>[].obs;
  var isShowErrorPoint = false;
  var errorTextPoint = "".obs;
  var focusPoint = FocusNode();
  var controllerPoint = TextEditingController();

  uploadFile(file) async {
    showDialogUploadFile();
    var fileList = <File>[];
    for (var element in file) {
      fileList.add(element.file!);
    }

  await fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;

        files.refresh();
        filesUploadExam.addAll(listResFile);
        filesUploadExam.refresh();
        files.addAll(file);
        for(int i =0;i<files.length;i++){
          files[i].url = filesUploadExam[i].link;
        }
        listResFile.clear();

      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_GREEN_BOLD);
      }
    });
    Get.back();
  }



  createExam(title, startTime,endTime, description, typeExercise, subjectId, teacherId, classId, files,scoreOfExam) {
    examRepo.createExamFile(title,startTime,endTime, description, typeExercise, subjectId, teacherId, classId, files,scoreOfExam).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Tạo bài kiểm tra thành công");
        Get.back();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tạo bài kiểm tra thất bại", backgroundColor: ColorUtils.COLOR_GREEN_BOLD);
      }
    });
  }
  setTypeExercise(type){
    switch(type){
      case "Trắc nghiệm & tự luận":
        return "ALL";
      case "Trắc nghiệm":
        return "SELECTED_RESPONSE";
      case "Tự luận":
        return "CONSTRUCTED_RESPONSE";
      default:
        return "";
    }
  }

}