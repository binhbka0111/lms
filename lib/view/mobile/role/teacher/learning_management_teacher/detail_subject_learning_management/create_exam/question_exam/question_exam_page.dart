import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/create_exam/question_exam/question_exam_controller.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/file_device.dart';
import '../../../../../../../../commom/widget/file_widget.dart';
import '../../../../../../../../commom/widget/text_field_custom.dart';
import '../../../../../../../../data/model/common/req_file.dart';
import '../../../../../../../../data/model/res/exercise/exercise.dart';
import '../create_exam_controller.dart';


class QuestionExamPage extends GetView<QuestionExamController> {
  @override
  final controller = Get.put(QuestionExamController());

  QuestionExamPage({super.key});
  @override
  Widget build(BuildContext context) {

    return Obx(() => PageView.builder(

          onPageChanged: (value) {
            controller.onPageViewChange(value);
          },
          controller: controller.pageController,
          itemCount: controller.listQuestion.length,
          itemBuilder: (context, index) {
            controller.controllerQuestion.add(TextEditingController());
            controller.focusQuestion.add(FocusNode());
            controller.controllerScoreQuestion.add(TextEditingController());
            controller.focusScoreQuestion.add(FocusNode());
            return Obx(() =>
                SingleChildScrollView(
                  physics: const ClampingScrollPhysics(),
                  child: Column(
                    children: [
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 16.w),
                        child: Row(
                          children: [
                            InkWell(
                              onTap: () {
                                if (index >= 1) {
                                  controller.pageController.animateToPage(
                                      index - 1,
                                      duration: const Duration(seconds: 1),
                                      curve: Curves.linear);
                                }
                                controller.listQuestion.refresh();
                              },
                              child: Icon(
                                Icons.arrow_back_ios_new_outlined,
                                color: index == 0
                                    ? const Color.fromRGBO(177, 177, 177, 1)
                                    : Colors.black,
                              ),
                            ),
                            Expanded(child: Container()),
                            Text(
                              "Câu ${index + 1}",
                              style: TextStyle(
                                  fontSize: 14.sp, fontWeight: FontWeight.w400),
                            ),
                            Expanded(child: Container()),
                            InkWell(
                              onTap: () {
                                if (index < controller.listQuestion.length-1) {
                                  controller.pageController.animateToPage(
                                      index + 1,
                                      duration: const Duration(seconds: 1),
                                      curve: Curves.linear);
                                }
                                controller.listQuestion.refresh();
                              },
                              child: Icon(
                                Icons.arrow_forward_ios_outlined,
                                color: controller.setColorArrowForward(index),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 16.h)),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 16.w),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Visibility(
                                visible: Get.find<CreateExamController>().typeExam.value ==
                                    "Trắc nghiệm & tự luận",
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  height: 28.h,
                                  child: Text(
                                    "Loại bài kiểm tra",
                                    style: TextStyle(
                                      color: const Color.fromRGBO(133, 133, 133, 1),
                                      fontSize: 13.sp,
                                      fontWeight: FontWeight.w400,),
                                  ),
                                )),
                            Visibility(
                              visible: Get.find<CreateExamController>().typeExam.value ==
                                  "Trắc nghiệm & tự luận",
                              child: SizedBox(
                                width: 118.w,
                                height: 24.h,
                                child: RadioListTile(
                                    title: Transform.translate(
                                      offset: const Offset(-8, 0),
                                      child: Text(
                                        "Trắc Nghiệm",
                                        style: TextStyle(
                                            fontSize: 12.sp,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.black),
                                      ),
                                    ),
                                    value: 0,
                                    dense: true,
                                    visualDensity: const VisualDensity(
                                      horizontal:
                                      VisualDensity.minimumDensity,
                                      vertical: VisualDensity.minimumDensity,
                                    ),
                                    contentPadding: EdgeInsets.zero,
                                    activeColor: ColorUtils.PRIMARY_COLOR,
                                    groupValue:
                                    controller.groupValue[index],
                                    onChanged: (int? value) {
                                      controller.groupValue[index] = 0;
                                      controller.groupValue.refresh();
                                    }),
                              ),
                            ),
                            Visibility(
                                visible: Get.find<CreateExamController>()
                                    .typeExam
                                    .value ==
                                    "Trắc nghiệm & tự luận",
                                child: SizedBox(
                                  width: 88.w,
                                  height: 24.h,
                                  child: RadioListTile(
                                      title: Transform.translate(
                                        offset: const Offset(-8, 0),
                                        child: Text(
                                          "Tự luận",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w400,
                                              color: Colors.black),
                                        ),
                                      ),
                                      value: 1,
                                      dense: true,
                                      visualDensity: const VisualDensity(
                                        horizontal:
                                        VisualDensity.minimumDensity,
                                        vertical:
                                        VisualDensity.minimumDensity,
                                      ),
                                      contentPadding: EdgeInsets.zero,
                                      activeColor: ColorUtils.PRIMARY_COLOR,
                                      groupValue:
                                      controller.groupValue[index],
                                      onChanged: (int? value) {
                                        controller.groupValue[index] =
                                        1;
                                        controller.groupValue.refresh();
                                      }),
                                )),
                            Visibility(
                                visible: controller.listQuestion.length >1,
                                child: InkWell(
                                  onTap: () {
                                    if( controller.listQuestion.length>1){
                                      controller.controllerQuestion.removeAt(index);
                                      controller.controllerScoreQuestion.removeAt(index);
                                      controller.listQuestion.removeAt(index);
                                      controller.listQuestion.refresh();
                                      controller.pageController.jumpToPage(controller.listQuestion.length-1);
                                      controller.update();
                                    }
                                  },
                                  child:  const Icon(Icons.delete,color: Colors.red,),
                                ))
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      Obx(() => Visibility(
                          visible: controller.isShowNote[index] == true,
                          child: Visibility(
                              visible: controller.groupValue[index] == 0,
                              child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 16.w),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: const Color.fromRGBO(
                                            253, 185, 36, 1)),
                                    color: const Color.fromRGBO(255, 246, 229, 1),
                                    borderRadius: BorderRadius.circular(6.r)),
                                padding: EdgeInsets.symmetric(horizontal: 8.h, vertical: 4.h),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        const Icon(
                                          Icons.info,
                                          color:
                                          Color.fromRGBO(250, 162, 0, 1),
                                        ),
                                        const Icon(
                                          Icons.check,
                                          color:
                                          Color.fromRGBO(77, 197, 145, 1),
                                        ),

                                        const Text("Là đáp án đúng"),

                                        const Icon(
                                          Icons.clear,
                                          color: Color.fromRGBO(
                                              177, 177, 177, 1),
                                        ),
                                        const Text("Là đáp án sai"),
                                        InkWell(
                                          onTap: () {
                                            controller.isShowNote[index] =
                                            false;
                                            controller.isShowNote.refresh();
                                          },
                                          child: const Icon(
                                            Icons.clear,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Padding(padding: EdgeInsets.only(top: 8.h)),
                                    Text(
                                      "Nhấn thêm một lần để đổi trạng thái đáp án",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12.sp),
                                    )
                                  ],
                                ),
                              )))),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              GetBuilder<QuestionExamController>(
                                builder: (controller) {
                                  return Container(
                                    margin:
                                    EdgeInsets.symmetric(horizontal: 0.w),

                                    child: MyTextFormFieldBinh(
                                      enable: true,
                                      focusNode:
                                      controller.focusScoreQuestion[index],
                                      iconPrefix: null,
                                      iconSuffix: null,
                                      state: StateType.DEFAULT,
                                      labelText: "Nhập Điểm Cho Câu Hỏi",
                                      autofocus: false,
                                      controller: controller
                                          .controllerScoreQuestion[index],
                                      helperText: controller.listErrorTextPointQuestion[index],
                                      showHelperText: controller
                                          .listQuestion[index]
                                          .errorPointQuestion,
                                      textInputAction: TextInputAction.next,
                                      ishowIconPrefix: false,
                                      keyboardType: TextInputType.number,
                                    ),
                                  );
                                },
                              ),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              GetBuilder<QuestionExamController>(
                                builder: (controller) {
                                  return Container(
                                    margin:
                                    EdgeInsets.symmetric(horizontal: 16.w),
                                    child: MyOutlineBorderTextFormFieldNhat(
                                      enable: true,
                                      focusNode:
                                      controller.focusQuestion[index],
                                      iconPrefix: null,
                                      iconSuffix: null,
                                      state: StateType.DEFAULT,
                                      labelText: "Nhập câu hỏi",
                                      autofocus: false,
                                      controller: controller.controllerQuestion[index],
                                      helperText: "vui lòng nhập câu hỏi",
                                      showHelperText: controller.listQuestion[index].errorContentQuestion,
                                      textInputAction: TextInputAction.next,
                                      ishowIconPrefix: false,
                                      keyboardType: TextInputType.text,
                                    ),
                                  );
                                },
                              ),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              Visibility(
                                  visible:
                                  controller.groupValue[index] == 0,
                                  child: controller
                                      .listQuestion
                                  [controller.indexClick.value]
                                      .answerOption!
                                      .isNotEmpty
                                      ? ListView.builder(
                                      shrinkWrap: true,
                                      padding: EdgeInsets.zero,
                                      physics:
                                      const NeverScrollableScrollPhysics(),
                                      itemCount: controller
                                          .listQuestion
                                      [
                                      controller.indexClick.value]
                                          .answerOption
                                          ?.length,
                                      itemBuilder: (context, indexAnswer) {
                                        return GetBuilder<
                                            QuestionExamController>(
                                          builder: (controller) {
                                            return Column(
                                              children: [
                                                Container(
                                                  margin: EdgeInsets.symmetric(
                                                      horizontal: 16.w,
                                                      vertical: 4.h),
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        height: 32.h,
                                                        width: 32.h,
                                                        decoration:
                                                        const BoxDecoration(
                                                            shape: BoxShape
                                                                .circle,
                                                            color: Color
                                                                .fromRGBO(
                                                                254,
                                                                230,
                                                                211,
                                                                1)),
                                                        padding:
                                                        EdgeInsets.all(8.h),
                                                        alignment:
                                                        Alignment.center,
                                                        child: Text(
                                                          String.fromCharCode(
                                                              indexAnswer + 65),
                                                          style: TextStyle(
                                                              color: ColorUtils.PRIMARY_COLOR,
                                                              fontSize: 14.sp,
                                                              fontWeight:
                                                              FontWeight
                                                                  .w500),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child:
                                                        OutlineBorderTextFormField(
                                                          enable: true,
                                                          focusNode: controller
                                                              .listQuestion
                                                          [controller
                                                              .indexClick
                                                              .value]
                                                              .answerOption![
                                                          indexAnswer]
                                                              .focusNode!,
                                                          iconPrefix: null,
                                                          iconSuffix: null,
                                                          state:
                                                          StateType.DEFAULT,
                                                          labelText:
                                                          "Nhập câu trả lời ${indexAnswer + 1}",
                                                          autofocus: false,
                                                          controller: controller
                                                              .listQuestion
                                                          [controller
                                                              .indexClick
                                                              .value]
                                                              .answerOption![
                                                          indexAnswer]
                                                              .textEditingController!,
                                                          helperText:"",
                                                          showHelperText: false,
                                                          textInputAction:
                                                          TextInputAction
                                                              .next,
                                                          ishowIconPrefix:
                                                          false,
                                                          keyboardType:
                                                          TextInputType
                                                              .text,
                                                          height: 56,
                                                        ),
                                                      ),
                                                      Visibility(

                                                          visible: controller.listQuestion[controller.indexClick.value].answerOption?[indexAnswer].status == "TRUE",
                                                          child: InkWell(
                                                            onTap: () {
                                                              controller.listQuestion[controller.indexClick.value].answerOption?[indexAnswer].status = "FALSE";
                                                              controller.listQuestion.refresh();
                                                              controller.update();
                                                            },
                                                            child: const Icon(
                                                              Icons.check,
                                                              color: Color
                                                                  .fromRGBO(
                                                                  77,
                                                                  197,
                                                                  145,
                                                                  1),
                                                            ),
                                                          )),
                                                      Visibility(
                                                          visible: controller
                                                              .listQuestion
                                                          [controller
                                                              .indexClick
                                                              .value]
                                                              .answerOption?[
                                                          indexAnswer]
                                                              .status ==
                                                              "FALSE",
                                                          child: InkWell(
                                                            onTap: () {

                                                              for (int i = 0; i < controller.listQuestion[controller.indexClick.value].answerOption!.length; i++) {
                                                                controller.listQuestion[controller.indexClick.value].answerOption?[i].status = "FALSE";
                                                              }
                                                              controller.listQuestion[controller.indexClick.value].answerOption?[indexAnswer].status = "TRUE";
                                                              controller.listQuestion.refresh();
                                                              controller.update();
                                                            },
                                                            child: const Icon(
                                                              Icons.close,
                                                              color: Color
                                                                  .fromRGBO(
                                                                  177,
                                                                  177,
                                                                  177,
                                                                  1),
                                                            ),
                                                          )),
                                                      InkWell(
                                                        onTap: () {
                                                          if (controller
                                                              .listQuestion
                                                          [controller
                                                              .indexClick
                                                              .value]
                                                              .answerOption!
                                                              .length >
                                                              1) {
                                                            controller
                                                                .listQuestion
                                                            [controller
                                                                .indexClick
                                                                .value]
                                                                .answerOption
                                                                ?.removeAt(
                                                                indexAnswer);
                                                            controller
                                                                .listQuestion
                                                                .refresh();
                                                            controller.update();
                                                          }
                                                        },
                                                        child: const Icon(
                                                          Icons.delete,
                                                          color: Color.fromRGBO(
                                                              255, 69, 89, 1),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                Visibility(
                                                  visible: controller.listQuestion[controller.indexClick.value].answerOption![indexAnswer].validateAnswer!,
                                                  child: Container(
                                                      padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                                      child:  Text(
                                                        "${controller.listQuestion[controller.indexClick.value].answerOption?[indexAnswer].errorTextAnswer}",
                                                        style: const TextStyle(color: Colors.red),
                                                      )),
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      })
                                      : Container()),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              Visibility(
                                  visible:
                                  controller.groupValue[index] == 0,
                                  child: InkWell(
                                    onTap: () {
                                      if(controller.listQuestion[index].answerOption!.length<26){
                                        controller.listQuestion[index].answerOption?.add(
                                            AnswerOption(
                                                key: String.fromCharCode(controller.listQuestion[index].answerOption!.length + 65),
                                                status: "FALSE",
                                                value: "",
                                                textEditingController:
                                                TextEditingController(),
                                                focusNode: FocusNode(),
                                                validateAnswer: false));
                                        controller.listQuestion.refresh();
                                      }else{
                                        AppUtils.shared.showToast("Không thể thêm câu trả lời");
                                      }
                                      controller.update();
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(left: 16.w),
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            height: 24.h,
                                            width: 24.h,
                                            child: Image.asset(
                                                "assets/images/image_add_answer_question.png"),
                                          ),
                                          Padding(
                                              padding:
                                              EdgeInsets.only(right: 8.w)),
                                          Text(
                                            "Thêm câu trả lời",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    72, 98, 141, 1),
                                                fontWeight: FontWeight.w400,
                                                fontSize: 14.sp),
                                          )
                                        ],
                                      ),
                                    ),
                                  )),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              InkWell(
                                onTap: () {
                                  FileDevice.showSelectFileV2(Get.context!)
                                      .then((value) {
                                    if (value.isNotEmpty) {
                                      controller.uploadFile(index,value);
                                    }
                                  });
                                },
                                child:
                                Container(
                                  margin:
                                  EdgeInsets.symmetric(horizontal: 16.w),
                                  child: DottedBorder(
                                      dashPattern: const [5, 5],
                                      radius: const Radius.circular(6),
                                      borderType: BorderType.RRect,
                                      color: const Color.fromRGBO(
                                          192, 192, 192, 1),
                                      padding: EdgeInsets.all(16.h),
                                      child: SizedBox(
                                        width: double.infinity,
                                        child: controller.listFile[index].isNotEmpty
                                            ? Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            getFileWidgets(controller.listFile[index], index),
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    top: 8.h)),
                                            TextButton(
                                                onPressed: () {
                                                  FileDevice.showSelectFileV2(Get.context!).then((value) {
                                                    if (value.isNotEmpty) {
                                                      controller.uploadFile(index,value);
                                                    }
                                                  });
                                                },
                                                child: Text(
                                                  "Thêm tệp",
                                                  style: TextStyle(
                                                      color: const Color
                                                          .fromRGBO(
                                                          72, 98, 141, 1),
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                      FontWeight
                                                          .w400),
                                                )),
                                          ],
                                        )
                                            : Column(
                                          children: [
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    top: 16.h)),
                                            const Icon(
                                              Icons
                                                  .drive_folder_upload_rounded,
                                              color: ColorUtils.PRIMARY_COLOR,
                                              size: 40,
                                            ),
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    top: 4.h)),
                                            Text(
                                              "Tải lên Ảnh / Video",
                                              style: TextStyle(
                                                  color: const Color
                                                      .fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontSize: 14.sp,
                                                  fontWeight:
                                                  FontWeight.w500),
                                            ),
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    top: 12.h)),
                                            Text(
                                              "File (Video, Ảnh, Zip,...) có dung lượng không quá 10Mb",
                                              style: TextStyle(
                                                  color: const Color
                                                      .fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontSize: 12.sp,
                                                  fontWeight:
                                                  FontWeight.w400),
                                            ),
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    bottom: 16.h)),
                                          ],
                                        ),
                                      )),
                                ),
                              )
                            ],
                          ),

                        ],
                      ),
                      Container(
                        color: Colors.white,
                        margin: EdgeInsets.only(bottom: 8.h, left: 16.h, right: 16.h,top: 8.h),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child: SizedBox(
                                  width: double.infinity,
                                  height: 48,
                                  child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          side: const BorderSide(
                                              width: 1,
                                              color: ColorUtils.PRIMARY_COLOR),
                                          backgroundColor:
                                          const Color.fromRGBO(
                                              255, 255, 255, 1)),
                                      onPressed: () {
                                        FocusScope.of(context).unfocus();
                                        if (controller.groupValue[index] == 0) {
                                          controller.listQuestion[index].typeQuestion = "SELECTED_RESPONSE";
                                        }
                                        if (controller.groupValue[index] == 1) {
                                          controller.listQuestion[index].typeQuestion = "CONSTRUCTED_RESPONSE";
                                          controller.listQuestion[index].answerOption = [];
                                        }

                                        if (Get.find<CreateExamController>().controllerNameExam.text.trim() == "") {
                                          Get.find<CreateExamController>().isShowErrorTextNameExam = true;
                                          Get.find<CreateExamController>().update();
                                        } else {
                                          Get.find<CreateExamController>().isShowErrorTextNameExam = false;
                                          Get.find<CreateExamController>().update();
                                          if (Get.find<CreateExamController>().controllerDateStart.text.trim() == "") {
                                            Get.find<CreateExamController>().isShowErrorTextDateStart = true;
                                            Get.find<CreateExamController>().update();
                                          } else {
                                            Get.find<CreateExamController>().isShowErrorTextDateStart = false;
                                            Get.find<CreateExamController>().update();
                                            if(Get.find<CreateExamController>().controllerTimeStart.text.trim() == ""){
                                              Get.find<CreateExamController>().isShowErrorTextTimeStart = true;
                                              Get.find<CreateExamController>().update();
                                            }else{
                                              Get.find<CreateExamController>().isShowErrorTextTimeStart = false;
                                              Get.find<CreateExamController>().update();
                                              if(Get.find<CreateExamController>().controllerDateEnd.text.trim() == ""){
                                                Get.find<CreateExamController>().isShowErrorTextDateEnd = true;
                                                Get.find<CreateExamController>().update();
                                              }else{
                                                Get.find<CreateExamController>().isShowErrorTextDateEnd = false;
                                                Get.find<CreateExamController>().update();
                                                if (Get.find<CreateExamController>().controllerTimeEnd.text.trim() == "") {
                                                  Get.find<CreateExamController>().isShowErrorTextTimeEnd = true;
                                                  Get.find<CreateExamController>().update();
                                                }else{
                                                  Get.find<CreateExamController>().isShowErrorTextTimeEnd = false;
                                                  Get.find<CreateExamController>().update();
                                                  if (controller.controllerQuestion[index].text == "") {
                                                    controller.listQuestion[index].errorContentQuestion = true;
                                                    controller.update();
                                                  } else {
                                                    controller.listQuestion[index].errorContentQuestion = false;
                                                    controller.update();
                                                    if (controller.controllerScoreQuestion[index].text.trim() == "") {
                                                      controller.listQuestion[index].errorPointQuestion = true;
                                                      controller.listErrorTextPointQuestion[index] = "Vui lòng nhập điểm cho câu hỏi";
                                                      controller.update();
                                                    } else {
                                                      controller.listQuestion[index].errorPointQuestion = false;
                                                      controller.update();
                                                      if(controller.controllerScoreQuestion[index].text.trim().substring(controller.controllerScoreQuestion[index].text.trim().length - 1) == "."){
                                                        controller.listQuestion[index].errorPointQuestion = true;
                                                        controller.listErrorTextPointQuestion[index] = "Vui lòng nhập đúng định dạng điểm";
                                                        controller.update();
                                                      }else{
                                                        controller.listQuestion[index].point = double.tryParse(controller.controllerScoreQuestion[index].text.trim());
                                                        controller.listQuestion.refresh();
                                                      }
                                                      if (controller.listQuestion[index].point == null) {
                                                        controller.listQuestion[index].errorPointQuestion = true;
                                                        controller.listErrorTextPointQuestion[index] = "Vui lòng nhập đúng định dạng điểm";
                                                        controller.update();
                                                      }
                                                      else {
                                                        if(DateFormat("dd/MM/yyyy HH:mm").
                                                        parse("${Get.find<CreateExamController>().controllerDateStart.text.trim()} "
                                                            "${Get.find<CreateExamController>().controllerTimeStart.text.trim()}")
                                                            .millisecondsSinceEpoch >
                                                            DateFormat("dd/MM/yyyy HH:mm").
                                                            parse("${Get.find<CreateExamController>().controllerDateEnd.text.trim()} "
                                                                "${Get.find<CreateExamController>().controllerTimeEnd.text.trim()}")
                                                                .millisecondsSinceEpoch){
                                                          AppUtils.shared.showToast("Vui lòng chọn thời gian bắt đầu nhỏ hơn thời gian kết thúc");
                                                        }else{
                                                          var listAnswer = <String>[];
                                                          var listContentQuestion = <String>[];
                                                          for(int a = 0;a <controller.listQuestion.length;a++){
                                                            for (int i = 0; i < controller.listQuestion[a].answerOption!.length; i++) {
                                                              if (controller.listQuestion[a].answerOption?[i].textEditingController?.text == "") {
                                                                controller.listQuestion[a].answerOption?[i].validateAnswer = true;
                                                                controller.listQuestion[a].answerOption?[i].errorTextAnswer = "Vui lòng nhập câu trả lời";
                                                                listAnswer.add("");
                                                                controller.update();
                                                              } else {
                                                                controller.listQuestion[a].answerOption?[i].validateAnswer = false;
                                                                listAnswer.add(controller.listQuestion[a].answerOption![i].textEditingController!.text);
                                                                controller.update();
                                                              }
                                                            }
                                                          }


                                                          for(int i = 0;i<controller.listQuestion.length;i++){
                                                            if (controller.controllerQuestion[i].text == ""){
                                                              controller.listQuestion[i].errorContentQuestion = true;
                                                              controller.update();
                                                              listContentQuestion.add("");
                                                            }else{
                                                              controller.listQuestion[i].errorContentQuestion = false;
                                                              controller.update();
                                                              listContentQuestion.add(controller.controllerQuestion[i].text);
                                                            }
                                                          }

                                                          var listScore = <String>[];
                                                          for(int i = 0;i<controller.listQuestion.length;i++){
                                                            if(controller.controllerScoreQuestion[i].text.trim() == ""){
                                                              controller.listQuestion[i].errorPointQuestion = true;
                                                              controller.listErrorTextPointQuestion[i] = "Vui lòng nhập điểm cho câu hỏi";
                                                              controller.update();
                                                              listScore.add("");
                                                            }else{
                                                              controller.listQuestion[i].errorPointQuestion = false;
                                                              controller.update();
                                                              if(controller.controllerScoreQuestion[i].text.trim().substring(controller.controllerScoreQuestion[i].text.trim().length - 1) == "."){
                                                                controller.listQuestion[i].errorPointQuestion = true;
                                                                controller.listErrorTextPointQuestion[i] = "Vui lòng nhập đúng định dạng điểm";
                                                                controller.update();
                                                              }else{
                                                                controller.listQuestion[i].point = double.tryParse(controller.controllerScoreQuestion[i].text.trim());
                                                                controller.listQuestion.refresh();
                                                                listScore.add(double.tryParse(controller.controllerScoreQuestion[i].text.trim()).toString());
                                                              }
                                                              if (controller.listQuestion[i].point == null) {
                                                                controller.listQuestion[i].errorPointQuestion = true;
                                                                controller.listErrorTextPointQuestion[i] = "Vui lòng nhập đúng định dạng điểm";
                                                                controller.update();
                                                                listScore.add("");
                                                              }
                                                            }
                                                          }
                                                          if(listScore.contains("") == false){
                                                            if(listContentQuestion.contains("") == false){
                                                              if(listAnswer.contains("") == false){
                                                                controller.listStatusAnswer.value = [];
                                                                for(int i = 0;i<controller.listQuestion.length;i++){
                                                                  var list = <String>[];
                                                                  for (int j = 0; j < controller.listQuestion[i].answerOption!.length; j++) {
                                                                    list.add(controller.listQuestion[i].answerOption![j].status!);
                                                                  }
                                                                  controller.listStatusAnswer.add(list);
                                                                  controller.listStatusAnswer.refresh();
                                                                }


                                                                var listCheckCorrect = <String>[];
                                                                for(int i = 0;i<controller.listQuestion.length;i++){
                                                                  if( controller.listQuestion[i].typeQuestion =="CONSTRUCTED_RESPONSE"){
                                                                    listCheckCorrect.add("TRUE");
                                                                  }else{
                                                                    if(controller.listStatusAnswer[i].contains("TRUE") == true){
                                                                      listCheckCorrect.add("TRUE");
                                                                    }else{
                                                                      listCheckCorrect.add("");
                                                                    }
                                                                  }
                                                                }

                                                                if(listCheckCorrect.where((item) => item == "TRUE").toList().length ==controller.listQuestion.length ){
                                                                  controller.confirmCreteExam();
                                                                }else{
                                                                  for (int i = 0; i < controller.listQuestion.length; i++) {
                                                                    for (int j = 0; j < controller.listQuestion[i].answerOption!.length; j++) {
                                                                      if(listCheckCorrect[i] == ""){
                                                                        controller.listQuestion[i].answerOption?[j].validateAnswer = true;
                                                                        controller.listQuestion[i].answerOption?[j].errorTextAnswer = "Vui lòng chọn 1 đáp án đúng";
                                                                        controller.update();
                                                                      }
                                                                    }
                                                                  }
                                                                }

                                                              }
                                                            }else{
                                                              for(int i = 0;i<controller.listQuestion.length;i++){
                                                                if (controller.controllerQuestion[i].text == ""){
                                                                  controller.listQuestion[i].errorContentQuestion = true;
                                                                  controller.update();
                                                                  listContentQuestion.add("");
                                                                }else{
                                                                  controller.listQuestion[i].errorContentQuestion = false;
                                                                  controller.update();
                                                                  listContentQuestion.add(controller.controllerQuestion[i].text);
                                                                }
                                                              }
                                                            }
                                                          }else{
                                                            for(int i = 0;i<controller.listQuestion.length;i++){
                                                              if(controller.controllerScoreQuestion[i].text.trim() == ""){
                                                                controller.listQuestion[i].errorPointQuestion = true;
                                                                controller.listErrorTextPointQuestion[i] = "Vui lòng nhập điểm cho câu hỏi";
                                                                controller.update();
                                                                listScore.add("");
                                                              }else{
                                                                controller.listQuestion[i].errorPointQuestion = false;
                                                                controller.update();
                                                                if(controller.controllerScoreQuestion[i].text.trim().substring(controller.controllerScoreQuestion[i].text.trim().length - 1) == "."){
                                                                  controller.listQuestion[i].errorPointQuestion = true;
                                                                  controller.listErrorTextPointQuestion[i] = "Vui lòng nhập đúng định dạng điểm";
                                                                  controller.update();
                                                                }else{
                                                                  controller.listQuestion[i].point = double.tryParse(controller.controllerScoreQuestion[i].text.trim());
                                                                  controller.listQuestion.refresh();
                                                                  listScore.add(double.tryParse(controller.controllerScoreQuestion[i].text.trim()).toString());
                                                                }
                                                                if (controller.listQuestion[i].point == null) {
                                                                  controller.listQuestion[i].errorPointQuestion = true;
                                                                  controller.listErrorTextPointQuestion[i] = "Vui lòng nhập đúng định dạng điểm";
                                                                  controller.update();
                                                                  listScore.add("");
                                                                }
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }

                                          }
                                        }



                                      },
                                      child: const Text(
                                        'Xác nhận',
                                        style: TextStyle(
                                            color: ColorUtils.PRIMARY_COLOR,
                                            fontSize: 16),
                                      )),
                                )),
                            Padding(padding: EdgeInsets.only(right: 16.w)),
                            Expanded(
                                flex: 1,
                                child: SizedBox(
                                  height: 48,
                                  child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        backgroundColor: ColorUtils.PRIMARY_COLOR,
                                      ),
                                      onPressed: () {
                                        var question = Questions().obs;
                                        question.value.files =
                                        [];
                                        question.value.answerOption = [];
                                        question.value.answerOption
                                            ?.add(AnswerOption());
                                        question.value
                                            .errorContentQuestion = false;
                                        question.value.errorPointQuestion =
                                        false;
                                        question.value.answerOption![0] =
                                            AnswerOption(
                                                key: "A",
                                                value: "",
                                                status: "FALSE",
                                                textEditingController:
                                                TextEditingController(),
                                                focusNode: FocusNode(),
                                                validateAnswer: false,
                                                errorTextAnswer: "");
                                        controller.listQuestion
                                            .add(question.value);
                                        controller.listQuestion.refresh();
                                        var file = <ReqFile>[];
                                        controller.groupValue.add(0);
                                        controller.listFile.add(file);
                                        controller.groupValue.refresh();
                                        controller.listFile.refresh();
                                        controller.listErrorTextPointQuestion.add("");
                                        controller.listErrorTextPointQuestion.refresh();
                                        if (controller
                                            .isShowNote[0] ==
                                            true) {
                                          controller.isShowNote
                                              .add(true);
                                          controller.isShowNote.refresh();
                                        } else {
                                          controller.isShowNote
                                              .add(false);
                                          controller.isShowNote.refresh();
                                        }
                                        controller.pageController.animateToPage(
                                            controller.listQuestion.length-1,
                                            duration: const Duration(seconds: 1),
                                            curve: Curves.easeOutBack);
                                        controller.update();

                                      },
                                      child: Text(
                                        'Tạo câu tiếp',
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                255, 255, 255, 1),
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.w400,
                                            fontFamily:
                                            'assets/font/static/Inter-Regular.ttf'),
                                      )),
                                ))
                          ],
                        ),
                      )
                    ],
                  ),
                ));
          },
        ));
  }

  Widget getFileWidgets(List<ReqFile> listFile, index) {
    return Card(
      child: ListView.builder(
          padding: EdgeInsets.zero,
          itemCount: listFile.length,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, i) {
            return FileWidget.itemFile(Get.context!,
                remove: true, file: listFile[i], function: () {
              controller.listFile[index].removeAt(i);
              controller.listFile.refresh();
              controller.listQuestion[index].files?.removeAt(i);
              controller.listQuestion.refresh();
            });
          }),
    );
  }
}
