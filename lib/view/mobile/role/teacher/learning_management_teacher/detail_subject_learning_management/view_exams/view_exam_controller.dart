import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/routes/app_pages.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../../data/model/res/exercise/list_student_exercise.dart';
import '../../../../../../../data/repository/exams/exam_repo.dart';
import '../../../../../home/home_controller.dart';
import '../../../teacher_home_controller.dart';
import '../learning_management_teacher_controller.dart';

class ViewExamController extends GetxController{
  var controllerDateStart = TextEditingController().obs;
  var controllerDateEnd = TextEditingController().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy HH:mm');
  final ExamRepo examRepo = ExamRepo();
  var listExam = ExamStudent().obs;
  RxList<ItemsExam> itemsExam = <ItemsExam>[].obs;
  var statusExercise = "".obs;
  var isPublic = "".obs;
  var listIdStudent = <String>[].obs;
  var studentSubmitted = <StudentSubmitted>[].obs;
  var listStudentExercise = ListStudentSubmitted().obs;
  var studentUnSubmitted = <StudentUnSubmitted>[].obs;
  var listItemPopupMenu = <PopupMenuItem>[];

  var showAnnounced = false;
  var showUpdateExam = false;
  var showDeleteExam = false;
  var showMarkExam = false;
  var showAnnouncedPointExam = false;

  var indexAnnounced = 0;
  var indexUpdateExam = 0;
  var indexDeleteExam = 0;
  var indexMarkExam = 0;
  var indexAnnouncedPointExam = 0;

  var heightAnnounced = 30.0;
  var heightUpdateExam = 30.0;
  var heightDeleteExam = 30.0;
  var heightMarkExam = 30.0;
  var heightAnnouncedPointExam = 30.0;
  RxBool isReady = false.obs;


  @override
  void onInit() {
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      controllerDateStart.value.text =   outputDateFormat.format(DateTime(Get.find<TeacherHomeController>().fromYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<TeacherHomeController>().fromYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
    }else{
      controllerDateStart.value.text =   outputDateFormat.format(DateTime(Get.find<TeacherHomeController>().toYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<TeacherHomeController>().toYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
    }
    getListExam(statusExercise.value,isPublic.value);
    super.onInit();
  }


  clickItemListViewExam(index){
    if(itemsExam[index].files!.isNotEmpty){
      Get.toNamed(Routes.detailExamUploadFilePage,arguments: itemsExam[index]);
    }else{
      if(itemsExam[index].questions!.isNotEmpty){
        Get.toNamed(Routes.viewExamQuestionPage,arguments: itemsExam[index].id);
      }else{
        if(itemsExam[index].link != ""&&itemsExam[index].link != null){
          Get.toNamed(Routes.detailExamAssignLinkPage,arguments: itemsExam[index]);
        }
      }
    }
  }

  getListItemPopupMenu(index){
    listItemPopupMenu = [];
    var announced =  PopupMenuItem<int>(
        value: 0,
        padding: EdgeInsets.zero,
        height: heightAnnounced,
        child: showAnnounced
            ?Container(
          padding: EdgeInsets.symmetric(vertical: 4.h),
          decoration:  const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black26))),
          child: Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Icon(
                Icons
                    .upload,
                color: Colors
                    .black,
              ),
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Text(
                  "Công bố")
            ],
          ),
        )
            :Row(
          children: [
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Icon(
              Icons
                  .upload,
              color: Colors
                  .black,
            ),
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Text(
                "Công bố")
          ],
        )
    );
    var updateExam =  PopupMenuItem<int>(
        value: 1,
        padding: EdgeInsets.zero,
        height: heightUpdateExam,
        child: showUpdateExam
            ?Container(
          padding: EdgeInsets.symmetric(vertical: 4.h),
          decoration:  const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black26))),
          child: Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Icon(
                Icons
                    .mode_edit_outline,
                color: Colors
                    .black,
              ),
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Text(
                  "Sửa bài kiểm tra")
            ],
          ),
        )
            :Row(
          children: [
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Icon(
              Icons
                  .mode_edit_outline,
              color: Colors
                  .black,
            ),
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Text(
                "Sửa bài kiểm tra")
          ],
        )
    );
    var deleteExam =  PopupMenuItem<int>(
        value: 2,
        padding: EdgeInsets.zero,
        height: heightDeleteExam,
        child: Row(
          children: [
            Padding(
                padding: EdgeInsets
                    .only(
                    right:
                    8.w)),
            const Icon(
                Icons.delete),
            Padding(
                padding: EdgeInsets
                    .only(
                    right:
                    8.w)),
            const Text(
              "Xóa bài kiểm tra",
            )
          ],
        )
    );
    var markExam =  PopupMenuItem<int>(
        value: 3,
        padding: EdgeInsets.zero,
        height: heightMarkExam,
        child: showMarkExam
            ?Container(
          padding: EdgeInsets.symmetric(vertical: 4.h),
          decoration:  const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black26))),
          child: Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Icon(
                Icons
                    .check_circle_outline,
                color: Colors
                    .black,
              ),
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Text(
                  "Chấm điểm"),

            ],
          ),
        )
            :Row(
          children: [
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Icon(
              Icons
                  .check_circle_outline,
              color: Colors
                  .black,
            ),
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Text(
                "Chấm điểm"),

          ],
        )
    );
    var announcedPointExam =  PopupMenuItem<int>(
        value: 4,
        padding: EdgeInsets.zero,
        height: heightAnnouncedPointExam,
        child: showAnnouncedPointExam
            ?Container(
          padding: EdgeInsets.symmetric(vertical: 4.h),
          decoration:  const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black26))),
          child: Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Icon(
                Icons
                    .upload,
                color: Colors
                    .black,
              ),
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Text(
                  "Công bố điểm")
            ],
          ),
        )
            :Row(
          children: [
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Icon(
              Icons
                  .upload,
              color: Colors
                  .black,
            ),
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Text(
                "Công bố điểm")
          ],
        )
    );
    var viewResultExam =   PopupMenuItem<int>(
      value: 5,
      padding: EdgeInsets.zero,
      height: 30,
      child: Row(
        children: [
          Padding(
              padding: EdgeInsets.only(
                  right:
                  8.w)),
          const Icon(
            Icons.remove_red_eye,
            color: Colors
                .black,
          ),
          Padding(
              padding: EdgeInsets.only(
                  right:
                  8.w)),
          const Text(
              "Xem kết quả bài kiểm tra")
        ],
      ),
    );

    if(itemsExam[index].createdBy == AppCache().userId && itemsExam[index].isPublic == "FALSE"){
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXAM_PUBLIC)){
        listItemPopupMenu.add(announced);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXAM_EDIT)){
        listItemPopupMenu.add(updateExam);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXAM_DELETE)){
        listItemPopupMenu.add(deleteExam);
      }
    }
    if(itemsExam[index].isPublicScore == "FALSE" && itemsExam[index].createdBy == AppCache().userId && itemsExam[index].isPublic ==
        "TRUE"){
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXAM_MARK)){
        listItemPopupMenu.add(markExam);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXAM_PUBLIC_SCORE)){
        listItemPopupMenu.add(announcedPointExam);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_RESULT_EXAM_VIEW)){
        listItemPopupMenu.add(viewResultExam);
      }
    }
    if(itemsExam[index].isPublicScore == "TRUE" && itemsExam[index].createdBy == AppCache().userId && itemsExam[index].isPublic == "TRUE"){
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXAM_MARK)){
        listItemPopupMenu.add(markExam);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_RESULT_EXAM_VIEW)){
        listItemPopupMenu.add(viewResultExam);
      }
    }

    if(itemsExam[index].createdBy != AppCache().userId && itemsExam[index].isPublic ==
        "TRUE"){
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_RESULT_EXAM_VIEW)){
        listItemPopupMenu.add(viewResultExam);
      }
    }

    indexAnnounced = listItemPopupMenu.indexOf(announced);
    indexUpdateExam = listItemPopupMenu.indexOf(updateExam);
    indexDeleteExam = listItemPopupMenu.indexOf(deleteExam);
    indexMarkExam = listItemPopupMenu.indexOf(markExam);
    indexAnnouncedPointExam = listItemPopupMenu.indexOf(announcedPointExam);

    showAnnounced = setVisiblePopupMenuItem(indexAnnounced,listItemPopupMenu);
    showUpdateExam = setVisiblePopupMenuItem(indexUpdateExam,listItemPopupMenu);
    showDeleteExam = setVisiblePopupMenuItem(indexDeleteExam,listItemPopupMenu);
    showMarkExam = setVisiblePopupMenuItem(indexMarkExam,listItemPopupMenu);
    showAnnouncedPointExam = setVisiblePopupMenuItem(indexAnnouncedPointExam,listItemPopupMenu);

    heightAnnounced = setHeightPopupMenuItem(indexAnnounced,listItemPopupMenu);
    heightUpdateExam = setHeightPopupMenuItem(indexUpdateExam,listItemPopupMenu);
    heightDeleteExam = setHeightPopupMenuItem(indexDeleteExam,listItemPopupMenu);
    heightMarkExam = setHeightPopupMenuItem(indexMarkExam,listItemPopupMenu);
    heightAnnouncedPointExam = setHeightPopupMenuItem(indexAnnouncedPointExam,listItemPopupMenu);

    update();


    return listItemPopupMenu;
  }


  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }


  getListExam(status,isPublic){
    var idSubject = Get.find<LearningManagementTeacherController>().detailSubject.value.id;
    var fromDate = DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2),),00,00).millisecondsSinceEpoch;
    var toDate = DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2)),23,59).millisecondsSinceEpoch;
    examRepo.listExamTeacher(idSubject,status,fromDate, toDate,isPublic).then((value) {
      if (value.state == Status.SUCCESS) {
        listExam.value= value.object!;
        if(listExam.value.items != null){
          itemsExam.value = listExam.value.items!;
        }
        AppUtils.shared.hideLoading();
      }

    });
    isReady.value = true;
  }


  publicExam(examId) {
    examRepo.publicExam(examId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Công bố bài kiểm tra thành công");
        getListExam(setTypeExercise(statusExercise.value),setStatusPublic(isPublic.value));
        Get.back();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Công bố bài kiểm tra thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


  deleteExam(examId) {
    examRepo.deleteExam(examId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Xóa bài kiểm tra thành công");
        getListExam(setTypeExercise(statusExercise.value),setStatusPublic(isPublic.value));
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Xóa bài kiểm tra thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


  getListStudentSubmit(exerciseId){
    examRepo.studentsSubmitted(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudentExercise.value = value.object!;
        studentSubmitted.value = listStudentExercise.value.studentSubmitted!;
        studentUnSubmitted.value = listStudentExercise.value.studentUnSubmitted!;
        for (int i = 0; i < studentSubmitted.length; i++) {
          listIdStudent.add(studentSubmitted[i].student!.id!);
        }
      }
    });
    studentSubmitted.refresh();
    studentUnSubmitted.refresh();
  }


  teacherPublicScore(idExercise) {
    examRepo.teacherPublicScore(idExercise,listIdStudent).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Công bố điểm bài kiểm tra thành công");
        Get.back();
        onInit();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Công bố điểm bài kiểm tra thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);

      }
    });
  }




  setTypeExercise(type){
    switch(type){
      case "Đến hạn":
        return "DUE";
      case "Đã hết hạn":
        return "EXPIRED";
      case "Sắp đến hạn":
        return "DEADLINE_COMING_SOON";
      default:
        return "";
    }
  }

  getStatusExercise(type){
    switch(type){
      case "DUE":
        return "Đến hạn";
      case "EXPIRED":
        return "Đã hết hạn";
      case "DEADLINE_COMING_SOON":
        return "Sắp đến hạn";
      default:
        return "";
    }
  }


  getTypeExercise(type){
    switch(type){
      case "ALL":
        return "Trắc nghiệm & Tự luận";
      case "SELECTED_RESPONSE":
        return "Trắc nghiệm";
      case "CONSTRUCTED_RESPONSE":
        return "Tự luận";
      default:
        return "";
    }
  }


  getStatusPublic(type){
    switch(type){
      case "TRUE":
        return "Công bố";
      case "FALSE":
        return "Chưa công bố";
      default:
        return "";
    }
  }



  setStatusPublic(type){
    switch(type){
      case "Công bố":
        return "TRUE";
      case "Chưa công bố":
        return "FALSE";
      default:
        return "";
    }
  }

  goToGradingExam(ItemsExam itemsExam){
    if (itemsExam.files!.isNotEmpty) {
      Get.toNamed(Routes.gradingExamUploadFilePage, arguments: itemsExam);
    } else {
      if (itemsExam.questions!.isNotEmpty) {
        Get.toNamed(Routes.gradingExamQuestionPage, arguments:itemsExam);
      } else {
        if (itemsExam.link != "" && itemsExam.link != null) {
          Get.toNamed(Routes.gradingExamAssignLinkPage, arguments: itemsExam);
        }
      }
    }
  }








}