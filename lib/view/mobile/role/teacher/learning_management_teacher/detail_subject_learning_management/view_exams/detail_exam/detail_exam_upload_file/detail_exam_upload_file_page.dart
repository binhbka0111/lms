import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import '../../../../../../../../../commom/utils/ViewPdf.dart';
import '../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../../../../commom/utils/global.dart';
import '../../../../../../../../../commom/utils/open_url.dart';
import '../../../learning_management_teacher_controller.dart';
import 'detail_exam_upload_file_controller.dart';

class DetailExamUploadFilePage extends GetView<DetailExamUploadFileController>{
  @override
  final controller = Get.put(DetailExamUploadFileController());

  DetailExamUploadFilePage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                comeToHome();
              },
              icon: const Icon(Icons.home),
              color: Colors.white,
            )
          ],
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title: Text(
            'Bài Kiểm Tra Môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${controller.detailExam.value.title}",
                  style: TextStyle(
                      color: const Color.fromRGBO(133, 133, 133, 1),
                      fontSize: 12.sp,
                      fontWeight: FontWeight.w500),
                ),
                Padding(padding: EdgeInsets.only(top: 8.h)),
                Row(
                  children: [
                    SizedBox(
                      width: 100.w,
                      child: Text(
                        "Ngày tạo",
                        style: TextStyle(
                            color: const Color.fromRGBO(133, 133, 133, 1),
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Text(
                      controller.outputDateFormat.format(
                          DateTime.fromMillisecondsSinceEpoch(controller
                              .detailExam.value.createdAt!)),
                      style: TextStyle(
                          color: const Color.fromRGBO(26, 26, 26, 1),
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 8.h)),
                Row(
                  children: [
                    SizedBox(
                      width: 100.w,
                      child: Text("Thời gian bắt đầu",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                    ),
                    Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExam.value.startTime!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 8.h)),
                Row(
                  children: [
                    SizedBox(
                      width: 100.w,
                      child: Text("Thời gian kết thúc",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                    ),
                    Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExam.value.endTime!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 8.h)),
                Row(
                  children: [
                    SizedBox(
                      width: 100.w,
                      child: Text(
                        "Ghi chú",
                        style: TextStyle(
                            color: const Color.fromRGBO(133, 133, 133, 1),
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(
                      width: 230.w,
                      child: Text(
                        "${controller.detailExam.value.description}",
                        style: TextStyle(
                            color: const Color.fromRGBO(26, 26, 26, 1),
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 8.h)),
                Row(
                  children: [
                    SizedBox(
                      width: 100.w,
                      child: Text("Điểm tối đa",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                    ),
                    Text("${controller.detailExam.value.scoreOfExam}",style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                  ],
                ),
                const Padding(padding: EdgeInsets.only(top: 16)),
                const Text('File Bài Tập',
                    style: TextStyle(
                        color: Color.fromRGBO(26, 26, 26, 1),
                        fontFamily: 'assets/font/static/Inter-Medium.ttf',
                        fontSize: 16,
                        fontWeight: FontWeight.w500)),
                const Padding(padding: EdgeInsets.only(top: 4)),
                DottedBorder(
                    dashPattern: const [5, 5],
                    radius: const Radius.circular(8),
                    borderType: BorderType.RRect,
                    color: const Color.fromRGBO(133, 133, 133, 1),
                    child: Container(
                      margin: const EdgeInsets.only(right: 12, left: 12),
                      child: Column(
                        children: [
                          ListView.builder(
                              shrinkWrap: true,
                              itemCount: controller.filesUploadExam.length,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {
                                    var action = 0;
                                    var extension = controller.filesUploadExam[index].ext;
                                    if (extension == "png" ||
                                        extension == "jpg" ||
                                        extension == "jpeg" ||
                                        extension == "gif" ||
                                        extension == "bmp") {
                                      action = 1;
                                    } else if (extension == "pdf") {
                                      action = 2;
                                    } else {
                                      action = 0;
                                    }
                                    switch (action) {
                                      case 1:
                                        OpenUrl.openImageViewer(context, controller.filesUploadExam[index].link!);
                                        break;
                                      case 2:
                                        Get.to(ViewPdfPage(url:controller.filesUploadExam[index].link!));
                                        break;
                                      default:
                                        OpenUrl.openFile(controller.filesUploadExam[index].link!);
                                        break;
                                    }
                                  },
                                  child: Container(
                                    margin: const EdgeInsets.only(top: 12),
                                    padding: const EdgeInsets.all(12),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(6),
                                        color: const Color.fromRGBO(246, 246, 246, 1)),
                                    child: Row(
                                      children: [
                                        Image.network(
                                          controller.filesUploadExam[index].link!,
                                          errorBuilder: (
                                              BuildContext context,
                                              Object error,
                                              StackTrace? stackTrace,
                                              ) {
                                            return Image.asset(
                                              getFileIcon(controller.filesUploadExam[index].name),
                                              height: 35,
                                              width: 30,
                                            );
                                          },
                                          height: 35,
                                          width: 30,
                                        ),
                                        const Padding(padding: EdgeInsets.only(left: 8)),
                                        SizedBox(
                                          width: 220.w,
                                          child: Text(
                                            '${controller.filesUploadExam[index].name}',
                                            style: TextStyle(
                                                color: const Color.fromRGBO(26, 59, 112, 1),
                                                fontSize: 14.sp,
                                                fontFamily: 'assets/font/static/Inter-Medium.ttf',
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Expanded(child: Container()),
                                        Image.asset('assets/images/icon_upfile_subject.png',height: 16,width: 16,),
                                      ],
                                    ),
                                  ),
                                );
                              }),
                          Padding(padding: EdgeInsets.only(bottom: 8.h)),
                        ],
                      ),
                    )),
              ],
            ),
          ),
        )) ;
  }
}