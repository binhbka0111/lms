import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:html/parser.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/commom/widget/list_view_image.dart';
import '../../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../../commom/widget/text_field_custom.dart';
import '../../../../learning_management_teacher_controller.dart';
import '../../grading_exam_upload_file/grading_exam_upload_file_controller.dart';
import 'detail_student_do_exam_upload_file_controller.dart';


class DetailStudentDoExamUploadFilePage
    extends GetView<DetailStudentDoExamUploadFileController> {
  final controller = Get.put(DetailStudentDoExamUploadFileController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {
                  comeToHome();
                },
                icon: const Icon(Icons.home),
                color: Colors.white,
              )
            ],
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            title: Text(
              'Bài Tập Môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}',
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body: Obx(() => controller.isReady.value
              ? Column(
            children: [
              Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.all(16.h),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${controller.studentDoExam.value.title}",
                            style: TextStyle(
                                color: const Color.fromRGBO(133, 133, 133, 1),
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w500),
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Ngày tạo",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                controller.outputDateFormat.format(
                                    DateTime.fromMillisecondsSinceEpoch(controller
                                        .studentDoExam.value.createdAt ??
                                        0)),
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Thời gian bắt đầu",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                              Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.studentDoExam.value.startTime!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Thời gian kết thúc",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                              Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.studentDoExam.value.endTime!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Ghi chú",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                "${controller.studentDoExam.value.description}",
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            children: [
                              SizedBox(
                                width: 48.h,
                                height: 48.h,
                                child:
                                CacheNetWorkCustom(urlImage: '${ controller.studentDoExam.value.student?.image}'),

                              ),
                              Padding(padding: EdgeInsets.only(right: 8.w)),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("${controller.studentDoExam.value.student?.fullName}",style: TextStyle(color: Colors.black,fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                  Visibility(
                                      visible:controller.studentDoExam.value.student!
                                          .birthday != null,
                                      child:  Text(
                                        controller.outputDateFormatV2.format(
                                            DateTime.fromMillisecondsSinceEpoch(
                                                controller.studentDoExam.value.student
                                                    ?.birthday ??
                                                    0)),
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                133, 133, 133, 1),
                                            fontSize: 12.sp),
                                      ))
                                ],
                              )

                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Obx(() => ListViewShowImage.showGridviewImage(controller.listImage)),
                          Obx(() => ListViewShowImage.showListViewNotImage(controller.listNotImage)),


                          const Text('Bài làm',
                              style: TextStyle(
                                  color: Color.fromRGBO(26, 26, 26, 1),
                                  fontFamily:
                                  'assets/font/static/Inter-Medium.ttf',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500)),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Text(
                            "${parse(parse(controller.studentDoExam.value.contentAnswer ?? "").body?.text).documentElement?.text}",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400,
                                fontSize: 14.sp),
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Obx(() => ListViewShowImage.showGridviewImage(controller.listImageDoExam)),
                          Obx(() => ListViewShowImage.showListViewNotImage(controller.listNotImageDoExam)),
                          Padding(padding: EdgeInsets.only(top: 16.h)),
                          GetBuilder<DetailStudentDoExamUploadFileController>(
                            builder: (controller) {
                              return MyTextFormFieldBinh(
                                enable: true,
                                focusNode: controller.focusScoreQuestion,
                                iconPrefix: null,
                                iconSuffix: null,
                                state: StateType.DEFAULT,
                                labelText: "Nhập Điểm Cho Câu Hỏi",
                                autofocus: false,
                                controller:
                                controller.controllerScoreQuestion,
                                helperText:
                                controller.helperTextScoreQuestion,
                                showHelperText: controller.isShowHelperText,
                                textInputAction: TextInputAction.next,
                                ishowIconPrefix: false,
                                keyboardType: TextInputType.number,
                              );
                            },
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal:16.w),
                            child: MyOutlineBorderTextFormFieldBinh(
                              enable: true,
                              focusNode: controller.focusComment,
                              iconPrefix: null,
                              iconSuffix: null,
                              state: StateType.DEFAULT,
                              labelText: "Nhận xét",
                              autofocus: false,
                              controller: controller.controllerComment,
                              helperText: "vui lòng nhập câu hỏi",
                              showHelperText: false,
                              textInputAction: TextInputAction.next,
                              ishowIconPrefix: false,
                              keyboardType: TextInputType.text,
                            ),
                          )
                        ],
                      ),
                    ),
                  )),
              Container(
                color: Colors.white,
                width: double.infinity,
                padding:
                EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                child: SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                      onPressed: () {
                        if (controller.controllerScoreQuestion.text.trim() ==
                            "") {
                          controller.isShowHelperText = true;
                          controller.helperTextScoreQuestion =
                          "Vui lòng nhập điểm của câu hỏi";
                          controller.update();
                        } else {
                          if (controller.controllerScoreQuestion.text.trim().substring(controller.controllerScoreQuestion.text.trim().length - 1) == ".") {
                            controller.isShowHelperText = true;
                            controller.helperTextScoreQuestion =
                            "Vui lòng nhập đúng định dạng điểm";
                            controller.update();
                          }else if(double.tryParse(controller.controllerScoreQuestion.text.trim())== null){
                            controller.isShowHelperText = true;
                            controller.helperTextScoreQuestion  = "Vui lòng nhập đúng định dạng điểm";
                            controller.update();
                          } else {
                            controller.isShowHelperText = false;
                            controller.update();
                            controller.teacherGrading(Get.find<GradingExamUploadFileController>().detailExam.value.id,controller.studentID.value, controller.controllerComment.text, double.tryParse(controller.controllerScoreQuestion.text.trim()));
                          }
                        }
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: ColorUtils.PRIMARY_COLOR),
                      child: Text(
                        "Xác nhận",
                        style:
                        TextStyle(color: Colors.white, fontSize: 14.sp),
                      )),
                ),
              )
            ],
          )
              : Container()),
        );
  }

  Future<Null> updated(StateSetter updateState) async {
    updateState(() {
      if (controller.controllerScoreQuestionAll.text.trim() == "") {
        controller.isShowHelperTextAll = true;
        controller.helperTextScoreQuestionAll = "Vui lòng nhập điểm của câu hỏi";
      } else {
        if (controller.controllerScoreQuestionAll.text.trim().substring(controller.controllerScoreQuestionAll.text.trim().length - 1) == ".") {
          controller.isShowHelperTextAll =
          true;
          controller.helperTextScoreQuestionAll = "Vui lòng nhập đúng định dạng điểm";
        } else {
          controller.isShowHelperTextAll = false;

        }
      }

    });
  }
}
