import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/res/exams/detail_exam.dart';
import '../../../../../../../../../data/model/res/exercise/exercise.dart';
import '../../../../../../../../../data/repository/exams/exam_repo.dart';


class ViewExamQuestionController extends GetxController{
  final ExamRepo examRepo = ExamRepo();
  var idExam = "".obs;
  var detailExam = DetailExam().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var isReady = false.obs;
  var listQuestion = <Questions>[].obs;
  var detailExamCopy = DetailExam().obs;
  var listIdQuestion = <String>[];
  @override
  void onInit() {
    super.onInit();
    var tmpItemExam = Get.arguments;
    if(tmpItemExam!=null){
      idExam.value = tmpItemExam;
      getDetailExam(idExam.value);
    }
  }



  getDetailExam(examId) {
    examRepo.detailExam(examId).then((value) {
      if (value.state == Status.SUCCESS) {
        detailExam.value = value.object!;
        detailExamCopy.value = value.object!;
        for(int i = 0 ; i<detailExamCopy.value.questions!.length;i++){
          listQuestion.add(detailExamCopy.value.questions![i]);
          listIdQuestion.add(detailExamCopy.value.questions![i].id!);
        }
        isReady.value = true;
        AppUtils.shared.showToast("Lấy chi tiết bài kiểm tra thành công");
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lấy chi tiết bài kiểm tra thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        isReady.value = true;
      }
    });
  }

  getIndexQuestion(id){
    return (listIdQuestion.indexOf(id)+1);
  }

}