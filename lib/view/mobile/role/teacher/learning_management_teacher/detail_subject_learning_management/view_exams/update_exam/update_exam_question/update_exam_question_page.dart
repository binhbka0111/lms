import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/update_exam/update_exam_question/update_exam_question_controller.dart';
import 'package:tiengviet/tiengviet.dart';
import '../../../../../../../../../commom/app_cache.dart';
import '../../../../../../../../../commom/constants/date_format.dart';
import '../../../../../../../../../commom/utils/ViewPdf.dart';
import '../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../commom/utils/date_time_picker.dart';
import '../../../../../../../../../commom/utils/file_device.dart';
import '../../../../../../../../../commom/utils/global.dart';
import '../../../../../../../../../commom/utils/open_url.dart';
import '../../../../../../../../../commom/utils/time_utils.dart';
import '../../../../../../../../../commom/widget/list_view_image.dart';
import '../../../../../../../../../commom/widget/text_field_custom.dart';
import '../../../../../../../../../data/model/common/req_file.dart';
import '../../../../../../../../../data/model/res/exercise/exercise.dart';
import '../../../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../teacher_home_controller.dart';
import '../../../learning_management_teacher_controller.dart';

class UpdateExamQuestionPage extends GetView<UpdateExamQuestionController> {
  @override
  final controller = Get.put(UpdateExamQuestionController());

  UpdateExamQuestionPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
          },
          child: Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: AppBar(
                backgroundColor: ColorUtils.PRIMARY_COLOR,
                elevation: 0,
                title: Text(
                  "Cập nhật bài kiểm tra môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w500),
                ),
                actions: [
                  InkWell(
                    onTap: () {
                      comeToHome();
                    },
                    child: const Icon(
                      Icons.home,
                      color: Colors.white,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(right: 16.w))
                ],
              ),
              body: Column(
                children: [
                  Expanded(child: SingleChildScrollView(
                    controller: controller.scrollController,
                    physics: const ScrollPhysics(),
                    child: Obx(() => controller.isReady.value
                        ? Container(
                      margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GetBuilder<UpdateExamQuestionController>(builder: (controller) {
                            return MyOutlineBorderTextFormFieldNhat(
                              enable: true,
                              focusNode: controller.focusNameExercise,
                              iconPrefix: null,
                              iconSuffix: null,
                              state: StateType.DEFAULT,
                              labelText: "Tiêu đề (*)",
                              autofocus: false,
                              controller: controller.controllerNameExam,
                              helperText: "vui lòng nhập tiêu đề",
                              showHelperText: controller.isShowErrorTextNameExam,
                              textInputAction: TextInputAction.next,
                              ishowIconPrefix: false,
                              keyboardType: TextInputType.text,
                            );
                          },),

                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          MyOutlineBorderTextFormFieldNhat(
                            enable: true,
                            focusNode: controller.focusDescribe,
                            iconPrefix: null,
                            iconSuffix: null,
                            state: StateType.DEFAULT,
                            labelText: "Mô Tả",
                            autofocus: false,
                            controller: controller.controllerDescribe,
                            helperText: "",
                            showHelperText: false,
                            textInputAction: TextInputAction.next,
                            ishowIconPrefix: false,
                            keyboardType: TextInputType.text,
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          GetBuilder<UpdateExamQuestionController>(builder: (controller) {
                            return Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child:Container(
                                          margin: EdgeInsets.only(right: 4.w),
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.only(left: 8.w),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: const BorderRadius.all(
                                                  Radius.circular(6.0)),
                                              border: Border.all(
                                                width: 1,
                                                style: BorderStyle.solid,
                                                color: const Color.fromRGBO(
                                                    192, 192, 192, 1),
                                              )),
                                          child: TextFormField(
                                            keyboardType: TextInputType.multiline,
                                            maxLines: null,
                                            style: TextStyle(
                                              fontSize: 12.0.sp,
                                              color:
                                              const Color.fromRGBO(26, 26, 26, 1),
                                            ),
                                            onTap: () {
                                              selectDateTimeStart(
                                                  controller
                                                      .controllerDateStart.value.text,
                                                  DateTimeFormat.formatDateShort,
                                                  context);
                                            },
                                            readOnly: true,
                                            cursorColor: ColorUtils.PRIMARY_COLOR,
                                            controller: controller.controllerDateStart,
                                            decoration: InputDecoration(
                                              suffixIcon: Container(
                                                margin: EdgeInsets.only(bottom: 4.h),
                                                child: SizedBox(
                                                  height: 14,
                                                  width: 14,
                                                  child: Container(
                                                    margin: EdgeInsets.zero,
                                                    child: SvgPicture.asset(
                                                      "assets/images/icon_date_picker.svg",
                                                      fit: BoxFit.scaleDown,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              label: const Text("Ngày bắt đầu"),
                                              border: InputBorder.none,
                                              labelStyle: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      177, 177, 177, 1),
                                                  fontSize: 12.sp,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily:
                                                  'assets/font/static/Inter-Medium.ttf'),
                                            ),
                                          ),
                                        )),
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 4.w),
                                        alignment: Alignment.centerLeft,
                                        padding: EdgeInsets.only(left: 8.w),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: const BorderRadius.all(
                                                Radius.circular(6.0)),
                                            border: Border.all(
                                              width: 1,
                                              style: BorderStyle.solid,
                                              color: const Color.fromRGBO(
                                                  192, 192, 192, 1),
                                            )),
                                        child: TextFormField(
                                          keyboardType: TextInputType.multiline,
                                          maxLines: null,
                                          style: TextStyle(
                                            fontSize: 12.0.sp,
                                            color:
                                            const Color.fromRGBO(26, 26, 26, 1),
                                          ),
                                          onTap: () {
                                            selectTimeStart(
                                                controller
                                                    .controllerTimeStart.value.text,
                                                DateTimeFormat.formatTime);
                                          },
                                          readOnly: true,
                                          cursorColor: ColorUtils.PRIMARY_COLOR,
                                          controller: controller.controllerTimeStart,
                                          decoration: InputDecoration(
                                            suffixIcon: Container(
                                              margin: EdgeInsets.only(bottom: 4.h),
                                              child: SizedBox(
                                                height: 14,
                                                width: 14,
                                                child: Container(
                                                  margin: EdgeInsets.zero,
                                                  child: SvgPicture.asset(
                                                    "assets/images/icon_date_picker.svg",
                                                    fit: BoxFit.scaleDown,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            label: const Text("Giờ bắt đầu"),
                                            border: InputBorder.none,
                                            labelStyle: TextStyle(
                                                color: const Color.fromRGBO(
                                                    177, 177, 177, 1),
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w500,
                                                fontFamily:
                                                'assets/font/static/Inter-Medium.ttf'),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Visibility(
                                        visible: controller.isShowErrorTextDateStart,
                                        child: Container(
                                            padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                            child: Text(
                                              controller.errorTextDateStart,
                                              style: const TextStyle(color: Colors.red),
                                            )),
                                      ),),
                                    Expanded(
                                        flex: 1,
                                        child: Visibility(
                                          visible: controller.isShowErrorTextTimeStart,
                                          child: Container(
                                              padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                              child: Text(
                                                controller.errorTextTimeStart,
                                                style: const TextStyle(color: Colors.red),
                                              )),
                                        )
                                    )
                                  ],
                                ),
                              ],
                            );
                          },),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          GetBuilder<UpdateExamQuestionController>(builder: (controller) {
                            return Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child: Container(
                                          margin: EdgeInsets.only(right: 4.w),
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.only(left: 8.w),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: const BorderRadius.all(
                                                  Radius.circular(6.0)),
                                              border: Border.all(
                                                width: 1,
                                                style: BorderStyle.solid,
                                                color: const Color.fromRGBO(
                                                    192, 192, 192, 1),
                                              )),
                                          child: TextFormField(
                                            keyboardType: TextInputType.multiline,
                                            maxLines: null,
                                            style: TextStyle(
                                              fontSize: 12.0.sp,
                                              color:
                                              const Color.fromRGBO(26, 26, 26, 1),
                                            ),
                                            onTap: () {
                                              selectDateTimeEnd(
                                                  controller
                                                      .controllerDateEnd.value.text,
                                                  DateTimeFormat.formatDateShort,
                                                  context);
                                            },
                                            readOnly: true,
                                            cursorColor: ColorUtils.PRIMARY_COLOR,
                                            controller: controller.controllerDateEnd,
                                            decoration: InputDecoration(
                                              suffixIcon: Container(
                                                margin: EdgeInsets.only(bottom: 4.h),
                                                child: SizedBox(
                                                  height: 14,
                                                  width: 14,
                                                  child: Container(
                                                    margin: EdgeInsets.zero,
                                                    child: SvgPicture.asset(
                                                      "assets/images/icon_date_picker.svg",
                                                      fit: BoxFit.scaleDown,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              label: const Text("Ngày kết thúc"),
                                              border: InputBorder.none,
                                              labelStyle: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      177, 177, 177, 1),
                                                  fontSize: 12.sp,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily:
                                                  'assets/font/static/Inter-Medium.ttf'),
                                            ),
                                          ),
                                        )),
                                    Expanded(
                                        flex: 1,
                                        child: Container(
                                          margin: EdgeInsets.only(left: 4.w),
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.only(left: 8.w),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: const BorderRadius.all(
                                                  Radius.circular(6.0)),
                                              border: Border.all(
                                                width: 1,
                                                style: BorderStyle.solid,
                                                color: const Color.fromRGBO(
                                                    192, 192, 192, 1),
                                              )),
                                          child: TextFormField(
                                            keyboardType: TextInputType.multiline,
                                            maxLines: null,
                                            style: TextStyle(
                                              fontSize: 12.0.sp,
                                              color:
                                              const Color.fromRGBO(26, 26, 26, 1),
                                            ),
                                            onTap: () {
                                              selectTimeEnd(
                                                  controller
                                                      .controllerTimeEnd.value.text,
                                                  DateTimeFormat.formatTime);
                                            },
                                            readOnly: true,
                                            cursorColor: ColorUtils.PRIMARY_COLOR,
                                            controller: controller.controllerTimeEnd,
                                            decoration: InputDecoration(
                                              suffixIcon: Container(
                                                margin: EdgeInsets.only(bottom: 4.h),
                                                child: SizedBox(
                                                  height: 14,
                                                  width: 14,
                                                  child: Container(
                                                    margin: EdgeInsets.zero,
                                                    child: SvgPicture.asset(
                                                      "assets/images/icon_date_picker.svg",
                                                      fit: BoxFit.scaleDown,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              label: const Text("Giờ kết thúc"),
                                              border: InputBorder.none,
                                              labelStyle: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      177, 177, 177, 1),
                                                  fontSize: 12.sp,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily:
                                                  'assets/font/static/Inter-Medium.ttf'),
                                            ),
                                          ),
                                        )
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child: Visibility(
                                          visible: controller.isShowErrorTextDateEnd,
                                          child: Container(
                                              padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                              child: Text(
                                                controller.errorTextDateEnd,
                                                style: const TextStyle(color: Colors.red),
                                              )),
                                        )),
                                    Expanded(
                                        flex: 1,
                                        child: Visibility(
                                          visible: controller.isShowErrorTextTimeEnd,
                                          child: Container(
                                              padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                              child: Text(
                                                controller.errorTextTimeEnd,
                                                style: const TextStyle(color: Colors.red),
                                              )),
                                        )
                                    )
                                  ],
                                ),
                              ],
                            );
                          },),
                          const Padding(padding: EdgeInsets.only(top: 8)),
                          Container(
                            color: Colors.white,
                            height: 40.h,

                            child: TextFormField(
                              controller: controller.controllerFilter,
                              onChanged: (value) {
                                controller.listQuestion.value = [];
                                controller.listQuestion.value = controller.listQuestionCopy.where((element) => TiengViet.parse(element.content!.toLowerCase()).contains(TiengViet.parse(value.toLowerCase())) == true).toList();
                                controller.setListQuestion();
                                controller.listQuestion.refresh();
                              },
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Color.fromRGBO(177, 177, 177, 1)),
                                      borderRadius: BorderRadius.circular(6)),
                                  suffixIcon: const Icon(
                                    Icons.search,
                                    color: Colors.black,
                                  ),
                                  contentPadding: EdgeInsets.only(left: 8.w),
                                  hintText: "Tìm kiếm",
                                  isCollapsed: true,
                                  hintStyle: TextStyle(
                                      fontSize: 14.sp,
                                      color: const Color.fromRGBO(177, 177, 177, 1)),
                                  focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: ColorUtils.PRIMARY_COLOR))),
                              textAlignVertical: TextAlignVertical.center,
                              cursorColor: ColorUtils.PRIMARY_COLOR,
                            ),
                          ),
                          const Padding(padding: EdgeInsets.only(top: 16)),
                          Obx(() =>  ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: controller.listQuestion.length,
                            itemBuilder: (context, index) {
                              var listImage =  controller.listQuestion[index].files!.where((element) => element.ext == "png" ||
                                  element.ext == "jpg" ||
                                  element.ext == "jpeg" ||
                                  element.ext == "gif" ||
                                  element.ext == "bmp").toList().obs;
                              var listNotImage =  controller.listQuestion[index].files!.where((element) =>
                              element.ext != "png" &&
                                  element.ext != "jpg" &&
                                  element.ext != "jpeg" &&
                                  element.ext != "gif" &&
                                  element.ext != "bmp")
                                  .toList().obs;
                              return controller.listStatusEdit[index]
                                  ?Obx(() => Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      controller.listExpand[index] = !controller.listExpand[index];
                                      controller.listExpand.refresh();
                                      controller.listQuestion.refresh();
                                    },
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        SizedBox(
                                          width: 300.w,
                                          child: RichText(
                                              text: TextSpan(children: [
                                                TextSpan(
                                                    text: 'Câu ${controller.getIndexQuestion(controller.listQuestion[index].id)??0}: ',
                                                    style: TextStyle(
                                                        color: const Color.fromRGBO(
                                                            133, 133, 133, 1),
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 12.sp,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf')),
                                                TextSpan(
                                                    text:
                                                    '[${controller.listQuestion[index].point??0}đ] ',
                                                    style: TextStyle(
                                                        color: ColorUtils.PRIMARY_COLOR,
                                                        fontSize: 12.sp,
                                                        fontWeight: FontWeight.w500,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf')),
                                                TextSpan(
                                                    text:
                                                    '${controller.listQuestion[index].content??""}: ',
                                                    style: TextStyle(
                                                        color: const Color.fromRGBO(
                                                            26, 26, 26, 1),
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 12.sp,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf')),
                                              ])),
                                        ),
                                        Visibility(
                                            visible: controller.listExpand[index] == false,
                                            child:  SizedBox(
                                              height: 16.w,
                                              width: 16.w,
                                              child: Image.asset("assets/icons/icon_arrow_forward.png"),
                                            )),
                                        Visibility(
                                            visible: controller.listExpand[index] == true,
                                            child: Container(
                                              margin: EdgeInsets.only(right: 8.w),
                                              height: 16.w,
                                              width: 16.w,
                                              child: Image.asset("assets/icons/icon_arrow_drop.png"),
                                            )),

                                      ],
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.only(top: 16.h)),
                                  Visibility(
                                      visible: controller.listExpand[index] == true,
                                      child:  Column(
                                        children: [
                                          ListViewShowImage.showGridviewImage(listImage),
                                          ListViewShowImage.showListViewNotImage(listNotImage),
                                          ListView.builder(
                                            shrinkWrap: true,
                                            physics: const NeverScrollableScrollPhysics(),
                                            itemCount: controller.listQuestion[index].answerOption?.length,
                                            itemBuilder: (context, indexAnswer) {
                                              return Container(
                                                margin: EdgeInsets.only(bottom: 8.h),
                                                child: Row(
                                                  children: [
                                                    Container(
                                                      height: 32.h,
                                                      width: 32.h,
                                                      decoration: BoxDecoration(
                                                          shape: BoxShape.circle,
                                                          color: controller.listQuestion[index]
                                                              .answerOption?[
                                                          indexAnswer]
                                                              .status ==
                                                              "TRUE"
                                                              ? const Color.fromRGBO(
                                                              77, 197, 145, 1)
                                                              : const Color.fromRGBO(
                                                              235, 235, 235, 1)),
                                                      padding: EdgeInsets.all(8.h),
                                                      alignment: Alignment.center,
                                                      child: Text(
                                                        String.fromCharCode(indexAnswer + 65),
                                                        style: TextStyle(
                                                            color: controller.listQuestion[index].answerOption?[
                                                            indexAnswer]
                                                                .status ==
                                                                "TRUE"
                                                                ? Colors.white
                                                                : const Color.fromRGBO(
                                                                177, 177, 177, 1),
                                                            fontSize: 14.sp,
                                                            fontWeight: FontWeight.w500),
                                                      ),
                                                    ),
                                                    Padding(
                                                        padding:
                                                        EdgeInsets.only(right: 8.w)),
                                                    Container(
                                                      padding: EdgeInsets.symmetric(
                                                          horizontal: 13.w,
                                                          vertical: 8.h),
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                          BorderRadius.circular(6.r),
                                                          border: Border.all(
                                                              color: controller.listQuestion[index].answerOption?[indexAnswer].status == "TRUE"
                                                                  ? const Color.fromRGBO(
                                                                  77, 197, 145, 1)
                                                                  : const Color.fromRGBO(
                                                                  192, 192, 192, 1)),
                                                          color: controller.listQuestion[index]
                                                              .answerOption?[
                                                          indexAnswer]
                                                              .status ==
                                                              "TRUE"
                                                              ? Colors.white
                                                              : const Color.fromRGBO(235, 235, 235, 1)),
                                                      child: SizedBox(
                                                        width: 230.w,
                                                        child: Text(
                                                          "${controller.listQuestion[index].answerOption?[indexAnswer].value}",style: TextStyle(
                                                            color: controller.listQuestion[index].answerOption?[indexAnswer].status == "TRUE"
                                                                ? const Color.fromRGBO(
                                                                77, 197, 145, 1)
                                                                : Colors.black
                                                        ),),
                                                      ),
                                                    ),
                                                    Padding(
                                                        padding:
                                                        EdgeInsets.only(right: 8.w)),
                                                    Visibility(
                                                        visible: controller.listQuestion[index]
                                                            .answerOption?[
                                                        indexAnswer]
                                                            .status ==
                                                            "TRUE",
                                                        child: const Icon(
                                                          Icons.check,
                                                          color: Color.fromRGBO(
                                                              77, 197, 145, 1),
                                                        )),
                                                    Visibility(
                                                        visible:controller.listQuestion[index]
                                                            .answerOption?[
                                                        indexAnswer]
                                                            .status ==
                                                            "FALSE",
                                                        child: const Icon(
                                                          Icons.clear,
                                                          color: Color.fromRGBO(
                                                              177, 177, 177, 1),
                                                        ))
                                                  ],
                                                ),
                                              );
                                            },
                                          ),
                                          Padding(padding: EdgeInsets.only(bottom: 16.h)),
                                          Row(
                                            children: [
                                              InkWell(
                                                  onTap: () {
                                                    var id  = controller.listQuestion[index].id;
                                                    controller.listErrorTextPointQuestion.removeAt(index);
                                                    controller.listStatusEdit.removeAt(index);
                                                    controller.controllerQuestion.removeAt(index);
                                                    controller.focusQuestion.removeAt(index);
                                                    controller.controllerScoreQuestion.removeAt(index);
                                                    controller.focusScoreQuestion.removeAt(index);
                                                    controller.listExpand.removeAt(index);
                                                    controller.listQuestion.removeWhere((element) => element.id == id);
                                                    controller.listQuestionCopy.removeWhere((element) => element.id == id);
                                                    controller.listErrorTextPointQuestion.refresh();
                                                    controller.listStatusEdit.refresh();
                                                    controller.listIdQuestion.removeWhere((element) => element == id);
                                                    controller.update();
                                                  },
                                                  child: Row(
                                                    children: [
                                                      Text("Xóa câu hỏi",style: TextStyle(color: const Color.fromRGBO(255, 69, 89, 1),fontSize: 14.sp,fontWeight: FontWeight.w400),),
                                                      const Icon(Icons.delete,color: Color.fromRGBO(255, 69, 89, 1),)
                                                    ],
                                                  )
                                              ),
                                              Padding(padding: EdgeInsets.only(right: 16.w)),
                                              InkWell(
                                                  onTap: () {
                                                    controller.listStatusEdit[index] = false;
                                                    controller.listStatusEdit.refresh();
                                                    controller.listQuestion.refresh();
                                                  },

                                                  child: Row(
                                                    children: [
                                                      Text("Chỉnh sửa câu hỏi",style: TextStyle(color: ColorUtils.PRIMARY_COLOR,fontSize: 14.sp,fontWeight: FontWeight.w400),),
                                                      const Icon(Icons.edit,color:ColorUtils.PRIMARY_COLOR,)
                                                    ],
                                                  )
                                              ),

                                            ],
                                          ),
                                          Padding(padding: EdgeInsets.only(bottom: 16.h)),

                                        ],
                                      )),
                                  const Divider()
                                ],
                              ))
                                  :Obx(() => Container(
                                margin: EdgeInsets.only(bottom: 8.h),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6.r),
                                    border: Border.all(color: ColorUtils.PRIMARY_COLOR)
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(padding: EdgeInsets.only(top: 8.h)),
                                    Container(
                                      margin: EdgeInsets.only(left: 16.w),
                                      child: Text(
                                          'Câu ${index + 1}: ',
                                          style: TextStyle(
                                              color: const Color.fromRGBO(
                                                  133, 133, 133, 1),
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14.sp,
                                              fontFamily:
                                              'assets/font/static/Inter-Medium.ttf')),
                                    ),

                                    Visibility(
                                        visible: controller.detailExam.typeExercise == "ALL",
                                        child: Container(
                                          margin: EdgeInsets.symmetric(horizontal: 16.w),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                alignment: Alignment.centerLeft,
                                                height: 28.h,
                                                child: Text(
                                                  "Loại bài kiểm tra",
                                                  style: TextStyle(
                                                      color: const Color.fromRGBO(133, 133, 133, 1),
                                                      fontSize: 13.sp,
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ),
                                              // Padding(padding: EdgeInsets.only(right: 8.w)),
                                              SizedBox(
                                                width: 116.w,
                                                height: 28.h,
                                                child: RadioListTile(
                                                    title: Transform.translate(
                                                      offset: const Offset(-8, 0),
                                                      child: Text(
                                                        "Trắc Nghiệm",
                                                        style: TextStyle(
                                                            fontSize: 12.sp,
                                                            fontWeight: FontWeight.w400,
                                                            color: Colors.black),
                                                      ),
                                                    ),
                                                    value: 0,
                                                    dense: true,
                                                    visualDensity: const VisualDensity(
                                                      horizontal:
                                                      VisualDensity.minimumDensity,
                                                      vertical: VisualDensity.minimumDensity,
                                                    ),
                                                    contentPadding: EdgeInsets.zero,
                                                    activeColor: ColorUtils.PRIMARY_COLOR,
                                                    groupValue:
                                                    controller.groupValue[index],
                                                    onChanged: (int? value) {
                                                      controller.groupValue[index] = 0;
                                                      controller.groupValue.refresh();
                                                    }),
                                              ),
                                              SizedBox(
                                                width: 85.w,
                                                height: 28.h,
                                                child: RadioListTile(
                                                    title: Transform.translate(
                                                      offset: const Offset(-8, 0),
                                                      child: Text(
                                                        "Tự luận",
                                                        style: TextStyle(
                                                            fontSize: 12.sp,
                                                            fontWeight: FontWeight.w400,
                                                            color: Colors.black),
                                                      ),
                                                    ),
                                                    value: 1,
                                                    dense: true,
                                                    visualDensity: const VisualDensity(
                                                      horizontal:
                                                      VisualDensity.minimumDensity,
                                                      vertical:
                                                      VisualDensity.minimumDensity,
                                                    ),
                                                    contentPadding: EdgeInsets.zero,
                                                    activeColor: ColorUtils.PRIMARY_COLOR,
                                                    groupValue:
                                                    controller.groupValue[index],
                                                    onChanged: (int? value) {
                                                      controller.groupValue[index] = 1;
                                                      controller.groupValue.refresh();
                                                    }),
                                              ),
                                            ],
                                          ),
                                        )),
                                    Obx(() => Visibility(
                                        visible: controller.isShowNote[index] == true,
                                        child: Visibility(
                                            visible: controller.groupValue[index] == 0,
                                            child: Container(
                                              margin: EdgeInsets.symmetric(horizontal: 16.w),
                                              decoration: BoxDecoration(
                                                  border: Border.all(
                                                      color: const Color.fromRGBO(
                                                          253, 185, 36, 1)),
                                                  color: const Color.fromRGBO(255, 246, 229, 1),
                                                  borderRadius: BorderRadius.circular(6.r)),
                                              padding: EdgeInsets.all(8.h),
                                              child: Column(
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      const Icon(
                                                        Icons.info,
                                                        color:
                                                        Color.fromRGBO(250, 162, 0, 1),
                                                      ),
                                                      const Icon(
                                                        Icons.check,
                                                        color:
                                                        Color.fromRGBO(77, 197, 145, 1),
                                                      ),

                                                      const Text("Là đáp án đúng"),

                                                      const Icon(
                                                        Icons.clear,
                                                        color: Color.fromRGBO(
                                                            177, 177, 177, 1),
                                                      ),
                                                      const Text("Là đáp án sai"),
                                                      InkWell(
                                                        onTap: () {
                                                          controller.isShowNote[index] =
                                                          false;
                                                          controller.isShowNote.refresh();
                                                        },
                                                        child: const Icon(
                                                          Icons.clear,
                                                          color: Colors.black,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Padding(padding: EdgeInsets.only(top: 8.h)),
                                                  Text(
                                                    "Nhấn thêm một lần để đổi trạng thái đáp án",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.w400,
                                                        fontSize: 12.sp),
                                                  )
                                                ],
                                              ),
                                            )))),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          children: [
                                            Padding(padding: EdgeInsets.only(top: 8.h)),
                                            GetBuilder<UpdateExamQuestionController>(
                                              builder: (controller) {
                                                return MyTextFormFieldBinh(
                                                  enable: true,
                                                  focusNode:
                                                  controller.focusScoreQuestion[index],
                                                  iconPrefix: null,
                                                  iconSuffix: null,
                                                  state: StateType.DEFAULT,
                                                  labelText: "Nhập Điểm Cho Câu Hỏi",
                                                  autofocus: false,
                                                  controller: controller
                                                      .controllerScoreQuestion[index],
                                                  helperText:
                                                  controller.listErrorTextPointQuestion[index],
                                                  showHelperText: controller
                                                      .listQuestion[index]
                                                      .errorPointQuestion,
                                                  textInputAction: TextInputAction.next,
                                                  ishowIconPrefix: false,
                                                  keyboardType: TextInputType.number,
                                                );
                                              },
                                            ),
                                            Padding(padding: EdgeInsets.only(top: 8.h)),
                                            GetBuilder<UpdateExamQuestionController>(
                                              builder: (controller) {
                                                return Container(
                                                  margin:
                                                  EdgeInsets.symmetric(horizontal: 16.w),
                                                  child: MyOutlineBorderTextFormFieldNhat(
                                                    enable: true,
                                                    focusNode:
                                                    controller.focusQuestion[index],
                                                    iconPrefix: null,
                                                    iconSuffix: null,
                                                    state: StateType.DEFAULT,
                                                    labelText: "Nhập câu hỏi",
                                                    autofocus: false,
                                                    controller:
                                                    controller.controllerQuestion[index],
                                                    helperText: "vui lòng nhập câu hỏi",
                                                    showHelperText: controller.listQuestion[index].errorContentQuestion,
                                                    textInputAction: TextInputAction.next,
                                                    ishowIconPrefix: false,
                                                    keyboardType: TextInputType.text,
                                                  ),
                                                );
                                              },
                                            ),
                                            Padding(padding: EdgeInsets.only(top: 8.h)),
                                            Visibility(
                                                visible:
                                                controller.groupValue[index] == 0,
                                                child: controller
                                                    .listQuestion
                                                [index]
                                                    .answerOption!
                                                    .isNotEmpty
                                                    ? ListView.builder(
                                                    shrinkWrap: true,
                                                    physics: const NeverScrollableScrollPhysics(),
                                                    itemCount: controller.listQuestion[index].answerOption?.length,
                                                    itemBuilder: (context, indexAnswer) {
                                                      return GetBuilder<UpdateExamQuestionController>(
                                                        builder: (controller) {
                                                          return Column(
                                                            children: [
                                                              Container(
                                                                margin: EdgeInsets.symmetric(horizontal: 16.w, vertical: 4.h),
                                                                child: Row(
                                                                  children: [
                                                                    Container(
                                                                      height: 32.h,
                                                                      width: 32.h,
                                                                      decoration:
                                                                      const BoxDecoration(
                                                                          shape: BoxShape
                                                                              .circle,
                                                                          color: Color
                                                                              .fromRGBO(
                                                                              254,
                                                                              230,
                                                                              211,
                                                                              1)),
                                                                      padding:
                                                                      EdgeInsets.all(8.h),
                                                                      alignment:
                                                                      Alignment.center,
                                                                      child: Text(String.fromCharCode(indexAnswer + 65),
                                                                        style: TextStyle(
                                                                            color: ColorUtils.PRIMARY_COLOR,
                                                                            fontSize: 14.sp,
                                                                            fontWeight:
                                                                            FontWeight
                                                                                .w500),
                                                                      ),
                                                                    ),
                                                                    Expanded(
                                                                      child:
                                                                      MyOutlineBorderTextFormFieldBinh(
                                                                        enable: true,
                                                                        focusNode: controller
                                                                            .listQuestion[index]
                                                                            .answerOption![
                                                                        indexAnswer]
                                                                            .focusNode!,
                                                                        iconPrefix: null,
                                                                        iconSuffix: null,
                                                                        state:
                                                                        StateType.DEFAULT,
                                                                        labelText:
                                                                        "Nhập câu trả lời ${indexAnswer + 1}",
                                                                        autofocus: false,
                                                                        controller: controller
                                                                            .listQuestion[index]
                                                                            .answerOption![
                                                                        indexAnswer]
                                                                            .textEditingController!,
                                                                        helperText:
                                                                        controller.listQuestion[index].answerOption?[indexAnswer].errorTextAnswer,
                                                                        showHelperText: controller
                                                                            .listQuestion
                                                                            [index]
                                                                            .answerOption?[
                                                                        indexAnswer]
                                                                            .validateAnswer,
                                                                        textInputAction:
                                                                        TextInputAction
                                                                            .next,
                                                                        ishowIconPrefix:
                                                                        false,
                                                                        keyboardType:
                                                                        TextInputType
                                                                            .text,
                                                                      ),
                                                                    ),
                                                                    Visibility(
                                                                        visible: controller
                                                                            .listQuestion[index]
                                                                            .answerOption?[
                                                                        indexAnswer]
                                                                            .status ==
                                                                            "TRUE",
                                                                        child: InkWell(
                                                                          onTap: () {
                                                                            for (int i = 0; i < controller.listQuestion[index].answerOption!.length; i++) {
                                                                              controller.listQuestion[index].answerOption?[i].status = "FALSE";
                                                                            }
                                                                            controller.listQuestion[index].answerOption?[indexAnswer].status = "FALSE";
                                                                            controller.listQuestion.refresh();
                                                                            controller.update();
                                                                          },
                                                                          child: const Icon(
                                                                            Icons.check,
                                                                            color: Color
                                                                                .fromRGBO(
                                                                                77,
                                                                                197,
                                                                                145,
                                                                                1),
                                                                          ),
                                                                        )),
                                                                    Visibility(
                                                                        visible: controller
                                                                            .listQuestion
                                                                        [index]
                                                                            .answerOption?[
                                                                        indexAnswer]
                                                                            .status ==
                                                                            "FALSE",
                                                                        child: InkWell(
                                                                          onTap: () {
                                                                            for (int i = 0; i < controller.listQuestion[index].answerOption!.length; i++) {
                                                                              controller.listQuestion[index].answerOption?[i].status = "FALSE";
                                                                            }
                                                                            controller.listQuestion[index].answerOption?[indexAnswer].status = "TRUE";
                                                                            controller.listQuestion.refresh();
                                                                            controller.update();
                                                                          },
                                                                          child: const Icon(
                                                                            Icons.close,
                                                                            color: Color
                                                                                .fromRGBO(
                                                                                177,
                                                                                177,
                                                                                177,
                                                                                1),
                                                                          ),
                                                                        )),
                                                                    InkWell(
                                                                      onTap: () {
                                                                        if (controller.listQuestion[index].answerOption!.length > 1) {
                                                                          controller.listQuestion[index].answerOption?.removeAt(indexAnswer);
                                                                          controller.listQuestion.refresh();
                                                                          controller.update();
                                                                        }
                                                                      },
                                                                      child: const Icon(
                                                                        Icons.delete,
                                                                        color: Color.fromRGBO(
                                                                            255, 69, 89, 1),
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              ),

                                                            ],
                                                          );
                                                        },
                                                      );
                                                    })
                                                    : Container()),
                                            Padding(padding: EdgeInsets.only(top: 8.h)),
                                            Visibility(
                                                visible:
                                                controller.groupValue[index] == 0,
                                                child: InkWell(
                                                  onTap: () {
                                                    controller.listQuestion[index].answerOption?.add(AnswerOption(
                                                        key: String.fromCharCode(controller.listQuestion[index].answerOption!.length + 65),
                                                        status: "FALSE",
                                                        value: "",
                                                        textEditingController:
                                                        TextEditingController(),
                                                        focusNode: FocusNode(),
                                                        validateAnswer: false));
                                                    controller.listQuestion.refresh();
                                                    controller.update();

                                                  },
                                                  child: Container(
                                                    margin: EdgeInsets.only(left: 16.w),
                                                    child: Row(
                                                      children: [
                                                        SizedBox(
                                                          height: 24.h,
                                                          width: 24.h,
                                                          child: Image.asset(
                                                              "assets/images/image_add_answer_question.png"),
                                                        ),
                                                        Padding(
                                                            padding:
                                                            EdgeInsets.only(right: 8.w)),
                                                        Text(
                                                          "Thêm câu trả lời",
                                                          style: TextStyle(
                                                              color: const Color.fromRGBO(
                                                                  72, 98, 141, 1),
                                                              fontWeight: FontWeight.w400,
                                                              fontSize: 14.sp),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                )),
                                            Padding(padding: EdgeInsets.only(top: 8.h)),
                                            InkWell(
                                              onTap: () {
                                                FileDevice.showSelectFileV2(Get.context!)
                                                    .then((value) {
                                                  if (value.isNotEmpty) {
                                                    controller.uploadFile(index,value);
                                                  }
                                                });
                                              },
                                              child: Container(
                                                margin:
                                                EdgeInsets.symmetric(horizontal: 14.w),
                                                child: DottedBorder(
                                                    dashPattern: const [5, 5],
                                                    radius: const Radius.circular(6),
                                                    borderType: BorderType.RRect,
                                                    color: const Color.fromRGBO(
                                                        192, 192, 192, 1),
                                                    padding: EdgeInsets.all(14.h),
                                                    child: SizedBox(
                                                      width: double.infinity,
                                                      child: controller.listFile[index]
                                                          .isNotEmpty
                                                          ? Column(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment.start,
                                                        children: [
                                                          ListView.builder(
                                                              shrinkWrap: true,
                                                              physics: const NeverScrollableScrollPhysics(),
                                                              itemCount: controller.listQuestion[index].files!.length,
                                                              itemBuilder: (context, indexFile) {
                                                                return InkWell(
                                                                  onTap: () {
                                                                    var action = 0;
                                                                    var extension = controller.listQuestion[index].files![indexFile].ext;
                                                                    if (extension == "png" ||
                                                                        extension == "jpg" ||
                                                                        extension == "jpeg" ||
                                                                        extension == "gif" ||
                                                                        extension == "bmp") {
                                                                      action = 1;
                                                                    } else if (extension == "pdf") {
                                                                      action = 2;
                                                                    } else {
                                                                      action = 0;
                                                                    }
                                                                    switch (action) {
                                                                      case 1:
                                                                        OpenUrl.openImageViewer(context, controller.listQuestion[index].files![indexFile].link!);
                                                                        break;
                                                                      case 2:
                                                                        Get.to(ViewPdfPage(url:controller.listQuestion[index].files![indexFile].link!));
                                                                        break;
                                                                      default:
                                                                        OpenUrl.openFile(controller.listQuestion[index].files![indexFile].link!);
                                                                        break;
                                                                    }
                                                                  },
                                                                  child: Container(
                                                                    margin: const EdgeInsets.only(top: 12),
                                                                    padding: const EdgeInsets.all(9),
                                                                    decoration: BoxDecoration(
                                                                        borderRadius: BorderRadius.circular(6),
                                                                        color: const Color.fromRGBO(246, 246, 246, 1)),
                                                                    child: Row(
                                                                      children: [
                                                                        Image.network(
                                                                          controller.listQuestion[index].files![indexFile].link!,
                                                                          errorBuilder: (
                                                                              BuildContext context,
                                                                              Object error,
                                                                              StackTrace? stackTrace,
                                                                              ) {
                                                                            return Image.asset(
                                                                              getFileIcon(controller.listQuestion[index].files![indexFile].name),
                                                                              height: 35,
                                                                              width: 30,
                                                                            );
                                                                          },
                                                                          height: 35,
                                                                          width: 30,
                                                                        ),
                                                                        const Padding(padding: EdgeInsets.only(left: 8)),
                                                                        SizedBox(
                                                                          width: 200.w,
                                                                          child: Text(
                                                                            '${controller.listQuestion[index].files![indexFile].name}',
                                                                            style: TextStyle(
                                                                                color: const Color.fromRGBO(26, 59, 112, 1),
                                                                                fontSize: 14.sp,
                                                                                fontFamily: 'assets/font/static/Inter-Medium.ttf',
                                                                                fontWeight: FontWeight.w500),
                                                                          ),
                                                                        ),
                                                                        Expanded(child: Container()),
                                                                        GestureDetector(
                                                                            child: const Icon(
                                                                              Icons.delete_outline_outlined,
                                                                              color: ColorUtils.COLOR_WORK_TYPE_4,
                                                                              size: 24,
                                                                            ),
                                                                            onTap: () {
                                                                              controller.listQuestion[index].files!.removeAt(indexFile);
                                                                              controller.listQuestion.refresh();
                                                                            }),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                );
                                                              }),
                                                          Padding(
                                                              padding: EdgeInsets.only(
                                                                  top: 8.h)),
                                                          TextButton(
                                                              onPressed: () {
                                                                FileDevice
                                                                    .showSelectFileV2(
                                                                    Get.context!)
                                                                    .then((value) {
                                                                  if (value.isNotEmpty) {
                                                                    controller.uploadFile(index,value);
                                                                  }
                                                                });
                                                              },
                                                              child: Text(
                                                                "Thêm tệp",
                                                                style: TextStyle(
                                                                    color: const Color
                                                                        .fromRGBO(
                                                                        72, 98, 141, 1),
                                                                    fontSize: 14.sp,
                                                                    fontWeight:
                                                                    FontWeight
                                                                        .w400),
                                                              )),
                                                        ],
                                                      )
                                                          : Column(
                                                        children: [
                                                          Padding(
                                                              padding: EdgeInsets.only(
                                                                  top: 16.h)),
                                                          const Icon(
                                                            Icons
                                                                .drive_folder_upload_rounded,
                                                            color: ColorUtils.PRIMARY_COLOR,
                                                            size: 40,
                                                          ),
                                                          Padding(
                                                              padding: EdgeInsets.only(
                                                                  top: 4.h)),
                                                          Text(
                                                            "Tải lên Ảnh / Video",
                                                            style: TextStyle(
                                                                color: const Color
                                                                    .fromRGBO(
                                                                    133, 133, 133, 1),
                                                                fontSize: 14.sp,
                                                                fontWeight:
                                                                FontWeight.w500),
                                                          ),
                                                          Padding(
                                                              padding: EdgeInsets.only(
                                                                  top: 12.h)),
                                                          Text(
                                                            "File (Video, Ảnh, Zip,...) có dung lượng không quá 10Mb",
                                                            style: TextStyle(
                                                                color: const Color
                                                                    .fromRGBO(
                                                                    133, 133, 133, 1),
                                                                fontSize: 12.sp,
                                                                fontWeight:
                                                                FontWeight.w400),
                                                          ),
                                                          Padding(
                                                              padding: EdgeInsets.only(
                                                                  bottom: 16.h)),
                                                        ],
                                                      ),
                                                    )),
                                              ),
                                            ),
                                            Padding(padding: EdgeInsets.only(top: 8.h)),
                                            Row(
                                              children: [
                                                Expanded(child: Container()),
                                                InkWell(
                                                  onTap: () {
                                                    controller.listExpand[index] = false;
                                                    controller.listExpand.refresh();
                                                    controller.listStatusEdit[index] = true;
                                                    controller.listStatusEdit.refresh();

                                                    if(controller.detailExam.questions!.where((element) => element.id == controller.listQuestion[index].id).toList().isNotEmpty){
                                                      controller.controllerScoreQuestion[index].text = controller.detailExam.questions![index].point.toString();
                                                      controller.controllerQuestion[index].text = controller.detailExam.questions![index].content!;
                                                      controller.listQuestion[index].answerOption = [];
                                                      controller.listQuestion[index].files = [];
                                                      for(int i =0;i<controller.detailExam.questions![index].answerOption!.length;i++){
                                                        controller.listQuestion[index].answerOption?.add(AnswerOption(
                                                            value: controller.detailExam.questions![index].answerOption![i].value,
                                                            textEditingController: TextEditingController(),
                                                            validateAnswer: false,
                                                            key: controller.detailExam.questions![index].answerOption![i].key,
                                                            focusNode: FocusNode(),
                                                            errorTextAnswer: "",
                                                            status:controller.detailExam.questions![index].answerOption![i].status
                                                        ));
                                                        controller.listQuestion[index].answerOption?[i].textEditingController?.text = controller.detailExam.questions![index].answerOption![i].value!;
                                                      }

                                                      for(int i =0;i<controller.detailExam.questions![index].files!.length;i++){
                                                        controller.listQuestion[index].files?.add(ResponseFileUpload(
                                                          size: controller.detailExam.questions![index].files![i].size,
                                                          ext: controller.detailExam.questions![index].files![i].ext,
                                                          link: controller.detailExam.questions![index].files![i].link,
                                                          name: controller.detailExam.questions![index].files![i].name,
                                                          originalFileName: controller.detailExam.questions![index].files![i].originalFileName,
                                                          tmpFolderUpload: controller.detailExam.questions![index].files![i].tmpFolderUpload,
                                                        ));

                                                      }
                                                      controller.listQuestion[index].errorPointQuestion = false;
                                                      controller.listQuestion[index].errorContentQuestion = false;
                                                      controller.listQuestion.refresh();
                                                    }else{
                                                      if(controller.listQuestionCopy.where((element) => element.id == controller.listQuestion[index].id).toList().isNotEmpty){
                                                        controller.controllerScoreQuestion[index].text = controller.listQuestion[index].point.toString();
                                                        controller.controllerQuestion[index].text = controller.listQuestion[index].content!;
                                                        controller.listQuestion[index].answerOption = [];
                                                        controller.listQuestion[index].files = [];
                                                        for(int i =0;i<controller.listQuestionCopy[index].answerOption!.length;i++){
                                                          controller.listQuestion[index].answerOption?.add(AnswerOption(
                                                              value: controller.listQuestionCopy[index].answerOption![i].value,
                                                              textEditingController: TextEditingController(),
                                                              validateAnswer: false,
                                                              key: controller.listQuestionCopy[index].answerOption![i].key,
                                                              focusNode: FocusNode(),
                                                              errorTextAnswer: "",
                                                              status:controller.listQuestionCopy[index].answerOption![i].status
                                                          ));
                                                          controller.listQuestion[index].answerOption?[i].textEditingController?.text = controller.listQuestionCopy[index].answerOption![i].value!;
                                                        }

                                                        for(int i =0;i<controller.listQuestionCopy[index].files!.length;i++){
                                                          controller.listQuestion[index].files?.add(ResponseFileUpload(
                                                            size: controller.listQuestionCopy[index].files![i].size,
                                                            ext: controller.listQuestionCopy[index].files![i].ext,
                                                            link: controller.listQuestionCopy[index].files![i].link,
                                                            name: controller.listQuestionCopy[index].files![i].name,
                                                            originalFileName: controller.listQuestionCopy[index].files![i].originalFileName,
                                                            tmpFolderUpload: controller.listQuestionCopy[index].files![i].tmpFolderUpload,
                                                          ));

                                                        }
                                                        controller.listQuestion[index].errorPointQuestion = false;
                                                        controller.listQuestion[index].errorContentQuestion = false;
                                                        controller.listQuestion.refresh();
                                                      }else{
                                                        var id = controller.listQuestion[index].id;
                                                        controller.listErrorTextPointQuestion.removeAt(index);
                                                        controller.listStatusEdit.removeAt(index);
                                                        controller.controllerQuestion.removeAt(index);
                                                        controller.focusQuestion.removeAt(index);
                                                        controller.controllerScoreQuestion.removeAt(index);
                                                        controller.focusScoreQuestion.removeAt(index);
                                                        controller.listIdQuestion.removeAt(index);
                                                        controller.listExpand.removeAt(index);
                                                        controller.listQuestion.removeWhere((element) => element.id == id);
                                                        controller.listQuestionCopy.removeWhere((element) => element.id == id);
                                                        controller.listErrorTextPointQuestion.refresh();
                                                        controller.listStatusEdit.refresh();
                                                        controller.listIdQuestion.removeWhere((element) => element == id);
                                                      }
                                                    }

                                                    controller.update();
                                                  },
                                                  child: Text("Hủy",style: TextStyle(color: const Color.fromRGBO(255, 69, 89, 1),fontSize: 14.sp,fontWeight: FontWeight.w400),),
                                                ),
                                                Padding(padding: EdgeInsets.only(right: 16.w)),
                                                InkWell(
                                                  onTap: () {
                                                    if (controller.controllerQuestion[index].text == "") {
                                                      controller.listQuestion[index].errorContentQuestion = true;
                                                      controller.update();
                                                    } else {
                                                      controller.listQuestion[index].errorContentQuestion = false;
                                                      controller.update();
                                                      if (controller.controllerScoreQuestion[index].text.trim() == "") {
                                                        controller.listQuestion[index].errorPointQuestion = true;
                                                        controller.listErrorTextPointQuestion[index] = "Vui lòng nhập điểm cho câu hỏi";
                                                        controller.update();
                                                      } else {
                                                        controller.listQuestion[index].errorPointQuestion = false;
                                                        controller.update();
                                                        if(controller.controllerScoreQuestion[index].text.trim().substring(controller.controllerScoreQuestion[index].text.trim().length - 1) == "."){
                                                          controller.listQuestion[index].errorPointQuestion = true;
                                                          controller.listErrorTextPointQuestion[index] = "Vui lòng nhập đúng định dạng điểm";
                                                          controller.update();
                                                        }else{
                                                          controller.listQuestion[index].point = double.tryParse(controller.controllerScoreQuestion[index].text.trim());
                                                        }
                                                        if (controller.listQuestion[index].point == null) {
                                                          controller.listQuestion[index].errorPointQuestion = true;
                                                          controller.listErrorTextPointQuestion[index] = "Vui lòng nhập đúng định dạng điểm";
                                                          controller.update();

                                                        }
                                                        else {
                                                          var listAnswer = <String>[];
                                                          for (int i = 0; i < controller.listQuestion[index].answerOption!.length; i++) {
                                                            if (controller.listQuestion[index].answerOption?[i].textEditingController?.text == "") {
                                                              controller.listQuestion[index].answerOption?[i].validateAnswer = true;
                                                              controller.update();
                                                              listAnswer.add("");
                                                            } else {
                                                              controller.listQuestion[index].answerOption?[i].validateAnswer = false;
                                                              listAnswer.add(controller.listQuestion[index].answerOption![i].textEditingController!.text);
                                                              controller.update();
                                                            }
                                                          }
                                                          if(listAnswer.contains("") == false){
                                                            controller.listStatus.value = [];
                                                            for (int i = 0; i < controller.listQuestion[index].answerOption!.length; i++) {
                                                              controller.listStatus.add(controller.listQuestion[index].answerOption![i].status!) ;
                                                            }
                                                            if( controller.listQuestion[index].typeQuestion =="CONSTRUCTED_RESPONSE"){
                                                              if (controller.groupValue[index] == 0) {
                                                                controller.listQuestion[index].typeQuestion = "SELECTED_RESPONSE";
                                                              }
                                                              if (controller.groupValue[index] == 1) {
                                                                controller.listQuestion[index].typeQuestion = "CONSTRUCTED_RESPONSE";
                                                                controller.listQuestion[index].answerOption = [];
                                                              }
                                                              for (int i = 0; i < controller.listQuestion.length; i++) {
                                                                controller.listQuestion[i].content = controller.controllerQuestion[i].text.trim();
                                                                controller.listQuestion[i].point = double.tryParse(controller.controllerScoreQuestion[i].text.trim());
                                                                if (controller.groupValue[i] == 0) {
                                                                  controller.listQuestion[i].typeQuestion = "SELECTED_RESPONSE";
                                                                }
                                                                if (controller.groupValue[i] == 1) {
                                                                  controller. listQuestion[i].typeQuestion = "CONSTRUCTED_RESPONSE";
                                                                  controller.listQuestion[i].answerOption = [];
                                                                }
                                                                for (int j = 0; j < controller.listQuestion[i].answerOption!.length; j++) {
                                                                  controller.listQuestion[i].answerOption![j].value = controller.listQuestion[i].answerOption![j].textEditingController?.text.trim();
                                                                }
                                                              }
                                                              controller.listStatusEdit[index] = true;
                                                              controller.listStatusEdit.refresh();
                                                              controller.listQuestion.refresh();
                                                            }else{
                                                              if(controller.listStatus.contains("TRUE") == true){
                                                                if (controller.groupValue[index] == 0) {
                                                                  controller.listQuestion[index].typeQuestion = "SELECTED_RESPONSE";
                                                                }
                                                                if (controller.groupValue[index] == 1) {
                                                                  controller.listQuestion[index].typeQuestion = "CONSTRUCTED_RESPONSE";
                                                                  controller.listQuestion[index].answerOption = [];
                                                                }
                                                                for (int i = 0; i < controller.listQuestion.length; i++) {
                                                                  controller.listQuestion[i].content = controller.controllerQuestion[i].text.trim();
                                                                  controller.listQuestion[i].point = double.tryParse(controller.controllerScoreQuestion[i].text.trim());
                                                                  if (controller.groupValue[i] == 0) {
                                                                    controller.listQuestion[i].typeQuestion = "SELECTED_RESPONSE";
                                                                  }
                                                                  if (controller.groupValue[i] == 1) {
                                                                    controller. listQuestion[i].typeQuestion = "CONSTRUCTED_RESPONSE";
                                                                    controller.listQuestion[i].answerOption = [];
                                                                  }
                                                                  for (int j = 0; j < controller.listQuestion[i].answerOption!.length; j++) {
                                                                    controller.listQuestion[i].answerOption![j].value = controller.listQuestion[i].answerOption![j].textEditingController?.text.trim();
                                                                  }
                                                                }
                                                                controller.listStatusEdit[index] = true;
                                                                controller.listStatusEdit.refresh();
                                                                controller.listQuestion.refresh();
                                                                controller.update();
                                                              }else{
                                                                for (int i = 0; i < controller.listQuestion[index].answerOption!.length; i++) {
                                                                  controller.listQuestion[index].answerOption?[i].validateAnswer = true;
                                                                  controller.listQuestion[index].answerOption?[i].errorTextAnswer = "Vui lòng chọn 1 đáp án đúng";
                                                                  controller.update();
                                                                }
                                                              }
                                                            }

                                                          }

                                                        }
                                                      }
                                                    }
                                                  },
                                                  child: Text("Lưu thay đổi",style: TextStyle(color: ColorUtils.PRIMARY_COLOR,fontSize: 14.sp,fontWeight: FontWeight.w400),),
                                                ),
                                                Padding(padding: EdgeInsets.only(right: 16.w)),
                                              ],
                                            ),
                                            Padding(padding: EdgeInsets.only(bottom: 8.h)),
                                          ],
                                        ),

                                      ],
                                    ),
                                  ],
                                ),
                              ));
                            },
                          )),

                        ],
                      ),
                    )
                        : Container()),
                  )),
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.symmetric(
                        vertical: 16.h, horizontal: 16.w),
                    child: Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: SizedBox(
                              width: double.infinity,
                              height: 48,
                              child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      side: const BorderSide(
                                          width: 1,
                                          color: ColorUtils.PRIMARY_COLOR),
                                      backgroundColor:
                                      const Color.fromRGBO(
                                          255, 255, 255, 1)),
                                  onPressed: () {
                                    controller.controllerFilter.text = "";
                                    controller.listQuestion.value = [];
                                    controller.listQuestion.value = controller.listQuestionCopy.where((element) => TiengViet.parse(element.content!.toLowerCase()).contains(TiengViet.parse("")) == true).toList();
                                    controller.setListQuestion();
                                    controller.listQuestion.refresh();
                                    FocusScope.of(context).unfocus();
                                    if (controller.controllerNameExam.text.trim() == "") {
                                      controller.isShowErrorTextNameExam = true;
                                      controller.update();
                                    } else {
                                      controller.isShowErrorTextNameExam = false;
                                      controller.update();
                                      if (controller.controllerDateStart.text.trim() == "") {
                                        controller.isShowErrorTextDateStart = true;
                                        controller.update();
                                      } else {
                                        controller.isShowErrorTextDateStart = false;
                                        controller.update();
                                        if(controller.controllerTimeStart.text.trim() == ""){
                                          controller.isShowErrorTextTimeStart = true;
                                          controller.update();
                                        }else{
                                          controller.isShowErrorTextTimeStart = false;
                                          controller.update();
                                          if(controller.controllerDateEnd.text.trim() == ""){
                                            controller.isShowErrorTextDateEnd = true;
                                            controller.update();
                                          }else{
                                            controller.isShowErrorTextDateEnd = false;
                                            controller.update();
                                            if (controller.controllerTimeEnd.text.trim() == "") {
                                              controller.isShowErrorTextTimeEnd = true;
                                              controller.update();
                                            }else{
                                              controller.isShowErrorTextTimeEnd = false;
                                              controller.update();
                                              if(DateFormat("dd/MM/yyyy HH:mm").
                                              parse("${controller.controllerDateStart.text.trim()} "
                                                  "${controller.controllerTimeStart.text.trim()}")
                                                  .millisecondsSinceEpoch >
                                                  DateFormat("dd/MM/yyyy HH:mm").
                                                  parse("${controller.controllerDateEnd.text.trim()} "
                                                      "${controller.controllerTimeEnd.text.trim()}")
                                                      .millisecondsSinceEpoch){
                                                AppUtils.shared.showToast("Vui lòng chọn thời gian bắt đầu nhỏ hơn thời gian kết thúc");
                                              }else{
                                                var scoreFactor = 0.0;
                                                for (int i = 0; i < controller.listQuestion.length; i++) {
                                                  controller.listQuestion[i].point = double.tryParse(controller.controllerScoreQuestion[i].text.trim());
                                                  scoreFactor += controller.listQuestion[i].point!;
                                                }
                                                for (int i = 0; i < controller.listQuestion.length; i++) {
                                                  controller. listQuestion[i].content = controller.controllerQuestion[i].text.trim();
                                                  controller. listQuestion[i].point = double.tryParse(controller.controllerScoreQuestion[i].text.trim());
                                                  if (controller.groupValue[i] == 0) {
                                                    controller.listQuestion[i].typeQuestion = "SELECTED_RESPONSE";
                                                  }
                                                  if (controller.groupValue[i] == 1) {
                                                    controller.listQuestion[i].typeQuestion = "CONSTRUCTED_RESPONSE";
                                                    controller.listQuestion[i].answerOption = [];
                                                  }
                                                  for (int j = 0; j < controller.listQuestion[i].answerOption!.length; j++) {
                                                    controller.listQuestion[i].answerOption![j].value = controller.listQuestion[i].answerOption![j].textEditingController?.text.trim();
                                                  }
                                                }

                                                var listAnswer = <String>[];
                                                var listContentQuestion = <String>[];
                                                for(int a = 0;a <controller.listQuestion.length;a++){
                                                  for (int i = 0; i < controller.listQuestion[a].answerOption!.length; i++) {
                                                    if (controller.listQuestion[a].answerOption?[i].textEditingController?.text == "") {
                                                      controller.listQuestion[a].answerOption?[i].validateAnswer = true;
                                                      controller.listQuestion[a].answerOption?[i].errorTextAnswer = "Vui lòng nhập câu trả lời";
                                                      listAnswer.add("");
                                                      controller.update();
                                                    } else {
                                                      controller.listQuestion[a].answerOption?[i].validateAnswer = false;
                                                      listAnswer.add(controller.listQuestion[a].answerOption![i].textEditingController!.text);
                                                      controller.update();
                                                    }
                                                  }
                                                }

                                                for(int i = 0;i<controller.listQuestion.length;i++){
                                                  if (controller.controllerQuestion[i].text == ""){
                                                    controller.listQuestion[i].errorContentQuestion = true;
                                                    controller.update();
                                                    listContentQuestion.add("");
                                                  }else{
                                                    controller.listQuestion[i].errorContentQuestion = false;
                                                    controller.update();
                                                    listContentQuestion.add(controller.controllerQuestion[i].text);
                                                  }
                                                }
                                                var listScore = <String>[];
                                                for(int i = 0;i<controller.listQuestion.length;i++){
                                                  if(controller.controllerScoreQuestion[i].text.trim() == ""){
                                                    controller.listQuestion[i].errorPointQuestion = true;
                                                    controller.listErrorTextPointQuestion[i] = "Vui lòng nhập điểm cho câu hỏi";
                                                    controller.update();
                                                    listScore.add("");
                                                  }else{
                                                    controller.listQuestion[i].errorPointQuestion = false;
                                                    controller.update();
                                                    if(controller.controllerScoreQuestion[i].text.trim().substring(controller.controllerScoreQuestion[i].text.trim().length - 1) == "."){
                                                      controller.listQuestion[i].errorPointQuestion = true;
                                                      controller.listErrorTextPointQuestion[i] = "Vui lòng nhập đúng định dạng điểm";
                                                      controller.update();
                                                    }else{
                                                      controller.listQuestion[i].point = double.tryParse(controller.controllerScoreQuestion[i].text.trim());
                                                      controller.listQuestion.refresh();
                                                      listScore.add(double.tryParse(controller.controllerScoreQuestion[i].text.trim()).toString());
                                                    }
                                                    if (controller.listQuestion[i].point == null) {
                                                      controller.listQuestion[i].errorPointQuestion = true;
                                                      controller.listErrorTextPointQuestion[i] = "Vui lòng nhập đúng định dạng điểm";
                                                      controller.update();
                                                      listScore.add("");
                                                    }
                                                  }
                                                }
                                                if(listScore.contains("") == false){
                                                  if(listContentQuestion.contains("") == false){
                                                    if(listAnswer.contains("") == false){
                                                      controller.listStatusAnswer.value = [];
                                                      for(int i = 0;i<controller.listQuestion.length;i++){
                                                        var list = <String>[];
                                                        for (int j = 0; j < controller.listQuestion[i].answerOption!.length; j++) {
                                                          list.add(controller.listQuestion[i].answerOption![j].status!);
                                                        }
                                                        controller.listStatusAnswer.add(list);
                                                        controller.listStatusAnswer.refresh();
                                                      }


                                                      var listCheckCorrect = <String>[];
                                                      for(int i = 0;i<controller.listQuestion.length;i++){
                                                        if( controller.listQuestion[i].typeQuestion =="CONSTRUCTED_RESPONSE"){
                                                          listCheckCorrect.add("TRUE");
                                                        }else{
                                                          if(controller.listStatusAnswer[i].contains("TRUE") == true){
                                                            listCheckCorrect.add("TRUE");
                                                          }else{
                                                            listCheckCorrect.add("");
                                                          }
                                                        }
                                                      }

                                                      if(listCheckCorrect.where((item) => item == "TRUE").toList().length ==controller.listQuestion.length ){
                                                        controller.updateExam(
                                                            controller.detailExam.id,
                                                            controller.controllerNameExam.text.trim(),
                                                            scoreFactor,
                                                            DateFormat("dd/MM/yyyy HH:mm").parse("${controller.controllerDateStart.text.trim()} ${controller.controllerTimeStart.text.trim()}").millisecondsSinceEpoch,
                                                            DateFormat("dd/MM/yyyy HH:mm").parse("${controller.controllerDateEnd.text.trim()} ${controller.controllerTimeEnd.text.trim()}").millisecondsSinceEpoch,
                                                            controller.controllerDescribe.text.trim(),
                                                            controller.detailExam.typeExercise,
                                                            Get.find<LearningManagementTeacherController>().detailSubject.value.id,
                                                            AppCache().userId,
                                                            Get.find<TeacherHomeController>().currentClass.value.classId,
                                                            controller.listQuestion);
                                                      }else{
                                                        for (int i = 0; i < controller.listQuestion.length; i++) {
                                                          for (int j = 0; j < controller.listQuestion[i].answerOption!.length; j++) {
                                                            if(listCheckCorrect[i] == ""){
                                                              controller.listQuestion[i].answerOption?[j].validateAnswer = true;
                                                              controller.listQuestion[i].answerOption?[j].errorTextAnswer = "Vui lòng chọn 1 đáp án đúng";
                                                              controller.update();
                                                            }
                                                          }
                                                        }
                                                      }


                                                    }
                                                  }else{
                                                    for(int i = 0;i<controller.listQuestion.length;i++){
                                                      if (controller.controllerQuestion[i].text == ""){
                                                        controller.listQuestion[i].errorContentQuestion = true;
                                                        controller.update();
                                                        listContentQuestion.add("");
                                                      }else{
                                                        controller.listQuestion[i].errorContentQuestion = false;
                                                        controller.update();
                                                        listContentQuestion.add(controller.controllerQuestion[i].text);
                                                      }
                                                    }
                                                  }
                                                }else{
                                                  for(int i = 0;i<controller.listQuestion.length;i++){
                                                    if(controller.controllerScoreQuestion[i].text.trim() == ""){
                                                      controller.listQuestion[i].errorPointQuestion = true;
                                                      controller.listErrorTextPointQuestion[i] = "Vui lòng nhập điểm cho câu hỏi";
                                                      controller.update();
                                                      listScore.add("");
                                                    }else{
                                                      controller.listQuestion[i].errorPointQuestion = false;
                                                      controller.update();
                                                      if(controller.controllerScoreQuestion[i].text.trim().substring(controller.controllerScoreQuestion[i].text.trim().length - 1) == "."){
                                                        controller.listQuestion[i].errorPointQuestion = true;
                                                        controller.listErrorTextPointQuestion[i] = "Vui lòng nhập đúng định dạng điểm";
                                                        controller.update();
                                                      }else{
                                                        controller.listQuestion[i].point = double.tryParse(controller.controllerScoreQuestion[i].text.trim());
                                                        controller.listQuestion.refresh();
                                                        listScore.add(double.tryParse(controller.controllerScoreQuestion[i].text.trim()).toString());
                                                      }
                                                      if (controller.listQuestion[i].point == null) {
                                                        controller.listQuestion[i].errorPointQuestion = true;
                                                        controller.listErrorTextPointQuestion[i] = "Vui lòng nhập đúng định dạng điểm";
                                                        controller.update();
                                                        listScore.add("");
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }

                                      }
                                    }

                                  },
                                  child: const Text(
                                    'Xác nhận',
                                    style: TextStyle(
                                        color: ColorUtils.PRIMARY_COLOR,
                                        fontSize: 16),
                                  )),
                            )),
                        Padding(padding: EdgeInsets.only(right: 16.w)),
                        Expanded(
                            flex: 1,
                            child: SizedBox(
                              height: 48,
                              child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: ColorUtils.PRIMARY_COLOR,
                                  ),
                                  onPressed: () {
                                    controller.controllerFilter.text = "";
                                    controller.listQuestion.value = [];
                                    controller.listQuestion.value = controller.listQuestionCopy.where((element) => TiengViet.parse(element.content!.toLowerCase()).contains(TiengViet.parse("")) == true).toList();
                                    controller.setListQuestion();

                                    controller.listIdQuestion = [];
                                    for(int j = 0;j <controller.listQuestionCopy.length;j++){
                                      controller.listIdQuestion.add(controller.listQuestionCopy[j].id!);
                                    }
                                    var id = DateTime.now().millisecondsSinceEpoch.toString();
                                    controller.listIdQuestion.add(id);
                                    var question = Questions().obs;
                                    question.value.files = [];
                                    question.value.id = id;
                                    question.value.answerOption = [];
                                    question.value.answerOption?.add(AnswerOption());
                                    question.value.errorContentQuestion = false;
                                    question.value.errorPointQuestion = false;
                                    question.value.answerOption![0] = AnswerOption(
                                          key: "A",
                                          value: "",
                                          status: "FALSE",
                                          textEditingController:
                                          TextEditingController(),
                                          focusNode: FocusNode(),
                                          validateAnswer: false,
                                          errorTextAnswer: "");
                                    controller.listQuestion
                                        .add(question.value);
                                    controller.listQuestion.refresh();
                                    var file = <ReqFile>[];
                                    controller.groupValue.add(0);
                                    controller.listFile.add(file);
                                    controller.groupValue.refresh();
                                    controller.listFile.refresh();
                                    if (controller.isShowNote[0] == true) {
                                      controller.isShowNote.add(true);
                                      controller.isShowNote.refresh();
                                    } else {
                                      controller.isShowNote.add(false);
                                      controller.isShowNote.refresh();
                                    }
                                    controller.listErrorTextPointQuestion.add("");
                                    controller.listStatusEdit.add(false);
                                    controller.controllerQuestion.add(TextEditingController());
                                    controller.focusQuestion.add(FocusNode());
                                    controller.controllerScoreQuestion.add(TextEditingController());
                                    controller.focusScoreQuestion.add(FocusNode());
                                    controller.listExpand.add(true);
                                    controller.update();
                                    controller.scrollController.jumpTo(controller.scrollController.position.maxScrollExtent+300.h);
                                  },
                                  child: Text(
                                    'Tạo câu tiếp',
                                    style: TextStyle(
                                        color: const Color.fromRGBO(
                                            255, 255, 255, 1),
                                        fontSize: 16.sp,
                                        fontWeight: FontWeight.w400,
                                        fontFamily:
                                        'assets/font/static/Inter-Regular.ttf'),
                                  )),
                            ))
                      ],
                    ),
                  )
                ],
              )),
        );
  }

  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10),
        isDisablePreviousDay: true)
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (int.parse(date.substring(6, 10)) <= DateTime.now().year &&
        int.parse(date.substring(3, 5)) <= DateTime.now().month &&
        int.parse(date.substring(0, 2)) < DateTime.now().day) {
      AppUtils.shared.showToast("Vui lòng không chọn ngày trong quá khứ");
      controller.controllerDateEnd.text = "";
    }else{
      controller.controllerDateEnd.text = date;
    }
    
  }


  selectTimeEnd(stringTime,format) async {
    var now = DateTime.now();
    if (!stringTime.isEmpty) {
      now = TimeUtils.convertStringToDate(stringTime, format);
    }
    var time = "";
    await DateTimePicker.showTimePicker(Get.context!, now).then((times) {
      time =
          TimeUtils.convertDateTimeToFormat(times, DateTimeFormat.formatTime);
    });

    if (time.isNotEmpty) {
      controller.controllerTimeEnd.text = time;
    }
  }



  selectDateTimeStart(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10),
        isDisablePreviousDay: true)
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (int.parse(date.substring(6, 10)) <= DateTime.now().year &&
        int.parse(date.substring(3, 5)) <= DateTime.now().month &&
        int.parse(date.substring(0, 2)) < DateTime.now().day) {
      AppUtils.shared.showToast("Vui lòng không chọn ngày trong quá khứ");
      controller.controllerDateStart.text = "";
    }else{
      controller.controllerDateStart.text = date;
    }
    
  }


  selectTimeStart(stringTime,format) async {
    var now = DateTime.now();
    if (!stringTime.isEmpty) {
      now = TimeUtils.convertStringToDate(stringTime, format);
    }
    var time = "";
    await DateTimePicker.showTimePicker(Get.context!, now).then((times) {
      time =
          TimeUtils.convertDateTimeToFormat(times, DateTimeFormat.formatTime);
    });

    if (time.isNotEmpty) {
      controller.controllerTimeStart.text = time;
    }
  }

}
