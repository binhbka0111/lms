import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../../../../data/repository/exams/exam_repo.dart';
import '../../view_exam_controller.dart';


class UpdateExamAssignLinkController extends GetxController{
  final ExamRepo examRepo = ExamRepo();
  var focusAttachLink = FocusNode();
  var controllerAttachLink = TextEditingController();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var detailExam = ItemsExam().obs;
  var focusNameExercise = FocusNode();
  var controllerNameExam = TextEditingController();
  var focusDescribe = FocusNode();
  var controllerDescribe = TextEditingController();
  var controllerDateEnd = TextEditingController();
  var controllerTimeEnd = TextEditingController();
  var controllerDateStart = TextEditingController();
  var controllerTimeStart  = TextEditingController();
  var isShowErrorTextDateEnd= false;
  var isShowErrorTextTimeEnd= false;
  var isShowErrorTextDateStart= false;
  var isShowErrorTextTimeStart= false;
  var isShowErrorTextNameExam = false;
  var isShowErrorLink = false;
  var errorTextDateStart = "Vui lòng nhập ngày bắt đầu";
  var errorTextTimeStart = "Vui lòng nhập giờ bắt đầu";
  var errorTextDateEnd = "Vui lòng nhập ngày kết thúc";
  var errorTextTimeEnd = "Vui lòng nhập giờ kết thúc";
  var isShowErrorPoint = false;
  var errorTextPoint = "".obs;
  var focusPoint = FocusNode();
  var controllerPoint = TextEditingController();
  @override
  void onInit() {
    var detailItem = Get.arguments;
    if (detailItem != null) {
      detailExam.value = detailItem;
      controllerAttachLink.text = detailExam.value.link!;
      controllerNameExam.text = detailExam.value.title!;
      controllerDescribe.text = detailExam.value.description!;
      controllerPoint.text = detailExam.value.scoreOfExam.toString();
      controllerDateEnd.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(detailExam.value.endTime!)).substring(0,10);
      controllerDateStart.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(detailExam.value.startTime!)).substring(0,10);
      controllerTimeEnd.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(detailExam.value.endTime!)).substring(11,16);
      controllerTimeStart.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(detailExam.value.startTime!)).substring(11,16);
    }
    super.onInit();
  }


  updateExam(exerciseId,title, startTime,endTime, description, typeExercise, subjectId, teacherId, classId, link,scoreOfExam) {
    examRepo.updateExamLink(exerciseId,title,startTime,endTime, description, typeExercise, subjectId, teacherId, classId, link,scoreOfExam).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật bài kiểm tra thành công");
        Get.back();
        Get.find<ViewExamController>().onInit();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Cập nhật bài kiểm tra thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


}