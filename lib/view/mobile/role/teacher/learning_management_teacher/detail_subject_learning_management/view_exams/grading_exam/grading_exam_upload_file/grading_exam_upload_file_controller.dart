import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../../../../data/model/res/exercise/list_student_exercise.dart';
import '../../../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../../../../data/repository/exams/exam_repo.dart';

class GradingExamUploadFileController extends GetxController with GetSingleTickerProviderStateMixin{
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var detailExam = ItemsExam().obs;
  var listTitle = <String>[].obs;
  var listStudentExercise = ListStudentSubmitted().obs;
  var filesUploadExercise = <ResponseFileUpload>[].obs;
  final ExamRepo examRepo = ExamRepo();
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  var studentSubmitted = <StudentSubmitted>[].obs;
  var studentUnSubmitted = <StudentUnSubmitted>[].obs;
  TabController? tabController ;
  ScrollController? scrollController;
  bool fixedScroll = false;
  RxInt indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  var listImage = [];
  var listNotImage = [];
  @override
  void onInit() {
    scrollController = ScrollController();
    scrollController?.addListener(_scrollListener);
    tabController = TabController(length: 2, vsync: this);
    var detailItem = Get.arguments;
    if (detailItem != null) {
      detailExam.value = detailItem;
      filesUploadExercise.value = detailExam.value.files!;
      listImage = filesUploadExercise.where((element) => element.ext == "png" ||
          element.ext == "jpg" ||
          element.ext == "jpeg" ||
          element.ext == "gif" ||
          element.ext == "bmp").toList().obs;
      listNotImage = filesUploadExercise.where((element) =>
      element.ext != "png" &&
          element.ext != "jpg" &&
          element.ext != "jpeg" &&
          element.ext != "gif" &&
          element.ext != "bmp")
          .toList().obs;
    }
    getListStudentSubmit(detailExam.value.id);
    super.onInit();
  }


  @override
  dispose() {
    pageController.dispose();
    scrollController?.dispose();
    super.dispose();
  }


  getListStudentSubmit(exerciseId){
    examRepo.studentsSubmitted(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudentExercise.value = value.object!;
        studentSubmitted.value = listStudentExercise.value.studentSubmitted!;
        studentUnSubmitted.value = listStudentExercise.value.studentUnSubmitted!;
        update();
      }
    });
  }

  _scrollListener() {
    if (fixedScroll) {
      scrollController?.jumpTo(0);
    }
  }


  onPageViewChange(int page) {
    indexClick.value = page;
    if (page != 0) {
      indexClick.value - 1;
    } else {
      indexClick.value = 0;
    }
  }


  showColor(index) {
    click.value = [];
    for (int i = 0; i < 2; i++) {
      click.add(false);
    }
    click[index] = true;
  }







}