import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../../../../data/model/res/exams/detail_exam.dart';
import '../../../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../../../../data/repository/exams/exam_repo.dart';

class DetailExamUploadFileController extends GetxController{
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var itemExam = ItemsExam().obs;
  var filesUploadExam = <ResponseFileUpload>[].obs;
  final ExamRepo examRepo = ExamRepo();
  var detailExam = DetailExam().obs;
  var isReady = false.obs;
  @override
  void onInit() {
    var detailItem = Get.arguments;
    if (detailItem != null) {
      itemExam.value = detailItem;
      filesUploadExam.value = itemExam.value.files!;
    }
    super.onInit();
  }

  getDetailExam(examId) {
    examRepo.detailExam(examId).then((value) {
      if (value.state == Status.SUCCESS) {
        detailExam.value = value.object!;
        isReady.value = true;
        AppUtils.shared.showToast("Lấy chi tiết bài kiểm tra thành công");
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lấy chi tiết bài kiểm tra thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        isReady.value = true;
      }
    });
  }

}