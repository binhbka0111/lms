import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/list_view_image.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/detail_exam/view_exam_question/view_exam_question_controller.dart';
import 'package:tiengviet/tiengviet.dart';
import '../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../learning_management_teacher_controller.dart';

class ViewExamQuestionPage extends GetView<ViewExamQuestionController> {
  @override
  final controller = Get.put(ViewExamQuestionController());

  ViewExamQuestionPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            elevation: 0,
            title: Text(
              "Bài Kiểm Tra môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w500),
            ),
            actions: [
              InkWell(
                onTap: () {
                  comeToHome();
                },
                child: const Icon(
                  Icons.home,
                  color: Colors.white,
                ),
              ),
              Padding(padding: EdgeInsets.only(right: 16.w))
            ],
          ),
          body: SingleChildScrollView(
            physics: const ScrollPhysics(),
            child: Obx(() => controller.isReady.value
                ? Container(
                    margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${controller.detailExam.value.title}",
                          style: TextStyle(
                              color: const Color.fromRGBO(133, 133, 133, 1),
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w500),
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Row(
                          children: [
                            SizedBox(
                              width: 100.w,
                              child: Text(
                                "Ngày tạo",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Text(
                              controller.outputDateFormat.format(
                                  DateTime.fromMillisecondsSinceEpoch(controller
                                      .detailExam.value.createdAt!)),
                              style: TextStyle(
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Row(
                          children: [
                            SizedBox(
                              width: 100.w,
                              child: Text("Thời gian bắt đầu",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                            ),
                            Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExam.value.startTime!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Row(
                          children: [
                            SizedBox(
                              width: 100.w,
                              child: Text("Thời gian kết thúc",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                            ),
                            Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExam.value.endTime!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Row(
                          children: [
                            SizedBox(
                              width: 100.w,
                              child: Text(
                                "Ghi chú",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            SizedBox(
                              width: 230.w,
                              child: Text(
                                "${controller.detailExam.value.description}",
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Row(
                          children: [
                            SizedBox(
                              width: 100.w,
                              child: Text("Điểm tối đa",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                            ),
                            Text("${controller.detailExam.value.scoreOfExam}",style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                          ],
                        ),

                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Container(
                          color: Colors.white,
                          height: 40.h,
                          child:
                          TextFormField(
                            onChanged: (value) {
                              controller.detailExam.value.questions?.clear();
                              controller.detailExam.value.questions = controller.listQuestion.where((element) => TiengViet.parse(element.content!.toLowerCase()).contains(TiengViet.parse(value.toLowerCase())) == true).toList();
                              controller.detailExam.refresh();
                            },
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Color.fromRGBO(177, 177, 177, 1)),
                                    borderRadius: BorderRadius.circular(6)),
                                suffixIcon: const Icon(
                                  Icons.search,
                                  color: Colors.black,
                                ),
                                contentPadding: EdgeInsets.only(left: 8.w),
                                hintText: "Tìm kiếm",
                                isCollapsed: true,
                                hintStyle: TextStyle(
                                    fontSize: 14.sp,
                                    color: const Color.fromRGBO(177, 177, 177, 1)),
                                focusedBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Color.fromRGBO(248, 129, 37, 1)))),
                            textAlignVertical: TextAlignVertical.center,
                            cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 16)),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: controller.detailExam.value.questions?.length,
                          itemBuilder: (context, index) {
                            var listImage =  controller.detailExam.value.questions![index].files!.where((element) => element.ext == "png" ||
                                element.ext == "jpg" ||
                                element.ext == "jpeg" ||
                                element.ext == "gif" ||
                                element.ext == "bmp").toList().obs;
                            var listNotImage =  controller.detailExam.value.questions![index].files!.where((element) =>
                            element.ext != "png" &&
                                element.ext != "jpg" &&
                                element.ext != "jpeg" &&
                                element.ext != "gif" &&
                                element.ext != "bmp")
                                .toList().obs;

                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  width: 300.w,
                                  child: RichText(
                                      text: TextSpan(children: [
                                    TextSpan(
                                        text: 'Câu ${controller.getIndexQuestion( controller.detailExam.value.questions![index].id)}: ',
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                133, 133, 133, 1),
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12.sp,
                                            fontFamily:
                                                'assets/font/static/Inter-Medium.ttf')),
                                    TextSpan(
                                        text:
                                            '[${controller.detailExam.value.questions?[index].point.toString()}đ] ',
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                248, 129, 37, 1),
                                            fontSize: 12.sp,
                                            fontWeight: FontWeight.w500,
                                            fontFamily:
                                                'assets/font/static/Inter-Medium.ttf')),
                                    TextSpan(
                                        text:
                                            '${controller.detailExam.value.questions?[index].content}: ',
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                26, 26, 26, 1),
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12.sp,
                                            fontFamily:
                                                'assets/font/static/Inter-Medium.ttf')),
                                  ])),
                                ),
                                Padding(padding: EdgeInsets.only(top: 16.h)),
                                ListViewShowImage.showGridviewImage(listImage),
                                ListViewShowImage.showListViewNotImage(listNotImage),
                                ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: controller.detailExam.value
                                      .questions?[index].answerOption?.length,
                                  itemBuilder: (context, indexAnswer) {
                                    return Container(
                                      width: 280.w,
                                      margin: EdgeInsets.only(bottom: 8.h),
                                      child: Row(
                                        children: [
                                          Container(
                                            height: 32.h,
                                            width: 32.h,
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: controller
                                                            .detailExam
                                                            .value
                                                            .questions?[index]
                                                            .answerOption?[
                                                                indexAnswer]
                                                            .status ==
                                                        "TRUE"
                                                    ? const Color.fromRGBO(
                                                        77, 197, 145, 1)
                                                    : const Color.fromRGBO(
                                                        235, 235, 235, 1)),
                                            padding: EdgeInsets.all(8.h),
                                            alignment: Alignment.center,
                                            child: Text(
                                              "${controller.detailExam.value.questions?[index].answerOption?[indexAnswer].key}",
                                              style: TextStyle(
                                                  color: controller
                                                              .detailExam
                                                              .value
                                                              .questions?[index]
                                                              .answerOption?[
                                                                  indexAnswer]
                                                              .status ==
                                                          "TRUE"
                                                      ? Colors.white
                                                      : const Color.fromRGBO(
                                                          177, 177, 177, 1),
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Padding(
                                              padding:
                                                  EdgeInsets.only(right: 8.w)),
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 16.w,
                                                vertical: 8.h),
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(6.r),
                                                border: Border.all(
                                                    color: controller.detailExam.value.questions?[index].answerOption?[indexAnswer].status == "TRUE"
                                                        ? const Color.fromRGBO(
                                                            77, 197, 145, 1)
                                                        : const Color.fromRGBO(
                                                            192, 192, 192, 1)),
                                                color: controller
                                                            .detailExam
                                                            .value
                                                            .questions?[index]
                                                            .answerOption?[
                                                                indexAnswer]
                                                            .status ==
                                                        "TRUE"
                                                    ? Colors.white
                                                    : const Color.fromRGBO(235, 235, 235, 1)),
                                            child: SizedBox(
                                              width: 220.w,
                                              child: Text(
                                                  "${controller.detailExam.value.questions?[index].answerOption?[indexAnswer].value}",style: TextStyle(
                                                    color: controller.detailExam.value.questions?[index].answerOption?[indexAnswer].status == "TRUE"
                                                        ? const Color.fromRGBO(
                                                        77, 197, 145, 1)
                                                        : Colors.black
                                              ),),
                                            ),
                                          ),
                                          Padding(
                                              padding:
                                                  EdgeInsets.only(right: 8.w)),
                                          Visibility(
                                              visible: controller
                                                      .detailExam
                                                      .value
                                                      .questions?[index]
                                                      .answerOption?[
                                                          indexAnswer]
                                                      .status ==
                                                  'TRUE',
                                              child: const Icon(
                                                Icons.check,
                                                color: Color.fromRGBO(
                                                    77, 197, 145, 1),
                                              )),
                                          Visibility(
                                              visible: controller
                                                      .detailExam
                                                      .value
                                                      .questions?[index]
                                                      .answerOption?[
                                                          indexAnswer]
                                                      .status ==
                                                  "FALSE",
                                              child: const Icon(
                                                Icons.clear,
                                                color: Color.fromRGBO(
                                                    177, 177, 177, 1),
                                              ))
                                        ],
                                      ),
                                    );
                                  },
                                )
                              ],
                            );
                          },
                        ),
                      ],
                    ),
                  )
                : Container()),
          )),
    );
  }
}
