import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/loading_custom.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exams/view_exam_controller.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import '../../../../../../../commom/constants/date_format.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../../commom/utils/date_time_picker.dart';
import '../../../../../../../commom/utils/time_utils.dart';
import '../../../../../../../routes/app_pages.dart';
import '../../../../../home/home_controller.dart';

class ViewExamTeacherPage extends GetWidget<ViewExamController>{
  @override
  final controller = Get.put(ViewExamController());

  ViewExamTeacherPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              comeToHome();
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Text(
          'Bài kiểm tra',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: RefreshIndicator(
        color: ColorUtils.PRIMARY_COLOR,
        onRefresh: () async{  controller.getListExam(controller.setTypeExercise(controller.statusExercise.value),controller.setStatusPublic(controller.isPublic.value)); },
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(top: 16, right: 16, left: 16),
            child: SingleChildScrollView(
              child: Obx(() => Column(
                children: [
                  Container(
                    height: 60.h,
                    padding: EdgeInsets.symmetric(vertical: 14.h),
                    color: Colors.white,
                    child: Row(
                      children: [
                        Expanded(
                            child:
                            TextFormField(
                              style: TextStyle(
                                fontSize: 12.0.sp,
                                color: const Color.fromRGBO(26, 26, 26, 1),
                              ),
                              onTap: () {
                                selectDateTimeStart(controller.controllerDateStart.value.text, DateTimeFormat.formatDateShort,context);
                              },
                              cursorColor: ColorUtils.PRIMARY_COLOR,
                              controller:
                              controller.controllerDateStart.value,
                              readOnly: true,
                              decoration: InputDecoration(
                                suffixIcon: Container(
                                  margin: EdgeInsets.zero,
                                  child: SvgPicture.asset(
                                    "assets/images/icon_date_picker.svg",
                                    fit: BoxFit.scaleDown,
                                  ),
                                ),
                                labelText: "Từ ngày",
                                enabledBorder:const OutlineInputBorder(
                                  borderSide: BorderSide(color:Color.fromRGBO(177, 177, 177, 1),),
                                ),

                                focusedBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(color:Color.fromRGBO(177, 177, 177, 1),),
                                ),
                                labelStyle: TextStyle(
                                    color: const Color.fromRGBO(177, 177, 177, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'assets/font/static/Inter-Medium.ttf'
                                ),
                              ),
                            )
                        ),
                        Expanded(
                          child:
                          Container(
                            margin: EdgeInsets.only(left: 16.w),
                            child:
                            TextFormField(
                              style: TextStyle(
                                fontSize: 12.0.sp,
                                color: const Color.fromRGBO(26, 26, 26, 1),
                              ),
                              onTap: () {
                                selectDateTimeEnd(controller.controllerDateEnd.value.text, DateTimeFormat.formatDateShort,context);
                              },
                              cursorColor: ColorUtils.PRIMARY_COLOR,
                              controller:
                              controller.controllerDateEnd.value,
                              readOnly: true,
                              decoration: InputDecoration(
                                suffixIcon: Container(
                                  margin: EdgeInsets.zero,
                                  child: SvgPicture.asset(
                                    "assets/images/icon_date_picker.svg",
                                    fit: BoxFit.scaleDown,
                                  ),
                                ),
                                enabledBorder:const OutlineInputBorder(
                                  borderSide: BorderSide(color:Color.fromRGBO(177, 177, 177, 1),),
                                ),

                                focusedBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(color:Color.fromRGBO(177, 177, 177, 1),),
                                ),
                                labelText: "Đến ngày",
                                labelStyle: TextStyle(
                                    color: const Color.fromRGBO(177, 177, 177, 1),
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'assets/font/static/Inter-Medium.ttf'
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.h),
                    height: 40,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                            color: const Color.fromRGBO(192, 192, 192, 1)),
                        borderRadius: BorderRadius.circular(8)),
                    child: Theme(
                      data: Theme.of(context).copyWith(
                        canvasColor: Colors.white,
                      ),
                      child: DropdownButtonHideUnderline(
                          child: DropdownButton(
                            isExpanded: true,
                            iconSize: 0,
                            icon: const Visibility(
                                visible: false,
                                child: Icon(Icons.arrow_downward)),
                            elevation: 16,
                            hint: controller.statusExercise.value != ""
                                ? Row(
                              children: [
                                Text(
                                  controller.statusExercise.value,
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: ColorUtils.PRIMARY_COLOR),
                                ),
                                Expanded(child: Container()),
                                const Icon(
                                  Icons.keyboard_arrow_down,
                                  color: Colors.black,
                                  size: 18,
                                )
                              ],
                            )
                                : Row(
                              children: [
                                Text(
                                  'Hạn nộp',
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black),
                                ),
                                Expanded(child: Container()),
                                const Icon(
                                  Icons.keyboard_arrow_down,
                                  color: Colors.black,
                                  size: 18,
                                )
                              ],
                            ),
                            items: ["Tất cả","Đến hạn","Đã hết hạn","Sắp đến hạn"].map(
                                  (value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(
                                    value,
                                    style: TextStyle(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w400,
                                        color: ColorUtils.PRIMARY_COLOR),
                                  ),
                                );
                              },
                            ).toList(),
                            onChanged: (String? value) {
                              controller.statusExercise.value = value!;
                              controller.getListExam(controller.setTypeExercise(controller.statusExercise.value),controller.setStatusPublic(controller.isPublic.value));
                            },
                          )),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 8.h)),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.h),
                    height: 40,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                            color: const Color.fromRGBO(192, 192, 192, 1)),
                        borderRadius: BorderRadius.circular(8)),
                    child: Theme(
                      data: Theme.of(context).copyWith(
                        canvasColor: Colors.white,
                      ),
                      child: DropdownButtonHideUnderline(
                          child: DropdownButton(
                            isExpanded: true,
                            iconSize: 0,
                            icon: const Visibility(
                                visible: false,
                                child: Icon(Icons.arrow_downward)),
                            elevation: 16,
                            hint: controller.isPublic.value != ""
                                ? Row(
                              children: [
                                Text(
                                  controller.isPublic.value,
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: ColorUtils.PRIMARY_COLOR),
                                ),
                                Expanded(child: Container()),
                                const Icon(
                                  Icons.keyboard_arrow_down,
                                  color: Colors.black,
                                  size: 18,
                                )
                              ],
                            )
                                : Row(
                              children: [
                                Text(
                                  'Trạng thái công bố',
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black),
                                ),
                                Expanded(child: Container()),
                                const Icon(
                                  Icons.keyboard_arrow_down,
                                  color: Colors.black,
                                  size: 18,
                                )
                              ],
                            ),
                            items: ["Tất cả","Công bố","Chưa công bố"].map(
                                  (value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(
                                    value,
                                    style: TextStyle(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w400,
                                        color: ColorUtils.PRIMARY_COLOR),
                                  ),
                                );
                              },
                            ).toList(),
                            onChanged: (String? value) {
                              controller.isPublic.value = value!;
                              controller.getListExam(controller.setTypeExercise(controller.statusExercise.value),controller.setStatusPublic(controller.isPublic.value));
                            },
                          )),
                    ),
                  ),
                  controller.isReady.value?ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount:controller.itemsExam.length,
                      itemBuilder: (context, index) {
                        controller.getListItemPopupMenu(index);
                        return InkWell(
                          onTap: () {
                            checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> controller.clickItemListViewExam(index),StringConstant.FEATURE_EXAM_DETAIL);
                          },
                          child: Column(
                            children: [
                              Container(
                                  margin: const EdgeInsets.only(top: 16, bottom: 8),
                                  child:
                                  Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(bottom: 24.h),
                                        child:  Image.asset('assets/images/icon_subject.png',width: 24,height: 24,),
                                      ),
                                      const Padding(padding: EdgeInsets.only(right: 8)),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            width: 240.w,
                                            child: Text(
                                              '${controller.itemsExam[index].title}',
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily:
                                                  'assets/font/static/Inter-Medium.ttf'),
                                            ),
                                          ),
                                          const Padding(padding: EdgeInsets.only(top: 4)),
                                          RichText(
                                              text: TextSpan(children: [
                                                 TextSpan(
                                                    text: 'Thời gian bắt đầu: ',
                                                    style: TextStyle(
                                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 12.sp,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf')),
                                                TextSpan(
                                                    text: controller.outputDateFormatV2.format(DateTime.fromMicrosecondsSinceEpoch(controller.itemsExam[index].startTime!*1000)),
                                                    style: TextStyle(
                                                        color: const Color.fromRGBO(26, 26, 26, 1),
                                                        fontSize: 12.sp,
                                                        fontWeight: FontWeight.w500,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf'))
                                              ])),
                                          Padding(padding: EdgeInsets.only(top: 4.h)),
                                          RichText(
                                              text: TextSpan(children: [
                                                 TextSpan(
                                                    text: 'Thời gian kết thúc: ',
                                                    style: TextStyle(
                                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 12.sp,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf')),
                                                TextSpan(
                                                    text: controller.outputDateFormatV2.format(DateTime.fromMicrosecondsSinceEpoch(controller.itemsExam[index].endTime!*1000)),
                                                    style: TextStyle(
                                                        color: const Color.fromRGBO(26, 26, 26, 1),
                                                        fontSize: 12.sp,
                                                        fontWeight: FontWeight.w500,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf'))
                                              ])),
                                          Padding(padding: EdgeInsets.only(top: 4.h)),
                                          RichText(
                                              text: TextSpan(children: [
                                                TextSpan(
                                                    text: 'Loại bài tập: ',
                                                    style: TextStyle(
                                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 12.sp,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf')),
                                                TextSpan(
                                                    text: '${controller.getTypeExercise(controller.itemsExam[index].typeExercise)}',
                                                    style: TextStyle(
                                                        color: ColorUtils.PRIMARY_COLOR,
                                                        fontSize: 12.sp,
                                                        fontWeight: FontWeight.w500,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf')),
                                              ])),
                                          Padding(padding: EdgeInsets.only(top: 4.h)),
                                          RichText(
                                              text: TextSpan(children: [
                                                TextSpan(
                                                    text: 'Hạn nộp: ',
                                                    style: TextStyle(
                                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 12.sp,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf')),
                                                TextSpan(
                                                    text: '${controller.getStatusExercise(controller.itemsExam[index].status)}',
                                                    style: TextStyle(
                                                        color: ColorUtils.PRIMARY_COLOR,
                                                        fontSize: 12.sp,
                                                        fontWeight: FontWeight.w500,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf')),
                                              ])),
                                          Padding(padding: EdgeInsets.only(top: 4.h)),
                                          RichText(
                                              text: TextSpan(children: [
                                                TextSpan(
                                                    text: 'Trạng thái công bố: ',
                                                    style: TextStyle(
                                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 12.sp,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf')),
                                                TextSpan(
                                                    text: '${controller.getStatusPublic(controller.itemsExam[index].isPublic)}',
                                                    style: TextStyle(
                                                        color: controller.itemsExam[index].isPublic == "TRUE"? Colors.green:Colors.red,
                                                        fontSize: 12.sp,
                                                        fontWeight: FontWeight.w500,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf')),
                                              ])),

                                        ],
                                      ),
                                      const Spacer(),
                                      Visibility(
                                          visible: controller.listItemPopupMenu.isNotEmpty,
                                          child: Container(
                                            margin: EdgeInsets.only(
                                                bottom: 32.h),
                                            child: PopupMenuButton(
                                                padding: EdgeInsets.zero,
                                                shape: const RoundedRectangleBorder(
                                                    borderRadius:
                                                    BorderRadius.all(
                                                        Radius.circular(
                                                            6.0))),
                                                icon: const Icon(
                                                  Icons.more_vert_outlined,
                                                  color: Color.fromRGBO(
                                                      177, 177, 177, 1),
                                                ),
                                                itemBuilder: (context) {
                                                  return controller.getListItemPopupMenu(index);
                                                },
                                                onSelected: (value) {
                                                  switch (value) {
                                                    case 0:
                                                      Get.dialog(Center(
                                                        child: Wrap(
                                                            children: [
                                                              Container(
                                                                height:
                                                                220.h,
                                                                width: double
                                                                    .infinity,
                                                                decoration: BoxDecoration(
                                                                    color: Colors
                                                                        .transparent,
                                                                    borderRadius:
                                                                    BorderRadius.circular(16)),
                                                                child:
                                                                Stack(
                                                                  children: [
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          top: 40.h,
                                                                          right: 50.w,
                                                                          left: 50.w),
                                                                      height:
                                                                      180.h,
                                                                      decoration: BoxDecoration(
                                                                          color: Colors.white,
                                                                          borderRadius: BorderRadius.circular(16)),
                                                                      child:
                                                                      Column(
                                                                        children: [
                                                                          Padding(padding: EdgeInsets.only(top: 32.h)),
                                                                          Text(
                                                                            "Bạn có chắc muốn công bố bài kiểm tra này?",
                                                                            style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontSize: 14.sp),
                                                                          ),
                                                                          Container(
                                                                              color: Colors.white,
                                                                              padding: EdgeInsets.all(16.h),
                                                                              child: SizedBox(
                                                                                width: double.infinity,
                                                                                height: 46,
                                                                                child: ElevatedButton(
                                                                                    style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                                                                                    onPressed: () {
                                                                                      controller.publicExam(controller.itemsExam[index].id);
                                                                                    },
                                                                                    child: Text(
                                                                                      'Công bố',
                                                                                      style: TextStyle(color: Colors.white, fontSize: 16.sp),
                                                                                    )),
                                                                              )),
                                                                          TextButton(
                                                                              onPressed: () {
                                                                                Get.back();
                                                                              },
                                                                              child: Text(
                                                                                "Hủy",
                                                                                style: TextStyle(color: Colors.red, fontSize: 16.sp),
                                                                              ))
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                        alignment:
                                                                        Alignment.center,
                                                                        margin: const EdgeInsets.only(top: 10),
                                                                        height: 80,
                                                                        child: Image.asset("assets/images/image_app_logo.png"))
                                                                  ],
                                                                ),
                                                              ),
                                                            ]),
                                                      ));
                                                      break;
                                                    case 1:
                                                      if (controller.itemsExam[index].files!.isNotEmpty) {
                                                        Get.toNamed(Routes.updateExamUploadFilePage, arguments: controller.itemsExam[index]);
                                                      } else {
                                                        if (controller.itemsExam[index].questions!.isNotEmpty) {
                                                          Get.toNamed(Routes.updateExamQuestionPage, arguments: controller.itemsExam[index].id);
                                                        } else {
                                                          if (controller.itemsExam[index].link != "" && controller.itemsExam[index].link != null) {
                                                            Get.toNamed(Routes.updateExamAssignLinkPage, arguments: controller.itemsExam[index]);
                                                          }
                                                        }
                                                      }
                                                      break;
                                                    case 2:
                                                      Get.dialog(Center(
                                                        child: Wrap(
                                                            children: [
                                                              Container(
                                                                width: double
                                                                    .infinity,
                                                                decoration: BoxDecoration(
                                                                    color: Colors
                                                                        .transparent,
                                                                    borderRadius:
                                                                    BorderRadius.circular(16)),
                                                                child:
                                                                Stack(
                                                                  children: [
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          top: 40.h,
                                                                          right: 50.w,
                                                                          left: 50.w),
                                                                      decoration: BoxDecoration(
                                                                          color: Colors.white,
                                                                          borderRadius: BorderRadius.circular(16)),
                                                                      child:
                                                                      Column(
                                                                        children: [
                                                                          Padding(padding: EdgeInsets.only(top: 32.h)),
                                                                          Container(
                                                                            margin: EdgeInsets.symmetric(horizontal: 16.w),
                                                                            child: Text(
                                                                              "Bạn có chắc muốn xóa bài kiểm tra này?",
                                                                              style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontSize: 14.sp),
                                                                            ),
                                                                          ),
                                                                          Container(
                                                                              color: Colors.white,
                                                                              padding: EdgeInsets.all(16.h),
                                                                              child: SizedBox(
                                                                                width: double.infinity,
                                                                                height: 46,
                                                                                child: ElevatedButton(
                                                                                    style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                                                                                    onPressed: () {
                                                                                      controller.deleteExam(controller.itemsExam[index].id);
                                                                                      Get.back();
                                                                                    },
                                                                                    child: Text(
                                                                                      'Xóa',
                                                                                      style: TextStyle(color: Colors.white, fontSize: 16.sp),
                                                                                    )),
                                                                              )),
                                                                          TextButton(
                                                                              onPressed: () {
                                                                                Get.back();
                                                                              },
                                                                              child: Text(
                                                                                "Hủy",
                                                                                style: TextStyle(color: Colors.red, fontSize: 16.sp),
                                                                              ))
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                        alignment:
                                                                        Alignment.center,
                                                                        margin: const EdgeInsets.only(top: 10),
                                                                        height: 80,
                                                                        child: Image.asset("assets/images/image_app_logo.png"))
                                                                  ],
                                                                ),
                                                              ),
                                                            ]),
                                                      ));
                                                      break;
                                                    case 3:
                                                      // if (controller.itemsExam[index].files!.isNotEmpty) {
                                                      //   Get.toNamed(Routes.gradingExamUploadFilePage, arguments: controller.itemsExam[index]);
                                                      // } else {
                                                      //   if (controller.itemsExam[
                                                      //   index].questions!.isNotEmpty) {
                                                      //     Get.toNamed(Routes.gradingExamQuestionPage, arguments: controller.itemsExam[index]);
                                                      //   } else {
                                                      //     if (controller.itemsExam[index].link != "" && controller.itemsExam[index].link != null) {
                                                      //       Get.toNamed(Routes.gradingExamAssignLinkPage, arguments: controller.itemsExam[index]);
                                                      //     }
                                                      //   }
                                                      // }
                                                      controller.goToGradingExam(controller.itemsExam[index]);
                                                      break;
                                                    case 4:
                                                      controller.getListStudentSubmit(controller.itemsExam[index].id);
                                                      Get.dialog(Obx(() => Center(
                                                        child: Wrap(children: [
                                                          Container(
                                                            width: double.infinity,
                                                            decoration: BoxDecoration(
                                                                color: Colors.transparent,
                                                                borderRadius:
                                                                BorderRadius.circular(
                                                                    16)),
                                                            child: Stack(
                                                              children: [
                                                                Container(
                                                                  margin:  EdgeInsets
                                                                      .only(
                                                                      top: 40.h,
                                                                      right: 50.w,
                                                                      left: 50.w),
                                                                  decoration: BoxDecoration(
                                                                      color: Colors.white,
                                                                      borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                          16)),
                                                                  child: Column(
                                                                    children: [
                                                                      Padding(
                                                                          padding: EdgeInsets
                                                                              .only(
                                                                              top: 32
                                                                                  .h)),
                                                                      Container(
                                                                        margin: EdgeInsets.symmetric(horizontal: 16.w),
                                                                        child: Text("Có ${controller.studentSubmitted.where((element) => element.isGrade == "TRUE").toList().length} học sinh đã chấm điểm, ${controller.studentSubmitted.where((element) => element.isGrade == "FALSE").toList().length} học sinh chưa chấm điểm, ${controller.studentUnSubmitted.length} học sinh chưa nộp bài",
                                                                          style: TextStyle(
                                                                              color: const Color
                                                                                  .fromRGBO(
                                                                                  133,
                                                                                  133,
                                                                                  133,
                                                                                  1),
                                                                              fontSize:
                                                                              14.sp),
                                                                        ),
                                                                      ),
                                                                      Container(
                                                                          color: Colors
                                                                              .white,
                                                                          padding:
                                                                          EdgeInsets
                                                                              .all(16
                                                                              .h),
                                                                          child: SizedBox(
                                                                            width: double
                                                                                .infinity,
                                                                            height: 46,
                                                                            child: ElevatedButton(
                                                                                style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                                                                                onPressed: () {
                                                                                  controller.teacherPublicScore(controller.itemsExam[index].id);
                                                                                },
                                                                                child: Text(
                                                                                  'Công bố',
                                                                                  style: TextStyle(
                                                                                      color:
                                                                                      Colors.white,
                                                                                      fontSize: 16.sp),
                                                                                )),
                                                                          )),
                                                                      TextButton(
                                                                          onPressed: () {
                                                                            Get.back();
                                                                          },
                                                                          child: Text(
                                                                            "Hủy",
                                                                            style: TextStyle(
                                                                                color: Colors
                                                                                    .red,
                                                                                fontSize:
                                                                                16.sp),
                                                                          ))
                                                                    ],
                                                                  ),
                                                                ),
                                                                Container(
                                                                    alignment:
                                                                    Alignment.center,
                                                                    margin:
                                                                    const EdgeInsets
                                                                        .only(
                                                                        top: 10),
                                                                    height: 80,
                                                                    child: Image.asset(
                                                                        "assets/images/image_app_logo.png"))
                                                              ],
                                                            ),
                                                          ),
                                                        ]),
                                                      )));
                                                      break;
                                                    case 5:
                                                      Get.toNamed(Routes.listStudentExamPage,arguments: controller.itemsExam[index]);
                                                      break;
                                                  }
                                                }),
                                          )),
                                    ],
                                  )
                              ),
                              const Divider()
                            ],
                          ),
                        );
                      })
                      :const Center(
                    heightFactor: 1.0,
                    child: LoadingCustom(),
                  )
                ],
              )),
            ),
          ),
        ),
      ),
    );
  }

  selectDateTimeStart(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy')
                .parse(controller.controllerDateEnd.value.text)
                .millisecondsSinceEpoch) {
          controller.controllerDateStart.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian Từ ngày nhỏ hơn thời gian Đến ngày");
        }
      } else {
        controller.controllerDateStart.value.text = date;
      }
      controller.getListExam(controller.setTypeExercise(controller.statusExercise.value),controller.setStatusPublic(controller.isPublic.value));

    }
    
  }

  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy')
            .parse(controller.controllerDateStart.value.text)
            .millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch) {
          controller.controllerDateEnd.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian Đến ngày lớn hơn thời gian Từ ngày");
        }
      } else {
        controller.controllerDateEnd.value.text = date;
      }

    }
    controller.getListExam(controller.setTypeExercise(controller.statusExercise.value),controller.setStatusPublic(controller.isPublic.value));
    
  }
}

