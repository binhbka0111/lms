import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import 'dart:io';
import '../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../../../../data/model/common/req_file.dart';
import '../../../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../../../../data/repository/exams/exam_repo.dart';
import '../../../../../../../../../data/repository/file/file_repo.dart';
import '../../view_exam_controller.dart';

class UpdateExamUploadFileController extends GetxController{
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var detailExam = ItemsExam().obs;
  var filesUploadExam = <ResponseFileUpload>[].obs;
  var focusNameExercise = FocusNode();
  var controllerNameExam = TextEditingController();
  var focusDescribe = FocusNode();
  var controllerDescribe = TextEditingController();
  var controllerDateEnd = TextEditingController();
  var controllerTimeEnd = TextEditingController();
  var controllerDateStart = TextEditingController();
  var controllerTimeStart  = TextEditingController();
  final FileRepo fileRepo = FileRepo();
  final ExamRepo examRepo = ExamRepo();
  var files = <ReqFile>[].obs;
  var isShowErrorTextDateEnd= false;
  var isShowErrorTextTimeEnd= false;
  var isShowErrorTextDateStart= false;
  var isShowErrorTextTimeStart= false;
  var isShowErrorTextNameExam = false;
  var isShowErrorLink = false;
  var errorTextDateStart = "Vui lòng nhập ngày bắt đầu";
  var errorTextTimeStart = "Vui lòng nhập giờ bắt đầu";
  var errorTextDateEnd = "Vui lòng nhập ngày kết thúc";
  var errorTextTimeEnd = "Vui lòng nhập giờ kết thúc";
  var isShowErrorPoint = false;
  var errorTextPoint = "".obs;
  var focusPoint = FocusNode();
  var controllerPoint = TextEditingController();
  @override
  void onInit() {
    var detailItem = Get.arguments;
    if (detailItem != null) {
      detailExam.value = detailItem;
      filesUploadExam.value = detailExam.value.files!;
      controllerNameExam.text = detailExam.value.title!;
      controllerDescribe.text = detailExam.value.description!;
      controllerPoint.text = detailExam.value.scoreOfExam.toString();
      controllerDateEnd.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(detailExam.value.endTime!)).substring(0,10);
      controllerTimeEnd.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(detailExam.value.endTime!)).substring(11,16);
      controllerDateStart.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(detailExam.value.startTime!)).substring(0,10);
      controllerTimeStart.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(detailExam.value.startTime!)).substring(11,16);
    }
    super.onInit();
  }






  uploadFile(file) async {
    showDialogUploadFile();
    var fileList = <File>[];
    for (var element in file) {
      fileList.add(element.file!);
    }

    await fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;
        files.addAll(file);
        filesUploadExam.addAll(listResFile);
        filesUploadExam.refresh();
        listResFile.clear();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
    Get.back();
  }



  updateExam(exerciseId,title, startTime,endTime, description, typeExercise, subjectId, teacherId, classId, files,scoreOfExam) {
    examRepo.updateExamFile(exerciseId,title,startTime,endTime, description, typeExercise, subjectId, teacherId, classId, files,scoreOfExam).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật bài kiểm tra thành công");
        Get.back();
        Get.find<ViewExamController>().onInit();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Cập nhật bài kiểm tra thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


}