import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../../../../data/model/res/exercise/list_student_exercise.dart';
import '../../../../../../../../../data/repository/exams/exam_repo.dart';
import '../list_student_submitted_exam/list_student_submit_exam_controller.dart';
import '../list_student_un_submit_exam/list_student_un_submit_controller.dart';

class GradingExamQuestionController extends GetxController{
  var focusAttachLink = FocusNode();
  var controllerAttachLink = TextEditingController();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var detailExam = ItemsExam().obs;
  var listTitle = <String>[].obs;
  var listStudentExam = ListStudentSubmitted().obs;
  final ExamRepo examRepo = ExamRepo();
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  RxInt indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  @override
  void onInit() {
    var detailItem = Get.arguments;
    if (detailItem != null) {
      detailExam.value = detailItem;
      controllerAttachLink.text = detailExam.value.link!;
    }
    listTitle.value = ["Học sinh đã nộp","Học sinh chưa nộp"];
    super.onInit();
  }


  @override
  dispose() {
    pageController.dispose();
    super.dispose();
  }

  onPageViewChange(int page) {
    indexClick.value = page;
    if (page != 0) {
      indexClick.value - 1;
    } else {
      indexClick.value = 0;
    }
  }


  showColor(index) {
    click.value = [];
    for (int i = 0; i < 2; i++) {
      click.add(false);
    }
    click[index] = true;
  }


  getListStudent(index){
    switch(index){
      case 0:
        return  Get.find<ListStudentSubmitExamController>().studentSubmitted.length;
      case 1:
        return Get.find<ListStudentUnSubmitExamController>().studentUnSubmitted.length;
    }
  }




}