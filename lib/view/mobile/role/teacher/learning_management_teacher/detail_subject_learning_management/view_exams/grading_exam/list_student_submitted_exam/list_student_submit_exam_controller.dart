import 'dart:ui';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/routes/app_pages.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/res/exercise/list_student_exercise.dart';
import '../../../../../../../../../data/repository/exams/exam_repo.dart';
import '../grading_exam_assign_link/grading_exam_assign_link_controller.dart';
import '../grading_exam_question/grading_exam_question_controller.dart';
import '../grading_exam_upload_file/grading_exam_upload_file_controller.dart';


class ListStudentSubmitExamController extends GetxController{
  var listStudentExercise = ListStudentSubmitted().obs;
  var studentSubmitted = <StudentSubmitted>[].obs;
  var studentUnSubmitted = <StudentUnSubmitted>[].obs;
  final ExamRepo examRepo = ExamRepo();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy');
  var listIdStudent = <String>[].obs;
  @override
  void onInit() {
    if(Get.isRegistered<GradingExamAssignLinkController>()){
      getListStudentSubmit( Get.find<GradingExamAssignLinkController>().detailExam.value.id);
    }
    if(Get.isRegistered<GradingExamUploadFileController>()){
      getListStudentSubmit( Get.find<GradingExamUploadFileController>().detailExam.value.id);
    }
    if(Get.isRegistered<GradingExamQuestionController>()){
      getListStudentSubmit( Get.find<GradingExamQuestionController>().detailExam.value.id);
    }
    super.onInit();
  }


  getListStudentSubmit(exerciseId){
    examRepo.studentsSubmitted(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudentExercise.value = value.object!;
        studentSubmitted.value = listStudentExercise.value.studentSubmitted!;
        studentUnSubmitted.value = listStudentExercise.value.studentUnSubmitted!;
        for (int i = 0; i < studentSubmitted.length; i++) {
          listIdStudent.add(studentSubmitted[i].student!.id!);
        }
      }
    });
  }


  clickStudentDoExam(id){
    if(Get.isRegistered<GradingExamAssignLinkController>()){
      Get.toNamed(Routes.detailStudentDoExamAssignLinkPage,arguments: id);
    }
    if(Get.isRegistered<GradingExamUploadFileController>()){
      Get.toNamed(Routes.detailStudentDoExamUploadFilePage,arguments:id);
    }
    if(Get.isRegistered<GradingExamQuestionController>()){
      Get.toNamed(Routes.detailStudentDoExamQuestionPage,arguments: id);
    }
  }



  getColorTextStatus(status) {
    switch (status) {
      case "LATE_SUBMISSION":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "SUBMITTED":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "NOT_SUBMISSION":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "UNSUBMITTED":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "GRADED":
        return const Color.fromRGBO(77, 197, 145, 1);
    }
  }

  getColorTextIsGrade(status) {
    switch (status) {
      case "FALSE":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "TRUE":
        return const Color.fromRGBO(77, 197, 145, 1);
    }
  }
  getColorTextIsPublicScore(status) {
    switch (status) {
      case "FALSE":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "TRUE":
        return const Color.fromRGBO(77, 197, 145, 1);
    }
  }

  getColorBackgroundIsGrade(status) {
    switch (status) {
      case "FALSE":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "TRUE":
        return const Color.fromRGBO(192, 242, 220, 1);
    }
  }

  getColorBackgroundIsPublicScore(status) {
    switch (status) {
      case "FALSE":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "TRUE":
        return const Color.fromRGBO(192, 242, 220, 1);
    }
  }



  getColorBackgroundStatus(status) {
    switch (status) {
      case "LATE_SUBMISSION":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "SUBMITTED":
        return const Color.fromRGBO(192, 242, 220, 1);
      case "NOT_SUBMISSION":
        return const Color.fromRGBO(252, 211, 215, 1);
      case "UNSUBMITTED":
        return const Color.fromRGBO(252, 211, 215, 1);
      case "GRADED":
        return const Color.fromRGBO(192, 242, 220, 1);
    }
  }

  getTextIsGrade(status) {
    switch (status) {
      case "FALSE":
        return "Chưa chấm";
      case "TRUE":
        return "Đã chấm";
    }
  }

  getTextIsPublicScore(status) {
    switch (status) {
      case "FALSE":
        return "Chưa công bố";
      case "TRUE":
        return "Đã công bố";
    }
  }



  getStatus(status) {
    switch (status) {
      case "SUBMITTED":
        return "Đã nộp bài";
      case "LATE_SUBMISSION":
        return "Nộp bài muộn";
      case "NOT_SUBMISSION":
        return "Không nộp";
      case "UNSUBMITTED":
        return "Chưa nộp bài";
      case "GRADED ":
        return "Đã chấm điểm";
    }
  }

}