import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import '../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../learning_management_teacher_controller.dart';
import 'detail_exam_assign_link_controller.dart';

class DetailExamAssignLinkPage extends GetView<DetailExamAssignLinkController>{
  @override
  final controller = Get.put(DetailExamAssignLinkController());

  DetailExamAssignLinkPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(child: Scaffold(
      appBar:  AppBar(
        actions: [
          IconButton(
            onPressed: () {
              comeToHome();
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title:  Text(
          'Bài Kiểm Tra Môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}',
          style: const TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(16.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${controller.detailExam.value.title}",
              style: TextStyle(
                  color: const Color.fromRGBO(133, 133, 133, 1),
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w500),
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Row(
              children: [
                SizedBox(
                  width: 100.w,
                  child: Text(
                    "Ngày tạo",
                    style: TextStyle(
                        color: const Color.fromRGBO(133, 133, 133, 1),
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Text(
                  controller.outputDateFormat.format(
                      DateTime.fromMillisecondsSinceEpoch(controller
                          .detailExam.value.createdAt!)),
                  style: TextStyle(
                      color: const Color.fromRGBO(26, 26, 26, 1),
                      fontSize: 12.sp,
                      fontWeight: FontWeight.w400),
                ),
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Row(
              children: [
                SizedBox(
                  width: 100.w,
                  child: Text("Thời gian bắt đầu",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                ),
                Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExam.value.startTime!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Row(
              children: [
                SizedBox(
                  width: 100.w,
                  child: Text("Thời gian kết thúc",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                ),
                Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExam.value.endTime!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Row(
              children: [
                SizedBox(
                  width: 100.w,
                  child: Text(
                    "Ghi chú",
                    style: TextStyle(
                        color: const Color.fromRGBO(133, 133, 133, 1),
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                SizedBox(
                  width: 230.w,
                  child: Text(
                    "${controller.detailExam.value.description}",
                    style: TextStyle(
                        color: const Color.fromRGBO(26, 26, 26, 1),
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Row(
              children: [
                SizedBox(
                  width: 100.w,
                  child: Text("Điểm tối đa",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                ),
                Text("${controller.detailExam.value.scoreOfExam}",style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            FocusScope(
              node: FocusScopeNode(),
              child: Focus(
                  onFocusChange: (focus) {
                    controller.update();
                  },
                  child: Container(
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.all(2.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                        const BorderRadius.all(Radius.circular(6.0)),
                        border: Border.all(
                          width: 1,
                          style: BorderStyle.solid,
                          color: controller.focusAttachLink.hasFocus
                              ? ColorUtils.PRIMARY_COLOR
                              : const Color.fromRGBO(192, 192, 192, 1),
                        )),
                    child: TextFormField(
                      cursorColor: ColorUtils.PRIMARY_COLOR,
                      focusNode: controller.focusAttachLink,
                      onChanged: (value) {
                        controller.update();
                      },
                      onFieldSubmitted: (value) {
                        controller.update();
                      },
                      readOnly: true,
                      controller: controller.controllerAttachLink,
                      maxLines: null,
                      decoration: InputDecoration(
                        labelText: "Đường dẫn",
                        labelStyle: TextStyle(
                            fontSize: 14.0.sp,
                            color: const Color.fromRGBO(177, 177, 177, 1),
                            fontWeight: FontWeight.w500,
                            fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                        contentPadding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 16),
                        prefixIcon:  null,

                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        border: InputBorder.none,
                        errorStyle: const TextStyle(height: 0),
                        focusedErrorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        floatingLabelBehavior: FloatingLabelBehavior.auto,
                      ),
                    ),
                  )),
            ),
          ],
        ),
      ),
    ));
  }

}