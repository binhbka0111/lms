import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import 'dart:io';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/req_file.dart';
import '../../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../../../data/repository/file/file_repo.dart';
import '../../../../../../../../data/repository/notification_repo/notification_repo.dart';
import '../../learning_management_teacher_controller.dart';
import '../list_all_notify_subject_controller.dart';


class CreateNewNotifySubjectController extends GetxController{
  var createNewNotification = false.obs;
  var oldNotification = false.obs;
  var focusContentNotify = FocusNode();
  var controllerContentNotify = TextEditingController();
  var focusTitle= FocusNode();
  var controllerTitle = TextEditingController();
  var pinNotification = false.obs;
  var showFileUp = true.obs;
  var isPin = false.obs;
  var filesUploadExercise = <ResponseFileUpload>[].obs;
  var files = <ReqFile>[].obs;
  final FileRepo fileRepo = FileRepo();
  final NotificationRepo _notificationRepo = NotificationRepo();




  uploadFile(file) async {
    showDialogUploadFile();
    var fileList = <File>[];
    for (var element in file) {
      fileList.add(element.file!);
    }

   await fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;

        files.refresh();
        filesUploadExercise.addAll(listResFile);
        filesUploadExercise.refresh();
        files.addAll(file);
        for(int i =0;i<files.length;i++){
          files[i].url = filesUploadExercise[i].link;
        }
        listResFile.clear();
        update();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
    Get.back();
  }



  sendNotification(title,content,status,subjectId, files) {
    _notificationRepo.sendNotificationSubject(title,content,status,subjectId, files).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Gửi thông báo thành công");
        controllerTitle.text = "";
        controllerContentNotify.text = "";
        isPin.value = false;
        files = [];
        filesUploadExercise.value = [];
        Get.back();
        Get.find<LearningManagementTeacherController>().getListNotify();
        if(Get.isRegistered<ListAllNotifySubjectController>()){
          Get.find<ListAllNotifySubjectController>().onInit();
        }
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Gửi thông báo thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

  setStatusPin(status){
    switch(status){
      case true:
        return "PIN";
      case false:
        return "NOT_PIN";
    }
  }
}