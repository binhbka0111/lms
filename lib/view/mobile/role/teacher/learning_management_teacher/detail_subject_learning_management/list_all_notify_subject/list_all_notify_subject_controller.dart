import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import 'dart:io';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/req_file.dart';
import '../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../../data/model/res/notification/notification_subject.dart';
import '../../../../../../../data/repository/file/file_repo.dart';
import '../../../../../../../data/repository/notification_repo/notification_repo.dart';
import '../learning_management_teacher_controller.dart';

class ListAllNotifySubjectController extends GetxController{
  var createNewNotification = false.obs;
  var oldNotification = false.obs;
  var focusContentNotify = FocusNode();
  var controllerContentNotify = TextEditingController();
  var focusTitle= FocusNode();
  var controllerTitle = TextEditingController();
  var showFileUp = true.obs;
  var isPin = false.obs;
  var filesUploadExercise = <ResponseFileUpload>[].obs;
  var files = <ReqFile>[].obs;
  final FileRepo fileRepo = FileRepo();
  final NotificationRepo _notificationRepo = NotificationRepo();
  var notificationSubject = NotificationSubject().obs;
  var listNotificationSubject = <ItemsNotificationSubject>[].obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  RxBool isReady = false.obs;
  @override
  void onInit() {
    getListNotify();
    super.onInit();

  }




  uploadFile() async {
    showDialogUploadFile();
    var fileList = <File>[];
    for (var element in files) {
      fileList.add(element.file!);
    }

    await fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;
        filesUploadExercise.value = [];
        filesUploadExercise.addAll(listResFile);
        filesUploadExercise.refresh();
        listResFile.clear();

      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
    Get.back();
  }



  sendNotification(title,content,status,subjectId, files) {
    _notificationRepo.sendNotificationSubject(title,content,status,subjectId, files).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Gửi thông báo thành công");
        controllerTitle.text = "";
        controllerContentNotify.text = "";
        isPin.value = false;
        files = [];
        filesUploadExercise.value = [];
        Get.back();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Gửi thông báo thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

  setStatusPin(status){
    switch(status){
      case true:
        return "PIN";
      case false:
        return "NOT_PIN";
    }
  }


  getListNotify()async{
    await _notificationRepo.getListNotifySubject(Get.find<LearningManagementTeacherController>().detailSubject.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        notificationSubject.value = value.object!;
        listNotificationSubject.value =  notificationSubject.value.items!;
      }
    });
    isReady.value = true;
  }


  pinNotify(notifyId) {
    _notificationRepo.pinNotifySubject(notifyId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Ghim thông báo thành công");
        getListNotify();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Ghim thông báo thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


  unPinNotify(notifyId) {
    _notificationRepo.pinNotifySubject(notifyId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Bỏ ghim thông báo thành công");
        getListNotify();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Bỏ ghim thông báo thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

}