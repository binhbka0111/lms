import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/list_all_notify_subject/update_notify_subject/update_notify_subject_controller.dart';
import '../../../../../../../../commom/utils/ViewPdf.dart';
import '../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../commom/utils/file_device.dart';
import '../../../../../../../../commom/utils/global.dart';
import '../../../../../../../../commom/utils/open_url.dart';
import '../../learning_management_teacher_controller.dart';


class UpdateNotifySubjectPage
    extends GetWidget<UpdateNotifySubjectController> {
  @override
  final controller = Get.put(UpdateNotifySubjectController());

  UpdateNotifySubjectPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Scaffold(
        backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                comeToHome();
              },
              icon: const Icon(Icons.home),
              color: Colors.white,
            )
          ],
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title: Text(
            'Tạo thông báo',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontWeight: FontWeight.w500,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 16.w),
              child: Column(
                children: [
                  Padding(padding: EdgeInsets.only(top: 16.h)),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 24.h),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: GetBuilder<UpdateNotifySubjectController>(builder: (controller) {
                      return Column(
                        children: [
                          Row(
                            children: [
                              Expanded(child: Container(
                                color: Colors.transparent,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    FocusScope(
                                      node: FocusScopeNode(),
                                      child: Focus(
                                          onFocusChange: (focus) {
                                            controller.update();
                                          },
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            padding: const EdgeInsets.all(2.0),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                const BorderRadius.all(Radius.circular(6.0)),
                                                border: Border.all(
                                                  width: 1,
                                                  style: BorderStyle.solid,
                                                  color: controller.focusTitle.hasFocus
                                                      ? ColorUtils.PRIMARY_COLOR
                                                      : const Color.fromRGBO(192, 192, 192, 1),
                                                )),
                                            child: TextFormField(
                                              cursorColor: ColorUtils.PRIMARY_COLOR,
                                              focusNode: controller.focusTitle,
                                              onChanged: (value) {
                                                controller.update();
                                              },
                                              onFieldSubmitted: (value) {
                                                controller.update();
                                              },
                                              maxLines: null,
                                              controller: controller.controllerTitle,
                                              decoration: InputDecoration(
                                                labelText: "Tiêu đề",
                                                labelStyle: TextStyle(
                                                    fontSize: 14.0.sp,
                                                    color: const Color.fromRGBO(177, 177, 177, 1),
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                                                contentPadding: const EdgeInsets.symmetric(
                                                    vertical: 8, horizontal: 16),
                                                prefixIcon:  null,
                                                suffixIcon:  Visibility(
                                                    visible:
                                                    controller.controllerTitle.text.isNotEmpty ==
                                                        true,
                                                    child: InkWell(
                                                      onTap: () {
                                                        controller.controllerTitle.text = "";
                                                      },
                                                      child: const Icon(
                                                        Icons.close_outlined,
                                                        color: Colors.black,
                                                        size: 20,
                                                      ),
                                                    )),
                                                enabledBorder: InputBorder.none,
                                                errorBorder: InputBorder.none,
                                                border: InputBorder.none,
                                                errorStyle: const TextStyle(height: 0),
                                                focusedErrorBorder: InputBorder.none,
                                                disabledBorder: InputBorder.none,
                                                focusedBorder: InputBorder.none,
                                                floatingLabelBehavior: FloatingLabelBehavior.auto,
                                              ),
                                            ),
                                          )),
                                    ),
                                  ],
                                ),
                              )),
                              Padding(padding: EdgeInsets.only(right: 16.w)),
                              Visibility(
                                  visible: controller.isPin.value == true,
                                  child:  InkWell(
                                    onTap: () {
                                      controller.isPin.value = false;
                                      controller.update();
                                    },
                                    child: const Icon(Icons.push_pin,color: ColorUtils.PRIMARY_COLOR,size: 28,),
                                  )),
                              Visibility(
                                  visible: controller.isPin.value == false,
                                  child:  InkWell(
                                    onTap: () {
                                      controller.isPin.value = true;
                                      controller.update();
                                    },
                                    child: Image.asset("assets/images/icon_off_pin.png",height: 24.h,width: 24.w,color: const Color.fromRGBO(192, 192, 192, 1),),
                                  ))
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Container(
                            color: Colors.transparent,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                FocusScope(
                                  node: FocusScopeNode(),
                                  child: Focus(
                                      onFocusChange: (focus) {
                                        controller.update();
                                      },
                                      child: Container(
                                        alignment: Alignment.centerLeft,
                                        padding: const EdgeInsets.all(2.0),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                            const BorderRadius.all(Radius.circular(6.0)),
                                            border: Border.all(
                                              width: 1,
                                              style: BorderStyle.solid,
                                              color: controller.focusContentNotify.hasFocus
                                                  ? ColorUtils.PRIMARY_COLOR
                                                  : const Color.fromRGBO(192, 192, 192, 1),
                                            )),
                                        child: TextFormField(
                                          cursorColor: ColorUtils.PRIMARY_COLOR,
                                          focusNode: controller.focusContentNotify,
                                          onChanged: (value) {
                                            controller.update();
                                          },
                                          onFieldSubmitted: (value) {
                                            controller.update();
                                          },
                                          maxLines: null,
                                          controller: controller.controllerContentNotify,
                                          decoration: InputDecoration(
                                            labelText: "Nội dung",
                                            labelStyle: TextStyle(
                                                fontSize: 14.0.sp,
                                                color: const Color.fromRGBO(177, 177, 177, 1),
                                                fontWeight: FontWeight.w500,
                                                fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                                            contentPadding: const EdgeInsets.symmetric(
                                                vertical: 8, horizontal: 16),
                                            prefixIcon: null,
                                            suffixIcon:   Row(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Visibility(
                                                    visible:
                                                    controller.controllerContentNotify.text.isNotEmpty ==
                                                        true,
                                                    child: InkWell(
                                                      onTap: () {
                                                        controller.controllerContentNotify.text = "";
                                                      },
                                                      child: const Icon(
                                                        Icons.close_outlined,
                                                        color: Colors.black,
                                                        size: 20,
                                                      ),
                                                    )),
                                                const Padding(
                                                    padding: EdgeInsets.only(left: 4)),
                                                Row(
                                                  mainAxisSize: MainAxisSize.min,
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(top: 16.h),
                                                      child: SvgPicture.asset(
                                                        "assets/images/icon_resize.svg",
                                                        width: 14,
                                                        height: 14,
                                                        fit: BoxFit.scaleDown,
                                                        alignment: Alignment.bottomRight,
                                                        // color: widget.enable
                                                        //     ? (widget.focusNode.hasFocus
                                                        //         ? const Color.fromRGBO(26, 26, 26, 1)
                                                        //         : getColorState(widget.state))
                                                        //     : const Color.fromRGBO(177, 177, 177, 1),
                                                      ),
                                                    ),
                                                    const SizedBox(
                                                      width: 6,
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                            enabledBorder: InputBorder.none,
                                            errorBorder: InputBorder.none,
                                            border: InputBorder.none,
                                            errorStyle: const TextStyle(height: 0),
                                            focusedErrorBorder: InputBorder.none,
                                            disabledBorder: InputBorder.none,
                                            focusedBorder: InputBorder.none,
                                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                                          ),
                                        ),
                                      )),
                                ),
                              ],
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 16.h)),
                          InkWell(
                            onTap: () {
                              FileDevice.showSelectFileV2(Get.context!).then((value) {
                                if (value.isNotEmpty) {
                                  controller.files.addAll(value);
                                  controller.files.refresh();
                                  controller.uploadFile();
                                  controller.update();
                                }
                              });
                            },
                            child: Obx(() => DottedBorder(
                                dashPattern: const [5, 5],
                                radius: Radius.circular(6.r),
                                borderType: BorderType.RRect,
                                color: const Color.fromRGBO(192, 192, 192, 1),
                                child: SizedBox(
                                  width: double.infinity,
                                  child: controller.filesUploadExercise.isNotEmpty
                                      ? Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      ListView.builder(
                                          shrinkWrap: true,
                                          physics: const NeverScrollableScrollPhysics(),
                                          itemCount: controller.filesUploadExercise.length,
                                          itemBuilder: (context, index) {
                                            return InkWell(
                                              onTap: () {
                                                var action = 0;
                                                var extension = controller.filesUploadExercise[index].ext;
                                                if (extension == "png" ||
                                                    extension == "jpg" ||
                                                    extension == "jpeg" ||
                                                    extension == "gif" ||
                                                    extension == "bmp") {
                                                  action = 1;
                                                } else if (extension == "pdf") {
                                                  action = 2;
                                                } else {
                                                  action = 0;
                                                }
                                                switch (action) {
                                                  case 1:
                                                    OpenUrl.openImageViewer(context, controller.filesUploadExercise[index].link!);
                                                    break;
                                                  case 2:
                                                    Get.to(ViewPdfPage(url:controller.filesUploadExercise[index].link!));
                                                    break;
                                                  default:
                                                    OpenUrl.openFile(controller.filesUploadExercise[index].link!);
                                                    break;
                                                }
                                              },
                                              child: Container(
                                                margin: const EdgeInsets.only(top: 12),
                                                padding: const EdgeInsets.all(12),
                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(6),
                                                    color: const Color.fromRGBO(246, 246, 246, 1)),
                                                child: Row(
                                                  children: [
                                                    Image.network(
                                                      controller.filesUploadExercise[index].link!,
                                                      errorBuilder: (
                                                          BuildContext context,
                                                          Object error,
                                                          StackTrace? stackTrace,
                                                          ) {
                                                        return Image.asset(
                                                          getFileIcon(controller.filesUploadExercise[index].name),
                                                          height: 35,
                                                          width: 30,
                                                        );
                                                      },
                                                      height: 35,
                                                      width: 30,
                                                    ),
                                                    const Padding(padding: EdgeInsets.only(left: 8)),
                                                    SizedBox(
                                                      width: 220.w,
                                                      child: Text(
                                                        '${controller.filesUploadExercise[index].name}',
                                                        style: TextStyle(
                                                            color: const Color.fromRGBO(26, 59, 112, 1),
                                                            fontSize: 14.sp,
                                                            fontFamily: 'assets/font/static/Inter-Medium.ttf',
                                                            fontWeight: FontWeight.w500),
                                                      ),
                                                    ),
                                                    Expanded(child: Container()),
                                                    GestureDetector(
                                                        child: const Icon(
                                                          Icons.delete_outline_outlined,
                                                          color: ColorUtils.COLOR_WORK_TYPE_4,
                                                          size: 24,
                                                        ),
                                                        onTap: () {
                                                          controller.filesUploadExercise.removeAt(index);
                                                          controller.filesUploadExercise.refresh();
                                                        }),
                                                  ],
                                                ),
                                              ),
                                            );
                                          }),
                                      Padding(padding: EdgeInsets.only(top: 8.h)),
                                      TextButton(
                                          onPressed: () {
                                            FileDevice.showSelectFileV2(Get.context!)
                                                .then((value) {
                                              if (value.isNotEmpty) {
                                                controller.files.addAll(value);
                                                controller.files.refresh();
                                                controller.uploadFile();
                                                controller.update();
                                              }
                                            });
                                          },
                                          child: Text(
                                            "Thêm tệp",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    72, 98, 141, 1),
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w400),
                                          )),
                                    ],
                                  )
                                      : Column(
                                    children: [
                                      Padding(padding: EdgeInsets.only(top: 16.h)),
                                      const Icon(
                                        Icons.drive_folder_upload_rounded,
                                        color: ColorUtils.PRIMARY_COLOR,
                                        size: 40,
                                      ),
                                      Padding(padding: EdgeInsets.only(top: 4.h)),
                                      Text(
                                        "Tải lên Ảnh / Video",
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                133, 133, 133, 1),
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      Padding(padding: EdgeInsets.only(top: 12.h)),
                                      Text(
                                        "File (Video, Ảnh, Zip,...) có dung lượng không quá 10Mb",
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                133, 133, 133, 1),
                                            fontSize: 12.sp,
                                            fontWeight: FontWeight.w400),
                                      ),
                                      Padding(padding: EdgeInsets.only(bottom: 16.h)),
                                    ],
                                  ),
                                ))),
                          ),
                          Padding(padding: EdgeInsets.only(top: 16.h)),
                          Row(
                            children: [
                              Expanded(child: Container()),
                              SizedBox(
                                height: 30.h,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: controller
                                          .controllerTitle.value.text != ""||controller.controllerContentNotify.value.text != ""||controller.files.isNotEmpty
                                          ? ColorUtils.PRIMARY_COLOR
                                          : const Color.fromRGBO(
                                          246, 246, 246, 1),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                          BorderRadius.circular(
                                              6))),
                                  onPressed: () {
                                    if(controller.controllerTitle.text ==""){

                                    }else{
                                      if(controller.controllerContentNotify.text == "" ){

                                      }else{
                                        controller.updateNotification(
                                            controller.detailNotificationSubject.value.id,
                                            controller.controllerTitle.value.text
                                            , controller.controllerContentNotify.value.text
                                            , controller.setStatusPin(controller.isPin.value)
                                            , Get.find<LearningManagementTeacherController>().detailSubject.value.id
                                            , controller.filesUploadExercise);

                                      }
                                    }
                                  },
                                  child: Text(
                                    "Chỉnh sửa",
                                    style: TextStyle(
                                        color: controller
                                            .controllerTitle
                                            .value
                                            .text !=
                                            ""||controller.controllerContentNotify.value.text != ""||controller.files.isNotEmpty
                                            ? Colors.white
                                            : Colors.grey,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12.sp),
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      );
                    },),
                  ),
                ],
              ),
            )),
      ),
    );
  }



}
