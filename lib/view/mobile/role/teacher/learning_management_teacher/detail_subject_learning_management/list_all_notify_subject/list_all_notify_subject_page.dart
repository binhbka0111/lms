import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:html/parser.dart';
import 'package:slova_lms/commom/widget/list_view_image.dart';
import 'package:slova_lms/commom/widget/loading_custom.dart';
import '../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../routes/app_pages.dart';
import '../../../../../home/home_controller.dart';
import 'list_all_notify_subject_controller.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';

class ListAllNotifySubjectPage extends GetView<ListAllNotifySubjectController> {
  @override
  final controller = Get.put(ListAllNotifySubjectController(),);

  ListAllNotifySubjectPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                comeToHome();
              },
              icon: const Icon(Icons.home),
              color: Colors.white,
            )
          ],
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title: Text(
            'Quản lý thông báo',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontWeight: FontWeight.w500,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body: Obx(() => controller.isReady.value?RefreshIndicator(
            color: ColorUtils.PRIMARY_COLOR,
            child: Column(
              children: [
                Expanded(child: SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.all(16.h),
                    child: GetX<ListAllNotifySubjectController>(
                      init : ListAllNotifySubjectController(),
                      builder: (controller) {

                        return Visibility(
                            visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_LIST_NOTIFY),
                            child: ListView.builder(
                                itemCount: controller.listNotificationSubject.length,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  var listImage = controller.listNotificationSubject[index].files!.where((element) => element.ext == "png" ||
                                      element.ext == "jpg" ||
                                      element.ext == "jpeg" ||
                                      element.ext == "gif" ||
                                      element.ext == "bmp").toList().obs;
                                  var listNotImage = controller
                                      .listNotificationSubject[index].files!
                                      .where((element) =>
                                  element.ext != "png" &&
                                      element.ext != "jpg" &&
                                      element.ext != "jpeg" &&
                                      element.ext != "gif" &&
                                      element.ext != "bmp")
                                      .toList().obs;

                                  return Obx(() => Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 330.w,
                                        child: Text(
                                          "Thông Báo: ${controller.listNotificationSubject[index].title}",
                                          style: TextStyle(
                                              color: ColorUtils.PRIMARY_COLOR,
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                      Padding(padding: EdgeInsets.only(top: 8.h)),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Thời gian tạo: ",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(133, 133, 133, 1),
                                                fontWeight: FontWeight.w400,
                                                fontSize: 14.sp),
                                          ),
                                          Text(
                                            controller.outputDateFormat.format(
                                                DateTime.fromMillisecondsSinceEpoch(controller
                                                    .listNotificationSubject[index]
                                                    .createdAt!)),
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ],
                                      ),
                                      Padding(padding: EdgeInsets.only(top: 8.h)),
                                      Obx(() => Text(
                                        "${parse(parse(controller.listNotificationSubject[index].content).body?.text).documentElement?.text}",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14.sp),
                                      )),
                                      Row(
                                        children: [
                                          Expanded(child: Container()),
                                          Visibility(
                                              visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_NOTIFY_EDIT),
                                              child: InkWell(
                                                onTap: () {

                                                  Get.toNamed(Routes.updateNotifySubjectPage,arguments: controller.listNotificationSubject[index]);
                                                },
                                                child: Row(
                                                  children: [
                                                    Text("Sửa thông báo",
                                                        style: TextStyle(
                                                            color: const Color.fromRGBO(
                                                                26, 59, 112, 1),
                                                            fontSize: 12.sp,
                                                            fontWeight: FontWeight.w400)),
                                                    Padding(padding: EdgeInsets.only(right: 4.w)),
                                                    Image.asset(
                                                      "assets/images/icon_edit_notification.png",
                                                      width: 16,
                                                      height: 16,
                                                    )
                                                  ],
                                                ),
                                              )),
                                          Padding(padding: EdgeInsets.only(right: 16.w)),
                                          Visibility(
                                              visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_NOTIFY_PIN),
                                              child: Visibility(
                                                  visible: controller.listNotificationSubject[index].status == "NOT_PIN",
                                                  child: InkWell(
                                                    onTap: () {
                                                      controller.pinNotify(controller.listNotificationSubject[index].id);
                                                    },
                                                    child: Row(
                                                      children: [
                                                        Text("Ghim thông báo",
                                                            style: TextStyle(
                                                                color: ColorUtils.PRIMARY_COLOR,
                                                                fontSize: 12.sp,
                                                                fontWeight: FontWeight.w400)),
                                                        Image.asset(
                                                          "assets/images/icon_pin_notification.png",
                                                          width: 16,
                                                          height: 16,
                                                        )
                                                      ],
                                                    ),
                                                  ))),
                                          Visibility(
                                              visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_NOTIFY_PIN),
                                              child: Visibility(
                                                  visible: controller.listNotificationSubject[index].status == "PIN",
                                                  child: InkWell(
                                                    onTap: () {
                                                      controller.unPinNotify(controller.listNotificationSubject[index].id);
                                                    },
                                                    child:Text("Bỏ Ghim",
                                                        style: TextStyle(
                                                            color: Colors.red,
                                                            fontSize: 12.sp,
                                                            fontWeight: FontWeight.w400)),
                                                  )))
                                        ],
                                      ),
                                      Padding(padding: EdgeInsets.only(top: 8.h)),
                                      Obx(() => ListViewShowImage.showGridviewImage(listImage)),
                                      Obx(() => ListViewShowImage.showListViewNotImage(listNotImage)),
                                      Padding(padding: EdgeInsets.only(top: 8.h)),
                                      const Divider()
                                    ],
                                  ));
                                }));
                      },),
                  ),
                )),
                Visibility(
                    visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_NOTIFY_CREATE),
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 16.w,vertical: 8.h),
                      child: SizedBox(
                        width: double.infinity,
                        height: 48,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                side: const BorderSide(
                                    width: 1,
                                    color: ColorUtils.PRIMARY_COLOR),
                                backgroundColor:
                                const Color.fromRGBO(
                                    255, 255, 255, 1)),
                            onPressed: () {
                              Get.toNamed(Routes.createNewNotifySubjectPage);
                            },
                            child: const Text(
                              'Tạo thông báo mới',
                              style: TextStyle(
                                  color: ColorUtils.PRIMARY_COLOR,
                                  fontSize: 16),
                            )),
                      ),
                    )),
                SizedBox(height: 16.h,)
              ],
            ), onRefresh: ()async{
          controller.getListNotify();
        }):const LoadingCustom()),
      ),
    );
  }
}
