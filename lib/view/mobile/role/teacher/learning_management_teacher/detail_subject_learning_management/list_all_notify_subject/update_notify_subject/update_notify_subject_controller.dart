import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:html/parser.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import 'dart:io';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/req_file.dart';
import '../../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../../../data/model/res/notification/notification_subject.dart';
import '../../../../../../../../data/repository/file/file_repo.dart';
import '../../../../../../../../data/repository/notification_repo/notification_repo.dart';
import '../../learning_management_teacher_controller.dart';
import '../list_all_notify_subject_controller.dart';


class UpdateNotifySubjectController extends GetxController{
  var createNewNotification = false.obs;
  var oldNotification = false.obs;
  var focusContentNotify = FocusNode();
  var controllerContentNotify = TextEditingController();
  var focusTitle= FocusNode();
  var controllerTitle = TextEditingController();
  var showFileUp = true.obs;
  var isPin = false.obs;
  var filesUploadExercise = <ResponseFileUpload>[].obs;
  var files = <ReqFile>[].obs;
  final FileRepo fileRepo = FileRepo();
  final NotificationRepo _notificationRepo = NotificationRepo();
  var detailNotificationSubject = ItemsNotificationSubject().obs;
  var listNotificationSubject = <ItemsNotificationSubject>[].obs;
  @override
  void onInit() {
    var detailItem = Get.arguments;
    if (detailItem != null) {
      detailNotificationSubject.value = detailItem;
      filesUploadExercise.value = detailNotificationSubject.value.files!;
      controllerContentNotify.text = parse(parse(detailNotificationSubject.value.content!).body?.text).documentElement!.text;
      controllerTitle.text = detailNotificationSubject.value.title!;
      isPin.value = getStatusPin(detailNotificationSubject.value.status);
    }
    super.onInit();

  }



  getStatusPin(status){
    switch(status){
      case "PIN":
        return true;
      case "NOT_PIN":
        return false;
    }
  }




  uploadFile() async {
    showDialogUploadFile();
    var fileList = <File>[];
    for (var element in files) {
      fileList.add(element.file!);
    }

    await  fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;
        filesUploadExercise.addAll(listResFile);
        filesUploadExercise.refresh();
        listResFile.clear();
        files.value = [];
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
    Get.back();
  }



  updateNotification(notifyId,title,content,status,subjectId, files) {
    _notificationRepo.updateNotificationSubject(notifyId,title,content,status,subjectId, files).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Chỉnh sửa thông báo thành công");
        Get.back(result:  listNotificationSubject);
        Get.find<ListAllNotifySubjectController>().onInit();
        Get.find<LearningManagementTeacherController>().getListNotify();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Chỉnh sửa thông báo thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

  getlistNotify(){
    _notificationRepo.getListNotifySubject(Get.find<LearningManagementTeacherController>().detailSubject.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        listNotificationSubject.value =  value.object!.items!;
      }
    });
  }

  setStatusPin(status){
    switch(status){
      case true:
        return "PIN";
      case false:
        return "NOT_PIN";
    }
  }
}