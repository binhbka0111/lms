import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/res/transcript/detail_transcript.dart';
import '../../../../../../../../data/repository/transcript_repo/transcript_repo.dart';
import '../list_transcripts_teacher_controller.dart';

class UpdateTranscriptTeacherController extends GetxController{
  final TranscriptsRepo _transcriptsRepo = TranscriptsRepo();
  var detailTranscript = DetailTranscript().obs;
  var transcriptId = "".obs;
  var listValue = <dynamic>[].obs;
  var isReady = false.obs;
  var isReady2 = false.obs;
  var listPointView= <PointTranscript>[].obs;
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy');
  var listTextEdit = <dynamic>[].obs;
  var scoreFactor = "".obs;
  var scoreKey = "".obs;
  var valueScoreFactor = "".obs;
  var listIndexScore = <dynamic>[].obs;
  var listOrderScoreFactor = <String>[].obs;
  var listNameScoreFactor =<String>[].obs;
  var indexOfScoreFactor = 0.obs;
  var indexOfScore = 0.obs;
  double totalWidth = 0;

  @override
  void onInit() {
    var tmpTranscriptId = Get.arguments;
    if (tmpTranscriptId != null) {
      transcriptId.value = tmpTranscriptId;
    }
    getDetailTranscript(transcriptId.value);
    super.onInit();
  }



  getDetailTranscript(transcriptId) {
    _transcriptsRepo.transcriptInScreenUpdate(transcriptId).then((value) {
      try{
        var listPoint = <dynamic>[];
        for(int i = 0;i<listValue[0].length;i++){
          if(listValue[0][i].key!="birthday" &&listValue[0][i].key!="order"
             &&listValue[0][i].key!="fullName" &&listValue[0][i].key!="sex"
              &&listValue[0][i].key!="description") {
              listPointView.add(PointTranscript(key: listValue[0][i].key,
                point: listValue[0][i].point,
                title: detailTranscript.value.tableHeaders?[i].title));
              listPoint.add(listValue[0][i].point);
              listNameScoreFactor.add(listValue[0][i].key);
          }

        }
        scoreKey.value = listPointView[0].key!;
        scoreFactor.value = listPointView[0].title!;

        for(int i = 0;i<listPoint.length;i++){
          var list = <String>[];
          for(int j =0;j<listPoint[i].length;j++){
            list.add("Điểm thứ ${j+1}");
          }
          listIndexScore.add(list);
        }
        listOrderScoreFactor.value = listIndexScore[0];
        valueScoreFactor.value = listIndexScore[0][0];
        setWidthTranscrip();
        for(int i = 0;i<detailTranscript.value.tableRows!.length;i++){
          var list = <dynamic>[];
          for(int j = 0;j< detailTranscript.value.tableHeaders!.length;j++){
            var list1 = <TextEditingController>[];
            if(listValue[i][j].key!="birthday"
                &&listValue[i][j].key!="order"
                &&listValue[i][j].key!="fullName"
                &&listValue[i][j].key!="sex"
                &&listValue[i][j].key!="description"){
              for(int k = 0;k<listValue[i][j].point.length;k++){
                list1.add(TextEditingController());
              }
            }
            list.add(list1);
          }
          listTextEdit.add(list);
        }

        for(int i = 0;i<detailTranscript.value.tableRows!.length;i++){
          for(int j = 0;j< detailTranscript.value.tableHeaders!.length;j++){
            if(listValue[i][j].key!="birthday"
                &&listValue[i][j].key!="order"
                &&listValue[i][j].key!="fullName"
                &&listValue[i][j].key!="sex"
                &&listValue[i][j].key!="description"){
              for(int k = 0;k<listValue[i][j].point.length;k++){
                listTextEdit[i][j][k].text = listValue[i][j].point[k];
              }
            }

          }

        }

        listTextEdit.refresh();
        isReady.value = true;

      }catch(e){
        print('lỗi');
      }

    });



  }


  updateTranscript() {
    _transcriptsRepo.updateTranscript(transcriptId.value,
        detailTranscript.value.semester?.id,
        detailTranscript.value,
        listValue).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật bảng điểm thành công");
        Get.back();
        Get.find<ListTranscriptsSubjectTeacherController>().onInit();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Cập nhật bảng điểm thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }

  setWidthTranscrip(){
    totalWidth = 0;
    for (int i = 0; i < listValue[0].length; i++) {
      totalWidth += setWidthHeader(listValue[0][i].key, listValue[0][i].point);
    }
    totalWidth = totalWidth+20.w;
  }

  setWidthHeader(key, point) {
    switch (key) {
      case "order":
        return 50.w;
      case "birthday":
        return 0.w;
      case "sex":
        return 0.w;
      case "description":
        return 0.w;
      case "fullName":
        return 120.w;
      default:
        return setVisible(key)?150.w:0.w;
    }
  }

  setTexValue(key, value) {
    switch (key) {
      case "birthday":
        return outputDateFormatV2
            .format(DateTime.fromMillisecondsSinceEpoch(value ?? 0))
            .toString();
      case "order":
        return value.toString();
      case "fullName":
        return value ?? "";
      case "sex":
        return value ?? "";
      case "description":
        return value ?? "";
      default:
        return "  $value  ";
    }
  }
  setVisible(key){
    if(key == scoreKey.value){
      return true;
    }else if(key == "order"){
      return true;
    }
    else if(key == "fullName"){
      return true;
    }else{
      return false;
    }
  }


  setText(key, value,index,indexDetail) {
    switch (key) {
      case "birthday":
        return Container(
          alignment: Alignment.center,
          child: Text(setTexValue(key, value), textAlign: TextAlign.center),
        );
      case "order":
        return Container(
          alignment: Alignment.center,
          child: Text(setTexValue(key, value), textAlign: TextAlign.center),
        );
      case "fullName":
        return Container(
          alignment: Alignment.center,
          child: Text(setTexValue(key, value), textAlign: TextAlign.center),
        );
      case "sex":
        return Container(
          alignment: Alignment.center,
          child: Text(setTexValue(key, value), textAlign: TextAlign.center),
        );
      case "description":
        return InkWell(
          onTap: () {
            Get.dialog(Center(
              child: Wrap(children: [
                Container(
                  height: 220.h,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(16)),
                  child: Stack(
                    children: [
                      Container(
                        width: double.infinity,
                        margin:
                        EdgeInsets.only(top: 40.h, right: 50.w, left: 50.w),
                        height: 180.h,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(16)),
                        child: Center(
                          child: Text(setTexValue(key, value),
                              textAlign: TextAlign.center),
                        ),
                      ),
                      Container(
                          alignment: Alignment.center,
                          margin: const EdgeInsets.only(top: 10),
                          height: 80,
                          child:
                          Image.asset("assets/images/image_app_logo.png"))
                    ],
                  ),
                ),
              ]),
            ));
          },
          child: const Icon(Icons.note_add_rounded),
        );
      default:
        return Container(
          alignment: Alignment.center,
          height: 30.h,
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: value.length,
            itemBuilder: (context, i) {
              return indexOfScore.value == i?Container(
                height: 30.h,
                width: 45.h,
                margin: EdgeInsets.only(bottom: 8.h,left: 4.w,right: 4.w),
                padding: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  border: Border.all(
                      color: const Color.fromRGBO(133, 133, 133, 1)),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: TextFormField(
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(4),
                    FilteringTextInputFormatter.allow(RegExp("[0-9a-zA-Z.]")),
                  ],
                  controller: listTextEdit[index][indexDetail][i],
                  cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      color:  Color.fromRGBO(248, 129, 37, 1)
                  ),
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                  ),
                ),
              ):Container();
            },
          ),
        );
    }
  }




  setUnderlineText(key){
    switch(key){
      case "birthday":
        return  TextDecoration.none;
      case "order":
        return  TextDecoration.none;
      case "fullName":
        return  TextDecoration.none;
      case "sex":
        return  TextDecoration.none;
      case "description":
        return  TextDecoration.none;
      default:
        return TextDecoration.underline;
    }
  }
}