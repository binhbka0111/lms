import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_transcripts_teacher/update_transcript_subject/update_transcript_teacher_controller.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';
import '../../../../../../../../data/model/res/transcript/detail_transcript.dart';
import '../../../../teacher_home_controller.dart';
import '../../learning_management_teacher_controller.dart';


class UpdateTranscriptTeacherPage extends GetWidget<UpdateTranscriptTeacherController>{
  @override
  final controller = Get.put(UpdateTranscriptTeacherController());

  UpdateTranscriptTeacherPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromRGBO(246, 246, 246, 1),
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                comeToHome();
              },
              icon: const Icon(Icons.home),
              color: Colors.white,
            )
          ],
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title: const Text(
            'Cập nhật bảng điểm',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body: Obx(() => controller.isReady.value?Column(
          children: [
            Expanded(child: Container(
              margin: EdgeInsets.only(top: 16.h,left: 16.w,right: 16.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("${Get.find<TeacherHomeController>().currentClass.value.name}",
                    style: TextStyle(fontSize: 12.sp,fontWeight: FontWeight.w500,
                        color: const Color.fromRGBO(133, 133, 133, 1)),),
                  SizedBox(
                    height: 8.h,
                  ),
                  Container(
                    padding: EdgeInsets.all(16.h),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6.r),
                        color: Colors.white
                    ),
                    child: IntrinsicHeight(
                      child: Row(
                        children: [
                          Expanded(
                              flex: 1,
                              child: IntrinsicWidth(
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                                  margin: EdgeInsets.only(right: 4.w),
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          color: const Color.fromRGBO(192, 192, 192, 1)),
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                      canvasColor: Colors.white,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        isExpanded: true,
                                        iconSize: 0,
                                        icon: const Visibility(
                                            visible: false,
                                            child: Icon(Icons.arrow_downward)),
                                        elevation: 16,
                                        hint: controller.scoreFactor.value != ""
                                            ? Row(
                                          children: [
                                            Text(
                                              controller.scoreFactor.value,
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: ColorUtils.PRIMARY_COLOR),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        )
                                            : Row(
                                          children: [
                                            Text(
                                              'Hệ số điểm',
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.black),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        ),
                                        items: controller.listPointView.map(
                                              (value) {
                                            return DropdownMenuItem<PointTranscript>(
                                              value: value,
                                              child: Text(
                                                value.title!,
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: ColorUtils.PRIMARY_COLOR),
                                              ),
                                            );
                                          },
                                        ).toList(),
                                        onChanged: (PointTranscript? value) {
                                          controller.scoreFactor.value = value!.title!;
                                          controller.scoreKey.value = value.key!;
                                          controller.indexOfScoreFactor.value = controller.listNameScoreFactor.indexOf(value.key);
                                          controller.listOrderScoreFactor.value = [];
                                          controller.listOrderScoreFactor.value = controller.listIndexScore[controller.indexOfScoreFactor.value];
                                          controller.listOrderScoreFactor.refresh();
                                          controller.indexOfScore.value = 0;
                                          controller.valueScoreFactor.value = controller.listIndexScore[controller.indexOfScoreFactor.value][0];
                                          controller.setWidthTranscrip();
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              )),
                          Expanded(
                              flex: 1,
                              child:  IntrinsicWidth(
                                child: Container(
                                  margin: EdgeInsets.only(left: 4.w),
                                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          color: const Color.fromRGBO(192, 192, 192, 1)),
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                      canvasColor: Colors.white,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        isExpanded: true,
                                        iconSize: 0,
                                        icon: const Visibility(
                                            visible: false,
                                            child: Icon(Icons.arrow_downward)),
                                        elevation: 16,
                                        hint: controller.valueScoreFactor.value != ""
                                            ? Row(
                                          children: [
                                            Text(
                                              controller.valueScoreFactor.value,
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: ColorUtils.PRIMARY_COLOR),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        )
                                            : Row(
                                          children: [
                                            Text(
                                              'Điểm thứ',
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.black),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        ),
                                        items: controller.listOrderScoreFactor.map(
                                              (value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(
                                                value,
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: ColorUtils.PRIMARY_COLOR),
                                              ),
                                            );
                                          },
                                        ).toList(),
                                        onChanged: (String? value) {
                                          controller.valueScoreFactor.value = value!;
                                          controller.indexOfScore.value = controller.listOrderScoreFactor.indexOf(value);
                                          controller.listOrderScoreFactor.refresh();
                                          controller.listValue.refresh();
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ))
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  RichText(
                      text:
                      TextSpan(children: [
                        TextSpan(
                            text:
                            "Nhập Điểm ",
                            style: TextStyle(
                                color: const Color.fromRGBO(
                                    133, 133, 133, 1),
                                fontSize: 12.sp,
                                fontWeight:
                                FontWeight.w500)),
                        TextSpan(
                            text:
                            "Môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory!.name}",
                            style: TextStyle(
                                color: const Color.fromRGBO(
                                    248, 126, 47, 1),
                                fontSize: 12.sp,
                                fontWeight:
                                FontWeight.w500)),

                      ])),
                  SizedBox(
                    height: 8.h,
                  ),
                  Expanded(
                      child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: IntrinsicWidth(
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              color: Colors.white,
                              padding: EdgeInsets.all(8.h),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    height: 30.h,
                                    alignment: Alignment.centerLeft,
                                    padding: EdgeInsets.only(top: 8.h),
                                    decoration: BoxDecoration(
                                        color: const Color.fromRGBO(235, 235, 235, 1),
                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(8.r),topRight: Radius.circular(8.r))
                                    ),
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: controller.detailTranscript.value.tableHeaders?.length,
                                      physics: const NeverScrollableScrollPhysics(),
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context, index) {
                                        return controller.setVisible(controller.detailTranscript.value.tableHeaders?[index].key)?SizedBox(
                                          width: controller.setWidthHeader(controller.detailTranscript.value.tableHeaders?[index].key,controller.listValue[0][index].point),
                                          child: Text(controller.detailTranscript.value.tableHeaders![index].title!,textAlign: TextAlign.center),
                                        ):Container();
                                      },),
                                  ),
                                  Expanded(child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8.r),bottomRight: Radius.circular(8.r))
                                    ),
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: controller.listValue.length,
                                      itemBuilder: (context, index) {
                                        return IntrinsicWidth(
                                          child: Container(
                                              margin: EdgeInsets.symmetric(vertical: 8.h),
                                              child: Container(
                                                decoration: const BoxDecoration(
                                                    border: Border(bottom: BorderSide(color: Colors.black26))),
                                                child: SizedBox(
                                                  height: 40.h,
                                                  child: ListView.builder(
                                                    shrinkWrap: true,
                                                    itemCount: controller.listValue[index].length,
                                                    scrollDirection: Axis.horizontal,
                                                    physics: const NeverScrollableScrollPhysics(),
                                                    itemBuilder: (context, indexDetail) {
                                                      return  Obx(() => controller.setVisible(controller.listValue[index][indexDetail].key)
                                                          ?SizedBox(
                                                          width: controller.setWidthHeader(controller.listValue[index][indexDetail].key,
                                                              controller.listValue[index][indexDetail].point),
                                                          child: Visibility(
                                                              visible: controller.listValue[index][indexDetail].point != null,
                                                              child:  controller.setText(controller.listValue[index][indexDetail].key,
                                                                  controller.listValue[index][indexDetail].point,
                                                                  index,indexDetail)))
                                                          :Container());
                                                    },),
                                                ),
                                              )
                                          ),
                                        );
                                      },),
                                  ))
                                ],
                              ),
                            ),
                          )))
                ],
              ),
            )),
            Container(
              color: const Color.fromRGBO(255, 255, 255, 1),
              height: 78,
              padding: const EdgeInsets.all(16),
              child: SizedBox(
                width: double.infinity,
                height: 46,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: ColorUtils.PRIMARY_COLOR,
                    ),
                    onPressed: () {
                      for(int i = 0;i<controller.detailTranscript.value.tableRows!.length;i++){
                        for(int j = 0;j< controller.detailTranscript.value.tableHeaders!.length;j++){
                          if(controller.listValue[i][j].key!="birthday"
                              &&controller.listValue[i][j].key!="order"
                              &&controller.listValue[i][j].key!="fullName"
                              &&controller.listValue[i][j].key!="sex"
                              &&controller.listValue[i][j].key!="description"){
                            for(int k = 0;k<controller.listValue[i][j].point.length;k++){
                              controller.listValue[i][j].point[k] = controller.listTextEdit[i][j][k].text;
                            }
                          }
                        }
                      }



                      controller.updateTranscript();
                    },
                    child: const Text(
                      'Lưu điểm',
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 16),
                    )),
              ),
            )
          ],
        ):Container())
    );
  }




}