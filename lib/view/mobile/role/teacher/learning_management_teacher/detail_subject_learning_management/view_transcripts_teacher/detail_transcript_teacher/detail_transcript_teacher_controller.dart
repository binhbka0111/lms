import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import '../../../../../../../../data/model/res/transcript/detail_transcript.dart';
import '../../../../../../../../data/repository/transcript_repo/transcript_repo.dart';

class DetailTranscriptSubjectController extends GetxController {
  final TranscriptsRepo _transcriptsRepo = TranscriptsRepo();
  var detailTranscript = DetailTranscript().obs;
  var transcriptId = "".obs;
  var listValue = <dynamic>[].obs;
  var isReady = false.obs;
  var listPointTranscript = <PointTranscript>[].obs;
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy');
  var listTextEdit = <dynamic>[].obs;
  @override
  void onInit() {
    var tmpTranscriptId = Get.arguments;
    if (tmpTranscriptId != null) {
      transcriptId.value = tmpTranscriptId;
    }
    getDetailTranscript(transcriptId.value);

    super.onInit();
  }

  getDetailTranscript(transcriptId) {
    _transcriptsRepo.detailTranscript(transcriptId).then((value) {
      isReady.value = true;
    });
  }


  totalWidth() {
    num totalWidth = 0;
    for (int i = 0; i < listValue[0].length; i++) {
      totalWidth += setWidthHeader(
          detailTranscript.value.tableHeaders![i].key, listValue[0][i].point);
    }
    totalWidth = totalWidth+20.w;
    return totalWidth;
  }

  setWidthHeader(title, point) {
    switch (title) {
      case "order":
        return 50.w;
      case "birthday":
        return 80.w;
      case "sex":
        return 80.w;
      case "description":
        return 80.w;
      case "fullName":
        return 150.w;
      default:
        return point.length * 100.w;
    }
  }

  setTexValue(key,value){
    switch(key){
      case "birthday":
        return  outputDateFormatV2.format(DateTime.fromMillisecondsSinceEpoch(value??0)).toString();
      case "order":
        return  value.toString();
      case "fullName":
        return  value??"";
      case "sex":
        return  value??"";
      case "description":
        return  value??"";
      default:
        if(value == ""||value == null){
          return "";
        }else{
          return "$value";
        }

    }
  }

  setText(key, value) {
    switch (key) {
      case "birthday":
        return Text(
          setTexValue(key, value),
          textAlign: TextAlign.center,
        );
      case "order":
        return Text(
          setTexValue(key, value),
          textAlign: TextAlign.center,
        );
      case "fullName":
        return Text(
          setTexValue(key, value),
          textAlign: TextAlign.start,
        );
      case "sex":
        return Text(setTexValue(key, value), textAlign: TextAlign.center);
      case "description":
        return InkWell(
          onTap: () {
            Get.dialog(Center(
              child: Wrap(children: [
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(16)),
                  child: Stack(
                    children: [
                      Container(
                        width: double.infinity,
                        margin:
                            EdgeInsets.only(top: 40.h, right: 50.w, left: 50.w),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(16)),
                        child: Center(
                          child: Container(
                            margin: EdgeInsets.fromLTRB(16.w, 40.h, 16.w, 16.h),
                            padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 8.h),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(color: ColorUtils.PRIMARY_COLOR)
                            ),
                            child: Text(setTexValue(key, value),
                                textAlign: TextAlign.center),
                          ),
                        ),
                      ),
                      Container(
                          alignment: Alignment.center,
                          margin: const EdgeInsets.only(top: 10),
                          height: 80,
                          child:
                              Image.asset("assets/images/image_app_logo.png"))
                    ],
                  ),
                ),
              ]),
            ));
          },
          child: Container(
            alignment: Alignment.topCenter,
            child:  const Icon(Icons.note_add_rounded),
          ),
        );
      default:
        return Center(
          child: SizedBox(
            height: 30.h,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: value.length,
              itemBuilder: (context, index) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 30.w,
                      margin: EdgeInsets.symmetric(horizontal: 8.w),
                      decoration: const BoxDecoration(
                          border: Border(
                              bottom: BorderSide(color: Colors.black26))),
                      child: Center(
                          child: Text(
                        setTexValue(key, value[index]),
                      )),
                    ),
                  ],
                );
              },
            ),
          ),
        );
    }
  }

  setUnderlineText(key) {
    switch (key) {
      case "birthday":
        return TextDecoration.none;
      case "order":
        return TextDecoration.none;
      case "fullName":
        return TextDecoration.none;
      case "sex":
        return TextDecoration.none;
      case "description":
        return TextDecoration.none;
      default:
        return TextDecoration.underline;
    }
  }
}
