import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import '../../../../../../../commom/app_cache.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/res/transcript/semester.dart';
import '../../../../../../../data/model/res/transcript/transcript.dart';
import '../../../../../../../data/repository/transcript_repo/transcript_repo.dart';
import '../../../../../../../routes/app_pages.dart';
import '../../../teacher_home_controller.dart';
import '../learning_management_teacher_controller.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class ListTranscriptsSubjectTeacherController extends GetxController{
  final TranscriptsRepo _transcriptsRepo = TranscriptsRepo();
  var transcript = <Transcript>[].obs;
  var semesters = <Semester>[].obs;
  var listSemester = <SemesterTranscript>[].obs;
  var controllerDateStart = TextEditingController().obs;
  var controllerDateEnd = TextEditingController().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy HH:mm');
  var isPublic = "".obs;
  var semesterName ="".obs;
  var semesterId ="".obs;
  var selectedDay = DateTime.now().obs;
  var listItemPopupMenu = <PopupMenuItem>[];

  var heightAnnounced = 30.0;
  var heightUpdateTranscript = 30.0;


  var showAnnounced = true;
  var showUpdateTranscript = false;


  var indexAnnounced = 0;
  var indexUpdateTranscript = 0;

  @override
  void onInit() {
    getListSemester();
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      selectedDay.value = DateTime(Get.find<TeacherHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
    }else{
      selectedDay.value = DateTime(Get.find<TeacherHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
    }
    controllerDateStart.value.text =   outputDateFormat.format(DateTime(findFirstDateOfTheWeek(selectedDay.value).year, findFirstDateOfTheWeek(selectedDay.value).month,findFirstDateOfTheWeek(selectedDay.value).day));
    controllerDateEnd.value.text = outputDateFormat.format(DateTime(findLastDateOfTheWeek(selectedDay.value).year, findLastDateOfTheWeek(selectedDay.value).month,findLastDateOfTheWeek(selectedDay.value).day));
    getListTranscript();
    super.onInit();
  }



  getListItemPopupMenu(index){
    listItemPopupMenu = [];
    var announced =   PopupMenuItem<int>(
      value: 1,
      padding: EdgeInsets.zero,
      height: heightAnnounced,
      child: showAnnounced
          ?Container(
        padding: EdgeInsets.symmetric(vertical: 4.h),
        decoration:  const BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.black26))),
        child: Row(
          children: [
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Icon(
              Icons
                  .upload,
              color: Colors
                  .black,
            ),
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Text(
                "Công bố")
          ],
        ),
      )
          :Row(
        children: [
          Padding(
              padding: EdgeInsets.only(
                  right:
                  8.w)),
          const Icon(
            Icons
                .upload,
            color: Colors
                .black,
          ),
          Padding(
              padding: EdgeInsets.only(
                  right:
                  8.w)),
          const Text(
              "Công bố")
        ],
      ),
    );
    var updateTranscript =   PopupMenuItem<int>(
        value: 2,
        padding: EdgeInsets.zero,
        height: heightUpdateTranscript,
        child: showUpdateTranscript
            ?Container(
          padding: EdgeInsets.symmetric(vertical: 4.h),
          decoration:  const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black26))),
          child:Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Icon(
                Icons
                    .mode_edit_outline,
                color: Colors
                    .black,
              ),
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Text(
                  "Nhập bảng điểm")
            ],
          ) ,
        )
            : Row(
          children: [
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Icon(
              Icons
                  .mode_edit_outline,
              color: Colors
                  .black,
            ),
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Text(
                "Nhập bảng điểm")
          ],
        )
    );
    var deleteTranscript =    PopupMenuItem<int>(
      value: 3,
      padding: EdgeInsets.zero,
      height: 30,
      child: Row(
        children: [
          Padding(
              padding: EdgeInsets
                  .only(
                  right:
                  8.w)),
          const Icon(
              Icons.delete),
          Padding(
              padding: EdgeInsets
                  .only(
                  right:
                  8.w)),
          const Text(
            "Xóa bảng điểm",
          )
        ],
      ),
    );

    if(transcript[index].isPublic == "FALSE" && transcript[index].createdBy?.id == AppCache().userId){
      if(checkVisibleFeature(
          Get.find<HomeController>().userGroupByApp,
          StringConstant.FEATURE_ANNOUNCED_TRANSCRIPT)){
        listItemPopupMenu.add(announced);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EDIT_TRANSCRIPT)){
        listItemPopupMenu.add(updateTranscript);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_DELETE_TRANSCRIPT)){
        listItemPopupMenu.add(deleteTranscript);
      }
    }

    indexAnnounced = listItemPopupMenu.indexOf(announced);
    indexUpdateTranscript = listItemPopupMenu.indexOf(updateTranscript);

    showAnnounced = setVisiblePopupMenuItem(indexAnnounced,listItemPopupMenu);
    showUpdateTranscript =setVisiblePopupMenuItem(indexUpdateTranscript,listItemPopupMenu);

    heightAnnounced = setHeightPopupMenuItem(indexAnnounced,listItemPopupMenu);
    heightUpdateTranscript = setHeightPopupMenuItem(indexUpdateTranscript,listItemPopupMenu);


    update();

    return listItemPopupMenu;

  }


  getListTranscript(){
    transcript.value =[];
    var idSubject = Get.find<LearningManagementTeacherController>().detailSubject.value.id;
    var fromDate = DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2),),00,00).millisecondsSinceEpoch;
    var toDate = DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2)),23,59).millisecondsSinceEpoch;
    _transcriptsRepo.getListTranscript(idSubject, setStatusPublic(isPublic.value), semesterId, fromDate, toDate,"SUBJECT","","").then((value) {
      if (value.state == Status.SUCCESS) {
        transcript.value= value.object!;
      }
    });
    transcript.refresh();
  }


  getListSemester(){
    listSemester.value = [];
    listSemester.add(SemesterTranscript(id: "",name: "Tất cả"));
    _transcriptsRepo.getListSemester().then((value) {
      if (value.state == Status.SUCCESS) {
        semesters.value= value.object!;
        for(int i =0;i< semesters.length ;i++){
          listSemester.add(SemesterTranscript(id: "",name: ""));
          listSemester[i+1].name = semesters[i].name!;
          listSemester[i+1].id = semesters[i].id!;
        }
      }
    });
    listSemester.refresh();
  }


  publicTranscript(transcriptId) {
    _transcriptsRepo.publicTranscript(transcriptId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Công bố bảng điểm thành công");
        getListSemester();
        getListTranscript();
        Get.back();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Công bố bảng điểm thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }





  deleteTranscript(transcriptId) {
    _transcriptsRepo.deleteTranscript(transcriptId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Xóa bảng điểm thành công");
        getListSemester();
        getListTranscript();
        Get.back();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Xóa bảng điểm thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }






  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }


  getStatusPublic(type){
    switch(type){
      case "TRUE":
        return "Đã công bố";
      case "FALSE":
        return "Chưa công bố";
      default:
        return "";
    }
  }



  setStatusPublic(type){
    switch(type){
      case "Đã công bố":
        return "TRUE";
      case "Chưa công bố":
        return "FALSE";
      default:
        return "";
    }
  }


  getColorTextIsPublic(status) {
    switch (status) {
      case "FALSE":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "TRUE":
        return const Color.fromRGBO(77, 197, 145, 1);
    }
  }

  getColorBackgroundIsPublic(status) {
    switch (status) {
      case "FALSE":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "TRUE":
        return const Color.fromRGBO(192, 242, 220, 1);
    }
  }


  goToDetailTranscript(index){
    Get.toNamed(Routes.detailTranscriptPage,arguments: transcript[index].id);
  }
}