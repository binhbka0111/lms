import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'detail_transcript_teacher_controller.dart';




class DetailTranscriptSubjectPage extends GetWidget<DetailTranscriptSubjectController>{
  @override
  final controller = Get.put(DetailTranscriptSubjectController());

  DetailTranscriptSubjectPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(179, 179, 179, 1),
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              comeToHome();
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Text(
          'Bảng điểm',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() => controller.isReady.value
          ?SingleChildScrollView(
        scrollDirection: Axis.horizontal,
          child: IntrinsicWidth(
            child: Container(
              width: controller.totalWidth(),
              color: Colors.white,
              padding: EdgeInsets.all(8.h),
              margin: EdgeInsets.all(8.h),
              child: IntrinsicWidth(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Text("Điểm môn học",style: TextStyle(color: Color.fromRGBO(133, 133, 133, 1)),),
                    SizedBox(height: 4.h,),
                    Row(
                      children: [Text("${controller.detailTranscript.value.name}",style: const TextStyle(color: Colors.black),),
                        Container(
                          margin: EdgeInsets.only(left: 4.w),
                          padding: EdgeInsets.only(
                              right: 8.r, left: 8.r, top: 2.h, bottom: 2.h),
                          alignment: Alignment.center,
                          height: 20,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: ColorUtils.PRIMARY_COLOR,
                          ),
                          child: Text(
                            '${controller.detailTranscript.value.semester?.name}',
                            style:
                            TextStyle(color: Colors.white, fontSize: 10.sp, fontWeight: FontWeight.w500),
                          ),
                        ),],
                    ),
                    SizedBox(height: 4.h,),
                    Container(
                      height: 48.h,
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(top: 16.h),
                      decoration: BoxDecoration(
                          color: const Color.fromRGBO(235, 235, 235, 1),
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(8.r),topRight: Radius.circular(8.r))
                      ),

                      child: Visibility(
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: controller.detailTranscript.value.tableHeaders?.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return SizedBox(
                              width: controller.setWidthHeader(controller.listValue[0][index].key,controller.listValue[0][index].point),
                              child: Text(controller.detailTranscript.value.tableHeaders![index].title!,textAlign: controller.detailTranscript.value.tableHeaders![index].key == "fullName"?TextAlign.start:TextAlign.center),
                            );
                          },),
                      ),
                    ),
                    Expanded(child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8.r),bottomRight: Radius.circular(8.r))
                      ),
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: controller.listValue.length,
                        itemBuilder: (context, index) {
                          controller.listPointTranscript.value = controller.listValue[index];
                          return IntrinsicWidth(
                            child: Container(
                                margin: EdgeInsets.only(top: 16.h),
                                child: Container(
                                  decoration: const BoxDecoration(
                                      border: Border(bottom: BorderSide(color: Colors.black26))),
                                  child: SizedBox(
                                    height: 30.h,
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: controller.listPointTranscript.length,
                                      scrollDirection: Axis.horizontal,
                                      physics: const NeverScrollableScrollPhysics(),
                                      itemBuilder: (context, indexDetail) {
                                        return  SizedBox(
                                            width: controller.setWidthHeader(controller.listValue[index][indexDetail].key,
                                                controller.listValue[index][indexDetail].point),
                                            child: Visibility(
                                                visible: controller.listValue[index][indexDetail].point != null,
                                                child:  controller.setText(controller.listValue[index][indexDetail].key,
                                                    controller.listValue[index][indexDetail].point)));
                                      },),
                                  ),
                                )
                            ),
                          );
                        },),
                    )),

                  ],
                ),
              ),
            ),
          )):Container())
    );
  }
}

