import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:html/parser.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/result_exam/list_student_submit_result_exam/result_exam_assign_link/result_exam_assign_link_controller.dart';
import '../../../../../../../../../../commom/utils/ViewPdf.dart';
import '../../../../../../../../../../commom/utils/global.dart';
import '../../../../../../../../../../commom/utils/open_url.dart';
import '../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../learning_management_teacher_controller.dart';


class ResultExamAssignLinkPage
    extends GetView<ResultExamAssignLinkController> {
  @override
  final controller = Get.put(ResultExamAssignLinkController());

  ResultExamAssignLinkPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {
                  comeToHome();
                },
                icon: const Icon(Icons.home),
                color: Colors.white,
              )
            ],
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            title: Text(
              'Bài Kiểm Tra Môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}',
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body: Obx(() => controller.isReady.value
              ? Column(
            children: [
              Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.all(16.h),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${controller.studentDoExam.value.title}",
                            style: TextStyle(
                                color: const Color.fromRGBO(133, 133, 133, 1),
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w500),
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Ngày tạo",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                controller.outputDateFormat.format(
                                    DateTime.fromMillisecondsSinceEpoch(controller
                                        .studentDoExam.value.createdAt ??
                                        0)),
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Thời gian bắt đầu",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                controller.outputDateFormat.format(
                                    DateTime.fromMillisecondsSinceEpoch(controller
                                        .studentDoExam.value.startTime ??
                                        0)),
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Thời gian kết thúc",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                controller.outputDateFormat.format(
                                    DateTime.fromMillisecondsSinceEpoch(controller
                                        .studentDoExam.value.endTime ??
                                        0)),
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Ghi chú",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                "${controller.studentDoExam.value.description}",
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            children: [
                              SizedBox(
                                width: 48.w,
                                height: 48.w,
                                child:
                                CacheNetWorkCustom(urlImage: '${controller.studentDoExam.value.student?.image }'),

                              ),
                              Padding(padding: EdgeInsets.only(right: 8.w)),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("${controller.studentDoExam.value.student?.fullName}",style: TextStyle(color: Colors.black,fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                  Visibility(
                                      visible:controller.studentDoExam.value.student!
                                          .birthday != null,
                                      child:  Text(
                                        controller.outputDateFormatV2.format(
                                            DateTime.fromMillisecondsSinceEpoch(
                                                controller.studentDoExam.value.student
                                                    ?.birthday ??
                                                    0)),
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                133, 133, 133, 1),
                                            fontSize: 12.sp),
                                      ))
                                ],
                              )

                            ],
                          ),
                          FocusScope(
                            node: FocusScopeNode(),
                            child: Focus(
                                onFocusChange: (focus) {
                                  controller.update();
                                },
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  padding: const EdgeInsets.all(2.0),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(6.0)),
                                      border: Border.all(
                                        width: 1,
                                        style: BorderStyle.solid,
                                        color: controller.focusAttachLink.hasFocus
                                            ? ColorUtils.PRIMARY_COLOR
                                            : const Color.fromRGBO(
                                            192, 192, 192, 1),
                                      )),
                                  child: TextFormField(
                                    cursorColor: ColorUtils.PRIMARY_COLOR,
                                    focusNode: controller.focusAttachLink,
                                    onChanged: (value) {
                                      controller.update();
                                    },
                                    onFieldSubmitted: (value) {
                                      controller.update();
                                    },
                                    readOnly: true,
                                    controller: controller.controllerAttachLink,
                                    maxLines: null,
                                    decoration: InputDecoration(
                                      labelText: "Đường dẫn",
                                      labelStyle: TextStyle(
                                          fontSize: 14.0.sp,
                                          color: const Color.fromRGBO(
                                              177, 177, 177, 1),
                                          fontWeight: FontWeight.w500,
                                          fontFamily:
                                          'assets/font/static/Inter-Medium.ttf'),
                                      contentPadding: const EdgeInsets.symmetric(
                                          vertical: 8, horizontal: 16),
                                      prefixIcon: null,
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      border: InputBorder.none,
                                      errorStyle: const TextStyle(height: 0),
                                      focusedErrorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      floatingLabelBehavior:
                                      FloatingLabelBehavior.auto,
                                    ),
                                  ),
                                )),
                          ),
                          Padding(padding: EdgeInsets.only(top: 16.h)),
                          const Text('Bài làm',
                              style: TextStyle(
                                  color: Color.fromRGBO(26, 26, 26, 1),
                                  fontFamily:
                                  'assets/font/static/Inter-Medium.ttf',
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500)),
                          Padding(padding: EdgeInsets.only(top: 16.h)),
                          Text(
                            "${parse(parse(controller.studentDoExam.value.contentAnswer ?? "").body?.text).documentElement?.text}",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400,
                                fontSize: 14.sp),
                          ),
                          Obx(() => Visibility(
                              visible: controller
                                  .studentDoExam.value.filesAnswer!
                                  .where((element) =>
                              element.ext == "png" ||
                                  element.ext == "jpg" ||
                                  element.ext == "jpeg" ||
                                  element.ext == "gif" ||
                                  element.ext == "bmp")
                                  .toList()
                                  .isNotEmpty,
                              child: GridView.builder(
                                  padding: const EdgeInsets.all(10),
                                  gridDelegate:
                                  SliverGridDelegateWithMaxCrossAxisExtent(
                                    maxCrossAxisExtent: 250.w,
                                    crossAxisSpacing: 10,
                                    mainAxisSpacing: 10,
                                  ),
                                  scrollDirection: Axis.vertical, //chiều cuộn
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: controller
                                      .studentDoExam.value.filesAnswer!
                                      .where((element) =>
                                  element.ext == "png" ||
                                      element.ext == "jpg" ||
                                      element.ext == "jpeg" ||
                                      element.ext == "gif" ||
                                      element.ext == "bmp")
                                      .toList()
                                      .length,
                                  itemBuilder: (context, indexGrid) {
                                    return InkWell(
                                      onTap: () {
                                        OpenUrl.openImageViewer(
                                            context,
                                            controller.studentDoExam.value
                                                .filesAnswer!
                                                .where((element) =>
                                            element.ext == "png" ||
                                                element.ext == "jpg" ||
                                                element.ext == "jpeg" ||
                                                element.ext == "gif" ||
                                                element.ext == "bmp")
                                                .toList()[indexGrid]
                                                .link!);
                                      },
                                      child: SizedBox(
                                        width: 200.w,
                                        height: 200.w,
                                        child:
                                        CacheNetWorkCustomBanner(urlImage: '${
                                            controller.studentDoExam.value
                                            .filesAnswer!
                                            .where((element) =>
                                        element.ext == "png" ||
                                            element.ext == "jpg" ||
                                            element.ext == "jpeg" ||
                                            element.ext == "gif" ||
                                            element.ext == "bmp")
                                            .toList()[indexGrid]
                                            .link }'),

                                      ),
                                    );
                                  }))),
                          Obx(() => Visibility(
                              visible: controller
                                  .studentDoExam.value.filesAnswer!
                                  .where((element) =>
                              element.ext != "png" &&
                                  element.ext != "jpg" &&
                                  element.ext != "jpeg" &&
                                  element.ext != "gif" &&
                                  element.ext != "bmp")
                                  .toList()
                                  .isNotEmpty,
                              child: DottedBorder(
                                  dashPattern: const [5, 5],
                                  radius: const Radius.circular(6),
                                  borderType: BorderType.RRect,
                                  padding: const EdgeInsets.only(
                                      top: 12, left: 12, right: 12, bottom: 12),
                                  color: ColorUtils.PRIMARY_COLOR,
                                  child: ListView.builder(
                                      shrinkWrap: true,
                                      physics:
                                      const NeverScrollableScrollPhysics(),
                                      itemCount: controller
                                          .studentDoExam.value.filesAnswer!
                                          .where((element) =>
                                      element.ext != "png" &&
                                          element.ext != "jpg" &&
                                          element.ext != "jpeg" &&
                                          element.ext != "gif" &&
                                          element.ext != "bmp")
                                          .toList()
                                          .length,
                                      itemBuilder: (context, indexNotImage) {
                                        return InkWell(
                                          onTap: () {
                                            var action = 0;
                                            var extension = controller
                                                .studentDoExam
                                                .value
                                                .filesAnswer!
                                                .where((element) =>
                                            element.ext != "png" &&
                                                element.ext != "jpg" &&
                                                element.ext != "jpeg" &&
                                                element.ext != "gif" &&
                                                element.ext != "bmp")
                                                .toList()[indexNotImage]
                                                .ext;
                                            if (extension == "png" ||
                                                extension == "jpg" ||
                                                extension == "jpeg" ||
                                                extension == "gif" ||
                                                extension == "bmp") {
                                              action = 1;
                                            } else if (extension == "pdf") {
                                              action = 2;
                                            } else {
                                              action = 0;
                                            }
                                            switch (action) {
                                              case 1:
                                                OpenUrl.openImageViewer(
                                                    context,
                                                    controller.studentDoExam
                                                        .value.filesAnswer!
                                                        .where((element) =>
                                                    element.ext != "png" &&
                                                        element.ext !=
                                                            "jpg" &&
                                                        element.ext !=
                                                            "jpeg" &&
                                                        element.ext !=
                                                            "gif" &&
                                                        element.ext != "bmp")
                                                        .toList()[indexNotImage]
                                                        .link!);
                                                break;
                                              case 2:
                                                Get.to(ViewPdfPage(
                                                    url:
                                                    controller
                                                        .studentDoExam
                                                        .value
                                                        .filesAnswer!
                                                        .where((element) =>
                                                    element
                                                        .ext !=
                                                        "png" &&
                                                        element
                                                            .ext !=
                                                            "jpg" &&
                                                        element.ext !=
                                                            "jpeg" &&
                                                        element.ext !=
                                                            "gif" &&
                                                        element.ext !=
                                                            "bmp")
                                                        .toList()[
                                                    indexNotImage]
                                                        .link!));
                                                break;
                                              default:
                                                OpenUrl.openFile(controller
                                                    .studentDoExam
                                                    .value
                                                    .filesAnswer!
                                                    .where((element) =>
                                                element.ext != "png" &&
                                                    element.ext != "jpg" &&
                                                    element.ext != "jpeg" &&
                                                    element.ext != "gif" &&
                                                    element.ext != "bmp")
                                                    .toList()[indexNotImage]
                                                    .link!);
                                                break;
                                            }
                                          },
                                          child: Container(
                                            margin:
                                            const EdgeInsets.only(top: 12),
                                            padding: const EdgeInsets.all(12),
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                BorderRadius.circular(6),
                                                color: const Color.fromRGBO(
                                                    246, 246, 246, 1)),
                                            child: Row(
                                              children: [
                                                Image.network(
                                                  controller.studentDoExam
                                                      .value.filesAnswer!
                                                      .where((element) =>
                                                  element.ext != "png" &&
                                                      element.ext != "jpg" &&
                                                      element.ext != "jpeg" &&
                                                      element.ext != "gif" &&
                                                      element.ext != "bmp")
                                                      .toList()[indexNotImage]
                                                      .link!,
                                                  errorBuilder: (
                                                      BuildContext context,
                                                      Object error,
                                                      StackTrace? stackTrace,
                                                      ) {
                                                    return Image.asset(
                                                      getFileIcon(controller
                                                          .studentDoExam
                                                          .value
                                                          .filesAnswer!
                                                          .where((element) =>
                                                      element.ext != "png" &&
                                                          element.ext !=
                                                              "jpg" &&
                                                          element.ext !=
                                                              "jpeg" &&
                                                          element.ext !=
                                                              "gif" &&
                                                          element.ext !=
                                                              "bmp")
                                                          .toList()[indexNotImage]
                                                          .name),
                                                      height: 35,
                                                      width: 30,
                                                    );
                                                  },
                                                  height: 35,
                                                  width: 30,
                                                ),
                                                const Padding(
                                                    padding:
                                                    EdgeInsets.only(left: 8)),
                                                SizedBox(
                                                  width: 220.w,
                                                  child: Text(
                                                    '${controller.studentDoExam.value.filesAnswer!.where((element) => element.ext != "png" && element.ext != "jpg" && element.ext != "jpeg" && element.ext != "gif" && element.ext != "bmp").toList()[indexNotImage].name}',
                                                    style: TextStyle(
                                                        color:
                                                        const Color.fromRGBO(
                                                            26, 59, 112, 1),
                                                        fontSize: 14.sp,
                                                        fontFamily:
                                                        'assets/font/static/Inter-Medium.ttf',
                                                        fontWeight:
                                                        FontWeight.w500),
                                                  ),
                                                ),
                                                Expanded(child: Container()),
                                                Image.asset(
                                                  'assets/images/icon_upfile_subject.png',
                                                  height: 16,
                                                  width: 16,
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      })))),
                        ],
                      ),
                    ),
                  )),
            ],
          )
              : Container()),
        );
  }
  
}
