import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/res/exercise/list_student_exercise.dart';
import '../../../../../../../../data/repository/exams/exam_repo.dart';
import '../list_student_exam_controller.dart';


class ListStudentUnSubmitResultExamController extends GetxController{
  var listStudentExercise = ListStudentSubmitted().obs;
  var studentUnSubmitted = <StudentUnSubmitted>[].obs;
  final ExamRepo examRepo = ExamRepo();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy');
  @override
  void onInit() {
    getListStudentUnSubmit(Get.find<ListStudentExamController>().itemsExam.value.id);
    super.onInit();
  }

  getListStudentUnSubmit(exerciseId){
    examRepo.studentsSubmitted(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudentExercise.value = value.object!;
        studentUnSubmitted.value = listStudentExercise.value.studentUnSubmitted!;
      }
    });
  }

}