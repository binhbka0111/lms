import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../../data/model/res/exercise/teacherCommentAnswer.dart';
import '../../../../../../../../../data/model/res/exams/student_do_exam.dart';
import '../../../../../../../../../data/repository/exams/exam_repo.dart';
import '../../list_student_exam_controller.dart';





class ResultExamUploadFileController extends GetxController{
  var focusAttachLink = FocusNode();
  var controllerAttachLink = TextEditingController();
  var focusScoreQuestion = FocusNode();
  var controllerScoreQuestion = TextEditingController();
  var focusScoreQuestionAll = FocusNode();
  var controllerScoreQuestionAll = TextEditingController();
  var focusComment = FocusNode();
  var controllerComment = TextEditingController();
  var focusCommentAll = FocusNode();
  var controllerCommentAll = TextEditingController();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy');
  var studentDoExam = StudentDoExam().obs;
  var studentID = "".obs;
  var isReady = false.obs;
  final ExamRepo examRepo = ExamRepo();
  var helperTextScoreQuestion = "Vui lòng nhập điểm số";
  var isShowhelperText = false;
  var helperTextScoreQuestionAll = "Vui lòng nhập điểm số";
  var isShowhelperTextAll = false;
  var teacherCommentAnswers = <TeacherCommentAnswer>[].obs;
  var listImage = [];
  var listNotImage = [];
  var listImageDoExam = [];
  var listNotImageDoExam = [];
  @override
  void onInit() {
    var tmpStudentId = Get.arguments;
    if (tmpStudentId != null) {
      studentID.value = tmpStudentId;
    }
    getDetailExam(Get.find<ListStudentExamController>().itemsExam.value.id,studentID.value);
    super.onInit();
  }


  getDetailExam(exerciseId,studentId) {
    examRepo.detailStudentDoExam(exerciseId,studentId).then((value) {
      if (value.state == Status.SUCCESS) {
        studentDoExam.value = value.object!;
        controllerComment.text = studentDoExam.value.teacherComment!;
        controllerScoreQuestion.text = studentDoExam.value.score == null?"":"${studentDoExam.value.score}";

        listImage = studentDoExam.value.files!.where((element) => element.ext == "png" ||
            element.ext == "jpg" ||
            element.ext == "jpeg" ||
            element.ext == "gif" ||
            element.ext == "bmp").toList().obs;
        listNotImage = studentDoExam.value.files!.where((element) =>
        element.ext != "png" &&
            element.ext != "jpg" &&
            element.ext != "jpeg" &&
            element.ext != "gif" &&
            element.ext != "bmp")
            .toList().obs;

        listImageDoExam =  studentDoExam.value.filesAnswer!.where((element) => element.ext == "png" ||
            element.ext == "jpg" ||
            element.ext == "jpeg" ||
            element.ext == "gif" ||
            element.ext == "bmp").toList().obs;
        listNotImageDoExam =  studentDoExam.value.filesAnswer!.where((element) =>
        element.ext != "png" &&
            element.ext != "jpg" &&
            element.ext != "jpeg" &&
            element.ext != "gif" &&
            element.ext != "bmp")
            .toList().obs;
        AppUtils.shared.showToast("Lấy chi tiết bài kiểm tra thành công");
        isReady.value = true;
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lấy chi tiết bài kiểm tra thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        isReady.value = true;
      }
    });
  }


}