import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../../data/model/res/exercise/teacherCommentAnswer.dart';
import '../../../../../../../../../data/model/res/exams/student_do_exam.dart';
import '../../../../../../../../../data/repository/exams/exam_repo.dart';
import '../../list_student_exam_controller.dart';




class ResultExamQuestionController extends GetxController{
  var focusScoreQuestion = <FocusNode>[];
  var controllerScoreQuestion = <TextEditingController>[];
  var focusScoreQuestionAll = FocusNode();
  var controllerScoreQuestionAll = TextEditingController();
  var focusComment = <FocusNode>[];
  var controllerComment = <TextEditingController>[];
  var focusCommentAll = FocusNode();
  var controllerCommentAll = TextEditingController();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var studentDoExam = StudentDoExam().obs;
  var studentID = "".obs;
  var isReady = false.obs;
  var isEvaluate =false.obs;
  final ExamRepo examRepo = ExamRepo();
  var helperTextScoreQuestion = <String>[];
  var isShowhelperText = <bool>[];
  var helperTextScoreQuestionAll = "Vui lòng nhập điểm số";
  var isShowhelperTextAll = false;
  var  teacherCommentAnswers = <TeacherCommentAnswer>[].obs;
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy');
  var teacherCommentAnswer = TeacherCommentAnswer(teacherComment: "",point: 0.0,questionAnswerId: "").obs;


  @override
  void onInit() {
    var tmpStudentId = Get.arguments;
    if (tmpStudentId != null) {
      studentID.value = tmpStudentId;
    }
    getDetailExam(Get.find<ListStudentExamController>().itemsExam.value.id,studentID.value);
    super.onInit();
  }


  getDetailExam(exerciseId,studentId) {
    examRepo.detailStudentDoExam(exerciseId,studentId).then((value) {
      if (value.state == Status.SUCCESS) {
        studentDoExam.value = value.object!;
        for(int i = 0;i<studentDoExam.value.questionAnswers!.length;i++){
          isShowhelperText.add(false);
          helperTextScoreQuestion.add("");
          focusScoreQuestion.add(FocusNode());
          focusComment.add(FocusNode());
          controllerScoreQuestion.add(TextEditingController());
          controllerComment.add(TextEditingController());
          teacherCommentAnswers.add(TeacherCommentAnswer());
          controllerComment[i].text = studentDoExam.value.questionAnswers![i].teacherComment??"";
          controllerScoreQuestion[i].text = studentDoExam.value.questionAnswers![i].point.toString();
        }
        controllerCommentAll.text = studentDoExam.value.teacherComment??"";
        AppUtils.shared.showToast("Lấy chi tiết bài kiểm tra thành công");
        isReady.value = true;
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lấy chi tiết bài kiểm tra thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        isReady.value = true;
      }
    });
  }




  setColorTextKey(status,key,answer){
    if(answer == ""){
      return const Color.fromRGBO(177, 177, 177, 1);
    }else{
      if(status == "TRUE"){
        return const Color.fromRGBO(77, 197, 145, 1);
      }else{
        if(key == answer){
          return const Color.fromRGBO(255, 69, 89, 1);
        }else{
          return const Color.fromRGBO(177, 177, 177, 1);
        }
      }
    }

  }

  setBorderAnswer(status,key,answer){
    if(answer == ""){
      return const Color.fromRGBO(177, 177, 177, 1);
    }else{
      if(status == "TRUE"){
        return const Color.fromRGBO(77, 197, 145, 1);
      }else{
        if(key == answer){
          return const Color.fromRGBO(255, 69, 89, 1);
        }else{
          return const Color.fromRGBO(177, 177, 177, 1);
        }
      }
    }

  }

  setBackgroundKey(status,key,answer){
    if(answer == ""){
      return const Color.fromRGBO(255, 255, 255, 1);
    }else{
      if(status == "TRUE"){
        return const Color.fromRGBO(184, 239, 215, 1);
      }else{
        if(key == answer){
          return const Color.fromRGBO(255, 181, 189, 1);
        }else{
          return const Color.fromRGBO(255, 255, 255, 1);
        }
      }
    }

  }


  setColorTextAnswer(status,key,answer){
    if(answer == ""){
      return const Color.fromRGBO(26, 26, 26, 1);
    }else{
      if(status == "TRUE"){
        return const Color.fromRGBO(77, 197, 145, 1);
      }else{
        if(key == answer){
          return const Color.fromRGBO(255, 69, 89, 1);
        }else{
          return const Color.fromRGBO(26, 26, 26, 1);
        }
      }
    }
  }



  setBorderKey(status,key,answer){
    if(answer == ""){
      return const Color.fromRGBO(177, 177, 177, 1);
    }else{
      if(status == "TRUE"){
        return const Color.fromRGBO(184, 239, 215, 1);
      }else{
        if(key == answer){
          return const Color.fromRGBO(255, 181, 189, 1);
        }else{
          return const Color.fromRGBO(177, 177, 177, 1);
        }
      }
    }
  }



  visibleFalse(status,key,answer){
    if(answer == ""){
      return false;
    }else{
      if(status == "TRUE"){
        return false;
      }else{
        if(key == answer){
          return true;
        }else{
          return false;
        }
      }
    }


  }


  visibleTrue(status,key,answer){
    if(answer == ""){
      return false;
    }else{
      if(status == "TRUE"){
        return true;
      }else{
        return false;
      }
    }
  }




}