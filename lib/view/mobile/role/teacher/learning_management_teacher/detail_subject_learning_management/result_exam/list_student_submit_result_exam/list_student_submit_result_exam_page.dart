import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import '../../../../../../home/home_controller.dart';
import 'list_student_submit_result_exam_controller.dart';





class ListStudentSubmitResultExamPage extends GetView<ListStudentSubmitResultExamController>{
  @override
  final controller = Get.put(ListStudentSubmitResultExamController());

  ListStudentSubmitResultExamPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Obx(() => SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 16.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.only(top: 16.h)),
            ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: controller.studentSubmitted.length,
              itemBuilder: (context, index) {
                return Obx(() => InkWell(
                  onTap: () {
                    checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> controller.onClickListStudentSubmitResult(index),StringConstant.FEATURE_EXAM_DETAIL);
                  },
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  SizedBox(
                                    width: 48.h,
                                    height: 48.h,
                                    child:
                                    CacheNetWorkCustom(urlImage: '${controller.studentSubmitted[index].student?.image}'),

                                  ),
                                  Padding(padding: EdgeInsets.only(right: 8.w)),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("${controller.studentSubmitted[index].student?.fullName}",style: TextStyle(color: Colors.black,fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                      Visibility(
                                          visible: controller
                                              .studentSubmitted[index]
                                              .student!
                                              .birthday != null,
                                          child:  Text(
                                            controller.outputDateFormatV2.format(
                                                DateTime.fromMillisecondsSinceEpoch(
                                                    controller
                                                        .studentSubmitted[index]
                                                        .student
                                                        ?.birthday ??
                                                        0)),
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontSize: 12.sp),
                                          ))
                                    ],
                                  ),
                                  SizedBox(
                                    width: 16.w,
                                  ),
                                  Text("${controller.studentSubmitted[index].score??0}/${controller.studentSubmitted[index].scoreOfExam??0}đ")

                                ],
                              ),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              Row(
                                children: [
                                  Text("Thời gian nộp bài",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                                  Padding(padding: EdgeInsets.only(right: 16.w)),
                                  Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.studentSubmitted[index].createdAt!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                ],
                              ),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              Row(
                                children: [
                                  SizedBox(
                                    width:140.h,
                                    child: Text("Trạng thái nộp bài",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                                    decoration: BoxDecoration(
                                        color: controller.getColorBackgroundStatus(controller.studentSubmitted[index].statusExerciseByStudent),
                                        borderRadius: BorderRadius.circular(6)
                                    ),
                                    child: Text(
                                      "${controller.getStatus(controller.studentSubmitted[index].statusExerciseByStudent)}",
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          color: controller.getColorTextStatus(controller.studentSubmitted[index].statusExerciseByStudent),
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              Row(
                                children: [
                             SizedBox(
                               width: 140.h,
                               child:      Text("Trạng thái chấm điểm",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                             ),
                                  Container(
                                    padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                                    decoration: BoxDecoration(
                                        color: controller.getColorBackgroundIsGrade(controller.studentSubmitted[index].isGrade),
                                        borderRadius: BorderRadius.circular(6)
                                    ),
                                    child: Text(
                                      "${controller.getTextIsGrade(controller.studentSubmitted[index].isGrade)}",
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          color: controller.getColorTextIsGrade(controller.studentSubmitted[index].isGrade),
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                ],
                              ),


                            ],
                          ),

                        ],
                      ),
                      const Divider(),
                    ],
                  ),
                ));
              },)
          ],
        ),
      ),
    ));
  }

}