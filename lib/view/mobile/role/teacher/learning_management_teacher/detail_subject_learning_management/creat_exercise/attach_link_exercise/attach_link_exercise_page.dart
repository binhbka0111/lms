import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/global.dart';
import '../../../../../../../../commom/app_cache.dart';
import '../../../../teacher_home_controller.dart';
import '../../learning_management_teacher_controller.dart';
import '../creat_exercise_controller.dart';
import 'attach_link_exercise_controller.dart';
import 'package:validators/validators.dart';
class AttachLinkExercisePage extends GetView<AttachLinkExerciseController>{
  @override
  final controller = Get.put(AttachLinkExerciseController());

  AttachLinkExercisePage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        dismissKeyboard();
        Get.find<CreateExerciseController>().scrollController?.animateTo(
          0,
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeOut,
        );
      },
      child:
      GetBuilder<AttachLinkExerciseController>(builder: (controller) {
        return LayoutBuilder(builder: (context,constraints){
          return SingleChildScrollView(
            physics: const ClampingScrollPhysics(),
            child: ConstrainedBox(
                constraints: BoxConstraints(minWidth: constraints.maxWidth, minHeight: constraints.minHeight),
                child: IntrinsicHeight(
                  child: Column(
                    children: [Container(
                  margin:  EdgeInsets.symmetric(horizontal: 16.w),
                  color: Colors.transparent,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      FocusScope(
                        node: FocusScopeNode(),
                        child: Focus(
                            onFocusChange: (focus) {
                              controller.update();
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding: const EdgeInsets.all(2.0),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                      const BorderRadius.all(Radius.circular(6.0)),
                                      border: Border.all(
                                        width: 1,
                                        style: BorderStyle.solid,
                                        color: controller.focusAttachLink.hasFocus
                                            ? ColorUtils.PRIMARY_COLOR
                                            : const Color.fromRGBO(192, 192, 192, 1),
                                      )),
                                  child: TextFormField(
                                    cursorColor: ColorUtils.PRIMARY_COLOR,
                                    focusNode: controller.focusAttachLink,
                                    onChanged: (value) {
                                      if(controller.controllerAttachLink.text.trim() != ""){
                                        controller.isShowErrorLink = false;
                                      }
                                      controller.update();
                                    },
                                    onFieldSubmitted: (value) {
                                      controller.update();
                                      Get.find<CreateExerciseController>().scrollController?.animateTo(
                                        0,
                                        duration: const Duration(milliseconds: 500),
                                        curve: Curves.easeOut,
                                      );
                                    },
                                    controller: controller.controllerAttachLink,
                                    decoration: InputDecoration(
                                      labelText: "Gán đường dẫn",
                                      labelStyle: TextStyle(
                                          fontSize: 14.0.sp,
                                          color: const Color.fromRGBO(177, 177, 177, 1),
                                          fontWeight: FontWeight.w500,
                                          fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                                      contentPadding: const EdgeInsets.symmetric(
                                          vertical: 8, horizontal: 16),
                                      prefixIcon:  null,
                                      suffixIcon:  Visibility(
                                          visible:
                                          controller.controllerAttachLink.text.isNotEmpty ==
                                              true,
                                          child: InkWell(
                                            onTap: () {
                                              controller.controllerAttachLink.text = "";
                                            },
                                            child: const Icon(
                                              Icons.close_outlined,
                                              color: Colors.black,
                                              size: 20,
                                            ),
                                          )),
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      border: InputBorder.none,
                                      errorStyle: const TextStyle(height: 0),
                                      focusedErrorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      floatingLabelBehavior: FloatingLabelBehavior.auto,
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: controller.isShowErrorLink,
                                  child: Container(
                                      padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                      child: const Text(
                                        "Vui lòng nhập đường link",
                                        style: TextStyle(color: Colors.red),
                                      )),
                                )
                              ],
                            )),
                      ),
                      SizedBox(
                        height: 8.h,
                      ),
                      FocusScope(
                        node: FocusScopeNode(),
                        child: Focus(
                            onFocusChange: (focus) {
                              controller.update();
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  padding: const EdgeInsets.all(2.0),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                      const BorderRadius.all(Radius.circular(6.0)),
                                      border: Border.all(
                                        width: 1,
                                        style: BorderStyle.solid,
                                        color: controller.focusPoint.hasFocus
                                            ? ColorUtils.PRIMARY_COLOR
                                            : const Color.fromRGBO(192, 192, 192, 1),
                                      )),
                                  child: TextFormField(
                                    cursorColor: ColorUtils.PRIMARY_COLOR,
                                    focusNode: controller.focusPoint,
                                    inputFormatters: <TextInputFormatter> [
                                      FilteringTextInputFormatter.allow(RegExp('[0-9.,]')),
                                    ],
                                    onChanged: (value) {
                                      if(controller.controllerPoint.text.trim() != ""){
                                        controller.isShowErrorPoint = false;
                                      }
                                      controller.update();
                                    },
                                    onFieldSubmitted: (value) {
                                      controller.update();
                                      Get.find<CreateExerciseController>().scrollController?.animateTo(
                                        0,
                                        duration: const Duration(milliseconds: 500),
                                        curve: Curves.easeOut,
                                      );
                                    },
                                    controller: controller.controllerPoint,
                                    keyboardType: TextInputType.number,

                                    decoration: InputDecoration(
                                      labelText: "Nhập điểm bài tập",
                                      labelStyle: TextStyle(
                                          fontSize: 14.0.sp,
                                          color: const Color.fromRGBO(177, 177, 177, 1),
                                          fontWeight: FontWeight.w500,
                                          fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                                      contentPadding: const EdgeInsets.symmetric(
                                          vertical: 8, horizontal: 16),
                                      prefixIcon:  null,
                                      suffixIcon:  Visibility(
                                          visible:
                                          controller.controllerPoint.text.isNotEmpty ==
                                              true,
                                          child: InkWell(
                                            onTap: () {
                                              controller.controllerPoint.text = "";
                                            },
                                            child: const Icon(
                                              Icons.close_outlined,
                                              color: Colors.black,
                                              size: 20,
                                            ),
                                          )),
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      border: InputBorder.none,
                                      errorStyle: const TextStyle(height: 0),
                                      focusedErrorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      floatingLabelBehavior: FloatingLabelBehavior.auto,

                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: controller.isShowErrorPoint,
                                  child: Container(
                                      padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                      child: Text(
                                        controller.errorTextPoint.value,
                                        style: const TextStyle(color: Colors.red),
                                      )),
                                )
                              ],
                            )),
                      ),
                    ],
                  ),
                ),

                      const Spacer(),
                      InkWell(
                      onTap: () {
                        if(controller.controllerAttachLink.text != "" &&controller.controllerPoint.text != ""){
                          if (Get.find<CreateExerciseController>().controllerNameExercise.text.trim() == "") {
                            Get.find<CreateExerciseController>().isShowErrorTextNameExercise = true;
                            Get.find<CreateExerciseController>().update();
                          } else {
                            Get.find<CreateExerciseController>().isShowErrorTextNameExercise = false;
                            Get.find<CreateExerciseController>().update();
                            if (Get.find<CreateExerciseController>().controllerDateEnd.text == "") {
                              Get.find<CreateExerciseController>().isShowErrorTextDate = true;
                              Get.find<CreateExerciseController>().update();
                            } else {
                              Get.find<CreateExerciseController>().isShowErrorTextDate = false;
                              Get.find<CreateExerciseController>().update();
                              if (Get.find<CreateExerciseController>().controllerTimeEnd.text.trim() == "") {
                                Get.find<CreateExerciseController>().isShowErrorTextTime = true;
                                Get.find<CreateExerciseController>().update();
                              }else{
                                Get.find<CreateExerciseController>().isShowErrorTextTime = false;
                                Get.find<CreateExerciseController>().update();
                                if(isURL(controller.controllerAttachLink.value.text)){
                                  controller.isShowErrorLink = false;
                                  controller.update();
                                  if (controller.controllerPoint.text.trim() == "") {
                                    controller.isShowErrorPoint = true;
                                    controller.errorTextPoint.value = "Vui lòng nhập điểm cho câu hỏi";
                                    controller.update();
                                  }else{
                                    controller.isShowErrorPoint= false;
                                    controller.update();
                                    if(controller.controllerPoint.text.trim().substring(controller.controllerPoint.text.trim().length - 1) == "."){
                                      controller.isShowErrorPoint = true;
                                      controller.errorTextPoint.value = "Vui lòng nhập đúng định dạng điểm";
                                      controller.update();
                                    }
                                    else if (double.tryParse(controller.controllerPoint.text.trim()) == null) {
                                      controller.isShowErrorPoint = true;
                                      controller.errorTextPoint.value = "Vui lòng nhập đúng định dạng điểm";
                                      controller.update();
                                    }else{
                                      controller.createExercise(
                                          Get.find<CreateExerciseController>().controllerNameExercise.text.trim()
                                          , DateFormat("dd/MM/yyyy HH:mm").parse("${Get.find<CreateExerciseController>().controllerDateEnd.text.trim()} ${Get.find<CreateExerciseController>().controllerTimeEnd.text.trim()}").millisecondsSinceEpoch
                                          , Get.find<CreateExerciseController>().controllerDescribe.text.trim()
                                          , controller.setTypeExercise(Get.find<CreateExerciseController>().typeExercise.value)
                                          , Get.find<LearningManagementTeacherController>().detailSubject.value.id
                                          , AppCache().userId
                                          , Get.find<TeacherHomeController>().currentClass.value.classId
                                          , controller.controllerAttachLink.text
                                          , double.tryParse(controller.controllerPoint.text.trim()));
                                    }
                                  }
                                }else{
                                  controller.isShowErrorLink = true;
                                  controller.update();
                                }
                              }
                            }
                          }
                        }
                      },
                      child: Container(
                        color: Colors.white,
                        margin: EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                        width: double.infinity,
                        child: Container(
                          height: 40.h,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            border: Border.all(
                              color: controller.controllerPoint.text != ""? ColorUtils.PRIMARY_COLOR:const Color.fromRGBO(177, 177, 177, 1),
                            ),

                          ),
                          alignment: Alignment.center,
                          child: Text("Xác nhận",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400,
                              color: controller.controllerPoint.text != ""? ColorUtils.PRIMARY_COLOR:const Color.fromRGBO(177, 177, 177, 1)
                          ),textAlign: TextAlign.center),
                        ),
                      ),
                    )],
                  ),
                ),),
          );
        });
      },),
    );
  }

}