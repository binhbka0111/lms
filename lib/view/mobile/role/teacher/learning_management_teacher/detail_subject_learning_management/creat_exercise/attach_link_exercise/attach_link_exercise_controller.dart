import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/res/exercise/exercise_link.dart';
import '../../../../../../../../data/repository/exercise/excercise_repo.dart';

class AttachLinkExerciseController extends GetxController{
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  var focusAttachLink = FocusNode();
  var controllerAttachLink = TextEditingController();

  var textAttachLink = "".obs;
  var exerciseLink = ExerciseLink().obs;
  var isShowErrorLink = false;
  var isShowErrorPoint = false;
  var errorTextPoint = "".obs;
  var focusPoint = FocusNode();
  var controllerPoint = TextEditingController();

  createExercise(title, deadline, description, typeExercise, subjectId, teacherId, classId, link,scoreOfExercise) {
    exerciseRepo.createExerciseLink(title,deadline, description, typeExercise, subjectId, teacherId, classId, link,scoreOfExercise).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Tạo bài tập thành công");
        Get.back();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tạo bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_GREEN_BOLD);
      }
    });
  }

  setTypeExercise(type){
    switch(type){
      case "Trắc nghiệm & tự luận":
        return "ALL";
      case "Trắc nghiệm":
        return "SELECTED_RESPONSE";
      case "Tự luận":
        return "CONSTRUCTED_RESPONSE";
      default:
        return "";
    }
  }


}