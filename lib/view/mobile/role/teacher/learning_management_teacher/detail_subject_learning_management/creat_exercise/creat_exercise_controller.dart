import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../../../../../../commom/utils/color_utils.dart';
import 'file_upload_exercise/file_upload_exercise_controller.dart';

class CreateExerciseController extends GetxController with GetSingleTickerProviderStateMixin{
  var focusDateStart = FocusNode();
  var controllerDateEnd = TextEditingController();
  var focusNameExercise = FocusNode();
  var controllerNameExercise = TextEditingController();
  var focusAssure = FocusNode();
  var controllerAssure = TextEditingController();
  var controllerTimeEnd = TextEditingController();
  var focusDescribe = FocusNode();
  var controllerDescribe = TextEditingController();
  var isShowErrorTextNameExercise = false;
  var isShowErrorTextDate= false;
  var isShowErrorTextTime= false;
  var coefficient = ''.obs;
  var isExpand = true.obs;
  var typeExercise = 'Trắc nghiệm & tự luận'.obs;
  String getTempIFSCValidation(String text) {
    return text.length > 7 ? "This line is helper text" : "";
  }
  var outputDateFormat = DateFormat('dd/MM/yyyy');

  var listStatus = <String>[].obs;
  var indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  TabController? tabController ;
  ScrollController? scrollController;
  bool fixedScroll = false;
  final List<Widget> myTabs = [
    const Tab(text: 'Câu hỏi',),
    const Tab(text: 'Đăng Tải Tệp'),
    const Tab(text: 'Gắn Link'),
  ];
  @override
  void onInit() {
    scrollController = ScrollController();
    tabController = TabController(length: 3, vsync: this);
    super.onInit();
  }

  @override
  dispose() {
    tabController?.dispose();
    scrollController?.dispose();
    super.dispose();
  }


  onPageViewChange(int page) {
    indexClick.value = page;
    if (page != 0) {
      indexClick.value - 1;
    } else {
      indexClick.value = 0;
    }
  }

  getColorButtonFileUpload(){
    if(Get.isRegistered<FileUploadExerciseController>()){
      if(Get.find<FileUploadExerciseController>().files.isNotEmpty &&Get.find<FileUploadExerciseController>().controllerPoint.text != ""){
        return ColorUtils.PRIMARY_COLOR;
      }else{
        return const Color.fromRGBO(177, 177, 177, 1);
      }
    }else{
      return const Color.fromRGBO(177, 177, 177, 1);
    }
  }


}