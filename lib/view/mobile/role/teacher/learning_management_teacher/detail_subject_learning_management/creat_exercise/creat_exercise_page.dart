import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/creat_exercise/file_upload_exercise/file_upload_exercise_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/creat_exercise/question_exercise/question_exercise_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/creat_exercise/question_exercise/question_exercise_page.dart';
import '../../../../../../../commom/app_cache.dart';
import '../../../../../../../commom/constants/date_format.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../../commom/utils/date_time_picker.dart';
import '../../../../../../../commom/utils/time_utils.dart';
import '../../../../../../../commom/widget/text_field_custom.dart';
import '../../../teacher_home_controller.dart';
import '../learning_management_teacher_controller.dart';
import 'attach_link_exercise/attach_link_exercise_page.dart';
import 'creat_exercise_controller.dart';
import 'file_upload_exercise/file_upload_exercise_page.dart';

class CreateExercisePage extends GetWidget<CreateExerciseController> {
  @override
  final controller = Get.put(CreateExerciseController());

   CreateExercisePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              comeToHome();
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Text(
          'Tạo Bài Tập',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body:Stack(
        children: [
          NestedScrollView(
            controller: controller.scrollController,
            physics: const ClampingScrollPhysics(),
            headerSliverBuilder: (context, value) {
              return [
                SliverToBoxAdapter(
                  child: Obx(() => Container(
                    margin: EdgeInsets.only(top: 8.h),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 12.w),
                          child: GetBuilder<CreateExerciseController>(builder: (controller) {
                            return MyOutlineBorderTextFormFieldNhat(
                              enable: true,
                              focusNode: controller.focusNameExercise,
                              iconPrefix: null,
                              iconSuffix: null,
                              state: StateType.DEFAULT,
                              labelText: "Tên Bài Tập",
                              autofocus: false,
                              controller: controller.controllerNameExercise,
                              helperText: "Vui lòng nhập tên bài tập",
                              showHelperText: controller.isShowErrorTextNameExercise,
                              textInputAction: TextInputAction.next,
                              ishowIconPrefix: false,
                              keyboardType: TextInputType.text,
                            );
                          },),
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 12.w),
                          child: GetBuilder<CreateExerciseController>(builder: (controller) {
                            return MyOutlineBorderTextFormFieldNhat(
                              enable: true,
                              focusNode: controller.focusDescribe,
                              iconPrefix: null,
                              iconSuffix: null,
                              state: StateType.DEFAULT,
                              labelText: "Mô Tả",
                              autofocus: false,
                              controller: controller.controllerDescribe,
                              helperText: "",
                              showHelperText: false,
                              textInputAction: TextInputAction.next,
                              ishowIconPrefix: false,
                              keyboardType: TextInputType.text,
                            );
                          },),
                        ),

                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 16.w,vertical: 8.h),
                          margin: EdgeInsets.symmetric(horizontal: 12.w),
                          height: 56,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: const Color.fromRGBO(192, 192, 192, 1)),
                              borderRadius: BorderRadius.circular(8)),
                          child: Theme(
                            data: Theme.of(context).copyWith(
                              canvasColor: Colors.white,
                            ),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                isExpanded: true,
                                iconSize: 0,
                                icon: const Visibility(
                                    visible: false,
                                    child: Icon(Icons.arrow_downward)),
                                elevation: 16,
                                hint: controller.typeExercise.value != ""
                                    ? Row(
                                  children: [
                                    Text(
                                      controller.typeExercise.value,
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w400,
                                          color: ColorUtils.PRIMARY_COLOR),
                                    ),
                                    Expanded(child: Container()),
                                    const Icon(
                                      Icons.keyboard_arrow_down,
                                      color: Colors.black,
                                      size: 18,
                                    )
                                  ],
                                )
                                    : Row(
                                  children: [
                                    Text(
                                      'Loại bài tập',
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black),
                                    ),
                                    Expanded(child: Container()),
                                    const Icon(
                                      Icons.keyboard_arrow_down,
                                      color: Colors.black,
                                      size: 18,
                                    )
                                  ],
                                ),
                                items: [
                                  'Trắc nghiệm',
                                  'Tự luận',
                                  'Trắc nghiệm & tự luận',
                                ].map(
                                      (value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(
                                        value,
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w400,
                                            color: ColorUtils.PRIMARY_COLOR),
                                      ),
                                    );
                                  },
                                ).toList(),
                                onChanged: (String? value) {
                                  controller.typeExercise.value = value!;
                                  switch(controller.typeExercise.value){
                                    case "Trắc nghiệm":
                                      for(int i = 0;i<Get.find<QuestionExerciseController>().groupValue.length;i++){
                                        Get.find<QuestionExerciseController>().groupValue[i] = 0;
                                      }
                                      break;
                                    case "Tự luận":
                                      for(int i = 0;i<Get.find<QuestionExerciseController>().groupValue.length;i++){
                                        Get.find<QuestionExerciseController>().groupValue[i] = 1;
                                      }
                                      break;
                                    case "Trắc nghiệm & tự luận":
                                      for(int i = 0;i<Get.find<QuestionExerciseController>().groupValue.length;i++){
                                        Get.find<QuestionExerciseController>().groupValue[i] = 0;
                                      }
                                      break;
                                  }
                                },
                              ),
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        GetBuilder<CreateExerciseController>(builder: (controller) {
                          return Container(
                            margin: EdgeInsets.symmetric(horizontal: 12.w),
                            child: Column(

                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child: Container(
                                          margin: EdgeInsets.only(right: 4.w),
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.only(left: 8.w),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: const BorderRadius.all(
                                                  Radius.circular(6.0)),
                                              border: Border.all(
                                                width: 1,
                                                style: BorderStyle.solid,
                                                color: const Color.fromRGBO(
                                                    192, 192, 192, 1),
                                              )),
                                          child: TextFormField(
                                            keyboardType: TextInputType.multiline,
                                            maxLines: null,
                                            style: TextStyle(
                                              fontSize: 12.0.sp,
                                              color:
                                              const Color.fromRGBO(26, 26, 26, 1),
                                            ),
                                            onTap: () {
                                              selectDateTimeEnd(
                                                  controller
                                                      .controllerDateEnd.value.text,
                                                  DateTimeFormat.formatDateShort,
                                                  context);
                                            },
                                            readOnly: true,
                                            cursorColor: ColorUtils.PRIMARY_COLOR,
                                            controller: controller.controllerDateEnd,
                                            decoration: InputDecoration(
                                              suffixIcon: Container(
                                                margin: EdgeInsets.only(bottom: 4.h),
                                                child: SizedBox(
                                                  height: 14,
                                                  width: 14,
                                                  child: Container(
                                                    margin: EdgeInsets.zero,
                                                    child: SvgPicture.asset(
                                                      "assets/images/icon_date_picker.svg",
                                                      fit: BoxFit.scaleDown,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              label: const Text("Hạn nộp"),
                                              border: InputBorder.none,
                                              labelStyle: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      177, 177, 177, 1),
                                                  fontSize: 12.sp,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily:
                                                  'assets/font/static/Inter-Medium.ttf'),
                                            ),
                                          ),
                                        )),
                                    Expanded(
                                        flex: 1,
                                        child:  Container(
                                          margin: EdgeInsets.only(left: 4.w),
                                          alignment: Alignment.centerLeft,
                                          padding: EdgeInsets.only(left: 8.w),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: const BorderRadius.all(
                                                  Radius.circular(6.0)),
                                              border: Border.all(
                                                width: 1,
                                                style: BorderStyle.solid,
                                                color: const Color.fromRGBO(
                                                    192, 192, 192, 1),
                                              )),
                                          child: TextFormField(
                                            keyboardType: TextInputType.multiline,
                                            maxLines: null,
                                            style: TextStyle(
                                              fontSize: 12.0.sp,
                                              color:
                                              const Color.fromRGBO(26, 26, 26, 1),
                                            ),
                                            onTap: () {
                                              selectTime(
                                                  controller
                                                      .controllerTimeEnd.value.text,
                                                  DateTimeFormat.formatTime);
                                            },
                                            readOnly: true,
                                            cursorColor: ColorUtils.PRIMARY_COLOR,
                                            controller: controller.controllerTimeEnd,
                                            decoration: InputDecoration(
                                              suffixIcon: Container(
                                                margin: EdgeInsets.only(bottom: 4.h),
                                                child: SizedBox(
                                                  height: 14,
                                                  width: 14,
                                                  child: Container(
                                                    margin: EdgeInsets.zero,
                                                    child: SvgPicture.asset(
                                                      "assets/images/icon_date_picker.svg",
                                                      fit: BoxFit.scaleDown,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              label: const Text("Giờ nộp"),
                                              border: InputBorder.none,
                                              labelStyle: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      177, 177, 177, 1),
                                                  fontSize: 12.sp,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily:
                                                  'assets/font/static/Inter-Medium.ttf'),
                                            ),
                                          ),
                                        )
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Visibility(
                                        visible: controller.isShowErrorTextDate,
                                        child: Container(
                                            padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                            child: const Text(
                                              "Vui lòng nhập ngày nộp",
                                              style: TextStyle(color: Colors.red),
                                            )),
                                      ),),
                                    Expanded(
                                      flex: 1,
                                      child: Visibility(
                                        visible: controller.isShowErrorTextTime,
                                        child: Container(
                                            padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                            child: const Text(
                                              "Vui lòng nhập giờ nộp",
                                              style: TextStyle(color: Colors.red),
                                            )),
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          );
                        },),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                      ],
                    ),
                  )),
                ),
              ];
            },
            body:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                TabBar(
                  controller: controller.tabController,
                  labelColor: ColorUtils.PRIMARY_COLOR,
                  isScrollable: true,
                  indicatorColor: Colors.transparent,
                  unselectedLabelColor: const Color.fromRGBO(177, 177, 177, 1),
                  tabs: controller.myTabs,
                  onTap: (value) {
                    controller.tabController!.index = value;
                    controller.update();
                  },
                ),
                GetBuilder<CreateExerciseController>(builder: (controller) {
                  return Expanded(child: listWidget.elementAt(controller.tabController!.index));
                },)
              ],
            ),
          ),
          GetBuilder<CreateExerciseController>(builder: (controller) {
            return controller.tabController?.index == 1?Positioned(
                bottom: 0.h,
                right: 0.w,
                left: 0.w,
                child:  InkWell(
                  onTap: () {
                    if(Get.find<FileUploadExerciseController>().files.isNotEmpty &&Get.find<FileUploadExerciseController>().controllerPoint.text != ""){
                      if (Get.find<CreateExerciseController>().controllerNameExercise.text.trim() == "") {
                        Get.find<CreateExerciseController>().isShowErrorTextNameExercise = true;
                        Get.find<CreateExerciseController>().update();
                      } else {
                        Get.find<CreateExerciseController>().isShowErrorTextNameExercise = false;
                        Get.find<CreateExerciseController>().update();
                        if (Get.find<CreateExerciseController>().controllerDateEnd.text.trim() == "") {
                          Get.find<CreateExerciseController>().isShowErrorTextDate = true;
                          Get.find<CreateExerciseController>().update();
                        } else {
                          Get.find<CreateExerciseController>().isShowErrorTextDate = false;
                          Get.find<CreateExerciseController>().update();
                          if (Get.find<CreateExerciseController>().typeExercise.value == "") {
                            AppUtils.shared.showToast("Vui lòng chọn loại bài tập");
                          } else {
                            if (Get.find<CreateExerciseController>().controllerTimeEnd.text.trim() == "") {
                              Get.find<CreateExerciseController>().isShowErrorTextTime = true;
                              Get.find<CreateExerciseController>().update();
                            }else{
                              Get.find<CreateExerciseController>().isShowErrorTextTime = false;
                              Get.find<CreateExerciseController>().update();
                              if(Get.find<FileUploadExerciseController>().files.isEmpty){
                                AppUtils.shared.showToast("Vui lòng thêm file");
                              }else{
                                if (Get.find<FileUploadExerciseController>().controllerPoint.text.trim() == "") {
                                  Get.find<FileUploadExerciseController>().isShowErrorPoint = true;
                                  Get.find<FileUploadExerciseController>().errorTextPoint.value = "Vui lòng nhập điểm cho câu hỏi";
                                  controller.update();
                                }else{
                                  Get.find<FileUploadExerciseController>().isShowErrorPoint= false;
                                  controller.update();
                                  if(Get.find<FileUploadExerciseController>().controllerPoint.text.trim().substring(Get.find<FileUploadExerciseController>().controllerPoint.text.trim().length - 1) == "."){
                                    Get.find<FileUploadExerciseController>().isShowErrorPoint = true;
                                    Get.find<FileUploadExerciseController>().errorTextPoint.value = "Vui lòng nhập đúng định dạng điểm";
                                    controller.update();
                                  }
                                  else if (double.tryParse(Get.find<FileUploadExerciseController>().controllerPoint.text.trim()) == null) {
                                    Get.find<FileUploadExerciseController>().isShowErrorPoint = true;
                                    Get.find<FileUploadExerciseController>().errorTextPoint.value = "Vui lòng nhập đúng định dạng điểm";
                                    Get.find<FileUploadExerciseController>().update();
                                  }else{
                                    Get.find<FileUploadExerciseController>().createExercise(
                                        Get.find<CreateExerciseController>().controllerNameExercise.text.trim()
                                        , DateFormat("dd/MM/yyyy HH:mm").parse("${Get.find<CreateExerciseController>().controllerDateEnd.text.trim()} ${Get.find<CreateExerciseController>().controllerTimeEnd.text.trim()}").millisecondsSinceEpoch
                                        , Get.find<CreateExerciseController>().controllerDescribe.text.trim()
                                        , Get.find<FileUploadExerciseController>().setTypeExercise(Get.find<CreateExerciseController>().typeExercise.value)
                                        , Get.find<LearningManagementTeacherController>().detailSubject.value.id
                                        , AppCache().userId
                                        , Get.find<TeacherHomeController>().currentClass.value.classId
                                        , Get.find<FileUploadExerciseController>().filesUploadExercise
                                        , double.tryParse(Get.find<FileUploadExerciseController>().controllerPoint.text.trim()));
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  },
                  child: GetBuilder<FileUploadExerciseController>(builder: (controller) {
                    return Container(
                      color: Colors.white,
                      margin: EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.w),
                      width: double.infinity,
                      child: Container(
                        height: 40.h,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          border: Border.all(
                              color: Get.find<CreateExerciseController>().getColorButtonFileUpload()
                          ),
                        ),
                        alignment: Alignment.center,
                        child: Text("Xác nhận",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Get.find<CreateExerciseController>().getColorButtonFileUpload()),
                            textAlign: TextAlign.center),
                      ),
                    );
                  },),
                )):const SizedBox();
          },)
        ],
      ),


    );
  }




  final List<Widget> listWidget = [
    QuestionExercisePage(),
    FileUploadExercisePage(),
    AttachLinkExercisePage(),
  ];


  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
            initialDate: DateTime(DateTime.now().year - 10),
            isDisablePreviousDay: true)
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (int.parse(date.substring(6, 10)) <= DateTime.now().year &&
        int.parse(date.substring(3, 5)) <= DateTime.now().month &&
        int.parse(date.substring(0, 2)) < DateTime.now().day) {
      AppUtils.shared.showToast("Vui lòng không chọn ngày trong quá khứ");
      controller.controllerDateEnd.text = "";
    }else{
      controller.controllerDateEnd.text = date;
    }
  }


  selectTime(stringTime,format) async {
    var now = DateTime.now();
    if (!stringTime.isEmpty) {
      now = TimeUtils.convertStringToDate(stringTime, format);
    }
    var time = "";
    await DateTimePicker.showTimePicker(Get.context!, now).then((times) {
      time =
          TimeUtils.convertDateTimeToFormat(times, DateTimeFormat.formatTime);
    });

    if (time.isNotEmpty) {
      controller.controllerTimeEnd.text = time;
    }
  }
}
