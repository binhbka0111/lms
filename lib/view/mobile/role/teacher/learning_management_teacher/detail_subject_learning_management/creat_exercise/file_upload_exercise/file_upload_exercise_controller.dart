import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/req_file.dart';
import 'dart:io';
import '../../../../../../../../data/model/res/exercise/exercise_file.dart';
import '../../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../../../data/repository/exercise/excercise_repo.dart';
import '../../../../../../../../data/repository/file/file_repo.dart';

class FileUploadExerciseController extends GetxController{
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  final FileRepo fileRepo = FileRepo();
  var exerciseFile = ExerciseFile().obs;
  var filesUploadExercise = <ResponseFileUpload>[].obs;
  var files = <ReqFile>[].obs;
  var isShowErrorPoint = false;
  var errorTextPoint = "".obs;
  var focusPoint = FocusNode();
  var controllerPoint = TextEditingController();
  uploadFile(file) async {
    showDialogUploadFile();
    var fileList = <File>[];
    for (var element in file) {
      fileList.add(element.file!);
    }

   await fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;

        filesUploadExercise.addAll(listResFile);
        filesUploadExercise.refresh();
        files.addAll(file);
        for(int i =0;i<files.length;i++){
          files[i].url = filesUploadExercise[i].link;
        }
        files.refresh();
        listResFile.clear();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_GREEN_BOLD);
      }
    });
    Get.back();
  }



  createExercise(title, deadline, description, typeExercise, subjectId, teacherId, classId, files,scoreOfExercise) {
    exerciseRepo.createExerciseFile(title,deadline, description, typeExercise, subjectId, teacherId, classId, files,scoreOfExercise).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Tạo bài tập thành công");
        Get.back();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tạo bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_GREEN_BOLD);
      }
    });
  }

  setTypeExercise(type){
    switch(type){
      case "Trắc nghiệm & tự luận":
        return "ALL";
      case "Trắc nghiệm":
        return "SELECTED_RESPONSE";
      case "Tự luận":
        return "CONSTRUCTED_RESPONSE";
      default:
        return "";
    }
  }

}