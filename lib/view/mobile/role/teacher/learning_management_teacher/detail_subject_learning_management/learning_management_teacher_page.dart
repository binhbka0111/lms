import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:html/parser.dart';
import 'package:slova_lms/commom/widget/list_view_image.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/learning_management_teacher_controller.dart';
import '../../../../../../commom/app_cache.dart';
import '../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../commom/utils/color_utils.dart';
import '../../../../../../routes/app_pages.dart';
import '../../../../home/home_controller.dart';
import '../../teacher_home_controller.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';



class SubjectLearningManagementTeacherPage extends GetWidget<LearningManagementTeacherController>{

  const SubjectLearningManagementTeacherPage({super.key});

  @override
  Widget build(BuildContext context) {
    return 
      Scaffold(
      backgroundColor: const Color.fromRGBO(249, 249, 249, 1),
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              comeToHome();
              },
            icon: const Icon(Icons.home, size: 22,),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title:  Text(
          'Quản Lý Học Tập',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontWeight: FontWeight.w500,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body:
      SingleChildScrollView(
          child:
          Container(
            margin: EdgeInsets.only(top: 16.h, right: 16.w, left: 16.w),
            child:
            Obx(() => Column(
              children: [
                Container(
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(255, 255, 255, 1),
                        borderRadius: BorderRadius.circular(6)),
                    padding: const EdgeInsets.all(16),
                    child:
                    Column(
                      children: [
                        InkWell(
                          onTap: (){
                            Get.find<LearningManagementTeacherController>().isVisibleClass.value=! Get.find<LearningManagementTeacherController>().isVisibleClass.value;
                          },
                          child:Row(
                            children: [
                              Text(
                                'Lớp Học',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 16.sp, fontWeight: FontWeight.w500),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 4.w),
                                padding: EdgeInsets.only(
                                    right: 8.r, left: 8.r, top: 2.h, bottom: 2.h),
                                alignment: Alignment.center,
                                height: 16.sp,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: ColorUtils.PRIMARY_COLOR,
                                ),
                                child: Text(
                                  '${Get.find<TeacherHomeController>().currentClass.value.name}',
                                  style:
                                  TextStyle(color: Colors.white, fontSize: 10.sp, fontWeight: FontWeight.w500),
                                ),
                              ),
                              Expanded(child: Container()),
                              Get.find<LearningManagementTeacherController>().isVisibleClass.value ? const Icon(Icons.keyboard_arrow_up, size: 24, color: Color.fromRGBO(26, 26, 26, 1),) : const Icon(
                                Icons.arrow_forward_ios,
                                size: 14,
                                color: Color.fromRGBO(26, 26, 26, 1),
                              )
                            ],
                          ),
                        ),
                        Visibility(
                            visible: Get.find<LearningManagementTeacherController>().isVisibleClass.value,
                            child: Column(
                              children: [
                                Padding(padding: EdgeInsets.only(top: 25.h)),
                                InkWell(
                                  onTap: () {
                                    checkClickFeature(
                                    Get.find<HomeController>().userGroupByApp,
                                            ()=> Get.find<LearningManagementTeacherController>().clickListStudent(),
                                        StringConstant.FEATURE_STUDENT_LIST);
                                  },
                                  child: Row(
                                    children: [
                                      Text(
                                        '1. Danh Sách Học Sinh Lớp',
                                        style: TextStyle(
                                            color: const Color.fromRGBO(26, 26, 26, 1),
                                            fontSize: 14.sp,fontWeight: FontWeight.w400),
                                      ),
                                      Expanded(child: Container()),
                                      const InkWell(
                                        child: Icon(
                                          Icons.arrow_forward_ios,
                                          color: ColorUtils.PRIMARY_COLOR,
                                          size: 14,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(padding: EdgeInsets.only(top: 25.h)),
                                Column(
                                  children: [
                                    InkWell(
                                      onTap: (){
                                        Get.find<LearningManagementTeacherController>().isVisibleSubjectScore.value=!Get.find<LearningManagementTeacherController>().isVisibleSubjectScore.value;
                                      },
                                      child:
                                      Row(
                                        children: [
                                          Text(
                                            '2. Điểm Môn Học',
                                            style: TextStyle(
                                                color: const Color.fromRGBO(26, 26, 26, 1),
                                                fontSize: 14.sp,fontWeight: FontWeight.w400),
                                          ),
                                          Expanded(child: Container()),
                                          const Icon(
                                            Icons.arrow_forward_ios,
                                            color: ColorUtils.PRIMARY_COLOR,
                                            size: 14,
                                          ),
                                        ],
                                      ),
                                    ),

                                    Visibility(
                                        visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_TRANSCRIPT),
                                        child: Visibility(
                                        visible: Get.find<LearningManagementTeacherController>().isVisibleSubjectScore.value,
                                        child:
                                        Column(
                                          children: [
                                            InkWell(
                                              onTap: (){
                                                Get.find<LearningManagementTeacherController>().clickTranscriptSubject();
                                              },
                                              child:    Container(
                                                margin: EdgeInsets.only(top: 17.h),
                                                padding: EdgeInsets.only(left: 16.h),
                                                height: 40,
                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(12),
                                                    color:Get.find<LearningManagementTeacherController>().indexItemSubjectScore.value == 0? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(255, 255, 255, 1),
                                                    border: Border.all(
                                                        color: const Color.fromRGBO(
                                                            239, 239, 239, 1))),
                                                child: Row(
                                                  children: [
                                                    Get.find<LearningManagementTeacherController>().indexItemSubjectScore.value == 0 ? Image.asset('assets/images/icon_white.png',width: 12.h,height: 12.h): Image.asset('assets/images/icon_black.png',width: 12.h,height: 12.h) ,
                                                    Padding(
                                                        padding: EdgeInsets.only(left: 9.w)),
                                                    Text(
                                                      "Điểm môn học",
                                                      style: TextStyle(
                                                          color:Get.find<LearningManagementTeacherController>().indexItemSubjectScore.value==0? const Color.fromRGBO(255, 255, 255, 1): const Color.fromRGBO(90, 90, 90, 1) ,
                                                          fontSize: 14.sp,fontWeight: FontWeight.w400),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                    ))
                                  ],
                                )
                              ],
                            ))
                      ],
                    )
                ),
                Padding(padding: EdgeInsets.only(top: 16.h)),
                Container(
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(255, 255, 255, 1),
                        borderRadius: BorderRadius.circular(6)),
                    padding: const EdgeInsets.all(16),
                    child:
                    Column(
                      children: [
                        InkWell(
                          onTap: (){
                            Get.find<LearningManagementTeacherController>().isVisibleNotify.value=!Get.find<LearningManagementTeacherController>().isVisibleNotify.value;
                          },
                          child:   Row(
                            children: [
                              Text(
                                'Thông Báo',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1), fontSize: 16.sp, fontWeight: FontWeight.w500),
                              ),
                              Expanded(child: Container()),
                              Visibility(
                                  visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_LIST_NOTIFY),
                                  child: InkWell(
                                onTap: () {
                                  Get.toNamed(Routes.listAllNotifySubjectPage);
                                },
                                child: Text('Xem tất cả',
                                  style:  TextStyle(color: ColorUtils.PRIMARY_COLOR,fontSize: 14.sp),
                                ),
                              )),
                              Padding(padding: EdgeInsets.only(right: 8.w)),
                              controller.isVisibleNotify.value
                                  ? const Icon(Icons.keyboard_arrow_up, size: 24, color: Color.fromRGBO(26, 26, 26, 1),)
                                  : const Icon(Icons.arrow_forward_ios, size: 14, color: Color.fromRGBO(26, 26, 26, 1),),
                            ],
                          ),
                        ),
                       Visibility(
                           visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_LIST_NOTIFY),
                           child:  Visibility(
                           visible:Get.find<LearningManagementTeacherController>().isVisibleNotify.value,
                           child: Get.find<LearningManagementTeacherController>().listNotificationSubject.isNotEmpty
                               ?Column(
                             children: [
                               Container(
                                 margin: EdgeInsets.only(top:24.h),
                                 child:  ListView.builder(
                                     shrinkWrap: true,
                                     physics: const NeverScrollableScrollPhysics(),
                                     itemCount: (Get.find<LearningManagementTeacherController>().listNotificationSubject.length>3)?3:Get.find<LearningManagementTeacherController>().listNotificationSubject.length,
                                     itemBuilder:(context, index){
                                       var listImage = Get.find<LearningManagementTeacherController>().listNotificationSubject[index].files!.where((element) => element.ext == "png" ||
                                           element.ext == "jpg" ||
                                           element.ext == "jpeg" ||
                                           element.ext == "gif" ||
                                           element.ext == "bmp").toList().obs;
                                       var listNotImage = Get.find<LearningManagementTeacherController>()
                                           .listNotificationSubject[index].files!
                                           .where((element) =>
                                       element.ext != "png" &&
                                           element.ext != "jpg" &&
                                           element.ext != "jpeg" &&
                                           element.ext != "gif" &&
                                           element.ext != "bmp")
                                           .toList().obs;
                                       return Obx(() => Container(
                                         margin: const EdgeInsets.only(bottom: 10),
                                         child: Column(
                                           crossAxisAlignment: CrossAxisAlignment.start,
                                           children: [
                                             Row(
                                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                               crossAxisAlignment: CrossAxisAlignment.start,
                                               children: [
                                                 SizedBox(
                                                   width: 240.w,
                                                   child: Text('${Get.find<LearningManagementTeacherController>().listNotificationSubject[index].title}',
                                                     style: TextStyle(color: ColorUtils.PRIMARY_COLOR,fontSize: 12.sp),
                                                     overflow: Get.find<LearningManagementTeacherController>().listExtend[index]?TextOverflow.visible:TextOverflow.ellipsis,),
                                                 ),
                                                 Visibility(
                                                     visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_NOTIFY_PIN),
                                                     child: Column(
                                                       children: [
                                                         Visibility(
                                                             visible: Get.find<LearningManagementTeacherController>().listNotificationSubject[index].status == "PIN",
                                                             child: InkWell(
                                                               onTap: () {
                                                                 Get.find<LearningManagementTeacherController>().unPinNotify(Get.find<LearningManagementTeacherController>().listNotificationSubject[index].id);
                                                               },
                                                               child: Text("Bỏ Ghim",style: TextStyle(color: const Color.fromRGBO(255, 69, 89, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                                             )),
                                                         Visibility(
                                                             visible: Get.find<LearningManagementTeacherController>().listNotificationSubject[index].status == "NOT_PIN",
                                                             child: InkWell(
                                                               onTap: () {
                                                                 Get.find<LearningManagementTeacherController>().pinNotify(Get.find<LearningManagementTeacherController>().listNotificationSubject[index].id);
                                                               },
                                                               child: Text("Ghim",style: TextStyle(color: ColorUtils.PRIMARY_COLOR,fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                                             )),
                                                         Padding(padding: EdgeInsets.only(top: 4.h)),

                                                       ],
                                                     ))
                                               ],
                                             ),
                                             Padding(padding: EdgeInsets.only(top: 8.h)),
                                             SizedBox(
                                               width: 280.w,
                                               child: Text('${parse(parse(Get.find<LearningManagementTeacherController>().listNotificationSubject[index].content).body?.text).documentElement?.text}'
                                                 ,style: TextStyle(color: Colors.black, fontSize: 14.sp, fontWeight: FontWeight.w400)
                                                 ,overflow: Get.find<LearningManagementTeacherController>().listExtend[index]?TextOverflow.visible:TextOverflow.ellipsis,),
                                             ),

                                             Visibility(
                                                 visible: Get.find<LearningManagementTeacherController>().listExtend[index],
                                                 child: ListViewShowImage.showGridviewImage(listImage)),
                                             Visibility(
                                                 visible: Get.find<LearningManagementTeacherController>().listExtend[index],
                                                 child: ListViewShowImage.showListViewNotImage(listNotImage)),
                                             Visibility(
                                               visible: Get.find<LearningManagementTeacherController>()
                                                   .listNotificationSubject[index].files!.isNotEmpty||
                                                   textSize('${Get.find<LearningManagementTeacherController>().listNotificationSubject[index].title}',TextStyle(color: ColorUtils.PRIMARY_COLOR,fontSize: 12.sp))>240.w
                                                 || textSize('${parse(parse(Get.find<LearningManagementTeacherController>().listNotificationSubject[index].content).body?.text).documentElement?.text}',TextStyle(color: Colors.black, fontSize: 14.sp, fontWeight: FontWeight.w400))>280.w,
                                                 child: Row(
                                               mainAxisAlignment: MainAxisAlignment.end,
                                               children: [
                                                 Row(
                                                   mainAxisAlignment: MainAxisAlignment.end,
                                                   children: [
                                                     InkWell(
                                                       onTap: (){
                                                         Get.find<LearningManagementTeacherController>().listExtend[index] =! Get.find<LearningManagementTeacherController>().listExtend[index];
                                                       },
                                                       child:
                                                       Row(
                                                         children: [
                                                           Text( Get.find<LearningManagementTeacherController>().listExtend[index] ? 'Thu Gọn': 'Mở Rộng',style: const TextStyle(color: Color.fromRGBO(26, 59, 112, 1),fontSize: 12),),
                                                           Icon( Get.find<LearningManagementTeacherController>().listExtend[index] ?Icons.keyboard_arrow_up_outlined : Icons.keyboard_arrow_down_outlined,size: 24,color: const Color.fromRGBO(26, 59, 112, 1),),
                                                         ],
                                                       ),)
                                                   ],),
                                               ],
                                             )),
                                             const Divider()
                                           ],
                                         ),
                                       ));

                                     }
                                 ),
                               ),
                               Visibility(
                                   visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_NOTIFY_CREATE),
                                   child: Row(
                                     children: [
                                       InkWell(
                                         onTap: () {
                                           Get.toNamed(Routes.createNewNotifySubjectPage);
                                         },
                                         child: Row(
                                           children: [
                                             Text('Tạo Thông Báo Mới',style: TextStyle(color: ColorUtils.PRIMARY_COLOR, fontSize: 12.sp, fontWeight: FontWeight.w400),),
                                             const Icon(Icons.add,size: 24,color: ColorUtils.PRIMARY_COLOR,),
                                           ],
                                         ),
                                       ),
                                     ],
                                   )),

                             ],)
                               :Column(
                             children: [
                               Padding(padding: EdgeInsets.only(top: 16.h)),
                               Text("Môn học chưa có thông báo nào để hiện thị",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                               Padding(padding: EdgeInsets.only(top: 16.h)),
                               InkWell(
                                 onTap: () {
                                   Get.toNamed(Routes.createNewNotifySubjectPage);
                                 },
                                 child: Visibility(
                                   visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_NOTIFY_CREATE),
                                   child: Row(
                                     children: [
                                       Text('Tạo Thông Báo Mới',style: TextStyle(color: ColorUtils.PRIMARY_COLOR, fontSize: 12.sp, fontWeight: FontWeight.w400),),
                                       const Icon(Icons.add,size: 24,color: ColorUtils.PRIMARY_COLOR,),
                                     ],
                                   ),
                                 ),
                               )
                             ],
                           )))
                      ],
                    )
                ),
                const Padding(padding: EdgeInsets.only(top: 16)),
                Container(
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(255, 255, 255, 1),
                        borderRadius: BorderRadius.circular(6)),
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      children: [
                        InkWell(
                          onTap: (){
                            Get.find<LearningManagementTeacherController>().isVisibleSubject.value=! Get.find<LearningManagementTeacherController>().isVisibleSubject.value;
                          },
                          child:    Row(
                            children: [
                              Text(
                                'Môn Học',
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 16.sp, fontWeight: FontWeight.w500),
                              ),
                              Container(
                                margin: const EdgeInsets.only(left: 4),
                                padding: EdgeInsets.only(
                                    right: 8.w, left: 8.w, top: 2.h, bottom: 2.h),
                                alignment: Alignment.center,
                                height: 16.sp,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: ColorUtils.PRIMARY_COLOR,
                                ),
                                child: Text(
                                  '${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}',
                                  style:
                                  TextStyle(color: Colors.white, fontSize: 10.sp, fontWeight: FontWeight.w500),
                                ),
                              ),
                              Expanded(child: Container()),
                              Get.find<LearningManagementTeacherController>().isVisibleSubject.value ? const Icon(Icons.keyboard_arrow_up, size: 24, color: Color.fromRGBO(26, 26, 26, 1),) : const Icon(
                                Icons.arrow_forward_ios,
                                size: 14,
                                color: Color.fromRGBO(26, 26, 26, 1),
                              )
                            ],
                          ),
                        ),

                        Visibility(
                            visible: Get.find<LearningManagementTeacherController>().isVisibleSubject.value,
                            child:Column(
                              children: [
                                Visibility(
                                    visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_VIEW_EXERCISE),
                                    child: InkWell(
                                  onTap: (){
                                    Get.find<LearningManagementTeacherController>().indexItemSubject.value= 0;
                                    if(Get.find<LearningManagementTeacherController>().indexItemSubject.value==0){
                                      Get.toNamed(Routes.viewExerciseTeacherPage);
                                    }
                                  },
                                  child:  Container(
                                    margin: const EdgeInsets.only(top: 17),
                                    padding: const EdgeInsets.only(left: 16),
                                    height: 40,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                        color:  Get.find<LearningManagementTeacherController>().indexItemSubject.value == 0? ColorUtils.PRIMARY_COLOR:const Color.fromRGBO(255, 255, 255, 1),
                                        border: Border.all(
                                            color: const Color.fromRGBO(
                                                239, 239, 239, 1))),
                                    child: Row(
                                      children: [
                                        Get.find<LearningManagementTeacherController>().indexItemSubject.value==0? Image.asset('assets/images/icon_white.png',width: 12.h,height: 12.h) :Image.asset('assets/images/icon_black.png',width: 12.h,height: 12.h) ,
                                        Padding(
                                            padding: EdgeInsets.only(left: 9.w)),
                                        Text("Xem Bài Tập",
                                          style: TextStyle(
                                              color: Get.find<LearningManagementTeacherController>().indexItemSubject.value==0 ? const Color.fromRGBO(255, 255, 255, 1): const Color.fromRGBO(90, 90, 90, 1) ,
                                              fontSize: 14.sp, fontWeight: FontWeight.w400),
                                        )
                                      ],
                                    ),
                                  ),
                                )),
                                Visibility(
                                    visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_EXERCISE_CREATE),
                                    child: Visibility(
                                    visible: Get.find<LearningManagementTeacherController>().detailSubject.value.teacher?.id == AppCache().userId,
                                    child: InkWell(
                                      onTap: (){
                                        Get.find<LearningManagementTeacherController>().indexItemSubject.value= 1;
                                        if(Get.find<LearningManagementTeacherController>().indexItemSubject.value==1){
                                          Get.toNamed(Routes.createExercisePage);
                                        }
                                      },
                                      child:  Container(
                                        margin: const EdgeInsets.only(top: 17),
                                        padding: const EdgeInsets.only(left: 16),
                                        height: 40,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(12),
                                            color:  Get.find<LearningManagementTeacherController>().indexItemSubject.value==1?ColorUtils.PRIMARY_COLOR:const Color.fromRGBO(255, 255, 255, 1),
                                            border: Border.all(
                                                color: const Color.fromRGBO(
                                                    239, 239, 239, 1))),
                                        child: Row(
                                          children: [
                                            Get.find<LearningManagementTeacherController>().indexItemSubject.value==1?Image.asset('assets/images/icon_white.png',width: 12.h,height: 12.h) :Image.asset('assets/images/icon_black.png',width: 12.h,height: 12.h) ,
                                            Padding(
                                                padding: EdgeInsets.only(left: 9.w)),
                                            Text("Tạo Bài Tập Mới",
                                              style: TextStyle(
                                                  color: Get.find<LearningManagementTeacherController>().indexItemSubject.value==1 ? const Color.fromRGBO(255, 255, 255, 1): const Color.fromRGBO(90, 90, 90, 1) ,
                                                  fontSize: 14.sp, fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                      ),
                                    ))),
                                Visibility(
                                    visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_EXAMS_CREATE),
                                    child: Visibility(
                                    visible: Get.find<LearningManagementTeacherController>().detailSubject.value.teacher?.id == AppCache().userId,
                                    child: InkWell(
                                      onTap: (){
                                        Get.find<LearningManagementTeacherController>().indexItemSubject.value= 2;
                                        if(Get.find<LearningManagementTeacherController>().indexItemSubject.value==2){
                                          Get.toNamed(Routes.createExamPage);
                                        }
                                      },
                                      child:  Container(
                                        margin: const EdgeInsets.only(top: 17),
                                        padding: const EdgeInsets.only(left: 16),
                                        height: 40,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(12),
                                            color:  Get.find<LearningManagementTeacherController>().indexItemSubject.value==2?ColorUtils.PRIMARY_COLOR:const Color.fromRGBO(255, 255, 255, 1),
                                            border: Border.all(
                                                color: const Color.fromRGBO(
                                                    239, 239, 239, 1))),
                                        child: Row(
                                          children: [
                                            Get.find<LearningManagementTeacherController>().indexItemSubject.value==2?Image.asset('assets/images/icon_white.png',width: 12.h,height: 12.h):Image.asset('assets/images/icon_black.png',width: 12.h,height: 12.h) ,
                                            Padding(
                                                padding: EdgeInsets.only(left: 9.w)),
                                            Text("Tạo Bài Kiểm Tra",
                                              style: TextStyle(
                                                  color: Get.find<LearningManagementTeacherController>().indexItemSubject.value==2 ? const Color.fromRGBO(255, 255, 255, 1): const Color.fromRGBO(90, 90, 90, 1) ,
                                                  fontSize: 14.sp, fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                      ),
                                    ))),
                                Visibility(
                                    visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_VIEW_EXAM),
                                    child: InkWell(
                                  onTap: (){
                                    Get.find<LearningManagementTeacherController>().indexItemSubject.value= 3;
                                    if(Get.find<LearningManagementTeacherController>().indexItemSubject.value==3){
                                      Get.toNamed(Routes.viewExamTeacherPage);
                                    }
                                  },
                                  child:  Container(
                                    margin: const EdgeInsets.only(top: 17),
                                    padding: const EdgeInsets.only(left: 16),
                                    height: 40,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(12),
                                        color:  Get.find<LearningManagementTeacherController>().indexItemSubject.value==3?ColorUtils.PRIMARY_COLOR:const Color.fromRGBO(255, 255, 255, 1),
                                        border: Border.all(
                                            color: const Color.fromRGBO(
                                                239, 239, 239, 1))),
                                    child: Row(
                                      children: [
                                        Get.find<LearningManagementTeacherController>().indexItemSubject.value==3? Image.asset('assets/images/icon_white.png',width: 12.h,height: 12.h) :Image.asset('assets/images/icon_black.png',width: 12.h,height: 12.h) ,
                                        Padding(
                                            padding: EdgeInsets.only(left: 9.w)),
                                        Text("Xem bài kiểm tra",
                                          style: TextStyle(
                                              color: Get.find<LearningManagementTeacherController>().indexItemSubject.value==3 ? const Color.fromRGBO(255, 255, 255, 1): const Color.fromRGBO(90, 90, 90, 1) ,
                                              fontSize: 14.sp, fontWeight: FontWeight.w400),
                                        )
                                      ],
                                    ),
                                  ),
                                )),
                              ],
                            )
                        )
                      ],
                    )),
              ],
            )),
          )
      )

    );

  }


  double textSize(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style), maxLines: 1, textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.width;
  }


}