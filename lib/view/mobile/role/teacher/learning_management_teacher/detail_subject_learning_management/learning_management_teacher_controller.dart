import 'package:get/get.dart';
import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../commom/utils/color_utils.dart';
import '../../../../../../data/base_service/api_response.dart';
import '../../../../../../data/model/common/subject.dart';
import '../../../../../../data/model/res/notification/notification_subject.dart';
import '../../../../../../data/repository/notification_repo/notification_repo.dart';
import '../../../../../../routes/app_pages.dart';
import '../../teacher_home_controller.dart';

class LearningManagementTeacherController extends GetxController{
  RxBool isVisibleClass = false.obs;
  RxBool isVisibleSubject= false.obs;
  RxBool isVisibleSubjectScore= false.obs;
  RxBool isVisibleNotify = false.obs;
  RxInt indexItemSubjectScore = (-1).obs;
   RxInt indexItemSubject = (-1).obs;
  var detailSubject = SubjectRes().obs;
  var listExtend= <bool>[].obs;
  final NotificationRepo _notificationRepo = NotificationRepo();
  var notificationSubject = NotificationSubject().obs;
  var listNotificationSubject = <ItemsNotificationSubject>[].obs;


  @override
  void onInit() {
    super.onInit();
  }


  setUpData(){
    isVisibleClass.value = false;
    isVisibleSubject.value= false;
    isVisibleSubjectScore.value= false;
    isVisibleNotify.value= false;
    indexItemSubjectScore.value =(-1);
    indexItemSubject.value =(-1);
  }

  getListNotify(){
    _notificationRepo.getListNotifySubject(detailSubject.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        notificationSubject.value = value.object!;
        listNotificationSubject.value =  notificationSubject.value.items!;
        for(int i = 0 ; i<listNotificationSubject.length;i++){
          listExtend.add(false);
        }
      }
    });
  }


  pinNotify(notifyId) {
    _notificationRepo.pinNotifySubject(notifyId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Ghim thông báo thành công");
        getListNotify();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Ghim thông báo thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


  unPinNotify(notifyId) {
    _notificationRepo.pinNotifySubject(notifyId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Bỏ ghim thông báo thành công");
        getListNotify();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Bỏ ghim thông báo thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


  clickListStudent(){
    Get.toNamed(Routes.studentListPage, arguments: Get.find<TeacherHomeController>().currentClass.value);
  }

  clickTranscriptSubject(){
    indexItemSubjectScore.value= 0;
    Get.toNamed(Routes.listTranscriptsSubjectTeacherPage);
  }

}