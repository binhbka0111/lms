import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/list_view_image.dart';
import '../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../learning_management_teacher_controller.dart';
import '../list_student_submitted_exercise/list_student_submit_exercise_page.dart';
import '../list_student_un_submit_exercise/list_student_un_submit_exercise_page.dart';
import 'grading_exercise_upload_file_controller.dart';

class GradingExerciseUploadFilePage extends GetWidget<GradingExerciseUploadFileController>{
  @override
  final controller = Get.put(GradingExerciseUploadFileController());

  GradingExerciseUploadFilePage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar:  AppBar(
        actions: [
          IconButton(
            onPressed: () {
              comeToHome();
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title:  Text(
          'Bài Tập Môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}',
          style: const TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: GetBuilder<GradingExerciseUploadFileController>(builder: (controller) {
        return NestedScrollView(
          physics: const ClampingScrollPhysics(),
          controller: controller.scrollController,
          headerSliverBuilder: (context, value) {
            return [
              SliverToBoxAdapter(child: Obx(() => Container(
                margin: EdgeInsets.all(16.h),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${controller.detailExercise.value.title}",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Ngày tạo",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                        Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExercise.value.createdAt!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Hạn nộp",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                        Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExercise.value.deadline!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Ghi chú: ",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                        Text("${controller.detailExercise.value.description}",style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    ListViewShowImage.showGridviewImage(controller.listImage),
                    ListViewShowImage.showListViewNotImage(controller.listNotImage),
                  ],
                ),
              )),),
            ];
          },
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Obx(() => TabBar(
                controller: controller.tabController,
                labelColor: ColorUtils.PRIMARY_COLOR,
                isScrollable: true,
                indicatorColor: Colors.transparent,
                unselectedLabelColor: const Color.fromRGBO(177, 177, 177, 1),
                tabs: [
                  Tab(text: 'Học sinh đã nộp (${controller.studentSubmitted.length})',),
                  Tab(text: 'Học sinh chưa nộp (${controller.studentUnSubmitted.length})')
                ],
              )),
              Expanded(child: TabBarView(
                controller: controller.tabController,
                children: [
                  ListStudentSubmitExercisePage(),
                  ListStudentUnSubmitExercisePage()
                ],
              ))
            ],
          ),
        );
      },)
    );
  }

}