import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../../data/model/res/exercise/student_do_exercise.dart';
import '../../../../../../../../../../data/model/res/exercise/teacherCommentAnswer.dart';
import '../../../../../../../../../../data/repository/exercise/excercise_repo.dart';
import '../../grading_exercise_question/grading_exercise_question_controller.dart';
import '../list_student_submit_exercise_controller.dart';



class StudentDoExerciseQuestionController extends GetxController{
  var focusScoreQuestion = <FocusNode>[];
  var controllerScoreQuestion = <TextEditingController>[];
  var focusScoreQuestionAll = FocusNode();
  var controllerScoreQuestionAll = TextEditingController();
  var focusComment = <FocusNode>[];
  var controllerComment = <TextEditingController>[];
  var focusCommentAll = FocusNode();
  var controllerCommentAll = TextEditingController();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var studentDoExercise = StudentDoExercise().obs;
  var studentID = "".obs;
  var isReady = false.obs;
  var isEvaluate =false.obs;
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  var helperTextScoreQuestion = <String>[];
  var isShowhelperText = <bool>[];
  var helperTextScoreQuestionAll = "Vui lòng nhập điểm số";
  var isShowhelperTextAll = false;
  var  teacherCommentAnswers = <TeacherCommentAnswer>[].obs;
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy');
  var teacherCommentAnswer = TeacherCommentAnswer(teacherComment: "",point: 0.0,questionAnswerId: "").obs;

  @override
  void onInit() {
    var tmpStudentId = Get.arguments;
    if (tmpStudentId != null) {
      studentID.value = tmpStudentId;
    }
    getDetailExercise(Get.find<GradingExerciseQuestionController>().detailExercise.value.id,studentID.value);
    super.onInit();
  }


  getDetailExercise(exerciseId,studentId) {
    exerciseRepo.detailStudentDoExercise(exerciseId,studentId).then((value) {
      if (value.state == Status.SUCCESS) {
        studentDoExercise.value = value.object!;
        for(int i = 0;i<studentDoExercise.value.questionAnswers!.length;i++){
          isShowhelperText.add(false);
          helperTextScoreQuestion.add("");
          focusScoreQuestion.add(FocusNode());
          focusComment.add(FocusNode());
          controllerScoreQuestion.add(TextEditingController());
          controllerComment.add(TextEditingController());
          teacherCommentAnswers.add(TeacherCommentAnswer());
          controllerComment[i].text = studentDoExercise.value.questionAnswers![i].teacherComment??"";
          controllerScoreQuestion[i].text = studentDoExercise.value.questionAnswers![i].point.toString();
        }
        controllerCommentAll.text = studentDoExercise.value.teacherComment??"";
        AppUtils.shared.showToast("Lấy chi tiết bài tập thành công");
        isReady.value = true;
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lấy chi tiết bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        isReady.value = true;
      }
    });
  }



  teacherGrading(idExercise,idStudent,teacherComment,scoreExercise) {
    exerciseRepo.teacherGradingQuestion(idExercise,idStudent,teacherComment,scoreExercise,teacherCommentAnswers).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Chấm điểm bài tập thành công");
        Get.back();
        Get.back();
        Get.find<ListStudentSubmitExerciseController>().onInit();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Chấm điểm bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);

      }
    });
  }


  setColorTextKey(status,key,answer){
    if(status == "TRUE"){
      return const Color.fromRGBO(77, 197, 145, 1);
    }else{
      if(key == answer){
        return const Color.fromRGBO(255, 69, 89, 1);
      }else{
        return const Color.fromRGBO(177, 177, 177, 1);
      }
    }
  }

  setBorderAnswer(status,key,answer){
    if(status == "TRUE"){
      return const Color.fromRGBO(77, 197, 145, 1);
    }else{
      if(key == answer){
        return const Color.fromRGBO(255, 69, 89, 1);
      }else{
        return const Color.fromRGBO(177, 177, 177, 1);
      }
    }
  }

  setBackgroundKey(status,key,answer){
    if(status == "TRUE"){
      return const Color.fromRGBO(184, 239, 215, 1);
    }else{
      if(key == answer){
        return const Color.fromRGBO(255, 181, 189, 1);
      }else{
        return const Color.fromRGBO(255, 255, 255, 1);
      }
    }
  }


  setColorTextAnswer(status,key,answer){
    if(status == "TRUE"){
      return const Color.fromRGBO(77, 197, 145, 1);
    }else{
      if(key == answer){
        return const Color.fromRGBO(255, 69, 89, 1);
      }else{
        return const Color.fromRGBO(26, 26, 26, 1);
      }
    }
  }



  setBorderKey(status,key,answer){
    if(status == "TRUE"){
      return const Color.fromRGBO(184, 239, 215, 1);
    }else{
      if(key == answer){
        return const Color.fromRGBO(255, 181, 189, 1);
      }else{
        return const Color.fromRGBO(177, 177, 177, 1);
      }
    }
  }



  visibleFalse(status,key,answer){
    if(status == "TRUE"){
      return false;
    }else{
      if(key == answer){
        return true;
      }else{
        return false;
      }
    }
  }


  visibleTrue(status,key,answer){
    if(status == "TRUE"){
      return true;
    }else{
     return false;
    }
  }




}