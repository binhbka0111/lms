import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import '../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../learning_management_teacher_controller.dart';
import '../list_student_submitted_exercise/list_student_submit_exercise_page.dart';
import '../list_student_un_submit_exercise/list_student_un_submit_exercise_page.dart';
import 'grading_exercise_question_controller.dart';

class GradingExerciseQuestionPage extends GetView<GradingExerciseQuestionController>{
  @override
  final controller = Get.put(GradingExerciseQuestionController());

  GradingExerciseQuestionPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar:  AppBar(
        actions: [
          IconButton(
            onPressed: () {
              comeToHome();
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title:  Text(
          'Bài Tập Môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}',
          style: const TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() => Container(
        margin: EdgeInsets.all(16.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("${controller.detailExercise.value.title}",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Ngày tạo",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExercise.value.createdAt!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Hạn nộp",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExercise.value.deadline!)),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Ghi chú: ",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                SizedBox(
                    width: 270.w,
                    child: Text("${controller.detailExercise.value.description}",style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                  ),
              ],
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            SizedBox(
              height: 40,
              child: ListView.builder(
                  itemCount: controller.listTitle.length,
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context,index){
                    return GetBuilder<GradingExerciseQuestionController>(builder: (controller){
                      return TextButton(
                          onPressed: () {
                            controller.indexClick.value = index;
                            controller.showColor(controller.indexClick.value);
                            controller.pageController.animateToPage(index,
                                duration: const Duration(seconds: 1),
                                curve: Curves.easeOutBack);
                            controller.update();
                          },
                          child: Obx(() => Text(
                            "${controller.listTitle[index]} (${controller.getListStudent(index) ?? 0})",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 14.sp,
                                color: controller.indexClick.value == index
                                    ? ColorUtils.PRIMARY_COLOR
                                    : const Color.fromRGBO(177, 177, 177, 1)),
                          )));
                    });
                  }),
            ),
            Padding(padding: EdgeInsets.only(top: 16.h)),
            Expanded(
                child: PageView(
                  onPageChanged: (value) {
                    controller.onPageViewChange(value);
                  },
                  controller: controller.pageController,
                  physics: const ScrollPhysics(),
                  children: [
                    ListStudentSubmitExercisePage(),
                    ListStudentUnSubmitExercisePage()
                  ],
                ))
          ],
        ),
      )),
    );
  }

}