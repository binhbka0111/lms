import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../../../../data/repository/exercise/excercise_repo.dart';
import '../../view_exercise_controller.dart';

class UpdateExerciseAssignLinkController extends GetxController{
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  var focusAttachLink = FocusNode();
  var controllerAttachLink = TextEditingController();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var detailExercise = ItemsExercise().obs;
  var focusNameExercise = FocusNode();
  var controllerNameExercise = TextEditingController();
  var focusDescribe = FocusNode();
  var controllerDescribe = TextEditingController();
  var controllerDateEnd = TextEditingController();
  var controllerTimeEnd = TextEditingController();
  var isShowErrorTextNameExercise = false;
  var isShowErrorTextDate= false;
  var isShowErrorTextTime= false;
  var isShowErrorLink = false;
  var isShowErrorPoint = false;
  var errorTextPoint = "".obs;
  var focusPoint = FocusNode();
  var controllerPoint = TextEditingController();
  @override
  void onInit() {
    var detailItem = Get.arguments;
    if (detailItem != null) {
      detailExercise.value = detailItem;
      controllerAttachLink.text = detailExercise.value.link!;
      controllerNameExercise.text = detailExercise.value.title!;
      controllerDescribe.text = detailExercise.value.description!;
      controllerPoint.text = detailExercise.value.scoreOfExercise.toString();
      controllerDateEnd.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(detailExercise.value.deadline!)).substring(0,10);
      controllerTimeEnd.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(detailExercise.value.deadline!)).substring(11,16);
    }
    super.onInit();
  }


  updateExercise(exerciseId,title, deadline, description, typeExercise, subjectId, teacherId, classId, link, scoreOfExercise) {
    exerciseRepo.updateExerciseLink(exerciseId,title,deadline, description, typeExercise, subjectId, teacherId, classId, link, scoreOfExercise).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật bài tập thành công");
        Get.back();
        Get.find<ViewExerciseController>().onInit();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Cập nhật bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


}