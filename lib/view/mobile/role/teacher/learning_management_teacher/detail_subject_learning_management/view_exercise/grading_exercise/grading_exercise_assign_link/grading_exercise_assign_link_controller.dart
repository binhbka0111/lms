import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../../../../data/model/res/exercise/list_student_exercise.dart';
import '../../../../../../../../../data/repository/exercise/excercise_repo.dart';
import '../list_student_submitted_exercise/list_student_submit_exercise_controller.dart';
import '../list_student_un_submit_exercise/list_student_un_submit_exercise_controller.dart';



class GradingExerciseAssignLinkController extends GetxController{
  var focusAttachLink = FocusNode();
  var controllerAttachLink = TextEditingController();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var detailExercise = ItemsExercise().obs;
  var listTitle = <String>[].obs;
  var listStudentExercise = ListStudentSubmitted().obs;
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  RxInt indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  @override
  void onInit() {
    var detailItem = Get.arguments;
    if (detailItem != null) {
      detailExercise.value = detailItem;
      controllerAttachLink.text = detailExercise.value.link!;
    }
    listTitle.value = ["Học sinh đã nộp","Học sinh chưa nộp"];
    super.onInit();
  }

  @override
  dispose() {
    pageController.dispose();
    super.dispose();
  }

  onPageViewChange(int page) {
    indexClick.value = page;
    if (page != 0) {
      indexClick.value - 1;
    } else {
      indexClick.value = 0;
    }
  }


  showColor(index) {
    click.value = [];
    for (int i = 0; i < 2; i++) {
      click.add(false);
    }
    click[index] = true;
  }


  getListStudent(index){
    switch(index){
      case 0:
        return  Get.find<ListStudentSubmitExerciseController>().studentSubmitted.length;
      case 1:
        return Get.find<ListStudentUnSubmitExerciseController>().studentUnSubmitted.length;
    }
  }




}