import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/res/exercise/detail_exercise.dart';
import '../../../../../../../../../data/model/res/exercise/exercise.dart';
import '../../../../../../../../../data/repository/exercise/excercise_repo.dart';

class ViewExerciseQuestionController extends GetxController{
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  var idExercise = "".obs;
  var detailExercise = DetailExercise().obs;
  var listIdQuestion = <String>[];
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var isReady = false.obs;
  var listQuestion = <Questions>[].obs;
  var detailExerciseCopy = DetailExercise().obs;

  @override
  void onInit() {
    super.onInit();
    var tmpItemExercise = Get.arguments;
    if(tmpItemExercise!=null){
      idExercise.value = tmpItemExercise;
      getDetailExercise(idExercise.value);
    }
  }



  getDetailExercise(exerciseId) {
    exerciseRepo.detailExercise(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        detailExercise.value = value.object!;
        detailExerciseCopy.value = value.object!;
        for(int i = 0 ; i<detailExerciseCopy.value.questions!.length;i++){
          listQuestion.add(detailExerciseCopy.value.questions![i]);
          listIdQuestion.add(detailExerciseCopy.value.questions![i].id!);
        }
        isReady.value = true;
        AppUtils.shared.showToast("Lấy chi tiết bài tập thành công");
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lấy chi tiết bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        isReady.value = true;
      }
    });
  }

  getIndexQuestion(id){
    return (listIdQuestion.indexOf(id)+1);
  }

}