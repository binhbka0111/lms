import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../../../../data/model/res/exercise/detail_exercise.dart';
import '../../../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../../../../data/repository/exercise/excercise_repo.dart';

class DetailExerciseUploadFileController extends GetxController{
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var itemExercise = ItemsExercise().obs;
  var filesUploadExercise = <ResponseFileUpload>[].obs;
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  var detailExercise = DetailExercise().obs;
  var isReady = false.obs;
  @override
  void onInit() {
    var detailItem = Get.arguments;
    if (detailItem != null) {
      itemExercise.value = detailItem;
      filesUploadExercise.value = itemExercise.value.files!;
      getDetailExercise(itemExercise.value.id);
    }
    super.onInit();
  }


  getDetailExercise(exerciseId) {
    exerciseRepo.detailExercise(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        detailExercise.value = value.object!;
        isReady.value = true;
        AppUtils.shared.showToast("Lấy chi tiết bài tập thành công");
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lấy chi tiết bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        isReady.value = true;
      }
    });
  }

}