import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import 'dart:io';
import '../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../../../../data/model/common/req_file.dart';
import '../../../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../../../../data/repository/exercise/excercise_repo.dart';
import '../../../../../../../../../data/repository/file/file_repo.dart';
import '../../view_exercise_controller.dart';

class UpdateExerciseUploadFileController extends GetxController{
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var detailExercise = ItemsExercise().obs;
  var filesUploadExercise = <ResponseFileUpload>[].obs;
  var focusNameExercise = FocusNode();
  var controllerNameExercise = TextEditingController();
  var focusDescribe = FocusNode();
  var controllerDescribe = TextEditingController();
  var controllerDateEnd = TextEditingController();
  var controllerTimeEnd = TextEditingController();
  final FileRepo fileRepo = FileRepo();
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  var files = <ReqFile>[].obs;
  var isShowErrorTextNameExercise = false;
  var isShowErrorTextDate= false;
  var isShowErrorTextTime= false;
  var isShowErrorPoint = false;
  var errorTextPoint = "".obs;
  var focusPoint = FocusNode();
  var controllerPoint = TextEditingController();
  @override
  void onInit() {
    var detailItem = Get.arguments;
    if (detailItem != null) {
      detailExercise.value = detailItem;
      filesUploadExercise.value = detailExercise.value.files!;
      controllerNameExercise.text = detailExercise.value.title!;
      controllerDescribe.text = detailExercise.value.description!;
      controllerPoint.text = detailExercise.value.scoreOfExercise.toString();
      controllerDateEnd.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(detailExercise.value.deadline!)).substring(0,10);
      controllerTimeEnd.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(detailExercise.value.deadline!)).substring(11,16);
    }
    super.onInit();
  }






  uploadFile(file) async{
    showDialogUploadFile();
    var fileList = <File>[];
    for (var element in file) {
      fileList.add(element.file!);
    }

    await fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;
        files.addAll(file);
        filesUploadExercise.addAll(listResFile);
        filesUploadExercise.refresh();
        listResFile.clear();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
    Get.back();
  }



  updateExercise(exerciseId,title, deadline, description, typeExercise, subjectId, teacherId, classId, files,scoreOfExercise) {
    exerciseRepo.updateExerciseFile(exerciseId,title,deadline, description, typeExercise, subjectId, teacherId, classId, files,scoreOfExercise).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật bài tập thành công");
        Get.back();
        Get.find<ViewExerciseController>().onInit();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Cập nhật bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


}