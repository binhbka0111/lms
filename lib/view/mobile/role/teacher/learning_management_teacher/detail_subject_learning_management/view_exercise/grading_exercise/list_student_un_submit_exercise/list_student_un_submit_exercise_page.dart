import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'list_student_un_submit_exercise_controller.dart';

class ListStudentUnSubmitExercisePage extends GetView<ListStudentUnSubmitExerciseController>{
  @override
  final controller = Get.put(ListStudentUnSubmitExerciseController());

  ListStudentUnSubmitExercisePage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Obx(() => SingleChildScrollView(
      physics: const ClampingScrollPhysics(),
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 16.w),
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: controller.studentUnSubmitted.length,
          itemBuilder: (context, index) {
            return Column(
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 48.w,
                      height: 48.w,
                      child:

                      CacheNetWorkCustom(urlImage:   "${controller.studentUnSubmitted[index].image}",),

                    ),
                    Padding(padding: EdgeInsets.only(right: 8.w)),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("${controller.studentUnSubmitted[index].fullName}",style: TextStyle(color: Colors.black,fontSize: 12.sp,fontWeight: FontWeight.w400),),
                        Visibility(
                            visible: controller
                                .studentUnSubmitted[index].birthday != null,
                            child:  Text(
                              controller.outputDateFormatV2.format(
                                  DateTime.fromMillisecondsSinceEpoch(
                                      controller
                                          .studentUnSubmitted[index].birthday ??
                                          0)),
                              style: TextStyle(
                                  color: const Color.fromRGBO(
                                      133, 133, 133, 1),
                                  fontSize: 12.sp),
                            ))
                      ],
                    ),

                  ],
                ),
                const Divider(),
              ],
            );
          },),
      ),
    ));
  }

}