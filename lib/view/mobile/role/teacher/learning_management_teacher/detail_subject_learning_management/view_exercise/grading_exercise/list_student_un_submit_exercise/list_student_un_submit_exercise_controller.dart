import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/res/exercise/list_student_exercise.dart';
import '../../../../../../../../../data/repository/exercise/excercise_repo.dart';
import '../grading_exercise_assign_link/grading_exercise_assign_link_controller.dart';
import '../grading_exercise_question/grading_exercise_question_controller.dart';
import '../grading_exercise_upload_file/grading_exercise_upload_file_controller.dart';

class ListStudentUnSubmitExerciseController extends GetxController{
  var listStudentExercise = ListStudentSubmitted().obs;
  var studentUnSubmitted = <StudentUnSubmitted>[].obs;
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy');
  @override
  void onInit() {
    if(Get.isRegistered<GradingExerciseAssignLinkController>()){
      getListStudentUnSubmit( Get.find<GradingExerciseAssignLinkController>().detailExercise.value.id);
    }
    if(Get.isRegistered<GradingExerciseUploadFileController>()){
      getListStudentUnSubmit( Get.find<GradingExerciseUploadFileController>().detailExercise.value.id);
    }
    if(Get.isRegistered<GradingExerciseQuestionController>()){
      getListStudentUnSubmit( Get.find<GradingExerciseQuestionController>().detailExercise.value.id);
    }
    super.onInit();
  }

  getListStudentUnSubmit(exerciseId){
    exerciseRepo.studentsSubmitted(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudentExercise.value = value.object!;
        studentUnSubmitted.value = listStudentExercise.value.studentUnSubmitted!;
      }
    });
  }

}