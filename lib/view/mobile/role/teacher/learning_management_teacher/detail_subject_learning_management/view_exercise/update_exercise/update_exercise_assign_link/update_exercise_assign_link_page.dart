import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/view_exercise/update_exercise/update_exercise_assign_link/update_exercise_assign_link_controller.dart';
import '../../../../../../../../../commom/app_cache.dart';
import '../../../../../../../../../commom/constants/date_format.dart';
import '../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../../../../commom/utils/date_time_picker.dart';
import '../../../../../../../../../commom/utils/time_utils.dart';
import '../../../../../../../../../commom/widget/text_field_custom.dart';
import '../../../../../teacher_home_controller.dart';
import '../../../learning_management_teacher_controller.dart';
import 'package:validators/validators.dart';
class UpdateExerciseAssignLinkPage extends GetView<UpdateExerciseAssignLinkController>{
  @override
  final controller = Get.put(UpdateExerciseAssignLinkController());

  UpdateExerciseAssignLinkPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(child: Scaffold(
      appBar:  AppBar(
        actions: [
          IconButton(
            onPressed: () {
              comeToHome();
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title:  Text(
          'Cập nhật bài tập môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: GetBuilder<UpdateExerciseAssignLinkController>(builder: (controller) {
        return Column(
          children: [
            Expanded(child: Container(
              margin: EdgeInsets.all(16.h),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MyOutlineBorderTextFormFieldNhat(
                    enable: true,
                    focusNode: controller.focusNameExercise,
                    iconPrefix: null,
                    iconSuffix: null,
                    state: StateType.DEFAULT,
                    labelText: "Tiêu đề (*)",
                    autofocus: false,
                    controller: controller.controllerNameExercise,
                    helperText: "vui lòng nhập tiêu đề",
                    showHelperText: controller.isShowErrorTextNameExercise,
                    textInputAction: TextInputAction.next,
                    ishowIconPrefix: false,
                    keyboardType: TextInputType.text,
                  ),
                  Padding(padding: EdgeInsets.only(top: 8.h)),
                  MyOutlineBorderTextFormFieldNhat(
                    enable: true,
                    focusNode: controller.focusDescribe,
                    iconPrefix: null,
                    iconSuffix: null,
                    state: StateType.DEFAULT,
                    labelText: "Mô Tả",
                    autofocus: false,
                    controller: controller.controllerDescribe,
                    helperText: "",
                    showHelperText: false,
                    textInputAction: TextInputAction.next,
                    ishowIconPrefix: false,
                    keyboardType: TextInputType.text,
                  ),
                  Padding(padding: EdgeInsets.only(top: 8.h)),
                  Row(
                    children: [
                      Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.only(right: 4.w),
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(left: 8.w),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(6.0)),
                                border: Border.all(
                                  width: 1,
                                  style: BorderStyle.solid,
                                  color: const Color.fromRGBO(
                                      192, 192, 192, 1),
                                )),
                            child: TextFormField(
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              style: TextStyle(
                                fontSize: 12.0.sp,
                                color:
                                const Color.fromRGBO(26, 26, 26, 1),
                              ),
                              onTap: () {
                                selectDateTimeEnd(
                                    controller
                                        .controllerDateEnd.value.text,
                                    DateTimeFormat.formatDateShort,
                                    context);
                              },
                              readOnly: true,
                              cursorColor: ColorUtils.PRIMARY_COLOR,
                              controller: controller.controllerDateEnd,
                              decoration: InputDecoration(
                                suffixIcon: Container(
                                  margin: EdgeInsets.only(bottom: 4.h),
                                  child: SizedBox(
                                    height: 14,
                                    width: 14,
                                    child: Container(
                                      margin: EdgeInsets.zero,
                                      child: SvgPicture.asset(
                                        "assets/images/icon_date_picker.svg",
                                        fit: BoxFit.scaleDown,
                                      ),
                                    ),
                                  ),
                                ),
                                label: const Text("Ngày nộp"),
                                border: InputBorder.none,
                                labelStyle: TextStyle(
                                    color: const Color.fromRGBO(
                                        177, 177, 177, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily:
                                    'assets/font/static/Inter-Medium.ttf'),
                              ),
                            ),
                          )),
                      Expanded(
                          flex: 1,
                          child: Container(
                            margin: EdgeInsets.only(left: 4.w),
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(left: 8.w),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(6.0)),
                                border: Border.all(
                                  width: 1,
                                  style: BorderStyle.solid,
                                  color: const Color.fromRGBO(
                                      192, 192, 192, 1),
                                )),
                            child: TextFormField(
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              style: TextStyle(
                                fontSize: 12.0.sp,
                                color:
                                const Color.fromRGBO(26, 26, 26, 1),
                              ),
                              onTap: () {
                                selectTime(
                                    controller
                                        .controllerTimeEnd.value.text,
                                    DateTimeFormat.formatTime);
                              },
                              readOnly: true,
                              cursorColor: ColorUtils.PRIMARY_COLOR,
                              controller: controller.controllerTimeEnd,
                              decoration: InputDecoration(
                                suffixIcon: Container(
                                  margin: EdgeInsets.only(bottom: 4.h),
                                  child: SizedBox(
                                    height: 14,
                                    width: 14,
                                    child: Container(
                                      margin: EdgeInsets.zero,
                                      child: SvgPicture.asset(
                                        "assets/images/icon_date_picker.svg",
                                        fit: BoxFit.scaleDown,
                                      ),
                                    ),
                                  ),
                                ),
                                label: const Text("Giờ nộp"),
                                border: InputBorder.none,
                                labelStyle: TextStyle(
                                    color: const Color.fromRGBO(
                                        177, 177, 177, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500,
                                    fontFamily:
                                    'assets/font/static/Inter-Medium.ttf'),
                              ),
                            ),
                          )),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                          flex: 1,
                          child: Visibility(
                            visible: controller.isShowErrorTextDate,
                            child: Container(
                                padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                child: const Text(
                                  "Vui lòng nhập ngày nộp",
                                  style: TextStyle(color: Colors.red),
                                )),
                          )),
                      Expanded(
                          flex: 1,
                          child: Visibility(
                            visible: controller.isShowErrorTextTime,
                            child: Container(
                                padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                child: const Text(
                                  "Vui lòng nhập ngày nộp",
                                  style: TextStyle(color: Colors.red),
                                )),
                          )),
                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 8.h)),
                  MyOutlineBorderTextFormFieldNhat(
                    enable: true,
                    focusNode: controller.focusAttachLink,
                    iconPrefix: null,
                    iconSuffix: null,
                    state: StateType.DEFAULT,
                    labelText: "Đường dẫn (*)",
                    autofocus: false,
                    controller: controller.controllerAttachLink,
                    helperText: "Vui lòng nhập đúng định dạng link",
                    showHelperText: controller.isShowErrorLink,
                    textInputAction: TextInputAction.next,
                    ishowIconPrefix: false,
                    keyboardType: TextInputType.text,
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  FocusScope(
                    node: FocusScopeNode(),
                    child: Focus(
                        onFocusChange: (focus) {
                          controller.update();
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              padding: const EdgeInsets.all(2.0),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                  const BorderRadius.all(Radius.circular(6.0)),
                                  border: Border.all(
                                    width: 1,
                                    style: BorderStyle.solid,
                                    color: controller.focusPoint.hasFocus
                                        ? ColorUtils.PRIMARY_COLOR
                                        : const Color.fromRGBO(192, 192, 192, 1),
                                  )),
                              child: TextFormField(
                                cursorColor: ColorUtils.PRIMARY_COLOR,
                                focusNode: controller.focusPoint,
                                inputFormatters: <TextInputFormatter> [
                                  FilteringTextInputFormatter.allow(RegExp('[0-9.,]')),
                                ],
                                onChanged: (value) {
                                  if(controller.controllerPoint.text.trim() != ""){
                                    controller.isShowErrorPoint = false;
                                  }
                                  controller.update();
                                },
                                onFieldSubmitted: (value) {
                                  controller.update();
                                },
                                controller: controller.controllerPoint,
                                keyboardType: TextInputType.number,

                                decoration: InputDecoration(
                                  labelText: "Nhập điểm bài tập",
                                  labelStyle: TextStyle(
                                      fontSize: 14.0.sp,
                                      color: const Color.fromRGBO(177, 177, 177, 1),
                                      fontWeight: FontWeight.w500,
                                      fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                                  contentPadding: const EdgeInsets.symmetric(
                                      vertical: 8, horizontal: 16),
                                  prefixIcon:  null,
                                  suffixIcon:  Visibility(
                                      visible:
                                      controller.controllerPoint.text.isNotEmpty ==
                                          true,
                                      child: InkWell(
                                        onTap: () {
                                          controller.controllerPoint.text = "";
                                        },
                                        child: const Icon(
                                          Icons.close_outlined,
                                          color: Colors.black,
                                          size: 20,
                                        ),
                                      )),
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  border: InputBorder.none,
                                  errorStyle: const TextStyle(height: 0),
                                  focusedErrorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  floatingLabelBehavior: FloatingLabelBehavior.auto,

                                ),
                              ),
                            ),
                            Visibility(
                              visible: controller.isShowErrorPoint,
                              child: Container(
                                  padding: const EdgeInsets.only(left: 16, top: 2.0, bottom: 8),
                                  child: Text(
                                    controller.errorTextPoint.value,
                                    style: const TextStyle(color: Colors.red),
                                  )),
                            )
                          ],
                        )),
                  ),
                ],
              ),
            )),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.all(16),
              color: Colors.white,
              child: SizedBox(
                height: 46.h,
                child: ElevatedButton(
                  onPressed: () {
                    if (controller.controllerNameExercise.text.trim() == "") {
                      controller.isShowErrorTextNameExercise = true;
                      controller.update();
                    } else {
                      controller.isShowErrorTextNameExercise = false;
                      controller.update();
                      if (controller.controllerDateEnd.value.text.trim() == "") {
                        controller.isShowErrorTextDate = true;
                        controller.update();
                      } else {
                        controller.isShowErrorTextDate = false;
                        controller.update();
                        if (controller.controllerTimeEnd.text.trim() == "") {
                          controller.isShowErrorTextTime = true;
                          controller.update();
                        }else{
                          controller.isShowErrorTextTime = false;
                          controller.update();
                          if(isURL(controller.controllerAttachLink.value.text)){
                            controller.isShowErrorLink = false;
                            controller.update();
                            if (controller.controllerPoint.text.trim() == "") {
                              controller.isShowErrorPoint = true;
                              controller.errorTextPoint.value = "Vui lòng nhập điểm cho câu hỏi";
                              controller.update();
                            }else{
                              controller.isShowErrorPoint= false;
                              controller.update();
                              if(controller.controllerPoint.text.trim().substring(controller.controllerPoint.text.trim().length - 1) == "."){
                                controller.isShowErrorPoint = true;
                                controller.errorTextPoint.value = "Vui lòng nhập đúng định dạng điểm";
                                controller.update();
                              }
                              else if (double.tryParse(controller.controllerPoint.text.trim()) == null) {
                                controller.isShowErrorPoint = true;
                                controller.errorTextPoint.value = "Vui lòng nhập đúng định dạng điểm";
                                controller.update();
                              }else{
                                controller.updateExercise(
                                    controller.detailExercise.value.id,
                                    controller.controllerNameExercise.text.trim()
                                    , DateFormat("dd/MM/yyyy HH:mm").parse("${controller.controllerDateEnd.text.trim()} ${controller.controllerTimeEnd.text.trim()}").millisecondsSinceEpoch
                                    , controller.controllerDescribe.text.trim()
                                    , controller.detailExercise.value.typeExercise
                                    , Get.find<LearningManagementTeacherController>().detailSubject.value.id
                                    , AppCache().userId
                                    , Get.find<TeacherHomeController>().currentClass.value.classId
                                    , controller.controllerAttachLink.text
                                    , double.tryParse(controller.controllerPoint.text.trim()));
                              }
                            }
                          }else{
                            controller.isShowErrorLink = true;
                            controller.update();
                          }
                        }
                      }
                    }
                  },
                  style: ElevatedButton.styleFrom(
                      backgroundColor: ColorUtils.PRIMARY_COLOR,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6))),
                  child: Text("Xác nhận",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.sp,
                          fontWeight: FontWeight.w400)),
                ),
              ),
            )
          ],
        );
      },),
    ));
  }


  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10),
        isDisablePreviousDay: true)
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (int.parse(date.substring(6, 10)) <= DateTime.now().year &&
        int.parse(date.substring(3, 5)) <= DateTime.now().month &&
        int.parse(date.substring(0, 2)) < DateTime.now().day) {
      AppUtils.shared.showToast("Vui lòng không chọn ngày trong quá khứ");
      controller.controllerDateEnd.text = "";
    }else{
      controller.controllerDateEnd.text = date;
    }
    
  }


  selectTime(stringTime,format) async {
    var now = DateTime.now();
    if (!stringTime.isEmpty) {
      now = TimeUtils.convertStringToDate(stringTime, format);
    }
    var time = "";
    await DateTimePicker.showTimePicker(Get.context!, now).then((times) {
      time =
          TimeUtils.convertDateTimeToFormat(times, DateTimeFormat.formatTime);
    });

    if (time.isNotEmpty) {
      controller.controllerTimeEnd.text = time;
    }
  }

}