import 'dart:ui';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/res/exercise/list_student_exercise.dart';
import '../../../../../../../../../data/repository/exercise/excercise_repo.dart';
import '../grading_exercise_assign_link/grading_exercise_assign_link_controller.dart';
import '../grading_exercise_question/grading_exercise_question_controller.dart';
import '../grading_exercise_upload_file/grading_exercise_upload_file_controller.dart';
import 'package:slova_lms/routes/app_pages.dart';

class ListStudentSubmitExerciseController extends GetxController{
  var listStudentExercise = ListStudentSubmitted().obs;

  var studentUnSubmitted = <StudentUnSubmitted>[].obs;
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy');
  var listIdStudent = <String>[].obs;
  var studentSubmitted = <StudentSubmitted>[].obs;
  @override
  void onInit() {
    if(Get.isRegistered<GradingExerciseAssignLinkController>()){
      getListStudentSubmit( Get.find<GradingExerciseAssignLinkController>().detailExercise.value.id);
    }
    if(Get.isRegistered<GradingExerciseUploadFileController>()){
      getListStudentSubmit( Get.find<GradingExerciseUploadFileController>().detailExercise.value.id);
    }
    if(Get.isRegistered<GradingExerciseQuestionController>()){
      getListStudentSubmit( Get.find<GradingExerciseQuestionController>().detailExercise.value.id);
    }


    super.onInit();
  }


  getListStudentSubmit(exerciseId){
    exerciseRepo.studentsSubmitted(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudentExercise.value = value.object!;
        studentSubmitted.value = listStudentExercise.value.studentSubmitted!;
        studentUnSubmitted.value = listStudentExercise.value.studentUnSubmitted!;
        for (int i = 0; i < studentSubmitted.length; i++) {
          listIdStudent.add(studentSubmitted[i].student!.id!);
        }
      }
    });
  }


  clickStudentDoExercise(idStudent){
    if(Get.isRegistered<GradingExerciseAssignLinkController>()){
      Get.toNamed(Routes.detailStudentDoExerciseAssignLinkPage,arguments:idStudent);
    }
    if(Get.isRegistered<GradingExerciseUploadFileController>()){
      Get.toNamed(Routes.detailStudentDoExerciseUploadFilePage,arguments: idStudent);
    }
    if(Get.isRegistered<GradingExerciseQuestionController>()){
      Get.toNamed(Routes.detailStudentDoExerciseQuestionPage,arguments: idStudent);
    }
  }

  getColorTextStatus(status) {
    switch (status) {
      case "LATE_SUBMISSION":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "SUBMITTED":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "NOT_SUBMISSION":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "UNSUBMITTED":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "GRADED":
        return const Color.fromRGBO(77, 197, 145, 1);
    }
  }

  getColorTextIsGrade(status) {
    switch (status) {
      case "FALSE":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "TRUE":
        return const Color.fromRGBO(77, 197, 145, 1);
    }
  }
  getColorTextIsPublicScore(status) {
    switch (status) {
      case "FALSE":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "TRUE":
        return const Color.fromRGBO(77, 197, 145, 1);
    }
  }

  getColorBackgroundIsGrade(status) {
    switch (status) {
      case "FALSE":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "TRUE":
        return const Color.fromRGBO(192, 242, 220, 1);
    }
  }

  getColorBackgroundIsPublicScore(status) {
    switch (status) {
      case "FALSE":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "TRUE":
        return const Color.fromRGBO(192, 242, 220, 1);
    }
  }



  getColorBackgroundStatus(status) {
    switch (status) {
      case "LATE_SUBMISSION":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "SUBMITTED":
        return const Color.fromRGBO(192, 242, 220, 1);
      case "NOT_SUBMISSION":
        return const Color.fromRGBO(252, 211, 215, 1);
      case "UNSUBMITTED":
        return const Color.fromRGBO(252, 211, 215, 1);
      case "GRADED":
        return const Color.fromRGBO(192, 242, 220, 1);
    }
  }

  getTextIsGrade(status) {
    switch (status) {
      case "FALSE":
        return "Chưa chấm";
      case "TRUE":
        return "Đã chấm";
    }
  }

  getTextIsPublicScore(status) {
    switch (status) {
      case "FALSE":
        return "Chưa công bố";
      case "TRUE":
        return "Đã công bố";
    }
  }



  getStatus(status) {
    switch (status) {
      case "SUBMITTED":
        return "Đã nộp bài";
      case "LATE_SUBMISSION":
        return "Nộp bài muộn";
      case "NOT_SUBMISSION":
        return "Không nộp";
      case "UNSUBMITTED":
        return "Chưa nộp bài";
      case "GRADED ":
        return "Đã chấm điểm";
    }
  }

}