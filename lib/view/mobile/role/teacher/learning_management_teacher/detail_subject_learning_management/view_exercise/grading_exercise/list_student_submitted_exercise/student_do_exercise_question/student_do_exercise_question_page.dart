import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:html/parser.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/commom/widget/list_view_image.dart';
import '../../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../../commom/widget/text_field_custom.dart';
import '../../../../learning_management_teacher_controller.dart';
import '../../grading_exercise_question/grading_exercise_question_controller.dart';
import 'student_do_exercise_question_controller.dart';

class StudentDoExerciseQuestionPage
    extends GetView<StudentDoExerciseQuestionController> {
  @override
  final controller = Get.put(StudentDoExerciseQuestionController());

  StudentDoExerciseQuestionPage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {
                  comeToHome();
                },
                icon: const Icon(Icons.home),
                color: Colors.white,
              )
            ],
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            title: Text(
              'Bài Tập Môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}',
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body: Obx(() => controller.isReady.value
              ? Column(
            children: [
              Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.all(16.h),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${controller.studentDoExercise.value.title}",
                            style: TextStyle(
                                color: const Color.fromRGBO(133, 133, 133, 1),
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w500),
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Ngày tạo",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                controller.outputDateFormat.format(
                                    DateTime.fromMillisecondsSinceEpoch(controller
                                        .studentDoExercise.value.createdAt ??
                                        0)),
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Hạn nộp",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                controller.outputDateFormat.format(
                                    DateTime.fromMillisecondsSinceEpoch(controller
                                        .studentDoExercise.value.deadline ??
                                        0)),
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Ghi chú",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Padding(padding: EdgeInsets.only(right: 16.w)),
                              SizedBox(
                                width: 260.w,
                                child: Text(
                                  "${controller.studentDoExercise.value.description}",
                                  style: TextStyle(
                                      color: const Color.fromRGBO(26, 26, 26, 1),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            children: [
                              SizedBox(
                                width: 48.w,
                                height: 48.w,
                                child:
                                CacheNetWorkCustom(urlImage:   "${ controller.studentDoExercise.value.student?.image }",),

                              ),
                              Padding(padding: EdgeInsets.only(right: 8.w)),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("${controller.studentDoExercise.value.student?.fullName}",style: TextStyle(color: Colors.black,fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                  Visibility(
                                      visible:controller.studentDoExercise.value.student!
                                          .birthday != null,
                                      child:  Text(
                                        controller.outputDateFormatV2.format(
                                            DateTime.fromMillisecondsSinceEpoch(
                                                controller.studentDoExercise.value.student
                                                    ?.birthday ??
                                                    0)),
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                133, 133, 133, 1),
                                            fontSize: 12.sp),
                                      ))
                                ],
                              )

                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 16.h)),
                          ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: controller.studentDoExercise.value.questionAnswers?.length,
                            itemBuilder: (context, index) {
                              var listImage =  controller.studentDoExercise.value.questionAnswers![index].question!.files!.where((element) => element.ext == "png" ||
                                  element.ext == "jpg" ||
                                  element.ext == "jpeg" ||
                                  element.ext == "gif" ||
                                  element.ext == "bmp").toList().obs;
                              var listNotImage =  controller.studentDoExercise.value.questionAnswers![index].question!.files!.where((element) =>
                              element.ext != "png" &&
                                  element.ext != "jpg" &&
                                  element.ext != "jpeg" &&
                                  element.ext != "gif" &&
                                  element.ext != "bmp")
                                  .toList().obs;


                              var listImageStudentDoExercise =  controller.studentDoExercise.value.questionAnswers![index].files!.where((element) => element.ext == "png" ||
                                  element.ext == "jpg" ||
                                  element.ext == "jpeg" ||
                                  element.ext == "gif" ||
                                  element.ext == "bmp").toList().obs;
                              var listNotImageStudentDoExercise =  controller.studentDoExercise.value.questionAnswers![index].files!.where((element) =>
                              element.ext != "png" &&
                                  element.ext != "jpg" &&
                                  element.ext != "jpeg" &&
                                  element.ext != "gif" &&
                                  element.ext != "bmp")
                                  .toList().obs;
                              return Container(
                                margin: EdgeInsets.only(bottom: 8.h),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      width: 300.w,
                                      child: RichText(
                                          text: TextSpan(children: [
                                            TextSpan(
                                                text: 'Câu ${index + 1}: ',
                                                style: TextStyle(
                                                    color: const Color.fromRGBO(
                                                        133, 133, 133, 1),
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 12.sp,
                                                    fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf')),
                                            TextSpan(
                                                text:
                                                '[${controller.studentDoExercise.value.questionAnswers?[index].question!.point}đ] ',
                                                style: TextStyle(
                                                    color: ColorUtils.PRIMARY_COLOR,
                                                    fontSize: 12.sp,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf')),
                                            TextSpan(
                                                text:
                                                '${controller.studentDoExercise.value.questionAnswers![index].question?.content}: ',
                                                style: TextStyle(
                                                    color: const Color.fromRGBO(
                                                        26, 26, 26, 1),
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 12.sp,
                                                    fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf')),
                                          ])),
                                    ),
                                    Padding(padding: EdgeInsets.only(top: 16.h)),
                                    ListViewShowImage.showGridviewImage(listImage),
                                    ListViewShowImage.showListViewNotImage(listNotImage),
                                    Visibility(
                                        visible: controller.studentDoExercise.value.questionAnswers?[index].question?.typeQuestion == "SELECTED_RESPONSE",
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          physics: const NeverScrollableScrollPhysics(),
                                          itemCount: controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?.length,
                                          itemBuilder: (context, indexAnswer) {
                                            return Container(
                                              margin: EdgeInsets.only(bottom: 8.h),
                                              child: Row(
                                                children: [
                                                  Container(
                                                    height: 32.h,
                                                    width: 32.h,
                                                    decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: controller.setBackgroundKey(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                            , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                            , controller.studentDoExercise.value.questionAnswers?[index].answer!),
                                                        border: Border.all(color: controller.setBorderKey(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                            , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                            , controller.studentDoExercise.value.questionAnswers?[index].answer!))
                                                    ),
                                                    padding: EdgeInsets.all(8.h),
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                      "${controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key}",
                                                      style: TextStyle(
                                                          color: controller.setColorTextKey(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                              , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                              , controller.studentDoExercise.value.questionAnswers?[index].answer!),
                                                          fontSize: 14.sp,
                                                          fontWeight: FontWeight.w500),
                                                    ),
                                                  ),
                                                  Padding(
                                                      padding:
                                                      EdgeInsets.only(right: 8.w)),
                                                  Container(
                                                    padding: EdgeInsets.symmetric(
                                                        horizontal: 14.w,
                                                        vertical: 8.h),
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                        BorderRadius.circular(6.r),
                                                        border: Border.all(
                                                            color: controller.setBorderAnswer(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                                , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                                , controller.studentDoExercise.value.questionAnswers?[index].answer!)),
                                                        color: Colors.white),
                                                    child: SizedBox(
                                                      width: 220.w,
                                                      child: Text(
                                                        "${controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].value}",style: TextStyle(
                                                          color: controller.setColorTextAnswer(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                              , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                              , controller.studentDoExercise.value.questionAnswers?[index].answer!)
                                                      ),),
                                                    ),
                                                  ),
                                                  Padding(
                                                      padding:
                                                      EdgeInsets.only(right: 8.w)),
                                                  Visibility(
                                                      visible:  controller.visibleTrue(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                          , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                          , controller.studentDoExercise.value.questionAnswers?[index].answer!),
                                                      child: const Icon(
                                                        Icons.check,
                                                        color: Color.fromRGBO(
                                                            77, 197, 145, 1),
                                                      )),
                                                  Visibility(
                                                      visible: controller.visibleFalse(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                          , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                          , controller.studentDoExercise.value.questionAnswers?[index].answer!),
                                                      child: const Icon(
                                                        Icons.clear,
                                                        color: Color.fromRGBO(255, 69, 89, 1),
                                                      ))
                                                ],
                                              ),
                                            );
                                          },
                                        )),
                                    Visibility(
                                      visible: controller.studentDoExercise.value.questionAnswers?[index].question?.typeQuestion == "CONSTRUCTED_RESPONSE",
                                      child: const Text('Bài làm',
                                          style: TextStyle(
                                              color: Color.fromRGBO(26, 26, 26, 1),
                                              fontFamily:
                                              'assets/font/static/Inter-Medium.ttf',
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500)),),
                                    Visibility(
                                        visible: controller.studentDoExercise.value.questionAnswers?[index].question?.typeQuestion == "CONSTRUCTED_RESPONSE",
                                        child: Text(
                                          "${parse(parse(controller.studentDoExercise.value.questionAnswers?[index].answer ?? "").body?.text).documentElement?.text}",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14.sp),
                                        )),
                                    Padding(padding: EdgeInsets.only(top: 8.h)),
                                    Obx(() => ListViewShowImage.showGridviewImage(listImageStudentDoExercise)),
                                    Obx(() => ListViewShowImage.showListViewNotImage(listNotImageStudentDoExercise)),
                                    SizedBox(height: 8.h,),
                                    GetBuilder<StudentDoExerciseQuestionController>(
                                      builder: (controller) {
                                        return Visibility(
                                            visible: controller.studentDoExercise.value.questionAnswers?[index].question?.typeQuestion == "CONSTRUCTED_RESPONSE",
                                            child: MyTextFormFieldBinh(
                                              enable: true,
                                              focusNode: controller.focusScoreQuestion[index],
                                              iconPrefix: null,
                                              iconSuffix: null,
                                              state: StateType.DEFAULT,
                                              labelText: "Nhập Điểm Cho Câu Hỏi",
                                              autofocus: false,
                                              controller:
                                              controller.controllerScoreQuestion[index],
                                              helperText:
                                              controller.helperTextScoreQuestion[index],
                                              showHelperText: controller.isShowhelperText[index],
                                              textInputAction: TextInputAction.next,
                                              ishowIconPrefix: false,
                                              keyboardType: TextInputType.number,
                                            ));
                                      },
                                    ),
                                    Padding(padding: EdgeInsets.only(top: 8.h)),
                                    Container(
                                      margin: EdgeInsets.symmetric(horizontal: 16.w),
                                      child: MyOutlineBorderTextFormFieldBinh(
                                        enable: true,
                                        focusNode: controller.focusComment[index],
                                        iconPrefix: null,
                                        iconSuffix: null,
                                        state: StateType.DEFAULT,
                                        labelText: "Nhận xét",
                                        autofocus: false,
                                        controller: controller.controllerComment[index],
                                        helperText: "",
                                        showHelperText: false,
                                        textInputAction: TextInputAction.next,
                                        ishowIconPrefix: false,
                                        keyboardType: TextInputType.text,
                                      ),
                                    ),

                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  )),
              Container(
                color: Colors.white,
                width: double.infinity,
                padding:
                EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                child: SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                      onPressed: () {
                          for(int i = 0;i<controller.studentDoExercise.value.questionAnswers!.length;i++){
                            if(controller.studentDoExercise.value.questionAnswers![i].question?.typeQuestion == "SELECTED_RESPONSE"){
                              controller.isShowhelperText[i] = false;
                            }else{
                              if (controller.controllerScoreQuestion[i].text.trim() ==""){
                                controller.isShowhelperText[i] = true;
                                controller.helperTextScoreQuestion[i] = "Vui lòng nhập điểm của câu hỏi";
                                controller.update();
                              }else{
                                if (controller.controllerScoreQuestion[i].text.trim().substring(controller.controllerScoreQuestion[i].text.trim().length - 1) == ".") {
                                  controller.isShowhelperText[i] = true;
                                  controller.helperTextScoreQuestion[i]  = "Vui lòng nhập đúng định dạng điểm";
                                  controller.update();
                                }else if(double.tryParse(controller.controllerScoreQuestion[i].text.trim())== null){
                                  controller.isShowhelperText[i] = true;
                                  controller.helperTextScoreQuestion[i]  = "Vui lòng nhập đúng định dạng điểm";
                                  controller.update();
                                }else{
                                  controller.isShowhelperText[i] = false;
                                  controller.update();
                                }
                              }
                            }

                            }
                          var score = 0.0;
                          for(int i = 0;i<controller.studentDoExercise.value.questionAnswers!.length;i++){
                            if(controller.studentDoExercise.value.questionAnswers?[i].question?.typeQuestion == "CONSTRUCTED_RESPONSE"){
                              score += double.parse(controller.controllerScoreQuestion[i].text.trim());
                            }
                            if(controller.studentDoExercise.value.questionAnswers?[i].question?.typeQuestion == "SELECTED_RESPONSE"){
                              for(int j = 0;j<controller.studentDoExercise.value.questionAnswers![i].question!.answerOption!.length;j++){
                                if(controller.studentDoExercise.value.questionAnswers![i].question!.answerOption![j].key == controller.studentDoExercise.value.questionAnswers![i].answer){
                                  if(controller.studentDoExercise.value.questionAnswers![i].question!.answerOption![j].status == "TRUE"){
                                    score += controller.studentDoExercise.value.questionAnswers![i].question!.point!;
                                  }
                                  if(controller.studentDoExercise.value.questionAnswers![i].question!.answerOption![j].status == "FALSE"){
                                    score += 0.0;
                                  }
                                }
                              }
                            }
                          }
                          controller.controllerScoreQuestionAll.text = score.toString();
                          if(controller.isShowhelperText.contains(true) == true){

                          }else{
                            Get.dialog(StatefulBuilder(
                              builder: (context, setState) {
                                return Center(
                                  child: Wrap(children: [
                                    Container(
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          borderRadius:
                                          BorderRadius.circular(16)),
                                      child: Stack(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                                top: 40.h,
                                                right: 50.w,
                                                left: 50.w),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                BorderRadius.circular(
                                                    16)),
                                            child: Column(
                                              children: [
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        top: 32.h)),
                                                MyTextFormFieldBinh(
                                                  enable: true,
                                                  focusNode: controller
                                                      .focusScoreQuestionAll,
                                                  iconPrefix: null,
                                                  iconSuffix: null,
                                                  state: StateType.DEFAULT,
                                                  labelText:
                                                  "Nhập Điểm Cho Câu Hỏi",
                                                  autofocus: false,
                                                  controller: controller
                                                      .controllerScoreQuestionAll,
                                                  helperText: controller
                                                      .helperTextScoreQuestionAll,
                                                  showHelperText: controller
                                                      .isShowhelperTextAll,
                                                  textInputAction:
                                                  TextInputAction.next,
                                                  ishowIconPrefix: false,
                                                  keyboardType:
                                                  TextInputType.number,
                                                ),
                                                Padding(
                                                    padding: EdgeInsets.only(
                                                        top: 8.h)),
                                                Container(
                                                  margin: EdgeInsets.symmetric(horizontal: 16.w),
                                                  child: MyOutlineBorderTextFormFieldBinh(
                                                    enable: true,
                                                    focusNode: controller
                                                        .focusCommentAll,
                                                    iconPrefix: null,
                                                    iconSuffix: null,
                                                    state: StateType.DEFAULT,
                                                    labelText: "Nhận xét",
                                                    autofocus: false,
                                                    controller: controller
                                                        .controllerCommentAll,
                                                    helperText:
                                                    "vui lòng nhập câu hỏi",
                                                    showHelperText: false,
                                                    textInputAction:
                                                    TextInputAction.next,
                                                    ishowIconPrefix: false,
                                                    keyboardType:
                                                    TextInputType.text,
                                                  ),
                                                ),
                                                Container(
                                                    color: Colors.white,
                                                    padding:
                                                    EdgeInsets.all(16.h),
                                                    child: SizedBox(
                                                      width: double.infinity,
                                                      height: 46,
                                                      child: ElevatedButton(
                                                          style: ElevatedButton
                                                              .styleFrom(
                                                              backgroundColor: ColorUtils.PRIMARY_COLOR),
                                                          onPressed: () {
                                                            updated(setState);
                                                          },
                                                          child: Text(
                                                            'Hoàn tất',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize:
                                                                16.sp),
                                                          )),
                                                    )),
                                                TextButton(
                                                    onPressed: () {
                                                      Get.back();
                                                    },
                                                    child: Text(
                                                      "Hủy",
                                                      style: TextStyle(
                                                          color: Colors.red,
                                                          fontSize: 16.sp),
                                                    ))
                                              ],
                                            ),
                                          ),
                                          Container(
                                              alignment: Alignment.center,
                                              margin: const EdgeInsets.only(
                                                  top: 10),
                                              height: 80,
                                              child: Image.asset(
                                                  "assets/images/image_app_logo.png"))
                                        ],
                                      ),
                                    ),
                                  ]),
                                );
                              },
                            ));
                          }

                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: ColorUtils.PRIMARY_COLOR),
                      child: Text(
                        "Xác nhận",
                        style:
                        TextStyle(color: Colors.white, fontSize: 14.sp),
                      )),
                ),
              )
            ],
          )
              : Container()),
        );
  }

  Future<void> updated(StateSetter updateState) async {
    updateState(() {
      if (controller.controllerScoreQuestionAll.text.trim() == "") {
        controller.isShowhelperTextAll = true;
        controller.helperTextScoreQuestionAll = "Vui lòng nhập điểm của câu hỏi";
      } else {
        if (controller.controllerScoreQuestionAll.text.trim().substring(controller.controllerScoreQuestionAll.text.trim().length - 1) == ".") {
          controller.isShowhelperTextAll =
          true;
          controller.helperTextScoreQuestionAll = "Vui lòng nhập đúng định dạng điểm";
        } else {
          controller.isShowhelperTextAll = false;
          for(int i = 0;i<controller.studentDoExercise.value.questionAnswers!.length;i++){
            controller.teacherCommentAnswers[i].teacherComment = controller.controllerComment[i].text.trim();
            controller.teacherCommentAnswers[i].point = double.tryParse(controller.controllerScoreQuestion[i].text.trim());
            controller.teacherCommentAnswers[i].questionAnswerId = controller.studentDoExercise.value.questionAnswers![i].id;
          }
          controller.teacherGrading(
              Get.find<GradingExerciseQuestionController>().detailExercise.value.id
              ,controller.studentID.value
              ,controller.controllerCommentAll.text,
              double.tryParse(controller.controllerScoreQuestionAll.text.trim())
          );
        }
      }

    });
  }
}
