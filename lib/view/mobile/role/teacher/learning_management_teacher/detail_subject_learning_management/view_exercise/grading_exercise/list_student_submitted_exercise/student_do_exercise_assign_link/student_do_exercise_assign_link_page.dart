import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:html/parser.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import '../../../../../../../../../../commom/utils/ViewPdf.dart';
import '../../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../../commom/utils/global.dart';
import '../../../../../../../../../../commom/utils/open_url.dart';
import '../../../../../../../../../../commom/widget/text_field_custom.dart';
import '../../../../learning_management_teacher_controller.dart';
import '../../grading_exercise_assign_link/grading_exercise_assign_link_controller.dart';
import 'student_do_exercise_assign_link_controller.dart';

class StudentDoExerciseAssignLinkPage
    extends GetView<StudentDoExerciseAssignLinkController> {
  @override
  final controller = Get.put(StudentDoExerciseAssignLinkController());

  StudentDoExerciseAssignLinkPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              comeToHome();
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Text(
          'Bài Tập Môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}',
          style: const TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() => controller.isReady.value
          ? Column(
              children: [
                Expanded(
                    child: SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.all(16.h),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${controller.studentDoExercise.value.title}",
                          style: TextStyle(
                              color: const Color.fromRGBO(133, 133, 133, 1),
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w500),
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Ngày tạo",
                              style: TextStyle(
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w500),
                            ),
                            Text(
                              controller.outputDateFormat.format(
                                  DateTime.fromMillisecondsSinceEpoch(controller
                                          .studentDoExercise.value.createdAt ??
                                      0)),
                              style: TextStyle(
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Hạn nộp",
                              style: TextStyle(
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w500),
                            ),
                            Text(
                              controller.outputDateFormat.format(
                                  DateTime.fromMillisecondsSinceEpoch(controller
                                          .studentDoExercise.value.deadline ??
                                      0)),
                              style: TextStyle(
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Ghi chú",
                              style: TextStyle(
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w500),
                            ),
                            Text(
                              "${controller.studentDoExercise.value.description}",
                              style: TextStyle(
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Row(
                          children: [
                            SizedBox(
                              width: 48.w,
                              height: 48.w,
                              child:
                              CacheNetWorkCustom(urlImage:   "${controller.studentDoExercise.value.student?.image  }",),

                            ),
                            Padding(padding: EdgeInsets.only(right: 8.w)),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("${controller.studentDoExercise.value.student?.fullName}",style: TextStyle(color: Colors.black,fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                Visibility(
                                    visible:controller.studentDoExercise.value.student!
                                        .birthday != null,
                                    child:  Text(
                                      controller.outputDateFormatV2.format(
                                          DateTime.fromMillisecondsSinceEpoch(
                                              controller.studentDoExercise.value.student
                                                  ?.birthday ??
                                                  0)),
                                      style: TextStyle(
                                          color: const Color.fromRGBO(
                                              133, 133, 133, 1),
                                          fontSize: 12.sp),
                                    ))
                              ],
                            )

                          ],
                        ),
                        FocusScope(
                          node: FocusScopeNode(),
                          child: Focus(
                              onFocusChange: (focus) {
                                controller.update();
                              },
                              child: Container(
                                alignment: Alignment.centerLeft,
                                padding: const EdgeInsets.all(2.0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(6.0)),
                                    border: Border.all(
                                      width: 1,
                                      style: BorderStyle.solid,
                                      color: controller.focusAttachLink.hasFocus
                                          ? ColorUtils.PRIMARY_COLOR
                                          : const Color.fromRGBO(
                                              192, 192, 192, 1),
                                    )),
                                child: TextFormField(
                                  cursorColor: ColorUtils.PRIMARY_COLOR,
                                  focusNode: controller.focusAttachLink,
                                  onChanged: (value) {
                                    controller.update();
                                  },
                                  onFieldSubmitted: (value) {
                                    controller.update();
                                  },
                                  readOnly: true,
                                  controller: controller.controllerAttachLink,
                                  maxLines: null,
                                  decoration: InputDecoration(
                                    labelText: "Đường dẫn",
                                    labelStyle: TextStyle(
                                        fontSize: 14.0.sp,
                                        color: const Color.fromRGBO(
                                            177, 177, 177, 1),
                                        fontWeight: FontWeight.w500,
                                        fontFamily:
                                            'assets/font/static/Inter-Medium.ttf'),
                                    contentPadding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 16),
                                    prefixIcon: null,
                                    enabledBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    border: InputBorder.none,
                                    errorStyle: const TextStyle(height: 0),
                                    focusedErrorBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.auto,
                                  ),
                                ),
                              )),
                        ),
                        Padding(padding: EdgeInsets.only(top: 16.h)),
                        const Text('Bài làm',
                            style: TextStyle(
                                color: Color.fromRGBO(26, 26, 26, 1),
                                fontFamily:
                                    'assets/font/static/Inter-Medium.ttf',
                                fontSize: 16,
                                fontWeight: FontWeight.w500)),
                        Padding(padding: EdgeInsets.only(top: 16.h)),
                        Text(
                          "${parse(parse(controller.studentDoExercise.value.contentAnswer ?? "").body?.text).documentElement?.text}",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w400,
                              fontSize: 14.sp),
                        ),
                        Obx(() => Visibility(
                            visible: controller
                                .studentDoExercise.value.filesAnswer!
                                .where((element) =>
                                    element.ext == "png" ||
                                    element.ext == "jpg" ||
                                    element.ext == "jpeg" ||
                                    element.ext == "gif" ||
                                    element.ext == "bmp")
                                .toList()
                                .isNotEmpty,
                            child: GridView.builder(
                                padding: const EdgeInsets.all(10),
                                gridDelegate:
                                    SliverGridDelegateWithMaxCrossAxisExtent(
                                  maxCrossAxisExtent: 250.w,
                                  crossAxisSpacing: 10,
                                  mainAxisSpacing: 10,
                                ),
                                scrollDirection: Axis.vertical, //chiều cuộn
                                physics: const NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: controller
                                    .studentDoExercise.value.filesAnswer!
                                    .where((element) =>
                                        element.ext == "png" ||
                                        element.ext == "jpg" ||
                                        element.ext == "jpeg" ||
                                        element.ext == "gif" ||
                                        element.ext == "bmp")
                                    .toList()
                                    .length,
                                itemBuilder: (context, indexGrid) {
                                  return InkWell(
                                    onTap: () {
                                      OpenUrl.openImageViewer(
                                          context,
                                          controller.studentDoExercise.value
                                              .filesAnswer!
                                              .where((element) =>
                                                  element.ext == "png" ||
                                                  element.ext == "jpg" ||
                                                  element.ext == "jpeg" ||
                                                  element.ext == "gif" ||
                                                  element.ext == "bmp")
                                              .toList()[indexGrid]
                                              .link!);
                                    },
                                    child: SizedBox(
                                      width: 200.w,
                                      height: 200.w,
                                      child: Image.network(
                                        controller.studentDoExercise.value
                                                .filesAnswer!
                                                .where((element) =>
                                                    element.ext == "png" ||
                                                    element.ext == "jpg" ||
                                                    element.ext == "jpeg" ||
                                                    element.ext == "gif" ||
                                                    element.ext == "bmp")
                                                .toList()[indexGrid]
                                                .link ??
                                            "http://anhdep123.com/wp-content/uploads/2020/05/h%C3%ACnh-n%E1%BB%81n-tr%E1%BA%AFng-full-hd-768x1024.jpg",
                                        fit: BoxFit.cover,
                                        errorBuilder:
                                            (context, object, stackTrace) {
                                          return Image.asset(
                                            "assets/images/image_error_load.jpg",
                                          );
                                        },
                                      ),
                                    ),
                                  );
                                }))),
                        Obx(() => Visibility(
                            visible: controller
                                .studentDoExercise.value.filesAnswer!
                                .where((element) =>
                                    element.ext != "png" &&
                                    element.ext != "jpg" &&
                                    element.ext != "jpeg" &&
                                    element.ext != "gif" &&
                                    element.ext != "bmp")
                                .toList()
                                .isNotEmpty,
                            child: DottedBorder(
                                dashPattern: const [5, 5],
                                radius: const Radius.circular(6),
                                borderType: BorderType.RRect,
                                padding: const EdgeInsets.only(
                                    top: 12, left: 12, right: 12, bottom: 12),
                                color: ColorUtils.PRIMARY_COLOR,
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemCount: controller
                                        .studentDoExercise.value.filesAnswer!
                                        .where((element) =>
                                            element.ext != "png" &&
                                            element.ext != "jpg" &&
                                            element.ext != "jpeg" &&
                                            element.ext != "gif" &&
                                            element.ext != "bmp")
                                        .toList()
                                        .length,
                                    itemBuilder: (context, indexNotImage) {
                                      return InkWell(
                                        onTap: () {
                                          var action = 0;
                                          var extension = controller
                                              .studentDoExercise
                                              .value
                                              .filesAnswer!
                                              .where((element) =>
                                                  element.ext != "png" &&
                                                  element.ext != "jpg" &&
                                                  element.ext != "jpeg" &&
                                                  element.ext != "gif" &&
                                                  element.ext != "bmp")
                                              .toList()[indexNotImage]
                                              .ext;
                                          if (extension == "png" ||
                                              extension == "jpg" ||
                                              extension == "jpeg" ||
                                              extension == "gif" ||
                                              extension == "bmp") {
                                            action = 1;
                                          } else if (extension == "pdf") {
                                            action = 2;
                                          } else {
                                            action = 0;
                                          }
                                          switch (action) {
                                            case 1:
                                              OpenUrl.openImageViewer(
                                                  context,
                                                  controller.studentDoExercise
                                                      .value.filesAnswer!
                                                      .where((element) =>
                                                          element.ext != "png" &&
                                                          element.ext !=
                                                              "jpg" &&
                                                          element.ext !=
                                                              "jpeg" &&
                                                          element.ext !=
                                                              "gif" &&
                                                          element.ext != "bmp")
                                                      .toList()[indexNotImage]
                                                      .link!);
                                              break;
                                            case 2:
                                              Get.to(ViewPdfPage(
                                                  url:
                                                      controller
                                                          .studentDoExercise
                                                          .value
                                                          .filesAnswer!
                                                          .where((element) =>
                                                              element
                                                                      .ext !=
                                                                  "png" &&
                                                              element
                                                                      .ext !=
                                                                  "jpg" &&
                                                              element.ext !=
                                                                  "jpeg" &&
                                                              element.ext !=
                                                                  "gif" &&
                                                              element.ext !=
                                                                  "bmp")
                                                          .toList()[
                                                              indexNotImage]
                                                          .link!));
                                              break;
                                            default:
                                              OpenUrl.openFile(controller
                                                  .studentDoExercise
                                                  .value
                                                  .filesAnswer!
                                                  .where((element) =>
                                                      element.ext != "png" &&
                                                      element.ext != "jpg" &&
                                                      element.ext != "jpeg" &&
                                                      element.ext != "gif" &&
                                                      element.ext != "bmp")
                                                  .toList()[indexNotImage]
                                                  .link!);
                                              break;
                                          }
                                        },
                                        child: Container(
                                          margin:
                                              const EdgeInsets.only(top: 12),
                                          padding: const EdgeInsets.all(12),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(6),
                                              color: const Color.fromRGBO(
                                                  246, 246, 246, 1)),
                                          child: Row(
                                            children: [
                                              Image.network(
                                                controller.studentDoExercise
                                                    .value.filesAnswer!
                                                    .where((element) =>
                                                        element.ext != "png" &&
                                                        element.ext != "jpg" &&
                                                        element.ext != "jpeg" &&
                                                        element.ext != "gif" &&
                                                        element.ext != "bmp")
                                                    .toList()[indexNotImage]
                                                    .link!,
                                                errorBuilder: (
                                                  BuildContext context,
                                                  Object error,
                                                  StackTrace? stackTrace,
                                                ) {
                                                  return Image.asset(
                                                    getFileIcon(controller
                                                        .studentDoExercise
                                                        .value
                                                        .filesAnswer!
                                                        .where((element) =>
                                                            element.ext != "png" &&
                                                            element.ext !=
                                                                "jpg" &&
                                                            element.ext !=
                                                                "jpeg" &&
                                                            element.ext !=
                                                                "gif" &&
                                                            element.ext !=
                                                                "bmp")
                                                        .toList()[indexNotImage]
                                                        .name),
                                                    height: 35,
                                                    width: 30,
                                                  );
                                                },
                                                height: 35,
                                                width: 30,
                                              ),
                                              const Padding(
                                                  padding:
                                                      EdgeInsets.only(left: 8)),
                                              SizedBox(
                                                width: 220.w,
                                                child: Text(
                                                  '${controller.studentDoExercise.value.filesAnswer!.where((element) => element.ext != "png" && element.ext != "jpg" && element.ext != "jpeg" && element.ext != "gif" && element.ext != "bmp").toList()[indexNotImage].name}',
                                                  style: TextStyle(
                                                      color:
                                                          const Color.fromRGBO(
                                                              26, 59, 112, 1),
                                                      fontSize: 14.sp,
                                                      fontFamily:
                                                          'assets/font/static/Inter-Medium.ttf',
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                              Expanded(child: Container()),
                                              Image.asset(
                                                'assets/images/icon_upfile_subject.png',
                                                height: 16,
                                                width: 16,
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    })))),
                        Padding(padding: EdgeInsets.only(top: 16.h)),
                        GetBuilder<StudentDoExerciseAssignLinkController>(
                          builder: (controller) {
                            return MyTextFormFieldBinh(
                              enable: true,
                              focusNode: controller.focusScoreQuestion,
                              iconPrefix: null,
                              iconSuffix: null,
                              state: StateType.DEFAULT,
                              labelText: "Nhập Điểm Cho Câu Hỏi",
                              autofocus: false,
                              controller:
                              controller.controllerScoreQuestion,
                              helperText:
                              controller.helperTextScoreQuestion,
                              showHelperText: controller.isShowhelperText,
                              textInputAction: TextInputAction.next,
                              ishowIconPrefix: false,
                              keyboardType: TextInputType.number,
                            );
                          },
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 16.w),
                          child:MyOutlineBorderTextFormFieldBinh(
                            enable: true,
                            focusNode: controller.focusComment,
                            iconPrefix: null,
                            iconSuffix: null,
                            state: StateType.DEFAULT,
                            labelText: "Nhận xét",
                            autofocus: false,
                            controller: controller.controllerComment,
                            helperText: "vui lòng nhập câu hỏi",
                            showHelperText: false,
                            textInputAction: TextInputAction.next,
                            ishowIconPrefix: false,
                            keyboardType: TextInputType.text,
                          ),
                        )
                      ],
                    ),
                  ),
                )),
                Container(
                  color: Colors.white,
                  width: double.infinity,
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                  child: SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                        onPressed: () {
                          if (controller.controllerScoreQuestion.text.trim() ==
                              "") {
                            controller.isShowhelperText = true;
                            controller.helperTextScoreQuestion =
                                "Vui lòng nhập điểm của câu hỏi";
                            controller.update();
                          } else {
                            if (controller.controllerScoreQuestion.text
                                    .trim()
                                    .substring(controller
                                            .controllerScoreQuestion.text
                                            .trim()
                                            .length -
                                        1) ==
                                ".") {
                              controller.isShowhelperText = true;
                              controller.helperTextScoreQuestion =
                                  "Vui lòng nhập đúng định dạng điểm";
                              controller.update();
                            }else if(double.tryParse(controller.controllerScoreQuestion.text.trim())== null){
                              controller.isShowhelperText = true;
                              controller.helperTextScoreQuestion  = "Vui lòng nhập đúng định dạng điểm";
                              controller.update();
                            } else {
                              controller.isShowhelperText = false;
                              controller.update();
                              controller.teacherGrading(Get.find<GradingExerciseAssignLinkController>().detailExercise.value.id,controller.studentID.value, controller.controllerComment.text, double.tryParse(controller.controllerScoreQuestion.text.trim()));

                            }
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor: ColorUtils.PRIMARY_COLOR),
                        child: Text(
                          "Xác nhận",
                          style:
                              TextStyle(color: Colors.white, fontSize: 14.sp),
                        )),
                  ),
                )
              ],
            )
          : Container()),
    );
  }

  Future<void> updated(StateSetter updateState) async {
    updateState(() {
      if (controller.controllerScoreQuestionAll.text.trim() == "") {
        controller.isShowhelperTextAll = true;
        controller.helperTextScoreQuestionAll = "Vui lòng nhập điểm của câu hỏi";
      } else {
        if (controller.controllerScoreQuestionAll.text.trim().substring(controller.controllerScoreQuestionAll.text.trim().length - 1) == ".") {
          controller.isShowhelperTextAll =
          true;
          controller.helperTextScoreQuestionAll = "Vui lòng nhập đúng định dạng điểm";
        } else {
          controller.isShowhelperTextAll = false;

        }
      }

    });
  }
}
