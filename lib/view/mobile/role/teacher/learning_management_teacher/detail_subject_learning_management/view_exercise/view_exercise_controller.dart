import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/data/model/res/exercise/detail_exercise.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../../data/model/res/exercise/list_student_exercise.dart';
import '../../../../../../../data/repository/exercise/excercise_repo.dart';
import '../../../teacher_home_controller.dart';
import '../learning_management_teacher_controller.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';

class ViewExerciseController extends GetxController{
  var controllerDateStart = TextEditingController().obs;
  var controllerDateEnd = TextEditingController().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy HH:mm');
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  var listExercise = ExerciseStudent().obs;
  RxList<ItemsExercise> itemsExercise = <ItemsExercise>[].obs;
  var statusExercise = "".obs;
  var isPublic = "".obs;
  var listIdStudent = <String>[].obs;
  var studentSubmitted = <StudentSubmitted>[].obs;
  var listStudentExercise = ListStudentSubmitted().obs;
  var studentUnSubmitted = <StudentUnSubmitted>[].obs;
  var listItemPopupMenu = <PopupMenuItem>[];

  var showAnnounced = false;
  var showUpdateExercise = false;
  var showDeleteExercise = false;
  var showMarkExercise = false;
  var showAnnouncedPointExercise = false;

  var indexAnnounced = 0;
  var indexUpdateExercise = 0;
  var indexDeleteExercise = 0;
  var indexMarkExercise = 0;
  var indexAnnouncedPointExercise = 0;

  var heightAnnounced = 30.0;
  var heightUpdateExercise = 30.0;
  var heightDeleteExercise = 30.0;
  var heightMarkExercise = 30.0;
  var heightAnnouncedPointExercise = 30.0;
  DetailExercise detailExercise = DetailExercise();
  RxBool isReady = false.obs;

  @override
  void onInit() {
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      controllerDateStart.value.text =   outputDateFormat.format(DateTime(Get.find<TeacherHomeController>().fromYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<TeacherHomeController>().fromYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
    }else{
      controllerDateStart.value.text =   outputDateFormat.format(DateTime(Get.find<TeacherHomeController>().toYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<TeacherHomeController>().toYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
    }
    getListExercise(statusExercise.value,isPublic.value);
    super.onInit();
  }
  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }


  clickItemListViewExercise(index){
    if (itemsExercise[index].files!.isNotEmpty) {
      Get.toNamed(Routes.detailExerciseUploadFilePage, arguments: itemsExercise[index]);
    } else {
      if (itemsExercise[index].questions!.isNotEmpty) {
        Get.toNamed(Routes.viewExerciseQuestionPage, arguments: itemsExercise[index].id);
      } else {
        if (itemsExercise[index].link != "" && itemsExercise[index].link != null) {
          Get.toNamed(Routes.detailExerciseAssignLinkPage, arguments: itemsExercise[index]);
        }
      }
    }
  }


  getDetailExercise(index){
   exerciseRepo.detailExercise(itemsExercise[index].id).then((value) {
      if (value.state == Status.SUCCESS) {
        detailExercise = value.object!;
      }
    });
  }

  getListItemPopupMenu(index){
    listItemPopupMenu = [];
    var announced =  PopupMenuItem<int>(
        value: 0,
        padding: EdgeInsets.zero,
        height: heightAnnounced,
        child: showAnnounced
            ?Container(
          padding: EdgeInsets.symmetric(vertical: 4.h),
          decoration:  const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black26))),
          child: Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Icon(
                Icons
                    .upload,
                color: Colors
                    .black,
              ),
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Text(
                  "Công bố")
            ],
          ),
        )
            :Row(
          children: [
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Icon(
              Icons
                  .upload,
              color: Colors
                  .black,
            ),
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Text(
                "Công bố")
          ],
        )
    );
    var updateExercise =  PopupMenuItem<int>(
        value: 1,
        padding: EdgeInsets.zero,
        height: heightUpdateExercise,
        child: showUpdateExercise
            ?Container(
          padding: EdgeInsets.symmetric(vertical: 4.h),
          decoration:  const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black26))),
          child: Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Icon(
                Icons
                    .mode_edit_outline,
                color: Colors
                    .black,
              ),
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Text(
                  "Sửa bài tập")
            ],
          ),
        )
            :Row(
          children: [
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Icon(
              Icons
                  .mode_edit_outline,
              color: Colors
                  .black,
            ),
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Text(
                "Sửa bài tập")
          ],
        )
    );
    var deleteExercise =  PopupMenuItem<int>(
        value: 2,
        padding: EdgeInsets.zero,
        height: 30,
        child: Row(
          children: [
            Padding(
                padding: EdgeInsets
                    .only(
                    right:
                    8.w)),
            const Icon(
                Icons.delete),
            Padding(
                padding: EdgeInsets
                    .only(
                    right:
                    8.w)),
            const Text(
              "Xóa bài tập",
            )
          ],
        )
    );
    var markExercise =  PopupMenuItem<int>(
        value: 3,
        padding: EdgeInsets.zero,
        height: heightMarkExercise,
        child: showMarkExercise
            ?Container(
          padding: EdgeInsets.symmetric(vertical: 4.h),
          decoration:  const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black26))),
          child: Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Icon(
                Icons
                    .check_circle_outline,
                color: Colors
                    .black,
              ),
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Text(
                  "Chấm điểm"),

            ],
          ),
        )
            :Row(
          children: [
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Icon(
              Icons
                  .check_circle_outline,
              color: Colors
                  .black,
            ),
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Text(
                "Chấm điểm"),

          ],
        )
    );
    var announcedPointExercise =  PopupMenuItem<int>(
        value: 4,
        padding: EdgeInsets.zero,
        height: heightAnnouncedPointExercise,
        child: showAnnouncedPointExercise
            ?Container(
          padding: EdgeInsets.symmetric(vertical: 4.h),
          decoration:  const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black26))),
          child: Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Icon(
                Icons
                    .upload,
                color: Colors
                    .black,
              ),
              Padding(
                  padding: EdgeInsets.only(
                      right:
                      8.w)),
              const Text(
                  "Công bố điểm")
            ],
          ),
        )
            :Row(
          children: [
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Icon(
              Icons
                  .upload,
              color: Colors
                  .black,
            ),
            Padding(
                padding: EdgeInsets.only(
                    right:
                    8.w)),
            const Text(
                "Công bố điểm")
          ],
        )
    );
    var viewResultExercise =   PopupMenuItem<int>(
      value: 5,
      padding: EdgeInsets.zero,
      height: 30,
      child: Row(
        children: [
          Padding(
              padding: EdgeInsets.only(
                  right:
                  8.w)),
          const Icon(
            Icons.remove_red_eye,
            color: Colors
                .black,
          ),
          Padding(
              padding: EdgeInsets.only(
                  right:
                  8.w)),
          const Text(
              "Xem kết quả bài tập")
        ],
      ),
    );

    if(itemsExercise[index].createdBy == AppCache().userId && itemsExercise[index].isPublic == "FALSE"){
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXERCISE_PUBLIC)){
        listItemPopupMenu.add(announced);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXERCISE_EDIT)){
        listItemPopupMenu.add(updateExercise);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXERCISE_DELETE)){
        listItemPopupMenu.add(deleteExercise);
      }
    }
    if(itemsExercise[index].isPublicScore == "FALSE" && itemsExercise[index].createdBy == AppCache().userId && itemsExercise[index].isPublic ==
        "TRUE"){
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXERCISE_MARK)){
        listItemPopupMenu.add(markExercise);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXERCISE_PUBLIC_SCORE)){
        listItemPopupMenu.add(announcedPointExercise);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_RESULT_EXERCISE_VIEW)){
        listItemPopupMenu.add(viewResultExercise);
      }
    }
    if(itemsExercise[index].isPublicScore == "TRUE" && itemsExercise[index].createdBy == AppCache().userId && itemsExercise[index].isPublic == "TRUE"){
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXERCISE_MARK)){
        listItemPopupMenu.add(markExercise);
      }
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_RESULT_EXERCISE_VIEW)){
        listItemPopupMenu.add(viewResultExercise);
      }
    }

    if(itemsExercise[index].createdBy != AppCache().userId && itemsExercise[index].isPublic ==
        "TRUE"){
      if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_RESULT_EXERCISE_VIEW)){
        listItemPopupMenu.add(viewResultExercise);
      }
    }




    indexAnnounced = listItemPopupMenu.indexOf(announced);
    indexUpdateExercise = listItemPopupMenu.indexOf(updateExercise);
    indexDeleteExercise = listItemPopupMenu.indexOf(deleteExercise);
    indexMarkExercise = listItemPopupMenu.indexOf(markExercise);
    indexAnnouncedPointExercise = listItemPopupMenu.indexOf(announcedPointExercise);

    showAnnounced = setVisiblePopupMenuItem(indexAnnounced,listItemPopupMenu);
    showUpdateExercise = setVisiblePopupMenuItem(indexUpdateExercise,listItemPopupMenu);
    showDeleteExercise = setVisiblePopupMenuItem(indexDeleteExercise,listItemPopupMenu);
    showMarkExercise = setVisiblePopupMenuItem(indexMarkExercise,listItemPopupMenu);
    showAnnouncedPointExercise = setVisiblePopupMenuItem(indexAnnouncedPointExercise,listItemPopupMenu);

    heightAnnounced = setHeightPopupMenuItem(indexAnnounced,listItemPopupMenu);
    heightUpdateExercise = setHeightPopupMenuItem(indexUpdateExercise,listItemPopupMenu);
    heightDeleteExercise = setHeightPopupMenuItem(indexDeleteExercise,listItemPopupMenu);
    heightMarkExercise = setHeightPopupMenuItem(indexMarkExercise,listItemPopupMenu);
    heightAnnouncedPointExercise = setHeightPopupMenuItem(indexAnnouncedPointExercise,listItemPopupMenu);

    update();

    return listItemPopupMenu;
  }


  getListExercise(status,ispPublic){
    var idSubject = Get.find<LearningManagementTeacherController>().detailSubject.value.id;
    var fromDate = DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2),),00,00).millisecondsSinceEpoch;
    var toDate = DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2)),23,59).millisecondsSinceEpoch;
    exerciseRepo.listExerciseTeacher(idSubject,status,fromDate, toDate,ispPublic).then((value) {
      if (value.state == Status.SUCCESS) {
        listExercise.value= value.object!;
        if(listExercise.value.items != null){
          itemsExercise.value = listExercise.value.items!;
        }
        AppUtils.shared.hideLoading();
      }
    });
    isReady.value = true;
  }


  publicExercise(exerciseId) {
    exerciseRepo.publicExercise(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Công bố bài tập thành công");
        getListExercise(setTypeExercise(statusExercise.value),setStatusPublic(isPublic.value));
        Get.back();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Công bố bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


  deleteExercise(exerciseId) {
    exerciseRepo.deleteExercise(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Xóa bài tập thành công");
        getListExercise(setTypeExercise(statusExercise.value),setStatusPublic(isPublic.value));
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Xóa bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }



  getListStudentSubmit(exerciseId){
    exerciseRepo.studentsSubmitted(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudentExercise.value = value.object!;
        studentSubmitted.value = listStudentExercise.value.studentSubmitted!;
        studentUnSubmitted.value = listStudentExercise.value.studentUnSubmitted!;
        for (int i = 0; i < studentSubmitted.length; i++) {
          listIdStudent.add(studentSubmitted[i].student!.id!);
        }
      }
    });
    studentSubmitted.refresh();
    studentUnSubmitted.refresh();
  }


  teacherPublicScore(idExercise) {
    exerciseRepo.teacherPublicScore(idExercise,listIdStudent).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Công bố điểm bài tập thành công");
        Get.back();
        onInit();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Công bố điểm bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);

      }
    });
  }




  setTypeExercise(type){
    switch(type){
      case "Đến hạn":
        return "DUE";
      case "Đã hết hạn":
        return "EXPIRED";
      case "Sắp đến hạn":
        return "DEADLINE_COMING_SOON";
      default:
        return "";
    }
  }

  getStatusExercise(type){
    switch(type){
      case "DUE":
        return "Đến hạn";
      case "EXPIRED":
        return "Đã hết hạn";
      case "DEADLINE_COMING_SOON":
        return "Sắp đến hạn";
      default:
        return "";
    }
  }


  getTypeExercise(type){
    switch(type){
      case "ALL":
        return "Trắc nghiệm & Tự luận";
      case "SELECTED_RESPONSE":
        return "Trắc nghiệm";
      case "CONSTRUCTED_RESPONSE":
        return "Tự luận";
      default:
        return "";
    }
  }


  getStatusPublic(type){
    switch(type){
      case "TRUE":
        return "Công bố";
      case "FALSE":
        return "Chưa công bố";
      default:
        return "";
    }
  }



  setStatusPublic(type){
    switch(type){
      case "Công bố":
        return "TRUE";
      case "Chưa công bố":
        return "FALSE";
      default:
        return "";
    }
  }

  goToGradingExercise(ItemsExercise itemsExercise){
    if ( itemsExercise.files!.isNotEmpty) {
      Get.toNamed(Routes.gradingExerciseUploadFilePage, arguments: itemsExercise);
    } else {
      if (itemsExercise.questions!.isNotEmpty) {
        Get.toNamed(Routes.gradingExerciseQuestionPage, arguments: itemsExercise);
      } else {
        if (itemsExercise.link != "" && itemsExercise.link != null) {
          Get.toNamed(Routes.gradingExerciseAssignLinkPage, arguments: itemsExercise);
        }
      }
    }
  }






}