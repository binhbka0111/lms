import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import '../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/res/exercise/detail_exercise.dart';
import '../../../../../../../../../data/model/res/exercise/exercise.dart';
import '../../../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../../../../data/repository/exercise/excercise_repo.dart';
import 'dart:io';
import '../../../../../../../../../data/repository/file/file_repo.dart';
import '../../view_exercise_controller.dart';
class UpdateExerciseQuestionController extends GetxController{
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  var groupValue = <int>[].obs;
  var idExercise = "".obs;
  late final DetailExercise? detailExercise;
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var focusNameExercise = FocusNode();
  var controllerNameExercise = TextEditingController();
  var controllerFilter= TextEditingController();
  var focusDescribe = FocusNode();
  var controllerDescribe = TextEditingController();
  var controllerDateEnd = TextEditingController();
  var controllerTimeEnd = TextEditingController();
  var focusScoreQuestion = <FocusNode>[];
  var controllerScoreQuestion = <TextEditingController>[];
  var focusQuestion = <FocusNode>[];
  var controllerQuestion = <TextEditingController>[];
  var listErrorTextPointQuestion = <String>[].obs;
  var listStatusAnswer = <dynamic>[].obs;
  var isReady = false.obs;
  var listQuestion = <Questions>[].obs;
  var listQuestionCopy = <Questions>[].obs;
  var listStatusEdit = <bool>[].obs;
  var isShowNote = <bool>[].obs;
  var listStatus = <String>[].obs;
  var filesUploadExercise = <ResponseFileUpload>[].obs;
  final FileRepo fileRepo = FileRepo();
  var listExpand = <bool>[].obs;
  var isShowErrorTextNameExercise = false;
  var isShowErrorTextDate= false;
  var isShowErrorTextTime= false;
  var listIdQuestion = <String>[];
  AutoScrollController autoScrollController = AutoScrollController();
  ScrollController scrollController = ScrollController();
  @override
  void onInit() {
    super.onInit();
    var tmpItemExercise = Get.arguments;
    if(tmpItemExercise!=null){
      idExercise.value = tmpItemExercise;
      getDetailExerciseCopy();
      getDetailExercise(idExercise.value);
    }
  }




  getListQuestionCopy2(){
    listQuestionCopy.value = [];
    for(int i = 0;i <listQuestion.length;i++){
      if(listIdQuestion.contains(listQuestion[i].id)){
        listQuestionCopy.add(Questions(
            content: listQuestion[i].content,
            files: getListFile(i),
            id: listQuestion[i].id,
            answerOption: getListAnswerOption(i),
            errorContentQuestion: listQuestion[i].errorContentQuestion,
            errorPointQuestion: listQuestion[i].errorContentQuestion,
            point: listQuestion[i].point,
            typeQuestion:  listQuestion[i].typeQuestion
        ));
      }

    }
  }

  getListQuestionCopy(){
    for(int i = 0;i <listQuestion.length;i++){
      listQuestionCopy.add(Questions(
        content: listQuestion[i].content,
        files: getListFile(i),
        id: listQuestion[i].id,
        answerOption: getListAnswerOption(i),
        errorContentQuestion: listQuestion[i].errorContentQuestion,
        errorPointQuestion: listQuestion[i].errorContentQuestion,
        point: listQuestion[i].point,
        typeQuestion:  listQuestion[i].typeQuestion
      ));
    }
  }

  getListFile(i){
    var listFile = <ResponseFileUpload>[];
    for(int j = 0;j <listQuestion[i].files!.length;j++){
      listFile.add(ResponseFileUpload(
          tmpFolderUpload: listQuestion[i].files![j].tmpFolderUpload,
          size:  listQuestion[i].files![j].size,
          ext:  listQuestion[i].files![j].ext,
          link: listQuestion[i].files![j].link ,
          name: listQuestion[i].files![j].name,
          originalFileName: listQuestion[i].files![j].originalFileName
      ));
    }
    return listFile;
  }


  getListAnswerOption(i){
    var listAnswerOption = <AnswerOption>[];
    for(int j = 0;j <listQuestion[i].answerOption!.length;j++){
      listAnswerOption.add(AnswerOption(
          value: listQuestion[i].answerOption![j].value,
          textEditingController: TextEditingController(),
          validateAnswer: false,
          key: listQuestion[i].answerOption![j].key,
          focusNode: FocusNode(),
          errorTextAnswer: "",
          status:listQuestion[i].answerOption![j].status
      ));
    }
    return listAnswerOption;
  }



  getDetailExerciseCopy(){
    exerciseRepo.detailExercise(idExercise.value).then((value) {
      if (value.state == Status.SUCCESS) {
        detailExercise = value.object!;
      }
    });
  }



  getDetailExercise(exerciseId) {
    exerciseRepo.detailExercise(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Lấy chi tiết bài tập thành công");
        controllerNameExercise.text = value.object!.title!;
        controllerDescribe.text = value.object!.description!;
        controllerDateEnd.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(value.object!.deadline!)).substring(0,10);
        controllerTimeEnd.text = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(value.object!.deadline!)).substring(11,16);
        listQuestion.value = value.object!.questions!;
        for(int i = 0;i <listQuestion.length;i++){
          listIdQuestion.add(listQuestion[i].id!);
        }
        setListQuestion();
        getListQuestionCopy();
        isReady.value = true;
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lấy chi tiết bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        isReady.value = true;
      }
    });
  }


  setListQuestion(){
    listStatusEdit.value = [];
    isShowNote.value = [];
    listExpand.value = [];
    listErrorTextPointQuestion.value = [];
    controllerQuestion = [];
    focusQuestion = [];
    controllerScoreQuestion = [];
    focusScoreQuestion = [];
    groupValue.value = [];

    for(int i = 0;i <listQuestion.length;i++){
      listQuestion[i].errorPointQuestion = false;
      listQuestion[i].errorContentQuestion = false;
      listStatusEdit.add(true);
      isShowNote.add(false);
      listExpand.add(false);
      listErrorTextPointQuestion.add("");
      controllerQuestion.add(TextEditingController());
      focusQuestion.add(FocusNode());
      controllerScoreQuestion.add(TextEditingController());
      focusScoreQuestion.add(FocusNode());
      controllerQuestion[i].text = listQuestion[i].content!;
      controllerScoreQuestion[i].text = listQuestion[i].point.toString();
      if(listQuestion[i].answerOption!.isEmpty){
        groupValue.add(1);
      }else{
        groupValue.add(0);
      }
      for(int j =0;j<listQuestion[i].answerOption!.length;j++){
        listQuestion[i].answerOption?[j].focusNode = FocusNode();
        listQuestion[i].answerOption?[j].textEditingController = TextEditingController();
        listQuestion[i].answerOption?[j].validateAnswer = false;
        listQuestion[i].answerOption?[j].textEditingController?.text = listQuestion[i].answerOption![j].value!;
      }
    }
    listStatusEdit.refresh();
    isShowNote.refresh();
    listExpand.refresh();
    listErrorTextPointQuestion.refresh();
    groupValue.refresh();

  }




  uploadFile(index,file) async {
    showDialogUploadFile();
    var fileList = <File>[];
    for (var element in file) {
      fileList.add(element.file!);
    }
  await  fileRepo.uploadFile(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;
        listQuestion[index].files?.addAll(listResFile);
        listQuestion.refresh();
        listResFile = [];
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
    Get.back();
  }



  updateExercise(exerciseId,title,scoreFactor,deadline,description,typeExercise,subjectId,teacherId,classId,questions) {
    exerciseRepo.updateExerciseQuestions(exerciseId,title,scoreFactor,deadline,description,typeExercise,subjectId,teacherId,classId,questions).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật bài tập thành công");
        Get.back();
        Get.find<ViewExerciseController>().onInit();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Cập nhật bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


  getIndexQuestion(id){
    return (listIdQuestion.indexOf(id)+1);
  }

}