import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/res/exercise/list_student_exercise.dart';
import '../../../../../../../../../data/repository/exercise/excercise_repo.dart';
import '../list_student_excise_controller.dart';


class ListStudentUnSubmitResultExerciseController extends GetxController{
  var listStudentExercise = ListStudentSubmitted().obs;
  var studentUnSubmitted = <StudentUnSubmitted>[].obs;
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy');
  @override
  void onInit() {
    getListStudentUnSubmit(Get.find<ListStudentExerciseController>().itemsExercise.value.id);
    super.onInit();
  }

  getListStudentUnSubmit(exerciseId){
    exerciseRepo.studentsSubmitted(exerciseId).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudentExercise.value = value.object!;
        studentUnSubmitted.value = listStudentExercise.value.studentUnSubmitted!;
      }
    });
  }

}