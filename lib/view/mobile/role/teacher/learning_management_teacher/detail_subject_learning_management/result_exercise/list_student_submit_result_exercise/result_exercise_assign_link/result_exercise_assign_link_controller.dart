import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../../data/model/res/exercise/student_do_exercise.dart';
import '../../../../../../../../../../data/model/res/exercise/teacherCommentAnswer.dart';
import '../../../../../../../../../../data/repository/exercise/excercise_repo.dart';
import '../../list_student_excise_controller.dart';




class ResultExerciseAssignLinkController extends GetxController{
  var focusAttachLink = FocusNode();
  var controllerAttachLink = TextEditingController();
  var focusScoreQuestion = FocusNode();
  var controllerScoreQuestion = TextEditingController();
  var focusScoreQuestionAll = FocusNode();
  var controllerScoreQuestionAll = TextEditingController();
  var focusComment = FocusNode();
  var controllerComment = TextEditingController();
  var focusCommentAll = FocusNode();
  var controllerCommentAll = TextEditingController();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var studentDoExercise = StudentDoExercise().obs;
  var studentID = "".obs;
  var isReady = false.obs;
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  var helperTextScoreQuestion = "Vui lòng nhập điểm số";
  var isShowHelperText = false;
  var helperTextScoreQuestionAll = "Vui lòng nhập điểm số";
  var isShowHelperTextAll = false;
  var  teacherCommentAnswers = <TeacherCommentAnswer>[].obs;
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy');
  @override
  void onInit() {
    var tmpStudentId = Get.arguments;
    if (tmpStudentId != null) {
      studentID.value = tmpStudentId;
    }
    getDetailExercise(Get.find<ListStudentExerciseController>().itemsExercise.value.id,studentID.value);
    super.onInit();
  }


  getDetailExercise(exerciseId,studentId) {
    exerciseRepo.detailStudentDoExercise(exerciseId,studentId).then((value) {
      if (value.state == Status.SUCCESS) {
        studentDoExercise.value = value.object!;
        controllerAttachLink.text = studentDoExercise.value.link!;
        AppUtils.shared.showToast("Lấy chi tiết bài tập thành công");
        isReady.value = true;
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Lấy chi tiết bài tập thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
        isReady.value = true;
      }
    });
  }


}