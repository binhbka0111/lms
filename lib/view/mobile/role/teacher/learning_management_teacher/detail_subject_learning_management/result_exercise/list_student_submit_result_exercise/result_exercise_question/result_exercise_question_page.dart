import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:html/parser.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/detail_subject_learning_management/result_exercise/list_student_submit_result_exercise/result_exercise_question/result_exercise_question_controller.dart';
import '../../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../../../../commom/widget/list_view_image.dart';
import '../../../learning_management_teacher_controller.dart';

class ResultExerciseQuestionPage
    extends GetView<ResultExerciseQuestionController> {
  @override
  final controller = Get.put(ResultExerciseQuestionController());

  ResultExerciseQuestionPage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {
                  comeToHome();
                },
                icon: const Icon(Icons.home),
                color: Colors.white,
              )
            ],
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            title: Text(
              'Bài Tập Môn ${Get.find<LearningManagementTeacherController>().detailSubject.value.subjectCategory?.name}',
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body: Obx(() => controller.isReady.value
              ? Column(
            children: [
              Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      margin: EdgeInsets.all(16.h),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${controller.studentDoExercise.value.title}",
                            style: TextStyle(
                                color: const Color.fromRGBO(133, 133, 133, 1),
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w500),
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Ngày tạo",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                controller.outputDateFormat.format(
                                    DateTime.fromMillisecondsSinceEpoch(controller
                                        .studentDoExercise.value.createdAt ??
                                        0)),
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Hạn nộp",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Text(
                                controller.outputDateFormat.format(
                                    DateTime.fromMillisecondsSinceEpoch(controller
                                        .studentDoExercise.value.deadline ??
                                        0)),
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w400),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Ghi chú",
                                style: TextStyle(
                                    color: const Color.fromRGBO(133, 133, 133, 1),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Padding(padding: EdgeInsets.only(right: 16.w)),
                              SizedBox(
                                width: 260.w,
                                child: Text(
                                  "${controller.studentDoExercise.value.description}",
                                  style: TextStyle(
                                      color: const Color.fromRGBO(26, 26, 26, 1),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Row(
                            children: [
                              SizedBox(
                                width: 48.w,
                                height: 48.w,
                                child:

                                CacheNetWorkCustom(urlImage: '${controller.studentDoExercise.value.student?.image}'),

                              ),
                              Padding(padding: EdgeInsets.only(right: 8.w)),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("${controller.studentDoExercise.value.student?.fullName}",style: TextStyle(color: Colors.black,fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                  Visibility(
                                      visible:controller.studentDoExercise.value.student!.birthday != null,
                                      child:  Text(
                                        controller.outputDateFormatV2.format(
                                            DateTime.fromMillisecondsSinceEpoch(
                                                controller.studentDoExercise.value.student
                                                    ?.birthday ??
                                                    0)),
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                133, 133, 133, 1),
                                            fontSize: 12.sp),
                                      ))
                                ],
                              )

                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 16.h)),
                          ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: controller.studentDoExercise.value.questionAnswers?.length,
                            itemBuilder: (context, index) {
                              var listImage =  controller.studentDoExercise.value.questionAnswers![index].question!.files!.where((element) => element.ext == "png" ||
                                  element.ext == "jpg" ||
                                  element.ext == "jpeg" ||
                                  element.ext == "gif" ||
                                  element.ext == "bmp").toList().obs;
                              var listNotImage =  controller.studentDoExercise.value.questionAnswers![index].question!.files!.where((element) =>
                              element.ext != "png" &&
                                  element.ext != "jpg" &&
                                  element.ext != "jpeg" &&
                                  element.ext != "gif" &&
                                  element.ext != "bmp")
                                  .toList().obs;


                              var listImageStudentDoExercise =  controller.studentDoExercise.value.questionAnswers![index].files!.where((element) => element.ext == "png" ||
                                  element.ext == "jpg" ||
                                  element.ext == "jpeg" ||
                                  element.ext == "gif" ||
                                  element.ext == "bmp").toList().obs;
                              var listNotImageStudentDoExercise =  controller.studentDoExercise.value.questionAnswers![index].files!.where((element) =>
                              element.ext != "png" &&
                                  element.ext != "jpg" &&
                                  element.ext != "jpeg" &&
                                  element.ext != "gif" &&
                                  element.ext != "bmp")
                                  .toList().obs;
                              return Container(
                                margin: EdgeInsets.only(bottom: 8.h),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      child: RichText(
                                          text: TextSpan(children: [
                                            TextSpan(
                                                text: 'Câu ${index + 1}: ',
                                                style: TextStyle(
                                                    color: const Color.fromRGBO(
                                                        133, 133, 133, 1),
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 12.sp,
                                                    fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf')),
                                            TextSpan(
                                                text:
                                                '[${controller.studentDoExercise.value.questionAnswers?[index].question?.point.toString()}đ] ',
                                                style: TextStyle(
                                                    color: ColorUtils.PRIMARY_COLOR,
                                                    fontSize: 12.sp,
                                                    fontWeight: FontWeight.w500,
                                                    fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf')),
                                            TextSpan(
                                                text:
                                                '${controller.studentDoExercise.value.questionAnswers![index].question?.content}: ',
                                                style: TextStyle(
                                                    color: const Color.fromRGBO(
                                                        26, 26, 26, 1),
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 12.sp,
                                                    fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf')),
                                          ])),
                                    ),
                                    Padding(padding: EdgeInsets.only(top: 16.h)),
                                    ListViewShowImage.showGridviewImage(listImage),
                                    ListViewShowImage.showListViewNotImage(listNotImage),
                                    Visibility(
                                        visible: controller.studentDoExercise.value.questionAnswers?[index].question?.typeQuestion == "SELECTED_RESPONSE",
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          physics: const NeverScrollableScrollPhysics(),
                                          itemCount: controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?.length,
                                          itemBuilder: (context, indexAnswer) {
                                            return Container(
                                              margin: EdgeInsets.only(bottom: 8.h),
                                              child: Row(
                                                children: [
                                                  Container(
                                                    height: 32.h,
                                                    width: 32.h,
                                                    decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: controller.setBackgroundKey(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                            , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                            , controller.studentDoExercise.value.questionAnswers?[index].answer!),
                                                        border: Border.all(color: controller.setBorderKey(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                            , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                            , controller.studentDoExercise.value.questionAnswers?[index].answer!))
                                                    ),
                                                    padding: EdgeInsets.all(8.h),
                                                    alignment: Alignment.center,
                                                    child: Text(
                                                      "${controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key}",
                                                      style: TextStyle(
                                                          color: controller.setColorTextKey(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                              , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                              , controller.studentDoExercise.value.questionAnswers?[index].answer!),
                                                          fontSize: 14.sp,
                                                          fontWeight: FontWeight.w500),
                                                    ),
                                                  ),
                                                  Padding(
                                                      padding:
                                                      EdgeInsets.only(right: 8.w)),
                                                  Container(
                                                    padding: EdgeInsets.symmetric(
                                                        horizontal: 14.w,
                                                        vertical: 8.h),
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                        BorderRadius.circular(6.r),
                                                        border: Border.all(
                                                            color: controller.setBorderAnswer(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                                , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                                , controller.studentDoExercise.value.questionAnswers?[index].answer!)),
                                                        color: Colors.white),
                                                    child: SizedBox(
                                                      width: 220.w,
                                                      child: Text(
                                                        "${controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].value}",style: TextStyle(
                                                          color: controller.setColorTextAnswer(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                              , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                              , controller.studentDoExercise.value.questionAnswers?[index].answer!)
                                                      ),),
                                                    ),
                                                  ),
                                                  Padding(
                                                      padding:
                                                      EdgeInsets.only(right: 8.w)),
                                                  Visibility(
                                                      visible:  controller.visibleTrue(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                          , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                          , controller.studentDoExercise.value.questionAnswers?[index].answer!),
                                                      child: const Icon(
                                                        Icons.check,
                                                        color: Color.fromRGBO(
                                                            77, 197, 145, 1),
                                                      )),
                                                  Visibility(
                                                      visible: controller.visibleFalse(controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].status
                                                          , controller.studentDoExercise.value.questionAnswers?[index].question!.answerOption?[indexAnswer].key
                                                          , controller.studentDoExercise.value.questionAnswers?[index].answer!),
                                                      child: const Icon(
                                                        Icons.clear,
                                                        color: Color.fromRGBO(255, 69, 89, 1),
                                                      ))
                                                ],
                                              ),
                                            );
                                          },
                                        )),
                                    Visibility(
                                      visible: controller.studentDoExercise.value.questionAnswers?[index].question?.typeQuestion == "CONSTRUCTED_RESPONSE",
                                      child: const Text('Bài làm',
                                          style: TextStyle(
                                              color: Color.fromRGBO(26, 26, 26, 1),
                                              fontFamily:
                                              'assets/font/static/Inter-Medium.ttf',
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500)),),
                                    Visibility(
                                        visible: controller.studentDoExercise.value.questionAnswers?[index].question?.typeQuestion == "CONSTRUCTED_RESPONSE",
                                        child: Text(
                                          "${parse(parse(controller.studentDoExercise.value.questionAnswers?[index].answer ?? "").body?.text).documentElement?.text}",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14.sp),
                                        )),
                                    Padding(padding: EdgeInsets.only(top: 8.h)),
                                    Obx(() => ListViewShowImage.showGridviewImage(listImageStudentDoExercise)),
                                    Obx(() => ListViewShowImage.showListViewNotImage(listNotImageStudentDoExercise)),
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  )),

            ],
          )
              : Container()),
        );
  }

}
