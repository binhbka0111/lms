import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../../../../data/model/res/exercise/list_student_exercise.dart';
import 'list_student_submit_result_exercise/list_student_submit_result_exercise_controller.dart';
import 'list_student_un_submit_result_exercise/list_student_un_submit_result_exercise_controller.dart';


class ListStudentExerciseController extends GetxController{
  var focusAttachLink = FocusNode();
  var controllerAttachLink = TextEditingController();
  var outputDateFormat = DateFormat('dd/MM/yyyy HH:mm');
  var listTitle = <String>[].obs;
  var listStudentExam = ListStudentSubmitted().obs;
  var itemsExercise = ItemsExercise().obs;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  RxInt indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  @override
  void onInit() {
    var detailItem = Get.arguments;
    if (detailItem != null) {
      itemsExercise.value = detailItem;
    }
    listTitle.value = ["Học sinh đã nộp","Học sinh chưa nộp"];
    super.onInit();
  }
  @override
  dispose() {
    pageController.dispose();
    super.dispose();
  }

  onPageViewChange(int page) {
    indexClick.value = page;
    if (page != 0) {
      indexClick.value- 1;
    } else {
      indexClick.value = 0;
    }
  }


  showColor(index) {
    click.value = [];
    for (int i = 0; i < 2; i++) {
      click.add(false);
    }
    click[index] = true;
  }


  getListStudent(index){
    switch(index){
      case 0:
        return  Get.find<ListStudentSubmitResultExerciseController>().studentSubmitted.length;
      case 1:
        return Get.find<ListStudentUnSubmitResultExerciseController>().studentUnSubmitted.length;
    }
  }




}