import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/loading_custom.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/learning_management_teacher/view_transcript_synthetic_teacher/view_transcript_synthetic_teacher_controller.dart';
import '../../../../../../../commom/constants/date_format.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../commom/utils/date_time_picker.dart';
import '../../../../../../../commom/utils/time_utils.dart';
import '../../../../../../../data/model/res/transcript/transcript.dart';
import '../../../../../../../routes/app_pages.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';

class TranscriptsSyntheticTeacherPage extends GetWidget<TranscriptsSyntheticTeacherController>{
  @override
  final controller = Get.put(TranscriptsSyntheticTeacherController());

  TranscriptsSyntheticTeacherPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              Get.until((route) => route.settings.name == Routes.home);

            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Text(
          'Danh sách bảng điểm',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() => SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16.w),
          child: Column(
            children: [
              Container(
                height: 60.h,
                padding: EdgeInsets.symmetric(vertical: 14.h),
                color: Colors.white,
                child: Row(
                  children: [
                    Expanded(
                        child: TextFormField(
                          style: TextStyle(
                            fontSize: 12.0.sp,
                            color: const Color.fromRGBO(26, 26, 26, 1),
                          ),
                          onTap: () {
                            selectDateTimeStart(
                                controller.controllerDateStart.value.text,
                                DateTimeFormat.formatDateShort,
                                context);
                          },
                          cursorColor: ColorUtils.PRIMARY_COLOR,
                          controller: controller.controllerDateStart.value,
                          readOnly: true,
                          decoration: InputDecoration(
                            suffixIcon: Container(
                              margin: EdgeInsets.zero,
                              child: SvgPicture.asset(
                                "assets/images/icon_date_picker.svg",
                                fit: BoxFit.scaleDown,
                              ),
                            ),
                            labelText: "Từ ngày",
                            enabledBorder:const OutlineInputBorder(
                              borderSide: BorderSide(color:Color.fromRGBO(177, 177, 177, 1),),
                            ),

                            focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color:Color.fromRGBO(177, 177, 177, 1),),
                            ),
                            labelStyle: TextStyle(
                                color:
                                const Color.fromRGBO(177, 177, 177, 1),
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w500,
                                fontFamily:
                                'assets/font/static/Inter-Medium.ttf'),
                          ),
                        )),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 16.w),
                        child: TextFormField(
                          style: TextStyle(
                            fontSize: 12.0.sp,
                            color: const Color.fromRGBO(26, 26, 26, 1),
                          ),
                          onTap: () {
                            selectDateTimeEnd(
                                controller.controllerDateEnd.value.text,
                                DateTimeFormat.formatDateShort,
                                context);
                          },
                          cursorColor: ColorUtils.PRIMARY_COLOR,
                          controller:
                          controller.controllerDateEnd.value,
                          readOnly: true,
                          decoration: InputDecoration(
                            suffixIcon: Container(
                              margin: EdgeInsets.zero,
                              child: SvgPicture.asset(
                                "assets/images/icon_date_picker.svg",
                                fit: BoxFit.scaleDown,
                              ),
                            ),
                            focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color:Color.fromRGBO(177, 177, 177, 1),),
                            ),
                            labelText: "Đến ngày",
                            enabledBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color:Color.fromRGBO(177, 177, 177, 1),),
                            ),
                            labelStyle: TextStyle(
                                color: const Color.fromRGBO(
                                    177, 177, 177, 1),
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w500,
                                fontFamily:
                                'assets/font/static/Inter-Medium.ttf'),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16.w),
                height: 40,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                        color: const Color.fromRGBO(192, 192, 192, 1)),
                    borderRadius: BorderRadius.circular(8)),
                child: Theme(
                  data: Theme.of(context).copyWith(
                    canvasColor: Colors.white,
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                      isExpanded: true,
                      iconSize: 0,
                      icon: const Visibility(
                          visible: false,
                          child: Icon(Icons.arrow_downward)),
                      elevation: 16,
                      hint: controller.semesterName.value != ""
                          ? Row(
                        children: [
                          Text(
                            controller.semesterName.value,
                            style: TextStyle(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                color: ColorUtils.PRIMARY_COLOR),
                          ),
                          Expanded(child: Container()),
                          const Icon(
                            Icons.keyboard_arrow_down,
                            color: Colors.black,
                            size: 18,
                          )
                        ],
                      )
                          : Row(
                        children: [
                          Text(
                            'Kỳ học',
                            style: TextStyle(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                color: Colors.black54),
                          ),
                          Expanded(child: Container()),
                          const Icon(
                            Icons.keyboard_arrow_down,
                            color: Colors.black,
                            size: 18,
                          )
                        ],
                      ),
                      items: controller.listSemester.map(
                            (value) {
                          return DropdownMenuItem<SemesterTranscript>(
                            value: value,
                            child: Text(
                              value.name!,
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w400,
                                  color: ColorUtils.PRIMARY_COLOR),
                            ),
                          );
                        },
                      ).toList(),
                      onChanged: (SemesterTranscript? value) {
                        controller.semesterName.value = value!.name!;
                        controller.semesterId.value = value.id!;
                        controller.getListTranscript();
                      },
                    ),
                  ),),
              ),
              Padding(padding: EdgeInsets.only(top: 8.h)),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16.w),
                height: 40,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                        color: const Color.fromRGBO(192, 192, 192, 1)),
                    borderRadius: BorderRadius.circular(8)),
                child: Theme(
                  data: Theme.of(context).copyWith(
                    canvasColor: Colors.white,
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                      isExpanded: true,
                      iconSize: 0,
                      icon: const Visibility(
                          visible: false,
                          child: Icon(Icons.arrow_downward)),
                      elevation: 16,
                      hint: controller.isPublic.value != ""
                          ? Row(
                        children: [
                          Text(
                            controller.isPublic.value,
                            style: TextStyle(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                color: ColorUtils.PRIMARY_COLOR),
                          ),
                          Expanded(child: Container()),
                          const Icon(
                            Icons.keyboard_arrow_down,
                            color: Colors.black,
                            size: 18,
                          )
                        ],
                      )
                          : Row(
                        children: [
                          Text(
                            'Trạng thái công bố',
                            style: TextStyle(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                color: Colors.black54),
                          ),
                          Expanded(child: Container()),
                          const Icon(
                            Icons.keyboard_arrow_down,
                            color: Colors.black,
                            size: 18,
                          )
                        ],
                      ),
                      items: ["Tất cả","Đã công bố","Chưa công bố"].map(
                            (value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(
                              value,
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w400,
                                  color: ColorUtils.PRIMARY_COLOR),
                            ),
                          );
                        },
                      ).toList(),
                      onChanged: (String? value) {
                        controller.isPublic.value = value!;
                        controller.getListTranscript();
                      },
                    ),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 16.h)),
              controller.isReady.value
                  ?ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: controller.transcript.length,
                itemBuilder: (context, index) {
                  controller.getListItemPopupMenu(index);
                  return Obx(() => InkWell(
                    onTap: () {
                      checkClickFeature(Get.find<HomeController>().userGroupByApp,()=> controller.goToDetailTranscriptSynthetic(index),StringConstant.FEATURE_TRANSCRIPT_SYNTHETIC_DETAIL);
                    },
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 120.w,
                                      child: Text("Tên: ",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                                    ),
                                    SizedBox(
                                      width: 165.w,
                                      child: Text(controller.transcript[index].name!,style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                    )
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 8.h)),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 120.w,
                                      child: Text("Kỳ học: ",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                                    ),
                                    Text(controller.transcript[index].semester!.name!,style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 8.h)),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 120.w,
                                      child: Text("Trạng thái công bố",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                                    ),
                                    Container(
                                      padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                                      decoration: BoxDecoration(
                                          color: controller.getColorBackgroundIsPublic(controller.transcript[index].isPublic),
                                          borderRadius: BorderRadius.circular(6)
                                      ),
                                      child: Text(
                                        "${controller.getStatusPublic(controller.transcript[index].isPublic)}",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            color: controller.getColorTextIsPublic(controller.transcript[index].isPublic),
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 8.h)),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 120.w,
                                      child: Text("Người tạo: ",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                                    ),
                                    Text(controller.transcript[index].createdBy!.fullName!,style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 8.h)),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 120.w,
                                      child: Text("Thời gian tạo: ",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                                    ),
                                    Text(controller.outputDateFormatV2.format(
                                        DateTime.fromMillisecondsSinceEpoch(
                                            controller
                                                .transcript[index]
                                                .createdAt! )),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                  ],
                                ),

                              ],
                            ),
                            const Spacer(),
                           Visibility(
                             visible: controller.listItemPopupMenu.isNotEmpty,
                               child:  PopupMenuButton(
                               padding: EdgeInsets.zero,
                               shape: const RoundedRectangleBorder(
                                   borderRadius:
                                   BorderRadius.all(
                                       Radius.circular(
                                           6.0))),
                               icon: const Icon(
                                 Icons.more_vert_outlined,
                                 color: Color.fromRGBO(
                                     177, 177, 177, 1),
                               ),
                               itemBuilder: (context) {
                                 return  controller.getListItemPopupMenu(index);
                               },
                               onSelected: (value) {
                                 switch (value) {
                                   case 0:
                                     Get.dialog(Center(
                                       child: Wrap(
                                           children: [
                                             Container(
                                               width: double
                                                   .infinity,
                                               decoration: BoxDecoration(
                                                   color: Colors
                                                       .transparent,
                                                   borderRadius:
                                                   BorderRadius.circular(16)),
                                               child:
                                               Stack(
                                                 children: [
                                                   Container(
                                                     margin: EdgeInsets.only(
                                                         top: 40.h,
                                                         right: 50.w,
                                                         left: 50.w),
                                                     decoration: BoxDecoration(
                                                         color: Colors.white,
                                                         borderRadius: BorderRadius.circular(16)),
                                                     child:
                                                     Column(
                                                       crossAxisAlignment: CrossAxisAlignment.center,
                                                       children: [
                                                         Padding(padding: EdgeInsets.only(top: 32.h)),
                                                         Container(
                                                           margin: EdgeInsets.symmetric(horizontal: 16.w),
                                                           child: Text(
                                                             "Bạn có chắc muốn công bố bảng điểm này?",
                                                             style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontSize: 14.sp),
                                                           ),
                                                         ),
                                                         Container(
                                                             color: Colors.white,
                                                             padding: EdgeInsets.all(16.h),
                                                             child: SizedBox(
                                                               width: double.infinity,
                                                               height: 46,
                                                               child: ElevatedButton(
                                                                   style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                                                                   onPressed: () {
                                                                     controller.publicTranscript(controller.transcript[index].id);
                                                                   },
                                                                   child: Text(
                                                                     'Công bố',
                                                                     style: TextStyle(color: Colors.white, fontSize: 16.sp),
                                                                   )),
                                                             )),
                                                         TextButton(
                                                             onPressed: () {
                                                               Get.back();
                                                             },
                                                             child: Text(
                                                               "Hủy",
                                                               style: TextStyle(color: Colors.red, fontSize: 16.sp),
                                                             ))
                                                       ],
                                                     ),
                                                   ),
                                                   Container(
                                                       alignment:
                                                       Alignment.center,
                                                       margin: const EdgeInsets.only(top: 10),
                                                       height: 80,
                                                       child: Image.asset("assets/images/image_app_logo.png"))
                                                 ],
                                               ),
                                             ),
                                           ]),
                                     ));
                                     break;
                                   case 1:
                                     Get.toNamed(Routes.updateTranscriptPage,arguments: controller.transcript[index].id);
                                     break;
                                   case 2:
                                     Get.dialog(Center(
                                       child: Wrap(
                                           children: [
                                             Container(
                                               height:
                                               220.h,
                                               width: double
                                                   .infinity,
                                               decoration: BoxDecoration(
                                                   color: Colors
                                                       .transparent,
                                                   borderRadius:
                                                   BorderRadius.circular(16)),
                                               child:
                                               Stack(
                                                 children: [
                                                   Container(
                                                     margin: EdgeInsets.only(
                                                         top: 40.h,
                                                         right: 50.w,
                                                         left: 50.w),
                                                     height:
                                                     170.h,
                                                     decoration: BoxDecoration(
                                                         color: Colors.white,
                                                         borderRadius: BorderRadius.circular(16)),
                                                     child:
                                                     Column(
                                                       children: [
                                                         Padding(padding: EdgeInsets.only(top: 32.h)),
                                                         Text(
                                                           "Bạn có chắc muốn xóa bảng điểm này?",
                                                           style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontSize: 14.sp),
                                                         ),
                                                         Container(
                                                             color: Colors.white,
                                                             padding: EdgeInsets.all(16.h),
                                                             child: SizedBox(
                                                               width: double.infinity,
                                                               height: 46,
                                                               child: ElevatedButton(
                                                                   style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                                                                   onPressed: () {
                                                                     controller.deleteTranscript(controller.transcript[index].id);
                                                                     Get.back();
                                                                   },
                                                                   child: Text(
                                                                     'Xóa',
                                                                     style: TextStyle(color: Colors.white, fontSize: 16.sp),
                                                                   )),
                                                             )),
                                                         TextButton(
                                                             onPressed: () {
                                                               Get.back();
                                                             },
                                                             child: Text(
                                                               "Hủy",
                                                               style: TextStyle(color: Colors.red, fontSize: 16.sp),
                                                             ))
                                                       ],
                                                     ),
                                                   ),
                                                   Container(
                                                       alignment:
                                                       Alignment.center,
                                                       margin: const EdgeInsets.only(top: 10),
                                                       height: 80,
                                                       child: Image.asset("assets/images/image_app_logo.png"))
                                                 ],
                                               ),
                                             ),
                                           ]),
                                     ));
                                     break;
                                 }
                               }))
                          ],
                        ),
                        const Divider(),
                      ],
                    ),
                  ));
                },)
                  :const Center(
                heightFactor: 1.0,
                child: LoadingCustom(),
              )
            ],
          ),
        ),
      )),
    );
  }


  selectDateTimeStart(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy')
                .parse(controller.controllerDateEnd.value.text)
                .millisecondsSinceEpoch) {
          controller.controllerDateStart.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian Từ ngày nhỏ hơn thời gian Đến ngày");
        }
      } else {
        controller.controllerDateStart.value.text = date;
      }
      controller.getListTranscript();
    }
    
  }

  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy')
            .parse(controller.controllerDateStart.value.text)
            .millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch) {
          controller.controllerDateEnd.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian Đến ngày lớn hơn thời gian Từ ngày");
        }
      } else {
        controller.controllerDateEnd.value.text = date;
      }
    }
    controller.getListTranscript();
    
  }
}