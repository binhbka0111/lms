import 'package:get/get.dart';
import '../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/common/subject.dart';
import '../../../../../data/repository/subject/class_office_repo.dart';
import '../teacher_home_controller.dart';
import '../../../../../routes/app_pages.dart';
class SubjectTeacherController extends GetxController{
  RxList<SubjectRes> subjects = <SubjectRes>[].obs;
  final ClassOfficeRepo _subjectRepo = ClassOfficeRepo();
  var detailSubject = SubjectRes().obs;
  var isClick = false;
  RxBool isReady = false.obs;
  @override
  void onInit() async{
    await getListSubject();
    super.onInit();
  }


  getListSubject() async{
    subjects.clear();
    var classID = Get.find<TeacherHomeController>().currentClass.value.classId;
    await _subjectRepo.listSubjectTeacher(classID).then((value) {
      if (value.state == Status.SUCCESS) {
        subjects.value = value.object!;
      }
    });
    subjects.refresh();
    isReady.value = true;
  }


  checkHomeRoomTeacher() {
    if (Get.find<TeacherHomeController>().userProfile.value.classesOfHomeroomTeacher?.contains(Get.find<TeacherHomeController>().currentClass.value.classId) ==
        true) {
      return true;
    } else {
      return false;
    }
  }

  onClickSubject(index){
    detailSubject.value = subjects[index];
    isClick = true;
    clickSubjectTeacher();
    Get.toNamed(Routes.subjectLearningManagementTeacherPage);

  }


}