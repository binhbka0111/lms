import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/send_notification_teacher/send_notification_teacher_controller.dart';
import '../../../../../routes/app_pages.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';

class SendNotificationTeacherPage
    extends GetWidget<SendNotificationTeacherController> {
  @override
  final controller = Get.put(SendNotificationTeacherController());

  SendNotificationTeacherPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          appBar: AppBar(
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            elevation: 0,
            title: Text(
              "Gửi Thông Báo",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w500),
            ),
            actions: [
              InkWell(
                onTap: () {
                  comeToHome();
                },
                child: const Icon(
                  Icons.home,
                  color: Colors.white,
                ),
              ),
              Padding(padding: EdgeInsets.only(right: 16.w))
            ],
          ),
          body: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.all(16),
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      controller.isExpandParents.value =
                      !controller.isExpandParents.value;
                    },
                    child: Card(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Container(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Text(
                              "Cho Phụ Huynh",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontWeight: FontWeight.w500),
                            ),
                            Expanded(child: Container()),
                            Visibility(
                              visible: controller.isExpandParents.value == false,
                              child: const Icon(
                                Icons.arrow_forward_ios,
                                color: ColorUtils.PRIMARY_COLOR,
                                size: 18,
                              ),
                            ),
                            Visibility(
                              visible: controller.isExpandParents.value == true,
                              child: const Icon(Icons.keyboard_arrow_up,
                                  color: ColorUtils.PRIMARY_COLOR, size: 24,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                      visible: controller.isExpandParents.value == true,
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        child: Column(
                          children: [
                            Visibility(
                                visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_SEND_NOTIFY_SMS),
                                child: InkWell(
                              onTap: () {
                                controller.clickSMSParent.value = true;
                                controller.clickNotifyAppParents.value = false;
                                Get.toNamed(Routes.listClasBySendNotificationPage,arguments: ["PARENT","SMS"]);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: controller.clickSMSParent.value == true
                                        ? ColorUtils.PRIMARY_COLOR
                                        : const Color.fromRGBO(239, 239, 239, 1),
                                    borderRadius: BorderRadius.circular(12)),
                                padding: EdgeInsets.only(
                                    top: 8.h,
                                    left: 16.w,
                                    bottom: 8.h,
                                    right: 16.w),
                                child: Row(
                                  children: [
                                    Image.asset(
                                      "assets/images/icon_sms.png",
                                      height: 28.h,
                                      width: 28.h,
                                      color: controller.clickSMSParent.value == true
                                          ? Colors.white
                                          : const Color.fromRGBO(90, 90, 90, 1),
                                    ),
                                    Text(
                                      "Gửi Thông Báo Qua SMS",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14.sp,
                                          color: controller.clickSMSParent.value == true
                                              ? Colors.white
                                              : const Color.fromRGBO(90, 90, 90, 1)),
                                    )
                                  ],
                                ),
                              ),
                            )),
                            Padding(padding: EdgeInsets.only(top: 8.h)),
                            Visibility(
                                visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_SEND_NOTIFY_IN_APP),
                                child: InkWell(
                              onTap: () {
                                controller.clickSMSParent.value = false;
                                controller.clickNotifyAppParents.value = true;
                                Get.toNamed(Routes.listClasBySendNotificationPage,arguments: ["PARENT","INAPP"]);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: controller.clickNotifyAppParents.value == true
                                        ? ColorUtils.PRIMARY_COLOR
                                        : const Color.fromRGBO(239, 239, 239, 1),
                                    borderRadius: BorderRadius.circular(12)),
                                padding: EdgeInsets.only(
                                    top: 8.h,
                                    left: 16.w,
                                    bottom: 8.h,
                                    right: 16.w),
                                child: Row(
                                  children: [
                                    Image.asset(
                                      height: 28.h,
                                      width: 28.h,
                                      "assets/images/icon_send_notify_in_app.png",
                                      color:
                                      controller.clickNotifyAppParents.value == true
                                          ? Colors.white
                                          : const Color.fromRGBO(90, 90, 90, 1),
                                    ),
                                    Text(
                                      "Gửi Thông Báo Trên Ứng Dụng",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14.sp,
                                          color: controller
                                              .clickNotifyAppParents.value ==
                                              true
                                              ? Colors.white
                                              : const Color.fromRGBO(90, 90, 90, 1)),
                                    )
                                  ],
                                ),
                              ),
                            )),
                          ],
                        ),
                      )),
                  Padding(padding: EdgeInsets.only(top: 8.h)),
                  InkWell(
                    onTap: () {
                      controller.isExpandStudent.value =
                      !controller.isExpandStudent.value;
                    },

                    child: Card(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Container(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Text(
                              "Cho Học sinh",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontWeight: FontWeight.w500),
                            ),
                            Expanded(child: Container()),
                            Visibility(
                              visible: controller.isExpandStudent.value == false,
                              child: const Icon(
                                Icons.arrow_forward_ios,
                                color: ColorUtils.PRIMARY_COLOR,
                                size: 18,
                              ),
                            ),
                            Visibility(
                              visible: controller.isExpandStudent.value == true,
                              child: const Icon(Icons.keyboard_arrow_up,
                                color: ColorUtils.PRIMARY_COLOR, size: 24,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                      visible: controller.isExpandStudent.value == true,
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        child: Column(
                          children: [
                            Visibility(
                                visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_SEND_NOTIFY_SMS),
                                child: InkWell(
                              onTap: () {
                                controller.clickSMSStudent.value = true;
                                controller.clickNotifyAppStudent.value = false;
                                Get.toNamed(Routes.listClasBySendNotificationPage,arguments: ["STUDENT","SMS"]);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: controller.clickSMSStudent.value == true
                                        ? ColorUtils.PRIMARY_COLOR
                                        : const Color.fromRGBO(239, 239, 239, 1),
                                    borderRadius: BorderRadius.circular(12)),
                                padding: EdgeInsets.only(
                                    top: 8.h,
                                    left: 16.w,
                                    bottom: 8.h,
                                    right: 16.w),
                                child: Row(
                                  children: [
                                    Image.asset(
                                      height: 28.h,
                                      width: 28.h,
                                      "assets/images/icon_sms.png",
                                      color: controller.clickSMSStudent.value == true
                                          ? Colors.white
                                          : const Color.fromRGBO(90, 90, 90, 1),
                                    ),
                                    Text(
                                      "Gửi Thông Báo Qua SMS",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14.sp,
                                          color: controller.clickSMSStudent.value == true
                                              ? Colors.white
                                              : const Color.fromRGBO(90, 90, 90, 1)),
                                    )
                                  ],
                                ),
                              ),
                            )),
                            Padding(padding: EdgeInsets.only(top: 8.h)),
                            Visibility(
                                visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_SEND_NOTIFY_IN_APP),
                                child: InkWell(
                              onTap: () {
                                controller.clickSMSStudent.value = false;
                                controller.clickNotifyAppStudent.value = true;
                                Get.toNamed(Routes.listClasBySendNotificationPage,arguments: ["STUDENT","INAPP"]);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: controller.clickNotifyAppStudent.value == true
                                        ? ColorUtils.PRIMARY_COLOR
                                        : const Color.fromRGBO(239, 239, 239, 1),
                                    borderRadius: BorderRadius.circular(12)),
                                padding: EdgeInsets.only(
                                    top: 8.h,
                                    left: 16.w,
                                    bottom: 8.h,
                                    right: 16.w),
                                child: Row(
                                  children: [
                                    Image.asset(
                                      height: 28.h,
                                      width: 28.h,
                                      "assets/images/icon_send_notify_in_app.png",
                                      color:
                                      controller.clickNotifyAppStudent.value == true
                                          ? Colors.white
                                          : const Color.fromRGBO(90, 90, 90, 1),
                                    ),
                                    Text(
                                      "Gửi Thông Báo Trên Ứng Dụng",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14.sp,
                                          color: controller
                                              .clickNotifyAppStudent.value ==
                                              true
                                              ? Colors.white
                                              : const Color.fromRGBO(90, 90, 90, 1)),
                                    )
                                  ],
                                ),
                              ),
                            )),
                          ],
                        ),
                      )),
                  Padding(padding: EdgeInsets.only(top: 8.h)),
                  Visibility(
                    visible: AppCache().userType == "MANAGER",
                      child: InkWell(
                    onTap: () {
                      controller.isExpandTeacher.value =
                      !controller.isExpandTeacher.value;
                    },
                    child: Card(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Container(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Text(
                              "Cho Giáo Viên",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontWeight: FontWeight.w500),
                            ),
                            Expanded(child: Container()),
                            Visibility(
                              visible: controller.isExpandTeacher.value == false,
                              child: const Icon(
                                Icons.arrow_forward_ios,
                                color: ColorUtils.PRIMARY_COLOR,
                                size: 18,
                              ),
                            ),
                            Visibility(
                              visible: controller.isExpandTeacher.value == true,
                              child: const Icon(Icons.keyboard_arrow_up,
                                color: ColorUtils.PRIMARY_COLOR, size: 24,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )),
                  Visibility(
                      visible: controller.isExpandTeacher.value == true,
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        child: Column(
                          children: [
                            Visibility(
                                visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,
                                    StringConstant.FEATURE_SEND_NOTIFY_SMS),
                                child: InkWell(
                                  onTap: () {
                                    controller.clickSMSTeacher.value = true;
                                    controller.clickNotifyAppTeacher.value = false;
                                    Get.toNamed(Routes.listClasBySendNotificationPage,arguments: ["TEACHER","SMS"]);
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: controller.clickSMSTeacher.value == true
                                            ? ColorUtils.PRIMARY_COLOR
                                            : const Color.fromRGBO(239, 239, 239, 1),
                                        borderRadius: BorderRadius.circular(12)),
                                    padding: EdgeInsets.only(
                                        top: 8.h,
                                        left: 16.w,
                                        bottom: 8.h,
                                        right: 16.w),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          height: 28.h,
                                          width: 28.w,
                                          "assets/images/icon_sms.png",
                                          color: controller.clickSMSTeacher.value == true
                                              ? Colors.white
                                              : const Color.fromRGBO(90, 90, 90, 1),
                                        ),
                                        Text(
                                          "Gửi Thông Báo Qua SMS",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14.sp,
                                              color: controller.clickSMSTeacher.value == true
                                                  ? Colors.white
                                                  : const Color.fromRGBO(90, 90, 90, 1)),
                                        )
                                      ],
                                    ),
                                  ),
                                )),
                            Padding(padding: EdgeInsets.only(top: 8.h)),
                            Visibility(
                                visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,
                                    StringConstant.FEATURE_SEND_NOTIFY_IN_APP),
                                child: InkWell(
                                  onTap: () {
                                    controller.clickSMSTeacher.value = false;
                                    controller.clickNotifyAppTeacher.value = true;
                                    Get.toNamed(Routes.listClasBySendNotificationPage,arguments: ["TEACHER","INAPP"]);
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: controller.clickNotifyAppTeacher.value == true
                                            ? ColorUtils.PRIMARY_COLOR
                                            : const Color.fromRGBO(239, 239, 239, 1),
                                        borderRadius: BorderRadius.circular(12)),
                                    padding: EdgeInsets.only(
                                        top: 8.h,
                                        left: 16.w,
                                        bottom: 8.h,
                                        right: 16.w),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          height: 28.h,
                                          width: 28.w,
                                          "assets/images/icon_send_notify_in_app.png",
                                          color:
                                          controller.clickNotifyAppTeacher.value == true
                                              ? Colors.white
                                              : const Color.fromRGBO(90, 90, 90, 1),
                                        ),
                                        Text(
                                          "Gửi Thông Báo Trên Ứng Dụng",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14.sp,
                                              color: controller
                                                  .clickNotifyAppTeacher.value ==
                                                  true
                                                  ? Colors.white
                                                  : const Color.fromRGBO(90, 90, 90, 1)),
                                        )
                                      ],
                                    ),
                                  ),
                                )),
                          ],
                        ),
                      )),
                  Visibility(
                      visible: AppCache().userType == "MANAGER",
                      child: Padding(padding: EdgeInsets.only(top: 8.h))),
                  InkWell(
                    onTap: () {
                      controller.isExpandAll.value =
                      !controller.isExpandAll.value;
                    },
                    child: Card(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Container(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Text(
                              AppCache().userType == "TEACHER"?"Cho Phụ Huynh & Học Sinh":"Giáo viên,Phụ Huynh & Học Sinh",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontWeight: FontWeight.w500),
                            ),
                            Expanded(child: Container()),
                            Visibility(
                              visible: controller.isExpandAll.value == false,
                              child: const Icon(
                                Icons.arrow_forward_ios,
                                color: ColorUtils.PRIMARY_COLOR,
                                size: 18,
                              ),
                            ),
                            Visibility(
                              visible: controller.isExpandAll.value == true,
                              child: const Icon(Icons.keyboard_arrow_up,
                                color: ColorUtils.PRIMARY_COLOR, size: 24,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                      visible: controller.isExpandAll.value == true,
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        child: Column(
                          children: [
                            Visibility(
                              visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_SEND_NOTIFY_SMS),
                              child: InkWell(
                              onTap: () {
                                controller.clickSMSAll.value = true;
                                controller.clickNotifyAppAll.value = false;
                                Get.toNamed(Routes.listClasBySendNotificationPage,arguments: ["ALL","SMS"]);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: controller.clickSMSAll.value == true
                                        ? ColorUtils.PRIMARY_COLOR
                                        : const Color.fromRGBO(239, 239, 239, 1),
                                    borderRadius: BorderRadius.circular(12)),
                                padding: EdgeInsets.only(
                                    top: 8.h,
                                    left: 16.w,
                                    bottom: 8.h,
                                    right: 16.w),
                                child: Row(
                                  children: [
                                    Image.asset(
                                      height: 28.h,
                                      width: 28.h,
                                      "assets/images/icon_sms.png",
                                      color: controller.clickSMSAll.value == true
                                          ? Colors.white
                                          : const Color.fromRGBO(90, 90, 90, 1),
                                    ),
                                    Text(
                                      "Gửi Thông Báo Qua SMS",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14.sp,
                                          color: controller.clickSMSAll.value == true
                                              ? Colors.white
                                              : const Color.fromRGBO(90, 90, 90, 1)),
                                    )
                                  ],
                                ),
                              ),
                            ),),
                            Padding(padding: EdgeInsets.only(top: 8.h)),
                           Visibility(
                             visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_SEND_NOTIFY_IN_APP),
                             child:  InkWell(
                             onTap: () {
                               controller.clickSMSAll.value = false;
                               controller.clickNotifyAppAll.value = true;
                               Get.toNamed(Routes.listClasBySendNotificationPage,arguments: ["ALL","INAPP"]);
                             },
                             child: Container(
                               decoration: BoxDecoration(
                                   color: controller.clickNotifyAppAll.value == true
                                       ? ColorUtils.PRIMARY_COLOR
                                       : const Color.fromRGBO(239, 239, 239, 1),
                                   borderRadius: BorderRadius.circular(12)),
                               padding: EdgeInsets.only(
                                   top: 8.h,
                                   left: 16.w,
                                   bottom: 8.h,
                                   right: 16.w),
                               child: Row(
                                 children: [
                                   Image.asset(
                                     height: 28.h,
                                     width: 28.h,
                                     "assets/images/icon_send_notify_in_app.png",
                                     color:
                                     controller.clickNotifyAppAll.value == true
                                         ? Colors.white
                                         : const Color.fromRGBO(90, 90, 90, 1),
                                   ),
                                   Text(
                                     "Gửi Thông Báo Trên Ứng Dụng",
                                     style: TextStyle(
                                         fontWeight: FontWeight.w400,
                                         fontSize: 14.sp,
                                         color: controller
                                             .clickNotifyAppAll.value ==
                                             true
                                             ? Colors.white
                                             : const Color.fromRGBO(90, 90, 90, 1)),
                                   )
                                 ],
                               ),
                             ),
                           ),)
                          ],
                        ),
                      )),
                ],
              ),
            ),
          ),
        ));
  }
}
