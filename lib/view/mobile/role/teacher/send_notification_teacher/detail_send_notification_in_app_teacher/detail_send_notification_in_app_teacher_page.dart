import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/file_device.dart';
import '../../../../../../../../commom/utils/global.dart';
import '../../../../../../../../commom/widget/file_widget.dart';
import '../../../../../../../../commom/widget/text_field_custom.dart';
import '../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../data/model/common/req_file.dart';
import 'detail_send_notification_in_app_teacher_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';


class DetailSendInAppNotification
    extends GetWidget<DetailSendNotificationInAppController> {
  @override
  final controller = Get.put(DetailSendNotificationInAppController());

  DetailSendInAppNotification({super.key});
  bottomSheetAddUser() {
    return GetBuilder<DetailSendNotificationInAppController>(builder: (controller) {
      return  Wrap(children: [Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(32.r),
              topLeft: Radius.circular(32.r)),
          color: Colors.white,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.r),
                  color: const Color.fromRGBO(210, 212, 216, 1)),
              height: 6.h,
              width: 48.w,
              alignment: Alignment.topCenter,
              margin: EdgeInsets.only(top: 16.h),
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Visibility(
                visible: controller.listUserNotSendNotify.isNotEmpty,
                child: Container(
                  margin: EdgeInsets.only(left: 8.w),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {
                          clickCheckboxAllAdd();
                        },
                        child: Row(
                          children: [
                            Container(
                                alignment: Alignment.center,
                                decoration: ShapeDecoration(
                                  shape: CircleBorder(
                                      side: BorderSide(
                                          color:
                                          controller.isCheckAllAdd.value
                                              ? const Color.fromRGBO(
                                              248, 129, 37, 1)
                                              : const Color.fromRGBO(
                                              235, 235, 235, 1))),
                                ),
                                child: controller.isCheckAllAdd.value
                                    ?  Container(
                                    margin: const EdgeInsets.all(4),
                                    child: Icon(
                                      Icons.circle,
                                      size: ScreenUtil().setSp(16),
                                      color: ColorUtils.PRIMARY_COLOR,
                                    ))
                                    : Container(
                                    margin: const EdgeInsets.all(4),
                                    child: Icon(
                                      Icons.circle,
                                      size: ScreenUtil().setSp(16),
                                      color: Colors.transparent,
                                    ))),
                            Padding(padding: EdgeInsets.only(left: 8.w)),
                            Text(
                              "Chọn hết",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black),
                            ),
                          ],
                        ),
                      ),
                      Expanded(child: Container()),
                      TextButton(
                          onPressed: () {
                            Get.back();
                            cancelAddUser();
                          },
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white),
                          child: Text(
                            "Hủy",
                            style: TextStyle(
                                fontSize: 16.sp,
                                color: const Color.fromRGBO(123, 123, 123, 1)),
                          )),
                      TextButton(
                          onPressed: () {
                            addListUser();
                          },
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white),
                          child:  Text(
                            "Xong",
                            style: TextStyle(
                              fontSize: 16.sp,
                                color: ColorUtils.PRIMARY_COLOR),
                          )),
                    ],
                  ),
                )),
            SingleChildScrollView(
              child: Container(
                height: 300.h,
                margin: EdgeInsets.symmetric( horizontal: 8.w),
                child:
                GetBuilder<DetailSendNotificationInAppController>(
                  builder: (controller) {
                    return ListView.builder(
                        itemCount:
                        controller.listUserNotSendNotify.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              clickCheckboxAdd(index);
                            },
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                        alignment: Alignment.center,
                                        decoration: ShapeDecoration(
                                          shape: CircleBorder(
                                              side: BorderSide(
                                                  color: controller
                                                      .listCheckBoxAdd[index]
                                                      ? ColorUtils.PRIMARY_COLOR
                                                      : const Color.fromRGBO(
                                                      235, 235, 235, 1))),
                                        ),
                                        child: controller.listCheckBoxAdd[index]
                                            ?  Container(
                                            margin: const EdgeInsets.all(4),
                                            child: Icon(
                                              Icons.circle,
                                              size: ScreenUtil().setSp(16),
                                              color: ColorUtils.PRIMARY_COLOR,
                                            ))
                                            : Container(
                                            margin: const EdgeInsets.all(4),
                                            child: Icon(
                                              Icons.circle,
                                              size: ScreenUtil().setSp(16),
                                              color: Colors.transparent,
                                            ))),
                                    Padding(
                                        padding: EdgeInsets.only(left: 8.w)),
                                    SizedBox(
                                      width: 32.w,
                                      height: 32.w,
                                      child:
                                      CacheNetWorkCustom(urlImage:  '${controller.listUserNotSendNotify
                                      [index].image }',),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(left: 8.w)),
                                    Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${controller.listUserNotSendNotify[index].fullName}",
                                          style: TextStyle(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.black),
                                        ),
                                        SizedBox(
                                          width: 250.w,
                                          child: getRole(
                                              controller
                                                  .listUserNotSendNotify,
                                              controller.getListAddRole(
                                                  controller
                                                      .listUserNotSendNotify[index]
                                                      .type,
                                                  index),
                                              controller.listUserNotSendNotify[index].type,
                                              index),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                                const Divider(),
                              ],
                            ),
                          );
                        });
                  },
                ),
              ),
            ),
          ],
        ),
      )],);
    },);
  }

  bottomSheetMinusUser() {
    return GetBuilder<DetailSendNotificationInAppController>(builder: (controller) {
      return Wrap(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(32.r),
                  topLeft: Radius.circular(32.r)),
              color: Colors.white,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.r),
                      color: const Color.fromRGBO(210, 212, 216, 1)),
                  height: 6.h,
                  width: 48.w,
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.only(top: 16.h),
                ),
                Padding(padding: EdgeInsets.only(top: 16.h)),
                Visibility(
                    visible: controller.listUser.isNotEmpty,
                    child: Container(
                      margin: EdgeInsets.only(left: 8.w),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              clickCheckboxAllMinus();
                            },
                            child: Row(
                              children: [
                                Container(
                                    alignment: Alignment.center,
                                    decoration: ShapeDecoration(
                                      shape: CircleBorder(
                                          side: BorderSide(
                                              color: controller
                                                  .isCheckAllMinus.value
                                                  ? ColorUtils.PRIMARY_COLOR
                                                  : const Color.fromRGBO(
                                                  235, 235, 235, 1))),
                                    ),
                                    child: controller.isCheckAllMinus.value
                                        ?  Container(
                                        margin: const EdgeInsets.all(4),
                                        child: Icon(
                                          Icons.circle,
                                          size: ScreenUtil().setSp(16),
                                          color: ColorUtils.PRIMARY_COLOR,
                                        ))
                                        : Container(
                                        margin: const EdgeInsets.all(4),
                                        child: Icon(
                                          Icons.circle,
                                          size: ScreenUtil().setSp(16),
                                          color: Colors.transparent,
                                        ))),
                                Padding(padding: EdgeInsets.only(left: 8.w)),
                                Text(
                                  "Chọn hết",
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black),
                                ),
                              ],
                            ),
                          ),
                          Expanded(child: Container()),
                          TextButton(
                              onPressed: () {
                                Get.back();
                                cancelMinusUser();
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.white),
                              child:  Text(
                                "Hủy",
                                style: TextStyle(
                                    color: const Color.fromRGBO(123, 123, 123, 1),
                                    fontSize: 16.sp
                                ),
                              )),
                          TextButton(
                              onPressed: () {
                                minusListUser();

                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.white),
                              child: Text(
                                "Xong",
                                style: TextStyle(
                                    color: ColorUtils.PRIMARY_COLOR,
                                    fontSize: 16.sp
                                ),
                              )),
                        ],
                      ),
                    )),
                SingleChildScrollView(
                  child: Container(
                    height: 300.h,
                    margin: EdgeInsets.symmetric( horizontal: 8.w),
                    child:
                    GetBuilder<DetailSendNotificationInAppController>(
                      builder: (controller) {
                        return ListView.builder(
                            itemCount: controller.listUser.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  clickCheckboxMinus(index);
                                },
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                            alignment: Alignment.center,
                                            decoration: ShapeDecoration(
                                              shape: CircleBorder(
                                                  side: BorderSide(
                                                      color: controller
                                                          .listCheckBoxMinus[index]
                                                          ? ColorUtils.PRIMARY_COLOR
                                                          : const Color.fromRGBO(
                                                          235, 235, 235, 1))),
                                            ),
                                            child: controller.listCheckBoxMinus[index]
                                                ?  Container(
                                                margin: const EdgeInsets.all(4),
                                                child: Icon(
                                                  Icons.circle,
                                                  size: ScreenUtil().setSp(16),
                                                  color: ColorUtils.PRIMARY_COLOR,
                                                ))
                                                : Container(
                                                margin: const EdgeInsets.all(4),
                                                child: Icon(
                                                  Icons.circle,
                                                  size: ScreenUtil().setSp(16),
                                                  color: Colors.transparent,
                                                ))),
                                        Padding(
                                            padding: EdgeInsets.only(left: 8.w)),
                                        SizedBox(
                                            width: 32.w,
                                            height: 32.w,
                                            child:

                                            CacheNetWorkCustom(urlImage: '${ controller.listUser[index].image}',)

                                        ),
                                        Padding(
                                            padding: EdgeInsets.only(left: 8.w)),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "${controller.listUser[index].fullName}",
                                              style: TextStyle(
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.w500,
                                                  color: Colors.black),
                                            ),
                                            SizedBox(
                                              width: 250.w,
                                              child: getRole(
                                                  controller.listUser,
                                                  controller.getListMinusRole(
                                                      controller.listUser
                                                      [index].type,
                                                      index),
                                                  controller
                                                      .listUser[index].type,
                                                  index),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                    const Divider(),
                                  ],
                                ),
                              );
                            });
                      },
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      );
    },);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        dismissKeyboard();
        cancelAddUser();
        cancelMinusUser();
      },
      child: Obx(() => SafeArea(
          child: Scaffold(
              appBar: AppBar(
                backgroundColor: ColorUtils.PRIMARY_COLOR,
                elevation: 0,
                title: Text(
                  "Gửi Thông Báo Trên Ứng Dụng",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500),
                ),
                actions: [
                  InkWell(
                    onTap: () {
                      comeToHome();
                    },
                    child: const Icon(
                      Icons.home,
                      color: Colors.white,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(right: 16.w))
                ],
              ),
              body: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(padding: EdgeInsets.only(top: 16.h)),
                              Container(
                                margin: EdgeInsets.only(left: 16.w),
                                child: Text(
                                  "Thông Báo",
                                  style: TextStyle(
                                      color: const Color.fromRGBO(177, 177, 177, 1),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 12.w),
                                child: MyOutlineBorderTextFormFieldNhat(
                                  enable: true,
                                  focusNode: controller.focusTitle.value,
                                  iconPrefix: "",
                                  iconSuffix: "",
                                  state: StateType.DEFAULT,
                                  labelText: "Tiêu Đề (*)",
                                  autofocus: false,
                                  controller: controller.controllerTitle.value,
                                  helperText: "",
                                  showHelperText: false,
                                  ishowIconPrefix: false,
                                  textInputAction: TextInputAction.next,
                                  keyboardType: TextInputType.text,
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 12.w),
                                child: MyOutlineBorderTextFormFieldNhat(
                                  enable: true,
                                  focusNode: controller.focusTypeNotification.value,
                                  iconPrefix: "",
                                  iconSuffix: "",
                                  state: StateType.DEFAULT,
                                  labelText: "Loại thông báo (*)",
                                  autofocus: false,
                                  controller:
                                  controller.controllerTypeNotification.value,
                                  helperText: "",
                                  showHelperText: false,
                                  ishowIconPrefix: false,
                                  textInputAction: TextInputAction.next,
                                  keyboardType: TextInputType.text,
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 12.w),
                                child: MyOutlineBorderTextFormFieldNhat(
                                  enable: true,
                                  focusNode:
                                  controller.focusContentNotification.value,
                                  iconPrefix: null,
                                  iconSuffix: "assets/images/icon_resize.svg",
                                  state: StateType.DEFAULT,
                                  labelText: "Nội Dung Thông Báo",
                                  autofocus: false,
                                  controller: controller
                                      .controllerContentNotification.value,
                                  helperText: "",
                                  showHelperText: false,
                                  textInputAction: TextInputAction.next,
                                  ishowIconPrefix: false,
                                  keyboardType: TextInputType.text,
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(top: 16.h)),
                              InkWell(
                                onTap: () {
                                  FileDevice.showSelectFileV2(Get.context!,mutilpleImage: true)
                                      .then((value) {
                                    if (value.isNotEmpty) {

                                      controller.uploadFile(value);
                                    }
                                  });
                                },
                                child: Container(
                                  margin: EdgeInsets.symmetric(horizontal: 16.w),
                                  child: DottedBorder(
                                      dashPattern: const [5, 5],
                                      radius: Radius.circular(6.r),
                                      borderType: BorderType.RRect,
                                      color: const Color.fromRGBO(192, 192, 192, 1),
                                      child: SizedBox(
                                        width: double.infinity,
                                        child: controller.files.isNotEmpty
                                            ? Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            getFileWidgets(
                                                controller.files),
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    top: 8.h)),
                                            TextButton(
                                                onPressed: () {
                                                  FileDevice.showSelectFileV2(
                                                      Get.context!,mutilpleImage: true)
                                                      .then((value) {
                                                    if (value.isNotEmpty) {
                                                      controller.uploadFile(value);
                                                    }
                                                  });
                                                },
                                                child: Text(
                                                  "Thêm tệp",
                                                  style: TextStyle(
                                                      color: const Color
                                                          .fromRGBO(
                                                          72, 98, 141, 1),
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                      FontWeight.w400),
                                                )),
                                          ],
                                        )
                                            : Column(
                                          children: [
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    top: 16.h)),
                                            const Icon(
                                              Icons
                                                  .drive_folder_upload_rounded,
                                              color: ColorUtils.PRIMARY_COLOR,
                                              size: 40,
                                            ),
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    top: 4.h)),
                                            Text(
                                              "Tải lên Ảnh / Video",
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontSize: 14.sp,
                                                  fontWeight:
                                                  FontWeight.w500),
                                            ),
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    top: 12.h)),
                                            Text(
                                              "File (Video, Ảnh, Zip,...) có dung lượng không quá 10Mb",
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontSize: 12.sp,
                                                  fontWeight:
                                                  FontWeight.w400),
                                            ),
                                            Padding(
                                                padding: EdgeInsets.only(
                                                    bottom: 16.h)),
                                          ],
                                        ),
                                      )),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 16.w),
                                child: Row(
                                  children: [
                                    Text(
                                      "Danh Sách Người Nhận",
                                      style: TextStyle(
                                          color: const Color.fromRGBO(
                                              177, 177, 177, 1),
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Expanded(child: Container()),
                                    InkWell(
                                      onTap: () {
                                        controller.addListCheckBox();
                                        if (controller.listCheckBoxMinus
                                            .contains(false) ==
                                            true) {
                                          controller.isCheckAllMinus.value = false;
                                        } else {
                                          controller.isCheckAllMinus.value = true;
                                        }
                                        Get.bottomSheet(bottomSheetMinusUser());
                                      },
                                      child: Card(
                                        shape: const CircleBorder(),
                                        elevation: 1,
                                        child: Container(
                                          padding: EdgeInsets.only(bottom: 12.h),
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white,
                                          ),
                                          child: Icon(
                                            Icons.minimize,
                                            size: 20.h,
                                            color: ColorUtils.PRIMARY_COLOR,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 4.w,),
                                    InkWell(
                                      onTap: () {
                                        controller.isCheckAllAdd.value = false;
                                        controller.addListCheckBox();
                                        Get.bottomSheet(bottomSheetAddUser());
                                      },
                                      child: Card(
                                        shape: const CircleBorder(),
                                        elevation: 1,
                                        child: Container(
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white,
                                          ),
                                          child: Icon(
                                            Icons.add_rounded,
                                            size: 20.h,
                                            color: ColorUtils.PRIMARY_COLOR,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 16.w),
                                child: ListView.builder(
                                    itemCount: controller.listUser.length,
                                    shrinkWrap: true,
                                    physics: const ScrollPhysics(),
                                    itemBuilder: (context, index) {
                                      controller.student.value =
                                      controller.listUser[index].student!;
                                      controller.parent.value =
                                      controller.listUser[index].parent!;
                                      controller.typeInList.value =
                                      controller.listUser[index].type!;
                                      return InkWell(
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Padding(
                                                    padding:
                                                    EdgeInsets.only(left: 8.w)),
                                                SizedBox(
                                                    width: 32.w,
                                                    height: 32.w,
                                                    child:
                                                    CacheNetWorkCustom(urlImage: '${  controller.listUser[index].image}',)

                                                ),
                                                Padding(
                                                    padding:
                                                    EdgeInsets.only(left: 8.w)),
                                                Column(
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      "${controller.listUser[index].fullName}",
                                                      style: TextStyle(
                                                          fontSize: 16.sp,
                                                          fontWeight:
                                                          FontWeight.w500,
                                                          color: Colors.black),
                                                    ),
                                                    SizedBox(
                                                      width: 230.w,
                                                      child: getRole(
                                                          controller.listUser,
                                                          controller.getListAllRole(
                                                              controller
                                                                  .listUser
                                                              [index]
                                                                  .type,
                                                              index),
                                                          controller.listUser
                                                          [index].type,
                                                          index),
                                                    )
                                                  ],
                                                )
                                              ],
                                            ),
                                            const Divider(),
                                          ],
                                        ),
                                      );
                                    }),
                              )
                            ],
                          ),
                        )),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 16.w),
                      height: 36.h,
                      width: double.infinity,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: ColorUtils.PRIMARY_COLOR,
                          ),
                          onPressed: () {
                            if (controller.controllerTitle.value.text
                                .trim() ==
                                "") {
                              AppUtils.shared
                                  .showToast("Vui lòng nhập tiêu đề");
                            } else {
                              if (controller
                                  .controllerTypeNotification.value.text
                                  .trim() ==
                                  "") {
                                AppUtils.shared.showToast(
                                    "Vui lòng nhập loại thông báo");
                              } else {
                                if (controller.listUser.isEmpty) {
                                  AppUtils.shared.showToast(
                                      "Vui lòng chọn người để gửi thông báo");
                                } else {
                                  var listIdUser = <String>[];

                                  for (int i = 0;
                                  i < controller.listUser.length;
                                  i++) {
                                    listIdUser.add(
                                        controller.listUser[i].id!);
                                  }

                                  controller.sendNotification(
                                      controller.controllerTitle.value.text
                                          .trim(),
                                      controller.controllerContentNotification
                                          .value.text
                                          .trim(),
                                      controller.controllerTypeNotification
                                          .value.text
                                          .trim(),
                                      controller.classId.value,
                                      listIdUser,
                                      controller
                                          .filesUploadNotification);
                                }
                              }
                            }
                          },
                          child:  Text(
                            'Gửi',
                            style: TextStyle(
                                color: const Color.fromRGBO(255, 255, 255, 1),
                                fontSize: 16.sp),
                          )),
                    ),
                    SizedBox(height: 16.h,)
                  ],
                ),
              )))),
    );
  }

  RichText getRole(List<ItemContact> list, List<dynamic> list1, role, index) {
    return RichText(
      textAlign: TextAlign.start,
      text: TextSpan(
          text: "${controller.getTitle(role)}",
          style: TextStyle(
              color: const Color.fromRGBO(173, 173, 173, 1),
              fontSize: 14.sp,
              fontWeight: FontWeight.w500,
              fontFamily: 'static/Inter-Medium.ttf'),
          children: [
            TextSpan(
              text:
                  "${controller.getTextHomeRoomTeacher(list, list[index].positionName, index)}",
              style: TextStyle(
                  color: const Color.fromRGBO(26, 26, 26, 1),
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf'),
              children: list1.map((e) {
                var index = list1.indexOf(e);
                var showSplit = ", ";
                if (index == list1.length - 1) {
                  showSplit = "";
                }
                if (controller.typeInList.value == "TEACHER") {
                  return TextSpan(
                      text: "Môn ${e.fullName}$showSplit",
                      style: TextStyle(
                          color: const Color.fromRGBO(26, 26, 26, 1),
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'static/Inter-Medium.ttf'));
                }

                return TextSpan(
                    text: "${e.fullName}$showSplit",
                    style: TextStyle(
                        color: const Color.fromRGBO(26, 26, 26, 1),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'static/Inter-Medium.ttf'));
              }).toList(),
            ),
          ]),
    );
  }

  Widget getFileWidgets(List<ReqFile> listFile) {
    return Card(
      child: ListView.builder(
          padding: EdgeInsets.zero,
          itemCount: listFile.length,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, i) {
            return FileWidget.itemFile(Get.context!,
                remove: true, file: listFile[i], function: () {
              controller.files.removeAt(i);
              controller.files.refresh();
              controller.filesUploadNotification.removeAt(i);
              controller.filesUploadNotification.refresh();
            });
          }),
    );
  }

  clickCheckboxAdd( int index)  {
      controller.checkBoxListViewAdd(index);
      if (controller.listCheckBoxAdd.contains(false) == true) {
        controller.isCheckAllAdd.value = false;
      } else {
        controller.isCheckAllAdd.value = true;
      }
      controller.isCheckAllAdd.refresh();
      controller.update();
  }

 clickCheckboxMinus( int index)  {
      controller.checkBoxListViewMinus(index);
      if (controller.listCheckBoxMinus.contains(false) == true) {
        controller.isCheckAllMinus.value = false;
      } else {
        controller.isCheckAllMinus.value = true;
      }
      controller.isCheckAllMinus.refresh();
      controller.update();
  }

  clickCheckboxAllAdd() {
      controller.isCheckAllAdd.value = !controller.isCheckAllAdd.value;
      controller.checkAllAdd();
      controller.update();
  }
   cancelMinusUser() {
      controller.listCheckBoxMinus.value = [];
      for (int i = 0; i < controller.listUser.length; i++) {
        controller.listCheckBoxMinus.add(true);
      }
      controller.listCheckBoxMinus.refresh();
      controller.update();
  }


  cancelAddUser()  {
      controller.listCheckBoxAdd.value = [];
      for(int i = 0;i<controller.listUserNotSendNotify.length;i++){
        controller.listCheckBoxAdd.add(false);
      }
      controller.listCheckBoxAdd.refresh();
      controller.isCheckAllAdd.value = false;
      controller.update();
  }

  clickCheckboxAllMinus()  {
      controller.isCheckAllMinus.value = !controller.isCheckAllMinus.value;
      controller.checkAllMinus();
      controller.update();
  }

   addListUser()  {
      var listId = <String>[];
      for(int i = 0; i < controller.listUserNotSendNotify.length; i++){
        listId.add(controller.listUserNotSendNotify[i].id!);
      }
      for (int i = 0; i < controller.listUserNotSendNotify.length; i++) {
        if (controller.listCheckBoxAdd[i] == true) {
          controller.listUser.add(controller.listUserNotSendNotify[i]);
          listId.remove(controller.listUserNotSendNotify[i].id);
        }
      }
      controller.listUserNotSendNotify.value = [];
      for(int i = 0; i < controller.listAllUser.length; i++){
        if(listId.contains(controller.listAllUser[i].id) == true){
          controller.listUserNotSendNotify.add(controller.listAllUser[i]);
        }
      }
      controller.listUser.refresh();
      controller.listUserNotSendNotify.refresh();
      controller.listCheckBoxMinus.value = [];
      for (int i = 0; i < controller.listUser.length; i++) {
        controller.listCheckBoxMinus.add(true);
      }
      controller.listCheckBoxAdd.value = [];
      for (int i = 0; i < controller.listUserNotSendNotify.length; i++) {
        controller.listCheckBoxAdd.add(false);
      }
      controller.isCheckAllAdd.value = false;
      controller.listCheckBoxMinus.refresh();
      controller.listCheckBoxAdd.refresh();
      controller.update();
      Get.back();
  }

   minusListUser() {
      var listId = <String>[];
      for(int i = 0; i < controller.listUser.length; i++){
        listId.add(controller.listUser[i].id!);
      }
      for (int i = 0; i < controller.listUser.length; i++) {
        if (controller.listCheckBoxMinus[i] == false) {
          controller.listUserNotSendNotify.add(controller.listUser[i]);
          listId.remove(controller.listUser[i].id);
        }
      }
      controller.listUser.value = [];
      for(int i = 0; i < controller.listAllUser.length; i++){
        if(listId.contains(controller.listAllUser[i].id) == true){
          controller.listUser.add(controller.listAllUser[i]);
        }
      }
      controller.listUser.refresh();
      controller.listUserNotSendNotify.refresh();
      controller.listCheckBoxMinus.value = [];
      for (int i = 0; i < controller.listUser.length; i++) {
        controller.listCheckBoxMinus.add(true);
      }
      controller.listCheckBoxAdd.value = [];
      for (int i = 0; i < controller.listUserNotSendNotify.length; i++) {
        controller.listCheckBoxAdd.add(false);
      }
      controller.isCheckAllMinus.value = false;
      controller.listCheckBoxMinus.refresh();
      controller.listCheckBoxAdd.refresh();
      controller.update();
      Get.back();
  }
}
