import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../../../../../../../../commom/app_cache.dart';
import 'all_user/all_user_controller.dart';
import 'list_parent/list_parent_controller.dart';
import 'list_student/list_student_controller.dart';
import 'list_teacher/list_teacher_controller.dart';


class ListAllUserByClassSendNotificationController extends GetxController {
  var indexClick = 0.obs;
  var click = <bool>[].obs;
  var dropDownSort = ''.obs;
  var dropDownDetailSort = <String>[].obs;
  var listSort = ["A", "Z"].obs;
  var listSort1 = ["Z", "A"].obs;
  var listStatus = <String>[].obs;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: false,
  );
  var classId = "".obs;
  var controllerFillter = TextEditingController().obs;




  onPageViewChange(int page) {
    indexClick.value = page;
    if (page != 0) {
      indexClick.value - 1;
    } else {
      indexClick.value = 0;
    }
  }





  @override
  void onInit() {
    var tmpClassId = Get.arguments;
    if(tmpClassId!=null){
      classId.value = tmpClassId;
    }
    for (int i = 0; i < 3; i++) {
      click.add(false);
    }
    click[indexClick.value] = true;

    if(AppCache().userType == "TEACHER"){
      listStatus.value= ["Tất cả","Học sinh","Phụ huynh"];
    }else{
      listStatus.value= ["Tất cả","Học sinh","Phụ huynh","Giáo Viên"];
    }
    super.onInit();
  }

  @override
  dispose() {
    pageController.dispose();
    super.dispose();
  }

  getCount(index){
    switch(index){
      case 0:
        return Get.find<ListAllUser>().listAll.length;
      case 1:
        return Get.find<ListStudentController>().listStudent.length;
      case 2:
        return Get.find<ListParentController>().listParent.length;
      case 3:
        return Get.find<ListTeacherController>().listTeacher.length;
    }
  }

  showColor(index) {
    for (int i = 0; i < 3; i++) {
      click.add(false);
    }
    click[index] = true;
  }






}



