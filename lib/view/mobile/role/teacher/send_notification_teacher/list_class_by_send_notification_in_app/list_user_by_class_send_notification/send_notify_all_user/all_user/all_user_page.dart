import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/role/teacher/send_notification_teacher/list_class_by_send_notification_in_app/list_user_by_class_send_notification/send_notify_all_user/list_parent/list_parent_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/send_notification_teacher/list_class_by_send_notification_in_app/list_user_by_class_send_notification/send_notify_all_user/list_student/list_student_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/send_notification_teacher/list_class_by_send_notification_in_app/list_user_by_class_send_notification/send_notify_all_user/list_teacher/list_teacher_controller.dart';
import '../../../../../../../../../commom/utils/color_utils.dart';
import 'all_user_controller.dart';

class AllUserPage extends GetView<ListAllUser>{
  @override
  final controller = Get.put(ListAllUser());

  AllUserPage({super.key});

  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Obx(() => Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(6)),
      padding: const EdgeInsets.all(16),
      margin:  EdgeInsets.fromLTRB(16.w, 0, 16.w, 16.h),
      child: Column(
        children: [
          Row(
            children: [
              InkWell(
                onTap: () {
                  controller.isCheckAll.value = !controller.isCheckAll.value;
                  controller.checkAll();
                },
                child: Row(
                  children: [
                    Container(
                        width: 18.h,
                        height: 18.h,
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2.h),
                        margin: const EdgeInsets.only(right: 8),
                        decoration: ShapeDecoration(
                          shape: CircleBorder(
                              side: BorderSide(color: controller.isCheckAll.value ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(235, 235, 235, 1))),
                        ),
                        child: controller.isCheckAll.value
                            ? const Icon(Icons.circle, size: 12,
                          color:ColorUtils.PRIMARY_COLOR,
                        )
                            : null),
                    Padding(padding: EdgeInsets.only(left: 8.w)),
                    Text(
                      "Chọn hết",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w400,
                          color: Colors.black),
                    ),
                  ],
                ),
              ),
              Expanded(child: Container()),
              Text(
                "Đã chọn ${controller.listCheckBox.where((p0) => p0 == true).toList().length} /${controller.listAllCopy.length}",
                style: TextStyle(
                    color: const Color.fromRGBO(177, 177, 177, 1),
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w500),
              )
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 16.h)),
          Expanded(
            child: ListView.builder(
                itemCount: controller.listAll.length,
                shrinkWrap: true,
                physics: const ScrollPhysics(),
                itemBuilder: (context, index) {
                  controller.student.value =
                  controller.listAll[index].student!;
                  controller.parent.value =
                  controller.listAll[index].parent!;
                  return InkWell(
                    onTap: () {
                      controller.checkBoxListView(index);
                      if(controller.listCheckBox.contains(false) == true){
                        controller.isCheckAll.value = false;
                      }else{
                        controller.isCheckAll.value = true;
                      }

                    },
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                                width: 18.h,
                                height: 18.h,
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(2.h),
                                margin: const EdgeInsets.only(right: 8),
                                decoration: ShapeDecoration(
                                  shape: CircleBorder(
                                      side: BorderSide(color: controller.listCheckBox[index] ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(235, 235, 235, 1))),
                                ),
                                child: controller.listCheckBox[index]
                                    ? const Icon(Icons.circle, size: 12,
                                  color:ColorUtils.PRIMARY_COLOR,
                                )
                                    : null),
                            Padding(padding: EdgeInsets.only(left: 8.w)),
                            SizedBox(
                                width: 32.w,
                                height: 32.w,
                                child:

                                CacheNetWorkCustom(urlImage: '${ controller.listAll[index].image }')

                            ),
                            Padding(padding: EdgeInsets.only(left: 8.w)),
                            Expanded(child:  Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${controller.listAll[index].fullName}",
                                  style: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black),
                                ),
                                SizedBox(
                                  child: controller.listAll[index].type!="TEACHER"?getRole(
                                      controller.getListRole(controller.listAll[index].type,index),
                                      controller.listAll[index].type, index):getRoleTeacher(
                                      controller.getListRole(controller.listAll[index].type,index),
                                      controller.listAll[index].type, index),
                                )
                              ],
                            ))
                          ],
                        ),
                        const Divider(),
                      ],
                    ),
                  );
                }),
          ),
          Visibility(
              visible:  controller.listId.isNotEmpty,
              child: Container(
                margin: EdgeInsets.only(top: 8.h),
                decoration: BoxDecoration(
                  border: Border.all(color: const Color.fromRGBO(239, 239, 239, 1),),
                ),
                height: 86.h,
                child: ListView.builder(
                    itemCount: controller.listAllCopy.length,
                    physics: const ScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context,index){
                      return controller.listId.contains(controller.listAllCopy[index].id)
                          ?Container(
                        margin: EdgeInsets.symmetric(horizontal: 8.w),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              color: Colors.white,
                              child: Stack(
                                children: [
                                  Container(
                                      margin: EdgeInsets.all(8.w),
                                      width: 32.w,
                                      height: 32.w,
                                      child:

                                      CacheNetWorkCustom(urlImage: '${ controller.listAllCopy[index].image }')

                                  ),
                                  Positioned(
                                      top: 4,
                                      right: 0,
                                      child: InkWell(
                                        onTap: () {
                                          controller.listCheckBox[index] = false;
                                          controller.listId.remove(controller.listAllCopy[index].id!);
                                          if(controller.listCheckBox.contains(false) == true){
                                            controller.isCheckAll.value = false;
                                          }else{
                                            controller.isCheckAll.value = true;
                                          }

                                          for(int i = 0 ;i<Get.find<ListParentController>().listParent.length;i++){
                                            if(Get.find<ListParentController>().listParent[i].id != controller.listAll[index].id!){
                                            }else{
                                              Get.find<ListParentController>().listCheckBox[i] = false;
                                              Get.find<ListParentController>().listId.remove(Get.find<ListParentController>().listParent[i].id!);
                                            }

                                          }


                                          for(int i = 0 ;i<Get.find<ListStudentController>().listStudent.length;i++){
                                            if(Get.find<ListStudentController>().listStudent[i].id != controller.listAll[index].id!){
                                            }else{
                                              Get.find<ListStudentController>().listCheckBox[i] = false;
                                              Get.find<ListStudentController>().listId.remove(Get.find<ListStudentController>().listStudent[i].id!);
                                            }
                                          }

                                          if(AppCache().userType == "MANAGER"){
                                            for(int i = 0 ;i<Get.find<ListTeacherController>().listTeacher.length;i++){
                                              if(Get.find<ListTeacherController>().listTeacher[i].id !=controller.listAll[index].id!){
                                              }else{
                                                Get.find<ListTeacherController>().listCheckBox[i] = false;
                                                Get.find<ListTeacherController>().listId.remove(Get.find<ListTeacherController>().listTeacher[i].id!);
                                              }
                                            }
                                          }

                                          if(Get.find<ListStudentController>().listCheckBox.contains(false) == true){
                                            Get.find<ListStudentController>().isCheckAll.value = false;
                                          }else{
                                            Get.find<ListStudentController>().isCheckAll.value = true;
                                          }
                                          if(Get.find<ListParentController>().listCheckBox.contains(false) == true){
                                            Get.find<ListParentController>().isCheckAll.value = false;
                                          }else{
                                            Get.find<ListParentController>().isCheckAll.value = true;
                                          }
                                          if(AppCache().userType == "MANAGER"){
                                            if(Get.find<ListTeacherController>().listCheckBox.contains(false) == true){
                                              Get.find<ListTeacherController>().isCheckAll.value = false;
                                            }else{
                                              Get.find<ListTeacherController>().isCheckAll.value = true;
                                            }
                                          }
                                        },
                                        child: Container(
                                          padding: EdgeInsets.zero,
                                          decoration: const BoxDecoration(
                                            color: Colors.white,
                                            shape: BoxShape.circle,
                                          ),
                                          child: SvgPicture.asset("assets/images/icon_dangerous.svg",fit: BoxFit.cover,height: 18.w,width: 18.w,),
                                        ),
                                      ))
                                ],
                              ) ,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 8.h),
                              child:  Text("${controller.listAllCopy[index].fullName}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.w400,fontSize: 14),),
                            )
                          ],
                        ),
                      )
                          :Container();
                    }),
              ))
        ],
      ),
    ));
  }
  RichText getRole(List<dynamic> list, role, index) {
    return RichText(
      textAlign: TextAlign.left,
      text: TextSpan(
        text: '${controller.getTitle(role)}',
        style: TextStyle(
            color: const Color.fromRGBO(133, 133, 133, 1),
            fontSize: 12.sp,
            fontWeight: FontWeight.w500,
            fontFamily: 'static/Inter-Medium.ttf'),

        children: list.map((e) {
          var index = list.indexOf(e);
          var showSplit = ", ";
          if (index == list.length - 1) {
            showSplit = "";
          }
          return TextSpan(
              text: "${e.fullName}$showSplit",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf'));
        }).toList(),
      ),
    );
  }
  RichText getRoleTeacher(List<dynamic> list, role, index) {
    return RichText(
      textAlign: TextAlign.left,
      text: TextSpan(
          text: '${controller.getTitle(role)}',
          style: TextStyle(
              color: const Color.fromRGBO(133, 133, 133, 1),
              fontSize: 12.sp,
              fontWeight: FontWeight.w500,
              fontFamily: 'static/Inter-Medium.ttf'),

          children: [
            TextSpan(
              text:
              "${controller.getTextHomeRoomTeacher(controller.listAll[index].positionName, index)}",
              style: TextStyle(
                  color: const Color.fromRGBO(26, 26, 26, 1),
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf'),
              children: controller.listAll[index].subject?.map((e) {
                var indexSubject = controller.listAll[index].subject?.indexOf(e);
                var showSplit = ", ";
                if (indexSubject == controller.listAll[index].subject!.length - 1) {
                  showSplit = "";
                }
                if (controller.listAll[index].type == "TEACHER") {
                  return TextSpan(
                      text: "Môn ${e.fullName}$showSplit",
                      style: TextStyle(
                          color: const Color.fromRGBO(26, 26, 26, 1),
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'static/Inter-Medium.ttf'));
                }

                return TextSpan(
                    text: "${e.fullName}$showSplit",
                    style: TextStyle(
                        color: const Color.fromRGBO(26, 26, 26, 1),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'static/Inter-Medium.ttf'));
              }).toList(),
            ),
          ]
      ),
    );
  }

}