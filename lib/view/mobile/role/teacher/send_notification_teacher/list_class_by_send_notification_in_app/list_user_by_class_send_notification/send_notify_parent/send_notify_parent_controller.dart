import 'package:get/get.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../../data/repository/contact/contact_repo.dart';

class SendNotifyParentController extends GetxController{
  var selectedQuantity = 0.obs;
  var clickAll = <bool>[].obs;
  var listCheckBox = <bool>[].obs;
  var isCheckAll = false.obs;
  final ContactRepo _contactRepo = ContactRepo();
  var contacts = Contacts().obs;
  RxList<ItemContact> listParent = <ItemContact>[].obs;
  RxList<ItemContact> listParentCopy = <ItemContact>[].obs;
  var students = <Student>[].obs;
  var classId = "".obs;
  var listId = <String>[];
  @override
  void onInit() {
    var tmpClassId = Get.arguments;
    if(tmpClassId!=null){
      classId.value = tmpClassId;
    }
    getStudentClassContact();
    super.onInit();
  }


  getStudentClassContact() async {
    _contactRepo.getClassContact(classId, "PARENT","").then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        listParent.value = contacts.value.item!;
        listParentCopy.value = contacts.value.item!;
        for (int i = 0; i < listParent.length; i++) {
          listCheckBox.add(false);
        }
      }
    });
  }

  filterStudentClassContact(fullName) async {
    _contactRepo.fillterClassContact(classId, "PARENT",fullName).then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        listParent.value = contacts.value.item!;
        for (int i = 0; i < listParent.length; i++) {
          listCheckBox.add(false);
        }
      }
    });
  }


  checkBoxListView(index) {
    if (listCheckBox[index] == false) {
      listCheckBox[index] = true;
      selectedQuantity++;
      if(listId.contains(listParent[index].id!) == true){
      }else{
        listId.add(listParent[index].id!);
      }
    } else {
      listCheckBox[index] = false;
      selectedQuantity--;
      listId.remove(listParent[index].id!);
    }
  }


  checkAll() {
    if (isCheckAll.value == true) {
      listCheckBox.value = [];
      for (int i = 0; i < listParent.length; i++){
        if(listId.contains(listParent[i].id!) == true){
        }else{
          listId.add(listParent[i].id!);
        }
      }
      for (int i = 0; i < listId.length; i++) {
        listCheckBox.add(true);
      }
      selectedQuantity.value = listParent.length;
    } else {
      listCheckBox.value = [];
      for (int i = 0; i < listParent.length; i++) {
        listCheckBox.add(false);
        if(listId.contains(listParent[i].id!) == true){
          listId.remove(listParent[i].id!);
        }
      }
      selectedQuantity.value = 0;
    }
  }





}