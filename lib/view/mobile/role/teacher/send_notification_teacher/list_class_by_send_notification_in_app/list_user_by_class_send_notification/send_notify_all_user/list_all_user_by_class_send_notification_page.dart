import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import '../../../../../../../../commom/app_cache.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../routes/app_pages.dart';
import '../../list_class_by_send_notification_in_app_controller.dart';
import 'all_user/all_user_controller.dart';
import 'all_user/all_user_page.dart';
import 'list_all_user_by_class_send_notification_controller.dart';
import 'list_parent/list_parent_controller.dart';
import 'list_parent/list_parent_page.dart';
import 'list_student/list_student_controller.dart';
import 'package:tiengviet/tiengviet.dart';
import 'list_student/list_student_page.dart';
import 'list_teacher/list_teacher_controller.dart';
import 'list_teacher/list_teacher_page.dart';

class ListAllUserByClassSendNotificationPage extends GetWidget<ListAllUserByClassSendNotificationController> {
  @override
  final controller = Get.put(ListAllUserByClassSendNotificationController());

  ListAllUserByClassSendNotificationPage({super.key});



  @override
  Widget build(BuildContext context) {
    return Obx(() => GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
          },
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              backgroundColor: ColorUtils.PRIMARY_COLOR,
              elevation: 0,
              title: Text(
                Get.find<ListClasSendNotificationController>().typeSendNotify.value == "SMS"?"Gửi thông báo SMS":"Gửi Thông Báo Trên Ứng Dụng",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w500),
              ),
              actions: [
                InkWell(
                  onTap: () =>  Get.until((route) => route.settings.name == Routes.home),
                  child: const Icon(
                    Icons.home,
                    color: Colors.white,
                  ),
                ),
                Padding(padding: EdgeInsets.only(right: 16.w))
              ],
            ),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 8.h),
                  child: Row(
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: 40.h,
                          child: TextFormField(
                            onChanged: (value) {
                              Get.find<ListAllUser>().listAll.value = [];
                              Get.find<ListAllUser>().listCheckBox.value = [];
                              Get.find<ListAllUser>().listAll.value = Get.find<ListAllUser>().listAllCopy.where((element) => TiengViet.parse(element.fullName!.toLowerCase()).contains(TiengViet.parse(value.toLowerCase().trim())) == true).toList();
                              for(int i= 0; i <Get.find<ListAllUser>().listAll.length; i++){
                                Get.find<ListAllUser>().listCheckBox.add(false);
                              }
                              for(int j = 0; j <Get.find<ListAllUser>().listAll.length; j++){
                                if(Get.find<ListAllUser>().listId.contains(Get.find<ListAllUser>().listAll[j].id) ){
                                  Get.find<ListAllUser>().listCheckBox[j] = true;
                                }else{
                                  Get.find<ListAllUser>().listCheckBox[j] = false;
                                }
                              }
                              if(Get.find<ListAllUser>().listCheckBox.contains(false) == true){
                                Get.find<ListAllUser>().isCheckAll.value = false;
                              }else{
                                Get.find<ListAllUser>().isCheckAll.value = true;
                              }
                              Get.find<ListAllUser>().listAll.refresh();
                              Get.find<ListAllUser>().listCheckBox.refresh();
                              Get.find<ListStudentController>().listStudent.value = [];
                              Get.find<ListStudentController>().listCheckBox.value = [];
                              Get.find<ListStudentController>().listStudent.value = Get.find<ListStudentController>().listStudentCopy.where((element) => TiengViet.parse(element.fullName!.toLowerCase()).contains(TiengViet.parse(value.toLowerCase().trim())) == true).toList();
                              for(int i= 0; i <Get.find<ListStudentController>().listStudent.length; i++){
                                Get.find<ListStudentController>().listCheckBox.add(false);
                              }
                              for(int j = 0; j <Get.find<ListStudentController>().listStudent.length; j++){
                                if(Get.find<ListStudentController>().listId.contains(Get.find<ListStudentController>().listStudent[j].id) ){
                                  Get.find<ListStudentController>().listCheckBox[j] = true;
                                }else{
                                  Get.find<ListStudentController>().listCheckBox[j] = false;
                                }
                              }
                              if(Get.find<ListStudentController>().listCheckBox.isNotEmpty){
                                if(Get.find<ListStudentController>().listCheckBox.contains(false) == true){
                                  Get.find<ListStudentController>().isCheckAll.value = false;
                                }else{
                                  Get.find<ListStudentController>().isCheckAll.value = true;
                                }
                              }

                              Get.find<ListStudentController>().listStudent.refresh();
                              Get.find<ListStudentController>().listCheckBox.refresh();
                              Get.find<ListParentController>().listParent.value = [];
                              Get.find<ListParentController>().listCheckBox.value = [];
                              Get.find<ListParentController>().listParent.value = Get.find<ListParentController>().listParentCopy.where((element) => TiengViet.parse(element.fullName!.toLowerCase()).contains(TiengViet.parse(value.toLowerCase().trim())) == true).toList();
                              for(int i= 0; i <Get.find<ListParentController>().listParent.length; i++){
                                Get.find<ListParentController>().listCheckBox.add(false);
                              }
                              for(int j = 0; j <Get.find<ListParentController>().listParent.length; j++){
                                if(Get.find<ListParentController>().listId.contains(Get.find<ListParentController>().listParent[j].id) ){
                                  Get.find<ListParentController>().listCheckBox[j] = true;
                                }else{
                                  Get.find<ListParentController>().listCheckBox[j] = false;
                                }
                              }

                              if(Get.find<ListParentController>().listCheckBox.isNotEmpty){
                                if(Get.find<ListParentController>().listCheckBox.contains(false) == true){
                                  Get.find<ListParentController>().isCheckAll.value = false;
                                }else{
                                  Get.find<ListParentController>().isCheckAll.value = true;
                                }
                              }

                              Get.find<ListParentController>().listParent.refresh();
                              Get.find<ListParentController>().listCheckBox.refresh();


                              if(AppCache().userType == "MANAGER"){
                                Get.find<ListTeacherController>().listTeacher.value = [];
                                Get.find<ListTeacherController>().listCheckBox.value = [];
                                Get.find<ListTeacherController>().listTeacher.value = Get.find<ListTeacherController>().listTeacherCopy.where((element) => TiengViet.parse(element.fullName!.toLowerCase()).contains(TiengViet.parse(value.toLowerCase().trim())) == true).toList();
                                for(int i= 0; i <Get.find<ListTeacherController>().listTeacher.length; i++){
                                  Get.find<ListTeacherController>().listCheckBox.add(false);
                                }
                                for(int j = 0; j <Get.find<ListTeacherController>().listTeacher.length; j++){
                                  if(Get.find<ListTeacherController>().listId.contains(Get.find<ListTeacherController>().listTeacher[j].id) ){
                                    Get.find<ListTeacherController>().listCheckBox[j] = true;
                                  }else{
                                    Get.find<ListTeacherController>().listCheckBox[j] = false;
                                  }
                                }
                                if(Get.find<ListTeacherController>().listCheckBox.isNotEmpty){
                                  if(Get.find<ListTeacherController>().listCheckBox.contains(false) == true){
                                    Get.find<ListTeacherController>().isCheckAll.value = false;
                                  }else{
                                    Get.find<ListTeacherController>().isCheckAll.value = true;
                                  }
                                }

                                Get.find<ListTeacherController>().listTeacher.refresh();
                                Get.find<ListTeacherController>().listCheckBox.refresh();
                              }

                            },
                            controller: controller.controllerFillter.value,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color:
                                            Color.fromRGBO(177, 177, 177, 1)),
                                    borderRadius: BorderRadius.circular(6)),
                                prefixIcon: const Icon(
                                  Icons.search,
                                  color: Color.fromRGBO(177, 177, 177, 1),
                                ),
                                hintText: "Tìm kiếm",
                                isCollapsed: true,
                                hintStyle: TextStyle(
                                    fontSize: 14.sp,
                                    color:
                                        const Color.fromRGBO(177, 177, 177, 1)),
                                focusedBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: ColorUtils.PRIMARY_COLOR))),
                            textAlignVertical: TextAlignVertical.center,
                            cursorColor: ColorUtils.PRIMARY_COLOR,
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(left: 8.w)),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 16.w),
                  height: 40,
                  child: ListView.builder(
                      itemCount: controller.listStatus.length,
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {

                        return GetBuilder<ListAllUserByClassSendNotificationController>(
                            builder: (controller) {
                          return TextButton(
                              onPressed: () {
                                controller.showColor(index);
                                controller.pageController.animateToPage(index,
                                    duration: const Duration(milliseconds: 500),
                                    curve: Curves.linear);
                              },
                              child: Obx(() => Row(
                                children: [
                                  Text(
                                    controller.listStatus[index],
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12.sp,
                                        color: controller.indexClick.value == index
                                            ? ColorUtils.PRIMARY_COLOR
                                            : const Color.fromRGBO(
                                            177, 177, 177, 1)),
                                  ),
                                  Visibility(
                                      visible: Get.isRegistered<ListAllUser>()&&Get.isRegistered<ListParentController>()&&Get.isRegistered<ListStudentController>(),
                                      child: Text(
                                    " (${controller.getCount(index).toString()})",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12.sp,
                                        color: controller.indexClick.value == index
                                            ? ColorUtils.PRIMARY_COLOR
                                            : const Color.fromRGBO(
                                            177, 177, 177, 1)),
                                  ))
                                ],
                              )));
                        });
                      }),
                ),
                Expanded(
                    child: PageView(
                  onPageChanged: (value) {
                    controller.onPageViewChange(value);
                  },
                  controller: controller.pageController,
                  physics: const ScrollPhysics(),
                  children: AppCache().userType == "TEACHER"?[AllUserPage(),ListStudentPage(),ListParentPage()]:[AllUserPage(),ListStudentPage(),ListParentPage(),ListTeacherPage()],
                )),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.all(16),
                  color: Colors.white,
                  child: SizedBox(
                    height: 46.h,
                    child: ElevatedButton(
                      onPressed: () {
                        var listUser = <ItemContact>[];
                        var listUserNotSendNotify = <ItemContact>[];
                        switch (controller.indexClick.value) {
                          case 0:
                            for (int i = 0; i < Get.find<ListAllUser>().listAll.length; i++) {
                              if (Get.find<ListAllUser>().listId.contains(Get.find<ListAllUser>().listAllCopy[i].id!)) {
                                listUser.add(Get.find<ListAllUser>().listAll[i]);
                              }else{
                                listUserNotSendNotify.add(Get.find<ListAllUser>().listAll[i]);
                              }
                            }
                            break;
                          case 1:
                            for(int i = 0;i<Get.find<ListStudentController>().listStudentCopy.length;i++){
                              if(Get.find<ListStudentController>().listId.contains(Get.find<ListStudentController>().listStudentCopy[i].id!)){
                                listUser.add(Get.find<ListStudentController>().listStudentCopy[i]);
                              }else{
                                listUserNotSendNotify.add(Get.find<ListStudentController>().listStudentCopy[i]);
                              }
                            }
                            break;
                          case 2:
                            for(int i = 0;i<Get.find<ListParentController>().listParentCopy.length;i++){
                              if(Get.find<ListParentController>().listId.contains(Get.find<ListParentController>().listParentCopy[i].id!)){
                                listUser.add(Get.find<ListParentController>().listParentCopy[i]);
                              }else{
                                listUserNotSendNotify.add(Get.find<ListParentController>().listParentCopy[i]);
                              }
                            }
                            break;
                          case 3:
                            for (int i = 0; i < Get.find<ListTeacherController>().listTeacher.length; i++) {
                              if (Get.find<ListTeacherController>().listId.contains(Get.find<ListTeacherController>().listTeacherCopy[i].id!)) {
                                listUser.add(Get.find<ListTeacherController>().listTeacher[i]);
                              }else{
                                listUserNotSendNotify.add(Get.find<ListTeacherController>().listTeacher[i]);
                              }
                            }
                            break;
                        }
                        if(listUser.isEmpty){
                          AppUtils.shared.showToast("Vui lòng chọn người để gửi");
                        }else{
                          if(Get.find<ListClasSendNotificationController>().typeSendNotify.value == "SMS"){
                            Get.toNamed(Routes.detailSendSMSNotification,arguments: [listUser,listUserNotSendNotify,Get.find<ListAllUser>().contacts.value.id]);
                          }
                          if(Get.find<ListClasSendNotificationController>().typeSendNotify.value == "INAPP"){
                            Get.toNamed(Routes.detailSendInAppNotification,arguments: [listUser,listUserNotSendNotify,Get.find<ListAllUser>().contacts.value.id]);
                          }
                        }

                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: ColorUtils.PRIMARY_COLOR,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(6))),
                      child: Text("Tiếp Tục",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.w400)),
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
