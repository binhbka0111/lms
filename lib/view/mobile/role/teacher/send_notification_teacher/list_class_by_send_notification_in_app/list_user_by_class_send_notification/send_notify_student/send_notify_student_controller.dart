import 'package:get/get.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../../data/repository/contact/contact_repo.dart';

class SendNotifyStudentController extends GetxController{
  var selectedQuantity = 0.obs;
  var listCheckBox = <bool>[].obs;
  var isCheckAll = false.obs;
  final ContactRepo _contactRepo = ContactRepo();
  var contacts = Contacts().obs;
  RxList<ItemContact> listStudent = <ItemContact>[].obs;
  RxList<ItemContact> listStudentCopy = <ItemContact>[].obs;
  var students = <Student>[].obs;
  var classId = "".obs;
  var listId = <String>[];
  @override
  void onInit() {
    var tmpClassId = Get.arguments;
    if(tmpClassId!=null){
      classId.value = tmpClassId;
    }
    getStudentClassContact();
    super.onInit();
  }


  getStudentClassContact() async {
    _contactRepo.getClassContact(classId, "STUDENT","").then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        listStudent.value = contacts.value.item!;
        listStudentCopy.value = contacts.value.item!;
        for (int i = 0; i < listStudent.length; i++) {
          listCheckBox.add(false);
        }
      }
    });
  }

  fillterStudentClassContact(fullName) async {
    _contactRepo.fillterClassContact(classId, "STUDENT",fullName).then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        listStudent.value = contacts.value.item!;
        for (int i = 0; i < listStudent.length; i++) {
          listCheckBox.add(false);
        }
      }
    });
  }


  checkBoxListView(index) {
    if (listCheckBox[index] == false) {
      listCheckBox[index] = true;
      selectedQuantity++;
      if(listId.contains(listStudent[index].id!)== true){

      }else{
        listId.add(listStudent[index].id!);
      }

    } else {
      listCheckBox[index] = false;
      selectedQuantity--;
      listId.remove(listStudent[index].id!);
    }
  }


  checkAll() {
    if (isCheckAll.value == true) {
      listCheckBox.value = [];
      for (int i = 0; i < listStudent.length; i++){
        listId.add(listStudent[i].id!);
        if(listId.contains(listStudent[i].id!) == true){
        }else{
          listId.add(listStudent[i].id!);
        }
      }
      for (int i = 0; i < listId.length; i++) {
        listCheckBox.add(true);
      }
      selectedQuantity.value = listStudent.length;
    } else {
      listCheckBox.value = [];
      for (int i = 0; i < listStudent.length; i++) {
        listCheckBox.add(false);
        if(listId.contains(listStudent[i].id!) == true){
          listId.remove(listStudent[i].id!);
        }
      }
      selectedQuantity.value = 0;
    }
  }





}