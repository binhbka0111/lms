import 'package:get/get.dart';
import '../../../../../../../../../commom/app_cache.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../../data/repository/contact/contact_repo.dart';
import '../list_all_user_by_class_send_notification_controller.dart';
import '../list_parent/list_parent_controller.dart';
import '../list_student/list_student_controller.dart';
import '../list_teacher/list_teacher_controller.dart';

class ListAllUser extends GetxController{
  var clickAll = <bool>[].obs;
  var listCheckBox = <bool>[].obs;
  var isCheckAll = false.obs;
  final ContactRepo _contactRepo = ContactRepo();
  var contacts = Contacts().obs;
  RxList<ItemContact> listStudent = <ItemContact>[].obs;
  RxList<ItemContact> listParent = <ItemContact>[].obs;
  RxList<ItemContact> listAll= <ItemContact>[].obs;
  RxList<ItemContact> listAllCopy= <ItemContact>[].obs;
  var student = <Student>[].obs;
  var parent = <Parent>[].obs;
  var listId = <String>[];
  @override
  void onInit() {
    if(AppCache().userType == "TEACHER"){
      getStudentClassContact();
      getParentClassContact();
    }else{
      getClassContact();
    }
    super.onInit();
  }

  getClassContact() async {
    listAll.value = [];
    _contactRepo.getClassContact(Get.find<ListAllUserByClassSendNotificationController>().classId.value, "","").then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        listAll.value = contacts.value.item!;
        listAllCopy.value = contacts.value.item!;
        listCheckBox.value = [];
        for (int i = 0; i < listAll.length; i++) {
          listCheckBox.add(false);
        }
      }
    });
    listAll.refresh();
  }

  getStudentClassContact() async {
    listStudent.value = [];
    listAll.value = [];
    _contactRepo.getClassContact(Get.find<ListAllUserByClassSendNotificationController>().classId.value, "STUDENT","").then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        listStudent.value = contacts.value.item!;
        listAll.addAll(listStudent);
        listAllCopy.addAll(listStudent);
        listCheckBox.value = [];
        for (int i = 0; i < listAll.length; i++) {
          listCheckBox.add(false);
        }
      }
    });
    listStudent.refresh();
    listAll.refresh();
  }

  getListRole(role,index) {
    switch (role) {
      case "PARENT":
        return student;
      case "STUDENT":
        return parent;
      case "TEACHER":
        return listAll[index].subject??"";
    }
  }
  getTextHomeRoomTeacher(isHomeRoom, index) {
    var split = "";
    switch (isHomeRoom) {
      case "":
        return "";
      case null:
        return "";
      case "Giáo viên chủ nhiệm":
        if (listAll[index].subject!.isEmpty) {
          split = "";
        } else {
          split = ",";
        }
        return "Giáo viên chủ nhiệm$split ";
      default:
        return "";
    }
  }


  getParentClassContact() async {
    listParent.value = [];
    listAll.value = [];
    _contactRepo.getClassContact(Get.find<ListAllUserByClassSendNotificationController>().classId.value, "PARENT","").then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        listParent.value = contacts.value.item!;
        listAll.addAll(listParent);
        listAllCopy.addAll(listParent);
        listCheckBox.value = [];
        for (int i = 0; i < listAll.length; i++) {
          listCheckBox.add(false);
        }
      }
    });
    listParent.refresh();
    listAll.refresh();
  }

  checkBoxListView(index) {
    if (listCheckBox[index] == false) {
      listCheckBox[index] = true;
      if(listId.contains(listAll[index].id!)== true){
      }else{
        listId.add(listAll[index].id!);
      }

      for(int i = 0 ;i<Get.find<ListParentController>().listParent.length;i++){
        if(Get.find<ListParentController>().listParent[i].id !=listAll[index].id!){
        }else{
          Get.find<ListParentController>().listId.add(Get.find<ListParentController>().listParent[i].id!);
          Get.find<ListParentController>().listCheckBox[i] = true;
        }
      }
      for(int i = 0 ;i<Get.find<ListStudentController>().listStudent.length;i++){
        if(Get.find<ListStudentController>().listStudent[i].id != listAll[index].id!){
        }else{
          Get.find<ListStudentController>().listId.add(Get.find<ListStudentController>().listStudent[i].id!);
          Get.find<ListStudentController>().listCheckBox[i] = true;
        }
      }
      if(AppCache().userType == "MANAGER"){
        for(int i = 0 ;i<Get.find<ListTeacherController>().listTeacher.length;i++){
          if(Get.find<ListTeacherController>().listTeacher[i].id != listAll[index].id!){
          }else{
            Get.find<ListTeacherController>().listId.add(Get.find<ListTeacherController>().listTeacher[i].id!);
            Get.find<ListTeacherController>().listCheckBox[i] = true;
          }
        }
      }

    } else {
      listCheckBox[index] = false;
      listId.remove(listAll[index].id!);


      for(int i = 0 ;i<Get.find<ListParentController>().listParent.length;i++){
        if(Get.find<ListParentController>().listParent[i].id !=listAll[index].id!){
        }else{
          Get.find<ListParentController>().listCheckBox[i] = false;
          Get.find<ListParentController>().listId.remove(Get.find<ListParentController>().listParent[i].id!);
        }

      }


      for(int i = 0 ;i<Get.find<ListStudentController>().listStudent.length;i++){
        if(Get.find<ListStudentController>().listStudent[i].id !=listAll[index].id!){
        }else{
          Get.find<ListStudentController>().listCheckBox[i] = false;
          Get.find<ListStudentController>().listId.remove(Get.find<ListStudentController>().listStudent[i].id!);
        }
      }

      if(AppCache().userType == "MANAGER"){
        for(int i = 0 ;i<Get.find<ListTeacherController>().listTeacher.length;i++){
          if(Get.find<ListTeacherController>().listTeacher[i].id !=listAll[index].id!){
          }else{
            Get.find<ListTeacherController>().listCheckBox[i] = false;
            Get.find<ListTeacherController>().listId.remove(Get.find<ListTeacherController>().listTeacher[i].id!);
          }
        }
      }



    }

    if(Get.find<ListStudentController>().listCheckBox.contains(false) == true){
      Get.find<ListStudentController>().isCheckAll.value = false;
    }else{
      Get.find<ListStudentController>().isCheckAll.value = true;
    }
    if(Get.find<ListParentController>().listCheckBox.contains(false) == true){
      Get.find<ListParentController>().isCheckAll.value = false;
    }else{
      Get.find<ListParentController>().isCheckAll.value = true;
    }
    if(AppCache().userType == "MANAGER"){
      if(Get.find<ListTeacherController>().listCheckBox.contains(false) == true){
        Get.find<ListTeacherController>().isCheckAll.value = false;
      }else{
        Get.find<ListTeacherController>().isCheckAll.value = true;
      }
    }
  }


  checkAll() {
    if (isCheckAll.value == true) {
      listCheckBox.value = [];
      Get.find<ListParentController>().listCheckBox.value = [];
      Get.find<ListStudentController>().listCheckBox.value = [];
      for (int i = 0; i < listAll.length; i++){
        if(listId.contains(listAll[i].id!) == true){
        }else{
          listId.add(listAll[i].id!);
        }
      }
      for(int i = 0 ;i<Get.find<ListParentController>().listParent.length;i++){
        if(Get.find<ListParentController>().listId.contains(Get.find<ListParentController>().listParent[i].id) == false ){
          Get.find<ListParentController>().listId.add(Get.find<ListParentController>().listParent[i].id!);
        }
      }

      for(int i = 0 ;i<Get.find<ListStudentController>().listStudent.length;i++){
        if(Get.find<ListStudentController>().listId.contains(Get.find<ListStudentController>().listStudent[i].id) == false){
          Get.find<ListStudentController>().listId.add(Get.find<ListStudentController>().listStudent[i].id!);
        }
      }

      if(AppCache().userType == "MANAGER"){
        for(int i = 0 ;i<Get.find<ListTeacherController>().listTeacher.length;i++){
          if(Get.find<ListTeacherController>().listId.contains(Get.find<ListTeacherController>().listTeacher[i].id) == false){
            Get.find<ListTeacherController>().listId.add(Get.find<ListTeacherController>().listTeacher[i].id!);
          }
        }
      }


      for (int i = 0; i < listId.length; i++) {
        listCheckBox.add(true);
      }
      for (int i = 0; i < Get.find<ListStudentController>().listStudent.length; i++) {
        Get.find<ListStudentController>().listCheckBox.add(true);
      }
      for (int i = 0; i < Get.find<ListParentController>().listParent.length; i++) {
        Get.find<ListParentController>().listCheckBox.add(true);
      }
      if(AppCache().userType == "MANAGER"){
        for (int i = 0; i < Get.find<ListTeacherController>().listTeacher.length; i++) {
          Get.find<ListTeacherController>().listCheckBox.add(true);
        }
      }

    } else {
      listCheckBox.value = [];
      Get.find<ListParentController>().listCheckBox.value = [];
      Get.find<ListStudentController>().listCheckBox.value = [];
      if(AppCache().userType == "MANAGER"){
        Get.find<ListTeacherController>().listCheckBox.value = [];
      }

      for (int i = 0; i < listAll.length; i++) {
        listCheckBox.add(false);
        if(listId.contains(listAll[i].id!) == true){
          listId.remove(listAll[i].id!);
        }
      }

      for (int i = 0; i < Get.find<ListParentController>().listParent.length; i++) {
        Get.find<ListParentController>().listCheckBox.add(false);
        if(Get.find<ListParentController>().listId.contains(Get.find<ListParentController>().listParent[i].id!) == true){
          Get.find<ListParentController>().listId.remove(Get.find<ListParentController>().listParent[i].id!);
        }
      }

      for (int i = 0; i < Get.find<ListStudentController>().listStudent.length; i++) {
        Get.find<ListStudentController>().listCheckBox.add(false);
        if(Get.find<ListStudentController>().listId.contains(Get.find<ListStudentController>().listStudent[i].id!) == true){
          Get.find<ListStudentController>().listId.remove(Get.find<ListStudentController>().listStudent[i].id!);
        }
      }
      if(AppCache().userType == "MANAGER"){
        for (int i = 0; i < Get.find<ListTeacherController>().listTeacher.length; i++) {
          Get.find<ListTeacherController>().listCheckBox.add(false);
          if(Get.find<ListTeacherController>().listId.contains(Get.find<ListTeacherController>().listTeacher[i].id!) == true){
            Get.find<ListTeacherController>().listId.remove(Get.find<ListTeacherController>().listTeacher[i].id!);
          }
        }
      }

    }
    if(Get.find<ListStudentController>().listCheckBox.contains(false) == true){
      Get.find<ListStudentController>().isCheckAll.value = false;
    }else{
      Get.find<ListStudentController>().isCheckAll.value = true;
    }
    if(Get.find<ListParentController>().listCheckBox.contains(false) == true){
      Get.find<ListParentController>().isCheckAll.value = false;
    }else{
      Get.find<ListParentController>().isCheckAll.value = true;
    }
    if(AppCache().userType == "MANAGER"){
      if(Get.find<ListTeacherController>().listCheckBox.contains(false) == true){
        Get.find<ListTeacherController>().isCheckAll.value = false;
      }else{
        Get.find<ListTeacherController>().isCheckAll.value = true;
      }
    }

  }



  getTitle(role) {
    switch (role) {
      case "PARENT":
        return "Phụ huynh hs: ";
      case "STUDENT":
        return "Phụ huynh: ";
      case "TEACHER":
        return "Giáo viên: ";
      default:
        return "";
    }
  }


}