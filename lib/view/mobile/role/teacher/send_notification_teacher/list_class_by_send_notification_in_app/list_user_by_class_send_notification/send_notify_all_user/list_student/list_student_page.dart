import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/role/teacher/send_notification_teacher/list_class_by_send_notification_in_app/list_user_by_class_send_notification/send_notify_all_user/all_user/all_user_controller.dart';
import 'list_student_controller.dart';

class ListStudentPage extends GetView<ListStudentController>{
  @override
  final controller = Get.put(ListStudentController());

  ListStudentPage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Obx(() => Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(6)),
      padding: const EdgeInsets.all(16),
      margin:  EdgeInsets.fromLTRB(16.w, 0, 16.w, 16.h),
      child: Column(
        children: [
          Row(
            children: [
              InkWell(
                onTap: () {
                  controller.isCheckAll.value = !controller.isCheckAll.value;
                  controller.checkAll();
                },
                child: Row(
                  children: [
                    Container(
                        width: 18.h,
                        height: 18.h,
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(2.h),
                        margin: const EdgeInsets.only(right: 8),
                        decoration: ShapeDecoration(
                          shape: CircleBorder(
                              side: BorderSide(color: controller.isCheckAll.value ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(235, 235, 235, 1))),
                        ),
                        child: controller.isCheckAll.value
                            ? const Icon(Icons.circle, size: 12,
                          color:ColorUtils.PRIMARY_COLOR,
                        )
                            : null),
                    Padding(padding: EdgeInsets.only(left: 8.w)),
                    Text(
                      "Chọn hết",
                      style: TextStyle(
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w400,
                          color: Colors.black),
                    ),
                  ],
                ),
              ),
              Expanded(child: Container()),
              Text(
                "Đã chọn ${controller.listCheckBox.where((p0) => p0 == true).toList().length} /${controller.listStudentCopy.length}",
                style: TextStyle(
                    color: const Color.fromRGBO(177, 177, 177, 1),
                    fontSize: 12.sp,
                    fontWeight: FontWeight.w500),
              )
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 16.h)),
          Expanded(
            child: ListView.builder(
                itemCount: controller.listStudent.length,
                shrinkWrap: true,
                physics: const ScrollPhysics(),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      controller.checkBoxListView(index);
                      if(controller.listCheckBox.contains(false) == true){
                        controller.isCheckAll.value = false;
                      }else{
                        controller.isCheckAll.value = true;
                      }

                    },
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                                width: 18.h,
                                height: 18.h,
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(2.h),
                                margin: const EdgeInsets.only(right: 8),
                                decoration: ShapeDecoration(
                                  shape: CircleBorder(
                                      side: BorderSide(color: controller.listCheckBox[index] ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(235, 235, 235, 1))),
                                ),
                                child: controller.listCheckBox[index]
                                    ? const Icon(Icons.circle, size: 12,
                                  color:ColorUtils.PRIMARY_COLOR,
                                )
                                    : null),
                            Padding(padding: EdgeInsets.only(left: 8.w)),
                            SizedBox(
                              width: 32.h,
                              height: 32.h,
                              child:
                              CacheNetWorkCustom(urlImage:  '${  controller.listStudent[index].image }',),

                            ),
                            Padding(padding: EdgeInsets.only(left: 8.w)),
                            Expanded(child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${controller.listStudent[index].fullName}",
                                  style: TextStyle(
                                      fontSize: 16.sp,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black),
                                ),
                                controller.listStudent[index].parent!.isNotEmpty?
                                RichText(
                                  textAlign: TextAlign.left,
                                  text: TextSpan(
                                    text: 'Phụ huynh: ',
                                    style: TextStyle(
                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: 'static/Inter-Medium.ttf'),

                                    children: controller.listStudent[index].parent?.map((e) {
                                      var indexName = controller.listStudent[index].parent?.indexOf(e);
                                      var showSplit = ", ";
                                      if (indexName == controller.listStudent[index].parent!.length - 1) {
                                        showSplit = "";
                                      }
                                      return TextSpan(
                                          text: "${e.fullName}$showSplit",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w500,
                                              fontFamily: 'static/Inter-Medium.ttf'));
                                    }).toList(),
                                  ),
                                ):Container()
                              ],
                            ))
                          ],
                        ),
                        const Divider(),
                      ],
                    ),
                  );
                }),
          ),
          Visibility(
              visible: controller.listId.isNotEmpty,
              child: Container(
                margin: EdgeInsets.only(top: 8.h),
                decoration: BoxDecoration(
                  border: Border.all(color: const Color.fromRGBO(239, 239, 239, 1),),
                ),
                height: 86.h,
                child: ListView.builder(
                    itemCount: controller.listStudentCopy.length,
                    physics: const ScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context,index){
                      return controller.listId.contains(controller.listStudentCopy[index].id)
                          ?Container(
                        margin: EdgeInsets.symmetric(horizontal: 8.w),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              color: Colors.white,
                              child: Stack(
                                children: [
                                  Container(
                                    margin: EdgeInsets.all(8.h),
                                    width: 32.h,
                                    height: 32.h,
                                    child:
                                    CacheNetWorkCustom(urlImage: '${ controller.listStudentCopy[index].image}',)

                                  ),
                                  Positioned(
                                      top: 4,
                                      right: 0,
                                      child: InkWell(
                                        onTap: () {
                                          controller.listCheckBox[index] = false;
                                          controller.listId.remove(controller.listStudentCopy[index].id!);
                                          if(controller.listCheckBox.contains(false) == true){
                                            controller.isCheckAll.value = false;
                                          }else{
                                            controller.isCheckAll.value = true;
                                          }

                                          for(int i = 0 ;i<Get.find<ListAllUser>().listAll.length;i++){
                                            if(Get.find<ListAllUser>().listAll[i].id !=controller.listStudent[index].id!){
                                            }else{
                                              Get.find<ListAllUser>().listCheckBox[i] = false;
                                              Get.find<ListAllUser>().listId.remove(Get.find<ListAllUser>().listAll[i].id!);
                                            }
                                          }
                                          if(Get.find<ListAllUser>().listCheckBox.contains(false) == true){
                                            Get.find<ListAllUser>().isCheckAll.value = false;
                                          }else{
                                            Get.find<ListAllUser>().isCheckAll.value = true;
                                          }
                                        },
                                        child: Container(
                                          padding: EdgeInsets.zero,
                                          decoration: const BoxDecoration(
                                            color: Colors.white,
                                            shape: BoxShape.circle,
                                          ),
                                          child: SvgPicture.asset("assets/images/icon_dangerous.svg",fit: BoxFit.cover,height: 18.h,width: 18.h,),
                                        ),
                                      ))
                                ],
                              ) ,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 8.h),
                              child:  Text("${controller.listStudentCopy[index].fullName}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.w400,fontSize: 14),),
                            )
                          ],
                        ),
                      )
                          :Container();
                    }),
              ))
        ],
      ),
    ));
  }

}