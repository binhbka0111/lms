import 'package:get/get.dart';

import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../../data/repository/contact/contact_repo.dart';
import '../all_user/all_user_controller.dart';
import '../list_all_user_by_class_send_notification_controller.dart';

class ListStudentController extends GetxController{
  var clickAll = <bool>[].obs;
  var listCheckBox = <bool>[].obs;
  var isCheckAll = false.obs;
  final ContactRepo _contactRepo = ContactRepo();
  var contacts = Contacts().obs;
  RxList<ItemContact> listStudent = <ItemContact>[].obs;
  RxList<ItemContact> listStudentCopy = <ItemContact>[].obs;
  var listId = <String>[];
  @override
  void onInit() {
    getStudentClassContact();
    super.onInit();
  }


  getStudentClassContact() async {
    _contactRepo.getClassContact(Get.find<ListAllUserByClassSendNotificationController>().classId.value, "STUDENT","").then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        listStudent.value = contacts.value.item!;
        listStudentCopy.value = contacts.value.item!;
        for (int i = 0; i < listStudent.length; i++) {
          listCheckBox.add(false);
        }
      }
    });
  }


  checkBoxListView(index) {
    if (listCheckBox[index] == false) {
      listCheckBox[index] = true;
      if(listId.contains(listStudent[index].id!)== true){
      }else{
        listId.add(listStudent[index].id!);
      }

      for(int i = 0 ;i<Get.find<ListAllUser>().listAll.length;i++){
        if(Get.find<ListAllUser>().listAll[i].id !=listStudent[index].id!){
        }else{
          Get.find<ListAllUser>().listId.add(Get.find<ListAllUser>().listAll[i].id!);
          Get.find<ListAllUser>().listCheckBox[i] = true;
        }
      }


    } else {
      listCheckBox[index] = false;
      listId.remove(listStudent[index].id!);

      for(int i = 0 ;i<Get.find<ListAllUser>().listAll.length;i++){
        if(Get.find<ListAllUser>().listAll[i].id !=listStudent[index].id!){
        }else{
          Get.find<ListAllUser>().listCheckBox[i] = false;
          Get.find<ListAllUser>().listId.remove(Get.find<ListAllUser>().listAll[i].id!);
        }
      }
    }
    if(Get.find<ListAllUser>().listCheckBox.contains(false) == true){
      Get.find<ListAllUser>().isCheckAll.value = false;
    }else{
      Get.find<ListAllUser>().isCheckAll.value = true;
    }

  }


  checkAll() {
    if (isCheckAll.value == true) {
      listCheckBox.value = [];
      for (int i = 0; i < listStudent.length; i++){
        if(listId.contains(listStudent[i].id!) == true){
        }else{
          listId.add(listStudent[i].id!);
        }
      }

      for(int i = 0 ;i < listStudent.length;i++){
        if(Get.find<ListAllUser>().listId.contains(listStudent[i].id) == false){
          Get.find<ListAllUser>().listId.add(listStudent[i].id!);
        }
      }

      for (int i = 0; i < Get.find<ListAllUser>().listAll.length; i++) {
        if(Get.find<ListAllUser>().listId.contains(Get.find<ListAllUser>().listAll[i].id) == true){
          Get.find<ListAllUser>().listCheckBox[i] = true;
        }else{
          Get.find<ListAllUser>().listCheckBox[i] = false;
        }
      }
      for (int i = 0; i < listId.length; i++) {
        listCheckBox.add(true);
      }
    } else {
      listCheckBox.value = [];
      for (int i = 0; i < listStudent.length; i++) {
        listCheckBox.add(false);
        if(listId.contains(listStudent[i].id!) == true){
          listId.remove(listStudent[i].id!);
        }
      }

      for (int i = 0; i < listStudent.length; i++) {
        if(Get.find<ListAllUser>().listId.contains(listStudent[i].id!) == true){
          Get.find<ListAllUser>().listId.remove(listStudent[i].id!);
        }
      }
      for (int i = 0; i < Get.find<ListAllUser>().listAll.length; i++) {
        if(Get.find<ListAllUser>().listId.contains(Get.find<ListAllUser>().listAll[i].id) == true){
          Get.find<ListAllUser>().listCheckBox[i] = true;
        }else{
          Get.find<ListAllUser>().listCheckBox[i] = false;
        }
      }
    }
    if(Get.find<ListAllUser>().listCheckBox.contains(false) == true){
      Get.find<ListAllUser>().isCheckAll.value = false;
    }else{
      Get.find<ListAllUser>().isCheckAll.value = true;
    }

  }





}