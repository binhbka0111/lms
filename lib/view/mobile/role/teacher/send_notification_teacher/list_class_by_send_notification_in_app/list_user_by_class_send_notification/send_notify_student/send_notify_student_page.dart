import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../routes/app_pages.dart';
import '../../list_class_by_send_notification_in_app_controller.dart';
import 'send_notify_student_controller.dart';
import 'package:tiengviet/tiengviet.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';

class SendNotifyStudentPage extends GetView<SendNotifyStudentController>{
  SendNotifyStudentPage({super.key});
  @override
  final controller = Get.put(SendNotifyStudentController());

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
            onTap: (){
              FocusManager.instance.primaryFocus?.unfocus();
            },


            child: Scaffold(
              resizeToAvoidBottomInset: false,
              appBar: AppBar(
                backgroundColor: ColorUtils.PRIMARY_COLOR,
                elevation: 0,
                title: Text(
                  Get.find<ListClasSendNotificationController>().typeSendNotify.value == "SMS"?"Gửi thông báo SMS":"Gửi Thông Báo Trên Ứng Dụng",
                  style: TextStyle(
                      color: Colors.white, fontSize: 16.sp, fontWeight: FontWeight.w500),
                ),
                actions: [
                  InkWell(
                    onTap: () {
                      comeToHome();
                    },
                    child: const Icon(
                      Icons.home,
                      color: Colors.white,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(right: 16.w))
                ],
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 8.h),
                    child: Row(
                      children: [
                        Expanded(
                          child: SizedBox(
                            height: 40.h,
                            child: TextFormField(
                              onChanged: (value) {
                                controller.listStudent.value = [];
                                controller.listCheckBox.value = [];
                                controller.listStudent.value = controller.listStudentCopy.where((element) => TiengViet.parse(element.fullName!.toLowerCase().trim()).contains(TiengViet.parse(value.toLowerCase())) == true).toList();
                                for(int i= 0; i <controller.listStudent.length; i++){
                                    controller.listCheckBox.add(false);
                                }
                                for(int j = 0; j <controller.listStudent.length; j++){
                                  if(controller.listId.contains(controller.listStudent[j].id) ){
                                    controller.listCheckBox[j] = true;
                                    controller.selectedQuantity.value++;
                                  }else{
                                    controller.listCheckBox[j] = false;
                                  }
                                }
                                if(controller.listCheckBox.isNotEmpty){
                                  if(controller.listCheckBox.contains(false) == true){
                                    controller.isCheckAll.value = false;
                                  }else{
                                    controller.isCheckAll.value = true;
                                  }
                                }

                                controller.listStudent.refresh();
                                controller.listCheckBox.refresh();
                              },
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Color.fromRGBO(177, 177, 177, 1)),
                                      borderRadius: BorderRadius.circular(6)),
                                  prefixIcon: const Icon(
                                    Icons.search,
                                    color: Color.fromRGBO(177, 177, 177, 1),
                                  ),
                                  hintText: "Tìm kiếm",
                                  isCollapsed: true,
                                  hintStyle: TextStyle(
                                      fontSize: 14.sp,
                                      color: const Color.fromRGBO(177, 177, 177, 1)),
                                  focusedBorder: const OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Color.fromRGBO(248, 129, 37, 1)))),
                              textAlignVertical: TextAlignVertical.center,
                              cursorColor: const Color.fromRGBO(248, 129, 37, 1),
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(left: 8.w)),
                      ],
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 16.h)),
                  Expanded(
                      child: Obx(() => Container(
                        decoration: BoxDecoration(
                            color: Colors.white, borderRadius: BorderRadius.circular(6)),
                        padding: const EdgeInsets.all(16),
                        margin:  EdgeInsets.fromLTRB(16.w, 0, 16.w, 16.h),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                InkWell(
                                  onTap: () {
                                    controller.isCheckAll.value = !controller.isCheckAll.value;
                                    controller.checkAll();
                                  },
                                  child: Row(
                                    children: [
                                      Container(
                                          width: 18.h,
                                          height: 18.h,
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.all(2.h),
                                          margin: const EdgeInsets.only(right: 8),
                                          decoration: ShapeDecoration(
                                            shape: CircleBorder(
                                                side: BorderSide(color: controller.isCheckAll.value ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(235, 235, 235, 1))),
                                          ),
                                          child: controller.isCheckAll.value
                                              ? const Icon(Icons.circle, size: 12,
                                            color:ColorUtils.PRIMARY_COLOR,
                                          )
                                              : null),
                                      Padding(padding: EdgeInsets.only(left: 8.w)),
                                      Text(
                                        "Chọn hết",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.black),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(child: Container()),
                                Text(
                                  "Đã chọn ${controller.selectedQuantity.value} /${controller.listStudentCopy.length}",
                                  style: TextStyle(
                                      color: const Color.fromRGBO(177, 177, 177, 1),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500),
                                )
                              ],
                            ),
                            Padding(padding: EdgeInsets.only(top: 16.h)),
                            Expanded(
                              child: ListView.builder(
                                  itemCount: controller.listStudent.length,
                                  shrinkWrap: true,
                                  physics: const ScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                      onTap: () {
                                        controller.checkBoxListView(index);
                                        if(controller.listCheckBox.contains(false) == true){
                                          controller.isCheckAll.value = false;
                                        }else{
                                          controller.isCheckAll.value = true;
                                        }

                                      },
                                      child: Column(
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                  width: 18.h,
                                                  height: 18.h,
                                                  alignment: Alignment.center,
                                                  padding: EdgeInsets.all(2.h),
                                                  margin: const EdgeInsets.only(right: 8),
                                                  decoration: ShapeDecoration(
                                                    shape: CircleBorder(
                                                        side: BorderSide(color: controller.listCheckBox[index] ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(235, 235, 235, 1))),
                                                  ),
                                                  child: controller.listCheckBox[index]
                                                      ? const Icon(Icons.circle, size: 12,
                                                    color:ColorUtils.PRIMARY_COLOR,
                                                  )
                                                      : null),
                                              Padding(padding: EdgeInsets.only(left: 8.w)),
                                              SizedBox(
                                                width: 32.h,
                                                height: 32.h,
                                                child:
                                                CacheNetWorkCustom(urlImage:  '${  controller.listStudent[index].image }',),

                                              ),
                                              Padding(padding: EdgeInsets.only(left: 8.w)),
                                              Expanded(child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "${controller.listStudent[index].fullName}",
                                                    style: TextStyle(
                                                        fontSize: 16.sp,
                                                        fontWeight: FontWeight.w500,
                                                        color: Colors.black),
                                                  ),
                                                  controller.listStudent[index].parent!.isNotEmpty?
                                                  RichText(
                                                    textAlign: TextAlign.left,
                                                    text: TextSpan(
                                                      text: 'Phụ huynh: ',
                                                      style: TextStyle(
                                                          color: const Color.fromRGBO(133, 133, 133, 1),
                                                          fontSize: 12.sp,
                                                          fontWeight: FontWeight.w500,
                                                          fontFamily: 'static/Inter-Medium.ttf'),

                                                      children: controller.listStudent[index].parent?.map((e) {
                                                        var indexName = controller.listStudent[index].parent?.indexOf(e);
                                                        var showSplit = ", ";
                                                        if (indexName == controller.listStudent[index].parent!.length - 1) {
                                                          showSplit = "";
                                                        }
                                                        return TextSpan(
                                                            text: "${e.fullName}$showSplit",
                                                            style: TextStyle(
                                                                color: Colors.black,
                                                                fontSize: 12.sp,
                                                                fontWeight: FontWeight.w500,
                                                                fontFamily: 'static/Inter-Medium.ttf'));
                                                      }).toList(),
                                                    ),
                                                  ):Container()
                                                ],
                                              ))
                                            ],
                                          ),
                                          const Divider(),
                                        ],
                                      ),
                                    );
                                  }),
                            ),
                            Visibility(
                                visible: controller.listId.isNotEmpty,
                                child: Container(
                                  margin: EdgeInsets.only(top: 8.h),
                                  decoration: BoxDecoration(
                                    border: Border.all(color: const Color.fromRGBO(239, 239, 239, 1),),
                                  ),
                                  height: 86.h,
                                  child: ListView.builder(
                                      itemCount: controller.listStudentCopy.length,
                                      physics: const ScrollPhysics(),
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context,index){
                                        return controller.listId.contains(controller.listStudentCopy[index].id)
                                            ?Container(
                                          margin: EdgeInsets.symmetric(horizontal: 8.w),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Container(
                                                color: Colors.white,
                                                child: Stack(
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.all(8.h),
                                                      width: 32.h,
                                                      height: 32.h,
                                                      child:
                                                      CacheNetWorkCustom(urlImage: '${ controller.listStudentCopy[index].image }',)

                                                    ),
                                                    Positioned(
                                                        top: 4,
                                                        right: 0,
                                                        child: InkWell(
                                                          onTap: () {
                                                            controller.listCheckBox[index] = false;
                                                            controller.selectedQuantity--;
                                                            controller.listId.remove(controller.listStudentCopy[index].id!);
                                                            if(controller.listCheckBox.contains(false) == true){
                                                              controller.isCheckAll.value = false;
                                                            }else{
                                                              controller.isCheckAll.value = true;
                                                            }
                                                          },
                                                          child: Container(
                                                            padding: EdgeInsets.zero,
                                                            decoration: const BoxDecoration(
                                                              color: Colors.white,
                                                              shape: BoxShape.circle,
                                                            ),
                                                            child: SvgPicture.asset("assets/images/icon_dangerous.svg",fit: BoxFit.cover,height: 18.h,width: 18.h,),
                                                          ),
                                                        ))
                                                  ],
                                                ) ,
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(top: 8.h),
                                                child:  Text("${controller.listStudentCopy[index].fullName}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.w400,fontSize: 14),),
                                              )
                                            ],
                                          ),
                                        )
                                            :Container();
                                      }),
                                ))
                          ],
                        ),
                      ))),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.all(16),
                    color: Colors.white,
                    child: SizedBox(
                      height: 46.h,
                      child: ElevatedButton(
                        onPressed: () {
                          var listUser = <ItemContact>[];
                          var listUserNotSendNotifi = <ItemContact>[];
                          for(int i = 0;i<controller.listStudentCopy.length;i++){
                            if(controller.listId.contains(controller.listStudentCopy[i].id!)){
                              listUser.add(controller.listStudentCopy[i]);
                            }else{
                              listUserNotSendNotifi.add(controller.listStudentCopy[i]);
                            }
                          }
                          if(listUser.isEmpty){
                            AppUtils.shared.showToast("Vui lòng chọn người để gửi");
                          }else{
                            if(Get.find<ListClasSendNotificationController>().typeSendNotify.value == "SMS"){
                              Get.toNamed(Routes.detailSendSMSNotification,arguments: [listUser,listUserNotSendNotifi,controller.contacts.value.id]);
                            }
                            if(Get.find<ListClasSendNotificationController>().typeSendNotify.value == "INAPP"){
                              Get.toNamed(Routes.detailSendInAppNotification,arguments: [listUser,listUserNotSendNotifi,controller.contacts.value.id]);
                            }
                          }
                          
                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor: ColorUtils.PRIMARY_COLOR,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6))),
                        child: Text("Tiếp Tục",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16.sp,
                                fontWeight: FontWeight.w400)),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
  }

}