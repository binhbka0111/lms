import 'package:get/get.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../../data/repository/contact/contact_repo.dart';

class SendNotifyTeacherController extends GetxController{
  var selectedQuantity = 0.obs;
  var clickAll = <bool>[].obs;
  var listCheckBox = <bool>[].obs;
  var isCheckAll = false.obs;
  final ContactRepo _contactRepo = ContactRepo();
  var contacts = Contacts().obs;
  RxList<ItemContact> listTeacher = <ItemContact>[].obs;
  RxList<ItemContact> listTeacherCopy = <ItemContact>[].obs;
  var students = <Student>[].obs;
  var classId = "".obs;
  var typeInList = "".obs;
  var listId = <String>[];

  @override
  void onInit() {
    var tmpClassId = Get.arguments;
    if(tmpClassId!=null){
      classId.value = tmpClassId;
    }
    getStudentClassContact();
    super.onInit();
  }


  getStudentClassContact() async {
    _contactRepo.getClassContact(classId, "TEACHER","").then((value) {
      if (value.state == Status.SUCCESS) {
        contacts.value = value.object!;
        listTeacher.value = contacts.value.item!;
        listTeacherCopy.value = contacts.value.item!;
        for (int i = 0; i < listTeacher.length; i++) {
          listCheckBox.add(false);
        }
      }
    });
  }


  checkBoxListView(index) {
    if (listCheckBox[index] == false) {
      listCheckBox[index] = true;
      selectedQuantity++;
      if(listId.contains(listTeacher[index].id!)== true){

      }else{
        listId.add(listTeacher[index].id!);
      }

    } else {
      listCheckBox[index] = false;
      selectedQuantity--;
      listId.remove(listTeacher[index].id!);
    }
  }


  checkAll() {
    if (isCheckAll.value == true) {
      listCheckBox.value = [];
      for (int i = 0; i < listTeacher.length; i++){
        if(listId.contains(listTeacher[i].id!) == true){
        }else{
          listId.add(listTeacher[i].id!);
        }
      }
      for (int i = 0; i < listId.length; i++) {
        listCheckBox.add(true);
      }
      selectedQuantity.value = listTeacher.length;
    } else {
      listCheckBox.value = [];
      for (int i = 0; i < listTeacher.length; i++) {
        listCheckBox.add(false);
        if(listId.contains(listTeacher[i].id!) == true){
          listId.remove(listTeacher[i].id!);
        }
      }
      selectedQuantity.value = 0;
    }
  }


  getTextHomeRoomTeacher(isHomeRoom, index) {
    var split = "";
    switch (isHomeRoom) {
      case "":
        return "";
      case null:
        return "";
      case "Giáo viên chủ nhiệm":
        if (listTeacher[index].subject!.isEmpty) {
          split = "";
        } else {
          split = ",";
        }
        return "Giáo viên chủ nhiệm$split ";
      default:
        return "";
    }
  }




}