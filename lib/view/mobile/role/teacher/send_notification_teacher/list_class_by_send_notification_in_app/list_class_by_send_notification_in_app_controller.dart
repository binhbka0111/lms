import 'package:get/get.dart';
import '../../../../../../commom/app_cache.dart';
import '../../../../../../data/base_service/api_response.dart';
import '../../../../../../data/model/res/class/classTeacher.dart';
import '../../../../../../data/repository/subject/class_office_repo.dart';



class ListClasSendNotificationController extends GetxController{
  final ClassOfficeRepo _classRepo = ClassOfficeRepo();
  RxList<ClassId> classOfTeacher = <ClassId>[].obs;
  var role = "".obs;
  var typeSendNotify = "".obs;


  @override
  void onInit() {
    super.onInit();
    getListClassIdByTeacher();
    var tmpRole = Get.arguments[0];
    var tmpType = Get.arguments[1];
    if(tmpRole!=null){
      role.value = tmpRole;
    }
    if(tmpType!=null){
      typeSendNotify.value = tmpType;
    }
  }



  getListClassIdByTeacher() {
    var teacherId = AppCache().userId;
    _classRepo.listClassByTeacher(teacherId).then((value) {
      if (value.state == Status.SUCCESS) {
        classOfTeacher.value = value.object!;
        for (var f in classOfTeacher) {
          f.checked = false;
        }
        classOfTeacher.first.checked = true;
        classOfTeacher.refresh();
      }
    });
  }

}