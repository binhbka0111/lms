import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import '../../../../../../routes/app_pages.dart';
import 'list_class_by_send_notification_in_app_controller.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';


class ListClassSendNotificationPage extends GetWidget<ListClasSendNotificationController>{
  @override
  final controller = Get.put(ListClasSendNotificationController());

  ListClassSendNotificationPage({super.key});
  @override
  Widget build(BuildContext context) {
  return Obx(() => Scaffold(
      appBar: AppBar(
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        elevation: 0,
        title: Text(
          controller.typeSendNotify.value == "SMS"?"Gửi thông báo SMS":"Gửi Thông Báo Trên Ứng Dụng",
          style: TextStyle(
              color: Colors.white, fontSize: 16.sp, fontWeight: FontWeight.w500),
        ),
        actions: [
          InkWell(
            onTap: () {
              comeToHome();
            },
            child: const Icon(
              Icons.home,
              color: Colors.white,
            ),
          ),
          Padding(padding: EdgeInsets.only(right: 16.w))
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Danh sách lớp nhận thông báo",
              style: TextStyle(
                  color: const Color.fromRGBO(177, 177, 177, 1),
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w500),
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Expanded(
                child: ListView.builder(
                    itemCount: controller.classOfTeacher.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          switch(controller.role.value){
                            case "ALL":
                              Get.toNamed(Routes.listUserByClassSendNotificationPage,arguments: controller.classOfTeacher[index].id);
                              break;
                            case "STUDENT":
                              Get.toNamed(Routes.sendNotifyStudentPage,arguments: controller.classOfTeacher[index].id);
                              break;
                            case "PARENT":
                              Get.toNamed(Routes.sendNotifyParentPage,arguments: controller.classOfTeacher[index].id);
                              break;
                            case "TEACHER":
                              Get.toNamed(Routes.sendNotifyTeacherPage,arguments: controller.classOfTeacher[index].id);
                              break;
                          }

                        },
                        child: Container(
                          margin: EdgeInsets.only(top: 12.h),
                          padding: EdgeInsets.zero,
                          child: Card(
                            margin: EdgeInsets.zero,
                            elevation: 1,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)),
                            child: Container(
                              padding: const EdgeInsets.all(16),
                              child: Row(
                                children: [
                                  Text(
                                    "${controller.classOfTeacher[index].name}",
                                    style: TextStyle(
                                        fontSize: 14.sp,
                                        color: const Color.fromRGBO(26, 26, 26, 1),
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Padding(padding: EdgeInsets.only(left: 8.w)),
                                  Visibility(
                                      visible:  (controller.classOfTeacher[index].homeroomClass == true),
                                      child: const Icon(
                                        Icons.star,
                                        color: ColorUtils.PRIMARY_COLOR,
                                        size: 18,
                                      )),
                                  Expanded(child: Container()),
                                  const Icon(
                                    Icons.arrow_forward_ios,
                                    color: ColorUtils.PRIMARY_COLOR,
                                    size: 18,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    }
                )
            )
          ],
        ),
      )
  ));
  }
}