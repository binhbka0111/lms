import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/global.dart';
import '../../../../../../../../commom/widget/text_field_custom.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../../../data/model/common/contacts.dart';
import 'detail_send_notify_sms_controller.dart';
import 'dart:io' show Platform;
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';


class DetailSendSMSNotification extends GetWidget<DetailSendNotificationSMSController> {
  @override
  final controller = Get.put(DetailSendNotificationSMSController());

  DetailSendSMSNotification({super.key});
  bottomSheetAddUser() {
    return GetBuilder<DetailSendNotificationSMSController>(builder: (controller) {
      return  Wrap(children: [Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(32.r),
              topLeft: Radius.circular(32.r)),
          color: Colors.white,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.r),
                  color: const Color.fromRGBO(210, 212, 216, 1)),
              height: 6.h,
              width: 48.w,
              alignment: Alignment.topCenter,
              margin: EdgeInsets.only(top: 16.h),
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Visibility(
                visible: controller.listUserNotSendNotify.isNotEmpty,
                child: Container(
                  margin: EdgeInsets.only(left: 8.w),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {
                          clickCheckboxAllAdd();
                        },
                        child: Row(
                          children: [
                            Container(
                                alignment: Alignment.center,
                                decoration: ShapeDecoration(
                                  shape: CircleBorder(
                                      side: BorderSide(
                                          color:
                                          controller.isCheckAllAdd.value
                                              ? const Color.fromRGBO(
                                              248, 129, 37, 1)
                                              : const Color.fromRGBO(
                                              235, 235, 235, 1))),
                                ),
                                child: controller.isCheckAllAdd.value
                                    ?  Container(
                                    margin: const EdgeInsets.all(4),
                                    child: Icon(
                                      Icons.circle,
                                      size: ScreenUtil().setSp(16),
                                      color: ColorUtils.PRIMARY_COLOR,
                                    ))
                                    : Container(
                                    margin: const EdgeInsets.all(4),
                                    child: Icon(
                                      Icons.circle,
                                      size: ScreenUtil().setSp(16),
                                      color: Colors.transparent,
                                    ))),
                            Padding(padding: EdgeInsets.only(left: 8.w)),
                            Text(
                              "Chọn hết",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black),
                            ),
                          ],
                        ),
                      ),
                      Expanded(child: Container()),
                      TextButton(
                          onPressed: () {
                            Get.back();
                            cancelAddUser();
                          },
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white),
                          child: Text(
                            "Hủy",
                            style: TextStyle(
                                fontSize: 16.sp,
                                color: const Color.fromRGBO(123, 123, 123, 1)),
                          )),
                      TextButton(
                          onPressed: () {
                            addListUser();
                          },
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white),
                          child:  Text(
                            "Xong",
                            style: TextStyle(
                                fontSize: 16.sp,
                                color: ColorUtils.PRIMARY_COLOR),
                          )),
                    ],
                  ),
                )),
            SingleChildScrollView(
              child: Container(
                height: 300.h,
                margin: EdgeInsets.symmetric( horizontal: 8.w),
                child:
                GetBuilder<DetailSendNotificationSMSController>(
                  builder: (controller) {
                    return ListView.builder(
                        itemCount:
                        controller.listUserNotSendNotify.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              clickCheckboxAdd(index);
                            },
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Container(
                                        alignment: Alignment.center,
                                        decoration: ShapeDecoration(
                                          shape: CircleBorder(
                                              side: BorderSide(
                                                  color: controller
                                                      .listCheckBoxAdd[index]
                                                      ? ColorUtils.PRIMARY_COLOR
                                                      : const Color.fromRGBO(
                                                      235, 235, 235, 1))),
                                        ),
                                        child: controller.listCheckBoxAdd[index]
                                            ?  Container(
                                            margin: const EdgeInsets.all(4),
                                            child: Icon(
                                              Icons.circle,
                                              size: ScreenUtil().setSp(16),
                                              color: ColorUtils.PRIMARY_COLOR,
                                            ))
                                            : Container(
                                            margin: const EdgeInsets.all(4),
                                            child: Icon(
                                              Icons.circle,
                                              size: ScreenUtil().setSp(16),
                                              color: Colors.transparent,
                                            ))),
                                    Padding(
                                        padding: EdgeInsets.only(left: 8.w)),
                                    SizedBox(
                                      width: 32.w,
                                      height: 32.w,
                                      child:
                                      CacheNetWorkCustom(urlImage:  '${controller.listUserNotSendNotify
                                      [index].image }',),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(left: 8.w)),
                                    Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${controller.listUserNotSendNotify[index].fullName}",
                                          style: TextStyle(
                                              fontSize: 16.sp,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.black),
                                        ),
                                        SizedBox(
                                          width: 250.w,
                                          child: getRole(
                                              controller
                                                  .listUserNotSendNotify,
                                              controller.getListAddRole(
                                                  controller
                                                      .listUserNotSendNotify[index]
                                                      .type,
                                                  index),
                                              controller.listUserNotSendNotify[index].type,
                                              index),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                                const Divider(),
                              ],
                            ),
                          );
                        });
                  },
                ),
              ),
            ),
          ],
        ),
      )],);
    },);
  }

  bottomSheetMinusUser() {
    return GetBuilder<DetailSendNotificationSMSController>(builder: (controller) {
      return Wrap(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(32.r),
                  topLeft: Radius.circular(32.r)),
              color: Colors.white,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.r),
                      color: const Color.fromRGBO(210, 212, 216, 1)),
                  height: 6.h,
                  width: 48.w,
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.only(top: 16.h),
                ),
                Padding(padding: EdgeInsets.only(top: 16.h)),
                Visibility(
                    visible: controller.listUser.isNotEmpty,
                    child: Container(
                      margin: EdgeInsets.only(left: 8.w),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              clickCheckboxAllMinus();
                            },
                            child: Row(
                              children: [
                                Container(
                                    alignment: Alignment.center,
                                    decoration: ShapeDecoration(
                                      shape: CircleBorder(
                                          side: BorderSide(
                                              color: controller
                                                  .isCheckAllMinus.value
                                                  ? ColorUtils.PRIMARY_COLOR
                                                  : const Color.fromRGBO(
                                                  235, 235, 235, 1))),
                                    ),
                                    child: controller.isCheckAllMinus.value
                                        ?  Container(
                                        margin: const EdgeInsets.all(4),
                                        child: Icon(
                                          Icons.circle,
                                          size: ScreenUtil().setSp(16),
                                          color: ColorUtils.PRIMARY_COLOR,
                                        ))
                                        : Container(
                                        margin: const EdgeInsets.all(4),
                                        child: Icon(
                                          Icons.circle,
                                          size: ScreenUtil().setSp(16),
                                          color: Colors.transparent,
                                        ))),
                                Padding(padding: EdgeInsets.only(left: 8.w)),
                                Text(
                                  "Chọn hết",
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black),
                                ),
                              ],
                            ),
                          ),
                          Expanded(child: Container()),
                          TextButton(
                              onPressed: () {
                                Get.back();
                                cancelMinusUser();
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.white),
                              child:  Text(
                                "Hủy",
                                style: TextStyle(
                                    color: const Color.fromRGBO(123, 123, 123, 1),
                                    fontSize: 16.sp
                                ),
                              )),
                          TextButton(
                              onPressed: () {
                                minusListUser();

                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.white),
                              child: Text(
                                "Xong",
                                style: TextStyle(
                                    color: ColorUtils.PRIMARY_COLOR,
                                    fontSize: 16.sp
                                ),
                              )),
                        ],
                      ),
                    )),
                SingleChildScrollView(
                  child: Container(
                    height: 300.h,
                    margin: EdgeInsets.symmetric( horizontal: 8.w),
                    child:
                    GetBuilder<DetailSendNotificationSMSController>(
                      builder: (controller) {
                        return ListView.builder(
                            itemCount: controller.listUser.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  clickCheckboxMinus(index);
                                },
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                            alignment: Alignment.center,
                                            decoration: ShapeDecoration(
                                              shape: CircleBorder(
                                                  side: BorderSide(
                                                      color: controller
                                                          .listCheckBoxMinus[index]
                                                          ? ColorUtils.PRIMARY_COLOR
                                                          : const Color.fromRGBO(
                                                          235, 235, 235, 1))),
                                            ),
                                            child: controller.listCheckBoxMinus[index]
                                                ?  Container(
                                                margin: const EdgeInsets.all(4),
                                                child: Icon(
                                                  Icons.circle,
                                                  size: ScreenUtil().setSp(16),
                                                  color: ColorUtils.PRIMARY_COLOR,
                                                ))
                                                : Container(
                                                margin: const EdgeInsets.all(4),
                                                child: Icon(
                                                  Icons.circle,
                                                  size: ScreenUtil().setSp(16),
                                                  color: Colors.transparent,
                                                ))),
                                        Padding(
                                            padding: EdgeInsets.only(left: 8.w)),
                                        SizedBox(
                                            width: 32.w,
                                            height: 32.w,
                                            child:

                                            CacheNetWorkCustom(urlImage: '${ controller.listUser[index].image}',)

                                        ),
                                        Padding(
                                            padding: EdgeInsets.only(left: 8.w)),
                                        Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "${controller.listUser[index].fullName}",
                                              style: TextStyle(
                                                  fontSize: 16.sp,
                                                  fontWeight: FontWeight.w500,
                                                  color: Colors.black),
                                            ),
                                            SizedBox(
                                              width: 250.w,
                                              child: getRole(
                                                  controller.listUser,
                                                  controller.getListMinusRole(
                                                      controller.listUser
                                                      [index].type,
                                                      index),
                                                  controller
                                                      .listUser[index].type,
                                                  index),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                    const Divider(),
                                  ],
                                ),
                              );
                            });
                      },
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      );
    },);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        dismissKeyboard();
        cancelAddUser();
        cancelMinusUser();
      },
      child: Obx(() => SafeArea(
          child: Scaffold(
              appBar: AppBar(
                backgroundColor: ColorUtils.PRIMARY_COLOR,
                elevation: 0,
                title: Text(
                  "Gửi Thông Báo SMS",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.w500),
                ),
                actions: [
                  InkWell(
                    onTap: () {
                      comeToHome();
                    },
                    child: const Icon(
                      Icons.home,
                      color: Colors.white,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(right: 16.w))
                ],
              ),
              body: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(padding: EdgeInsets.only(top: 16.h)),
                              Container(
                                margin: EdgeInsets.only(left: 16.w),
                                child: Text(
                                  "Thông Báo",
                                  style: TextStyle(
                                      color: const Color.fromRGBO(177, 177, 177, 1),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(top: 8.h)),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 12.w),
                                child: MyOutlineBorderTextFormFieldNhat(
                                  enable: true,
                                  focusNode:
                                  controller.focusContentNotification.value,
                                  iconPrefix: null,
                                  iconSuffix: "assets/images/icon_resize.svg",
                                  state: StateType.DEFAULT,
                                  labelText: "Nội Dung Thông Báo",
                                  autofocus: false,
                                  controller: controller
                                      .controllerContentNotification.value,
                                  helperText: "",
                                  showHelperText: false,
                                  textInputAction: TextInputAction.next,
                                  ishowIconPrefix: false,
                                  keyboardType: TextInputType.text,
                                ),
                              ),
                              Padding(padding: EdgeInsets.only(top: 16.h)),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 16.w),
                                child: Row(
                                  children: [
                                    Text(
                                      "Danh Sách Người Nhận",
                                      style: TextStyle(
                                          color: const Color.fromRGBO(
                                              177, 177, 177, 1),
                                          fontSize: 12.sp,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Expanded(child: Container()),
                                    InkWell(
                                      onTap: () {
                                        controller.addListCheckBox();
                                        if (controller.listCheckBoxMinus
                                            .contains(false) ==
                                            true) {
                                          controller.isCheckAllMinus.value = false;
                                        } else {
                                          controller.isCheckAllMinus.value = true;
                                        }
                                        Get.bottomSheet(bottomSheetMinusUser());
                                      },
                                      child: Card(
                                        shape: const CircleBorder(),
                                        elevation: 1,
                                        child: Container(
                                          padding: EdgeInsets.only(bottom: 12.h),
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white,
                                          ),
                                          child: const Icon(
                                            Icons.minimize,
                                            color: ColorUtils.PRIMARY_COLOR,
                                          ),
                                        ),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        controller.isCheckAllAdd.value = false;
                                        controller.addListCheckBox();
                                        Get.bottomSheet(bottomSheetAddUser());
                                      },
                                      child: Card(
                                        shape: const CircleBorder(),
                                        elevation: 1,
                                        child: Container(
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white,
                                          ),
                                          child: const Icon(
                                            Icons.add_rounded,
                                            color: ColorUtils.PRIMARY_COLOR,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 16.w),
                                child: ListView.builder(
                                    itemCount: controller.listUser.length,
                                    shrinkWrap: true,
                                    physics: const ScrollPhysics(),
                                    itemBuilder: (context, index) {
                                      controller.student.value =
                                      controller.listUser[index].student!;
                                      controller.parent.value =
                                      controller.listUser[index].parent!;
                                      controller.typeInList.value =
                                      controller.listUser[index].type!;
                                      return InkWell(
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Padding(
                                                    padding:
                                                    EdgeInsets.only(left: 8.w)),
                                                SizedBox(
                                                    width: 32.w,
                                                    height: 32.w,
                                                    child:
                                                    CacheNetWorkCustom(urlImage: '${ controller.listUser[index].image  }',)

                                                ),
                                                Padding(
                                                    padding:
                                                    EdgeInsets.only(left: 8.w)),
                                                Column(
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      "${controller.listUser[index].fullName}",
                                                      style: TextStyle(
                                                          fontSize: 16.sp,
                                                          fontWeight:
                                                          FontWeight.w500,
                                                          color: Colors.black),
                                                    ),

                                                    SizedBox(
                                                      height: 4.h,
                                                    ),
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Text(
                                                            'SĐT: ',
                                                            style: TextStyle(
                                                                color: const Color.fromRGBO(133, 133, 133, 1),
                                                                fontSize: 12.sp,
                                                                fontWeight: FontWeight.w500,
                                                                fontFamily: 'static/Inter-Medium.ttf')),
                                                        Text(
                                                            controller.listUser[index].phone??"",
                                                            style: TextStyle(
                                                                color: Colors.black,
                                                                fontSize: 12.sp,
                                                                fontWeight: FontWeight.w500,
                                                                fontFamily: 'static/Inter-Medium.ttf'))
                                                      ],
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                            const Divider(),
                                          ],
                                        ),
                                      );
                                    }),
                              )
                            ],
                          ),
                        )),
                    SizedBox(
                      width: double.infinity,
                      height: 36.h,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: ColorUtils.PRIMARY_COLOR,
                          ),
                          onPressed: () {
                            if (controller
                                .controllerContentNotification.value.text
                                .trim() ==
                                "") {
                              AppUtils.shared.showToast(
                                  "Vui lòng nhập nội dung thông báo");
                            } else {
                              if (controller.listUser.isEmpty) {
                                AppUtils.shared.showToast(
                                    "Vui lòng chọn người để gửi thông báo");
                              } else {
                                var listPhoneUser = <String>[];

                                for (int i = 0; i < controller.listUser.length; i++) {
                                  if(controller.listUser[i].phone != ""&&controller.listUser[i].phone != null ){
                                    listPhoneUser.add(controller.listUser[i].phone!);
                                  }
                                }

                                if (Platform.isAndroid) {
                                  try{
                                    controller.checkPermissionSendSMS(controller.controllerContentNotification.value.text.trim(), listPhoneUser);
                                  }catch(e){
                                    print("lỗi");
                                  }
                                } else if (Platform.isIOS) {
                                  controller.sendingSMS(controller.controllerContentNotification.value.text.trim(),listPhoneUser);
                                }
                              }
                            }
                          },
                          child: const Text(
                            'Gửi',
                            style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                fontSize: 16),
                          )),
                    ),
                    SizedBox(height: 16.h,)
                  ],
                ),
              )))),
    );
  }


  clickCheckboxAdd( int index)  {
    controller.checkBoxListViewAdd(index);
    if (controller.listCheckBoxAdd.contains(false) == true) {
      controller.isCheckAllAdd.value = false;
    } else {
      controller.isCheckAllAdd.value = true;
    }
    controller.isCheckAllAdd.refresh();
    controller.update();
  }

  clickCheckboxMinus( int index)  {
    controller.checkBoxListViewMinus(index);
    if (controller.listCheckBoxMinus.contains(false) == true) {
      controller.isCheckAllMinus.value = false;
    } else {
      controller.isCheckAllMinus.value = true;
    }
    controller.isCheckAllMinus.refresh();
    controller.update();
  }

  clickCheckboxAllAdd() {
    controller.isCheckAllAdd.value = !controller.isCheckAllAdd.value;
    controller.checkAllAdd();
    controller.update();
  }
  cancelMinusUser() {
    controller.listCheckBoxMinus.value = [];
    for (int i = 0; i < controller.listUser.length; i++) {
      controller.listCheckBoxMinus.add(true);
    }
    controller.listCheckBoxMinus.refresh();
    controller.update();
  }


  cancelAddUser()  {
    controller.listCheckBoxAdd.value = [];
    for(int i = 0;i<controller.listUserNotSendNotify.length;i++){
      controller.listCheckBoxAdd.add(false);
    }
    controller.listCheckBoxAdd.refresh();
    controller.isCheckAllAdd.value = false;
    controller.update();
  }

  clickCheckboxAllMinus()  {
    controller.isCheckAllMinus.value = !controller.isCheckAllMinus.value;
    controller.checkAllMinus();
    controller.update();
  }

  addListUser()  {
    var listId = <String>[];
    for(int i = 0; i < controller.listUserNotSendNotify.length; i++){
      listId.add(controller.listUserNotSendNotify[i].id!);
    }
    for (int i = 0; i < controller.listUserNotSendNotify.length; i++) {
      if (controller.listCheckBoxAdd[i] == true) {
        controller.listUser.add(controller.listUserNotSendNotify[i]);
        listId.remove(controller.listUserNotSendNotify[i].id);
      }
    }
    controller.listUserNotSendNotify.value = [];
    for(int i = 0; i < controller.listAllUser.length; i++){
      if(listId.contains(controller.listAllUser[i].id) == true){
        controller.listUserNotSendNotify.add(controller.listAllUser[i]);
      }
    }
    controller.listUser.refresh();
    controller.listUserNotSendNotify.refresh();
    controller.listCheckBoxMinus.value = [];
    for (int i = 0; i < controller.listUser.length; i++) {
      controller.listCheckBoxMinus.add(true);
    }
    controller.listCheckBoxAdd.value = [];
    for (int i = 0; i < controller.listUserNotSendNotify.length; i++) {
      controller.listCheckBoxAdd.add(false);
    }
    controller.isCheckAllAdd.value = false;
    controller.listCheckBoxMinus.refresh();
    controller.listCheckBoxAdd.refresh();
    controller.update();
    Get.back();
  }

  minusListUser() {
    var listId = <String>[];
    for(int i = 0; i < controller.listUser.length; i++){
      listId.add(controller.listUser[i].id!);
    }
    for (int i = 0; i < controller.listUser.length; i++) {
      if (controller.listCheckBoxMinus[i] == false) {
        controller.listUserNotSendNotify.add(controller.listUser[i]);
        listId.remove(controller.listUser[i].id);
      }
    }
    controller.listUser.value = [];
    for(int i = 0; i < controller.listAllUser.length; i++){
      if(listId.contains(controller.listAllUser[i].id) == true){
        controller.listUser.add(controller.listAllUser[i]);
      }
    }
    controller.listUser.refresh();
    controller.listUserNotSendNotify.refresh();
    controller.listCheckBoxMinus.value = [];
    for (int i = 0; i < controller.listUser.length; i++) {
      controller.listCheckBoxMinus.add(true);
    }
    controller.listCheckBoxAdd.value = [];
    for (int i = 0; i < controller.listUserNotSendNotify.length; i++) {
      controller.listCheckBoxAdd.add(false);
    }
    controller.isCheckAllMinus.value = false;
    controller.listCheckBoxMinus.refresh();
    controller.listCheckBoxAdd.refresh();
    controller.update();
    Get.back();
  }

  RichText getRole(List<ItemContact> list, List<dynamic> list1, role, index) {
    return RichText(
      textAlign: TextAlign.start,
      text: TextSpan(
          text: "${controller.getTitle(role)}",
          style: TextStyle(
              color: const Color.fromRGBO(173, 173, 173, 1),
              fontSize: 14.sp,
              fontWeight: FontWeight.w500,
              fontFamily: 'static/Inter-Medium.ttf'),
          children: [
            TextSpan(
              text:
              "${controller.getTextHomeRoomTeacher(list, list[index].positionName, index)}",
              style: TextStyle(
                  color: const Color.fromRGBO(26, 26, 26, 1),
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf'),
              children: list1.map((e) {
                var index = list1.indexOf(e);
                var showSplit = ", ";
                if (index == list1.length - 1) {
                  showSplit = "";
                }
                if (controller.typeInList.value == "TEACHER") {
                  return TextSpan(
                      text: "Môn ${e.fullName}$showSplit",
                      style: TextStyle(
                          color: const Color.fromRGBO(26, 26, 26, 1),
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'static/Inter-Medium.ttf'));
                }

                return TextSpan(
                    text: "${e.fullName}$showSplit",
                    style: TextStyle(
                        color: const Color.fromRGBO(26, 26, 26, 1),
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'static/Inter-Medium.ttf'));
              }).toList(),
            ),
          ]),
    );
  }
}
