import 'package:flutter/cupertino.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:get/get.dart';
import '../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/contacts.dart';
import '../../../../../../../../data/model/common/req_file.dart';
import '../../../../../../../../data/model/res/file/response_file.dart';
import '../../../../../../../../data/repository/file/file_repo.dart';
import '../../../../../../../../data/repository/notification_repo/notification_repo.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';

class DetailSendNotificationSMSController extends GetxController{

  final FileRepo fileRepo = FileRepo();
  var controllerContentNotification = TextEditingController().obs;
  var focusContentNotification = FocusNode().obs;
  var listUser = <ItemContact>[].obs;
  var listUserNotSendNotify = <ItemContact>[].obs;
  var listAllUser = <ItemContact>[].obs;
  var listCheckBoxMinus = <bool>[].obs;
  var listCheckBoxAdd= <bool>[].obs;
  var student = <Student>[].obs;
  var parent = <Parent>[].obs;
  var filesUploadNotification = <ResponseFileUpload>[].obs;
  var files = <ReqFile>[].obs;
  final NotificationRepo _notificationRepo = NotificationRepo();
  var classId = "".obs;
  var isCheckAllAdd = false.obs;
  var isCheckAllMinus = false.obs;
  var typeInList = "".obs;
  var message = "";


  @override
  void onInit() {
    super.onInit();
    var tmpListUser = Get.arguments;
    if(tmpListUser!=null){
      listUser.value = tmpListUser[0];
      listUserNotSendNotify.value = tmpListUser[1];
      classId.value = tmpListUser[2];
    }
    listAllUser.addAll(listUser);
    listAllUser.addAll(listUserNotSendNotify);

    addListCheckBox();
  }


  addListCheckBox(){
    listCheckBoxMinus.value = [];
    listCheckBoxAdd.value = [];
    for(int i = 0;i<listUser.length;i++){
      listCheckBoxMinus.add(true);
    }
    listCheckBoxMinus.refresh();
    for(int i = 0;i<listUserNotSendNotify.length;i++){
      listCheckBoxAdd.add(false);
    }
    listCheckBoxAdd.refresh();
    update();
  }


  checkAllAdd() {
    if (isCheckAllAdd.value == true) {
      listCheckBoxAdd.value = [];
      for (int i = 0; i < listUserNotSendNotify.length; i++) {
        listCheckBoxAdd.add(true);
      }
    } else {
      listCheckBoxAdd.value = [];
      for (int i = 0; i < listUserNotSendNotify.length; i++) {
        listCheckBoxAdd.add(false);
      }
    }
    update();
  }


  checkBoxListViewAdd(index) {
    if (listCheckBoxAdd[index] == false) {
      listCheckBoxAdd[index] = true;
    } else {
      listCheckBoxAdd[index] = false;
    }
    update();
  }


  checkAllMinus() {
    if (isCheckAllMinus.value == true) {
      listCheckBoxMinus.value = [];
      for (int i = 0; i < listUser.length; i++) {
        listCheckBoxMinus.add(true);
      }
    } else {
      listCheckBoxMinus.value = [];
      for (int i = 0; i < listUser.length; i++) {
        listCheckBoxMinus.add(false);
      }
    }
    update();
  }


  checkBoxListViewMinus(index) {
    if (listCheckBoxMinus[index] == false) {
      listCheckBoxMinus[index] = true;
    } else {
      listCheckBoxMinus[index] = false;
    }
    update();
  }




  sendNotification(title,body,typeNotify,classId,listId, files) {
    _notificationRepo.sendNotification(title,body,typeNotify,classId, listId,files).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Gửi thông báo thành công");
        comeToHome();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Gửi thông báo thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
  }


  Future<void> sendingSMS(String msg,List<String> recipients) async {
    try {
      String result = (await sendSMS(message: msg, recipients: recipients,));
      message = result;
    } catch (error) {
      message = error.toString();
    }
  }


  checkPermissionSendSMS(String msg, List<String> recipients) async{
    PermissionStatus status = await Permission.sms.request();
    if(status == PermissionStatus.granted){
      await sendingSMS(msg,recipients);
    }
    if(status == PermissionStatus.denied){
      AppUtils.shared.snackbarError("Bạn đã hủy cấp quyền", "");
    }
    if(status == PermissionStatus.permanentlyDenied){
      openAppSettings();
    }

  }

  getTitle(role) {
    switch (role) {
      case "PARENT":
        return "Phụ huynh hs: ";
      case "STUDENT":
        return "Phụ huynh: ";
      case "TEACHER":
        return "Giáo viên: ";
    }
  }

  getListAddRole(role,index) {
    switch (role) {
      case "PARENT":
        return listUserNotSendNotify[index].student;
      case "STUDENT":
        return listUserNotSendNotify[index].parent;
      case "TEACHER":
        return listUserNotSendNotify[index].subject;
    }
  }

  getListMinusRole(role,index) {
    switch (role) {
      case "PARENT":
        return listUser[index].student;
      case "STUDENT":
        return listUser[index].parent;
      case "TEACHER":
        return listUser[index].subject;
    }
  }


  getListAllRole(role,index) {
    switch (role) {
      case "PARENT":
        return listUser[index].student;
      case "STUDENT":
        return listUser[index].parent;
      case "TEACHER":
        return listUser[index].subject;
    }
  }

  getTextHomeRoomTeacher(List<ItemContact> list,isHomeRoom, index) {
    var split = "";
    switch (isHomeRoom) {
      case "":
        return "";
      case null:
        return "";
      case "Giáo viên chủ nhiệm":
        if (list[index].subject!.isEmpty) {
          split = "";
        } else {
          split = ",";
        }
        return "Giáo viên chủ nhiệm$split ";
      default:
        return "";
    }
  }


}