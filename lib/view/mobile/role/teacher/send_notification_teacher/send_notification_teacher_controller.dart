import 'package:get/get.dart';


 class SendNotificationTeacherController extends GetxController{
 var isExpandParents = false.obs;
 var clickSMSParent = false.obs;
 var clickNotifyAppParents = false.obs;
 var isExpandStudent = false.obs;
 var clickSMSStudent = false.obs;
 var clickNotifyAppStudent= false.obs;
 var isExpandAll= false.obs;
 var clickSMSAll = false.obs;
 var clickNotifyAppAll = false.obs;
 var isExpandTeacher= false.obs;
 var clickSMSTeacher = false.obs;
 var clickNotifyAppTeacher = false.obs;
 }