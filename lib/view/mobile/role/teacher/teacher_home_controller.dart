import 'dart:ui';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/widget/logout.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/model/common/user_profile.dart';
import 'package:slova_lms/data/model/res/class/School.dart';
import 'package:slova_lms/data/repository/person/personal_info_repo.dart';
import 'package:slova_lms/data/repository/subject/class_office_repo.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/event_news/event_news_controller.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/schedule/schedule_controller.dart';
import '../../../../commom/utils/check_user_group_permission.dart';
import '../../../../data/model/common/Position.dart';
import '../../../../data/model/common/schedule.dart';
import '../../../../data/model/common/school_year.dart';
import '../../../../data/model/common/static_page.dart';
import '../../../../data/model/common/subject.dart';
import '../../../../data/model/res/class/classTeacher.dart';
import '../../../../data/repository/common/common_repo.dart';
import '../../../../data/repository/schedule/schedule_repo.dart';
import '../../../../data/repository/school_year/school_year_repo.dart';
import '../../phonebook/phone_book_controller.dart';
import 'diligent_management_teacher/diligent_management_teacher_controller.dart';



class TeacherHomeController extends GetxController {
  final CommonRepo _commonRepo = CommonRepo();
  final ClassOfficeRepo _classRepo = ClassOfficeRepo();
  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();
  final ScheduleRepo _scheduleRepo = ScheduleRepo();
  final SchoolYearRepo _schoolYearRepo = SchoolYearRepo();
  RxList<ClassId> classOfTeacher = <ClassId>[].obs;
  Rx<ClassId> currentClass = ClassId().obs;
  var position = Position().obs;
  var school = SchoolData().obs;
  var subject = Subject().obs;
  RxList<Schedule> scheduleTeacher = <Schedule>[].obs;
  RxList<Appointment> timeTable = <Appointment>[].obs;
  var userProfile = UserProfile().obs;
  var staticPage = ItemsStaticPage().obs;
  RxList<Clazzs>  listclass = <Clazzs>[].obs;
  RxList<SchoolYear> schoolYears = <SchoolYear>[].obs;
  var clickSchoolYear = <bool>[].obs;
  var fromYearPresent = 0.obs;
  var toYearPresent = 0.obs;
  var isReadyDrawer = false.obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  RxList<Schedule> scheduleToday = <Schedule>[].obs;
  var classUId = ClassId().obs;
  var calendarController = CalendarController().obs;
  var isSubjectToday = true.obs;
  RxList<SubjectRes> subjects = <SubjectRes>[].obs;
  final ClassOfficeRepo _subjectRepo = ClassOfficeRepo();
  var listPageClassroomManagement = <ItemHomePageTeacher>[].obs;
  var listPageInformationFromSchool = <ItemHomePageTeacher>[].obs;
  static const diligentManagement = "Quản Lý Chuyên Cần";
  static const learningManagement = "Quản Lý Học Tập";
  static const sendNotify = "Gửi Thông Báo";
  static const listStudentInClass = "Danh Sách Học Sinh";
  static const newAndEvent = "Tin Tức Sự Kiện";
  static const notification = "Thông báo";
  var isReady = false.obs;
  @override
  void onInit() {
    getSchoolYearByUser();
    super.onInit();
  }


  getListClassroomManagement(){
    if(Get.find<HomeController>().userGroupByApp.isEmpty){
      listPageClassroomManagement.add(ItemHomePageTeacher(name: diligentManagement,image: "assets/images/img_amicroteacher.png"));
      listPageClassroomManagement.add(ItemHomePageTeacher(name: learningManagement,image: "assets/images/pana.png"));
      listPageClassroomManagement.add(ItemHomePageTeacher(name: sendNotify,image: "assets/images/img_sendnotify.png"));
      listPageClassroomManagement.add(ItemHomePageTeacher(name: listStudentInClass,image: "assets/images/img_list_student.png"));
    }else{
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_MANAGE_DILIGENCE)){
        listPageClassroomManagement.add(ItemHomePageTeacher(name: diligentManagement,image: "assets/images/img_amicroteacher.png"));
      }
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_MANAGE_LEARNING_MANAGEMENT)){
        listPageClassroomManagement.add(ItemHomePageTeacher(name: learningManagement,image: "assets/images/pana.png"));
      }

      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_SEND_NOTIFY)){
        listPageClassroomManagement.add(ItemHomePageTeacher(name: sendNotify,image: "assets/images/img_sendnotify.png"));
      }
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_LIST_STUDENT)){
        listPageClassroomManagement.add(ItemHomePageTeacher(name: listStudentInClass,image: "assets/images/img_list_student.png"));
      }
    }
  }

  getListInformationFromSchool(){
    if(Get.find<HomeController>().userGroupByApp.isEmpty){
      listPageInformationFromSchool.add(ItemHomePageTeacher(name: newAndEvent,image: "assets/images/img_event.png"));
      listPageInformationFromSchool.add(ItemHomePageTeacher(name: notification,image: "assets/images/img_notify.png"));
    }else{
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_NEWS)){
        listPageInformationFromSchool.add(ItemHomePageTeacher(name: newAndEvent,image: "assets/images/img_event.png"));
      }
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_HOME_NOTIFY)){
        listPageInformationFromSchool.add(ItemHomePageTeacher(name: notification,image: "assets/images/img_notify.png"));
      }
    }


  }


  clickDiligentManagement(){
    Get.toNamed(Routes.diligentManagementTeacher);
    if(Get.isRegistered<DiligenceManagementTeacherController>()){
      Get.find<DiligenceManagementTeacherController>().indexClick.value = 0;
      Get.find<DiligenceManagementTeacherController>().update();
    }
  }

  clickLearningManagement(){
    if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_SUBJECT_DETAIL)){
      if(subjects.length == 1){
        if (Get.find<TeacherHomeController>().userProfile.value.classesOfHomeroomTeacher?.contains(Get.find<TeacherHomeController>().currentClass.value.classId) ==
            false){
          Get.toNamed(Routes.subjectLearningManagementTeacherPage,arguments: subjects[0]);
        }else{
          Get.toNamed(Routes.subjectTeacherPage);
        }
      }else{
        Get.toNamed(Routes.subjectTeacherPage);
      }
    }else{
      Get.toNamed(Routes.subjectTeacherPage);
    }

  }


  onClickSchoolYears(index){
    clickSchoolYear.value = <bool>[].obs;
    for(int i = 0;i<schoolYears.length;i++){
      clickSchoolYear.add(false);
    }
    clickSchoolYear[index] = true;
  }

  getSchoolYearByUser() async{
    await _schoolYearRepo.getListSchoolYearByUser().then((value) async{
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        if(schoolYears.isNotEmpty){
          fromYearPresent.value = schoolYears[0].fromYear!;
          toYearPresent.value = schoolYears[0].toYear!;
          if(DateTime.now().month<=12&&DateTime.now().month>9){
            calendarController.value.displayDate = DateTime(fromYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-2);
          }else{
            calendarController.value.displayDate = DateTime(toYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-2);
          }
        }
        for(int i = 0;i<schoolYears.length;i++){
          clickSchoolYear.add(false);
        }

      }

      await getDetailProfile();
      await Get.find<HomeController>().getCountNotifyNotSeen();
    });
    isReady.value = true;
  }





  setTimeCalendar(index){
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      calendarController.value.displayDate = DateTime(schoolYears[index].fromYear!.toInt(),DateTime.now().month,DateTime.now().day,DateTime.now().hour-2);
    }else{
      calendarController.value.displayDate = DateTime(schoolYears[index].toYear!.toInt(),DateTime.now().month,DateTime.now().day,DateTime.now().hour-2);
    }
  }


  getDetailProfile() async{
    await _personalInfoRepo.detailUserProfile().then((value) async{
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        if(userProfile.value.school == null){
          school.value.name = "";
          school.value.id = "";
        }else{
          school.value = userProfile.value.school!;
          Get.put(EventNewsController());
          userProfile.value.school!.id!;
          Get.find<EventNewsController>().setSchoolId(userProfile.value.school!.id!);
          Get.find<EventNewsController>().onRefresh();
        }
        if(userProfile.value.position == null){
          position.value.name = "";
          position.value.id = "";
        }else{
          position.value = userProfile.value.position!;
        }
        AppCache().userId = userProfile.value.id ?? "";

      }
    });
    await getListClassIdByTeacher();
  }

  getListClassIdByTeacher() async{
    var teacherId = userProfile.value.id;
    await _classRepo.listClassByTeacher(teacherId).then((value) async{
      if (value.state == Status.SUCCESS) {
        classOfTeacher.value = value.object!;
        Get.find<ScheduleController>().queryTimeTableClassByTeacher(userProfile.value.id,classOfTeacher[0].classId);
        for (var f in classOfTeacher) {
          f.checked = false;
        }
        classOfTeacher.first.checked = true;
        currentClass.value = classOfTeacher.first;
        classOfTeacher.refresh();
        //Call next API
        await getTablePlan();
      }
    });
    await getListSubject();

    isReadyDrawer.value = true;
  }

  //Lấy du lieu thoi khoa bieu theo lop hoc da chon
  getTablePlan() {
    var idTeacher = userProfile.value.id;
    getScheduleTimeTableTeacher(idTeacher);
  }

  onSelectedSchoolYears(index){
    timeTable.clear();
    fromYearPresent.value = schoolYears[index].fromYear!;
    toYearPresent.value = schoolYears[index].toYear!;
    Get.find<ScheduleController>().timeTable.clear();
    Get.find<ScheduleController>().queryTimeCalendar(AppCache().userType);
    AppCache().setSchoolYearId(schoolYears[index].id??"");
    getScheduleTimeTableTeacher(userProfile.value.id);

    _classRepo.listClassByTeacher(userProfile.value.id).then((value) {
     if (value.state == Status.SUCCESS) {
       classOfTeacher.value = value.object!;
       for (var f in classOfTeacher) {
         f.checked = false;
       }
       classOfTeacher.first.checked = true;
       currentClass.value = classOfTeacher.first;
       Get.find<ScheduleController>().queryTimeTableClassByTeacher(userProfile.value.id,classOfTeacher[0].classId);
       classOfTeacher.refresh();
     }
   });
    getListSubject();

    timeTable.refresh();
    _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        if(userProfile.value.school == null){
          school.value.name = "";
          school.value.id = "";
        }else{
          school.value = userProfile.value.school!;
        }
        if(userProfile.value.position == null){
          position.value.name = "";
          position.value.id = "";
        }else{
          position.value = userProfile.value.position!;
        }
      }
    });
    setTimeCalendar(index);
    Get.find<ScheduleController>().timeTable.refresh();
    Get.find<PhoneBookController>().getListClassByStudent();
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (schoolYears[index].fromYear == DateTime.now().year) {
        isSubjectToday.value = true;
      } else {
        isSubjectToday.value = false;
      }
    } else {
      if (schoolYears[index].toYear == DateTime.now().year) {
        isSubjectToday.value = true;
      } else {
        isSubjectToday.value = false;
      }
    }
    Get.back();
  }


  onRefresh() {
    _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        if(userProfile.value.school == null){
          school.value.name = "";
          school.value.id = "";
        }else{
          school.value = userProfile.value.school!;
        }
        if(userProfile.value.position == null){
          position.value.name = "";
          position.value.id = "";
        }else{
          position.value = userProfile.value.position!;
        }
      }
    });

    _classRepo.listClassByTeacher(userProfile.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        classOfTeacher.value = value.object!;
        for (var f in classOfTeacher) {
          f.checked = false;
        }
        classOfTeacher.first.checked = true;
        currentClass.value = classOfTeacher.first;
        classOfTeacher.refresh();
        //Call next API
        getTablePlan();
      }
    });
    getListSubject();
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      calendarController.value.displayDate = DateTime(fromYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
    }else{
      calendarController.value.displayDate = DateTime(toYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
    }

    Get.find<HomeController>().getCountNotifyNotSeen();
  }


  selectedClass(ClassId classId,index) {
    currentClass.value = classId;
    Get.find<ScheduleController>().queryTimeTableClassByTeacher(userProfile.value.id,classOfTeacher[index].classId);
    for (var f in classOfTeacher) {
      if (f.id != classId.id) {
        f.checked = false;
      } else {
        f.checked = true;
      }
    }
    classOfTeacher.refresh();
    getListSubject();
  }

  goToUpdateInfoUser() {
    Get.toNamed(Routes.updateInfoTeacher, arguments: userProfile.value);
  }

  void submitLogout() async {
    logout();
  }

  getScheduleTimeTableTeacher(idTeacher){
    _scheduleRepo.getScheduleTeacher(idTeacher).then((value) {
      if (value.state == Status.SUCCESS) {
        scheduleTeacher.value = value.object!;
        getAppointments();
      }
    });
    scheduleTeacher.refresh();
  }

  getAppointments() {
    timeTable.clear();
    for(int i =0; i<scheduleTeacher.length;i++){
      timeTable.add(Appointment(
          startTime: DateTime.fromMillisecondsSinceEpoch(scheduleTeacher[i].startTime!), // tg kết thúc
          endTime: DateTime.fromMillisecondsSinceEpoch(scheduleTeacher[i].endTime!), // tg bắt đầu
          subject: scheduleTeacher[i].subject!.subjectCategory!.name!,  // môn học
          notes: scheduleTeacher[i].subject?.clazz?.classCategory?.name!.toString(), // tên lớp
          resourceIds: scheduleTeacher[i].subject!.clazz!.students,
          recurrenceId: scheduleTeacher[i].subject?.teacher?.fullName!.toString(),
        // chi tiết học sinh
          color: const Color.fromRGBO(249, 154, 81, 1)
      ));
    }
    scheduleToday.value = scheduleTeacher.where((element) => outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(element.startTime!))
        == outputDateFormat.format(DateTime.now())&&outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(element.endTime!))
        == outputDateFormat.format(DateTime.now())).toList();
    timeTable.refresh();
  }

  getStaticPage(type){
    _commonRepo.getListStaticPage(type).then((value){
      if (value.state == Status.SUCCESS) {
        staticPage.value = value.object!;
        goToStaticPage();
      }
    });
  }





  goToStaticPage(){
    Get.toNamed(Routes.staticPage,arguments: staticPage.value);
  }


  void goToLogin() {
    Get.toNamed(Routes.login);
  }

  void goToStudentListPage() {
    Get.toNamed(Routes.studentListPage, arguments: currentClass.value);
  }
  void goToDetailAvatar(){
    Get.toNamed(Routes.getavatar, arguments: userProfile.value.image);
  }
  void goToChangePassPage(){
    Get.toNamed(Routes.changePass,arguments:userProfile.value.id);
  }
  goToSendNotificationPage(){
    Get.toNamed(Routes.sendNotificationTeacherPage);
  }

  getType(type, subject){
    switch(type){
      case "HOME_ROOM_TEACHER":
        return "Giáo viên chủ nhiệm";
      case "SUBJECT_TEACHER":
        return "Giáo viên $subject";
      default:
        return "";
    }

  }

  getListSubject() {
    subjects.clear();
    var classID = currentClass.value.classId;
    _subjectRepo.listSubjectTeacher(classID).then((value) {
      if (value.state == Status.SUCCESS) {
        subjects.value = value.object!;
      }
    });
    subjects.refresh();
  }

}

class ItemHomePageTeacher {
  String? name;
  String? image;


  ItemHomePageTeacher({this.name,this.image});
}
