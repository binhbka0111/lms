
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/ViewPdf.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/open_url.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/model/common/learning_managerment.dart';
import 'package:slova_lms/data/model/res/exercise/exercise.dart';
import 'package:slova_lms/data/repository/learning_managerment/learning_managerment_repo.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';

class DetailExamStudentController extends GetxController{
  var showFileUp = true.obs;
  var txtSubmitted = "Đã nộp bài".obs;
  var txtLateSubmission = "Nộp bài muộn".obs;
  var txtNotSubmission = "Không nộp bài".obs;
  var txtUnSubmited = "Chưa nộp bài".obs;
  var txtGraded = "".obs;
  var showButton = true.obs;
  var itemExercise = "".obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputDateFormatmilisecond = DateFormat('dd/MM/yyyy HH:mm:ss');
  final LearningManagermentRepo _learningRepo = LearningManagermentRepo();
  var detaileExams = DetailExerciser().obs;
  RxList<QuestionsStudent> questionStudent = <QuestionsStudent>[].obs;
  RxList<QuestionAnswers> questionAnswer = <QuestionAnswers>[].obs;
  RxList<AnswerOptionExStudent> answerOption = <AnswerOptionExStudent>[].obs;
  RxList<FilesExercise> fileQuestion = <FilesExercise>[].obs;
  RxList<FilesExercise> fileUpLoad = <FilesExercise>[].obs;
  RxList<FilesExercise> fileResult = <FilesExercise>[].obs;
  var indexAnsew = 0.obs;
  var focusAttachLink = FocusNode();
  var controllerAttachLink = TextEditingController();
  Widget widget = Container();
  var question = QuestionsStudent().obs;
  var countOfExercise = 0.obs;
  var listIndex = <int>[].obs;
  RxList<FilesExercise> questionFile = <FilesExercise>[].obs;
  var listItemOfCountExercise = <int>[].obs;
  var listCountExercise =0.obs;
  var isReady = false;

  String getTempIFSCValidation(String text) {
    return text.length > 7 ? "This line is helper text" : "";
  }
  var listIndexQuestion = <String>[].obs;



  @override
  void onInit() {
    super.onInit();
    var data = Get.arguments;
    if (data != null) {
      itemExercise.value = data;
    }
    getDetailExercise();

  }

  getColorStatus(state) {
    switch (state) {
      case "SUBMITTED":
        return const Color.fromRGBO(192, 242, 220, 1);
      case "LATE_SUBMISSION":
        return const Color.fromRGBO(255, 236, 193, 1);
      case "NOT_SUBMISSION":
        return const Color.fromRGBO(252, 211, 215, 1);
      case "UNSUBMITTED":
        return const Color.fromRGBO(252, 211, 215, 1);
      default:
        return const Color.fromRGBO(246, 246, 246, 1);
    }
  }

  getColorTextStatus(state) {
    switch (state) {
      case "SUBMITTED":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "LATE_SUBMISSION":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "NOT_SUBMISSION":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "UNSUBMITTED":
        return const Color.fromRGBO(255, 69, 89, 1);
      default:
        return const Color.fromRGBO(136, 136, 136, 1);
    }
  }

  getTextStatus(state) {
    switch (state) {
      case "SUBMITTED":
        return txtSubmitted;
      case "LATE_SUBMISSION":
        return txtLateSubmission;
      case "NOT_SUBMISSION":
        return txtNotSubmission;
      case "UNSUBMITTED":
        return txtUnSubmited;
      default:
        return txtGraded;
    }
  }

  getTypeExercise(state) {
    switch (state) {
      case "SELECTED_RESPONSE":
        return "Trắc nghiệm";
      case "CONSTRUCTED_RESPONSE":
        return "Tự luận";
      default:
        return "";
    }
  }


  getDetailExercise() async {
    var idExam = itemExercise.value;
    var idStudent = Get.find<StudentHomeController>().userProfile.value.id;
     await _learningRepo.detailExamsStudent(idExam, idStudent).then((value) {
      if (value.state == Status.SUCCESS) {
        detaileExams.value = value.object!;
        if (detaileExams.value.questions == null) {
        } else {
          questionStudent.value = detaileExams.value.questions!;
        }
        for(int i = 0; i< questionStudent.length; i++){
          listIndexQuestion.add(questionStudent[i].id!);

        }
        if(detaileExams.value.questionAnswers != null){
          questionAnswer.value= detaileExams.value.questionAnswers!;
        }else{
        }

        if(questionStudent.isNotEmpty){
          listCountExercise.value = (questionStudent.length/5).ceil();
        }

        for(int i = 0; i<listCountExercise.value; i++){
          listItemOfCountExercise.add(i);
        }


        if (detaileExams.value.filesAnswer == null) {
        } else {
          fileUpLoad.value = detaileExams.value.filesAnswer!;
        }

        if(detaileExams.value.files==null){
        }else{
          questionFile.value= detaileExams.value.files!;
        }

        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {});
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("thất bại", value.message ?? "");
      }
    });
     isReady= true;
     update();
  }

  getIconFile(ext) {
    switch (ext) {
      case "jpg":
        return "assets/images/icon_image_jpg.jpg";
      case "png":
        return "assets/images/icon_image_png.png";
      case "docx":
        return "assets/images/icon_fileDoc.png";
      case "xls":
        return "assets/images/icon_image_excel.jpg";
      case "zip":
        return "assets/images/icon_image_zip.png";
      case "rar":
        return "assets/images/icon_image_rar.png";
      default:
        return "assets/images/icon_image_rar.png";
    }
  }

  getAttackFile(index, h) {
    var action = 0;
    if (questionAnswer[index].files?[h].ext == "png" ||
        questionAnswer[index].files?[h].ext == "jpg" ||
        questionAnswer[index].files?[h].ext == "jpeg" ||
        questionAnswer[index].files?[h].ext == "gif" ||
        questionAnswer[index].files?[h].ext == "bmp") {
      action = 1;
    } else if (questionAnswer[index].files?[h].ext == "pdf") {
      action = 2;
    } else {
      action = 0;
    }
    switch (action) {
      case 1:
        OpenUrl.openImageViewer(
            Get.context!, questionAnswer[index].files?[h].link!);
        break;
      case 2:
        Get.to(ViewPdfPage(url: questionAnswer[index].files![h].link!));
        break;
      default:
        OpenUrl.openFile(questionAnswer[index].files![h].link!);
        break;
    }
  }
  getAttackFileAnswer(index,h) {
    var action = 0;
    if (questionAnswer[index].question!.files![h].ext== "png" ||
        questionAnswer[index].question!.files![h].ext == "jpg" ||
        questionAnswer[index].question!.files![h].ext == "jpeg" ||
        questionAnswer[index].question!.files![h].ext == "gif" ||
        questionAnswer[index].question!.files![h].ext == "bmp") {
      action = 1;
    } else if (questionAnswer[index].question!.files![h].ext == "pdf") {
      action = 2;
    } else {
      action = 0;
    }
    switch (action) {
      case 1:
        OpenUrl.openImageViewer(Get.context!, questionAnswer[index].question!.files![h].link!);
        break;
      case 2:
        Get.to(ViewPdfPage(url:questionAnswer[index].question!.files![h].link!));
        break;
      default:
        OpenUrl.openFile(questionAnswer[index].question!.files![h].link!);
        break;
    }
  }

  getAttackFileQuestion(index,h) {
    var action = 0;
    if (questionStudent[index].files![h].ext== "png" ||
        questionStudent[index].files![h].ext == "jpg" ||
        questionStudent[index].files![h].ext == "jpeg" ||
        questionStudent[index].files![h].ext == "gif" ||
        questionStudent[index].files![h].ext == "bmp") {
      action = 1;
    } else if (questionStudent[index].files![h].ext== "pdf") {
      action = 2;
    } else {
      action = 0;
    }
    switch (action) {
      case 1:
        OpenUrl.openImageViewer(Get.context!, questionStudent[index].files![h].link!);
        break;
      case 2:
        Get.to(ViewPdfPage(url:questionStudent[index].files![h].link!));
        break;
      default:
        OpenUrl.openFile(questionStudent[index].files![h].link!);
        break;
    }
  }
  getAttackFileQuestion2(index){
    var action = 0;
    if (questionFile[index].ext == "png" ||
        questionFile[index].ext == "jpg" ||
        questionFile[index].ext == "jpeg" ||
        questionFile[index].ext == "gif" ||
        questionFile[index].ext == "bmp") {
      action = 1;
    } else if (questionFile[index].ext == "pdf") {
      action = 2;
    } else {
      action = 0;
    }
    switch (action) {
      case 1:
        OpenUrl.openImageViewer(Get.context!,questionFile[index].link!);
        break;
      case 2:
        Get.to(ViewPdfPage(url: questionFile[index].link!));
        break;
      default:
        OpenUrl.openFile(questionFile[index].link!);
        break;
    }
  }

  getAttackFileResult(index) {
    var action = 0;
    if (fileUpLoad[index].ext == "png" ||
        fileUpLoad[index].ext == "jpg" ||
        fileUpLoad[index].ext == "jpeg" ||
        fileUpLoad[index].ext == "gif" ||
        fileUpLoad[index].ext == "bmp") {
      action = 1;
    } else if (fileUpLoad[index].ext == "pdf") {
      action = 2;
    } else {
      action = 0;
    }
    switch (action) {
      case 1:
        OpenUrl.openImageViewer(Get.context!, fileUpLoad[index].link!);
        break;
      case 2:
        Get.to(ViewPdfPage(url: fileUpLoad[index].link!));
        break;
      default:
        OpenUrl.openFile(fileUpLoad[index].link!);
        break;
    }
  }

  getWidget(index, i) {
    if(detaileExams.value.isSubmit=="FALSE"){
     if(questionAnswer.isNotEmpty){
       return submittedSave(index, i);
     }else{
       return notSubmitted(index, i);
     }
    }else{
      if(detaileExams.value.statusExerciseByStudent =="SUBMITTED"){
        if(detaileExams.value.isGrade=="TRUE"){
          return scored(index, i);
        }else{
          return submitted(index, i);
        }
      }else{
          return notSubmitted(index, i);
      }
    }
  }



  Widget notSubmitted(index, i) {
    return
      Column(
        children: [
          const Padding(
              padding: EdgeInsets.only(
                  top: 8)),
          Row(
            children: [
              InkWell(
                onTap: (){
                  // controller.indexAnsew.value = i;
                },
                child:
                Obx(() => Container(
                  alignment: Alignment.center,
                  height: 30.h,
                  width: 30.h,
                  decoration: BoxDecoration(
                      border: Border.all(
                          color:  const Color.fromRGBO(
                              133, 133, 133, 1),
                          width: 1),
                      color:  const Color.fromRGBO(255, 255, 255, 1),
                      borderRadius:
                      BorderRadius.circular(
                          30)),
                  child:
                  Text(
                    "${questionStudent[index].answerOption?[i].key!}",
                    style: TextStyle(
                        color: const Color.fromRGBO(
                            177, 177, 177, 1),
                        fontWeight:
                        FontWeight.w500,
                        fontSize: 14.sp),
                  ),
                )),
              ),


              Padding(
                  padding: EdgeInsets.only(
                      right: 8.w)),
              Expanded(
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 8),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: const Color.fromRGBO(
                                133, 133, 133, 1),
                            width: 1),
                        borderRadius:
                        BorderRadius.circular(
                            6)),
                    child: Text(
                      "${questionStudent[index].answerOption?[i].value}",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 14.sp,
                          fontWeight:
                          FontWeight.w400),
                    ),
                  ))
            ],
          )
        ],
      );
  }

  Widget scored(index, i) {
    return Column(
      children: [
        const Padding(
            padding: EdgeInsets.only(
                top: 8)),
        Row(
          children: [
            InkWell(
              onTap: () {

              },
              child:
              Obx(() =>
                  Container(
                    alignment: Alignment.center,
                    height: 30.h,
                    width: 30.h,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: getColorBoder(questionStudent[index].answerOption![i].status, questionAnswer[index].answer, questionStudent[index].answerOption![i].key),
                            width: 1),
                        color: getColorContainer(questionStudent[index].answerOption![i].status, questionAnswer[index].answer, questionStudent[index].answerOption![i].key),
                        borderRadius:
                        BorderRadius.circular(
                            30)),
                    child:
                    Text(
                      "${questionStudent[index].answerOption?[i].key}",
                      style: TextStyle(
                          color: getColorText(questionStudent[index].answerOption![i].status, questionAnswer[index].answer, questionStudent[index].answerOption![i].key),
                          fontWeight:
                          FontWeight.w500,
                          fontSize: 14.sp),
                    ),
                  )
              ),
            ),


            Padding(
                padding: EdgeInsets.only(
                    right: 8.w)),
            Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 8),
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: getColorBoder(questionStudent[index].answerOption![i].status, questionAnswer[index].answer, questionStudent[index].answerOption![i].key),
                          width: 1),
                      borderRadius:
                      BorderRadius.circular(
                          6)),
                  child: Text(
                    "${questionStudent[index].answerOption?[i].value}",
                    style: TextStyle(
                        color: getColorText(questionStudent[index].answerOption![i].status, questionAnswer[index].answer, questionStudent[index].answerOption![i].key),
                        fontSize: 14.sp,
                        fontWeight:
                        FontWeight.w400),
                  ),
                ))
          ],
        )
      ],
    );
  }

  Widget submittedSave(index, i) {
    return Column(
      children: [
        const Padding(
            padding: EdgeInsets.only(
                top: 8)),
        Row(
          children: [
            InkWell(
              onTap: () {

              },
              child:
              Obx(() =>
                  Container(
                    alignment: Alignment.center,
                    height: 30.h,
                    width: 30.h,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color:questionAnswer[index].answer== questionStudent[index].answerOption?[i].key?Colors.white: const Color.fromRGBO(133, 133, 133, 1),
                            width: 1),
                        // color: getColorAnswerOption(index, i),
                        color: questionAnswer[index].answer== questionStudent[index].answerOption?[i].key? ColorUtils.COLOR_PRIMARY: Colors.white ,
                        borderRadius:
                        BorderRadius.circular(
                            30)),
                    child:
                    Text(
                      "${questionAnswer[index].question?.answerOption?[i].key}",
                      style: TextStyle(
                          color: questionAnswer[index].answer== questionStudent[index].answerOption?[i].key
                              ? Colors.white
                              : const Color.fromRGBO(
                              177, 177, 177, 1),
                          fontWeight:
                          FontWeight.w500,
                          fontSize: 14.sp),
                    ),
                  )
              ),
            ),


            Padding(
                padding: EdgeInsets.only(
                    right: 8.w)),
            Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 8),
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: const Color.fromRGBO(
                              133, 133, 133, 1),
                          width: 1),
                      borderRadius:
                      BorderRadius.circular(
                          6)),
                  child: Text(
                    "${questionAnswer[index].question?.answerOption?[i].value}",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 14.sp,
                        fontWeight:
                        FontWeight.w400),
                  ),
                ))
          ],
        )
      ],
    );
  }

  Widget submitted(index, i) {
    return Column(
      children: [
        const Padding(
            padding: EdgeInsets.only(
                top: 8)),
        Row(
          children: [
            InkWell(
              onTap: () {
              },
              child:
              Obx(() =>
                  Container(
                    alignment: Alignment.center,
                    height: 30.h,
                    width: 30.h,
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: questionAnswer[index].question?.answerOption?[i].key == "${questionAnswer[index].answer}" ?Colors.white: const Color.fromRGBO(133, 133, 133, 1),
                            width: 1),
                        color:  questionAnswer[index].question?.answerOption?[i].key == questionAnswer[index].answer ? ColorUtils.COLOR_PRIMARY: Colors.white ,
                        borderRadius:
                        BorderRadius.circular(
                            30)),
                    child:
                    Text(
                      "${questionAnswer[index].question?.answerOption?[i].key}",
                      style: TextStyle(
                          color: questionAnswer[index].question
                              ?.answerOption?[i].key ==
                              "${questionAnswer[index].answer}"
                              ? Colors.white
                              : const Color.fromRGBO(
                              177, 177, 177, 1),
                          fontWeight:
                          FontWeight.w500,
                          fontSize: 14.sp),
                    ),
                  )
              ),
            ),


            Padding(
                padding: EdgeInsets.only(
                    right: 8.w)),
            Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 8),
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: const Color.fromRGBO(
                              133, 133, 133, 1),
                          width: 1),
                      borderRadius:
                      BorderRadius.circular(
                          6)),
                  child: Text(
                    "${questionAnswer[index].question?.answerOption?[i].value}",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 14.sp,
                        fontWeight:
                        FontWeight.w400),
                  ),
                ))
          ],
        )
      ],
    );
  }

  getColorContainer(status, ansew, key){
    if(status== "TRUE"){
      if(ansew == ""){
        return Colors.white;
      }else{
        return ColorUtils.lightGreenColors;
      }


    }else{
      if(ansew == key){
        return ColorUtils.lightRedColors;
      }else{
        return Colors.white;
      }
    }


  }


  getColorBoder(status, ansew, key){
    if(status== "TRUE"){
      if(ansew == ""){
        return ColorUtils.COLOR_TEXT_GREY;
      }else{
        return ColorUtils.lightGreenColors;
      }



    }else{
      if(ansew == key){
        return ColorUtils.lightRedColors;
      }else{
        return ColorUtils.COLOR_TEXT_GREY;
      }
    }


  }

  getColorText(status, ansew, key){
    if(status== "TRUE"){
      if(ansew == ""){
        return ColorUtils.COLOR_BLACK;
      }else{
        return const Color.fromRGBO(77, 197, 145, 1);
      }

    }else{
      if(ansew == key){
        return ColorUtils.COLOR_RED_1;
      }else{
        return ColorUtils.COLOR_BLACK;
      }
    }


  }


  Widget submmitedExam(){
    return
      ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: questionAnswer.length,
          itemBuilder: (context, index) {
            if (questionAnswer.isNotEmpty) {
              answerOption.value = questionAnswer[index].question!.answerOption!;
              fileQuestion.value = questionStudent[index].files!;
              fileResult.value = questionAnswer[index].files!;
            }else{
              questionAnswer[index].answer ="";
            }

            return
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                              child: RichText(
                                  text:
                                  TextSpan(children: [
                                    TextSpan(
                                        text:
                                        "Câu ${getIndexQuestion(questionStudent[index].id)}: ",
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                133, 133, 133, 1),
                                            fontSize: 12.sp,
                                            fontWeight:
                                            FontWeight.w500)),
                                    TextSpan(
                                        text:
                                        "[${questionAnswer[index].question?.point?? 0}đ]",
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                248, 126, 47, 1),
                                            fontSize: 12.sp,
                                            fontWeight:
                                            FontWeight.w500)),
                                    TextSpan(
                                        text:
                                        " (${getTypeExercise(questionAnswer[index].question?.typeQuestion ?? "")}) ",
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                248, 126, 47, 1),
                                            fontSize: 12.sp,
                                            fontWeight:
                                            FontWeight.w500)),
                                    TextSpan(
                                        text:
                                        questionAnswer[index].question?.content ?? "",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12.sp,
                                            fontWeight:
                                            FontWeight.w400)),
                                  ])))
                        ],
                      ),
                      SizedBox(height: 8.h,),
                      if (questionAnswer.isNotEmpty)
                        Column(
                          children: [
                            fileQuestion.isNotEmpty
                                ?
                            Container(
                                padding:
                                const EdgeInsets.all(12),
                                decoration: BoxDecoration(
                                    color: const Color.fromRGBO(
                                        246, 246, 246, 1),
                                    borderRadius:
                                    BorderRadius
                                        .circular(8)),
                                child:
                                ListView.builder(
                                    shrinkWrap: true,
                                    physics: const NeverScrollableScrollPhysics(),
                                    itemCount: fileQuestion.length,
                                    itemBuilder: (context, h){
                                      if(fileQuestion.isNotEmpty){
                                      }else{
                                        fileQuestion[index].name = "";
                                        fileQuestion[index].size = "";
                                      }
                                      return
                                        questionStudent[index].files?[h].ext== "png" ||
                                            questionStudent[index].files?[h].ext == "jpg" ||
                                            questionStudent[index].files?[h].ext == "jpeg" ||
                                            questionStudent[index].files?[h].ext == "gif" ||
                                            questionStudent[index].files?[h].ext == "bmp"?
                                        InkWell(
                                            onTap: (){
                                              getAttackFileAnswer(index,h);
                                            },
                                            child:     Row(
                                              children: [

                                                SizedBox(
                                                  width: 24,
                                                  height: 24,
                                                  child:
                                                  CacheNetWorkCustomBanner(urlImage:   questionStudent[index].files?[h].link?? "",),

                                                ),
                                                SizedBox(width: 6.w,),

                                                Expanded(
                                                  child:
                                                  Column(
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment
                                                        .start,
                                                    children: [
                                                      Text(
                                                        questionStudent[index].files?[h].name ?? "",
                                                        style: TextStyle(
                                                            color:
                                                            const Color.fromRGBO(
                                                                26,
                                                                59,
                                                                112,
                                                                1),
                                                            fontSize: 14.sp,
                                                            fontWeight:
                                                            FontWeight
                                                                .w500),
                                                      ),
                                                      Text(
                                                        questionStudent[index].files?[h].size ?? "",
                                                        style: TextStyle(
                                                            color:
                                                            const Color.fromRGBO(
                                                                192,
                                                                192,
                                                                192,
                                                                1),
                                                            fontSize: 10.sp,
                                                            fontWeight:
                                                            FontWeight
                                                                .w400),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            )
                                        ): Row(
                                          children: [
                                            Image.asset(
                                              '${getIconFile(questionStudent[index].files?[h].ext)}',
                                              height: 24,
                                              width: 24,
                                            ),
                                            SizedBox(width: 6.w,),

                                            Expanded(child:   Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment
                                                  .start,
                                              children: [
                                                Text(
                                                  '${questionStudent[index].files?[h].name}',
                                                  style: TextStyle(
                                                      color: const Color
                                                          .fromRGBO(
                                                          26,
                                                          59,
                                                          112,
                                                          1),
                                                      fontSize:
                                                      14.sp,
                                                      fontWeight:
                                                      FontWeight
                                                          .w500),
                                                ),
                                                Text(
                                                  "${questionStudent[index].files?[h].size}",
                                                  style: TextStyle(
                                                      color: const Color
                                                          .fromRGBO(
                                                          192,
                                                          192,
                                                          192,
                                                          1),
                                                      fontSize:
                                                      10.sp,
                                                      fontWeight:
                                                      FontWeight
                                                          .w400),
                                                ),
                                              ],
                                            ),),
                                            InkWell(
                                              onTap: (){
                                                getAttackFileAnswer(index,h);
                                              },
                                              child:         Image.asset(
                                                'assets/images/icon_upfile_subject.png',
                                                height: 16,
                                                width: 16,
                                              ),
                                            )
                                          ],
                                        );

                                    })) : Container(color: Colors.white,),
                            Column(
                              children: [
                                ListView.builder(
                                    shrinkWrap: true,
                                    physics: const NeverScrollableScrollPhysics(),
                                    itemCount: answerOption.length,
                                    itemBuilder: (context, i) {

                                      if (answerOption.isNotEmpty) {
                                      }else{
                                        answerOption[i]
                                            .key = "";
                                        answerOption[i]
                                            .value = "";
                                      }
                                      if(questionAnswer.isNotEmpty){

                                      }else{
                                        questionAnswer[i].answer ="";
                                      }
                                      return
                                        getWidget(index, i);
                                    }
                                ),
                                SizedBox(height: 6.h,),
                                questionStudent[index].typeQuestion =="SELECTED_RESPONSE"   ?
                                detaileExams.value.isPublicScore=="TRUE"?
                                questionAnswer[index].teacherComment =="" ?
                                const Text(""): Row(
                                  children: [
                                    Text("Nhận xét: ",style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w500),),
                                    Expanded(child: Text(questionAnswer[index].teacherComment ?? "",style: TextStyle(
                                        color: const Color.fromRGBO(90, 90, 90, 1),
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14.sp),),)
                                  ],
                                ):const Text(''):const Text(""),
                                //
                                // questionAnswer[index].question?.typeQuestion == 'CONSTRUCTED_RESPONSE' ?
                                // detaileExams.value.isPublicScore=="TRUE"? const Text(""): Row(
                                //   children: [
                                //     Text("Nhận xét: ",style: TextStyle(
                                //         color: Colors.black,
                                //         fontSize: 14.sp,
                                //         fontWeight: FontWeight.w500),),
                                //     Expanded(child: Text(questionAnswer[index].teacherComment ?? "",style: TextStyle(
                                //         color: const Color.fromRGBO(90, 90, 90, 1),
                                //         fontWeight: FontWeight.w400,
                                //         fontSize: 14.sp),),)
                                //   ],
                                // ):const Text('')
                              ],
                            )
                          ],
                        ) else Container(color: Colors.white,),

                      SizedBox(height: 4.h,),
                      questionAnswer[index].question?.typeQuestion != 'SELECTED_RESPONSE' ?
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Bài làm", style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500,
                              fontSize: 13.sp)),
                          questionAnswer[index].answer != "" ?
                          Html(data:  questionAnswer[index].answer??"",
                            style: {
                              "body":Style(
                                  fontWeight: FontWeight.w400,
                                  fontSize: FontSize(12.0),
                                  color: Colors.black

                              )
                            },): const Text("Chưa có câu trả lời", style:  TextStyle(color: Colors.black, fontWeight: FontWeight.w400, fontSize: 12),),


                          // file câu trả lời
                          fileResult.isNotEmpty
                              ?  Container(
                              padding:
                              const EdgeInsets.all(12),
                              decoration: BoxDecoration(
                                  color: const Color.fromRGBO(
                                      246, 246, 246, 1),
                                  borderRadius:
                                  BorderRadius
                                      .circular(8)),
                              child:
                              ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: fileResult.length,
                                  itemBuilder: (context, h){

                                    if(fileResult.isNotEmpty){
                                    }else{
                                      fileResult[h].name = "";
                                      fileResult[h].size = "";
                                    }
                                    return
                                      questionAnswer[index].files![h].ext== "png" ||
                                          questionAnswer[index].files![h].ext  == "jpg" ||
                                          questionAnswer[index].files![h].ext  == "jpeg" ||
                                          questionAnswer[index].files![h].ext  == "gif" ||
                                          questionAnswer[index].files![h].ext  == "bmp"?

                                      InkWell(
                                          onTap: (){
                                            getAttackFile(index,h);
                                          },
                                          child:     Row(
                                            children: [

                                              SizedBox(
                                                width: 24,
                                                height: 24,
                                                child:
                                                CacheNetWorkCustomBanner(urlImage: questionAnswer[index].files![h].link!,),

                                              ),
                                              SizedBox(width: 6.w,),

                                              Expanded(
                                                child:
                                                Column(
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment
                                                      .start,
                                                  children: [
                                                    Text(
                                                      questionAnswer[index].files![h].name!,
                                                      style: TextStyle(
                                                          color:
                                                          const Color.fromRGBO(
                                                              26,
                                                              59,
                                                              112,
                                                              1),
                                                          fontSize: 14.sp,
                                                          fontWeight:
                                                          FontWeight
                                                              .w500),
                                                    ),
                                                    Text(
                                                      questionAnswer[index].files![h].size!,
                                                      style: TextStyle(
                                                          color:
                                                          const Color.fromRGBO(
                                                              192,
                                                              192,
                                                              192,
                                                              1),
                                                          fontSize: 10.sp,
                                                          fontWeight:
                                                          FontWeight
                                                              .w400),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          )
                                      ):
                                      Row(
                                        children: [
                                          Image.asset(
                                            '${getIconFile(questionAnswer[index].files![h].ext)}',
                                            height: 24,
                                            width: 24,
                                          ),
                                          SizedBox(width: 6.w,),

                                          Expanded(child:   Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment
                                                .start,
                                            children: [
                                              Text(
                                                '${questionAnswer[index].files![h].name}',
                                                style: TextStyle(
                                                    color: const Color
                                                        .fromRGBO(
                                                        26,
                                                        59,
                                                        112,
                                                        1),
                                                    fontSize:
                                                    14.sp,
                                                    fontWeight:
                                                    FontWeight
                                                        .w500),
                                              ),
                                              Text(
                                                "${questionAnswer[index].files![h].size}",
                                                style: TextStyle(
                                                    color: const Color
                                                        .fromRGBO(
                                                        192,
                                                        192,
                                                        192,
                                                        1),
                                                    fontSize:
                                                    10.sp,
                                                    fontWeight:
                                                    FontWeight
                                                        .w400),
                                              ),
                                            ],
                                          ),),
                                          InkWell(
                                            onTap: (){
                                              getAttackFile(index, h);
                                            },
                                            child:         Image.asset(
                                              'assets/images/icon_upfile_subject.png',
                                              height: 16,
                                              width: 16,
                                            ),
                                          )
                                        ],
                                      );

                                  })
                          )
                              : Container(color: Colors.white,),

                          detaileExams.value.isPublicScore=="TRUE"?
                          questionAnswer[index].teacherComment =="" ?  const Text(""):
                           Column(
                             children: [
                               Row(
                                 children: [
                                   Text("Nhận xét: ",style: TextStyle(
                                       color: Colors.black,
                                       fontSize: 14.sp,
                                       fontWeight: FontWeight.w500),),
                                   Expanded(child: Text(questionAnswer[index].teacherComment ?? "",style: TextStyle(
                                       color: const Color.fromRGBO(90, 90, 90, 1),
                                       fontWeight: FontWeight.w400,
                                       fontSize: 14.sp),),)
                                 ],
                               ),
                               SizedBox(height: 8.h,),
                               Row(
                                 children: [
                                   Text("Điểm: ",style: TextStyle(
                                       color: ColorUtils.PRIMARY_COLOR,
                                       fontSize: 14.sp,
                                       fontWeight: FontWeight.w500),),
                                   Expanded(child: Text("${questionAnswer[index].point}đ",style: TextStyle(
                                       color: ColorUtils.PRIMARY_COLOR,
                                       fontWeight: FontWeight.w500,
                                       fontSize: 14.sp),),)
                                 ],
                               ),
                             ],
                           ) : const Text('')

                        ],
                      ):Container()


                    ],
                  )
              );

          });
  }
  Widget unsubmittedExam(){
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount:questionStudent.length,
        itemBuilder: (context, index) {
          if (questionStudent.isNotEmpty) {
            answerOption.value = questionStudent[index].answerOption!;
            fileQuestion.value = questionStudent[index].files!;
          }
          // if(questionAnswer != null || questionAnswer.isNotEmpty){
          // }else{
          //   questionAnswer[index].answer ="";
          // }
          return
            Container(
                padding: EdgeInsets.symmetric(horizontal: 16.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: RichText(
                                text:
                                TextSpan(children: [
                                  TextSpan(
                                      text:
                                      "Câu ${getIndexQuestion(questionStudent[index].id)}: ",
                                      style: TextStyle(
                                          color: const Color.fromRGBO(
                                              133, 133, 133, 1),
                                          fontSize: 12.sp,
                                          fontWeight:
                                          FontWeight.w500)),
                                  TextSpan(
                                      text:
                                      "[${questionStudent[index].point}đ]",
                                      style: TextStyle(
                                          color: const Color.fromRGBO(
                                              248, 126, 47, 1),
                                          fontSize: 12.sp,
                                          fontWeight:
                                          FontWeight.w500)),
                                  TextSpan(
                                      text:
                                      " (${getTypeExercise(questionStudent[index].typeQuestion ?? 0)}) ",
                                      style: TextStyle(
                                          color: const Color.fromRGBO(
                                              248, 126, 47, 1),
                                          fontSize: 12.sp,
                                          fontWeight:
                                          FontWeight.w500)),
                                  TextSpan(
                                      text:
                                      questionStudent[index].content ?? "",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 12.sp,
                                          fontWeight:
                                          FontWeight.w400)),
                                ])))
                      ],
                    ),
                    SizedBox(height: 8.h,),
                    if (questionStudent.isNotEmpty) Column(
                      children: [
                        fileQuestion.isNotEmpty
                            ?  Container(
                            padding:
                            const EdgeInsets.all(12),
                            decoration: BoxDecoration(
                                color: const Color.fromRGBO(
                                    246, 246, 246, 1),
                                borderRadius:
                                BorderRadius
                                    .circular(8)),
                            child:
                            ListView.builder(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: fileQuestion.length,
                                itemBuilder: (context, h){
                                  if(fileQuestion.isNotEmpty ){
                                  }else{
                                    fileQuestion[index].name = "";
                                    fileQuestion[index].size = "";
                                  }
                                  return
                                    questionStudent[index].files![h].ext== "png" ||
                                        questionStudent[index].files![h].ext == "jpg" ||
                                        questionStudent[index].files![h].ext == "jpeg" ||
                                        questionStudent[index].files![h].ext == "gif" ||
                                        questionStudent[index].files![h].ext == "bmp"?
                                    InkWell(
                                        onTap: (){
                                          getAttackFileQuestion(index,h);
                                        },
                                        child:     Row(
                                          children: [

                                            SizedBox(
                                              width: 24,
                                              height: 24,
                                              child:
                                              CacheNetWorkCustomBanner(urlImage: questionStudent[index].files?[h].link?? "",),

                                            ),
                                            SizedBox(width: 6.w,),

                                            Expanded(
                                              child:
                                              Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [
                                                  Text(
                                                    questionStudent[index].files?[h].name ?? "",
                                                    style: TextStyle(
                                                        color:
                                                        const Color.fromRGBO(
                                                            26,
                                                            59,
                                                            112,
                                                            1),
                                                        fontSize: 14.sp,
                                                        fontWeight:
                                                        FontWeight
                                                            .w500),
                                                  ),
                                                  Text(
                                                    questionStudent[index].files![h].size ?? "",
                                                    style: TextStyle(
                                                        color:
                                                        const Color.fromRGBO(
                                                            192,
                                                            192,
                                                            192,
                                                            1),
                                                        fontSize: 10.sp,
                                                        fontWeight:
                                                        FontWeight
                                                            .w400),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        )
                                    ): Row(
                                      children: [
                                        Image.asset(
                                          '${getIconFile(questionStudent[index].files![h].ext)}',
                                          height: 24,
                                          width: 24,
                                        ),
                                        SizedBox(width: 6.w,),

                                        Expanded(child:   Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment
                                              .start,
                                          children: [
                                            Text(
                                              '${questionStudent[index].files![h].name}',
                                              style: TextStyle(
                                                  color: const Color
                                                      .fromRGBO(
                                                      26,
                                                      59,
                                                      112,
                                                      1),
                                                  fontSize:
                                                  14.sp,
                                                  fontWeight:
                                                  FontWeight
                                                      .w500),
                                            ),
                                            Text(
                                              "${questionStudent[index].files![h].size}",
                                              style: TextStyle(
                                                  color: const Color
                                                      .fromRGBO(
                                                      192,
                                                      192,
                                                      192,
                                                      1),
                                                  fontSize:
                                                  10.sp,
                                                  fontWeight:
                                                  FontWeight
                                                      .w400),
                                            ),
                                          ],
                                        ),),
                                        InkWell(
                                          onTap: (){
                                            getAttackFileQuestion(index,h);
                                          },
                                          child:         Image.asset(
                                            'assets/images/icon_upfile_subject.png',
                                            height: 16,
                                            width: 16,
                                          ),
                                        )
                                      ],
                                    );

                                })
                        )
                            : Container(color: Colors.white,),
                        ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount:
                            answerOption.length,
                            itemBuilder: (context, i) {
                              if (answerOption.isNotEmpty) {
                              }else{
                                answerOption[i]
                                    .key = "";
                                answerOption[i]
                                    .value = "";
                              }

                              return
                                getWidget(index, i);

                            }
                        )

                      ],
                    ) else Container(color: Colors.white,),

                    SizedBox(height: 4.h,),



                  ],
                )
            );

        });
  }
  Widget linkSubmitted(){
    return   Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child:    Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border:  Border.all(color: ColorUtils.colorGray, width: 1)
              ),
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Đường dẫn: ", style: TextStyle(color: Colors.black, fontSize: 14.sp, fontWeight: FontWeight.w400),),
                  const SizedBox(height: 8,),
                  InkWell(
                    onTap: (){
                      OpenUrl.openLaunch(detaileExams.value.link!);
                    },
                    child:
                    Text(detaileExams.value.link?? "", style: TextStyle(color: Colors.blue, fontSize: 12.sp, fontWeight: FontWeight.w400),),
                  ),
                ],
              )
          ),
          SizedBox(height: 4.h,),

          fileUpLoad.isNotEmpty?
          Text("Bài làm", style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500,
              fontSize: 13.sp)):const Text(""),
          Html(data:detaileExams.value.contentAnswer??"",
            style: {
              "body":Style(
                  fontWeight: FontWeight.w400,
                  fontSize: FontSize(14.0),
                  color: const Color.fromRGBO(90, 90, 90, 1)
              )
            },),
          ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: fileUpLoad.length,
              itemBuilder: (context, indexFile){
                if(fileUpLoad.isNotEmpty){

                }else{
                  fileUpLoad[indexFile].name='';
                  fileUpLoad[indexFile].size='';
                  fileUpLoad[indexFile].link='';
                }
                return Column(
                  children: [
                    SizedBox(height: 16.h,),

                    Container(
                        padding:
                        const EdgeInsets.all(12),
                        decoration: BoxDecoration(
                            color: const Color.fromRGBO(
                                246, 246, 246, 1),
                            borderRadius:
                            BorderRadius
                                .circular(8)),
                        child:
                        fileUpLoad[indexFile].ext  == "png" ||
                            fileUpLoad[indexFile].ext   == "jpg" ||
                            fileUpLoad[indexFile].ext   == "jpeg" ||
                            fileUpLoad[indexFile].ext   == "gif" ||
                            fileUpLoad[indexFile].ext   == "bmp"?
                        InkWell(
                            onTap: (){
                              getAttackFileResult(indexFile);
                            },
                            child:     Row(
                              children: [

                                SizedBox(
                                  width: 24,
                                  height: 24,
                                  child:
                                  CacheNetWorkCustomBanner(urlImage:  fileUpLoad[indexFile].link ?? "",),

                                ),
                                SizedBox(width: 6.w,),

                                Expanded(
                                  child:
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment
                                        .start,
                                    children: [
                                      Text(
                                        fileUpLoad[indexFile].name ?? "",
                                        style: TextStyle(
                                            color:
                                            const Color.fromRGBO(
                                                26,
                                                59,
                                                112,
                                                1),
                                            fontSize: 14.sp,
                                            fontWeight:
                                            FontWeight
                                                .w500),
                                      ),
                                      Text(
                                        fileUpLoad[indexFile].size ?? "",
                                        style: TextStyle(
                                            color:
                                            const Color.fromRGBO(
                                                192,
                                                192,
                                                192,
                                                1),
                                            fontSize: 10.sp,
                                            fontWeight:
                                            FontWeight
                                                .w400),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                        ) : Row(
                          children: [
                            Image.asset(
                              '${getIconFile(fileUpLoad[indexFile].ext)}',
                              height: 24,
                              width: 24,
                            ),
                            SizedBox(width: 6.w,),

                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment
                                    .start,
                                children: [
                                  Text(
                                    '${fileUpLoad[indexFile].name}',
                                    style: TextStyle(
                                        color:
                                        const Color.fromRGBO(
                                            26,
                                            59,
                                            112,
                                            1),
                                        fontSize: 14.sp,
                                        fontWeight:
                                        FontWeight
                                            .w500),
                                  ),
                                  Text(
                                    "${fileUpLoad[indexFile].size}",
                                    style: TextStyle(
                                        color:
                                        const Color.fromRGBO(
                                            192,
                                            192,
                                            192,
                                            1),
                                        fontSize: 10.sp,
                                        fontWeight:
                                        FontWeight
                                            .w400),
                                  ),
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: (){
                                getAttackFileResult(indexFile);
                              },
                              child:
                              Image.asset(
                                'assets/images/icon_upfile_subject.png',
                                height: 16,
                                width: 16,
                              ),
                            )
                          ],
                        )
                    )
                  ],
                );

              }),
        ],),
    );
  }
  Widget filesSubmitted(){
    return     Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child:    Column(

        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: questionFile.length,
              itemBuilder: (context, indexFile){
                if(questionFile.isNotEmpty){

                }else{
                  questionFile[indexFile].name='';
                  questionFile[indexFile].size='';
                  questionFile[indexFile].link='';
                }
                return Column(
                  children: [
                    Container(
                        padding:
                        const EdgeInsets.all(12),
                        decoration: BoxDecoration(
                            color: const Color.fromRGBO(
                                246, 246, 246, 1),
                            borderRadius:
                            BorderRadius
                                .circular(8)),
                        child:
                        questionFile[indexFile].ext  == "png" ||
                            questionFile[indexFile].ext   == "jpg" ||
                            questionFile[indexFile].ext   == "jpeg" ||
                            questionFile[indexFile].ext   == "gif" ||
                            questionFile[indexFile].ext   == "bmp"?
                        InkWell(
                            onTap: (){
                              getAttackFileResult(indexFile);
                            },
                            child:     Row(
                              children: [

                                SizedBox(
                                  width: 24,
                                  height: 24,
                                  child:
                                  CacheNetWorkCustomBanner(urlImage:   questionFile[indexFile].link ?? "",),

                                ),
                                SizedBox(width: 6.w,),

                                Expanded(
                                  child:
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment
                                        .start,
                                    children: [
                                      Text(
                                        questionFile[indexFile].name ?? "",
                                        style: TextStyle(
                                            color:
                                            const Color.fromRGBO(
                                                26,
                                                59,
                                                112,
                                                1),
                                            fontSize: 14.sp,
                                            fontWeight:
                                            FontWeight
                                                .w500),
                                      ),
                                      Text(
                                        questionFile[indexFile].size ?? "",
                                        style: TextStyle(
                                            color:
                                            const Color.fromRGBO(
                                                192,
                                                192,
                                                192,
                                                1),
                                            fontSize: 10.sp,
                                            fontWeight:
                                            FontWeight
                                                .w400),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                        ) : Row(
                          children: [
                            Image.asset(
                              '${getIconFile(questionFile[indexFile].ext)}',
                              height: 24,
                              width: 24,
                            ),
                            SizedBox(width: 6.w,),

                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment
                                    .start,
                                children: [
                                  Text(
                                    '${questionFile[indexFile].name}',
                                    style: TextStyle(
                                        color:
                                        const Color.fromRGBO(
                                            26,
                                            59,
                                            112,
                                            1),
                                        fontSize: 14.sp,
                                        fontWeight:
                                        FontWeight
                                            .w500),
                                  ),
                                  Text(
                                    "${questionFile[indexFile].size}",
                                    style: TextStyle(
                                        color:
                                        const Color.fromRGBO(
                                            192,
                                            192,
                                            192,
                                            1),
                                        fontSize: 10.sp,
                                        fontWeight:
                                        FontWeight
                                            .w400),
                                  ),
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: (){
                                getAttackFileResult(indexFile);
                              },
                              child:
                              Image.asset(
                                'assets/images/icon_upfile_subject.png',
                                height: 16,
                                width: 16,
                              ),
                            )
                          ],
                        )
                    )
                  ],
                );

              }),
          const Padding(padding: EdgeInsets.only(top: 4)),
          fileUpLoad.isNotEmpty?
          Text("File tải lên", style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500,
              fontSize: 13.sp)):const Text(""),
          Html(data:detaileExams.value.contentAnswer??"",
            style: {
              "body":Style(
                  fontWeight: FontWeight.w400,
                  fontSize: FontSize(14.0),
                  color: const Color.fromRGBO(90, 90, 90, 1)
              )
            },),
          ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: fileUpLoad.length,
              itemBuilder: (context, indexFile){
                if(fileUpLoad.isNotEmpty){

                }else{
                  fileUpLoad[indexFile].name='';
                  fileUpLoad[indexFile].size='';
                  fileUpLoad[indexFile].link='';
                }
                return Column(
                  children: [
                    SizedBox(height: 16.h,),

                    Container(
                        padding:
                        const EdgeInsets.all(12),
                        decoration: BoxDecoration(
                            color: const Color.fromRGBO(
                                246, 246, 246, 1),
                            borderRadius:
                            BorderRadius
                                .circular(8)),
                        child:
                        fileUpLoad[indexFile].ext  == "png" ||
                            fileUpLoad[indexFile].ext   == "jpg" ||
                            fileUpLoad[indexFile].ext   == "jpeg" ||
                            fileUpLoad[indexFile].ext   == "gif" ||
                            fileUpLoad[indexFile].ext   == "bmp"?
                        InkWell(
                            onTap: (){
                              getAttackFileResult(indexFile);
                            },
                            child:     Row(
                              children: [

                                SizedBox(
                                  width: 24,
                                  height: 24,
                                  child:
                                  CacheNetWorkCustomBanner(urlImage:   fileUpLoad[indexFile].link ?? "",),

                                ),
                                SizedBox(width: 6.w,),

                                Expanded(
                                  child:
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment
                                        .start,
                                    children: [
                                      Text(
                                        fileUpLoad[indexFile].name ?? "",
                                        style: TextStyle(
                                            color:
                                            const Color.fromRGBO(
                                                26,
                                                59,
                                                112,
                                                1),
                                            fontSize: 14.sp,
                                            fontWeight:
                                            FontWeight
                                                .w500),
                                      ),
                                      Text(
                                        fileUpLoad[indexFile].size ?? "",
                                        style: TextStyle(
                                            color:
                                            const Color.fromRGBO(
                                                192,
                                                192,
                                                192,
                                                1),
                                            fontSize: 10.sp,
                                            fontWeight:
                                            FontWeight
                                                .w400),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                        ) : Row(
                          children: [
                            Image.asset(
                              '${getIconFile(fileUpLoad[indexFile].ext)}',
                              height: 24,
                              width: 24,
                            ),
                            SizedBox(width: 6.w,),

                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment
                                    .start,
                                children: [
                                  Text(
                                    '${fileUpLoad[indexFile].name}',
                                    style: TextStyle(
                                        color:
                                        const Color.fromRGBO(
                                            26,
                                            59,
                                            112,
                                            1),
                                        fontSize: 14.sp,
                                        fontWeight:
                                        FontWeight
                                            .w500),
                                  ),
                                  Text(
                                    "${fileUpLoad[indexFile].size}",
                                    style: TextStyle(
                                        color:
                                        const Color.fromRGBO(
                                            192,
                                            192,
                                            192,
                                            1),
                                        fontSize: 10.sp,
                                        fontWeight:
                                        FontWeight
                                            .w400),
                                  ),
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: (){
                                getAttackFileResult(indexFile);
                              },
                              child:
                              Image.asset(
                                'assets/images/icon_upfile_subject.png',
                                height: 16,
                                width: 16,
                              ),
                            )
                          ],
                        )
                    )
                  ],
                );

              }),
        ],),
    );
  }
  Widget linkUnsubmitted(){
    return
      Container(
          width: double.infinity,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border:  Border.all(color: ColorUtils.colorGray, width: 1)
          ),
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child:
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Đường dẫn: ", style: TextStyle(color: Colors.black, fontSize: 14.sp, fontWeight: FontWeight.w400),),
              const SizedBox(height: 8,),
              InkWell(
                onTap: (){
                  OpenUrl.openLaunch(detaileExams.value.link!);
                },
                child:
                Text(detaileExams.value.link?? "", style: TextStyle(color: Colors.blue, fontSize: 12.sp, fontWeight: FontWeight.w400),),
              ),
            ],
          )
      );
  }
  Widget fileUnSubmitted(){
    return  Container(
      decoration: BoxDecoration(
          color: const Color.fromRGBO(246, 246, 246, 1),
          borderRadius: BorderRadius.circular(8)
      ),
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child:
      ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: questionFile.length,
          itemBuilder: (context, index){
            if(questionFile.isNotEmpty){
            }else{
              questionFile[index].name = "";
              questionFile[index].size = "";
            }
            return
              questionFile[index].ext == "png" ||
                  questionFile[index].ext == "jpg" ||
                  questionFile[index].ext == "jpeg" ||
                  questionFile[index].ext== "gif" ||
                  questionFile[index].ext == "bmp"?
              InkWell(
                  onTap: (){
                    getAttackFileQuestion2(index);
                  },
                  child:

                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 8),
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child:  Row(
                      children: [

                        SizedBox(
                          width: 24,
                          height: 24,
                          child:
                          CacheNetWorkCustomBanner(urlImage:    '${questionFile[index].link}',),

                        ),
                        Padding(
                            padding: EdgeInsets.only(
                                right: 6.w)),
                        Expanded(
                          child:
                          Column(
                            crossAxisAlignment:
                            CrossAxisAlignment
                                .start,
                            children: [
                              Text(
                                '${questionFile[index].name}',
                                style: TextStyle(
                                    color:
                                    const Color.fromRGBO(
                                        26,
                                        59,
                                        112,
                                        1),
                                    fontSize: 14.sp,
                                    fontWeight:
                                    FontWeight
                                        .w500),
                              ),
                              Text(
                                '${questionFile[index].size}',
                                style: TextStyle(
                                    color:
                                    const Color.fromRGBO(
                                        192,
                                        192,
                                        192,
                                        1),
                                    fontSize: 10.sp,
                                    fontWeight:
                                    FontWeight
                                        .w400),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
              ):
              Row(
                children: [
                  Image.asset(
                    '${getIconFile( questionFile[index].ext )}',
                    height: 24,
                    width: 24,
                  ),
                  Padding(
                      padding: EdgeInsets
                          .only(
                          right:
                          6.w)),
                  Expanded(child:   Column(
                    crossAxisAlignment:
                    CrossAxisAlignment
                        .start,
                    children: [
                      Text(
                        '${ questionFile[index].name}',
                        style: TextStyle(
                            color: const Color
                                .fromRGBO(
                                26,
                                59,
                                112,
                                1),
                            fontSize:
                            14.sp,
                            fontWeight:
                            FontWeight
                                .w500),
                      ),
                      Text(
                        "${ questionFile[index].size}",
                        style: TextStyle(
                            color: const Color
                                .fromRGBO(
                                192,
                                192,
                                192,
                                1),
                            fontSize:
                            10.sp,
                            fontWeight:
                            FontWeight
                                .w400),
                      ),
                    ],
                  ),),
                  InkWell(
                    onTap: (){
                      // getAttackFileQuestion2(index);
                    },
                    child:         Image.asset(
                      'assets/images/icon_upfile_subject.png',
                      height: 16,
                      width: 16,
                    ),
                  )
                ],
              );

          }),

    );
  }

  getWidgetContainer(){

    if(detaileExams.value.isSubmit== null){
      if (questionStudent.isNotEmpty) {
          return unsubmittedExam();
      } else if (detaileExams.value.files!= null && detaileExams.value.link == null) {
        if(detaileExams.value.filesAnswer != null || detaileExams.value.contentAnswer != null){
          return filesSubmitted();
        }else{
          return fileUnSubmitted();
        }
      } else if(detaileExams.value.link!= null){
        if(detaileExams.value.filesAnswer != null || detaileExams.value.contentAnswer != null){
          return linkSubmitted();
        }else{
          return linkUnsubmitted();
        }

      }else{
        return Container();
      }
    } else if(detaileExams.value.isSubmit=="FALSE"){
      if (questionStudent.isNotEmpty) {
        if(questionAnswer.isNotEmpty){
          return submmitedExam();
        }else{
          return unsubmittedExam();
        }
      } else if (detaileExams.value.files!= null && detaileExams.value.link == null) {
        if(detaileExams.value.filesAnswer != null || detaileExams.value.contentAnswer != null){
          return filesSubmitted();
        }else{
          return fileUnSubmitted();
        }
      } else if(detaileExams.value.link!= null){
        if(detaileExams.value.filesAnswer != null || detaileExams.value.contentAnswer != null){
          return linkSubmitted();
        }else{
          return linkUnsubmitted();
        }
      }else{
        return Container();
      }
    } else{
      if (questionAnswer.isNotEmpty) {
        return submmitedExam();
      } else if (detaileExams.value.files!= null && detaileExams.value.link == null && questionStudent.isEmpty ) {
        return filesSubmitted();
      } else {
        return linkSubmitted();
      }
    }
  }
  getIndexQuestion(id){
    return  (listIndexQuestion.indexOf(id))+1;
  }
}