import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/file_widget.dart';
import 'package:slova_lms/commom/widget/loading_custom.dart';
import 'package:slova_lms/data/model/common/learning_managerment.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/detail_subject/view_exam_student/detail_exam_student/student_doing_exam/student_doing_exam_controller.dart';
import 'package:slova_lms/view/mobile/role/student/detail_subject/view_exam_student/view_exam_student_controller.dart';
import '../../../view_exercise/detail_exercise_student/student_doing_homework/file_upload_exercise_student/file_upload_exercise_student_controller.dart';

class StudentDoingExamPage extends GetWidget<StudentDoingExamController>{

  @override
  final controller = Get.put(StudentDoingExamController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<StudentDoingExamController>(builder: (controller){
      return
        controller.isReady? Obx(() => Scaffold(
          backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {},
                icon: const Icon(Icons.home),
                color: Colors.white,
              )
            ],
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            title:  Text(
              controller.itemsEx.value.title ?? "",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body:
          Column(
            children: [
              Expanded(child:      SingleChildScrollView(
                child:
                Column(
                  children: [
                    Card(
                      margin: const EdgeInsets.symmetric(horizontal: 16, vertical:  8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 80,
                                      child:       Text(
                                          "Tiêu đề: ",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w500)
                                      ),
                                    ),
                                    const SizedBox(width: 4),
                                    Expanded(child:  Text(
                                      controller.itemsEx.value.title ?? "",
                                      style: TextStyle(
                                          color: const Color.fromRGBO(90, 90, 90, 1),
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14.sp),
                                    ),)
                                  ],
                                ),
                                const Padding(padding: EdgeInsets.only(top: 16)),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 80,
                                      child:       Text(
                                          "Mô tả: ",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w500)
                                      ),
                                    ),
                                    const SizedBox(width: 4),
                                    Expanded(child: Text(
                                      controller.detailExam.value.description??"",
                                      style: TextStyle(
                                          color: const Color.fromRGBO(90, 90, 90, 1),
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14.sp),
                                    ),)
                                  ],
                                ),

                              ],
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(12),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Text(
                                              "Thời gian tạo: ",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            Expanded(child: Container()),
                                            Text(
                                              controller.outputDateFormatmilisecond.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExam.value.createdAt ?? 0)),
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(90, 90, 90, 1),
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14.sp),
                                            )
                                          ],
                                        ),
                                        const Padding(padding: EdgeInsets.only(top: 16)),
                                        Row(
                                          children: [
                                            Text(
                                              "Thời gian bắt đầu: ",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            Expanded(child: Container()),
                                            Text(
                                              controller.outputDateFormatmilisecond.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExam.value.startTime ?? 0)),
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(90, 90, 90, 1),
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14.sp),
                                            )
                                          ],
                                        ),
                                        const Padding(padding: EdgeInsets.only(top: 16)),
                                        Row(
                                          children: [
                                            Text(
                                              "Thời gian kết thúc: ",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            Expanded(child: Container()),
                                            Text(
                                              controller.outputDateFormatmilisecond.format(DateTime.fromMillisecondsSinceEpoch(controller.detailExam.value.endTime ?? 0)),
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(90, 90, 90, 1),
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14.sp),
                                            )
                                          ],
                                        ),
                                        const Padding(padding: EdgeInsets.only(top: 16)),
                                        Row(
                                          children: [
                                            Text(
                                              "Thời gian làm bài: ",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            Expanded(child: Container()),
                                            Text(
                                              "${controller.detailExam.value.examTime} phút",
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(90, 90, 90, 1),
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14.sp),
                                            )
                                          ],
                                        ),
                                        const Padding(padding: EdgeInsets.only(top: 16)),
                                        Row(
                                          children: [
                                            Text("Ghi chú: ",style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.w500),),
                                            Expanded(child: Text("${controller.detailExam.value.description}",style: TextStyle(
                                                color: const Color.fromRGBO(90, 90, 90, 1),
                                                fontWeight: FontWeight.w400,
                                                fontSize: 14.sp),),)
                                          ],
                                        ),
                                        const Padding(padding: EdgeInsets.only(top: 16)),
                                        controller.detailExam.value.statusExerciseByStudent != null? Obx(() => Row(
                                          children: [
                                            Text(
                                              "Trạng thái",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            Expanded(child: Container()),
                                            Container(
                                              padding: const EdgeInsets.symmetric(
                                                  horizontal: 12, vertical: 6),
                                              height: 24.h,
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                  color: controller.getColorStatus(
                                                      controller.detailExam.value
                                                          .statusExerciseByStudent),
                                                  borderRadius:
                                                  BorderRadius.circular(6)),
                                              child: Text(
                                                "${controller.getTextStatus(controller.detailExam.value.statusExerciseByStudent)}",
                                                style: TextStyle(
                                                    color: controller
                                                        .getColorTextStatus(controller
                                                        .detailExam
                                                        .value
                                                        .statusExerciseByStudent),
                                                    fontSize: 12.sp,
                                                    fontWeight: FontWeight.w500),
                                              ),
                                            )
                                          ],
                                        )) : Container(),
                                        // CountdownTimer(
                                        //   endTime: controller.detailExam.value.endTime+ 1000*30,
                                        // ),
                                        const Padding(padding: EdgeInsets.only(top: 16)),
                                        Row(
                                          children: [
                                            SizedBox(
                                              child: Text(
                                                  "Thời gian còn lại: ",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 14.sp,
                                                      fontWeight: FontWeight.w500)
                                              ),
                                            ),
                                            const Spacer(),

                                            // GetBuilder<StudentDoingExamController>(builder: (controller){
                                            //   return
                                            // },),
                                            Container(
                                            padding: const EdgeInsets.symmetric(horizontal: 16, vertical:8),
                                            decoration: BoxDecoration(
                                            color: ColorUtils.PRIMARY_COLOR,
                                            borderRadius: BorderRadius.circular(8)
                                            ),
                                            child:  CountdownTimer(
                                            //   controller: CountdownTimerController(endTime: 2),
                                            textStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 16.sp),
                                            onEnd: () {
                                            AppUtils.shared.showToast("Đã hết thời gian làm bài");
                                            Get.put(ViewExamStudentController()).onInit();
                                            Get.back();
                                            },
                                            endTime: controller.detailExam.value.endTime+1000*3,
      ),
      )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),

                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    controller.detailExam.value.typeExercise=="SELECTED_RESPONSE" ?
                    controller.listCountExercise.value <=1 ?Container():
                    Row(
                      children: [
                        const Spacer(),
                        const Icon(Icons.navigate_before_outlined,size: 24,),
                        const Padding(padding: EdgeInsets.only(right: 6)),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 6.w),
                          height: 40,
                          width:120,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: const Color.fromRGBO(192, 192, 192, 1)),
                              borderRadius: BorderRadius.circular(8)),
                          child: Theme(
                            data: Theme.of(context).copyWith(
                              canvasColor: Colors.white,
                            ),
                            child:
                            Obx(() =>  DropdownButtonHideUnderline(
                                child: DropdownButton(
                                  isExpanded: true,
                                  iconSize: 0,
                                  icon: const Visibility(
                                      visible: false,
                                      child: Icon(Icons.arrow_downward)),
                                  elevation: 16,
                                  hint: controller.countOfExercise.value != ''
                                      ? Row(
                                    children: [
                                      Text(
                                        '${controller.countOfExercise.value*5 +1} - ${(controller.countOfExercise.value+1)*5}',
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w400,
                                            color: ColorUtils.PRIMARY_COLOR),
                                      ),
                                      Expanded(child: Container()),
                                      const Icon(
                                        Icons.keyboard_arrow_down,
                                        color: Colors.black,
                                        size: 18,
                                      )
                                    ],
                                  )
                                      : Row(
                                    children: [
                                      Container(
                                        alignment: Alignment.center,
                                        padding: const EdgeInsets.symmetric(horizontal: 20),
                                        child:    Text(
                                          'abc',
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w400,
                                              color: Colors.black),
                                        ),
                                      ),
                                      Expanded(child: Container()),
                                      const Icon(
                                        Icons.keyboard_arrow_down,
                                        color: Colors.black,
                                        size: 18,
                                      )
                                    ],
                                  ),
                                  items: controller.listItemOfCountExercise.map(
                                        (value) {
                                      return DropdownMenuItem<int>(
                                        value: value,
                                        child: Text(
                                          '${value*5+1} - ${(value+1)*5}',
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w400,
                                              color: ColorUtils.PRIMARY_COLOR),
                                        ),
                                      );
                                    },
                                  ).toList(),
                                  onChanged: (int? value) {
                                    controller.countOfExercise.value = value!;
                                    controller.questionStudent.refresh();
                                    if((value+1)*5 >  controller.detailExam.value.questions!.length){
                                      controller.questionStudent.value = controller.detailExam.value.questions!.sublist(value*5, controller.detailExam.value.questions!.length);
                                    }else{
                                      controller.questionStudent.value = controller.detailExam.value.questions!.sublist(value*5, (value+1)*5);
                                    }

                                    if(controller.detailExam.value.isSubmit =="FALSE"){
                                      if((value+1)*5 >  controller.detailExam.value.questionAnswers!.length){
                                        controller.questionAnswer.value = controller.detailExam.value.questionAnswers!.sublist(value*5, controller.detailExam.value.questionAnswers!.length);
                                      }else{
                                        controller.questionAnswer.value = controller.detailExam.value.questionAnswers!.sublist(value*5, (value+1)*5);
                                      }
                                    }
                                    controller.questionStudent.refresh();

                                  },
                                ))),

                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(right: 6)),
                        const Icon(Icons.navigate_next_outlined,size: 24,),
                        const Spacer(),
                      ],
                    ):Container(),
                    controller.getWidgetContainer(),

                  ],
                ),
              )),
              Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child:

                Row(
                  children: [
                    Visibility(
                      visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,
                          StringConstant.FEATURE_EXAM_SAVE),
                      child: Expanded(child:
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                          onPressed: () {

                            for (int i = 0; i < controller.questionStudent.length; i++) {
                              if(controller.questionStudent[i].typeQuestion=="SELECTED_RESPONSE"){
                                controller.listAswer.add(controller.listkey[i]);
                              }
                              if(controller.questionStudent[i].typeQuestion=="CONSTRUCTED_RESPONSE"){
                                controller.listAswer.add(controller.listControllerNameExercise[i].text);
                              }

                              controller.questionAnswer.add(QuestionAnswers(
                                  answer: controller.listAswer[i],
                                  id: controller.questionStudent[i].id,
                                  files:controller.fileSaves!= []? controller.filesUploadExercise[i]: []
                              ));
                            }
                            controller.getSaveExams(controller.questionAnswer,
                                controller.files.isNotEmpty?
                                Get.find<FileUploadExerciseStudentController>().filesUploadExercise: controller.fileExercise,
                                "FALSE", controller.answer.value);
                            Get.back();
                            Get.find<ViewExamStudentController>().getListExamStudentToday('', '');

                          },
                          child: Text(
                            'Lưu bài làm',
                            style: TextStyle(
                                color:
                                Colors.white,
                                fontSize: 16.sp),
                          )),
                      ),
                    ),
                    Padding(padding:  EdgeInsets.only( right: 8.w)),
                    Visibility(
                      visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,
                          StringConstant.FEATURE_EXAM_SUBMIT),
                      child: Expanded(child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: ColorUtils.PRIMARY_COLOR,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)
                            )
                        ),
                        onPressed: (){
                          Get.dialog(
                              Center(
                                child: Wrap(children: [
                                  Container(
                                    height: 260,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius:
                                        BorderRadius.circular(
                                            16)),
                                    child: Stack(
                                      children: [
                                        Container(
                                          margin: const EdgeInsets
                                              .only(
                                              top: 50,
                                              right: 50,
                                              left: 50),
                                          height: 200,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                              BorderRadius
                                                  .circular(
                                                  16)),
                                          child: Column(
                                            children: [
                                              const Padding(
                                                  padding: EdgeInsets
                                                      .only(
                                                      top: 32)),
                                              Container(
                                                margin: const EdgeInsets.only(right: 16, left: 16),
                                                child:   Text(
                                                  "Sau khi nộp bạn sẽ không thể sửa đổi câu trả lời. Bạn có chắc muốn nộp bài?",
                                                  style: TextStyle(
                                                      color: const Color
                                                          .fromRGBO(
                                                          133,
                                                          133,
                                                          133,
                                                          1),
                                                      fontSize:
                                                      14.sp),
                                                ),
                                              ),
                                              Container(
                                                  color: Colors
                                                      .white,
                                                  padding:
                                                  EdgeInsets
                                                      .all(16
                                                      .h),
                                                  child: SizedBox(
                                                    width: double
                                                        .infinity,
                                                    height: 32,
                                                    child:

                                                    ElevatedButton(
                                                        style: ElevatedButton.styleFrom(backgroundColor: ColorUtils.PRIMARY_COLOR),
                                                        onPressed: () {

                                                          for (int i = 0; i < controller.questionStudent.length; i++) {
                                                            if(controller.questionStudent[i].typeQuestion=="SELECTED_RESPONSE"){
                                                              controller.listAswer.add(controller.listkey[i]);
                                                            }
                                                            if(controller.questionStudent[i].typeQuestion=="CONSTRUCTED_RESPONSE"){
                                                              controller.listAswer.add(controller.listControllerNameExercise[i].text);
                                                            }

                                                            controller.questionAnswer.add(QuestionAnswers(
                                                                answer: controller.listAswer[i],
                                                                id: controller.questionStudent[i].id,
                                                                files:controller.fileSaves!= []? controller.filesUploadExercise[i]: []
                                                            ));
                                                          }

                                                          checkClickFeature(Get.find<HomeController>().userGroupByApp,
                                                                  () {
                                                                controller.getSubmitExams(controller.questionAnswer,
                                                                    controller.files.isNotEmpty?
                                                                    Get.find<FileUploadExerciseStudentController>().filesUploadExercise:
                                                                    controller.fileExercise, "TRUE", controller.answer.value);
                                                                Get.back();
                                                                Get.back();
                                                                Get.find<ViewExamStudentController>().getListExamStudentToday('', '');
                                                              }, StringConstant.FEATURE_EXAM_SUBMIT);



                                                        },
                                                        child: Text(
                                                          'Nộp bài',
                                                          style: TextStyle(
                                                              color:
                                                              Colors.white,
                                                              fontSize: 16.sp),
                                                        )),
                                                  )),
                                              TextButton(
                                                  onPressed: () {
                                                    Get.back();
                                                  },
                                                  child: Text(
                                                    "Xem lại bài",
                                                    style: TextStyle(
                                                        color: Colors
                                                            .red,
                                                        fontSize:
                                                        16.sp),
                                                  ))
                                            ],
                                          ),
                                        ),
                                        Container(
                                            alignment:
                                            Alignment.center,
                                            margin:
                                            const EdgeInsets
                                                .only(
                                                top: 10),
                                            height: 80,
                                            child: Image.asset(
                                                "assets/images/image_app_logo.png"))
                                      ],
                                    ),
                                  ),
                                ]),
                              ));

                        },
                        child: Text('Nộp Bài', style: TextStyle(color: Colors.white, fontSize: 14.sp, fontWeight: FontWeight.w500),),
                      )),
                    )
                  ],
                ),
              )
            ],
          ),
        )): const LoadingCustom();
    });
  }

  Widget getFileWidget2(List<FilesExercise> listFile, index) {
    return Card(
      child: ListView.builder(
          padding: EdgeInsets.zero,
          itemCount: listFile.length,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, i) {
            return FileWidget2.itemFile(Get.context!,
                remove: true, file: listFile[i], function: () {
                  controller.filesUploadExercise[index].removeAt(i);
                  controller.listFile.refresh();
                  controller.filesUploadExercise.removeAt(i);
                });
          }),
    );
  }
}


