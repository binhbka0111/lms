import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/loading_custom.dart';
import '../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../student_home_controller.dart';
import '../../detail_subject_student_controller.dart';
import '../view_exam_student_controller.dart';
import 'detail_exam_student_controller.dart';

class DetailExamStudentPage extends GetWidget<DetailExamStudentController>{
  final controller = Get.put(DetailExamStudentController());

  @override
  Widget build(BuildContext context) {
   return GetBuilder<DetailExamStudentController>(builder: (controller){
     return
     controller.isReady ?
     Obx(() => Scaffold(
         backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
         appBar: AppBar(
           actions: [
             IconButton(
               onPressed: () {
                 Get.find<StudentHomeController>().subjectId.value = "";
                 Get.delete<DetailSubjectStudentController>();
                 Get.delete<ViewExamStudentController>();
                 Get.delete<DetailExamStudentController>();
                 comeToHome();

               },
               icon: const Icon(Icons.home),
               color: Colors.white,
             )
           ],
           backgroundColor: ColorUtils.PRIMARY_COLOR,
           title: Text(
             controller.detaileExams.value.title ?? "",
             style: TextStyle(
                 color: Colors.white,
                 fontSize: 16.sp,
                 fontFamily: 'static/Inter-Medium.ttf'),
           ),
         ),
         body:

        Obx(() =>  SingleChildScrollView(
          child:
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.all(16),
                color: const Color.fromRGBO(254, 230, 211, 1),
                child: Row(
                  children: [
                    Expanded(child:     Text(
                      controller.detaileExams.value.title ?? "",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w400),
                    ),),
                    Text(
                      "${controller.questionStudent.length} câu",
                      style: TextStyle(
                          color: ColorUtils.PRIMARY_COLOR,
                          fontSize: 14.sp,
                          fontWeight: FontWeight.w400),
                    )
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child:   Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(12),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Thời gian tạo: ",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Expanded(child: Container()),
                              Text(
                                controller.outputDateFormatmilisecond.format(DateTime.fromMillisecondsSinceEpoch(controller.detaileExams.value.createdAt ?? 0)),
                                style: TextStyle(
                                    color: const Color.fromRGBO(90, 90, 90, 1),
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14.sp),
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 16)),
                          Row(
                            children: [
                              Text(
                                "Thời gian bắt đầu: ",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Expanded(child: Container()),
                              Text(
                                controller.outputDateFormatmilisecond.format(DateTime.fromMillisecondsSinceEpoch(controller.detaileExams.value.startTime ?? 0)),
                                style: TextStyle(
                                    color: const Color.fromRGBO(90, 90, 90, 1),
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14.sp),
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 16)),
                          Row(
                            children: [
                              Text(
                                "Thời gian kết thúc: ",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Expanded(child: Container()),
                              Text(
                                controller.outputDateFormatmilisecond.format(DateTime.fromMillisecondsSinceEpoch(controller.detaileExams.value.endTime ?? 0)),
                                style: TextStyle(
                                    color: const Color.fromRGBO(90, 90, 90, 1),
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14.sp),
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 16)),
                          Row(
                            children: [
                              Text(
                                "Thời gian làm bài: ",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Expanded(child: Container()),
                              Text(
                                "${controller.detaileExams.value.examTime} phút",
                                style: TextStyle(
                                    color: const Color.fromRGBO(90, 90, 90, 1),
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14.sp),
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 16)),
                          Row(
                            children: [
                              Text("Ghi chú: ",style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w500),),
                              Expanded(child: Text("${controller.detaileExams.value.description}",style: TextStyle(
                                  color: const Color.fromRGBO(90, 90, 90, 1),
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14.sp),),)
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 16)),
                          Obx(() => Row(
                            children: [
                              Text(
                                "Trạng thái",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                              Expanded(child: Container()),
                              Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 12, vertical: 6),
                                height: 24.h,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: controller.getColorStatus(
                                        controller.detaileExams.value
                                            .statusExerciseByStudent),
                                    borderRadius:
                                    BorderRadius.circular(6)),
                                child: Text(
                                  "${controller.getTextStatus(controller.detaileExams.value.statusExerciseByStudent)}",
                                  style: TextStyle(
                                      color: controller
                                          .getColorTextStatus(controller
                                          .detaileExams
                                          .value
                                          .statusExerciseByStudent),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500),
                                ),
                              )
                            ],
                          )),
                          const Padding(padding: EdgeInsets.only(top: 16)),
                          controller.detaileExams.value.isPublicScore=="TRUE"?
                          Row(
                            children: [
                              Text("Tổng điểm đạt được: ",style: TextStyle(
                                  color: ColorUtils.PRIMARY_COLOR,
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w500),),
                              Expanded(child: Text("${controller.detaileExams.value.score} /${controller.detaileExams.value.scoreOfExam ?? ""} ",style: TextStyle(
                                  color: ColorUtils.PRIMARY_COLOR,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.sp),),)
                            ],
                          ):  Row(
                            children: [
                              Text("Điểm tối đa: ",style: TextStyle(
                                  color: ColorUtils.PRIMARY_COLOR,
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w500),),
                              Expanded(child: Text("${controller.detaileExams.value.scoreOfExam ?? ""} ",style: TextStyle(
                                  color: ColorUtils.PRIMARY_COLOR,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.sp),),)
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 16)),
                          const Divider(),
                        ],
                      ),
                    ),

                    Row(
                      children: [
                        Text("Nhận xét của giáo viên: ",style: TextStyle(
                            color: Colors.black,
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w500),),
                        Expanded(child: Text(controller.detaileExams.value.teacherComment ?? "",style: TextStyle(
                            color: const Color.fromRGBO(90, 90, 90, 1),
                            fontWeight: FontWeight.w400,
                            fontSize: 14.sp),),)
                      ],
                    ),
                    controller.detaileExams.value.typeExercise=="SELECTED_RESPONSE" ?
                    controller.listCountExercise.value <=1 ?Container():
                    Row(
                      children: [
                        const Spacer(),
                        const Icon(Icons.navigate_before_outlined,size: 24,),
                        const Padding(padding: EdgeInsets.only(right: 6)),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 6.w),
                          height: 40,
                          width:120,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: const Color.fromRGBO(192, 192, 192, 1)),
                              borderRadius: BorderRadius.circular(8)),
                          child: Theme(
                            data: Theme.of(context).copyWith(
                              canvasColor: Colors.white,
                            ),
                            child:
                            Obx(() =>  DropdownButtonHideUnderline(
                                child: DropdownButton(
                                  isExpanded: true,
                                  iconSize: 0,
                                  icon: const Visibility(
                                      visible: false,
                                      child: Icon(Icons.arrow_downward)),
                                  elevation: 16,
                                  hint: controller.countOfExercise.value != ''
                                      ? Row(
                                    children: [
                                      Text(
                                        '${controller.countOfExercise.value*5 +1} - ${(controller.countOfExercise.value+1)*5}',
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.w400,
                                            color: ColorUtils.PRIMARY_COLOR),
                                      ),
                                      Expanded(child: Container()),
                                      const Icon(
                                        Icons.keyboard_arrow_down,
                                        color: Colors.black,
                                        size: 18,
                                      )
                                    ],
                                  )
                                      : Row(
                                    children: [
                                      Container(
                                        alignment: Alignment.center,
                                        padding: const EdgeInsets.symmetric(horizontal: 20),
                                        child:    Text(
                                          'abc',
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w400,
                                              color: Colors.black),
                                        ),
                                      ),
                                      Expanded(child: Container()),
                                      const Icon(
                                        Icons.keyboard_arrow_down,
                                        color: Colors.black,
                                        size: 18,
                                      )
                                    ],
                                  ),
                                  items: controller.listItemOfCountExercise.map(
                                        (value) {
                                      return DropdownMenuItem<int>(
                                        value: value,
                                        child: Text(
                                          '${value*5+1} - ${(value+1)*5}',
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w400,
                                              color: ColorUtils.PRIMARY_COLOR),
                                        ),
                                      );
                                    },
                                  ).toList(),
                                  onChanged: (int? value) {
                                    controller.countOfExercise.value = value!;
                                    controller.questionStudent.refresh();
                                    if((value+1)*5 >  controller.detaileExams.value.questions!.length){
                                      controller.questionStudent.value = controller.detaileExams.value.questions!.sublist(value*5, controller.detaileExams.value.questions!.length);
                                    }else{
                                      controller.questionStudent.value = controller.detaileExams.value.questions!.sublist(value*5, (value+1)*5);
                                    }

                                    if(controller.detaileExams.value.isSubmit =="FALSE"){
                                      if((value+1)*5 >  controller.detaileExams.value.questionAnswers!.length){
                                        controller.questionAnswer.value = controller.detaileExams.value.questionAnswers!.sublist(value*5, controller.detaileExams.value.questionAnswers!.length);
                                      }else{
                                        controller.questionAnswer.value = controller.detaileExams.value.questionAnswers!.sublist(value*5, (value+1)*5);
                                      }
                                    }
                                    controller.questionStudent.refresh();

                                  },
                                ))),

                          ),
                        ),
                        const Padding(padding: EdgeInsets.only(right: 6)),
                        const Icon(Icons.navigate_next_outlined,size: 24,),
                        const Spacer(),
                      ],
                    ):Container(),

                  ],
                ),
              ),
              SizedBox(height: 16.h,),
              controller.getWidgetContainer(),

            ],
          ),
        ))
     )):
     const LoadingCustom();});
  }
}