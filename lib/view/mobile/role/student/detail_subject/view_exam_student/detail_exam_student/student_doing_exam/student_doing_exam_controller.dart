import 'dart:io';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/ViewPdf.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/file_device.dart';
import 'package:slova_lms/commom/utils/open_url.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import 'package:slova_lms/commom/widget/file_widget.dart';
import 'package:slova_lms/commom/widget/text_field_custom.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/model/common/learning_managerment.dart';
import 'package:slova_lms/data/model/common/req_file.dart';
import 'package:slova_lms/data/model/res/exercise/exercise.dart';
import 'package:slova_lms/data/repository/file/file_repo.dart';
import 'package:slova_lms/data/repository/learning_managerment/learning_managerment_repo.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/role/student/detail_subject/view_exam_student/view_exam_student_controller.dart';
import 'package:slova_lms/view/mobile/role/student/detail_subject/view_exercise/detail_exercise_student/student_doing_homework/file_upload_exercise_student/file_upload_exercise_student_page.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';
import '../../../view_exercise/detail_exercise_student/student_doing_homework/file_upload_exercise_student/file_upload_exercise_student_controller.dart';


class StudentDoingExamController extends GetxController{
  final LearningManagermentRepo _learningRepo = LearningManagermentRepo();
  var detailExam = DetailExerciser().obs;
  final FileRepo fileRepo = FileRepo();
  RxList<QuestionsStudent> questionStudent = <QuestionsStudent>[].obs;
  RxList<QuestionAnswers> questionAnswer = <QuestionAnswers>[].obs;
  RxList<QuestionAnswers> listquestionAnswer = <QuestionAnswers>[].obs;
  RxList<AnswerOptionExStudent> answerOption = <AnswerOptionExStudent>[].obs;
  RxList<FilesExercise> questionFile = <FilesExercise>[].obs;
  RxList<FilesExercise> fileUpLoad = <FilesExercise>[].obs;
  var outputDateFormatmilisecond = DateFormat('dd/MM/yyyy hh:mm:ss');
  var txtSubmitted ="Đã nộp bài".obs;
  var txtLateSubmission ="Nộp bài muộn".obs;
  var txtNotSubmission ="Không nộp bài".obs;
  var txtUnSubmited ="Chưa nộp bài".obs;
  var txtGraded ="".obs;
  var files = <ReqFile>[].obs;
  RxList<FocusNode> listFocusNameExercise = <FocusNode>[].obs;
  var focusNameExercise = FocusNode();
  var focusAttackLink = FocusNode();
  var listIndex = <int>[].obs;
  var controllerAttackLink = TextEditingController().obs;
  var controllerNameExercise = TextEditingController().obs;
  var indexAnsew = 0.obs;
  int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 30;
  RxList<FilesExercise> fileAnswerReponse = <FilesExercise>[].obs;
  var fileSaves = <dynamic>[].obs;
  var fileExercise= <FilesExercise>[].obs;
  RxList<TextEditingController> listControllerNameExercise = <TextEditingController>[].obs;
  String getTempIFSCValidation(String text) {
    return text.length > 7 ? "This line is helper text" : "";
  }
  var itemsEx = ItemsExercise().obs;
  var answer = "".obs;
  var listFile = <dynamic>[].obs;
  var countOfExercise = 0.obs;
  var   filesUploadExercise = <dynamic>[].obs;
  var listkey = <dynamic>[].obs;
  var listAswer = <String>[].obs;
  var filesAnswer= <FilesExercise>[].obs;
  var listIndexQuestion = <String>[].obs;
  var isReady = false;
  var listCountExercise =0.obs;
  var listItemOfCountExercise = <int>[].obs;
  var statusCode = 0.obs;






  @override
  void onInit() {
    super.onInit();
    var data = Get.arguments;
    if(data != null){
      itemsEx.value = data;
    }

    getDetailExercise();


  }

  getTypeExercise(state){
    switch(state){
      case "SELECTED_RESPONSE":
        return "Trắc nghiệm";
      case "CONSTRUCTED_RESPONSE":
        return "Tự luận";
      default:
        return "";
    }
  }
  getColorStatus(state) {
    switch (state) {
      case "SUBMITTED":
        return const Color.fromRGBO(192, 242, 220, 1);
      case "LATE_SUBMISSION":
        return const Color.fromRGBO(255, 236, 193, 1);
      case "NOT_SUBMISSION":
        return const Color.fromRGBO(252, 211, 215, 1);
      case "UNSUBMITTED":
        return const Color.fromRGBO(252, 211, 215, 1);
      default:
        return const Color.fromRGBO(246, 246, 246, 1);
    }
  }

  getColorTextStatus(state) {
    switch (state) {
      case "SUBMITTED":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "LATE_SUBMISSION":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "NOT_SUBMISSION":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "UNSUBMITTED":
        return const Color.fromRGBO(255, 69, 89, 1);
      default:
        return const Color.fromRGBO(136, 136, 136, 1);
    }
  }

  getTextStatus(state) {
    switch (state) {
      case "SUBMITTED":
        return txtSubmitted;
      case "LATE_SUBMISSION":
        return txtLateSubmission;
      case "NOT_SUBMISSION":
        return txtNotSubmission;
      case "UNSUBMITTED":
        return txtUnSubmited;
      default:
        return txtGraded;
    }
  }


  getDetailExercise() async {
    var idExercise = itemsEx.value.id;
    var idStudent= Get.find<StudentHomeController>().userProfile.value.id;
   await  _learningRepo.detailExamsStudent(idExercise,idStudent).then((value) {
      if (value.state == Status.SUCCESS) {
        detailExam.value= value.object!;
        if(detailExam.value.link != null){
          controllerAttackLink.value.text= detailExam.value.link!;
        }
        if(detailExam.value.questionAnswers==null){

        }else{
          questionAnswer.value= detailExam.value.questionAnswers!;
          for(int i = 0; i<questionAnswer.length; i++){
            if(detailExam.value.questionAnswers![i].files != null){
              fileSaves.add(detailExam.value.questionAnswers![i].files);
              var fileUp = <ReqFile>[].obs;
              listFile.add(fileUp);
              var listFileUp = <FilesExercise>[].obs;
              filesUploadExercise.add(listFileUp);
            }
            if(questionAnswer[i].typeQuestion =="SELECTED_RESPONSE"){
              answer.value =questionAnswer[i].answer !;
              listkey.add("$answer");
            }

          }
        }
        if(questionStudent.isNotEmpty){
          listCountExercise.value = (questionStudent.length/5).ceil();
        }

        for(int i = 0; i<listCountExercise.value; i++){
          listItemOfCountExercise.add(i);
        }

        if(detailExam.value.questions==null){

        }else{
          questionStudent.value = detailExam.value.questions!;
          for(int i = 0; i< questionStudent.length; i++){
            listIndexQuestion.add(questionStudent[i].id!);
          }
          for(int i=0; i< questionStudent.length; i++){
            listkey.add("$answer");
            listControllerNameExercise.add(TextEditingController());
            var fileUp = <ReqFile>[].obs;
            listFile.add(fileUp);
            var listFileUp = <FilesExercise>[].obs;
            filesUploadExercise.add(listFileUp);

          }

        }

        if(detailExam.value.files==null){

        }else{
          questionFile.value= detailExam.value.files!;
        }
        if (detailExam.value.filesAnswer == null) {


        } else {
          fileUpLoad.value = detailExam.value.filesAnswer!;
        }


        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      }
      else {
        if(statusCode.value == 400){
          Get.back();
          Get.find<ViewExamStudentController>().onInit();
          AppUtils().snackbarError("Thông báo: ", "Bài kiểm tra chưa đến giờ làm");
        }
      }


    });
   isReady= true;
   update();
  }
  getIconFile(ext){
    switch(ext){
      case "jpg":
        return "assets/icons/file/icon_image.png";
      case "png":
        return "assets/icons/file/icon_image.png";
      case "docx":
        return "assets/icons/file/icon_word.png";
      case "doc":
        return "assets/icons/file/icon_word.png";
      case "xls":
        return "assets/icons/file/icon_excel.png";
      case "xlsx":
        return "assets/icons/file/icon_excel.png";
      case "zip":
        return "assets/images/icon_image_zip.png";
      case "rar":
        return "assets/images/icon_image_rar.png";
      case "pptx":
        return "assets/icons/file/icon_ppt.png";
      case "ppt":
        return "assets/icons/file/icon_ppt.png";
      case "pdf":
        return "assets/icons/file/icon_pdf.png";
      default:
        return "assets/icons/file/icon_unknow.png";
    }

  }
  getAttackFileAnswerOption(index, i){
    var action = 0;
    if (fileSaves[index][i].ext == "png" ||
        fileSaves[index][i].ext == "jpg" ||
        fileSaves[index][i].ext == "jpeg" ||
        fileSaves[index][i].ext == "gif" ||
        fileSaves[index][i].ext == "bmp") {
      action = 1;
    } else if (fileSaves[index][i].ext == "pdf") {
      action = 2;
    } else {
      action = 0;
    }
    switch (action) {
      case 1:
        OpenUrl.openImageViewer(Get.context!,fileSaves[index][i].link!);
        break;
      case 2:
        Get.to(ViewPdfPage(url: fileSaves[index][i].link!));
        break;
      default:
        OpenUrl.openFile(fileSaves[index][i].link!);
        break;
    }
  }
  getAttackFile(index, h){
    var action = 0;
    if (questionStudent[index].files?[h].ext == "png" ||
        questionStudent[index].files?[h].ext == "jpg" ||
        questionStudent[index].files?[h].ext == "jpeg" ||
        questionStudent[index].files?[h].ext == "gif" ||
        questionStudent[index].files?[h].ext == "bmp") {
      action = 1;
    } else if (questionStudent[index].files?[h].ext == "pdf") {
      action = 2;
    } else {
      action = 0;
    }
    switch (action) {
      case 1:
        OpenUrl.openImageViewer(Get.context!,questionStudent[index].files?[h].link!);
        break;
      case 2:
        Get.to(ViewPdfPage(url: questionStudent[index].files![h].link!));
        break;
      default:
        OpenUrl.openFile(questionStudent[index].files![h].link!);
        break;
    }
  }

  getAttackFileQuestion(index){
    var action = 0;
    if (questionFile[index].ext == "png" ||
        questionFile[index].ext == "jpg" ||
        questionFile[index].ext == "jpeg" ||
        questionFile[index].ext == "gif" ||
        questionFile[index].ext == "bmp") {
      action = 1;
    } else if (questionFile[index].ext == "pdf") {
      action = 2;
    } else {
      action = 0;
    }
    switch (action) {
      case 1:
        OpenUrl.openImageViewer(Get.context!,questionFile[index].link!);
        break;
      case 2:
        Get.to(ViewPdfPage(url: questionFile[index].link!));
        break;
      default:
        OpenUrl.openFile(questionFile[index].link!);
        break;
    }
  }
  getAttackFileResult(index) {
    var action = 0;
    if (fileUpLoad[index].ext == "png" ||
        fileUpLoad[index].ext == "jpg" ||
        fileUpLoad[index].ext == "jpeg" ||
        fileUpLoad[index].ext == "gif" ||
        fileUpLoad[index].ext == "bmp") {
      action = 1;
    } else if (fileUpLoad[index].ext == "pdf") {
      action = 2;
    } else {
      action = 0;
    }
    switch (action) {
      case 1:
        OpenUrl.openImageViewer(Get.context!, fileUpLoad[index].link!);
        break;
      case 2:
        Get.to(ViewPdfPage(url: fileUpLoad[index].link!));
        break;
      default:
        OpenUrl.openFile(fileUpLoad[index].link!);
        break;
    }
  }

  getSubmitExams(questionAnswers,files,isSubmit,contentAnswer){
    var idExam = itemsEx.value.id;
    var idStudent= Get.find<StudentHomeController>().userProfile.value.id;
    _learningRepo.submitExam(questionAnswers,files,isSubmit,contentAnswer,idExam,idStudent).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
        Get.find<ViewExamStudentController>().getListExamStudentToday('', '');
        AppUtils.shared.showToast("Nộp bài thành công");
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("thất bại", value.message ?? "");
      }

    });
  }

  getSaveExams(questionAnswers,files,isSubmit,contentAnswer){
    var idExam = itemsEx.value.id;
    var idStudent= Get.find<StudentHomeController>().userProfile.value.id;
    _learningRepo.submitExam(questionAnswers,files,isSubmit,contentAnswer,idExam,idStudent).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Lưu bài thành công");
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("thất bại", value.message ?? "");
      }

    });
  }




  uploadFile(index,files) async {
    showDialogUploadFile();
    var fileList = <File>[];
    files.forEach((element) {
      fileList.add(element.file!);
    });
   await fileRepo.uploadFileExercise(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;
        filesUploadExercise[index].addAll(listResFile);
        filesUploadExercise.refresh();
        listFile[index].addAll(files);
        for(int i =0;i<files.length;i++){
          files[i].url = filesUploadExercise[index][i].link;
        }

        listFile.refresh();
        update();

      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
   Get.back();

  }




  uploadFileSave(index,files) async {
    showDialogUploadFile();
    var fileList = <File>[];
    files.forEach((element) {
      fileList.add(element.file!);
    });
 await   fileRepo.uploadFileExercise(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        listFile[index].addAll(files);
        listFile.refresh();
        var listResFile = value.object!;
        fileSaves[index].addAll(listResFile);
        fileSaves.refresh();
        listResFile.clear();
        listFile[index].clear();
        filesUploadExercise[index].addAll(fileSaves[index]);
        filesUploadExercise.refresh();

      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại",
            backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
 Get.back();

  }

  uploadFileSaveLink(file)async {
    showDialogUploadFile();

    var fileList = <File>[];
    file.forEach((element) {
      fileList.add(element.file!);
    });

   await fileRepo.uploadFileExercise(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        files.addAll(file);
        files.refresh();
        var listResFile = value.object!;
        filesAnswer.addAll(listResFile);
        filesAnswer.refresh();
        listResFile.clear();
        files.value = [];


      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
   Get.back();
  }



  Widget saveExam(){
    return
      Column(
        children: [
          ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: questionAnswer.length,
              itemBuilder: (context, index) {
                if (questionAnswer.isNotEmpty) {
                  listControllerNameExercise.add(TextEditingController());
                  listFocusNameExercise.add(FocusNode());
      if(questionAnswer[index].question!.answerOption!= null){
        answerOption.value = questionAnswer[index].question!.answerOption!;
      }
                  listFile.add(files);
                  listControllerNameExercise[index].text = questionAnswer[index].answer!;
                  filesUploadExercise[index] = questionAnswer[index].files;
                  for(int i = 0 ; i< answerOption.length; i++){
                    if(questionAnswer[index].answer == questionStudent[index].answerOption?[i].key){
                      listIndex.add(i);
                      listIndex[index] = i;
                    }else{
                      listIndex.add(-1);
                    }

                    if(questionAnswer[index].answer == questionStudent[index].answerOption?[i].key){
                      listIndex[index] = i;
                    }
                  }

                  fileExercise.value = questionStudent[index].files!;
                }
                else{
                  questionAnswer[index].answer ="";
                }
                return
                  Container(
                      margin: const EdgeInsets.only(top: 16),
                      padding: const EdgeInsets.all(16),

                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                  child: RichText(
                                      text:
                                      TextSpan(children: [
                                        TextSpan(
                                            text:
                                            "Câu ${getIndexQuestion(questionStudent[index].id)}: ",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontSize: 12.sp,
                                                fontWeight:
                                                FontWeight.w500)),
                                        TextSpan(
                                            text:
                                            "[${questionStudent[index].point ?? 0}đ]",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    248, 126, 47, 1),
                                                fontSize: 12.sp,
                                                fontWeight:
                                                FontWeight.w500)),
                                        TextSpan(
                                            text:
                                            " (${getTypeExercise(questionStudent[index].typeQuestion ?? 0)}) ",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    248, 126, 47, 1),
                                                fontSize: 12.sp,
                                                fontWeight:
                                                FontWeight.w500)),
                                        TextSpan(
                                            text:
                                            questionStudent[index].content ?? "",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 12.sp,
                                                fontWeight:
                                                FontWeight.w400)),
                                      ])))
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 8)),
                          Column(
                            children: [
                              fileExercise.isNotEmpty?  Container(
                                  padding:
                                  const EdgeInsets.all(12),
                                  decoration: BoxDecoration(
                                      color: const Color.fromRGBO(
                                          246, 246, 246, 1),
                                      borderRadius:
                                      BorderRadius
                                          .circular(8)),
                                  child:
                                  ListView.builder(
                                      shrinkWrap: true,
                                      physics: const NeverScrollableScrollPhysics(),
                                      itemCount: fileExercise.length,
                                      itemBuilder: (context, h){
                                        if(fileExercise.isNotEmpty){
                                        }else{
                                          fileExercise[h].name = "";
                                          fileExercise[h].size = "";
                                        }
                                        return
                                          questionStudent[index].files?[h].ext == "png" ||
                                              questionStudent[index].files?[h].ext == "jpg" ||
                                              questionStudent[index].files?[h].ext == "jpeg" ||
                                              questionStudent[index].files?[h].ext == "gif" ||
                                              questionStudent[index].files?[h].ext == "bmp"?
                                          InkWell(
                                              onTap: (){
                                                getAttackFile(index,h);
                                              },
                                              child:     Row(
                                                children: [

                                                  SizedBox(
                                                    width: 24,
                                                    height: 24,
                                                    child:
                                                    CacheNetWorkCustomBanner(urlImage: '${questionStudent[index].files?[h].link}',),

                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          right: 6.w)),
                                                  Expanded(
                                                    child:
                                                    Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .start,
                                                      children: [
                                                        Text(
                                                          '${questionStudent[index].files?[h].name}',
                                                          style: TextStyle(
                                                              color:
                                                              const Color.fromRGBO(
                                                                  26,
                                                                  59,
                                                                  112,
                                                                  1),
                                                              fontSize: 14.sp,
                                                              fontWeight:
                                                              FontWeight
                                                                  .w500),
                                                        ),
                                                        Text(
                                                          "${questionStudent[index].files?[h].size}",
                                                          style: TextStyle(
                                                              color:
                                                              const Color.fromRGBO(
                                                                  192,
                                                                  192,
                                                                  192,
                                                                  1),
                                                              fontSize: 10.sp,
                                                              fontWeight:
                                                              FontWeight
                                                                  .w400),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              )
                                          ): Row(
                                            children: [
                                              Image.asset(
                                                '${getIconFile(questionStudent[index].files?[h].ext)}',
                                                height: 24,
                                                width: 24,
                                              ),
                                              Padding(
                                                  padding: EdgeInsets
                                                      .only(
                                                      right:
                                                      6.w)),
                                              Expanded(child:   Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [
                                                  Text(
                                                    '${questionStudent[index].files?[h].name}',
                                                    style: TextStyle(
                                                        color: const Color
                                                            .fromRGBO(
                                                            26,
                                                            59,
                                                            112,
                                                            1),
                                                        fontSize:
                                                        14.sp,
                                                        fontWeight:
                                                        FontWeight
                                                            .w500),
                                                  ),
                                                  Text(
                                                    "${questionStudent[index].files?[h].size}",
                                                    style: TextStyle(
                                                        color: const Color
                                                            .fromRGBO(
                                                            192,
                                                            192,
                                                            192,
                                                            1),
                                                        fontSize:
                                                        10.sp,
                                                        fontWeight:
                                                        FontWeight
                                                            .w400),
                                                  ),
                                                ],
                                              ),),
                                              InkWell(
                                                onTap: (){getAttackFile(index, h);
                                                },
                                                child:         Image.asset(
                                                  'assets/images/icon_upfile_subject.png',
                                                  height: 16,
                                                  width: 16,
                                                ),
                                              )
                                            ],
                                          );

                                      })
                              ): Container(color: Colors.white,),
                              ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount:
                                  answerOption.length,
                                  itemBuilder: (context, i) {
                                    if (answerOption.isNotEmpty) {

                                    }else{
                                      answerOption[i]
                                          .key = "";
                                      answerOption[i]
                                          .value = "";
                                    }
                                    if(questionAnswer.isNotEmpty){

                                    }else{
                                      questionAnswer[i].answer ="";
                                    }
                                    return
                                      Obx(() =>   Column(
                                        children: [
                                          const Padding(
                                              padding: EdgeInsets.only(
                                                  top: 8)),
                                          Row(
                                            children: [
                                              InkWell(
                                                  onTap: (){
                                                    listIndex[index] = i;
                                                    listkey[index] = questionStudent[index].answerOption![i].key!;
                                                  },
                                                  child:
                                                  Obx(() => Container(
                                                    alignment: Alignment.center,
                                                    height: 30.h,
                                                    width: 30.h,
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: listIndex[index] == i ? ColorUtils.BG_BASE1 : const Color.fromRGBO(
                                                                133, 133, 133, 1),
                                                            width: 1),
                                                        color:listIndex[index] == i ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(255, 255, 255, 1),
                                                        borderRadius:
                                                        BorderRadius.circular(
                                                            30)),
                                                    child:
                                                    Text(
                                                      "${questionStudent[index].answerOption?[i].key!}",
                                                      style: TextStyle(
                                                          color:listIndex[index] == i?ColorUtils.BG_BASE1 :const Color.fromRGBO(
                                                              177, 177, 177, 1),
                                                          fontWeight:
                                                          FontWeight.w500,
                                                          fontSize: 14.sp),
                                                    ),
                                                  ))


                                              ),


                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      right: 8.w)),
                                              Expanded(
                                                  child: Container(
                                                    padding: const EdgeInsets.symmetric(
                                                        horizontal: 16,
                                                        vertical: 8),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: const Color.fromRGBO(
                                                                133, 133, 133, 1),
                                                            width: 1),
                                                        borderRadius:
                                                        BorderRadius.circular(
                                                            6)),
                                                    child: Text(
                                                      "${questionStudent[index].answerOption?[i].value}",
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 14.sp,
                                                          fontWeight:
                                                          FontWeight.w400),
                                                    ),
                                                  ))
                                            ],
                                          )
                                        ],
                                      ));
                                  }
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 4)),
                          questionStudent[index].typeQuestion =="CONSTRUCTED_RESPONSE"?
                          Container(
                              margin: const EdgeInsets.symmetric(vertical: 8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  OutlineBorderTextFormField2(
                                    enable: true,
                                    focusNode: listFocusNameExercise[index],
                                    iconPrefix: null,
                                    iconSuffix: null,
                                    state: StateType.DEFAULT,
                                    labelText: "Nhập câu trả lời",
                                    autofocus: false,
                                    controller: listControllerNameExercise[index],
                                    helperText: "",
                                    showHelperText: false,
                                    textInputAction: TextInputAction.next,
                                    isShowIconPrefix: false,
                                    keyboardType: TextInputType.text,
                                    validation: (textToValidate) {
                                      return
                                        getTempIFSCValidation(textToValidate);
                                    },
                                  ),
                                  const Padding(padding: EdgeInsets.only(top: 4)),
                                  // FileUploadExerciseStudentPage(),
                                  Obx(() =>  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment
                                        .start,
                                    children: [
                                      detailExam.value.isSubmit == "FALSE" ?   fileSaves[index].length != 0?
                                      DottedBorder(
                                          dashPattern: const [5, 5],
                                          radius: Radius.circular(6.r),
                                          borderType: BorderType.RRect,
                                          color: const Color.fromRGBO(192, 192, 192, 1),
                                          child: SizedBox(
                                              width: double.infinity,
                                              child:
                                              Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Obx(() =>  Card(
                                                    child:
                                                    ListView.builder(
                                                        padding: EdgeInsets.zero,
                                                        itemCount: fileSaves[index].length,
                                                        shrinkWrap: true,
                                                        physics: const NeverScrollableScrollPhysics(),
                                                        itemBuilder: (context, i) {
                                                          return       fileSaves[index][i].ext== "png" ||
                                                              fileSaves[index][i].ext  == "jpg" ||
                                                              fileSaves[index][i].ext  == "jpeg" ||
                                                              fileSaves[index][i].ext == "gif" ||
                                                              fileSaves[index][i].ext  == "bmp"?
                                                          InkWell(
                                                              onTap: (){
                                                                getAttackFileAnswerOption(index,i);
                                                              },
                                                              child:

                                                              Obx(() =>  Container(
                                                                margin: const EdgeInsets.symmetric(vertical: 8),
                                                                padding: const EdgeInsets.symmetric(horizontal: 16),
                                                                child:  Row(
                                                                  children: [

                                                                    SizedBox(
                                                                      width: 24,
                                                                      height: 24,
                                                                      child:
                                                                      CacheNetWorkCustomBanner(urlImage: '${fileSaves[index][i].link}',),

                                                                    ),
                                                                    Padding(
                                                                        padding: EdgeInsets.only(
                                                                            right: 6.w)),
                                                                    Expanded(
                                                                      child:
                                                                      Column(
                                                                        crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                        children: [
                                                                          Text(
                                                                            '${fileSaves[index][i].name }',
                                                                            style: TextStyle(
                                                                                color:
                                                                                const Color.fromRGBO(
                                                                                    26,
                                                                                    59,
                                                                                    112,
                                                                                    1),
                                                                                fontSize: 14.sp,
                                                                                fontWeight:
                                                                                FontWeight
                                                                                    .w500),
                                                                          ),
                                                                          Text(
                                                                            '${fileSaves[index][i].size}',
                                                                            style: TextStyle(
                                                                                color:
                                                                                const Color.fromRGBO(
                                                                                    192,
                                                                                    192,
                                                                                    192,
                                                                                    1),
                                                                                fontSize: 10.sp,
                                                                                fontWeight:
                                                                                FontWeight
                                                                                    .w400),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    GestureDetector(
                                                                        child: const Icon(
                                                                          Icons.delete_outline_outlined,
                                                                          color: ColorUtils.COLOR_WORK_TYPE_4,
                                                                          size: 24,
                                                                        ),
                                                                        onTap: () {
                                                                          fileSaves[index].removeAt(i);
                                                                          fileSaves.refresh();
                                                                        }),
                                                                  ],
                                                                ),
                                                              ))
                                                          ):
                                                          Row(
                                                            children: [
                                                              Image.asset(
                                                                '${getIconFile( fileSaves[index][i].ext )}',
                                                                height: 24,
                                                                width: 24,
                                                              ),
                                                              Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                      right:
                                                                      6.w)),
                                                              Expanded(child:   Column(
                                                                crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                                children: [
                                                                  Text(
                                                                    '${fileSaves[index][i].name }',
                                                                    style: TextStyle(
                                                                        color: const Color
                                                                            .fromRGBO(
                                                                            26,
                                                                            59,
                                                                            112,
                                                                            1),
                                                                        fontSize:
                                                                        14.sp,
                                                                        fontWeight:
                                                                        FontWeight
                                                                            .w500),
                                                                  ),
                                                                  Text(
                                                                    "${ fileSaves[index][i].size }",
                                                                    style: TextStyle(
                                                                        color: const Color
                                                                            .fromRGBO(
                                                                            192,
                                                                            192,
                                                                            192,
                                                                            1),
                                                                        fontSize:
                                                                        10.sp,
                                                                        fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                                  ),
                                                                ],
                                                              ),),
                                                              InkWell(
                                                                onTap: (){
                                                                  getAttackFileAnswerOption(index,i);
                                                                },
                                                                child:         Image.asset(
                                                                  'assets/images/icon_upfile_subject.png',
                                                                  height: 16,
                                                                  width: 16,
                                                                ),
                                                              )
                                                            ],
                                                          );
                                                        }),
                                                  )),

                                                  Padding(padding: EdgeInsets.only(top: 8.h)),
                                                  TextButton(
                                                      onPressed: () {
                                                        FileDevice.showSelectFileV2(Get.context!)
                                                            .then((value) {
                                                          if (value.isNotEmpty) {
                                                            listFile[index].addAll(value);
                                                            listFile.refresh();
                                                            uploadFileSave(index, value);
                                                          }
                                                        });
                                                      },
                                                      child: Text(
                                                        "Thêm tệp +",
                                                        style: TextStyle(
                                                            color: const Color.fromRGBO(
                                                                72, 98, 141, 1),
                                                            fontSize: 14.sp,
                                                            fontWeight: FontWeight.w400),
                                                      )),
                                                ],
                                              )

                                          )):
                                      InkWell(
                                        onTap: (){
                                          FileDevice.showSelectFileV2(Get.context!)
                                              .then((value) {
                                            if (value.isNotEmpty) {
                                              listFile[index].addAll(value);
                                              listFile.refresh();
                                              uploadFileSave(index,value);
                                            }
                                          });
                                        },
                                        child:   Container(
                                          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                                          decoration: BoxDecoration(
                                              color: ColorUtils.PRIMARY_COLOR,
                                              borderRadius: BorderRadius.circular(12)
                                          ),
                                          child:Text('Tải file',textAlign: TextAlign.center, style:  TextStyle(color:ColorUtils.BG_BASE1, fontSize: 12.sp , fontWeight: FontWeight.w400),),
                                        ),
                                      ): Container()
                                    ],
                                  ))
                                ],
                              )
                          ):Container()
                        ],
                      )
                  );

              }),



        ],
      );
  }
  Widget studingExam(){
    return
      Column(
        children: [
          ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount:questionStudent.length,
              itemBuilder: (context, index) {
                if (questionStudent.isNotEmpty) {
                  listControllerNameExercise.add(TextEditingController());
                  listFocusNameExercise.add(FocusNode());
                  answerOption.value =
                  questionStudent[index].answerOption!;
                  listIndex.add(-1);

                  fileExercise.value = questionStudent[index].files!;
                }
                // if(questionAnswer.isNotEmpty){
                //
                // }else{questionAnswer[index].answer ="";
                // }
                return
                  Container(
                      margin: const EdgeInsets.only(top: 16),
                      padding: const EdgeInsets.all(16),

                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                  child: RichText(
                                      text:
                                      TextSpan(children: [
                                        TextSpan(
                                            text:
                                            "Câu ${getIndexQuestion(questionStudent[index].id)}: ",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontSize: 12.sp,
                                                fontWeight:
                                                FontWeight.w500)),
                                        TextSpan(
                                            text:
                                            "[${questionStudent[index].point ?? 0}đ]",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    248, 126, 47, 1),
                                                fontSize: 12.sp,
                                                fontWeight:
                                                FontWeight.w500)),
                                        TextSpan(
                                            text:
                                            " (${getTypeExercise(questionStudent[index].typeQuestion ?? 0)}) ",
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    248, 126, 47, 1),
                                                fontSize: 12.sp,
                                                fontWeight:
                                                FontWeight.w500)),
                                        TextSpan(
                                            text:
                                            questionStudent[index].content ?? "",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 12.sp,
                                                fontWeight:
                                                FontWeight.w400)),
                                      ])))
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 8)),
                          Column(
                            children: [
                              fileExercise.isNotEmpty?  Container(
                                  padding:
                                  const EdgeInsets.all(12),
                                  decoration: BoxDecoration(
                                      color: const Color.fromRGBO(
                                          246, 246, 246, 1),
                                      borderRadius:
                                      BorderRadius
                                          .circular(8)),
                                  child:
                                  ListView.builder(
                                      shrinkWrap: true,
                                      physics: const NeverScrollableScrollPhysics(),
                                      itemCount: fileExercise.length,
                                      itemBuilder: (context, h){
                                        if(fileExercise.isNotEmpty){
                                        }else{
                                          fileExercise[h].name = "";
                                          fileExercise[h].size = "";
                                        }
                                        return
                                          questionStudent[index].files?[h].ext == "png" ||
                                              questionStudent[index].files?[h].ext == "jpg" ||
                                              questionStudent[index].files?[h].ext == "jpeg" ||
                                              questionStudent[index].files?[h].ext == "gif" ||
                                              questionStudent[index].files?[h].ext == "bmp"?
                                          InkWell(
                                              onTap: (){
                                                getAttackFile(index,h);
                                              },
                                              child:     Row(
                                                children: [

                                                  SizedBox(
                                                    width: 24,
                                                    height: 24,
                                                    child:
                                                    CacheNetWorkCustomBanner(urlImage: '${questionStudent[index].files?[h].link}',),

                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          right: 6.w)),
                                                  Expanded(
                                                    child:
                                                    Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .start,
                                                      children: [
                                                        Text(
                                                          '${questionStudent[index].files?[h].name}',
                                                          style: TextStyle(
                                                              color:
                                                              const Color.fromRGBO(
                                                                  26,
                                                                  59,
                                                                  112,
                                                                  1),
                                                              fontSize: 14.sp,
                                                              fontWeight:
                                                              FontWeight
                                                                  .w500),
                                                        ),
                                                        Text(
                                                          "${questionStudent[index].files?[h].size}",
                                                          style: TextStyle(
                                                              color:
                                                              const Color.fromRGBO(
                                                                  192,
                                                                  192,
                                                                  192,
                                                                  1),
                                                              fontSize: 10.sp,
                                                              fontWeight:
                                                              FontWeight
                                                                  .w400),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              )
                                          ):
                                          Row(
                                            children: [
                                              Image.asset(
                                                '${getIconFile(questionStudent[index].files?[h].ext)}',
                                                height: 24,
                                                width: 24,
                                              ),
                                              Padding(
                                                  padding: EdgeInsets
                                                      .only(
                                                      right:
                                                      6.w)),
                                              Expanded(child:   Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [
                                                  Text(
                                                    '${questionStudent[index].files?[h].name}',
                                                    style: TextStyle(
                                                        color: const Color
                                                            .fromRGBO(
                                                            26,
                                                            59,
                                                            112,
                                                            1),
                                                        fontSize:
                                                        14.sp,
                                                        fontWeight:
                                                        FontWeight
                                                            .w500),
                                                  ),
                                                  Text(
                                                    "${questionStudent[index].files?[h].size}",
                                                    style: TextStyle(
                                                        color: const Color
                                                            .fromRGBO(
                                                            192,
                                                            192,
                                                            192,
                                                            1),
                                                        fontSize:
                                                        10.sp,
                                                        fontWeight:
                                                        FontWeight
                                                            .w400),
                                                  ),
                                                ],
                                              ),),
                                              InkWell(
                                                onTap: (){
                                                  getAttackFile(index, h);
                                                },
                                                child:         Image.asset(
                                                  'assets/images/icon_upfile_subject.png',
                                                  height: 16,
                                                  width: 16,
                                                ),
                                              )
                                            ],
                                          );

                                      })
                              ): Container(color: Colors.white,),
                              ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount:
                                  answerOption.length,
                                  itemBuilder: (context, i) {
                                    if (answerOption.isNotEmpty) {
                                    }else{
                                      answerOption[i]
                                          .key = "";
                                      answerOption[i]
                                          .value = "";
                                    }

                                    return
                                      Column(
                                        children: [
                                          const Padding(
                                              padding: EdgeInsets.only(
                                                  top: 8)),
                                          Row(
                                            children: [
                                              InkWell(
                                                onTap: (){
                                                  listIndex[index] = i;
                                                  listkey[index] = questionStudent[index].answerOption![i].key!;
                                                },
                                                child:
                                                Obx(() => Container(
                                                  alignment: Alignment.center,
                                                  height: 30.h,
                                                  width: 30.h,
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color: listIndex[index] == i ? ColorUtils.BG_BASE1 : const Color.fromRGBO(
                                                              133, 133, 133, 1),
                                                          width: 1),
                                                      color:listIndex[index] == i  ? ColorUtils.PRIMARY_COLOR : const Color.fromRGBO(255, 255, 255, 1),
                                                      borderRadius:
                                                      BorderRadius.circular(
                                                          30)),
                                                  child:
                                                  Text(
                                                    "${questionStudent[index].answerOption?[i].key!}",
                                                    style: TextStyle(
                                                        color:listIndex[index] == i?ColorUtils.BG_BASE1 :const Color.fromRGBO(
                                                            177, 177, 177, 1),
                                                        fontWeight:
                                                        FontWeight.w500,
                                                        fontSize: 14.sp),
                                                  ),
                                                )),
                                              ),


                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      right: 8.w)),
                                              Expanded(
                                                  child: Container(
                                                    padding: const EdgeInsets.symmetric(
                                                        horizontal: 16,
                                                        vertical: 8),
                                                    decoration: BoxDecoration(
                                                        border: Border.all(
                                                            color: const Color.fromRGBO(
                                                                133, 133, 133, 1),
                                                            width: 1),
                                                        borderRadius:
                                                        BorderRadius.circular(
                                                            6)),
                                                    child: Text(
                                                      "${questionStudent[index].answerOption?[i].value}",
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 14.sp,
                                                          fontWeight:
                                                          FontWeight.w400),
                                                    ),
                                                  ))
                                            ],
                                          )
                                        ],
                                      );
                                  }
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(top: 4)),
                          questionStudent[index].typeQuestion =="CONSTRUCTED_RESPONSE"?

                          Container(
                              margin: const EdgeInsets.symmetric(vertical: 8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  OutlineBorderTextFormField2(
                                    enable: true,
                                    focusNode: listFocusNameExercise[index],
                                    iconPrefix: null,
                                    iconSuffix: null,
                                    state: StateType.DEFAULT,
                                    labelText: "Nhập câu trả lời",
                                    autofocus: false,
                                    controller: listControllerNameExercise[index],
                                    helperText: "",
                                    showHelperText: false,
                                    textInputAction: TextInputAction.next,
                                    isShowIconPrefix: false,
                                    keyboardType: TextInputType.text,
                                    validation: (textToValidate) {
                                      return
                                        getTempIFSCValidation(textToValidate);
                                    },
                                  ),
                                  const Padding(padding: EdgeInsets.only(top: 4)),
                                  // FileUploadExerciseStudentPage(),
                                  Obx(() =>  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment
                                        .start,
                                    children: [
                                      listFile[index]
                                          .isNotEmpty?
                                      DottedBorder(
                                          dashPattern: const [5, 5],
                                          radius: Radius.circular(6.r),
                                          borderType: BorderType.RRect,
                                          color: const Color.fromRGBO(192, 192, 192, 1),
                                          child: SizedBox(
                                              width: double.infinity,
                                              child:
                                              Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  getFileWidgets(listFile[index],index),
                                                  Padding(padding: EdgeInsets.only(top: 8.h)),
                                                  TextButton(
                                                      onPressed: () {
                                                        FileDevice.showSelectFileV2(Get.context!)
                                                            .then((value) {
                                                          if (value.isNotEmpty) {
                                                            uploadFile(index,value);
                                                          }
                                                        });
                                                      },
                                                      child: Text(
                                                        "Thêm tệp +",
                                                        style: TextStyle(
                                                            color: const Color.fromRGBO(
                                                                72, 98, 141, 1),
                                                            fontSize: 14.sp,
                                                            fontWeight: FontWeight.w400),
                                                      )),
                                                ],
                                              )

                                          )):
                                      InkWell(
                                        onTap: (){
                                          FileDevice.showSelectFileV2(Get.context!)
                                              .then((value) {
                                            if (value.isNotEmpty) {

                                              uploadFile(index,value);
                                            }
                                          });
                                        },
                                        child:   Container(
                                          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                                          decoration: BoxDecoration(
                                              color: ColorUtils.PRIMARY_COLOR,
                                              borderRadius: BorderRadius.circular(12)
                                          ),
                                          child:Text('Tải file',textAlign: TextAlign.center, style:  TextStyle(color:ColorUtils.BG_BASE1, fontSize: 12.sp , fontWeight: FontWeight.w400),),
                                        ),
                                      )
                                    ],
                                  )),
                                  // FileUploadExerciseStudentPage(),
                                ],
                              )
                          ):Container()
                        ],
                      )
                  );

              }),



        ],
      );
  }


  Widget studingFile(){
    return      Column(
      children: [
        Container(
          decoration: BoxDecoration(
              color: const Color.fromRGBO(246, 246, 246, 1),
              borderRadius: BorderRadius.circular(8)
          ),
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child:
          ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: questionFile.length,
              itemBuilder: (context, index){
                if(questionFile.isNotEmpty){
                }else{
                  questionFile[index].name = "";
                  questionFile[index].size = "";
                }
                return
                  questionFile[index].ext == "png" ||
                      questionFile[index].ext == "jpg" ||
                      questionFile[index].ext == "jpeg" ||
                      questionFile[index].ext== "gif" ||
                      questionFile[index].ext == "bmp"?
                  InkWell(
                      onTap: (){
                        getAttackFileQuestion(index);
                      },
                      child:

                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 8),
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child:  Row(
                          children: [

                            SizedBox(
                              width: 24,
                              height: 24,
                              child:
                              CacheNetWorkCustomBanner(urlImage:  '${questionFile[index].link}',),
                            ),
                            Padding(
                                padding: EdgeInsets.only(
                                    right: 6.w)),
                            Expanded(
                              child:
                              Column(
                                crossAxisAlignment:
                                CrossAxisAlignment
                                    .start,
                                children: [
                                  Text(
                                    '${questionFile[index].name}',
                                    style: TextStyle(
                                        color:
                                        const Color.fromRGBO(
                                            26,
                                            59,
                                            112,
                                            1),
                                        fontSize: 14.sp,
                                        fontWeight:
                                        FontWeight
                                            .w500),
                                  ),
                                  Text(
                                    '${questionFile[index].size}',
                                    style: TextStyle(
                                        color:
                                        const Color.fromRGBO(
                                            192,
                                            192,
                                            192,
                                            1),
                                        fontSize: 10.sp,
                                        fontWeight:
                                        FontWeight
                                            .w400),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                  ):
                  Row(
                    children: [
                      Image.asset(
                        '${getIconFile( questionFile[index].ext )}',
                        height: 24,
                        width: 24,
                      ),
                      Padding(
                          padding: EdgeInsets
                              .only(
                              right:
                              6.w)),
                      Expanded(child:   Column(
                        crossAxisAlignment:
                        CrossAxisAlignment
                            .start,
                        children: [
                          Text(
                            '${ questionFile[index].name}',
                            style: TextStyle(
                                color: const Color
                                    .fromRGBO(
                                    26,
                                    59,
                                    112,
                                    1),
                                fontSize:
                                14.sp,
                                fontWeight:
                                FontWeight
                                    .w500),
                          ),
                          Text(
                            "${ questionFile[index].size}",
                            style: TextStyle(
                                color: const Color
                                    .fromRGBO(
                                    192,
                                    192,
                                    192,
                                    1),
                                fontSize:
                                10.sp,
                                fontWeight:
                                FontWeight
                                    .w400),
                          ),
                        ],
                      ),),
                      InkWell(
                        onTap: (){
                          getAttackFileQuestion(index);
                        },
                        child:         Image.asset(
                          'assets/images/icon_upfile_subject.png',
                          height: 16,
                          width: 16,
                        ),
                      )
                    ],
                  );

              }),

        ),
        const Padding(padding: EdgeInsets.only(top: 16)),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child:  OutlineBorderTextFormField2(
            enable: true,
            focusNode: focusNameExercise,
            iconPrefix: null,
            iconSuffix: null,
            state: StateType.DEFAULT,
            labelText: "Nhập câu trả lời",
            autofocus: false,
            controller: controllerNameExercise.value,
            helperText: "",
            showHelperText: false,
            textInputAction: TextInputAction.next,
            isShowIconPrefix: false,
            keyboardType: TextInputType.text,
            validation: (textToValidate) {
              return
                getTempIFSCValidation(textToValidate);
            },
          ),
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child:  FileUploadExerciseStudentPage(),
        ),
      ],
    );
  }
  Widget saveFile(){
    return      Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
              color: const Color.fromRGBO(246, 246, 246, 1),
              borderRadius: BorderRadius.circular(8)
          ),
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child:
          ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: questionFile.length,
              itemBuilder: (context, index){
                if(questionFile.isNotEmpty){
                }else{
                  questionFile[index].name = "";
                  questionFile[index].size = "";
                }
                return
                  questionFile[index].ext == "png" ||
                      questionFile[index].ext == "jpg" ||
                      questionFile[index].ext == "jpeg" ||
                      questionFile[index].ext== "gif" ||
                      questionFile[index].ext == "bmp"?
                  InkWell(
                      onTap: (){
                        getAttackFileQuestion(index);
                      },
                      child:

                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 8),
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child:  Row(
                          children: [

                            SizedBox(
                              width: 24,
                              height: 24,
                              child:

                              CacheNetWorkCustomBanner(urlImage: '${questionFile[index].link}',),

                            ),
                            Padding(
                                padding: EdgeInsets.only(
                                    right: 6.w)),
                            Expanded(
                              child:
                              Column(
                                crossAxisAlignment:
                                CrossAxisAlignment
                                    .start,
                                children: [
                                  Text(
                                    '${questionFile[index].name}',
                                    style: TextStyle(
                                        color:
                                        const Color.fromRGBO(
                                            26,
                                            59,
                                            112,
                                            1),
                                        fontSize: 14.sp,
                                        fontWeight:
                                        FontWeight
                                            .w500),
                                  ),
                                  Text(
                                    '${questionFile[index].size}',
                                    style: TextStyle(
                                        color:
                                        const Color.fromRGBO(
                                            192,
                                            192,
                                            192,
                                            1),
                                        fontSize: 10.sp,
                                        fontWeight:
                                        FontWeight
                                            .w400),
                                  ),
                                ],
                              ),
                            ),

                          ],
                        ),
                      )
                  ):
                  Row(
                    children: [
                      Image.asset(
                        '${getIconFile( questionFile[index].ext )}',
                        height: 24,
                        width: 24,
                      ),
                      Padding(
                          padding: EdgeInsets
                              .only(
                              right:
                              6.w)),
                      Expanded(child:   Column(
                        crossAxisAlignment:
                        CrossAxisAlignment
                            .start,
                        children: [
                          Text(
                            '${ questionFile[index].name}',
                            style: TextStyle(
                                color: const Color
                                    .fromRGBO(
                                    26,
                                    59,
                                    112,
                                    1),
                                fontSize:
                                14.sp,
                                fontWeight:
                                FontWeight
                                    .w500),
                          ),
                          Text(
                            "${ questionFile[index].size}",
                            style: TextStyle(
                                color: const Color
                                    .fromRGBO(
                                    192,
                                    192,
                                    192,
                                    1),
                                fontSize:
                                10.sp,
                                fontWeight:
                                FontWeight
                                    .w400),
                          ),
                        ],
                      ),),
                      InkWell(
                        onTap: (){
                          getAttackFileQuestion(index);
                        },
                        child:         Image.asset(
                          'assets/images/icon_upfile_subject.png',
                          height: 16,
                          width: 16,
                        ),
                      )
                    ],
                  );

              }),

        ),
        const Padding(padding: EdgeInsets.only(top: 16)),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child:  OutlineBorderTextFormField2(
            enable: true,
            focusNode: focusNameExercise,
            iconPrefix: null,
            iconSuffix: null,
            state: StateType.DEFAULT,
            labelText: "Nhập câu trả lời",
            autofocus: false,
            controller: controllerNameExercise.value,
            helperText: "",
            showHelperText: false,
            textInputAction: TextInputAction.next,
            isShowIconPrefix: false,
            keyboardType: TextInputType.text,
            validation: (textToValidate) {
              return
                getTempIFSCValidation(textToValidate);
            },
          ),
        ),
        Container(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child:
            filesAnswer.isNotEmpty ?
            DottedBorder(
                dashPattern: const [5, 5],
                radius: Radius.circular(6.r),
                borderType: BorderType.RRect,
                color: const Color.fromRGBO(192, 192, 192, 1),
                child: SizedBox(
                    width: double.infinity,
                    child:
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Card(
                          child: ListView.builder(
                              padding: EdgeInsets.zero,
                              itemCount: filesAnswer.length,
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, i) {
                                return        filesAnswer[i].ext == "png" ||
                                    filesAnswer[i].ext  == "jpg" ||
                                    filesAnswer[i].ext  == "jpeg" ||
                                    filesAnswer[i].ext == "gif" ||
                                    filesAnswer[i].ext  == "bmp"?
                                InkWell(
                                    onTap: (){
                                      getAttackFileQuestion(i);
                                    },
                                    child:

                                    Container(
                                      margin: const EdgeInsets.symmetric(vertical: 8),
                                      padding: const EdgeInsets.symmetric(horizontal: 16),
                                      child:  Row(
                                        children: [

                                          SizedBox(
                                            width: 24,
                                            height: 24,
                                            child:
                                            CacheNetWorkCustomBanner(urlImage:  '${filesAnswer[i].link }',),

                                          ),
                                          Padding(
                                              padding: EdgeInsets.only(
                                                  right: 6.w)),
                                          Expanded(
                                            child:
                                            Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment
                                                  .start,
                                              children: [
                                                Text(
                                                  '${filesAnswer[i].name }',
                                                  style: TextStyle(
                                                      color:
                                                      const Color.fromRGBO(
                                                          26,
                                                          59,
                                                          112,
                                                          1),
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                      FontWeight
                                                          .w500),
                                                ),
                                                Text(
                                                  '${filesAnswer[i].size }',
                                                  style: TextStyle(
                                                      color:
                                                      const Color.fromRGBO(
                                                          192,
                                                          192,
                                                          192,
                                                          1),
                                                      fontSize: 10.sp,
                                                      fontWeight:
                                                      FontWeight
                                                          .w400),
                                                ),
                                              ],
                                            ),
                                          ),
                                          GestureDetector(
                                              child: const Icon(
                                                Icons.delete_outline_outlined,
                                                color: ColorUtils.COLOR_WORK_TYPE_4,
                                                size: 24,
                                              ),
                                              onTap: () {
                                                filesAnswer.removeAt(i);
                                                filesAnswer.refresh();
                                              }),
                                        ],
                                      ),
                                    )
                                ):
                                Row(
                                  children: [
                                    Image.asset(
                                      '${getIconFile( filesAnswer[i].ext )}',
                                      height: 24,
                                      width: 24,
                                    ),
                                    Padding(
                                        padding: EdgeInsets
                                            .only(
                                            right:
                                            6.w)),
                                    Expanded(child:   Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment
                                          .start,
                                      children: [
                                        Text(
                                          '${ filesAnswer[i].name }',
                                          style: TextStyle(
                                              color: const Color
                                                  .fromRGBO(
                                                  26,
                                                  59,
                                                  112,
                                                  1),
                                              fontSize:
                                              14.sp,
                                              fontWeight:
                                              FontWeight
                                                  .w500),
                                        ),
                                        Text(
                                          "${ filesAnswer[i].size }",
                                          style: TextStyle(
                                              color: const Color
                                                  .fromRGBO(
                                                  192,
                                                  192,
                                                  192,
                                                  1),
                                              fontSize:
                                              10.sp,
                                              fontWeight:
                                              FontWeight
                                                  .w400),
                                        ),
                                      ],
                                    ),),
                                    InkWell(
                                      onTap: (){
                                        getAttackFileQuestion(i);
                                      },
                                      child:         Image.asset(
                                        'assets/images/icon_upfile_subject.png',
                                        height: 16,
                                        width: 16,
                                      ),
                                    )
                                  ],
                                );
                              }),
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        TextButton(
                            onPressed: () {
                              FileDevice.showSelectFileV2(Get.context!)
                                  .then((value) {
                                if (value.isNotEmpty) {

                                  uploadFileSaveLink(value);

                                }
                              });
                            },
                            child: Text(
                              "Thêm tệp +",
                              style: TextStyle(
                                  color: const Color.fromRGBO(
                                      72, 98, 141, 1),
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w400),
                            )),
                      ],
                    )

                )):
            InkWell(
              onTap: (){
                FileDevice.showSelectFileV2(Get.context!)
                    .then((value) {
                  if (value.isNotEmpty) {
                    // files.addAll(value);
                    // files.refresh();
                    uploadFileSaveLink(value);
                  }
                });
              },
              child:   Container(
                padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                decoration: BoxDecoration(
                    color: ColorUtils.PRIMARY_COLOR,
                    borderRadius: BorderRadius.circular(12)
                ),
                child:Text('Tải file',textAlign: TextAlign.center, style:  TextStyle(color:ColorUtils.BG_BASE1, fontSize: 12.sp , fontWeight: FontWeight.w400),),
              ),
            )
        ),
      ],
    );
  }

  Widget studingLink(){
    return    Column(
      children: [
        Container(
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border:  Border.all(color: ColorUtils.colorGray, width: 1)
            ),
            margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Đường dẫn: ", style: TextStyle(color: Colors.black, fontSize: 14.sp, fontWeight: FontWeight.w400),),
                const SizedBox(height: 8,),
                InkWell(
                  onTap: (){
                    OpenUrl.openLaunch(detailExam.value.link!);
                  },
                  child:
                  Text(detailExam.value.link?? "", style: TextStyle(color: Colors.blue, fontSize: 12.sp, fontWeight: FontWeight.w400),),
                ),
              ],
            )
        ),
        const Padding(padding: EdgeInsets.only(top: 16)),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child:  OutlineBorderTextFormField2(
            enable: true,
            focusNode: focusNameExercise,
            iconPrefix: null,
            iconSuffix: null,
            state: StateType.DEFAULT,
            labelText: "Nhập câu trả lời",
            autofocus: false,
            controller: controllerNameExercise.value,
            helperText: "",
            showHelperText: false,
            textInputAction: TextInputAction.next,
            isShowIconPrefix: false,
            keyboardType: TextInputType.text,
            validation: (textToValidate) {
              return
                getTempIFSCValidation(textToValidate);
            },
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 16),
          child: FileUploadExerciseStudentPage(),
        )
      ],
    );

  }
  Widget saveLink(){
    return    Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border:  Border.all(color: ColorUtils.colorGray, width: 1)
            ),
            margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Đường dẫn: ", style: TextStyle(color: Colors.black, fontSize: 14.sp, fontWeight: FontWeight.w400),),
                const SizedBox(height: 8,),
                InkWell(
                  onTap: (){
                    OpenUrl.openLaunch(detailExam.value.link!);
                  },
                  child:
                  Text(detailExam.value.link?? "", style: TextStyle(color: Colors.blue, fontSize: 12.sp, fontWeight: FontWeight.w400),),
                ),
              ],
            )
        ),
        const Padding(padding: EdgeInsets.only(top: 16)),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child:  OutlineBorderTextFormField2(
            enable: true,
            focusNode: focusNameExercise,
            iconPrefix: null,
            iconSuffix: null,
            state: StateType.DEFAULT,
            labelText: "Nhập câu trả lời",
            autofocus: false,
            controller: controllerNameExercise.value,
            helperText: "",
            showHelperText: false,
            textInputAction: TextInputAction.next,
            isShowIconPrefix: false,
            keyboardType: TextInputType.text,
            validation: (textToValidate) {
              return
                getTempIFSCValidation(textToValidate);
            },
          ),
        ),
        Obx(() => Container(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child:

            filesAnswer.isNotEmpty ?
            DottedBorder(
                dashPattern: const [5, 5],
                radius: Radius.circular(6.r),
                borderType: BorderType.RRect,
                color: const Color.fromRGBO(192, 192, 192, 1),
                child: SizedBox(
                    width: double.infinity,
                    child:
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Card(
                          child: ListView.builder(
                              padding: EdgeInsets.zero,
                              itemCount: filesAnswer.length,
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (context, i) {
                                return        filesAnswer[i].ext == "png" ||
                                    filesAnswer[i].ext  == "jpg" ||
                                    filesAnswer[i].ext  == "jpeg" ||
                                    filesAnswer[i].ext == "gif" ||
                                    filesAnswer[i].ext  == "bmp"?
                                InkWell(
                                    onTap: (){
                                      getAttackFileQuestion(i);
                                    },
                                    child:

                                    Container(
                                      margin: const EdgeInsets.symmetric(vertical: 8),
                                      padding: const EdgeInsets.symmetric(horizontal: 16),
                                      child:  Row(
                                        children: [

                                          SizedBox(
                                            width: 24,
                                            height: 24,
                                            child:
                                            CacheNetWorkCustomBanner(urlImage:   '${filesAnswer[i].link }',),
                                          ),
                                          Padding(
                                              padding: EdgeInsets.only(
                                                  right: 6.w)),
                                          Expanded(
                                            child:
                                            Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment
                                                  .start,
                                              children: [
                                                Text(
                                                  '${filesAnswer[i].name }',
                                                  style: TextStyle(
                                                      color:
                                                      const Color.fromRGBO(
                                                          26,
                                                          59,
                                                          112,
                                                          1),
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                      FontWeight
                                                          .w500),
                                                ),
                                                Text(
                                                  '${filesAnswer[i].size }',
                                                  style: TextStyle(
                                                      color:
                                                      const Color.fromRGBO(
                                                          192,
                                                          192,
                                                          192,
                                                          1),
                                                      fontSize: 10.sp,
                                                      fontWeight:
                                                      FontWeight
                                                          .w400),
                                                ),
                                              ],
                                            ),
                                          ),
                                          GestureDetector(
                                              child: const Icon(
                                                Icons.delete_outline_outlined,
                                                color: ColorUtils.COLOR_WORK_TYPE_4,
                                                size: 24,
                                              ),
                                              onTap: () {
                                                filesAnswer.removeAt(i);
                                                filesAnswer.refresh();
                                              }),
                                        ],
                                      ),
                                    )
                                ):
                                Row(
                                  children: [
                                    Image.asset(
                                      '${getIconFile( filesAnswer[i].ext )}',
                                      height: 24,
                                      width: 24,
                                    ),
                                    Padding(
                                        padding: EdgeInsets
                                            .only(
                                            right:
                                            6.w)),
                                    Expanded(child:   Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment
                                          .start,
                                      children: [
                                        Text(
                                          '${ filesAnswer[i].name }',
                                          style: TextStyle(
                                              color: const Color
                                                  .fromRGBO(
                                                  26,
                                                  59,
                                                  112,
                                                  1),
                                              fontSize:
                                              14.sp,
                                              fontWeight:
                                              FontWeight
                                                  .w500),
                                        ),
                                        Text(
                                          "${ filesAnswer[i].size }",
                                          style: TextStyle(
                                              color: const Color
                                                  .fromRGBO(
                                                  192,
                                                  192,
                                                  192,
                                                  1),
                                              fontSize:
                                              10.sp,
                                              fontWeight:
                                              FontWeight
                                                  .w400),
                                        ),
                                      ],
                                    ),),
                                    InkWell(
                                      onTap: (){
                                        getAttackFileQuestion(i);
                                      },
                                      child:         Image.asset(
                                        'assets/images/icon_upfile_subject.png',
                                        height: 16,
                                        width: 16,
                                      ),
                                    )
                                  ],
                                );
                              }),
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        TextButton(
                            onPressed: () {
                              FileDevice.showSelectFileV2(Get.context!)
                                  .then((value) {
                                if (value.isNotEmpty) {
                                  // files.addAll(value);
                                  // files.refresh();
                                  uploadFileSaveLink(value);

                                }
                              });
                            },
                            child: Text(
                              "Thêm tệp +",
                              style: TextStyle(
                                  color: const Color.fromRGBO(
                                      72, 98, 141, 1),
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w400),
                            )),
                      ],
                    )

                )):
            InkWell(
              onTap: (){
                FileDevice.showSelectFileV2(Get.context!)
                    .then((value) {
                  if (value.isNotEmpty) {
                    // files.addAll(value);
                    // files.refresh();
                    uploadFileSaveLink(value);
                  }
                });
              },
              child:   Container(
                padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                decoration: BoxDecoration(
                    color: ColorUtils.PRIMARY_COLOR,
                    borderRadius: BorderRadius.circular(12)
                ),
                child:Text('Tải file',textAlign: TextAlign.center, style:  TextStyle(color:ColorUtils.BG_BASE1, fontSize: 12.sp , fontWeight: FontWeight.w400),),
              ),
            )
        )),
      ],
    );
  }

  Widget getFileWidgets(List<ReqFile> listFile, index) {
    return Card(
      child: ListView.builder(
          padding: EdgeInsets.zero,
          itemCount: listFile.length,
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, i) {
            return FileWidget.itemFile(Get.context!,
                remove: true, file: listFile[i], function: () {
                  listFile.removeAt(i);
                });
          }),
    );
  }

  getWidgetContainer(){

     if(detailExam.value.isSubmit== null){
        if (questionStudent.isNotEmpty) {
          if(questionAnswer.isNotEmpty){
            return saveExam();
          }else{
            return studingExam();
          }
        } else if (detailExam.value.files!= null && detailExam.value.link == null) {
          if(detailExam.value.filesAnswer != null || detailExam.value.contentAnswer != null){
            return saveFile();
          }else{
            return studingFile();
          }
        } else if(detailExam.value.link!= null){
          if(detailExam.value.filesAnswer != null || detailExam.value.contentAnswer != null){
            return saveLink();
          }else{
            return studingLink();
          }

        }else{
          return Container();
        }
      } else if(detailExam.value.isSubmit=="FALSE"){
        if (questionStudent.isNotEmpty) {
          if(questionAnswer.isNotEmpty){
            return saveExam();
          }else{
            return studingExam();
          }
        } else if (detailExam.value.files!= null && detailExam.value.link == null) {
          if(detailExam.value.filesAnswer != null || detailExam.value.contentAnswer != null){
            return saveFile();
          }else{
            return studingFile();
          }
        } else if(detailExam.value.link!= null){
          if(detailExam.value.filesAnswer != null || detailExam.value.contentAnswer != null){
            return saveLink();
          }else{
            return studingLink();
          }
        }else{
          return Container();
        }
      } else{
      if (questionAnswer.isNotEmpty) {
        return saveExam();
      } else if (detailExam.value.files!= null && detailExam.value.link == null && questionStudent.isEmpty ) {
        return saveFile();
      } else {
        return saveLink();
      }
    }
  }
  checkSubmitted(){
    if(detailExam.value.isSubmit=="FALSE"){
      if(detailExam.value.files!= null && detailExam.value.link == null){
        if(filesAnswer.isEmpty){
          return Get.put(FileUploadExerciseStudentController()).filesAnswer;
        }else{
          return filesAnswer;
        }
      }else if(detailExam.value.link != null){
        if(filesAnswer.isEmpty){
          return Get.put(FileUploadExerciseStudentController()).filesAnswer;
        }else{
          return filesAnswer;
        }
      }else{
        return [];
      }
    }
    else{
      return Get.put(FileUploadExerciseStudentController()).filesAnswer;
    }
  }

  goToViewExercisePage(){
    Get.toNamed(Routes.viewExerciseStudentPage);
  }
  getIndexQuestion(id){
    return  (listIndexQuestion.indexOf(id))+1;
  }

}