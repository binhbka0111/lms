import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/model/common/learning_managerment.dart';
import 'package:slova_lms/data/repository/learning_managerment/learning_managerment_repo.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/subjects/subjects_controller.dart';

import '../../../../notification/notification_controller.dart';

class ViewExamStudentController extends GetxController{
  final LearningManagermentRepo _learningRepo = LearningManagermentRepo();
  var focusdateStart = FocusNode().obs;
  var controllerDateStart = TextEditingController().obs;
  var focusdateEnd = FocusNode().obs;
  var controllerDateEnd = TextEditingController().obs;
  var dateStart ="".obs;
  var dateEnd ="".obs;
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;
  var  listExam = ExerciseStudent().obs;
  RxList<ItemsExercise> items = <ItemsExercise>[].obs;
  var itemLoadMoreExam = <ItemsExercise>[].obs;
  var outputDateformat = DateFormat('dd/MM/yyyy HH:mm:ss');
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var showhiden =<bool>[].obs;
  var statusExercise = "".obs;
  var statusResultExx = "".obs;
  var txtSubmitted ="Đã nộp bài".obs;
  var txtLateSubmission ="Nộp bài muộn".obs;
  var txtNotSubmission ="Không nộp bài".obs;
  var txtUnSubmited ="Chưa nộp bài".obs;
  var txtGraded ="Đã chấm điểm".obs;
  var statusExerciseStudent = ''.obs;
  var controllerExam = ScrollController();
  var indexPage = 1.obs;
  var statusCode =0.obs;

  @override
  void onInit() {
    super.onInit();


    if(DateTime.now().month<=12&&DateTime.now().month>9){
      controllerDateStart.value.text =  outputDateFormat.format(DateTime(Get.find<StudentHomeController>().fromYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<StudentHomeController>().fromYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
    }else{
      controllerDateStart.value.text =   outputDateFormat.format(DateTime(Get.find<StudentHomeController>().toYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<StudentHomeController>().toYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
    }
    getListExamStudentToday(statusExercise.value,statusResultExx.value);
    controllerExam = ScrollController()..addListener(_scrollExams);


  }

  @override
  dispose() {
    controllerExam.dispose();
    super.dispose();
  }

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }
  getListExamStudentToday(status,statusEx){
    var subjectId = "";
    if(Get.find<StudentHomeController>().subjectId.value != ""){
      subjectId = Get.find<StudentHomeController>().subjectId.value;
    }else  if(Get.isRegistered<SubjectController>()){
      subjectId = Get.find<SubjectController>().idSubject.value;
    }else{
      subjectId = Get.find<NotificationController>().subjectId;
    }
    var studentId = Get.find<StudentHomeController>().userProfile.value.id;
    var fromdate = DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2),),00,00).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2)),23,59).millisecondsSinceEpoch;
    _learningRepo.listExamStudent(subjectId,status,fromdate, todate,studentId,statusEx,"","").then((value) {
      if (value.state == Status.SUCCESS) {
        listExam.value= value.object!;
        if(listExam.value.items != null || listExam.value.items !=""){
          items.value = listExam.value.items!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      }else{
        if(statusCode.value== 400){
          AppUtils().snackbarError("Bài kiểm tra chưa đến giờ làm", "");
        }
      }

    });
  }
  void _scrollExams(){
    if(controllerExam.position.extentAfter==0){
      var subjectId = "";
      if(Get.find<StudentHomeController>().subjectId.value != ""){
        subjectId = Get.find<StudentHomeController>().subjectId.value;
      }else  if(Get.isRegistered<SubjectController>()){
        subjectId = Get.find<SubjectController>().idSubject.value;
      }else{
        subjectId = Get.find<NotificationController>().subjectId;
      }
      if(itemLoadMoreExam.isNotEmpty){
        indexPage++;
      }
      _learningRepo.listExamStudent(subjectId,"",DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2),),00,00).millisecondsSinceEpoch,
          DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2)),23,59).millisecondsSinceEpoch,
          Get.put(StudentHomeController()).userProfile.value.id,"",indexPage.value,20).then((value) {
        if (value.state == Status.SUCCESS) {
          itemLoadMoreExam.value =[];
          listExam.value= value.object!;
          itemLoadMoreExam.value =listExam.value.items!;
          items.addAll(itemLoadMoreExam);
          items.refresh();
          AppUtils.shared.hideLoading();
          Future.delayed(const Duration(seconds: 1), () {
          });
        } else {
        }

      });

    }
  }




  getStatusExercise(status){
    switch(status){
      case 'CONSTRUCTED_RESPONSE':
        return "Tự luận";
      case 'SELECTED_RESPONSE':
        return"Trắc nghiệm";
      default:
        return "Trắc nghiêm & tự luận";

    }

  }



  setTypeExercise(type){
    switch(type){
      case "Đến hạn":
        return "DUE";
      case "Đã hết hạn":
        return "EXPIRED";
      case "Sắp đến hạn":
        return "DEADLINE_COMING_SOON";
      default:
        return "";
    }
  }
  showTypeExercise(type){
    switch(type){
      case "DUE":
        return "Đến hạn";
      case "EXPIRED":
        return "Đã hết hạn";
      case "DEADLINE_COMING_SOON":
        return "Sắp đến hạn";
      default:
        return "";
    }
  }

  setStastusExercise(type){
    switch(type){
      case "Đã nộp bài":
        return "SUBMITTED";
    // case "ĐÃ NỘP BÀI":
    //   return "LATE_SUBMISSION";
    // case "CHƯA NỘP BÀI":
    //   return "UNSUBMITTED";
      case "Chưa nộp bài":
        return "UNSUBMITTED";
      default:
        return "";
    }
  }
  getTextStatus(state) {
    switch (state) {
      case "SUBMITTED":
        return txtSubmitted;
      case "LATE_SUBMISSION":
        return txtSubmitted;
      case "NOT_SUBMISSION":
        return txtUnSubmited;
      case "UNSUBMITTED":
        return txtUnSubmited;
      default:
        return txtGraded;
    }
  }


  showColorTypeExercise(type){
    switch(type){
      case "DUE":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "EXPIRED":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "DEADLINE_COMING_SOON":
        return ColorUtils.PRIMARY_COLOR;
      default:
        return "";
    }
  }



  getColorTextStatus(state) {
    switch (state) {
      case "SUBMITTED":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "LATE_SUBMISSION":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "NOT_SUBMISSION":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "UNSUBMITTED":
        return const Color.fromRGBO(255, 69, 89, 1);
      default:
        return const Color.fromRGBO(136, 136, 136, 1);
    }
  }


  goToDetailExamStudentPage(index){
    checkClickPage(Get.find<HomeController>().userGroupByApp,
        () {
          Get.toNamed(Routes.detailExamsStudentPage,arguments: items[index].id);
        }, StringConstant.PAGE_DETAIL_EXAM);
  }

  goToStudentDoingExercise(index){
    Get.toNamed(Routes.studentDoingExercise,arguments: items[index]);
  }
  goToStudentDoingExam(index){
          Get.toNamed(Routes.studentDoingExam,arguments: items[index]);

  }
}