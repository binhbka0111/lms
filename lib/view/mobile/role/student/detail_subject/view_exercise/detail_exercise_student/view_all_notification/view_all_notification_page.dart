import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/detail_subject/view_exercise/detail_exercise_student/view_all_notification/view_all_notification_controller.dart';
import '../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../parent/detail_subject_parent/detail_subject_parent_controller.dart';
import '../../../../../parent/parent_home_controller.dart';
import '../../../../student_home_controller.dart';
import '../../../detail_subject_student_controller.dart';

class ViewAllNotificationPage extends GetWidget<ViewAllNotificationController>{
  final controller = Get.put(ViewAllNotificationController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorUtils.COLOR_PRIMARY,
          elevation: 0,
          title: Text("Tất cả thông báo",style: TextStyle(color: Colors.white,fontSize: 16.sp,fontWeight: FontWeight.w500),),
          actions:  [
            InkWell(
              onTap: (){
                if(AppCache().userType =='STUDENT'){
                  Get.find<StudentHomeController>().subjectId.value = "";
                  Get.delete<DetailSubjectStudentController>();
                }else{
                  Get.find<ParentHomeController>().subjectId.value = "";
                  Get.delete<DetailSubjectParentController>();
                }
                Get.delete<ViewAllNotificationController>();
                comeToHome();
              },
              child:  const Icon(Icons.home,color: Colors.white,),
            ),
            const Padding(padding: EdgeInsets.only(right: 16))
          ],
        ),
        body:
        Obx(() =>  SingleChildScrollView(
          child:
          Container(
            margin: const EdgeInsets.all(16),
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(children: [
                  const Text('Thông Báo', style:  TextStyle(color: Color.fromRGBO(26, 26, 26, 1), fontWeight: FontWeight.w500, fontSize: 16, fontFamily: 'assets/font/static/Inter-Medium.ttf'),),
                  Expanded(child: Container()),
                ],),
                const Padding(padding: EdgeInsets.only(top: 22)),

                controller.items.isEmpty?const Text("Hôm nay bạn không có thông báo!",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w400),):
                ListView.builder(
                    itemCount: controller.items.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index){
                      if( controller.items.isNotEmpty){
                        controller.files.value= controller.items[index].files!;
                      }else{
                        controller.files[index].name ="";
                        controller.files[index].link ="";
                      }
                      return
                        Container(
                            padding: const EdgeInsets.all(12),
                            decoration: BoxDecoration(
                                color: const Color.fromRGBO(
                                    246, 246, 246, 1),
                                borderRadius:
                                BorderRadius.circular(8)),
                            child:
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                           InkWell(
                             onTap: (){
                               checkClickFeature(Get.find<HomeController>().userGroupByApp,
                                   () {
                                     controller.goToDetailNotificationSubject(index);
                                   }, StringConstant.FEATURE_NOTIFY_SUBJECT_DETAIL);
                             },
                             child:
                             Html(data: '${controller.items[index].title}',
                               style: {
                                 "body":Style(
                                     fontWeight: FontWeight.w500,
                                     fontSize: FontSize(16.0),
                                     color: const Color.fromRGBO(90, 90, 90, 1)

                                 )
                               },),
                           ),

                                const Divider(
                                ),

                              ],
                            )
                        );
                    }),



              ],
            ),
          ),
        ))
    );
  }
}