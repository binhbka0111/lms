import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import '../../../../../../../../../commom/utils/file_device.dart';
import '../../../../../../../../../commom/widget/file_widget.dart';
import '../../../../../../../../../data/model/common/req_file.dart';
import 'file_upload_exercise_student_controller.dart';

class FileUploadExerciseStudentPage extends GetWidget<FileUploadExerciseStudentController> {
  final controller = Get.put(FileUploadExerciseStudentController());

  FileUploadExerciseStudentPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Obx(() => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                FileDevice.showSelectFileV2(Get.context!).then((value) {
                  if (value.isNotEmpty) {
                    controller.uploadFile(value);
                  }
                });
              },
              child: Column(
                children: [
                  Container(
                      child: controller.files.isNotEmpty
                          ? DottedBorder(
                              dashPattern: const [5, 5],
                              radius: Radius.circular(6.r),
                              borderType: BorderType.RRect,
                              color: const Color.fromRGBO(192, 192, 192, 1),
                              child: SizedBox(
                                  width: double.infinity,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      getFileWidgets(controller.files),
                                      Padding(padding: EdgeInsets.only(top: 8.h)),
                                      TextButton(
                                          onPressed: () {
                                            FileDevice.showSelectFileV2(Get.context!).then((value) {
                                              if (value.isNotEmpty) {
                                                controller.uploadFile(value);
                                              }
                                            });
                                          },
                                          child: Text(
                                            "Thêm tệp +",
                                            style:
                                                TextStyle(color: const Color.fromRGBO(72, 98, 141, 1), fontSize: 14.sp, fontWeight: FontWeight.w400),
                                          )),
                                    ],
                                  )))
                          : Container(
                              padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                              decoration: BoxDecoration(color: ColorUtils.PRIMARY_COLOR, borderRadius: BorderRadius.circular(12)),
                              child: Text(
                                'Tải file',
                                textAlign: TextAlign.center,
                                style: TextStyle(color: ColorUtils.BG_BASE1, fontSize: 12.sp, fontWeight: FontWeight.w400),
                              ),
                            ))
                ],
              ),
            ),
            const Divider(),
          ],
        ));
  }

  Widget getFileWidgets(List<ReqFile> listFile) {
    return Card(
      child: ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          padding: EdgeInsets.zero,
          itemCount: listFile.length,
          shrinkWrap: true,
          itemBuilder: (context, i) {
            return FileWidget.itemFile(Get.context!, remove: true, file: listFile[i], function: () {
              controller.files.removeAt(i);
              controller.files.refresh();
              controller.filesUploadExercise.removeAt(i);
              controller.filesUploadExercise.refresh();
            });
          }),
    );
  }
}
