import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/notification/notification_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/subjects/subjects_controller.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/subject.dart';
import '../../../../../../../../data/repository/subject/class_office_repo.dart';
import '../../../../../../../../routes/app_pages.dart';

class ViewAllNotificationController extends GetxController{
  var indexToClick =0.obs;
  var subjectId ="".obs;
  final ClassOfficeRepo _subjectRepo = ClassOfficeRepo();
  var detailSubject = DetailSubject().obs;
  var teacher = DetailSubjectTeacher().obs;
  var category = SubjectCategory().obs;
  var notificationSubject = ShowNotification().obs;
  RxList<Files> files = <Files>[].obs;
  RxList<Items> items = <Items>[].obs;
  var showNotification = false.obs;
  var idClass= ''.obs;


  @override
  void onInit() {
    super.onInit();

    getNotificationSubject();
  }



  getNotificationSubject(){
    var subjectId = "";
    if(AppCache().userType == "STUDENT"){
       idClass.value = Get.find<StudentHomeController>().clazzs[0].id!;
       if(Get.find<StudentHomeController>().subjectId.value != ""){
         subjectId = Get.find<StudentHomeController>().subjectId.value;
       }else  if(Get.isRegistered<SubjectController>()){
         subjectId = Get.find<SubjectController>().idSubject.value;
       }else{
         subjectId = Get.find<NotificationController>().subjectId;
       }
    }else{
      idClass.value =  Get.find<ParentHomeController>().currentStudentProfile.value.clazz![0].id!;
      if(Get.find<ParentHomeController>().subjectId.value != ""){
        subjectId = Get.find<ParentHomeController>().subjectId.value;
      }else  if(Get.isRegistered<SubjectController>()){
        subjectId = Get.find<SubjectController>().idSubject.value;
      }else{
        subjectId = Get.find<NotificationController>().subjectId;
      }
    }
    _subjectRepo.notificationSubjectNotPin(subjectId, idClass).then((value) {
      if (value.state == Status.SUCCESS) {
        notificationSubject.value = value.object!;
        if(notificationSubject.value.items != null){
          items.value= notificationSubject.value.items !;

        }
      }else{
      }
    });
  }
  getIconFile(ext){
    switch(ext){
      case "jpg":
        return "assets/icons/file/icon_image.png";
      case "png":
        return "assets/icons/file/icon_image.png";
      case "docx":
        return "assets/icons/file/icon_word.png";
      case "doc":
        return "assets/icons/file/icon_word.png";
      case "xls":
        return "assets/icons/file/icon_excel.png";
      case "xlsx":
        return "assets/icons/file/icon_excel.png";
      case "zip":
        return "assets/images/icon_image_zip.png";
      case "rar":
        return "assets/images/icon_image_rar.png";
      case "pptx":
        return "assets/icons/file/icon_ppt.png";
      case "ppt":
        return "assets/icons/file/icon_ppt.png";
      case "pdf":
        return "assets/icons/file/icon_pdf.png";
      default:
        return "assets/icons/file/icon_unknow.png";
    }

  }


  goToDetailNotificationSubject(index){
    checkClickFeature(Get.find<HomeController>().userGroupByApp,
            () => Get.toNamed(Routes.detailNotificationSubjectPage,
                arguments: items[index].id), StringConstant.FEATURE_NOTIFY_SUBJECT_DETAIL);
  }

}