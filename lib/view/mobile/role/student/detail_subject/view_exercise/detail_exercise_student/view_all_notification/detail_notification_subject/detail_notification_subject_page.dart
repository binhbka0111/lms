import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';
import '../../../../../../../../../commom/utils/click_subject_teacher.dart';
import '../../../../../../parent/detail_subject_parent/detail_subject_parent_controller.dart';
import '../../../../../../parent/parent_home_controller.dart';
import '../../../../detail_subject_student_controller.dart';
import '../view_all_notification_controller.dart';
import 'detail_notification_subject_controller.dart';

class DetailNotificationSubjectPage extends GetWidget<DetailNotificationSubjectController>{
  final controller = Get.put(DetailNotificationSubjectController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorUtils.COLOR_PRIMARY,
          elevation: 0,
          title: Text("Chi tiết thông báo",style: TextStyle(color: Colors.white,fontSize: 16.sp,fontWeight: FontWeight.w500),),
          actions:  [
           InkWell(
             onTap: (){
               if(AppCache().userType =='STUDENT'){
                 Get.find<StudentHomeController>().subjectId.value = "";
                 Get.delete<DetailSubjectStudentController>();
               }else{
                 Get.find<ParentHomeController>().subjectId.value = "";
                 Get.delete<DetailSubjectParentController>();
               }
               Get.delete<ViewAllNotificationController>();
               Get.delete<DetailNotificationSubjectController>();
               comeToHome();
             },
             child:  const Icon(Icons.home,color: Colors.white,),
           ),
            const Padding(padding: EdgeInsets.only(right: 16))
          ],
        ),
        body:
        Obx(() =>  SingleChildScrollView(
          child:
       Container(
         padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
         child: Column(
           children: [
             Html(data:controller.notificationSubject.value.title ?? "",
               style: {
                 "body":Style(
                     fontWeight: FontWeight.w500,
                     fontSize: FontSize(16.0),
                     color: const Color.fromRGBO(90, 90, 90, 1)

                 )
               },),
             Html(data:controller.notificationSubject.value.content ?? "",
               style: {
                 "body":Style(
                     fontWeight: FontWeight.w400,
                     fontSize: FontSize(14.0),
                     color: const Color.fromRGBO(90, 90, 90, 1)

                 )
               },),
             const Padding(padding: EdgeInsets.only(top: 8)),
             ListView.builder(
                 itemCount: controller.files.length,
                 shrinkWrap: true,
                 physics: const NeverScrollableScrollPhysics(),
                 itemBuilder:(context,index){
                   if(controller.files.isEmpty){
                     controller.files[index].name ="";
                     controller.files[index].link ="";
                   }
                   return
                   Column(
                     children: [
                       const Padding(padding: EdgeInsets.only(top: 16)),
                       controller.files[index].ext == "png" ||
                           controller.files[index].ext == "jpg" ||
                           controller.files[index].ext == "jpeg" ||
                           controller.files[index].ext == "gif" ||
                           controller.files[index].ext == "bmp"?
                       InkWell(
                           onTap: (){
                             controller.getAttackFile(index);
                           },
                           child:     Row(
                             children: [

                               SizedBox(
                                 width: 24,
                                 height: 24,
                                 child:
                                 CacheNetWorkCustomBanner(urlImage: '${ controller.files[index].link}',),


                               ),
                               Padding(
                                   padding: EdgeInsets.only(
                                       right: 6.w)),
                               Expanded(
                                 child:
                                 Column(
                                   crossAxisAlignment:
                                   CrossAxisAlignment
                                       .start,
                                   children: [
                                     Text(
                                       '${controller.files[index].name}',
                                       style: TextStyle(
                                           color:
                                           const Color.fromRGBO(
                                               26,
                                               59,
                                               112,
                                               1),
                                           fontSize: 14.sp,
                                           fontWeight:
                                           FontWeight
                                               .w500),
                                     ),
                                     Text(
                                       "${controller.files[index].size}",
                                       style: TextStyle(
                                           color:
                                           const Color.fromRGBO(
                                               192,
                                               192,
                                               192,
                                               1),
                                           fontSize: 10.sp,
                                           fontWeight:
                                           FontWeight
                                               .w400),
                                     ),
                                   ],
                                 ),
                               ),
                             ],
                           )
                       ):  Row(
                         children: [
                           Image.asset(
                             '${controller.getIconFile(controller.files[index].ext)}',
                             height: 24,
                             width: 24,
                           ),
                           Padding(
                               padding: EdgeInsets.only(
                                   right: 6.w)),
                           Expanded(
                             child:    Column(
                               crossAxisAlignment:
                               CrossAxisAlignment
                                   .start,
                               children: [
                                 Text(
                                   '${controller.files[index].name}',
                                   style: TextStyle(
                                       color:
                                       const Color.fromRGBO(
                                           26,
                                           59,
                                           112,
                                           1),
                                       fontSize: 14.sp,
                                       fontWeight:
                                       FontWeight
                                           .w500),
                                 ),
                                 Text(
                                   "${controller.files[index].size}",
                                   style: TextStyle(
                                       color:
                                       const Color.fromRGBO(
                                           192,
                                           192,
                                           192,
                                           1),
                                       fontSize: 10.sp,
                                       fontWeight:
                                       FontWeight
                                           .w400),
                                 ),
                               ],
                             ),
                           ),
                           InkWell(
                             onTap: (){
                               controller.getAttackFile(index);
                             },
                             child:
                             Image.asset(
                               'assets/images/icon_upfile_subject.png',
                               height: 16,
                               width: 16,
                             ),
                           )
                         ],
                       )
                     ],
                   );

                 }),
           ],
         ),
       )
        ))
    );
  }
}