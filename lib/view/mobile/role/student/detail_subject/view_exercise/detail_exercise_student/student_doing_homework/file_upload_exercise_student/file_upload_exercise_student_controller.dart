import 'dart:io';
import 'package:get/get.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import 'package:slova_lms/data/model/common/learning_managerment.dart';
import '../../../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/req_file.dart';
import '../../../../../../../../../data/model/res/exercise/exercise_file.dart';
import '../../../../../../../../../data/repository/exercise/excercise_repo.dart';
import '../../../../../../../../../data/repository/file/file_repo.dart';



class FileUploadExerciseStudentController extends GetxController{
  final ExerciseRepo exerciseRepo = ExerciseRepo();
  final FileRepo fileRepo = FileRepo();
  var exerciseFile = ExerciseFile().obs;
  var filesUploadExercise = <FilesExercise>[].obs;
  var files = <ReqFile>[].obs;
  var filesAnswer= <FilesExercise>[].obs;





  uploadFile(file) async {
    showDialogUploadFile();
    var fileList = <File>[];
    for (var element in file) {
      fileList.add(element.file!);
    }

  await  fileRepo.uploadFileExercise(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;

        filesUploadExercise.value = [];
        filesAnswer.addAll(listResFile);
        filesAnswer.refresh();
        filesUploadExercise.addAll(listResFile);
        filesUploadExercise.refresh();
        files.addAll(file);
        for(int i =0;i<files.length;i++){
          files[i].url = filesAnswer[i].link;
        }
        files.refresh();
        listResFile = [];

      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.showToast(value.message ?? "Tải tệp lên thất bại", backgroundColor: ColorUtils.COLOR_WORK_TYPE_4);
      }
    });
    Get.back();

  }



}