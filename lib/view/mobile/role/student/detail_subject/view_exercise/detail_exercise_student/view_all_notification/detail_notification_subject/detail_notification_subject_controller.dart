import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/open_url.dart';
import '../../../../../../../../../commom/utils/ViewPdf.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/subject.dart';
import '../../../../../../../../../data/repository/subject/class_office_repo.dart';

class DetailNotificationSubjectController extends GetxController{
  var indexToClick =0.obs;
  var nameSubject = "".obs;
  var subjectId ="".obs;
  final ClassOfficeRepo _subjectRepo = ClassOfficeRepo();
  var teacher = DetailSubjectTeacher().obs;
  var category = SubjectCategory().obs;
 var notificationSubject = DetailNotification().obs;
  RxList<Files> files = <Files>[].obs;
  var showNotification = false.obs;
var idNotifi = "".obs;
  @override
  void onInit() {
    super.onInit();
var data = Get.arguments;
if(data != null){
  idNotifi.value = data;
}
    getNotificationSubject(idNotifi.value);
  }
  getNotificationSubject(idNotification){
    var idNotification =idNotifi.value;
    _subjectRepo.detailNotificationSubject(idNotification).then((value) {
      if (value.state == Status.SUCCESS) {
        notificationSubject.value = value.object!;
        files.value = notificationSubject.value.files!;
      }else{
      }
    });
  }

  getIconFile(ext){
    switch(ext){
      case "jpg":
        return "assets/icons/file/icon_image.png";
      case "png":
        return "assets/icons/file/icon_image.png";
      case "docx":
        return "assets/icons/file/icon_word.png";
      case "doc":
        return "assets/icons/file/icon_word.png";
      case "xls":
        return "assets/icons/file/icon_excel.png";
      case "xlsx":
        return "assets/icons/file/icon_excel.png";
      case "zip":
        return "assets/images/icon_image_zip.png";
      case "rar":
        return "assets/images/icon_image_rar.png";
      case "pptx":
        return "assets/icons/file/icon_ppt.png";
      case "ppt":
        return "assets/icons/file/icon_ppt.png";
      case "pdf":
        return "assets/icons/file/icon_pdf.png";
      default:
        return "assets/icons/file/icon_unknow.png";
    }

  }
  getAttackFile(index){
    var action = 0;
    if (files[index].ext == "png" ||
        files[index].ext == "jpg" ||
        files[index].ext == "jpeg" ||
        files[index].ext == "gif" ||
        files[index].ext == "bmp") {
      action = 1;
    } else if (files[index].ext == "pdf") {
      action = 2;
    } else {
      action = 0;
    }
    switch (action) {
      case 1:
        OpenUrl.openImageViewer(Get.context!,files[index].link!);
        break;
      case 2:
        Get.to(ViewPdfPage(url: files[index].link!));
        break;
      default:
        OpenUrl.openFile(files[index].link!);
        break;
    }
  }


}