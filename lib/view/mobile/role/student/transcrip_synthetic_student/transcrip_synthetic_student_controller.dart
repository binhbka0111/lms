import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/res/transcript/semester.dart';
import '../../../../../../../data/model/res/transcript/transcript.dart';
import '../../../../../../../data/repository/transcript_repo/transcript_repo.dart';
import '../student_home_controller.dart';

class TranscriptsSyntheticStudentController extends GetxController{
  final TranscriptsRepo _transcriptsRepo = TranscriptsRepo();
  var transcript = <Transcript>[].obs;
  var semesters = <Semester>[].obs;
  var listSemester = <SemesterTranscript>[].obs;
  var controllerDateStart = TextEditingController().obs;
  var controllerDateEnd = TextEditingController().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy HH:mm');
  var semesterName = "Tất cả".obs;
  var semesterId ="".obs;
  var selectedDay = DateTime.now().obs;

  @override
  void onInit() {
    getListSemester();
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      selectedDay.value = DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
    }else{
      selectedDay.value = DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
    }
    controllerDateStart.value.text =   outputDateFormat.format(DateTime(findFirstDateOfTheWeek(selectedDay.value).year, findFirstDateOfTheWeek(selectedDay.value).month,findFirstDateOfTheWeek(selectedDay.value).day));
    controllerDateEnd.value.text = outputDateFormat.format(DateTime(findLastDateOfTheWeek(selectedDay.value).year, findLastDateOfTheWeek(selectedDay.value).month,findLastDateOfTheWeek(selectedDay.value).day));
    getListTranscript();
    super.onInit();
  }


  getListTranscript(){
    transcript.value = [];
    var fromDate = DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2),),00,00).millisecondsSinceEpoch;
    var toDate = DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2)),23,59).millisecondsSinceEpoch;
    _transcriptsRepo.getListTranscript("", "TRUE", semesterId.value, fromDate, toDate,"SYNTHETIC","${Get.find<StudentHomeController>().clazzs[0].id}","").then((value) {
      if (value.state == Status.SUCCESS) {
        transcript.value= value.object!;
        transcript.refresh();
      }
    });
  }



  getListSemester(){
    listSemester.value = [];
    listSemester.add(SemesterTranscript(id: "",name: "Tất cả"));
    _transcriptsRepo.getListSemester().then((value) {
      if (value.state == Status.SUCCESS) {
        semesters.value= value.object!;
        for(int i =0;i< semesters.length ;i++){
          listSemester.add(SemesterTranscript(id: "",name: ""));
          listSemester[i+1].name = semesters[i].name!;
          listSemester[i+1].id = semesters[i].id!;
        }
        semesterId.value = "Tất cả";
        semesterId.value = "";
      }
    });
    listSemester.refresh();
  }


  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }



  getColorTextIsPublic(status) {
    switch (status) {
      case "FALSE":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "TRUE":
        return const Color.fromRGBO(77, 197, 145, 1);
    }
  }

  getColorBackgroundIsPublic(status) {
    switch (status) {
      case "FALSE":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "TRUE":
        return const Color.fromRGBO(192, 242, 220, 1);
    }
  }

  getStatusPublic(type){
    switch(type){
      case "TRUE":
        return "Công bố";
      case "FALSE":
        return "Chưa công bố";
      default:
        return "";
    }
  }



}