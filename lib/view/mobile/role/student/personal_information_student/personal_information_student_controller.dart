import 'dart:io';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/widget/dialog_upload_file.dart';
import 'package:slova_lms/commom/widget/logout.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/model/common/req_file.dart';
import 'package:slova_lms/data/model/common/user_profile.dart';
import 'package:slova_lms/data/model/res/file/response_file.dart';
import 'package:slova_lms/data/repository/file/file_repo.dart';
import 'package:slova_lms/data/repository/person/personal_info_repo.dart';
import 'package:slova_lms/routes/app_pages.dart';
import '../../../../../commom/widget/text_field_custom.dart';
import '../../../../../data/model/common/Position.dart';
import '../../../../../data/model/common/static_page.dart';
import '../../../../../data/model/res/class/School.dart';
import '../../../../../data/repository/common/common_repo.dart';

class PersonalInformationStudentController extends GetxController {
  Rx<StateType> stateInputUser = StateType.DEFAULT.obs;
  final FileRepo fileRepo = FileRepo();
  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();
  var userProfile = UserProfile().obs;

  var files = <ReqFile>[].obs;
  var tmpUserProfile = UserProfile().obs;
  var position = Position().obs;
  var school = SchoolData().obs;
  var staticPage = ItemsStaticPage().obs;
  final CommonRepo _commonRepo = CommonRepo();
  RxList<Clazzs> clazzs  = <Clazzs>[].obs;
  var clazz = Clazzs().obs;
  RxBool isReady = false.obs;
  @override
  void onInit() {
    getDetailProfile();
    super.onInit();

  }
  updateInfoUser(fullName,id) async {
    await _personalInfoRepo.updateUserProfile(fullName,id).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Cập nhật thông tin cá nhân thành công");
        Future.delayed(const Duration(seconds: 1), () {
          getDetailProfile();
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Cập nhật thông tin cá nhân thất bại", value.message ?? "");
      }
    });
  }


  /// Flow update infomation
  /// 1. get all person data
  /// 2. Check if file change (if not change => go to step 4)
  /// 3. upload file => get response
  /// 4. Update infomation with new url of avatar (if change)
  /// 5. Check response success => finish

  uploadFile() async {
    showDialogUploadFile();
    var fileList = <File>[];
    for (var element in files) {
      fileList.add(element.file!);
    }

   await fileRepo.uploadFileAvatar(fileList).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Upload file thành công");
        var listResFile = value.object!;
        ResponseFileUpload updateAvatar = listResFile[0];
        tmpUserProfile.value.img = updateAvatar;
        updateInfoUser(tmpUserProfile.value, tmpUserProfile.value.id);
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Thông báo", value.message ?? "");
      }
    });
    Get.back();
  }

  goToUpdateInfoUser(){
    Get.toNamed(Routes.updateInfoStudent,arguments: userProfile.value);
  }


  getDetailProfile()async {
    await _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        if(userProfile.value.school == null){
          school.value.name = "";
          school.value.id = "";
        }else{
          school.value = userProfile.value.school!;
        }
        if(userProfile.value.position == null){
          position.value.name = "";
          position.value.id = "";
        }else{
          position.value = userProfile.value.position!;
        }
        clazzs.value = userProfile.value.item!.clazzs!;
        if(clazzs.isNotEmpty){
          clazz.value = clazzs[0];
        }
        tmpUserProfile.value = value.object!;
      }
    });
    isReady.value = true;
  }
  getStaticPage(type){
    _commonRepo.getListStaticPage(type).then((value){
      if (value.state == Status.SUCCESS) {
        staticPage.value = value.object!;
        goToStaticPage();
      }
    });
  }
  goToStaticPage(){
    Get.toNamed(Routes.staticPage,arguments: staticPage.value);
  }

  void submitLogout() async {
    logout();
  }


  void goToLogin() {
    Get.offAllNamed(Routes.login);
  }
  void goToChangePassPage(){
    Get.toNamed(Routes.changePass,arguments:userProfile.value.id);
  }
  void goToDetailAvatar(){
    Get.toNamed(Routes.getavatar, arguments: userProfile.value.image);
  }
}
