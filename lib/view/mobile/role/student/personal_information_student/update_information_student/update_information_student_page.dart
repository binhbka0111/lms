import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/role/student/personal_information_student/update_information_student/update_infomation_student_controller.dart';
import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../commom/utils/color_utils.dart';
import '../../../../../../commom/utils/file_device.dart';
import '../../../../../../commom/utils/global.dart';
import '../../../../../../commom/widget/custom_view.dart';
import '../../../../../../commom/widget/loading_custom.dart';
import '../../../../../../commom/widget/text_field_custom.dart';
import '../personal_information_student_controller.dart';

class UpdateInfoStudentPage extends GetWidget<UpdateInfoStudentControlller> {
  @override
  final controller = Get.put(UpdateInfoStudentControlller());

  UpdateInfoStudentPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            elevation: 0,
            leading: InkWell(
              onTap: () {
                Get.back(result: controller.userProfile.value);
                Get.find<PersonalInformationStudentController>().onInit();
              },
              child: const Icon(
                Icons.arrow_back_rounded,
                color: Colors.white,
              ),
            ),
            title: Text(
              "Chỉnh Sửa Thông Tin Cá Nhân",
              style: TextStyle(
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
            ),
          ),
          body: Obx(() => controller.isReady.value?WillPopScope(
              child: SingleChildScrollView(
                  reverse: true, // this is new
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Center(
                        child: Column(
                          children: [
                            Padding(padding: EdgeInsets.only(top: 16.h)),
                            Container(
                              height: 80.h,
                              margin: EdgeInsets.only(top: 10.h),
                              child: Stack(
                                children: [
                                  InkWell(
                                    onTap: (){
                                      controller.goToDetailAvatar();
                                    },
                                    child:      SizedBox(
                                        width: 80.h,
                                        height: 80.h,
                                        child:
                                        CacheNetWorkCustom(urlImage:'${controller.userProfile.value.image}')
                                    ),
                                  ),
                                  Positioned(
                                      right: 0,
                                      bottom: 0,
                                      child: InkWell(
                                        onTap: () {
                                          pickerImage();
                                        },
                                        child: Image.asset(
                                          'assets/images/img_cam.png',width: 24,height: 24,),
                                      ))
                                ],
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(top: 16.h, bottom: 8.h),
                                height: 50.h,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${controller.userProfile.value.fullName}',
                                      style: TextStyle(
                                          color: const Color.fromRGBO(26, 26, 26, 1),
                                          fontSize: 16.sp,
                                          fontFamily: 'static/Inter-Medium.ttf',
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Padding(padding: EdgeInsets.only(top: 4.h)),
                                    Text(
                                      '${controller.userProfile.value.email}',
                                      style: TextStyle(
                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                        fontSize: 10.sp,
                                      ),
                                    )
                                  ],
                                ))
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      OutlineBorderTextFormField(
                        enable: true,
                        focusNode: controller.focusName.value,
                        iconPrefix: "",
                        iconSuffix: "",
                        state: controller.stateInput,
                        labelText: "Họ và tên",
                        autofocus: false,
                        controller: controller.controllerName.value,
                        helperText: "",
                        showHelperText: false,
                        ishowIconPrefix: false,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        validation: (textToValidate) {
                          return controller
                              .getTempIFSCValidation(textToValidate);
                        },
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      OutlineBorderTextFormField(
                        enable: false,
                        iconPrefix: "",
                        iconSuffix: "",
                        focusNode: controller.focusRole.value,
                        state: StateType.DEFAULT,
                        labelText: "Lớp",
                        autofocus: false,
                        controller: controller.controllerClass.value,
                        helperText: "",
                        showHelperText: false,
                        ishowIconPrefix: false,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        validation: (textToValidate) {
                          return controller
                              .getTempIFSCValidation(textToValidate);
                        },
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      OutlineBorderTextFormField(
                        enable: false,
                        focusNode: controller.focusPhone.value,
                        iconPrefix: null,
                        iconSuffix: "",
                        state: StateType.DEFAULT,
                        labelText: "Số Điện thoại",
                        autofocus: false,
                        controller: controller.controllerPhone.value,
                        helperText: "",
                        showHelperText: false,
                        textInputAction: TextInputAction.next,
                        ishowIconPrefix: false,
                        keyboardType: TextInputType.text,
                        validation: (textToValidate) {
                          return controller
                              .getTempIFSCValidation(textToValidate);
                        },
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      OutlineBorderTextFormField(
                        enable: false,
                        focusNode: controller.focusEmail.value,
                        iconPrefix: "",
                        iconSuffix: "",
                        state: StateType.DEFAULT,
                        labelText: "Email",
                        autofocus: false,
                        controller: controller.controllerEmail.value,
                        helperText: "",
                        showHelperText: false,
                        ishowIconPrefix: false,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        validation: (textToValidate) {
                          return controller
                              .getTempIFSCValidation(textToValidate);
                        },
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      OutlineBorderTextFormField(
                        enable: false,
                        iconPrefix: "",
                        iconSuffix: "",
                        focusNode: controller.focusSchool.value,
                        state: StateType.DEFAULT,
                        labelText: "Trường",
                        autofocus: false,
                        controller: controller.controllerSchool.value,
                        helperText: "",
                        showHelperText: false,
                        ishowIconPrefix: false,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        validation: (textToValidate) {
                          return controller
                              .getTempIFSCValidation(textToValidate);
                        },
                      ),
                      SizedBox(height: 16.h,),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.only(right: 16, left: 16),
                        child: SizedBox(
                          height: 46,
                          child: ElevatedButton(
                            onPressed: () {
                              dismissKeyboard();
                              controller.tmpUserProfile.value.fullName =
                                  controller.controllerName.value.text;
                              if(controller.controllerName.value.text.trim().isEmpty||controller.controllerName.value.text.isEmpty){
                                controller.stateInput = StateType.ERROR;
                                AppUtils.shared.snackbarError(
                                    "Họ và tên không được để trống","");
                              }else{
                                controller.updateInfoUser(controller.tmpUserProfile.value,
                                    controller.userProfile.value.id);
                                controller.stateInput = StateType.DEFAULT;
                              }
                            },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: ColorUtils.PRIMARY_COLOR,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6))),
                            child: Text("Cập Nhật",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w400)),
                          ),
                        ),
                      ),
                      Padding(
                        // this is new
                          padding: EdgeInsets.only(
                              bottom:
                              MediaQuery.of(context).viewInsets.bottom)),
                    ],
                  )),
              onWillPop: () async {
                Get.back(result: controller.userProfile.value);
                Get.find<PersonalInformationStudentController>().onInit();
                return true;
              }):const LoadingCustom())),
    );
  }

  buildAvatarSelect() {
    var file = controller.files[0].file;
    return SizedBox(
      width: double.infinity,
      child: Container(
          width: 180,
          height: 180,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(100.r))),
          child: CircleAvatar(
            backgroundImage: Image.file(
              file!,
              width: 200,
              height: 200,
            ).image,
          )),
    );
  }

  Future<void> pickerImage() async {
    var file = await FileDevice.showSelectFileV2(Get.context! ,image: true);
    if (file.isNotEmpty) {
      controller.files.clear();
      controller.files.addAll(file);
      controller.files.refresh();
      Get.bottomSheet(
          getDialogConfirm(
              "Chọn hình đại diện",
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: buildAvatarSelect(),
              ),
              colorBtnOk: ColorUtils.COLOR_GREEN_BOLD,
              colorTextBtnOk: ColorUtils.COLOR_WHITE,
              btnRight: "Xác nhận", funcLeft: () {
            Get.back();
          }, funcRight: () {
            Get.back();
            controller.uploadFile();
          }),
          isScrollControlled: true);
    }
  }
}
