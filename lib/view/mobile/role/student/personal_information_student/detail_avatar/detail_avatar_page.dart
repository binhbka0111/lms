import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/role/student/personal_information_student/detail_avatar/detail_avatar_controller.dart';

class DetailAvatarPage extends GetWidget<DetailAvatarController>{
  @override
  final controller = Get.put(DetailAvatarController());

  DetailAvatarPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body:
        SafeArea(
          child:Obx(() =>
              Stack(
                children: [
                  SizedBox(
                      width:double.infinity,
                      height: double.infinity,
                      child:

                      CacheNetWorkCustomBanner(urlImage: controller.path.value,)

                  ),
                  Positioned(
                      right: 0,
                      top: 0,
                      child:InkWell(
                        onTap: (){
                          controller.gotoBack();
                        },
                        child: SizedBox(
                          height: 50.h,
                          width: 50.w,
                          child: const Icon(Icons.close,size: 30,color: Colors.black87,),
                        ),
                      )
                  ),
                ],
              ),),
        )
        );
  }

}