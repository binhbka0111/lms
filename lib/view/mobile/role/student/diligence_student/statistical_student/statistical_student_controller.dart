import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/model/common/diligence.dart';
import 'package:slova_lms/data/model/common/school_year.dart';
import 'package:slova_lms/data/model/common/user_profile.dart';
import 'package:slova_lms/data/repository/diligence/diligence_repo.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';
class StatisticalStudentController extends GetxController {

  String onTimeText = "Đúng giờ";
  String notOnTimeText= "Đi muộn";
  String absentWithoutLeaveText = "Không phép";
  String excusedAbsenceText = "Có phép";
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;
  var userProfile = UserProfile().obs;
  RxList<SchoolYear> dateTime = <SchoolYear>[].obs;
  var diligence = Diligence().obs;
  var outputDateFormat = DateFormat('EEEE,dd/MM/yyyy');
    var outputDateToDateFormat = DateFormat('dd/MM/yyyy');
  var outputMonthFormat = DateFormat('MM/yyyy');
  var isVisibility = false.obs;
  var isVisibilityIcon = false.obs;
  var isVisibilityLateTime = false.obs;
  var textColor = false.obs;
  RxList<String> listStatus = <String>[].obs;
  RxList<Items> item = <Items>[].obs;
  RxList<StatusDetailDiligence> dayOnSchool = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayAbsent = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayExcused = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayNotOnTime = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayOnTimeAndDayNotOnTime = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayAbsendAndDayExcused = <StatusDetailDiligence>[].obs;
  var listDayLeavingApplication = <DateTime>[].obs;
  var detailDiligence = DetailDiligence().obs;
  var studentOnTime = StudentOnSchool().obs;
  var studentNotOnTime = StudentNotOnTime().obs;
  var studentExcused = StudentExcusedAbsence().obs;
  var studentAbSend = StudentAbsentWithoutLeave().obs;
var totalDayOnTime = 0.obs;
var totalDayNotOnTime = 0.obs;
  var totalDayAbSend = 0.obs;
  var totalDayExSeCud = 0.obs;
  var controllerDateStart = TextEditingController().obs;
  var controllerDateEnd = TextEditingController().obs;
  var fromYearSelect = "".obs;
  var toYearSelect = "".obs;
  var textToDate =''.obs;
  var textFromDate = ''.obs;

  getColorStatus(state) {
    switch (state) {
      case "ON_TIME":
        return const Color.fromRGBO(221, 246, 235, 1);
      case "NOT_ON_TIME":
        return const Color.fromRGBO(255, 239, 203, 1);
      case "EXCUSED_ABSENCE":
        return const Color.fromRGBO(210, 210, 210, 1);
      default:
        return const Color.fromRGBO(255, 200, 206, 1);
    }
  }

  getColorTextStatus(state) {
    switch (state) {
      case "ON_TIME":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "NOT_ON_TIME":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "EXCUSED_ABSENCE":
        return const Color.fromRGBO(136, 136, 136, 1);
      default:
        return const Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getTextStatus(state) {
    switch (state) {
      case "ON_TIME":
        return onTimeText;
      case "NOT_ON_TIME":
        return notOnTimeText;
      case "ABSENT_WITHOUT_LEAVE":
        return absentWithoutLeaveText;
      default:
        return excusedAbsenceText;
    }
  }
  DateTime findFirstDateOfTheMonth(DateTime dateTime) {
    return DateTime(dateTime.year, dateTime.month, 1);
  }

  DateTime findLastDateOfTheMonth(DateTime dateTime) {
    return DateTime(dateTime.year, dateTime.month + 1, 0);
  }

  @override
  void onInit() {
    super.onInit();
    userProfile.value = Get.find<StudentHomeController>().userProfile.value;
    fromYearSelect.value = outputDateToDateFormat.format(DateTime.now());
    toYearSelect.value = outputDateToDateFormat.format(DateTime.now());
    setDateSchoolYears();

    if(DateTime.now().month<=12&&DateTime.now().month>9){
      controllerDateStart.value.text = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month));
      controllerDateEnd.value.text = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month+1).subtract(const Duration(days: 1)));
      textFromDate.value = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month));
      textToDate.value = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month+1).subtract(const Duration(days: 1)));
    }else{
      controllerDateStart.value.text = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month));
      controllerDateEnd.value.text = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month+1).subtract(const Duration(days: 1)));
      textFromDate.value = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month));
      textToDate.value = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month+1).subtract(const Duration(days: 1)));
    }
  }

  getDay(){
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      controllerDateStart.value.text = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month));
      controllerDateEnd.value.text = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month+1).subtract(const Duration(days: 1)));

    }else{
      controllerDateStart.value.text = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month));
      controllerDateEnd.value.text = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month+1).subtract(const Duration(days: 1)));
    }
  }

  getTotalDay(){
    if(totalDayExSeCud.value == 0){
      return totalDayAbSend.value;
    }else if(totalDayAbSend.value ==0){
      return totalDayExSeCud.value;
    }else{
      return totalDayExSeCud.value + totalDayAbSend.value;

    }
  }

  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<StudentHomeController>().fromYearPresent.value == DateTime.now().year) {
        getListDiligenceToMonth();
        return;
      } else {
        fromYearSelect.value = "${Get.find<StudentHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<StudentHomeController>().fromYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
      }
    } else {
      if (Get.find<StudentHomeController>().toYearPresent.value == DateTime.now().year) {
      getListDiligenceToMonth();
        return;
      } else {
        fromYearSelect.value = "${Get.find<StudentHomeController>().toYearPresent}";
        toYearSelect.value = "${Get.find<StudentHomeController>().toYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
      }
    }
  }

  getTotalDayOnTime(){
    if(totalDayOnTime.value == 0){
      return totalDayNotOnTime.value;
    }else if(totalDayNotOnTime.value ==0){
      return totalDayOnTime.value;
    }else{
      return totalDayOnTime.value + totalDayNotOnTime.value;

    }
  }
  getListDiligence(fromDate,toDate){
    var userId = Get.find<StudentHomeController>().userProfile.value.id;
    var fromDate = DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2))).millisecondsSinceEpoch;
    var toDate = DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.detailListDiligence(userId, fromDate, toDate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayOnSchool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotOnTime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        dayOnTimeAndDayNotOnTime.addAll(dayOnSchool);
        dayOnTimeAndDayNotOnTime.addAll(dayNotOnTime);
        dayAbsendAndDayExcused.addAll(dayExcused);
        dayAbsendAndDayExcused.addAll(dayAbsent);
        getSortList();
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbSend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExSeCud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  getListDiligenceToMonth(){
    var userId = Get.find<StudentHomeController>().userProfile.value.id;
    var fromDate = DateTime(DateTime.now().year,DateTime.now().month).millisecondsSinceEpoch;
    var toDate = DateTime(DateTime.now().year,DateTime.now().month+1).subtract(const Duration(days: 1)).millisecondsSinceEpoch;
    _diligenceRepo.detailListDiligence(userId, fromDate, toDate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayOnSchool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotOnTime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        dayOnTimeAndDayNotOnTime.addAll(dayOnSchool);
        dayOnTimeAndDayNotOnTime.addAll(dayNotOnTime);
        dayAbsendAndDayExcused.addAll(dayExcused);
        dayAbsendAndDayExcused.addAll(dayAbsent);
        getSortList();
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbSend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExSeCud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  getListDiligenceToSelectYear(fromDate, toDate){
    var userId = Get.find<StudentHomeController>().userProfile.value.id;
    var fromDate = DateTime(int.parse(toYearSelect.value.substring(0,4)), DateTime.now().month).millisecondsSinceEpoch;
    var toDate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), DateTime.now().month+1).subtract(const Duration(days: 1)).millisecondsSinceEpoch;
    _diligenceRepo.detailListDiligence(userId, fromDate, toDate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayOnSchool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotOnTime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        dayOnTimeAndDayNotOnTime.addAll(dayOnSchool);
        dayOnTimeAndDayNotOnTime.addAll(dayNotOnTime);
        dayAbsendAndDayExcused.addAll(dayExcused);
        dayAbsendAndDayExcused.addAll(dayAbsent);
        getSortList();
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbSend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExSeCud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  getConvertDateOnTime(index){
    var  dayOnTime = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime[index].date!));
    if(dayOnTime.substring(0,6)=="Monday"){
      return "Thứ 2, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime[index].date!))}";
    }else if(dayOnTime.substring(0,7)=="Tuesday"){
      return "Thứ 3, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime[index].date!))}";
    }else if(dayOnTime.substring(0,9)=="Wednesday"){
      return "Thứ 4, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime[index].date!))}";
    }else if(dayOnTime.substring(0,8)=="Thursday"){
      return "Thứ 5, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime[index].date!))}";
    }else if(dayOnTime.substring(0,6)=="Friday"){
      return "Thứ 6, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime[index].date!))}";
    }else if(dayOnTime.substring(0,8)=="Saturday"){
      return "Thứ 7, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime[index].date!))}";
    }else if(dayOnTime.substring(0,6)=="Sunday"){
      return "Chủ nhật, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnTimeAndDayNotOnTime[index].date!))}";
    }else{
      return "";
    }
  }
  getConvertDateAbsend(index){
    var  dayAbsend = outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused[index].date!));
    if(dayAbsend.substring(0,6)=="Monday"){
      return "Thứ 2, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused[index].date!))}";
    }else if(dayAbsend.substring(0,7)=="Tuesday"){
      return "Thứ 3, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused[index].date!))}";
    }else if(dayAbsend.substring(0,9)=="Wednesday"){
      return "Thứ 4, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused[index].date!))}";
    }else if(dayAbsend.substring(0,8)=="Thursday"){
      return "Thứ 5, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused[index].date!))}";
    }else if(dayAbsend.substring(0,6)=="Friday"){
      return "Thứ 6, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused[index].date!))}";
    }else if(dayAbsend.substring(0,8)=="Saturday"){
      return "Thứ 7, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused[index].date!))}";
    }else if(dayAbsend.substring(0,6)=="Sunday"){
      return "Chủ nhật, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsendAndDayExcused[index].date!))}";
    }else{
      return "";
    }
  }
  
  getSortList(){
    dayOnTimeAndDayNotOnTime.sort(
      (a, b) => b.date!.compareTo(int.parse(a.date.toString())),
    );
    dayAbsendAndDayExcused.sort(
          (a, b) => b.date!.compareTo(int.parse(a.date.toString())),
    );
  }

 


  goToDetailStatisticalStudentOnSchoolPage(index){
    Get.toNamed(Routes.detailDiligenceStudent, arguments:[dayOnTimeAndDayNotOnTime[index].date, dayOnTimeAndDayNotOnTime[index].statusDiligent,dayOnTimeAndDayNotOnTime[index].updatedAt] );
  }
  goToDetailStatisticalStudentExcusedAbsencePage(index){
    Get.toNamed(Routes.detailDiligenceStudent, arguments:[dayAbsendAndDayExcused[index].date, dayAbsendAndDayExcused[index].statusDiligent,dayAbsendAndDayExcused[index].updatedAt] );
  }
  goToDetailStatisticalStudent(){
    Get.toNamed(Routes.detailAllStasticalPage, arguments: [controllerDateStart.value.text.toString(), controllerDateEnd.value.text.toString()]);

  }



}

