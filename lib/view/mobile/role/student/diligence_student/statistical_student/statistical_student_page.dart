import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/constants/date_format.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/date_time_picker.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/diligence_student/statistical_student/statistical_student_controller.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_page.dart';

import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../commom/utils/time_utils.dart';

class StatisticalStudentPage extends GetView<StatisticalStudentController> {
  @override
  Widget build(BuildContext context) {
    var controller = Get.put(StatisticalStudentController());
    return Obx(() => Scaffold(
          backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {
                  Get.to(StudentHomePage());
                },
                icon: const Icon(Icons.home),
                color: Colors.white,
              )
            ],
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            title: Text(
              'Chuyên Cần',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.all(16),
                color: const Color.fromRGBO(255, 255, 255, 1),
                child: Row(
                  children: [
                    SizedBox(
                    height: 40.h,
                    width: 40.h,
                    child:
                    CacheNetWorkCustom(urlImage: '${controller.userProfile.value.image }',)

                ),

                    Container(
                      margin: const EdgeInsets.only(left: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${controller.userProfile.value.fullName}',
                            style: TextStyle(
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontSize: 14.sp),
                          ),
                          Padding(padding: EdgeInsets.only(top: 4.h)),
                          SizedBox(
                            width: 185.sp,
                            child: Text(
                              '${controller.userProfile.value.school!.name}',
                              style: TextStyle(
                                  color: const Color.fromRGBO(177, 177, 177, 1),
                                  fontSize: 13.sp),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Spacer(),
                    InkWell(
                      onTap: (){
                        checkClickFeature(Get.find<HomeController>().userGroupByApp,
                            () {
                              controller.goToDetailStatisticalStudent();
                            }, StringConstant.FEATURE_STATISTICAL_DILIGENCE_DETAIL);
                      },
                      child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              color: const Color.fromRGBO(254, 230, 211, 1)),
                          width: 80,
                          height: 18.sp,
                          child: Text("Chi tiết",
                              style: TextStyle(
                                  color: ColorUtils.PRIMARY_COLOR,
                                  fontSize: 13.sp,
                                  fontFamily: 'static/Inter-Medium.ttf'))),
                    )
                  ],
                ),
              ),
          Obx(() =>     Container(
            margin: const EdgeInsets.only(
                top: 16, bottom: 8, right: 16, left: 16),
            child: Row(
              children: [
                Text(
                  'Thống Kê',
                  style: TextStyle(
                      color: const Color.fromRGBO(26, 26, 26, 1),
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                ),
                Expanded(child: Container()),
                Row(
                  children: [
                    InkWell(
                        onTap: (){
                          Get.dialog(
                              Dialog(
                                child: IntrinsicHeight(
                                    child:
                                    Container(
                                      margin: const EdgeInsets.symmetric(horizontal: 16,vertical: 16),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(6)
                                      ),
                                      child:
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only( bottom: 8.h),
                                            child:
                                            Text("Vui lòng chọn ngày", style:  TextStyle(color: Colors.black, fontWeight: FontWeight.w500, fontSize: 12.sp),),),
                                          Row(
                                            children: [
                                              Expanded(
                                                child: Container(
                                                  alignment: Alignment.centerLeft,
                                                  padding: EdgeInsets.only(
                                                      left: 8.w),
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                      const BorderRadius.all(Radius.circular(6.0)),
                                                      border: Border.all(
                                                        width: 1,
                                                        style: BorderStyle.solid,
                                                        color: const Color.fromRGBO(192, 192, 192, 1),
                                                      )),
                                                  child:
                                                  TextFormField(
                                                    keyboardType: TextInputType.multiline,
                                                    maxLines: null,
                                                    style: TextStyle(
                                                      fontSize: 10.0.sp,
                                                      color: const Color.fromRGBO(26, 26, 26, 1),
                                                    ),
                                                    onTap: () {
                                                      selectDateTimeStart(controller.controllerDateStart.value.text, DateTimeFormat.formatDateShort,context);
                                                    },
                                                    cursorColor: ColorUtils.PRIMARY_COLOR,
                                                    controller:
                                                    controller.controllerDateStart.value,
                                                    readOnly: true,
                                                    decoration: InputDecoration(
                                                      suffixIcon: SizedBox(
                                                        height: 1,
                                                        width: 1,
                                                        child: SvgPicture.asset(
                                                          "assets/images/icon_date_picker.svg",
                                                          fit: BoxFit.scaleDown,
                                                        ),
                                                      ),
                                                      labelText: "Từ ngày",
                                                      border: InputBorder.none,
                                                      labelStyle: TextStyle(
                                                          color: const Color.fromRGBO(177, 177, 177, 1),
                                                          fontSize: 12.sp,
                                                          fontWeight: FontWeight.w500,
                                                          fontFamily: 'assets/font/static/Inter-Medium.ttf'
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(padding: EdgeInsets.only(right: 16.w)),
                                              Expanded(
                                                child: Container(
                                                  alignment: Alignment.centerLeft,
                                                  padding: EdgeInsets.only(
                                                      left: 8.w),
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                      const BorderRadius.all(Radius.circular(6.0)),
                                                      border: Border.all(
                                                        width: 1,
                                                        style: BorderStyle.solid,
                                                        color: const Color.fromRGBO(192, 192, 192, 1),
                                                      )),
                                                  child: TextFormField(
                                                    keyboardType: TextInputType.multiline,
                                                    maxLines: null,
                                                    style: TextStyle(
                                                      fontSize: 10.0.sp,
                                                      color: const Color.fromRGBO(26, 26, 26, 1),
                                                    ),
                                                    onTap: () {
                                                      selectDateTimeEnd(controller.controllerDateEnd.value.text, DateTimeFormat.formatDateShort,context);
                                                    },
                                                    readOnly: true,
                                                    cursorColor: ColorUtils.PRIMARY_COLOR,
                                                    controller:
                                                    controller.controllerDateEnd.value,
                                                    decoration: InputDecoration(
                                                      suffixIcon:
                                                      SizedBox(
                                                        height: 1,
                                                        width: 1,
                                                        child: SvgPicture.asset(
                                                          "assets/images/icon_date_picker.svg",
                                                          fit: BoxFit.scaleDown,
                                                        ),
                                                      ),
                                                      label: const Text("Đến ngày"),
                                                      border: InputBorder.none,
                                                      labelStyle: TextStyle(
                                                          color: const Color.fromRGBO(177, 177, 177, 1),
                                                          fontSize: 12.sp,
                                                          fontWeight: FontWeight.w500,
                                                          fontFamily: 'assets/font/static/Inter-Medium.ttf'
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          const Padding(padding: EdgeInsets.only(top: 16)),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(child:
                                              SizedBox(
                                                height: 24.h,
                                                child:  ElevatedButton(
                                                    style: ElevatedButton.styleFrom(
                                                        backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
                                                        side: const BorderSide(
                                                            color: ColorUtils.PRIMARY_COLOR,width: 1
                                                        ),
                                                        shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(6),
                                                        )
                                                    ),
                                                    onPressed: (){
                                                      Get.back();
                                                      controller.getDay();
                                                    },
                                                    child: Text('Hủy', style: TextStyle(color: ColorUtils.PRIMARY_COLOR, fontSize: 12.sp, fontWeight: FontWeight.w400, fontFamily: 'assets/font/static/Inter-Regular.ttf'),)),
                                              )),
                                              Padding(padding: EdgeInsets.only(right: 8.w)),
                                              Expanded(child:SizedBox(
                                                height: 24.h,
                                                child: ElevatedButton(
                                                    style: ElevatedButton.styleFrom(
                                                        backgroundColor: ColorUtils.PRIMARY_COLOR,
                                                        side: const BorderSide(
                                                            color: ColorUtils.PRIMARY_COLOR,width: 1
                                                        ),
                                                        shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(6),
                                                        )
                                                    ),
                                                    onPressed: (){
                                                      controller.dayAbsendAndDayExcused.clear();
                                                      controller.dayOnTimeAndDayNotOnTime.clear();



                                                      controller.getListDiligence(
                                                          DateTime(int.parse(controller.controllerDateStart.value.text.substring(6,10)), int.parse(controller.controllerDateStart.value.text.substring(4,5)),int.parse(controller.controllerDateStart.value.text.substring(1,2))).millisecondsSinceEpoch,
                                                          DateTime(int.parse(controller.controllerDateEnd.value.text.substring(6,10)), int.parse(controller.controllerDateEnd.value.text.substring(4,5)),int.parse(controller.controllerDateEnd.value.text.substring(0,2))).millisecondsSinceEpoch
                                                      );
                                                      controller.textFromDate.value = controller.controllerDateStart.value.text;
                                                      controller.textToDate.value = controller.controllerDateEnd.value.text;

                                                      controller.dayAbsendAndDayExcused.refresh();
                                                      controller.dayOnTimeAndDayNotOnTime.refresh();

                                                      Get.back();
                                                    },
                                                    child: Text('Xác Nhận', style: TextStyle(color: const Color.fromRGBO(255, 255, 255, 1), fontSize: 12.sp, fontWeight: FontWeight.w400, fontFamily: 'assets/font/static/Inter-Regular.ttf'),)),
                                              ))
                                            ],
                                          ),

                                        ],
                                      ),
                                    )
                                ),
                              )
                          );
                        },
                        child:
                        Row(
                          children: [
                            Text(
                              "${controller.textFromDate.value} - ${controller.textToDate.value}",
                              style: TextStyle(
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontSize: 13.sp,
                                  fontWeight: FontWeight.w500,
                                  fontFamily:
                                  'assets/font/static/Inter-Medium.ttf'),
                            ),
                            const Padding(padding: EdgeInsets.only(right: 8)),
                            SizedBox(
                              child: Image.asset("assets/images/icon_calenda.png",height: 16.sp, width: 16.sp,),
                            ),
                          ],
                        )
                    ),
                    Padding(padding: EdgeInsets.only(right: 4.w)),

                  ],
                )
              ],
            ),
          ),),
              SingleChildScrollView(
                child: Visibility(
                  visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_STATISTICAL_DILIGENCE_LIST),
                  child: Obx(() => Container(
                        margin: const EdgeInsets.only(left: 16, right: 16),
                        padding: const EdgeInsets.only(
                            top: 11, bottom: 11, right: 16, left: 16),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: const Color.fromRGBO(255, 255, 255, 1)),
                        child: SingleChildScrollView(
                          physics: const ScrollPhysics(),
                          child: Column(
                            children: [
                              Column(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      controller.isVisibility.value =
                                          !controller.isVisibility.value;
                                      controller.textColor.value =
                                          !controller.textColor.value;
                                    },
                                    child: Row(
                                      children: [
                                        Text(
                                          'Ngày lên lớp:',
                                          style: controller.textColor.value
                                              ? TextStyle(
                                                  color: const Color.fromRGBO(
                                                      26, 26, 26, 1),
                                                  fontSize: 13.sp)
                                              : TextStyle(
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontSize: 12.sp),
                                        ),
                                        Expanded(child: Container()),
                                        Row(
                                          children: [
                                            Text(
                                              '${controller.getTotalDayOnTime() ?? 0} ngày',
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontSize: 13.sp),
                                            ),
                                            const Padding(
                                                padding:
                                                    EdgeInsets.only(right: 9)),
                                            Icon(controller.isVisibility.value
                                                ? Icons.keyboard_arrow_up
                                                : Icons
                                                    .keyboard_arrow_down_outlined),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  Visibility(
                                    visible: controller.isVisibility.value,
                                    child: Container(
                                        margin: const EdgeInsets.only(top: 11),
                                        child: Column(
                                          children: [
                                            ListView.builder(
                                                shrinkWrap: true,
                                                itemCount: controller
                                                    .dayOnTimeAndDayNotOnTime.length,
                                                itemBuilder: (context, index) {
                                                  return GestureDetector(
                                                      onTap: () {
                                                        checkClickFeature(Get.find<HomeController>().userGroupByApp, () {
                                                          controller
                                                              .goToDetailStatisticalStudentOnSchoolPage(
                                                              index);
                                                        }, StringConstant.FEATURE_STATISTICAL_DILIGENCE_DETAIL);

                                                      },
                                                      child: Container(
                                                        margin: const EdgeInsets.only(
                                                            top: 8),
                                                        child: Row(
                                                          children: [
                                                            Text(
                                                              "${controller.getConvertDateOnTime(index)}",
                                                              style:
                                                              TextStyle(
                                                                  color: const Color
                                                                      .fromRGBO(
                                                                      90,
                                                                      90,
                                                                      90,
                                                                      1),
                                                                  fontSize:
                                                                  12.sp),
                                                            ),
                                                            Expanded(
                                                                child:
                                                                    Container()),
                                                            Container(
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                height: 18.sp,
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        left: 12,
                                                                        right:
                                                                            12),
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              25),
                                                                  color: controller
                                                                      .getColorStatus(controller
                                                                          .dayOnTimeAndDayNotOnTime
                                                                          [
                                                                              index]
                                                                          .statusDiligent),
                                                                ),
                                                                child: Text(
                                                                    '${controller.getTextStatus(controller.dayOnTimeAndDayNotOnTime[index].statusDiligent)}',
                                                                    style: TextStyle(
                                                                        color: controller.getColorTextStatus(controller
                                                                            .dayOnTimeAndDayNotOnTime
                                                                            [
                                                                                index]
                                                                            .statusDiligent),
                                                                        fontSize:
                                                                            11.sp)))
                                                          ],
                                                        ),
                                                      ));
                                                }),
                                          ],
                                        )),
                                  )
                                ],
                              ),
                              const Padding(padding: EdgeInsets.only(top: 22)),
                              Column(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      controller.isVisibilityLateTime.value =
                                          !controller.isVisibilityLateTime.value;
                                      controller.textColor.value =
                                          !controller.textColor.value;
                                    },
                                    child: Row(
                                      children: [
                                        Text(
                                          'Ngày nghỉ:',
                                          style: controller.textColor.value
                                              ? TextStyle(
                                                  color: const Color.fromRGBO(
                                                      26, 26, 26, 1),
                                                  fontSize: 13.sp)
                                              : TextStyle(
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontSize: 13.sp),
                                        ),
                                        Expanded(child: Container()),
                                        Row(
                                          children: [
                                            Text(
                                              ' ${controller.getTotalDay()} ngày',
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontSize: 13.sp),
                                            ),
                                            const Padding(
                                                padding:
                                                    EdgeInsets.only(right: 9)),
                                            Icon(controller
                                                    .isVisibilityLateTime.value
                                                ? Icons.keyboard_arrow_up
                                                : Icons
                                                    .keyboard_arrow_down_outlined),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  Visibility(
                                    visible:
                                        controller.isVisibilityLateTime.value,
                                    child: Container(
                                        margin: const EdgeInsets.only(top: 11),
                                        child: Column(
                                          children: [
                                            ListView.builder(
                                                shrinkWrap: true,
                                                itemCount: controller
                                                    .dayAbsendAndDayExcused.length,
                                                itemBuilder: (context, index) {
                                                  return GestureDetector(
                                                      onTap: () {
                                                        checkClickFeature(Get.find<HomeController>().userGroupByApp,
                                                            () => controller.goToDetailStatisticalStudentExcusedAbsencePage(index),
                                                          StringConstant.FEATURE_STATISTICAL_DILIGENCE_DETAIL);
                                                      },
                                                      child: Container(
                                                        margin:
                                                            const EdgeInsets.only(
                                                                top: 8),
                                                        child: Row(
                                                          children: [
                                                            Text(
                                                              "${controller.getConvertDateAbsend(index)}",
                                                              style:
                                                              TextStyle(
                                                                  color: const Color
                                                                      .fromRGBO(
                                                                      90,
                                                                      90,
                                                                      90,
                                                                      1),
                                                                  fontSize:
                                                                  12.sp),
                                                            ),
                                                            Expanded(
                                                                child:
                                                                    Container()),
                                                            Container(
                                                              alignment: Alignment
                                                                  .center,
                                                              height: 18.sp,
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left: 12,
                                                                      right: 12),
                                                              decoration:
                                                                  BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            25),
                                                                color: controller
                                                                    .getColorStatus(controller
                                                                        .dayAbsendAndDayExcused
                                                                        [
                                                                            index]
                                                                        .statusDiligent),
                                                              ),
                                                              child: Text(
                                                                  '${controller.getTextStatus(controller.dayAbsendAndDayExcused[index].statusDiligent)}',
                                                                  style: TextStyle(
                                                                      color: controller.getColorTextStatus(controller
                                                                          .dayAbsendAndDayExcused
                                                                          [
                                                                              index]
                                                                          .statusDiligent),
                                                                      fontSize:
                                                                          11.sp)),
                                                            )
                                                          ],
                                                        ),
                                                      ));
                                                }),
                                          ],
                                        )),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      )),
                ),
              ),
            ],
          ),
        ));
  }

  selectDateTimeStart(stringTime, format,context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {

      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy')
                .parse(controller.controllerDateEnd.value.text)
                .millisecondsSinceEpoch) {
          controller.controllerDateStart.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian bắt đầu nhỏ hơn thời gian bắt đầu");
        }
      } else {
        controller.controllerDateStart.value.text = date;
      }

    }
  }

  selectDateTimeEnd(stringTime, format,context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy')
            .parse(controller.controllerDateStart.value.text)
            .millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch) {
          controller.controllerDateEnd.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian kết thúc lớn hơn thời gian bắt đầu");
        }
      } else {
        controller.controllerDateEnd.value.text = date;

      }

    }
  }
}
