import 'package:flutter/cupertino.dart';
import 'package:flutter_neat_and_clean_calendar/flutter_neat_and_clean_calendar.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/model/common/diligence.dart';
import 'package:slova_lms/data/model/common/school_year.dart';
import 'package:slova_lms/data/model/common/user_profile.dart';
import 'package:slova_lms/data/repository/diligence/diligence_repo.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';
class DetailStatisticalAllStudentController extends GetxController{
  var format = CalendarFormat.month.obs;
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;
  var focusName = FocusNode().obs;
  var controllerName = TextEditingController().obs;
  var isAddShowJob = false.obs;
  var dateNow = "".obs;
  var dayon ="".obs;
  var showDetailDiligence = true.obs;
  var showStatusDiligence = true.obs;
  String textOnTime = "Đúng giờ";
  String textNotOnTime= "Đi muộn";
  String textAbsentWithoutLeave = "Không phép";
  var outputFormat = DateFormat('HH:mm:ss');
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  String textExcusedAbsence = "Có phép";
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var userProfile = UserProfile().obs;
  RxList<SchoolYear> dateTime = <SchoolYear>[].obs;
  var diligence = Diligence().obs;
  var outputMonthFormat = DateFormat('MM/yyyy');
  var isvisibility = false.obs;
  var isvisibilityIcon = false.obs;
  var isvisibilityLatetime = false.obs;
  var textColor = false.obs;
  RxList<String> listStatus = <String>[].obs;
  RxList<Items> item = <Items>[].obs;
  RxList<StatusDetailDiligence> dayonShool = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayAbsent = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayExcused = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayNotontime = <StatusDetailDiligence>[].obs;
  var detailDiligence = DetailDiligence().obs;
  var studentOnTime = StudentOnSchool().obs;
  var studentNotOnTime = StudentNotOnTime().obs;
  var studentExcused = StudentExcusedAbsence().obs;
  var studentAbsend = StudentAbsentWithoutLeave().obs;
  var totalDayOnTime = 0.obs;
  var totalDayNotOnTime = 0.obs;
  var totalDayAbsend = 0.obs;
  var totalDayExsecud = 0.obs;
  var selectDate = "".obs;
  var allStatus = <StatusDetailDiligence>[].obs;
  var eventList = <NeatCleanCalendarEvent>[].obs;
  var objectEvent = NeatCleanCalendarEvent("", startTime: DateTime.now(), endTime: DateTime.now()).obs;
  var objectEventTest = NeatCleanCalendarEvent("", startTime: DateTime.now(), endTime: DateTime.now()).obs;
var dateStart ="".obs;
var dateEnd ="".obs;
  var outputDateToDateFormat = DateFormat('dd/MM/yyyy ');
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;
  var eventTest = <NeatCleanCalendarEvent>[].obs;

  @override
  void onInit() {
    super.onInit();
    var data = Get.arguments;
    if(data != null){
      dateStart.value = data[0];
      dateEnd.value = data[1];
    }

    if(DateTime.now().month<=12&&DateTime.now().month>9){
      dateStart.value = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month));
      dateEnd.value = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month+1).subtract(const Duration(days: 1)));

    }else{
      dateStart.value = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month));
      dateEnd.value = outputDateToDateFormat.format(DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month+1).subtract(const Duration(days: 1)));
    }


    getListDiligence();

    // setDateSchoolYears();

    showDetailDiligence.value = false;
    showStatusDiligence.value = false;


  }


  getColorStatus(state) {
    switch (state) {
      case "ON_TIME":
        return const Color.fromRGBO(221, 246, 235, 1);
      case "NOT_ON_TIME":
        return const Color.fromRGBO(255, 239, 203, 1);
      case "ABSENT_WITHOUT_LEAVE":
        return const Color.fromRGBO(255, 200, 206, 1);
      default:
        return const Color.fromRGBO(210, 210, 210, 1);
    }
  }

  getColorTextStatus(state) {
    switch (state) {
      case "ON_TIME":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "NOT_ON_TIME":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "ABSENT_WITHOUT_LEAVE":
        return const Color.fromRGBO(255, 69, 89, 1);
      default:
        return const Color.fromRGBO(136, 136, 136, 1);
    }
  }

  getTextStatus(state) {
    switch (state) {
      case "ON_TIME":
        return textOnTime;
      case "NOT_ON_TIME":
        return textNotOnTime;
      case "ABSENT_WITHOUT_LEAVE":
        return textAbsentWithoutLeave;
      default:
        return textExcusedAbsence;
    }
  }

  getListDiligence(){
    var userId = Get.find<StudentHomeController>().userProfile.value.id;
    var fromdate = DateTime(int.parse(dateStart.value.substring(6,10)), int.parse(dateStart.value.substring(3,5)),int.parse(dateStart.value.substring(0,2))).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(dateEnd.value.substring(6,10)), int.parse(dateEnd.value.substring(3,5)),int.parse(dateEnd.value.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.detailListDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayonShool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotontime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        allStatus.addAll(dayonShool);
        allStatus.addAll(dayNotontime);
        allStatus.addAll(dayAbsent);
        allStatus.addAll(dayExcused);
        getStatusAll(dateStart.value, dateEnd.value);
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbsend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExsecud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  getListDiligenceToSelectYear(fromdate, todate){
    var userId = Get.find<StudentHomeController>().userProfile.value.id;
    var fromdate = DateTime(int.parse(toYearSelect.value.substring(0,4)), DateTime.now().month).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), DateTime.now().month+1).subtract(const Duration(days: 1)).millisecondsSinceEpoch;
    _diligenceRepo.detailListDiligence(userId, fromdate, todate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayonShool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotontime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        allStatus.addAll(dayonShool);
        allStatus.addAll(dayNotontime);
        allStatus.addAll(dayAbsent);
        allStatus.addAll(dayExcused);
        getStatusAll(dateStart.value, dateEnd.value);
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbsend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExsecud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }


  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<StudentHomeController>().fromYearPresent.value == DateTime.now().year) {
        getListDiligence();
        getStatusAll(dateStart.value, dateEnd.value);
        return;
      } else {
        fromYearSelect.value = "${Get.find<StudentHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<StudentHomeController>().fromYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
        getStatusAll(dateStart.value, dateEnd.value);

      }
    } else {
      if (Get.find<StudentHomeController>().toYearPresent.value == DateTime.now().year) {
        getListDiligence();
        getStatusAll(dateStart.value, dateEnd.value);
        return;
      } else {
        fromYearSelect.value = "${Get.find<StudentHomeController>().toYearPresent}";
        toYearSelect.value = "${Get.find<StudentHomeController>().toYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
        getStatusAll(dateStart.value, dateEnd.value);
      }
    }
  }

  getStatusAll(fromdate,todate){
    for(int i=0; i<allStatus.length; i++){
      eventList.add(
        NeatCleanCalendarEvent('Chi tiết',
        location: outputFormat.format(DateTime.fromMillisecondsSinceEpoch(allStatus[i].updatedAt!)),
        description:"${getTextStatus(allStatus[i].statusDiligent)}",
        startTime: DateTime.fromMillisecondsSinceEpoch(allStatus[i].date!),
        endTime:DateTime.fromMillisecondsSinceEpoch(allStatus[i].date!),
        color:getColorTextStatus(allStatus[i].statusDiligent),
      ),
      );
      eventTest.value = eventList.where(( element) =>
      element.startTime.year == DateTime.now().year
          && element.startTime.month == DateTime.now().month
          && element.startTime.day == DateTime.now().day
      ).toList();
      if(eventTest.isNotEmpty && eventTest[0].startTime.day == DateTime.now().day){
        objectEvent.value = eventTest[0];
        dateNow.value= outputDateFormat.format(eventTest[0].startTime);
        focusedDay.value = DateTime.now();
        showDetailDiligence.value= true;
      }else{
        dateNow.value= outputDateFormat.format(eventList[i].startTime);
        focusedDay.value = DateTime.now();

      }



    }

  }




}