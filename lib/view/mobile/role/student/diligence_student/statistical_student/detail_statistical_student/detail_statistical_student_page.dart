import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';
import 'package:flutter_neat_and_clean_calendar/flutter_neat_and_clean_calendar.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_page.dart';
import 'detail_statistical_student_controller.dart';

class DetailStatisticalStudentPage
    extends GetWidget<DetailStatisticalStudentController> {
  final controller = Get.put(DetailStatisticalStudentController());

  @override
  Widget build(BuildContext context) {
    var  eventList = [
      NeatCleanCalendarEvent('',
        isMultiDay: false,
        description: '${controller.getTextStatus(controller.status.value)}',
        startTime: DateTime.fromMillisecondsSinceEpoch(controller.date.value),
        endTime: DateTime.fromMillisecondsSinceEpoch(controller.date.value),
        color: controller.getColorTextStatus(controller.status.value),

      ),
    ].obs;
    return Obx(() => Scaffold(
          backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {
                  Get.to(StudentHomePage());
                },
                icon: const Icon(Icons.home),
                color: Colors.white,
              )
            ],
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            title: Text(
              'Chuyên Cần',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body:    SingleChildScrollView(
              child:
              Column(
                children: [
                  Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(top: 16.h, left: 16.w, bottom: 8.h),
                        child: Text(
                          'Chi Tiết',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: const Color.fromRGBO(26, 26, 26, 1), fontSize: 12.sp),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 16.w, right: 16.w),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: const Color.fromRGBO(255, 255, 255, 1)),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 16.h, right: 16.h, left: 16.h),
                              child: Row(
                                children: [
                                  SizedBox(
                                      height: 40.h,
                                      width: 40,
                                      child:
                                      CacheNetWorkCustom(urlImage: '${ Get.find<StudentHomeController>().userProfile.value.image}',)

                                  ),

                                  Padding(padding: EdgeInsets.only(left: 8.w)),
                                  Text(
                                    '${Get.find<StudentHomeController>().userProfile.value.fullName}',
                                    style: TextStyle(
                                        color: const Color.fromRGBO(26, 26, 26, 1),
                                        fontSize: 12.sp),
                                  ),
                                  Expanded(child: Container()),
                                  Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.date.value)),
                                    style: TextStyle(
                                        color: const Color.fromRGBO(26, 26, 26, 1),
                                        fontSize: 12.sp),
                                  )
                                ],
                              ),
                            ),
                            Obx(() => SizedBox(
                              child:  Calendar(
                                eventListBuilder: (BuildContext context,
                                    List<NeatCleanCalendarEvent> selectesdEvents) {
                                  return const Text("");
                                },

                                bottomBarArrowColor: Colors.black87,
                                bottomBarColor: Colors.orange,
                                startOnMonday: true,
                                weekDays: const ['Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'CN'],
                                eventsList: eventList,
                                isExpandable: false,
                                eventDoneColor: Colors.green,
                                selectedColor: Colors.orange,
                                selectedTodayColor: Colors.orange,
                                todayColor: Colors.orange,
                                eventColor: null,
                                locale: 'vi',
                                initialDate: controller.focusedDay.value,
                                onDateSelected: (value) {
                                  for(int i =0; i<eventList.length; i++){
                                    if(eventList[i].startTime == value){
                                      controller.objectEvent.value = eventList[i];
                                      controller.focusedDay.value= eventList[i].startTime;
                                      controller.showDetailDiligence.value = true;
                                      return;
                                    }else{
                                      controller.showDetailDiligence.value = false;
                                    }
                                  }
                                },
                                todayButtonText: 'Ngày hôm nay',
                                multiDayEndText: 'Kết thúc',
                                isExpanded: true,
                                onEventSelected: (value) {
                                },
                                onEventLongPressed: (value) {
                                },
                                hideTodayIcon: true,
                                hideBottomBar: true,
                                expandableDateFormat: 'EEEE, dd/MM/yyyy',
                                datePickerType: DatePickerType.hidden,
                                dayOfWeekStyle: const TextStyle(
                                    color: Colors.orange, fontWeight: FontWeight.w800, fontSize: 10),
                              ),
                            )),
                            // dateTimeSelectV2()
                          ],
                        ),
                      ),
                      controller.showDetailDiligence.value?
                      Container(
                        margin: EdgeInsets.only(top: 16.h, right: 16.w, left: 16.w),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: const Color.fromRGBO(255, 255, 255, 1)),
                        child:
                        Column(
                          children: [
                            ListTile(
                              title : Column(
                                children: [
                                  Padding(padding: EdgeInsets.only(top: 8.h)),
                                  SizedBox(
                                      width: double.infinity,
                                      child: Row(
                                        children: [
                                          Image.asset('assets/images/icon_diligence_student.png', width: 12.w,height: 12.h,),
                                          Padding(padding: EdgeInsets.only(right: 8.r)),
                                          Text('Điểm danh lúc ${controller.outputFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.time.value))}',style: TextStyle(color: Colors.black, fontSize: 12.sp, fontWeight: FontWeight.w400),)
                                        ],
                                      )
                                  ),
                                  Container(

                                      margin: EdgeInsets.only(left:10.w, top: 4.h),
                                      child:   Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(padding: EdgeInsets.only(top:6.h)),
                                          Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              const Icon(
                                                Icons.location_on,
                                                color: Color.fromRGBO(177, 177, 177, 1),
                                                size: 12,
                                              ),
                                              Container(
                                                height: 30.h,
                                                width:250.w,
                                                margin: const EdgeInsets.only(left: 6),
                                                child: Text(
                                                  '${Get.find<StudentHomeController>().userProfile.value.school!.name}, ${Get.find<StudentHomeController>().userProfile.value.school!.address}',
                                                  style: TextStyle(
                                                      color: const Color.fromRGBO(177, 177, 177, 1),
                                                      fontSize: 10.sp),
                                                ),
                                              )
                                            ],
                                          ),
                                          Padding(padding: EdgeInsets.only(top: 8.h)),
                                          Container(
                                            height: 20.sp,
                                            padding: const EdgeInsets.only(top: 3,bottom: 3,right: 12,left: 12),
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(6),
                                              color: controller.getColorStatus(controller.status.value),
                                            ),
                                            child:
                                            Text('${controller.getTextStatus(controller.status.value)}', style:  TextStyle(color: controller.getColorTextStatus(controller.status.value), fontSize: 10.sp),),),
                                        ],
                                      )
                                  ),

                                ],
                              ),
                            ),
                          ],
                        ),

                      ): Container()
                    ],
                  ),

                ],
              )

          ),
        ));
  }
}
