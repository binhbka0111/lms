import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'on_time_controller.dart';

class OnTimeStudentPage extends GetView<OnTimeStudentController> {
  final controller = Get.put(OnTimeStudentController());
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
        child: Obx(() => Container(
              margin: EdgeInsets.only(left: 16.w, right: 16.w),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: Colors.white,
              ),
              child: ListView.builder(
                  itemCount: controller.listStudentOntime.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {},
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 16.w, right: 8.w),
                                child: Text(
                                  '${index + 1}',
                                  style: const TextStyle(
                                      color: Color.fromRGBO(26, 26, 26, 1),
                                      fontSize: 14),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 8.h, bottom: 4.h),
                                child: Row(
                                  children: [
                                    SizedBox(
                                      width: 48.w,
                                      height: 48.w,
                                      child:
                                      CacheNetWorkCustom(urlImage: '${ controller
                                          .listStudentOntime
                                      [index]
                                          .student!
                                          .image}',),

                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(right: 4.w)),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        RichText(
                                            text: TextSpan(children: [
                                          const TextSpan(
                                              text: 'Học Sinh: ',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      248, 129, 37, 1),
                                                  fontSize: 14)),
                                          TextSpan(
                                              text:
                                                  '${controller.listStudentOntime[index].student?.fullName}',
                                              style: const TextStyle(
                                                  color: Color.fromRGBO(
                                                      26, 26, 26, 1),
                                                  fontSize: 14)),
                                        ])),
                                        Padding(
                                            padding: EdgeInsets.only(top: 4.h)),
                                        Visibility(
                                          visible: controller
                                              .listStudentOntime
                                              [index]
                                              .student!
                                              .birthday != null,
                                            child: Text(
                                          controller.outputDateFormat.format(
                                              DateTime
                                                  .fromMillisecondsSinceEpoch(
                                                  controller
                                                      .listStudentOntime
                                                      [index]
                                                      .student!
                                                      .birthday ??
                                                      0)),
                                          style: const TextStyle(
                                              color: Color.fromRGBO(
                                                  133, 133, 133, 1),
                                              fontSize: 14),
                                        ))
                                      ],
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                          const Divider()
                        ],
                      ),
                    );
                  }),
            )));
  }
}
