import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/diligence.dart';
import '../../../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../student_home_controller.dart';
import '../../diligence_monitor_student_controller.dart';
import '../detail_diligent_management_student_controller.dart';

class AbsentWithoutLeaveStudentController extends GetxController{
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudentAbsentWithoutLeave = <Diligents>[].obs;
  var listStudent = <Diligents>[].obs;
  var student = StudentDiligent().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var selectedDate = DateTime.now().obs;
  @override
  void onInit() {
    getListStudentAbsentWithoutLeave();
    super.onInit();
  }



  getListStudentAbsentWithoutLeave(){
    var classId =Get.find<StudentHomeController>().clazzs[0].id;
    if(Get.isRegistered<DetailDiligenceManagementStudentController>()){
      selectedDate.value = Get.find<DetailDiligenceManagementStudentController>().selectedDay.value;
    }else{
      selectedDate.value = DateTime(
          Get.find<DiligenceManagementMonitorStudentController>().selectedDate.value.year,
          Get.find<DiligenceManagementMonitorStudentController>().selectedDate.value.month,
          Get.find<DiligenceManagementMonitorStudentController>().selectedDate.value.day);
    }
    var fromdate = DateTime(selectedDate.value.year, selectedDate.value.month, selectedDate.value.day,00,00).millisecondsSinceEpoch;
    var todate = DateTime(selectedDate.value.year, selectedDate.value.month, selectedDate.value.day,23,59).millisecondsSinceEpoch;

    _diligenceRepo.getListStudentDetailDiligence(classId, fromdate, todate, "ABSENT_WITHOUT_LEAVE").then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudent.value = diligenceClass.value.studentDiligent!;
        listStudentAbsentWithoutLeave.value = listStudent.where((element) => element.statusDiligent == "ABSENT_WITHOUT_LEAVE").toList();
      }
    });
  }
}