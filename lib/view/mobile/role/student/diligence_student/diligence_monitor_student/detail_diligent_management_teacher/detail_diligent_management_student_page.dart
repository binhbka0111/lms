import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import '../../../../../../../../commom/utils/color_utils.dart';
import '../../../../../../../../commom/widget/hexColor.dart';
import 'absent_without_leave/absent_without_leave_controller.dart';
import 'absent_without_leave/absent_without_leave_page.dart';
import 'detail_diligent_management_student_controller.dart';
import 'excused_absence/excused_absence_controller.dart';
import 'excused_absence/excused_absence_page.dart';
import 'not_on_time/not_on_time_controller.dart';
import 'not_on_time/not_on_time_page.dart';
import 'on_time/on_time_controller.dart';
import 'on_time/on_time_page.dart';

class DetailDiligentManagementStudentPage extends GetView<DetailDiligenceManagementStudentController> {
  final controller = Get.put(DetailDiligenceManagementStudentController());
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
    backgroundColor: ColorUtils.PRIMARY_COLOR,
    elevation: 0,
    title: Text(
      'Chi Tiết Chuyên Cần',
      style: TextStyle(
          color: Colors.white,
          fontSize: 16.sp,
          fontFamily: 'static/Inter-Medium.ttf'),
    ),
      ),
      body: Obx(() => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Container(
        margin: EdgeInsets.symmetric(horizontal: 16.w,vertical: 8.h),
        child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 8.h, left: 8.w),
                child: Text(
                  "${controller.selectedDay.value.year}",
                  style: TextStyle(
                      color: ColorUtils.PRIMARY_COLOR,
                      fontSize: 13.sp),
                ),
              ),
              Row(
                children: [
                  Expanded(
                      child: Container(
                        margin: EdgeInsets.only(top: 4.h, left: 8.w, bottom: 4.h),
                        child: Text(
                          "Tháng ${controller.focusedDay.value.month}",
                          style: TextStyle(color: Colors.black87, fontSize: 14.sp),
                        ),
                      )),
                  InkWell(
                    child: (controller.weekNumber.value == 1)
                        ? const Icon(
                      Icons.expand_more,
                      size: 24,
                    )
                        : const Icon(
                      Icons.expand_less,
                      size: 24,
                    ),
                    onTap: () {
                      controller.changeShowFull();
                    },
                  ),
                  const SizedBox(
                    width: 16,
                  )
                ],
              ),
              SizedBox(
                height: controller.heightOffset.value,
                child: SfDateRangePicker(
                  viewSpacing: 10,
                  todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),
                  controller: controller.monthController.value,
                  view: DateRangePickerView.month,
                  enablePastDates: true,
                  selectionMode: DateRangePickerSelectionMode.single,
                  showNavigationArrow: true,
                  selectionRadius: 15,
                  monthFormat: "MMM",
                  headerHeight: 0,
                  selectionColor: const Color.fromRGBO(249, 154, 81, 1),
                  selectionShape: DateRangePickerSelectionShape.circle,
                  selectionTextStyle: const TextStyle(
                      color: ColorUtils.COLOR_WHITE, fontWeight: FontWeight.w700),
                  monthCellStyle: const DateRangePickerMonthCellStyle(
                    todayTextStyle: TextStyle(
                        color: Color.fromRGBO(249, 154, 81, 1),
                        fontWeight: FontWeight.w700),
                  ),
                  headerStyle: DateRangePickerHeaderStyle(
                      textAlign: TextAlign.left,
                      textStyle: TextStyle(
                        fontStyle: FontStyle.normal,
                        fontSize: 12.sp,
                        color: Colors.black87,
                      )),
                  monthViewSettings: DateRangePickerMonthViewSettings(
                      numberOfWeeksInView: controller.weekNumber.value,
                      firstDayOfWeek: 1,
                      showTrailingAndLeadingDates: true,
                      viewHeaderHeight: 16.h,
                      dayFormat: 'EEE',
                      viewHeaderStyle: DateRangePickerViewHeaderStyle(
                          textStyle: TextStyle(
                              color: HexColor("#B1B1B1"),
                              fontWeight: FontWeight.w600,
                              fontSize: 10.sp))),
                  onViewChanged: (DateRangePickerViewChangedArgs args) {
                    Future.delayed(const Duration(milliseconds: 400), () {
                      var visibleDates = args.visibleDateRange;
                      controller.focusedDay.value =
                          visibleDates.startDate ?? DateTime.now();
                    });
                  },
                  onSelectionChanged: (DateRangePickerSelectionChangedArgs args) {
                    // var dateTime = args.value
                    if (args.value is PickerDateRange) {
                      final DateTime rangeStartDate = args.value.startDate;

                      controller.selectedDay.value = rangeStartDate;
                      controller.timelineController.value.displayDate =
                          rangeStartDate;
                    } else if (args.value is DateTime) {
                      final DateTime selectedDate = args.value;
                      controller.timelineController.value.displayDate =
                          selectedDate;

                      controller.selectedDay.value = selectedDate;
                    } else if (args.value is List<DateTime>) {
                      final List<DateTime> selectedDates = args.value;
                      controller.timelineController.value.displayDate =
                      selectedDates[0];
                      controller.selectedDay.value = selectedDates[0];
                    } else {

                    }
                    controller.getListStudent();
                    Get.find<AbsentWithoutLeaveStudentController>().getListStudentAbsentWithoutLeave();
                    Get.find<ExcusedAbsenceStudentController>().getListStudentExcusedAbsence();
                    Get.find<NotOnTimeStudentController>().getListStudentNotOnTime();
                    Get.find<OnTimeStudentController>().getListStudentOntime();
                  },
                ),
              )
            ],
          ),
        ),
      ),
      Container(
        margin: EdgeInsets.only(left: 16.w),
        height: 50,
        child: ListView.builder(
            itemCount: controller.listStatus.length,
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context,index){
              return Obx(() => TextButton(
                  onPressed: () {
                    controller.indexClick.value = index;
                    controller.showColor(controller.indexClick.value);
                    controller.pageController.animateToPage(index,
                        duration: const Duration(seconds: 1),
                        curve: Curves.easeOutBack);
                  },
                  child: Text(
                    "${controller.listStatus[index]} (${controller.getListStatus(index) ?? 0})",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 12.sp,
                        color: controller.indexClick.value == index
                            ? const Color.fromRGBO(248, 129, 37, 1)
                            : const Color.fromRGBO(177, 177, 177, 1)),
                  )));
            }),
      ),
      Expanded(
          child: PageView(
            onPageChanged: (value) {
              controller.onPageViewChange(value);
            },
            controller: controller.pageController,
            physics: const ScrollPhysics(),
            children: [OnTimeStudentPage(), NotOnTimeStudentPage(), ExcusedAbsenceStudentPage(),AbsentWithoutLeaveStudentPage()],
          )),
    ],
      )),
    );
  }
}
