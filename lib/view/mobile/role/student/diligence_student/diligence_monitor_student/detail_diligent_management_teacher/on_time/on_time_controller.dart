import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/view/mobile/role/student/diligence_student/diligence_monitor_student/diligence_monitor_student_controller.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../../data/model/common/diligence.dart';
import '../../../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../student_home_controller.dart';
import '../detail_diligent_management_student_controller.dart';

class OnTimeStudentController extends GetxController{
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudentOntime = <Diligents>[].obs;
  var listStudent = <Diligents>[].obs;
  var student = StudentDiligent().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var selectedDate = DateTime.now().obs;
  @override
  void onInit() {
    getListStudentOntime();
    super.onInit();
  }



  getListStudentOntime(){
    var classId =Get.find<StudentHomeController>().clazzs[0].id;
    if(Get.isRegistered<DetailDiligenceManagementStudentController>()){
      selectedDate.value = Get.find<DetailDiligenceManagementStudentController>().selectedDay.value;
    }else{
      selectedDate.value = DateTime(
          Get.find<DiligenceManagementMonitorStudentController>().selectedDate.value.year,
          Get.find<DiligenceManagementMonitorStudentController>().selectedDate.value.month,
          Get.find<DiligenceManagementMonitorStudentController>().selectedDate.value.day);
    }
    var fromdate = DateTime(selectedDate.value.year, selectedDate.value.month, selectedDate.value.day,00,00).millisecondsSinceEpoch;
    var todate = DateTime(selectedDate.value.year, selectedDate.value.month, selectedDate.value.day,23,59).millisecondsSinceEpoch;

    _diligenceRepo.getListStudentDetailDiligence(classId, fromdate, todate, "ON_TIME").then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudent.value = diligenceClass.value.studentDiligent!;
        listStudentOntime.value = listStudent.where((element) => element.statusDiligent == "ON_TIME").toList();
      }
    });
  }
}