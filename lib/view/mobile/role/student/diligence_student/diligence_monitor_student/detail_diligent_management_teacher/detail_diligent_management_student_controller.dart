import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/diligence_student/diligence_monitor_student/detail_diligent_management_teacher/absent_without_leave/absent_without_leave_page.dart';
import 'package:slova_lms/view/mobile/role/student/diligence_student/diligence_monitor_student/detail_diligent_management_teacher/excused_absence/excused_absence_page.dart';
import 'package:slova_lms/view/mobile/role/student/diligence_student/diligence_monitor_student/detail_diligent_management_teacher/not_on_time/not_on_time_page.dart';
import 'package:slova_lms/view/mobile/role/student/diligence_student/diligence_monitor_student/detail_diligent_management_teacher/on_time/on_time_page.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/diligence.dart';
import '../../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../student_home_controller.dart';
import '../diligence_monitor_student_controller.dart';

class DetailDiligenceManagementStudentController extends GetxController{
  DateTime now = DateTime.now();
  RxInt indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  var countOnTime = 0.obs;
  var selectedDay = DateTime.now().obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudentOntime = <Diligents>[].obs;
  var listStudentNotOnTime = <Diligents>[].obs;
  var listStudentExcusedAbsence = <Diligents>[].obs;
  var listStudentAbsentWithoutLeave = <Diligents>[].obs;
  var listStudent = <Diligents>[].obs;
  var student = StudentDiligent().obs;
  var timelineController = CalendarController().obs;
  var weekNumber = 1.obs;
  var heightOffset = (Get.width/5).obs;
  var monthController = DateRangePickerController().obs;
  var quarterTurns = 270.obs;
  var focusedDay = DateTime.now().obs;
  var showFull = false;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  var listStatus = <String>[].obs;
  var listPage = <Widget>[];
  final onTime = OnTimeStudentPage();
  final notOnTimePage = NotOnTimeStudentPage();
  final excusedAbsencePage = ExcusedAbsenceStudentPage();
  final absentWithoutLeavePage = AbsentWithoutLeaveStudentPage();

  @override
  void onInit() {
    super.onInit();

    if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_ON_TIME_LIST)){
      listStatus.add("Đi học đúng giờ");
      listPage.add(onTime);
    }
    if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_NOT_ON_TIME_LIST)){
      listStatus.add("Đi học muộn");
      listPage.add(notOnTimePage);
    }
    if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_EXCUSED_ABSENCE_LIST)){
      listStatus.add("Nghỉ có phép");
      listPage.add(excusedAbsencePage);
    }
    if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_ABSENT_WITHOUT_LIST)){
      listStatus.add("Nghỉ không phép");
      listPage.add(absentWithoutLeavePage);
    }

    selectedDay.value = DateTime(
        Get.find<DiligenceManagementMonitorStudentController>().selectedDate.value.year,
        Get.find<DiligenceManagementMonitorStudentController>().selectedDate.value.month,
        Get.find<DiligenceManagementMonitorStudentController>().selectedDate.value.day);
    monthController.value.selectedDate = selectedDay.value;
    getListStudent();
  }

  @override
  dispose() {
    pageController.dispose();
    super.dispose();
  }
  getListStudent(){
    var classId =Get.find<StudentHomeController>().clazzs[0].id;
    var fromdate = DateTime(selectedDay.value.year, selectedDay.value.month, selectedDay.value.day,00,00).millisecondsSinceEpoch;
    var todate = DateTime(selectedDay.value.year, selectedDay.value.month, selectedDay.value.day,23,59).millisecondsSinceEpoch;
    listStudent.clear();
    listStudentOntime.clear();
    listStudentNotOnTime.clear();
    listStudentExcusedAbsence.clear();
    listStudentAbsentWithoutLeave.clear();
    _diligenceRepo.getListStudentDetailDiligence(classId, fromdate, todate, "").then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudent.value = diligenceClass.value.studentDiligent!;
        listStudentOntime.value = listStudent.where((element) => element.statusDiligent == "ON_TIME").toList();
        listStudentNotOnTime.value = listStudent.where((element) => element.statusDiligent == "NOT_ON_TIME").toList();
        listStudentExcusedAbsence.value = listStudent.where((element) => element.statusDiligent == "EXCUSED_ABSENCE").toList();
        listStudentAbsentWithoutLeave.value = listStudent.where((element) => element.statusDiligent == "ABSENT_WITHOUT_LEAVE").toList();
      }
    });
    listStudent.refresh();
    listStudentOntime.refresh();
    listStudentNotOnTime.refresh();
    listStudentExcusedAbsence.refresh();
    listStudentAbsentWithoutLeave.refresh();
  }

  getListStatus(index){
    switch(index){
      case 0:
        return listStudentOntime.length;
      case 1:
        return listStudentNotOnTime.length;
      case 2:
        return listStudentExcusedAbsence.length;
      case 3:
        return listStudentAbsentWithoutLeave.length;
    }
  }



  onPageViewChange(int page) {
    indexClick.value = page;
    if(page != 0) {
      indexClick.value-1;
    } else {
      indexClick.value = 0;
    }

  }


  showColor(index) {
    click.value = [];
    for (int i = 0; i < 4; i++) {
      click.add(false);
    }
    click[index] = true;
  }


  setWeekDay(weekDay){
    switch(weekDay){
      case 8:
        return "Chủ Nhật";
      default:
        return "Thứ $weekDay";
    }
  }

  void changeCalendar() {
    if(showFull){
      weekNumber.value = 4;
      heightOffset.value = (Get.width/2);
      quarterTurns.value = 90;
    } else {
      heightOffset.value = (Get.width/5);
      weekNumber.value = 1;
      quarterTurns.value = 270;
    }
  }

  void changeShowFull() {
    showFull = !showFull;
    changeCalendar();
  }
}