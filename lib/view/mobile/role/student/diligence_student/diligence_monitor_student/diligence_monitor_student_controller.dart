import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/diligence_student/diligence_monitor_student/attendance_monitor_student/attendance_monitor_student_page.dart';
import 'package:slova_lms/view/mobile/role/student/diligence_student/diligence_monitor_student/list_leave_application_student/list_leave_application_student_page.dart';
import '../../../../../../data/base_service/api_response.dart';
import '../../../../../../data/model/common/diligence.dart';
import '../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../student_home_controller.dart';
import 'attendance_monitor_student/attendance_monitor_student_controller.dart';
import 'list_leave_application_student/list_leave_application_student_controller.dart';


class DiligenceManagementMonitorStudentController extends GetxController {
  DateTime now = DateTime.now();
  var indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  var colorText = const Color.fromRGBO(177, 177, 177, 1).obs;
  var color = const Color.fromRGBO(246, 246, 246, 1).obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var selectedDate = DateTime.now().obs;
  var cupertinoDatePicker = DateTime.now().obs;
  var offset = 0.0.obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  var itemsLeavingApplications = <ItemsLeavingApplication>[].obs;
  var diligenceClass = DiligenceClass().obs;
  var listStudentDiligence = <Diligents>[].obs;
  var listStatus = <String>[].obs;
  var isShowPopupMenu = false.obs;
  var listPage = <Widget>[];
  final homeController = Get.find<HomeController>();
  var attendance = AttendanceMonitorStudentPage();
  var listLeaveApplication = ListLeaveApplicationStudentPage();

  @override
  void onInit() {

    if(DateTime.now().month<=12&&DateTime.now().month>9){
      selectedDate.value = DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
      cupertinoDatePicker.value = DateTime(Get.find<StudentHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day);
    }else{
      selectedDate.value = DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
      cupertinoDatePicker.value = DateTime(Get.find<StudentHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day);
    }
    pageController.addListener(() {
      if(indexClick.value == 0){
        Get.put(AttendanceStudentController());
      }
    });
    getListStudentDiligence();
    getListLeavingApplicationTeacher();
    for (int i = 0; i < 2; i++) {
      click.add(false);
    }

    if(checkVisiblePage(homeController.userGroupByApp, StringConstant.PAGE_ATTENDANCE)){
      listStatus.add("Điểm danh");
      listPage.add(attendance);
    }
    if(checkVisiblePage(homeController.userGroupByApp, StringConstant.PAGE_LIST_LEAVE_APPLICATION)){
      listStatus.add("Danh sách đơn xin nghỉ");
      listPage.add(listLeaveApplication);
    }

    super.onInit();
  }


  @override
  dispose() {
    pageController.dispose();
    super.dispose();
  }

  onPageViewChange(int page) {
    indexClick.value = page;
    if (page != 0) {
      indexClick.value - 1;
    } else {
      indexClick.value = 0;
    }
  }

  showColor(index) {
    click.value = [];
    for (int i = 0; i < 2; i++) {
      click.add(false);
    }
    click[index] = true;
  }

  setColorButtonAttendance(RxList<int> groupValue) {
    if (groupValue.contains(0) == true) {
      color.value = const Color.fromRGBO(246, 246, 246, 1);
    } else {
      color.value = const Color.fromRGBO(248, 129, 37, 1);
      if (Get.find<AttendanceStudentController>().status.value == "CONFIRM") {
        color.value = const Color.fromRGBO(246, 246, 246, 1);
      }
    }
  }

  setColorTextButtonAttendance(RxList<int> groupValue) {
    if (groupValue.contains(0)) {
      colorText.value = const Color.fromRGBO(177, 177, 177, 1);
    } else {
      colorText.value = const Color.fromRGBO(255, 255, 255, 1);
      if (Get.find<AttendanceStudentController>().status.value == "CONFIRM") {
        colorText.value = const Color.fromRGBO(177, 177, 177, 1);
      }
    }
  }

  getListStudentDiligence() {
    var classId = Get.find<StudentHomeController>().clazzs[0].id;
    var fromDate = DateTime(selectedDate.value.year, selectedDate.value.month,
        selectedDate.value.day, 00, 00)
        .millisecondsSinceEpoch;
    var toDate = DateTime(selectedDate.value.year, selectedDate.value.month,
        selectedDate.value.day, 23, 59)
        .millisecondsSinceEpoch;

    _diligenceRepo
        .getListStudentDiligence(classId, fromDate, toDate, "")
        .then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudentDiligence.value = diligenceClass.value.studentDiligent!;
      }
    });

  }
  getListLeavingApplicationTeacher() async{
    itemsLeavingApplications.clear();
    var classId =Get.find<StudentHomeController>().clazzs[0].id;
    var fromDate = DateTime(selectedDate.value.year,selectedDate.value.month,selectedDate.value.day,00,00,00).millisecondsSinceEpoch;
    var toDate = DateTime(selectedDate.value.year,selectedDate.value.month,selectedDate.value.day,23,59,59).millisecondsSinceEpoch;
    _diligenceRepo.getListLeavingApplicationTeacher(classId,fromDate,toDate,"","","").then((value) {
      if (value.state == Status.SUCCESS) {
        itemsLeavingApplications.value = value.object!;

      }
    });
    itemsLeavingApplications.refresh();
  }



  getListStatus(index){
    switch(index){
      case 0:
        return  Get.find<AttendanceStudentController>().listStudentDiligence.length;
      case 1:
        return Get.find<ListLeaveApplicationStudentController>().listApprove.length;
    }
  }


  onSelectedDateDiligence(){
    Get.find<AttendanceStudentController>().selectedDay.value = selectedDate.value;
    Get.find<ListLeaveApplicationStudentController>().selectedDay.value = selectedDate.value;
    Get.find<AttendanceStudentController>().onInit();
    Get.find<ListLeaveApplicationStudentController>().onInit();
    if(selectedDate.value.year == DateTime.now().year&&selectedDate.value.month == DateTime.now().month&&selectedDate.value.day == DateTime.now().day){
      Get.find<AttendanceStudentController>().isShowSwitchAttendance.value = true;
    }else{
      Get.find<AttendanceStudentController>().isShowSwitchAttendance.value = false;
    }


    if(Get.isRegistered<AttendanceStudentController>()) {
      Get
          .find<AttendanceStudentController>()
          .groupValue
          .clear();
      Get
          .find<ListLeaveApplicationStudentController>()
          .itemsLeavingApplications
          .clear();
    }

    if(Get.isRegistered<ListLeaveApplicationStudentController>()) {
      Get.find<AttendanceStudentController>().getListStudentDiligence();
      Get.find<ListLeaveApplicationStudentController>()
          .getListLeavingApplicationTeacher();

      Get
          .find<AttendanceStudentController>()
          .groupValue
          .refresh();
      Get
          .find<ListLeaveApplicationStudentController>()
          .itemsLeavingApplications
          .refresh();
    }
    update();

  }


  comeBackAttendance() {
    //Move tab to first
    pageController.animateTo(0, duration: const Duration(seconds: 1), curve: Curves.bounceIn);
  }

}
