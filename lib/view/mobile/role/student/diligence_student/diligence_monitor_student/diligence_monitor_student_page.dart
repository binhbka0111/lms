import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'attendance_monitor_student/attendance_monitor_student_controller.dart';
import 'diligence_monitor_student_controller.dart';
import 'list_leave_application_student/list_leave_application_student_controller.dart';


class DiligentManagementMonitorStudentPage extends GetWidget<DiligenceManagementMonitorStudentController> {
  @override
  final controller = Get.put(DiligenceManagementMonitorStudentController());
  final homeController = Get.find<HomeController>();

  DiligentManagementMonitorStudentPage({super.key});
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        elevation: 0,
        title: Text(
          'Quản Lý Chuyên Cần',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RefreshIndicator(
              color: ColorUtils.PRIMARY_COLOR,
              child: SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InkWell(
                      child: Obx(() => Container(
                        margin: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 8.h),
                        padding: EdgeInsets.all(16.h),
                        decoration: BoxDecoration(
                            color: Colors.white, borderRadius: BorderRadius.circular(6)),
                        child:
                        InkWell(
                          onTap: () {
                            showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return Wrap(
                                    children: [
                                      Row(
                                        children: [
                                          TextButton(
                                              onPressed: () {
                                                Get.back();
                                              },
                                              style: ElevatedButton.styleFrom(
                                                  backgroundColor: Colors.white),
                                              child: const Text(
                                                "Hủy",
                                                style: TextStyle(
                                                    color:
                                                    Color.fromRGBO(123, 123, 123, 1)),
                                              )),
                                          Expanded(child: Container()),
                                          TextButton(
                                              onPressed: () {
                                                controller.selectedDate.value= controller.cupertinoDatePicker.value;
                                                controller.onSelectedDateDiligence();
                                                controller.selectedDate.refresh();
                                                Get.back();
                                              },
                                              style: ElevatedButton.styleFrom(
                                                  backgroundColor: Colors.white),
                                              child: const Text(
                                                "Xong",
                                                style: TextStyle(
                                                    color: ColorUtils.PRIMARY_COLOR),
                                              )),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 300, // Just as an example
                                        child: CupertinoDatePicker(
                                          mode: CupertinoDatePickerMode.date,
                                          initialDateTime: controller.cupertinoDatePicker.value,
                                          onDateTimeChanged: (DateTime dateTime) {
                                            controller.cupertinoDatePicker.value = dateTime;
                                          },
                                        ),
                                      ),
                                    ],
                                  );
                                });
                          },
                          child:  Row(
                            children: [
                              Text(
                                DateFormat('d').format(controller.selectedDate.value),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 44.sp),
                              ),
                              Padding(
                                padding: EdgeInsets.only(right: 4.w),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Thứ ${controller.selectedDate.value.weekday+1}",
                                    style: TextStyle(
                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14.sp),
                                  ),
                                  Text(
                                    "Tháng ${controller.selectedDate.value.month.toString()}, ${controller.selectedDate.value.year.toString()}",
                                    style: TextStyle(
                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14.sp),
                                  ),
                                ],
                              ),
                              Expanded(child: Container()),
                              Visibility(
                                visible: checkVisiblePage(homeController.userGroupByApp, StringConstant.PAGE_DETAIL_DILIGENCE),
                                child: InkWell(
                                  onTap: () {
                                    Get.toNamed(Routes.detailDiligentManagementStudentPage);
                                  },
                                  child: Container(
                                    padding:
                                    EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                                    decoration: BoxDecoration(
                                        color: const Color.fromRGBO(253, 221, 196, 1),
                                        borderRadius: BorderRadius.circular(6.r)),
                                    child: Text(
                                      "Chi Tiết",
                                      style: TextStyle(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.w500,
                                          color: ColorUtils.PRIMARY_COLOR),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      )),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16.w),
                      height: 50,
                      child: ListView.builder(
                          itemCount: controller.listStatus.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context,index){
                            return Obx(() => TextButton(
                                onPressed: () {
                                  controller.indexClick.value = index;
                                  controller.showColor(controller.indexClick.value);
                                  controller.pageController.animateToPage(index,
                                      duration: const Duration(seconds: 1),
                                      curve: Curves.easeOutBack);
                                },
                                child: Text(
                                  "${controller.listStatus[index]} (${controller.getListStatus(index) ?? 0})",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14.sp,
                                      color: controller.indexClick.value == index
                                          ? ColorUtils.PRIMARY_COLOR
                                          : const Color.fromRGBO(177, 177, 177, 1)),
                                )));
                          }),
                    )
                  ],
                ),
              ), onRefresh: ()async{
            Get.find<AttendanceStudentController>().onInit();
            Get.find<ListLeaveApplicationStudentController>().onInit();
            controller.onInit();
          }),
          Expanded(
              child: PageView(
                onPageChanged: (value) {
                  controller.onPageViewChange(value);
                },
                controller: controller.pageController,
                physics: const ScrollPhysics(),
                children: controller.listPage,
              )),
        ],
      )),
    );
  }
}
