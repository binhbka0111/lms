import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import '../../../../../../../routes/app_pages.dart';
import 'list_leave_application_student_controller.dart';


class ListLeaveApplicationStudentPage extends GetWidget<ListLeaveApplicationStudentController> {
  final controller = Get.put(ListLeaveApplicationStudentController());

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Obx(() => ListView.builder(
          itemCount: controller.listApprove.length,
          shrinkWrap: true,
          physics: const ScrollPhysics(),
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
              },
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 16.w, vertical: 4.h),
                padding:
                EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(6)),
                child: Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 32.w,
                          height: 32.w,
                          child:
                          CacheNetWorkCustom(urlImage: '${controller.listApprove[index].student!.image}',),
                        ),
                        Padding(padding: EdgeInsets.only(right: 8.w)),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${controller.listApprove[index].student!.fullName}",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.sp),
                            ),
                          ],
                        ),
                        Expanded(child: Container()),
                        InkWell(
                          onTap: () {
                            Get.toNamed(Routes.detailApprovedLeaveApplicationTeacher,arguments: controller.listApprove[index].id);
                          },
                          child: Text(
                            "Chi Tiết",
                            style: TextStyle(
                                color: const Color.fromRGBO(26, 59, 112, 1),
                                fontWeight: FontWeight.w500,
                                fontSize: 14.sp),
                          ),
                        )
                      ],
                    ),
                    const Divider(),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 80.w,
                          child: Text(
                            "Thời gian:",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: const Color.fromRGBO(133, 133, 133, 1),
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                        Flexible(
                            child: RichText(
                                text: TextSpan(children: [
                                  TextSpan(
                                    text: controller.outputDateFormat.format(
                                        DateTime.fromMillisecondsSinceEpoch(
                                            controller
                                                .listApprove[index].fromDate!)),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14.sp),
                                  ),
                                  TextSpan(
                                    text: " đến ",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14.sp),
                                  ),
                                  TextSpan(
                                    text: controller.outputDateFormat.format(
                                        DateTime.fromMillisecondsSinceEpoch(
                                            controller
                                                .listApprove[index]
                                                .toDate!)),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14.sp),
                                  ),
                                ])))
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    Row(crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 80.w,
                            child: Text(
                              "Người gửi:",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Text(
                            "${controller.listApprove[index].parent?.fullName}",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: const Color.fromRGBO(51, 157, 255, 1),
                                fontWeight: FontWeight.w400),
                          ),
                        ]),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 80.w,
                            child: Text(
                              "Lý do:",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Flexible(
                            child: Text(
                              "${controller.listApprove[index].reason}",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400),
                            ),
                          )
                        ]),
                    Padding(padding: EdgeInsets.only(top: 8.h)),
                    Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 80.w,
                            child: Text(
                              "Trạng thái:",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                              decoration: BoxDecoration(
                                  color: controller.getColorBackgroundStatus(controller.listApprove[index].status),
                                  borderRadius: BorderRadius.circular(6)
                              ),
                              child: Text(
                                "${controller.getStatus(controller.listApprove[index].status)}",
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    color: controller.getColorTextStatus(controller.listApprove[index].status),
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          )
                        ]),
                  ],
                ),
              ),
            );
          })),
    );
  }
}
