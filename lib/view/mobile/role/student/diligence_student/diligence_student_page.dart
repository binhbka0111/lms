import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'diligence_student_controller.dart';

class DiligenceStudentPage extends GetWidget<DiligenceStudentController> {
  final controller = Get.put(DiligenceStudentController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
      appBar: AppBar(
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Text(
          'Chuyên Cần',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 16.h, right: 16.h, left: 16.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Chuyên Cần',
              style: TextStyle(
                  color: const Color.fromRGBO(177, 177, 177, 1),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf',
                  fontSize: 13.sp),
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Container(
              padding: EdgeInsets.all(16.h),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.r),
                  color: const Color.fromRGBO(255, 255, 255, 1)),
              width: double.infinity,
              child: Row(
                children: [
                  Visibility(
                    visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp,
                        StringConstant.PAGE_STATISTICAL),
                    child: InkWell(
                      onTap: () {
                        controller.goToStatisticalStudent();
                      },
                      child: Column(
                        children: [
                          Image.asset('assets/images/thongke.png',width: 24.w,height: 24.h,),
                          Padding(padding: EdgeInsets.only(top: 8.h)),
                          Text(
                            'Thống Kê',
                            style: TextStyle(
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w500,
                                fontFamily:
                                'assets/font/static/Inter-Medium.ttf'),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(right: 52.h)),
                  Visibility(
                    visible: controller.visibleDiligenceStudent(),
                      child: InkWell(
                    onTap: () {
                      checkClickPage(Get.find<HomeController>().userGroupByApp,
                          () {
                            controller.goToAttendanceMonitorStudent();
                          }, StringConstant.PAGE_ATTENDANCE);
                    },
                    child: Column(
                      children: [
                        Image.asset('assets/images/approval.png',width: 24.w,height: 24.h,),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Text(
                          'Điểm danh',
                          style: TextStyle(
                              color: const Color.fromRGBO(26, 26, 26, 1),
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w500,
                              fontFamily:
                              'assets/font/static/Inter-Medium.ttf'),
                        )
                      ],
                    ),
                  )),
                ],
              ),

            )
          ],
        ),
      ),
    );

  }
}
