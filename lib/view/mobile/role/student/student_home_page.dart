import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/appointment_builder_timelineday.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/commom/widget/meeting_data_source.dart';
import 'package:slova_lms/view/mobile/event_news/event_body.dart';
import 'package:slova_lms/view/mobile/event_news/event_news_controller.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/personal_information_student/personal_information_student_page.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';
import '../../../../commom/app_cache.dart';
import '../../../../commom/widget/loading_custom.dart';
import '../../../../routes/app_pages.dart';
import 'package:card_swiper/card_swiper.dart';

class StudentHomePage extends GetWidget<StudentHomeController> {
  @override
  final controller = Get.put(StudentHomeController());

  StudentHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    var isPhone = Get.context?.isPhone ?? true;

    DateTime oldTime = DateTime.now();
    DateTime newTime = DateTime.now();
    // LocalNotificationService.initialize(context);
    return WillPopScope(
      onWillPop: () async {
        newTime = DateTime.now();
        int difference = newTime.difference(oldTime).inMilliseconds;
        oldTime = newTime;
        if (difference < 2000) {
          return true;
        } else {
          AppUtils.shared.showToast("Nhấn back lần nữa để thoát",
              backgroundColor: ColorUtils.COLOR_BLACK.withOpacity(0.5),
              textColor: ColorUtils.COLOR_WHITE);
          return false;
        }
      },
      child: Obx(() => SafeArea(child: Scaffold(
        body: controller.isReady.value
            ? RefreshIndicator(
            color: ColorUtils.PRIMARY_COLOR,
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Stack(
                children: [
                  buildBanner(),
                  Container(
                    margin:
                    EdgeInsets.only(top: (isPhone) ? 160.h : 180.h),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 8.w),
                          child: Column(
                            children: [
                              buildIndicatorBanner(),
                              Padding(
                                  padding:
                                  EdgeInsets.only(bottom: 8.h)),
                              Container(
                                decoration: BoxDecoration(
                                  color: const Color.fromRGBO(
                                      255, 255, 255, 1),
                                  borderRadius:
                                  BorderRadius.circular(8.r),
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Colors.grey,
                                      offset: Offset(0, 0.25),
                                      blurRadius: 0.45,
                                    )
                                  ],
                                ),
                                child: Column(
                                  children: [
                                    Padding(
                                        padding:
                                        EdgeInsets.only(top: 8.h)),
                                    Container(
                                      margin: EdgeInsets.fromLTRB(
                                          16.w, 8.h, 16.w, 8.h),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            "assets/images/img_header.png",
                                            height: 48.h,
                                            width: 48.w,
                                          ),
                                          Padding(
                                              padding: EdgeInsets.only(
                                                  right: 8.h)),
                                          Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [
                                                  Obx(
                                                        () => Text(
                                                      "Chào, ${controller.userProfile.value.fullName ?? ""}",
                                                      style: TextStyle(
                                                          fontSize: 16.sp,
                                                          color: const Color
                                                              .fromRGBO(
                                                              26,
                                                              26,
                                                              26,
                                                              1)),
                                                    ),
                                                  ),
                                                  InkWell(
                                                      onTap: () {
                                                        Get.bottomSheet(

                                                            StatefulBuilder(
                                                                builder:
                                                                    (context,
                                                                    state) {
                                                                  return Wrap(
                                                                    children: [
                                                                      Container(
                                                                        decoration:
                                                                        BoxDecoration(
                                                                          borderRadius: BorderRadius.only(topRight: Radius.circular(32.r), topLeft: Radius.circular(32.r)),
                                                                          color: Colors.white,
                                                                        ),
                                                                        child:
                                                                        Column(
                                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                                          children: [
                                                                            Container(
                                                                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(6.r),
                                                                                  color: const Color.fromRGBO(210, 212, 216, 1)),
                                                                              height: 6.h,
                                                                              width: 48.w,
                                                                              alignment: Alignment.topCenter,
                                                                              margin: EdgeInsets.only(top: 16.h),
                                                                            ),
                                                                            Padding(
                                                                                padding:
                                                                                EdgeInsets.only(top: 12.h)),
                                                                            Text(
                                                                              "Năm Học",
                                                                              style: TextStyle(
                                                                                  fontSize: 16.sp,
                                                                                  color: Colors.black,
                                                                                  fontWeight: FontWeight.w500),
                                                                            ),
                                                                            SingleChildScrollView(
                                                                              child: Container(
                                                                                height: 300.w,
                                                                                margin: EdgeInsets.symmetric(vertical: 16.h, horizontal: 8.w),
                                                                                child: ListView.builder(
                                                                                    itemCount: controller.schoolYears.length,
                                                                                    shrinkWrap: true,
                                                                                    itemBuilder: (context, index) {
                                                                                      controller.classInSchoolYears.value = controller.schoolYears[index].clazz!;
                                                                                      return InkWell(
                                                                                        child: Container(
                                                                                          margin: EdgeInsets.symmetric(vertical: 4.h),
                                                                                          color: controller.clickSchoolYear[index] == true ? const Color.fromRGBO(254, 230, 211, 1) : Colors.white,
                                                                                          child: Row(
                                                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                                            children: [
                                                                                              SizedBox(
                                                                                                  height: 32.h,
                                                                                                  width: 32.h,
                                                                                                  child: CacheNetWorkCustom(
                                                                                                    urlImage: '${controller.schoolYears[index].image}',
                                                                                                  )),
                                                                                              Padding(padding: EdgeInsets.only(left: 8.w)),
                                                                                              Expanded(child: Column(
                                                                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                children: [
                                                                                                  RichText(
                                                                                                    text: TextSpan(children: [
                                                                                                      TextSpan(text: 'Học Sinh: ', style: TextStyle(color: ColorUtils.PRIMARY_COLOR, fontFamily: 'static/Inter-Regular.ttf', fontSize: 14.sp)),
                                                                                                      TextSpan(text: controller.schoolYears[index].fullName!, style: TextStyle(color: Colors.black, fontFamily: 'static/Inter-Regular.ttf', fontSize: 14.sp)),
                                                                                                    ]),
                                                                                                  ),
                                                                                                  Row(
                                                                                                    children: [
                                                                                                      Expanded(child: RichText(
                                                                                                        textAlign: TextAlign.start,
                                                                                                        text: TextSpan(
                                                                                                          text: 'Lớp: ',
                                                                                                          style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1), fontSize: 14.sp, fontWeight: FontWeight.w400, fontFamily: 'static/Inter-Medium.ttf'),
                                                                                                          children: controller.classInSchoolYears.map((e) {
                                                                                                            var index = controller.classInSchoolYears.indexOf(e);
                                                                                                            var showSplit = ", ";
                                                                                                            if (index == (controller.classInSchoolYears.length) - 1) {
                                                                                                              showSplit = "";
                                                                                                            }
                                                                                                            return TextSpan(text: "${e.name}$showSplit", style: TextStyle(color: ColorUtils.PRIMARY_COLOR, fontSize: 14.sp, fontWeight: FontWeight.w500, fontFamily: 'static/Inter-Medium.ttf'));
                                                                                                          }).toList(),
                                                                                                        ),
                                                                                                      )),
                                                                                                      SizedBox(width: 8.w,),
                                                                                                      Row(
                                                                                                        children: [
                                                                                                          Text(
                                                                                                            "${controller.schoolYears[index].fromYear}",
                                                                                                            style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontWeight: FontWeight.w400, fontSize: 14.sp),
                                                                                                          ),
                                                                                                          Text(
                                                                                                            "-",
                                                                                                            style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontWeight: FontWeight.w400, fontSize: 14.sp),
                                                                                                          ),
                                                                                                          Text(
                                                                                                            "${controller.schoolYears[index].toYear}",
                                                                                                            style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1), fontWeight: FontWeight.w400, fontSize: 14.sp),
                                                                                                          ),
                                                                                                        ],
                                                                                                      ),
                                                                                                    ],
                                                                                                  ),
                                                                                                ],
                                                                                              )),

                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        onTap: () {
                                                                                          updated(state, index);
                                                                                          controller.onSelectSchoolYears(index);
                                                                                        },
                                                                                      );
                                                                                    }),
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      )
                                                                    ],
                                                                  );
                                                                }));
                                                      },
                                                      child: Row(
                                                        children: [
                                                          RichText(
                                                            textAlign:
                                                            TextAlign
                                                                .start,
                                                            text: TextSpan(
                                                              text: 'Lớp: ',
                                                              style: TextStyle(
                                                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                                                  fontSize: 14.sp,
                                                                  fontWeight:
                                                                  FontWeight.w400,
                                                                  fontFamily: 'static/Inter-Medium.ttf'),
                                                              children:
                                                              controller.clazzs.map((e) {
                                                                    var index = controller.clazzs.indexOf(e);
                                                                    var showSplit = ", ";
                                                                    if (index == (controller.clazzs.length) - 1) {showSplit = "";
                                                                    }
                                                                    return TextSpan(text: "${e.name}$showSplit",
                                                                        style: TextStyle(color: ColorUtils.PRIMARY_COLOR,
                                                                            fontSize: 14.sp,
                                                                            fontWeight: FontWeight.w500,
                                                                            fontFamily: 'static/Inter-Medium.ttf'));
                                                                  }).toList(),
                                                            ),
                                                          ),
                                                          Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                  left:
                                                                  8.w)),
                                                          const Icon(
                                                            Icons
                                                                .keyboard_arrow_down_outlined,
                                                            size: 18,
                                                            color: Colors
                                                                .black,
                                                          )
                                                        ],
                                                      )),
                                                ],
                                              ))
                                        ],
                                      ),
                                    ),
                                    Padding(
                                        padding:
                                        EdgeInsets.only(top: 16.h)),
                                    action(),
                                    Padding(
                                        padding: EdgeInsets.only(
                                            bottom: 16.h)),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        Padding(padding: EdgeInsets.only(top: 8.h)),
                        Visibility(
                          visible: checkVisiblePage(
                              Get.find<HomeController>().userGroupByApp,
                              StringConstant.PAGE_SUBJECT),
                          child: Column(
                            children: [
                              Obx(() => Container(
                                margin: EdgeInsets.fromLTRB(
                                    16.w, 20.h, 16.w, 6.h),
                                child: Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Text("Môn Học",
                                            style: TextStyle(
                                                fontSize: 20.sp,
                                                color: const Color
                                                    .fromRGBO(
                                                    26,
                                                    26,
                                                    26,
                                                    1))),
                                        Padding(
                                            padding:
                                            EdgeInsets.only(
                                                top: 6.h)),
                                        Visibility(
                                          visible: controller
                                              .schoolYears
                                              .isNotEmpty,
                                          child: Text(
                                            "Năm ${controller.setTextSchoolYears()} bạn có ${controller.subject.length} môn học",
                                            style: TextStyle(
                                                fontSize: 12.sp,
                                                color: const Color
                                                    .fromRGBO(
                                                    133,
                                                    133,
                                                    133,
                                                    1)),
                                          ),
                                        )
                                      ],
                                    ),
                                    Flexible(child: Container()),
                                    Visibility(
                                      visible: controller.subject.isNotEmpty,
                                        child: InkWell(
                                      onTap: () {
                                        checkClickPage(
                                            Get.find<
                                                HomeController>()
                                                .userGroupByApp,
                                                () {
                                              controller
                                                  .toSubjectPage();
                                            },
                                            StringConstant
                                                .PAGE_LIST_SUBJECT);
                                      },
                                      child: Container(
                                        alignment:
                                        AlignmentDirectional
                                            .topEnd,
                                        width: 90.h,
                                        height: 30.h,
                                        child: Text(
                                          "Xem Tất cả",
                                          style: TextStyle(
                                              fontSize: 12.sp,
                                              color: ColorUtils
                                                  .PRIMARY_COLOR),
                                        ),
                                      ),
                                    ))
                                  ],
                                ),
                              )),
                              scheduleSubject(),
                            ],
                          ),
                        ),
                        GetBuilder<HomeController>(
                            builder: (homeController) {
                              return Visibility(
                                visible: checkVisibleFeature(
                                    Get.find<HomeController>()
                                        .userGroupByApp,
                                    StringConstant.FEATURE_LESSON_LIST),
                                child: scheduleTable(),
                              );
                            }),
                        GetBuilder<HomeController>(
                            builder: (homeController) {
                              return Visibility(
                                visible: checkVisibleFeature(
                                    homeController.userGroupByApp,
                                    StringConstant.FEATURE_NEWS_LIST),
                                child: Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 16.w),
                                  alignment: Alignment.centerLeft,
                                  child: Text("Tin Tức - Sự  Kiện",
                                      style: TextStyle(
                                          fontSize: 18.sp,
                                          color: const Color.fromRGBO(
                                              26, 26, 26, 1))),
                                ),
                              );
                            }),
                        GetBuilder<HomeController>(
                            builder: (homeController) {
                              return Visibility(
                                  visible: checkVisibleFeature(
                                      homeController.userGroupByApp,
                                      StringConstant.FEATURE_NEWS_LIST),
                                  child: const EventBody(isInHome: true));
                            }),
                      ],
                    ),
                  )
                ],
              ),
            ),
            onRefresh: () async {
              await controller.onRefresh();
              await Get.find<EventNewsController>().onRefresh();
            })
            : const LoadingCustom(),
      ))),
    );
  }

  buildIndicatorBanner() {
    return SizedBox(
      height: 6.h,
      child: ListView.builder(
          itemCount: controller.itemBanner.length,
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Obx(() => controller.indexSwiper.value == index
                ? Container(
                    width: 12.w,
                    height: 10.w,
                    margin: EdgeInsets.only(left: 4.w, right: 4.w),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(6)),
                  )
                : SizedBox(
                    width: 10.w,
                    height: 10.w,
                    child: const CircleAvatar(
                      backgroundColor: Color.fromRGBO(252, 252, 254, 0.5),
                    ),
                  ));
          }),
    );
  }

  buildBanner() {
    return Obx(() => SizedBox(
          height: 200.h,
          child: Swiper(
            controller: controller.controllerSwiperStudent,
            autoplayDelay: 5000,
            autoplay: true,
            onIndexChanged: (value) {
              controller.indexSwiper.value = value;
              // controller.itemBanner[index].selected= value;
            },
            itemCount: controller.itemBanner.length,
            itemBuilder: (context, index) {
              return CacheNetWorkCustomBanner2(
                  urlImage: "${controller.itemBanner[index].url}");
            },
          ),
        )
    );
  }

  scheduleTable() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(16.w, 16.h, 16.w, 6.h),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Thời khóa biểu",
                      style: TextStyle(
                          fontSize: 20.sp,
                          color: const Color.fromRGBO(26, 26, 26, 1))),
                  const Padding(padding: EdgeInsets.only(top: 6)),
                  Visibility(
                      visible: controller.isSubjectToday.value,
                      child: SizedBox(
                        child: Text(
                          "Hôm nay bạn có ${controller.scheduleToday.length} tiết học",
                          style: TextStyle(
                              fontSize: 12.sp,
                              color: const Color.fromRGBO(133, 133, 133, 1)),
                        ),
                      )),
                ],
              )),
              InkWell(
                onTap: () {
                  Get.find<HomeController>().comeCalendar();
                },
                child: Container(
                  alignment: AlignmentDirectional.topEnd,
                  width: 90.h,
                  height: 30.h,
                  child: Text(
                    "Xem Tất cả",
                    style: TextStyle(
                      fontSize: 12.sp,
                      color: ColorUtils.PRIMARY_COLOR,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 8.h)),
        Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
          margin: const EdgeInsets.only(left: 16, right: 16, bottom: 20),
          child: Obx(() => SfCalendar(
                view: CalendarView.timelineDay,
                firstDayOfWeek: 1,
                allowViewNavigation: false,
                dataSource: MeetingDataSource(controller.timeTable),
                todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),
                appointmentBuilder: appointmentBuilderTimeLineDay,
                selectionDecoration: BoxDecoration(
                  border: Border.all(
                      color: const Color.fromRGBO(249, 154, 81, 1),
                      width: 2),
                ),
                timeSlotViewSettings: TimeSlotViewSettings(
                  timeFormat: "H a",
                  timeIntervalWidth: 100.w,
                  timelineAppointmentHeight: 50,
                ),
                controller: controller.calendarController.value,
                scheduleViewSettings: const ScheduleViewSettings(
                    appointmentItemHeight: 20, hideEmptyScheduleWeek: true),
              )),
        ),
      ],
    );
  }

  scheduleSubject() {
    return Obx(() => GridView.builder(
        padding: const EdgeInsets.all(10),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: (Device.get().isPhone)
              ? 3
              : controller.isLandScape.value
                  ? 6
                  : 4,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
        ),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        physics: const ClampingScrollPhysics(),
        itemCount:
            (controller.subject.length > 6) ? 6 : controller.subject.length,
        itemBuilder: (context, index) {
          return SizedBox(
            height: 96.h,
            child: Card(
              elevation: 0.2,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              child: InkWell(
                onTap: () {
                  checkClickPage(
                      Get.find<HomeController>().userGroupByApp,
                      () => controller.getDetailSubjectRole(
                          AppCache().userType, index),
                      StringConstant.PAGE_SUBJECT_DETAIL);
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                        height: 60.w,
                        width: 60.w,
                        child: CacheNetWorkCustomBanner(
                          urlImage:
                              '${controller.subject[index].subjectCategory?.image}',
                        )),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    Text(
                      textAlign: TextAlign.center,
                      "${controller.subject[index].subjectCategory?.name}",
                      style: TextStyle(
                          color: const Color.fromRGBO(26, 26, 26, 1),
                          fontSize: 12.sp),
                    )
                  ],
                ),
              ),
            ),
          );
        }));
  }

  Row action() {
    return Row(
      children: [
        const Padding(padding: EdgeInsets.only(left: 16)),
        Visibility(
          visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp,
              StringConstant.PAGE_DILIGENCE),
          child: Expanded(
            flex: 3,
            child: InkWell(
              onTap: () {
                //To Diem danh
                controller.goToDiligenceStudentPage();
              },
              child: Column(
                children: [
                  Image.asset("assets/images/icon_diligence_std.png",
                      width: 24.w, height: 24.h),
                  const Padding(padding: EdgeInsets.only(top: 11)),
                  Text(
                    "Chuyên cần",
                    style: TextStyle(
                        color: const Color.fromRGBO(26, 26, 26, 1),
                        fontSize: 12.sp),
                  )
                ],
              ),
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: InkWell(
            onTap: () {
              checkClickFeature(Get.find<HomeController>().userGroupByApp, () {
                Get.toNamed(Routes.transcriptsSyntheticStudentPage);
              }, StringConstant.FEATURE_TRANSCRIPT_SYNTHETIC_LIST);
            },
            child: Column(
              children: [
                Image.asset("assets/images/icon_bangdiem_std.png",
                    width: 24.w, height: 24.h),
                const Padding(padding: EdgeInsets.only(top: 11)),
                Text(
                  "Bảng điểm",
                  style: TextStyle(
                      color: const Color.fromRGBO(26, 26, 26, 1),
                      fontSize: 12.sp),
                )
              ],
            ),
          ),
        ),
        Expanded(
            flex: 3,
            child: InkWell(
              onTap: () {
                //To Person Info
                Get.to(() => PersonalInformationStudentPage());
              },
              child: Column(
                children: [
                  Image.asset(
                    "assets/images/icon_profile.png",
                    width: 24.w,
                    height: 24.h,
                  ),
                  const Padding(padding: EdgeInsets.only(top: 11)),
                  Text(
                    "Cá nhân",
                    style: TextStyle(
                        color: const Color.fromRGBO(26, 26, 26, 1),
                        fontSize: 12.sp),
                  )
                ],
              ),
            )),
        const Padding(padding: EdgeInsets.only(right: 16)),
      ],
    );
  }

  Future<void> updated(StateSetter updateState, int index) async {
    updateState(() {
      controller.onClickShoolYears(index);
    });
  }
}
