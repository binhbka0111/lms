import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/student_home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/subjects/subjects_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class SubjectPage extends GetWidget<SubjectController> {
  final controller = Get.put(SubjectController());

  SubjectPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return OrientationBuilder(
        builder: (BuildContext context, Orientation orientation) {
      bool isLandScape = false;
      if (Device
          .get()
          .isTablet) {
        if (orientation == Orientation.portrait) {
          isLandScape = false;
        } else {
          isLandScape = true;
        }
      } else {
        isLandScape = false;
      }
      return
        WillPopScope(child:
        Scaffold(
          backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
          appBar: AppBar(
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            title: Text(
              'Môn Học',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body:
          GridView.builder(
              padding: const EdgeInsets.all(10),
              gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: !Device.get().isTablet ? 3 : (isLandScape) ? 6 : 4,
                crossAxisSpacing: 5, //khoảng cách theo chiều dọc
                mainAxisSpacing: 5, //khoảng cách teho chiều ngang
              ),
              scrollDirection: Axis.vertical,
              shrinkWrap: true, //chiều cuộn
              physics: const ClampingScrollPhysics(),
              itemCount: controller.items.length,
              itemBuilder: (context, index) {
                return
                  InkWell(
                    onTap: (){
                      checkClickPage(Get.find<HomeController>().userGroupByApp, () =>  controller.getDetailSubjectRole(AppCache().userType,index), StringConstant.PAGE_SUBJECT_DETAIL);
                    },
                    child:  SizedBox(
                      height: 100,
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                                height: 50.h,
                                width: 50.h,
                                child:

                                CacheNetWorkCustomBanner(urlImage: '${controller.items[index].subjectCategory?.image}',)

                            ),
                            const Padding(padding: EdgeInsets.only(top: 8)),
                            Container(
                              height: 30.sp,
                              margin: const EdgeInsets.symmetric(horizontal: 8),
                              child: Text(
                                textAlign: TextAlign.center,
                                "${controller.items[index].subjectCategory?.name}",
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
              }),
        ), onWillPop:()  async {
          if(AppCache().userType=="STUDENT"){
            Get.find<StudentHomeController>().subjectId.value ="";
            print('=========listsubject${ Get.find<StudentHomeController>().subjectId.value }');
            Get.back();
          }else{
            Get.find<ParentHomeController>().subjectId.value ="";
            print('=========listsubject${ Get.find<ParentHomeController>().subjectId.value }');
            Get.back();
          }
          return true;
        },);
        }
      );
  }
}
