import 'package:get/get.dart';
import 'package:slova_lms/data/model/common/subject.dart';
import 'package:slova_lms/routes/app_pages.dart';
class SubjectController extends GetxController{
  var items = <SubjectRes>[];
  var idSubject = "".obs;
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    var detailItem = Get.arguments;
    if (detailItem != null && detailItem.isNotEmpty) {
      items = detailItem;
    }
  }

  getDetailSubjectRole(role,index){
    idSubject.value= items[index].id!;
    if(role == "STUDENT"){
      Get.toNamed(Routes.detailSubjectStudentPage);
    }else{
      Get.toNamed(Routes.detailSubjectParentPage);
    }

  }


}