import 'dart:ui';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/data/model/common/subject.dart';
import 'package:slova_lms/data/model/common/user_profile.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/event_news/event_news_controller.dart';
import 'package:slova_lms/view/mobile/schedule/schedule_controller.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../data/model/common/banner.dart';
import '../../../../data/model/common/schedule.dart';
import '../../../../data/model/common/school_year.dart';
import '../../../../data/repository/common/common_repo.dart';
import '../../../../data/repository/person/personal_info_repo.dart';
import '../../../../data/repository/schedule/schedule_repo.dart';
import '../../../../data/repository/school_year/school_year_repo.dart';
import '../../../../data/repository/subject/class_office_repo.dart';
import '../../home/home_controller.dart';
import '../../phonebook/phone_book_controller.dart';
import 'package:card_swiper/card_swiper.dart';


class StudentHomeController extends GetxController {
  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();
  final CommonRepo _commonRepo = CommonRepo();
  final ClassOfficeRepo _subjectRepo = ClassOfficeRepo();
  final ScheduleRepo _scheduleRepo = ScheduleRepo();
  RxList<SubjectRes> subject = <SubjectRes>[].obs;
  var userProfile = UserProfile().obs;
  RxList<Clazzs> clazzs = <Clazzs>[].obs;
  RxList<Clazzs> classInSchoolYears = <Clazzs>[].obs;
  var banner = BannerHomePage().obs;
  RxList<ItemBanner> itemBanner = <ItemBanner>[].obs;
  RxInt indexPageBanner = 0.obs;
  RxList<Schedule> scheduleStudent = <Schedule>[].obs;
  RxList<Appointment> timeTable = <Appointment>[].obs;
  final SchoolYearRepo _schoolYearRepo = SchoolYearRepo();
  var clickSchoolYear = <bool>[].obs;
  RxList<SchoolYear> schoolYears = <SchoolYear>[].obs;
  var currentStudentProfile = UserProfile().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  RxList<Schedule> scheduleToday = <Schedule>[].obs;
  var calendarController = CalendarController().obs;
  var isSubjectToday = true.obs;
  var fromYearPresent = (DateTime.now().year - 1).obs;
  var toYearPresent = (DateTime.now().year).obs;
  RxBool isLandScape = false.obs;
  var subjectId = "".obs;
  var isReady = false.obs;
  var indexSwiper = 0.obs;
  SwiperController controllerSwiperStudent = SwiperController();

  @override
  void onInit() {
    super.onInit();
    getSchoolYearByUser();
    getDetailProfile();
    getBanner();
  }

  getSchoolYearByUser() async {
    await _schoolYearRepo.getListSchoolYearByUser().then((value) async{
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        clazzs.value = schoolYears[0].clazz!;
        for (int i = 0; i < schoolYears.length; i++) {
          clickSchoolYear.add(false);
        }
        await getListSubject();
        if (schoolYears.isNotEmpty) {
          fromYearPresent.value = schoolYears[0].fromYear!;
          toYearPresent.value = schoolYears[0].toYear!;
          if(DateTime.now().month<=12&&DateTime.now().month>9){
            calendarController.value.displayDate = DateTime(fromYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
          }else{
            calendarController.value.displayDate = DateTime(toYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
          }
        }
      }
    });
    await Get.find<HomeController>().getCountNotifyNotSeen();
  }

  setTextSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (fromYearPresent.value == DateTime.now().year) {
        return "nay";
      } else {
        return "${fromYearPresent.value} - ${toYearPresent.value}";
      }
    } else {
      if (toYearPresent.value == DateTime.now().year) {
        return "nay";
      } else {
        return "${fromYearPresent.value} - ${toYearPresent.value}";
      }
    }
  }

  onClickShoolYears(index) {
    clickSchoolYear.value = <bool>[].obs;
    for (int i = 0; i < schoolYears.length; i++) {
      clickSchoolYear.add(false);
    }
    clickSchoolYear[index] = true;
  }

  getScheduleTimeTableStudent(userId) async{
    scheduleStudent.value = [];
    scheduleToday.value = [];
    await Get.find<ScheduleController>().queryTimeTableByStudent(userId);
    await _scheduleRepo.getScheduleStudent(userId).then((value) {
      if (value.state == Status.SUCCESS) {
        scheduleStudent.value = value.object!;
        getAppointments();
        scheduleToday.value = scheduleStudent
            .where((element) =>
        outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(element.startTime!)) == outputDateFormat.format(DateTime.now()))
            .toList();
      }
    });
    scheduleStudent.refresh();
    scheduleToday.refresh();
  }

  getAppointments() {
    timeTable.value = <Appointment>[].obs;
    for (int i = 0; i < scheduleStudent.length; i++) {
      timeTable.add(Appointment(
          startTime: DateTime.fromMillisecondsSinceEpoch(
              scheduleStudent[i].startTime!),
          endTime: DateTime.fromMillisecondsSinceEpoch(
              scheduleStudent[i].endTime!),
          subject: scheduleStudent[i].subject!.subjectCategory!.name!,
          notes: scheduleStudent[i].subject?.clazz?.classCategory?.name!
              .toString(), // tên lớp
          resourceIds: scheduleStudent[i].subject!.clazz!.students,
          recurrenceId:
              scheduleStudent[i].subject!.teacher?.fullName!.toString(),
          color: const Color.fromRGBO(249, 154, 81, 1)));
    }

    timeTable.refresh();
  }

  selectPageBanner(int index) {
    indexPageBanner.value = index;
    for (var element in itemBanner) {
      element.selected = false;
    }
    itemBanner[index].selected = true;
    if (index != 0) {
      indexPageBanner.value - 1;
    } else {
      indexPageBanner.value = 0;
    }
    itemBanner.refresh();
  }

  getBanner() async {
    itemBanner.value = [];
    await _commonRepo.getBanner().then((value) {
      if (value.state == Status.SUCCESS) {
        banner.value = value.object!;
        itemBanner.value = banner.value.items!;
        itemBanner[0].selected = true;
        itemBanner.refresh();
      }
    });
  }

  getDetailProfile() async{
    await _personalInfoRepo.detailUserProfile().then((value) async{
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        Get.put(EventNewsController());
        userProfile.value.school!.id!;
        Get.find<EventNewsController>().setSchoolId(userProfile.value.school!.id!);
        Get.find<EventNewsController>().onRefresh();
        AppCache().userId = userProfile.value.id ?? "";
        await getScheduleTimeTableStudent(userProfile.value.id);
      }
    });
    isReady.value = true;

  }

  onRefresh() {
    getBanner();
    _schoolYearRepo.getListSchoolYearByUser().then((value) {
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        for (int i = 0; i < schoolYears.length; i++) {
          clickSchoolYear.add(false);
        }
      }
    });
    getListSubject();
    getScheduleTimeTableStudent(userProfile.value.id);
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      calendarController.value.displayDate = DateTime(fromYearPresent.value,
          DateTime.now().month, DateTime.now().day, DateTime.now().hour - 1);
    } else {
      calendarController.value.displayDate = DateTime(toYearPresent.value,
          DateTime.now().month, DateTime.now().day, DateTime.now().hour - 1);
    }
  }

  setTimeCalendar(index) {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      calendarController.value.displayDate = DateTime(
          schoolYears[index].fromYear!.toInt(),
          DateTime.now().month,
          DateTime.now().day,
          DateTime.now().hour - 1);
    } else {
      calendarController.value.displayDate = DateTime(
          schoolYears[index].toYear!.toInt(),
          DateTime.now().month,
          DateTime.now().day,
          DateTime.now().hour - 1);
    }
  }

  getListSubject() {
    subject.clear();
    _subjectRepo.listSubject(clazzs[0].id).then((value) {
      if (value.state == Status.SUCCESS) {
        subject.value = value.object!;
      }
    });
    subject.refresh();
  }

  onSelectSchoolYears(index) {
    clazzs.value = schoolYears[index].clazz!;
    AppCache().setSchoolYearId(schoolYears[index].id ?? "");
    fromYearPresent.value = schoolYears[index].fromYear!;
    toYearPresent.value = schoolYears[index].toYear!;
    Get.find<PhoneBookController>().getListClassByStudent();
    // Get.find<StatisticalStudentController>().getListDiligenceToMonth();
    onSelectUser();
    setTimeCalendar(index);
    Get.find<ScheduleController>()
        .queryTimeCalendar(AppCache().userType);

    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (schoolYears[index].fromYear == DateTime.now().year) {
        isSubjectToday.value = true;
      } else {
        isSubjectToday.value = false;
      }
    } else {
      if (schoolYears[index].toYear == DateTime.now().year) {
        isSubjectToday.value = true;
      } else {
        isSubjectToday.value = false;
      }
    }
    Get.back();
  }

  onSelectUser() {
    timeTable.clear();
    Get.find<ScheduleController>().timeTable.clear();
    getListSubject();
    getScheduleTimeTableStudent(userProfile.value.id);
    timeTable.refresh();
    Get.find<ScheduleController>().timeTable.refresh();
  }

  toSubjectPage() {
    Get.toNamed(Routes.subject, arguments: subject);
  }


  void toDetailProfile() {
    Get.toNamed(Routes.personalInformation, preventDuplicates: false);
  }

  void goToDiligenceStudentPage() {
    Get.toNamed(Routes.diligenceStudent);
  }

  void selectDate(index) {
    fromYearPresent.value = schoolYears[index].fromYear!;
    toYearPresent.value = schoolYears[index].toYear!;
  }

  getDetailSubjectRole(role,index){
    subjectId.value  = subject[index].id!;
    if(role == "STUDENT"){
      Get.toNamed(Routes.detailSubjectStudentPage );
    }else{
      Get.toNamed(Routes.detailSubjectParentPage);
    }

  }
}
