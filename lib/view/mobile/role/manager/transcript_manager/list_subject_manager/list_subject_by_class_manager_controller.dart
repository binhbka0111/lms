import 'package:get/get.dart';
import 'package:slova_lms/data/model/common/subject.dart';
import 'package:slova_lms/data/repository/subject/class_office_repo.dart';
import '../../../../../../../../../data/base_service/api_response.dart';
import '../list_block_transcript/list_class_transcript/class_transcript/class_transcript_controller.dart';


class ListSubjectByClassManagerController extends GetxController{
  RxList<SubjectRes> subjects = <SubjectRes>[].obs;
  final ClassOfficeRepo _subjectRepo = ClassOfficeRepo();
  @override
  void onInit() {
    getListSubject();
    super.onInit();
  }


  getListSubject() {
    subjects.clear();
    var classID = Get.find<ClassTranscriptManagerController>().classes.value.id;
    _subjectRepo.listSubject(classID).then((value) {
      if (value.state == Status.SUCCESS) {
        subjects.value = value.object!;
      }
    });
    subjects.refresh();
  }





}