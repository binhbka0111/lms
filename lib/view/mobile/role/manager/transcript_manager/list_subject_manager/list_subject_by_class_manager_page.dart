import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import '../../../../../../../../../routes/app_pages.dart';
import 'list_subject_by_class_manager_controller.dart';

class ListSubjectByClassManagerPage extends GetWidget<ListSubjectByClassManagerController> {
  @override
  final controller = Get.put(ListSubjectByClassManagerController());

  ListSubjectByClassManagerPage({super.key});


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
      OrientationBuilder(
          builder: (BuildContext context, Orientation orientation) {
            bool isLandScape = false;
            if (Device
                .get()
                .isTablet) {
              if (orientation == Orientation.portrait) {
                isLandScape = false;
              } else {
                isLandScape = true;
              }
            } else {
              isLandScape = false;
            }

            return
              Scaffold(
                backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
                appBar: AppBar(
                  backgroundColor: ColorUtils.PRIMARY_COLOR,
                  title: Text(
                    'Môn Học',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.sp,
                        fontFamily: 'static/Inter-Medium.ttf'),
                  ),
                ),
                body:
                SingleChildScrollView(
                  physics: const ScrollPhysics(),
                  child: Obx(() =>
                      Visibility(
                        //  visible: checkVisibleFeatureInPageHome(StringConstant.FEATURE_LIST_SUBJECT),
                          child: Container(
                            margin: EdgeInsets.only(top: 16.h),
                            child: Column(
                              children: [

                                GridView.builder(
                                    padding: const EdgeInsets.all(10),
                                    gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: !Device
                                          .get()
                                          .isTablet ? 3 : (isLandScape) ? 6 : 4,
                                      crossAxisSpacing: 5,
                                      //khoảng cách theo chiều dọc
                                      mainAxisSpacing: 5, //khoảng cách teho chiều ngang
                                    ),
                                    scrollDirection: Axis.vertical,
                                    shrinkWrap: true,
                                    //chiều cuộn
                                    physics: const ClampingScrollPhysics(),
                                    itemCount: controller.subjects.length,
                                    itemBuilder: (context, index) {
                                      return
                                        InkWell(
                                          onTap: () {
                                            Get.toNamed(Routes
                                                .viewTranscriptSubjectManagerController,
                                                arguments: controller
                                                    .subjects[index]);
                                          },
                                          child: SizedBox(
                                            height: 100,
                                            child: Card(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius
                                                      .circular(8)),
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment
                                                    .center,
                                                children: [
                                                  SizedBox(
                                                    height: 50.h,
                                                    width: 50.h,
                                                    child:
                                                    CacheNetWorkCustomBanner(
                                                        urlImage: '${controller
                                                            .subjects[index]
                                                            .subjectCategory
                                                            ?.image}'),

                                                  ),
                                                  const Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 8)),
                                                  Container(
                                                    margin: const EdgeInsets
                                                        .symmetric(
                                                        horizontal: 8),
                                                    height: 30.sp,
                                                    child: Text(
                                                      textAlign: TextAlign
                                                          .center,
                                                      "${controller
                                                          .subjects[index]
                                                          .subjectCategory
                                                          ?.name}",
                                                      style: TextStyle(
                                                          color: Color.fromRGBO(
                                                              26, 26, 26, 1),
                                                          fontSize: 12.sp),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                    })
                              ],
                            ),
                          ))),
                ),
              );
          });
  }
}
