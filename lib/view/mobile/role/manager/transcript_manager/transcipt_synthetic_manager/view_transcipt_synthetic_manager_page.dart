import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/constants/date_format.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/date_time_picker.dart';
import 'package:slova_lms/commom/utils/time_utils.dart';
import 'package:slova_lms/data/model/res/transcript/transcript.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/manager/transcript_manager/transcipt_synthetic_manager/view_transcipt_synthetic_manager_controller.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';

import '../../../../../../commom/constants/string_constant.dart';




class ViewTranscriptsSyntheticManagerPage extends GetWidget<ViewTranscriptsSyntheticManagerController>{
  @override
  final controller = Get.put(ViewTranscriptsSyntheticManagerController());

  ViewTranscriptsSyntheticManagerPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              comeToHome();
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Text(
          'Danh sách bảng điểm',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() => Visibility(
          // visible: checkVisibleFeaturePageHome1(StringConstant.FEATURE_LIST_TRANSCRIPT_GENERAL),
          child: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16.w),
          child: Column(
            children: [
              Container(
                height: 60.h,
                padding: EdgeInsets.symmetric(vertical: 14.h),
                color: Colors.white,
                child: Row(
                  children: [
                    Expanded(
                        child: TextFormField(
                          style: TextStyle(
                            fontSize: 12.0.sp,
                            color: const Color.fromRGBO(26, 26, 26, 1),
                          ),
                          onTap: () {
                            selectDateTimeStart(
                                controller.controllerDateStart.value.text,
                                DateTimeFormat.formatDateShort,
                                context);
                          },
                          cursorColor:
                          ColorUtils.PRIMARY_COLOR,
                          controller: controller.controllerDateStart.value,
                          readOnly: true,
                          decoration: InputDecoration(
                            suffixIcon: Container(
                              margin: EdgeInsets.zero,
                              child: SvgPicture.asset(
                                "assets/images/icon_date_picker.svg",
                                fit: BoxFit.scaleDown,
                              ),
                            ),
                            labelText: "Từ ngày",
                            border: const OutlineInputBorder(),
                            labelStyle: TextStyle(
                                color:
                                const Color.fromRGBO(177, 177, 177, 1),
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w500,
                                fontFamily:
                                'assets/font/static/Inter-Medium.ttf'),
                          ),
                        )),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 16.w),
                        child: TextFormField(
                          style: TextStyle(
                            fontSize: 12.0.sp,
                            color: const Color.fromRGBO(26, 26, 26, 1),
                          ),
                          onTap: () {
                            selectDateTimeEnd(
                                controller.controllerDateEnd.value.text,
                                DateTimeFormat.formatDateShort,
                                context);
                          },
                          cursorColor:
                          ColorUtils.PRIMARY_COLOR,
                          controller:
                          controller.controllerDateEnd.value,
                          readOnly: true,
                          decoration: InputDecoration(
                            suffixIcon: Container(
                              margin: EdgeInsets.zero,
                              child: SvgPicture.asset(
                                "assets/images/icon_date_picker.svg",
                                fit: BoxFit.scaleDown,
                              ),
                            ),
                            labelText: "Đến ngày",
                            border: const OutlineInputBorder(),
                            labelStyle: TextStyle(
                                color: const Color.fromRGBO(
                                    177, 177, 177, 1),
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w500,
                                fontFamily:
                                'assets/font/static/Inter-Medium.ttf'),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16.w),
                height: 40,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                        color: const Color.fromRGBO(192, 192, 192, 1)),
                    borderRadius: BorderRadius.circular(8)),
                child: Theme(
                  data: Theme.of(context).copyWith(
                    canvasColor: Colors.white,
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                      isExpanded: true,
                      iconSize: 0,
                      icon: const Visibility(
                          visible: false,
                          child: Icon(Icons.arrow_downward)),
                      elevation: 16,
                      hint: controller.semesterName.value != ""
                          ? Row(
                        children: [
                          Text(
                            controller.semesterName.value,
                            style: TextStyle(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                color: ColorUtils.PRIMARY_COLOR),
                          ),
                          Expanded(child: Container()),
                          const Icon(
                            Icons.keyboard_arrow_down,
                            color: Colors.black,
                            size: 18,
                          )
                        ],
                      )
                          : Row(
                        children: [
                          Text(
                            'Kỳ học',
                            style: TextStyle(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w400,
                                color: Colors.black54),
                          ),
                          Expanded(child: Container()),
                          const Icon(
                            Icons.keyboard_arrow_down,
                            color: Colors.black,
                            size: 18,
                          )
                        ],
                      ),
                      items: controller.listSemester.map(
                            (value) {
                          return DropdownMenuItem<SemesterTranscript>(
                            value: value,
                            child: Text(
                              value.name!,
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  fontWeight: FontWeight.w400,
                                  color: ColorUtils.PRIMARY_COLOR),
                            ),
                          );
                        },
                      ).toList(),
                      onChanged: (SemesterTranscript? value) {
                        controller.semesterName.value = value!.name!;
                        controller.semesterId.value = value.id!;
                        controller.getListTranscript();
                      },
                    ),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 16.h)),
              ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: controller.transcript.length,
                itemBuilder: (context, index) {
                  return Obx(() => InkWell(
                    onTap: () {
                      checkClickFeature(Get.find<HomeController>().userGroupByApp,() =>controller.clickItemListTranscript(index), StringConstant.FEATURE_TRANSCRIPT_SYNTHETIC_DETAIL);
                    },
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 120.w,
                                      child: Text("Tên: ",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                                    ),
                                    SizedBox(
                                      width: 200.w,
                                      child: Text(controller.transcript[index].name!,style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                    )
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 8.h)),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 120.w,
                                      child: Text("Kỳ học: ",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                                    ),
                                    Text(controller.transcript[index].semester!.name!,style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 8.h)),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 120.w,
                                      child: Text("Trạng thái công bố",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                                    ),
                                    Container(
                                      padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                                      decoration: BoxDecoration(
                                          color: controller.getColorBackgroundIsPublic(controller.transcript[index].isPublic),
                                          borderRadius: BorderRadius.circular(6)
                                      ),
                                      child: Text(
                                        "${controller.getStatusPublic(controller.transcript[index].isPublic)}",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            color: controller.getColorTextIsPublic(controller.transcript[index].isPublic),
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 8.h)),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 120.w,
                                      child: Text("Người tạo: ",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                                    ),
                                    Text(controller.transcript[index].createdBy!.fullName!,style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 8.h)),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 120.w,
                                      child: Text("Thời gian tạo: ",style: TextStyle(color: const Color.fromRGBO(133, 133, 133, 1),fontSize: 12.sp,fontWeight: FontWeight.w500),),
                                    ),
                                    Text(controller.outputDateFormatV2.format(
                                        DateTime.fromMillisecondsSinceEpoch(
                                            controller
                                                .transcript[index]
                                                .createdAt! )),style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 1),fontSize: 12.sp,fontWeight: FontWeight.w400),),
                                  ],
                                ),

                              ],
                            ),
                          ],
                        ),
                        const Divider(),
                      ],
                    ),
                  ));
                },)
            ],
          ),
        ),
      ))),
    );
  }


  selectDateTimeStart(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy')
                .parse(controller.controllerDateEnd.value.text)
                .millisecondsSinceEpoch) {
          controller.controllerDateStart.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian Từ ngày nhỏ hơn thời gian Đến ngày");
        }
      } else {
        controller.controllerDateStart.value.text = date;
      }
      controller.getListTranscript();
    }

  }

  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy')
            .parse(controller.controllerDateStart.value.text)
            .millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch) {
          controller.controllerDateEnd.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian Đến ngày lớn hơn thời gian Từ ngày");
        }
      } else {
        controller.controllerDateEnd.value.text = date;
      }
    }
    controller.getListTranscript();

  }
}