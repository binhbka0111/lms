import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/manager/transcript_manager/transcript_manager_controller.dart';
import '../../../../../routes/app_pages.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';


class TranscriptManagerPage
    extends GetWidget<TranscriptManagerController> {
  @override
  final controller = Get.put(TranscriptManagerController());

  TranscriptManagerPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            elevation: 0,
            title: Text(
              "Quản lý bảng điểm",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w500),
            ),
            actions: [
              InkWell(
                onTap: () {
                  comeToHome();
                },
                child: const Icon(
                  Icons.home,
                  color: Colors.white,
                ),
              ),
              Padding(padding: EdgeInsets.only(right: 16.w))
            ],
          ),
          body: Container(
            padding: const EdgeInsets.all(16),
            child: Column(
              children: [
                Visibility(
                   visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_LIST_TRANSCRIPT),
                    child: InkWell(
                  onTap: () {
                    Get.toNamed(Routes.listBlockTranscriptManagerPage);
                  },
                  child: Card(
                    elevation: 1,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)),
                    child: Container(
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        children: [
                          Text(
                            "Danh sách bảng điểm",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontWeight: FontWeight.w500),
                          ),
                          Expanded(child: Container()),
                          const Icon(
                            Icons.arrow_forward_ios,
                            color: ColorUtils.PRIMARY_COLOR,
                            size: 18,
                          )

                        ],
                      ),
                    ),
                  ),
                )),
                SizedBox(height: 8.h,),
                Visibility(
                    visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_STATISTICAL_TRANSCRIPT),
                    child: InkWell(
                  onTap: () {
                    Get.toNamed(Routes.statisticalTranscriptManagerPage);
                  },
                  child: Card(
                    elevation: 1,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)),
                    child: Container(
                      padding: const EdgeInsets.all(16),
                      child: Row(
                        children: [
                          Text(
                            "Thống kê bảng điểm",
                            style: TextStyle(
                                fontSize: 14.sp,
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontWeight: FontWeight.w500),
                          ),
                          Expanded(child: Container()),
                          const Icon(
                            Icons.arrow_forward_ios,
                            color: ColorUtils.PRIMARY_COLOR,
                            size: 18,
                          ),

                        ],
                      ),
                    ),
                  ),
                )),
              ],
            ),
          ),
        );
  }
}
