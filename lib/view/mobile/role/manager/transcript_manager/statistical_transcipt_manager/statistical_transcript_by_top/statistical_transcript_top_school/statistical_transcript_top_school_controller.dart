import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/data/model/res/school/school.dart';
import 'package:slova_lms/data/model/res/transcript/statistical_transcript.dart';
import 'package:slova_lms/data/model/res/transcript/transcript.dart';
import 'package:slova_lms/data/repository/school/school_repo.dart';
import 'package:slova_lms/data/repository/transcript_repo/transcript_repo.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/home_room_teacher.dart';
import '../../../../../../../../data/model/res/transcript/semester.dart';
import '../../../../manager_home_controller.dart';


class StatisticalTranscriptTopSchoolController extends GetxController{
  final TranscriptsRepo _transcriptsRepo = TranscriptsRepo();
  var transcript = <Transcript>[].obs;
  var semesters = <Semester>[].obs;
  var listSemester = <SemesterTranscript>[].obs;
  var controllerDateStart = TextEditingController().obs;
  var controllerDateEnd = TextEditingController().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy HH:mm');
  var semesterName = "".obs;
  var semesterId = "".obs;
  var selectedDay = DateTime.now().obs;
  var isPublic = "".obs;
  var indexPage = 1.obs;
  var numberTop = 10.obs;
  var statisticalTranscript = StatisticalTranscript().obs;
  var itemsStatisticalTranscript = <ItemsStatisticalTranscript>[].obs;
  var itemsStatisticalTranscriptLoadMore = <ItemsStatisticalTranscript>[].obs;
  var controllerLoadMore = ScrollController();
  var listHeader = ["STT","HỌ VÀ TÊN","LỚP","ĐIỂM TRUNG BÌNH"].obs;
  var listSchoolLevel = <SchoolLevel>[].obs;
  final SchoolRepo _schoolRepo = SchoolRepo();
  var schoolLevelName = "".obs;
  var schoolLevelId = "".obs;
  var school= School().obs;
  @override
  void onInit() async{
    await getListSemester();
    await getListSchoolLevel();
    await getTopSchoolTranscript(
    semesterId.value,
    schoolLevelId.value);
    controllerLoadMore = ScrollController()..addListener(_scrollListenerLoadMore);
    super.onInit();
  }

  void _scrollListenerLoadMore() {
    if(numberTop.value>10){
      if (controllerLoadMore.position.pixels == controllerLoadMore.position.maxScrollExtent) {
        if(itemsStatisticalTranscriptLoadMore.isNotEmpty){
          indexPage++;
        }
        _transcriptsRepo.getTopStudentTranscript(indexPage.value, 20, "", "", semesterId.value, numberTop.value,schoolLevelId.value).then((value) {
          if (value.state == Status.SUCCESS) {
            itemsStatisticalTranscriptLoadMore.value = [];
            statisticalTranscript.value = value.object!;
            itemsStatisticalTranscriptLoadMore.value = statisticalTranscript.value.items!;
            itemsStatisticalTranscript.addAll(itemsStatisticalTranscriptLoadMore);
            itemsStatisticalTranscript.refresh();
          }
        });
      }
    }
  }
  @override
  dispose() {
    controllerLoadMore.dispose();
    super.dispose();
  }

  getListSchoolLevel(){
    listSchoolLevel.value = [];
    listSchoolLevel.add(SchoolLevel(id: "",name: "Tất cả"));
    _schoolRepo.getListSchoolLevel(Get.find<ManagerHomeController>().userProfile.value.school?.id).then((value) {
      if (value.state == Status.SUCCESS) {
        school.value = value.object!;
        for(int i =0;i< school.value.schoolLevel!.length ;i++){
          listSchoolLevel.add(SchoolLevel(id: "",name: ""));
          listSchoolLevel[i+1].name = school.value.schoolLevel![i].name;
          listSchoolLevel[i+1].id = school.value.schoolLevel![i].id!;
        }
        schoolLevelId.value = listSchoolLevel[0].id!;
        schoolLevelName.value = listSchoolLevel[0].name!;

      }
    });

    listSchoolLevel.refresh();
  }


  getTopSchoolTranscript(semesterId,schoolLevelId){
    itemsStatisticalTranscript.value = [];
    _transcriptsRepo.getTopStudentTranscript(0, 20, "", "", semesterId, numberTop.value,schoolLevelId).then((value) {
      if (value.state == Status.SUCCESS) {
        statisticalTranscript.value = value.object!;
        itemsStatisticalTranscript.value = statisticalTranscript.value.items!;
        itemsStatisticalTranscript.refresh();
      }
    });
  }

  setWidthHeader(title){
    switch(title){
      case "STT":
        return 50.w;
      case "HỌ VÀ TÊN":
        return 150.w;
      default:
        return 120.w;
    }
  }
  setWidthBody(title){
    switch(title){
      case "order":
        return 50.w;
      case "fullName":
        return 150.w;
      default:
        return 120.w;
    }
  }




  getListSemester()async{
    listSemester.value = [];
    await _transcriptsRepo.getListSemester().then((value) {
      if (value.state == Status.SUCCESS) {
        semesters.value= value.object!;
        for(int i =0;i< semesters.length ;i++){
          listSemester.add(SemesterTranscript(id: semesters[i].id!,name: semesters[i].name!));
        }
        listSemester.refresh();
        semesterName.value = listSemester[0].name!;
        semesterId.value = listSemester[0].id!;

      }
    });


  }

}