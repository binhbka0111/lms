import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/data/model/res/class/block.dart';
import 'package:slova_lms/data/model/res/transcript/transcript.dart';
import 'package:slova_lms/view/mobile/role/manager/transcript_manager/statistical_transcipt_manager/statistical_transcript_by_top/statistical_transcript_top_class/statistical_transcript_top_class_controller.dart';
import '../../../../../../../../commom/app_cache.dart';
import '../../../../../../../../data/model/common/home_room_teacher.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';




class StatisticalTranscriptTopClassPage extends GetWidget<StatisticalTranscriptTopClassController>{
  @override
  final controller = Get.put(StatisticalTranscriptTopClassController());

  StatisticalTranscriptTopClassPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromRGBO(246, 246, 246, 1),
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                comeToHome();
              },
              icon: const Icon(Icons.home),
              color: Colors.white,
            )
          ],
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title: Text(
            'Thống kê điểm TB theo top lớp',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body: Obx(() => Container(
          margin: EdgeInsets.only(top: 16.h,left: 16.w,right: 16.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(16.h),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6.r),
                    color: Colors.white
                ),
                child: IntrinsicHeight(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                              flex: 1,
                              child: IntrinsicWidth(
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                                  margin: EdgeInsets.only(right: 4.w),
                                  height: 36.h,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          color: const Color.fromRGBO(192, 192, 192, 1)),
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                      canvasColor: Colors.white,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        isExpanded: true,
                                        iconSize: 0,
                                        icon: const Visibility(
                                            visible: false,
                                            child: Icon(Icons.arrow_downward)),
                                        elevation: 16,
                                        hint: controller.semesterName.value != ""
                                            ? Row(
                                          children: [
                                            Text(
                                              controller.semesterName.value,
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: ColorUtils.PRIMARY_COLOR),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        )
                                            : Row(
                                          children: [
                                            Text(
                                              'Học kỳ',
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.black),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        ),
                                        items: controller.listSemester.map(
                                              (value) {
                                            return DropdownMenuItem<SemesterTranscript>(
                                              value: value,
                                              child: Text(
                                                value.name!,
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: ColorUtils.PRIMARY_COLOR),
                                              ),
                                            );
                                          },
                                        ).toList(),
                                        onChanged: (SemesterTranscript? value) {
                                          controller.semesterName.value = value!.name!;
                                          controller.semesterId.value = value.id!;
                                          controller.getTopBlockTranscript(controller.semesterId.value);
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              )),
                          Expanded(
                              flex: 1,
                              child:  IntrinsicWidth(
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                                  margin: EdgeInsets.only(left: 4.w),
                                  height: 36.h,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          color: const Color.fromRGBO(192, 192, 192, 1)),
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                      canvasColor: Colors.white,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        isExpanded: true,
                                        iconSize: 0,
                                        icon: const Visibility(
                                            visible: false,
                                            child: Icon(Icons.arrow_downward)),
                                        elevation: 16,
                                        hint: controller.schoolLevelName.value != ""
                                            ? Row(
                                          children: [
                                            Text(
                                              controller.schoolLevelName.value,
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: ColorUtils.PRIMARY_COLOR),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        )
                                            : Row(
                                          children: [
                                            Text(
                                              'Cấp học',
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.black),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        ),
                                        items: controller.listSchoolLevel.map(
                                              (value) {
                                            return DropdownMenuItem<SchoolLevel>(
                                              value: value,
                                              child: Text(
                                                "${value.name}",
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: ColorUtils.PRIMARY_COLOR),
                                              ),
                                            );
                                          },
                                        ).toList(),
                                        onChanged: (SchoolLevel? value) {
                                          controller.schoolLevelName.value = value!.name!;
                                          controller.schoolLevelId.value = value.id!;
                                          controller.getListBlock(AppCache().schoolYearId,controller.schoolLevelId.value);
                                          controller.getTopBlockTranscript(controller.semesterId.value);
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ))
                        ],
                      ),
                      SizedBox(
                        height: 8.h,
                      ),
                      Row(
                        children: [
                          Expanded(
                              flex: 1,
                              child:  IntrinsicWidth(
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                                  margin: EdgeInsets.only(right: 4.w),
                                  height: 36.h,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          color: const Color.fromRGBO(192, 192, 192, 1)),
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                      canvasColor: Colors.white,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        isExpanded: true,
                                        iconSize: 0,
                                        icon: const Visibility(
                                            visible: false,
                                            child: Icon(Icons.arrow_downward)),
                                        elevation: 16,
                                        hint: controller.blockName.value != ""
                                            ? Row(
                                          children: [
                                            Text(
                                              controller.blockName.value,
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: ColorUtils.PRIMARY_COLOR),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        )
                                            : Row(
                                          children: [
                                            Text(
                                              'Khối',
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.black),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        ),
                                        items: controller.listBlock.map(
                                              (value) {
                                            return DropdownMenuItem<Block1>(
                                              value: value,
                                              child: Text(
                                                value.name!,
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: ColorUtils.PRIMARY_COLOR),
                                              ),
                                            );
                                          },
                                        ).toList(),
                                        onChanged: (Block1? value) {
                                          controller.blockName.value = value!.name!;
                                          controller.blockId.value = value.id!;
                                          controller.listClassInBlockBySchoolLevel(AppCache().schoolYearId,controller.blockId.value,controller.schoolLevelId.value);
                                          controller.getTopBlockTranscript(controller.semesterId.value);
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              )),
                          Expanded(
                              flex: 1,
                              child: IntrinsicWidth(
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                                  margin: EdgeInsets.only(left: 4.w),
                                  height: 36.h,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          color: const Color.fromRGBO(192, 192, 192, 1)),
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                      canvasColor: Colors.white,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        isExpanded: true,
                                        iconSize: 0,
                                        icon: const Visibility(
                                            visible: false,
                                            child: Icon(Icons.arrow_downward)),
                                        elevation: 16,
                                        hint: controller.className.value != ""
                                            ? Row(
                                          children: [
                                            Text(
                                              controller.className.value,
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: ColorUtils.PRIMARY_COLOR),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        )
                                            : Row(
                                          children: [
                                            Text(
                                              'Lớp',
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.black),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        ),
                                        items: controller.listClass.map(
                                              (value) {
                                            return DropdownMenuItem<Class1>(
                                              value: value,
                                              child: Text(
                                                value.name!,
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: ColorUtils.PRIMARY_COLOR),
                                              ),
                                            );
                                          },
                                        ).toList(),
                                        onChanged: (Class1? value) {
                                          controller.className.value = value!.name!;
                                          controller.classId.value = value.id!;
                                          controller.getTopBlockTranscript(controller.semesterId.value);
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              )),

                        ],
                      ),
                      SizedBox(
                        height: 8.h,
                      ),
                      Row(
                        children: [
                          Expanded(
                              flex: 1,
                              child:  IntrinsicWidth(
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                                  margin: EdgeInsets.only(right: 4.w),
                                  height: 36.h,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          color: const Color.fromRGBO(192, 192, 192, 1)),
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Theme(
                                    data: Theme.of(context).copyWith(
                                      canvasColor: Colors.white,
                                    ),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        isExpanded: true,
                                        iconSize: 0,
                                        icon: const Visibility(
                                            visible: false,
                                            child: Icon(Icons.arrow_downward)),
                                        elevation: 16,
                                        hint: controller.numberTop.value != 0
                                            ? Row(
                                          children: [
                                            Text(
                                              "Top ${controller.numberTop.value}",
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: ColorUtils.PRIMARY_COLOR),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        )
                                            : Row(
                                          children: [
                                            Text(
                                              'Top',
                                              style: TextStyle(
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400,
                                                  color: Colors.black),
                                            ),
                                            Expanded(child: Container()),
                                            const Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.black,
                                              size: 18,
                                            )
                                          ],
                                        ),
                                        items: [10,50,100].map(
                                              (value) {
                                            return DropdownMenuItem<int>(
                                              value: value,
                                              child: Text(
                                                "Top $value",
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: ColorUtils.PRIMARY_COLOR),
                                              ),
                                            );
                                          },
                                        ).toList(),
                                        onChanged: (int? value) {
                                          controller.indexPage.value = 1;
                                          controller.numberTop.value = value!;
                                          controller.getTopBlockTranscript(controller.semesterId.value);
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              )),
                          Expanded(
                              child: InkWell(
                                onTap: () {
                                  controller.indexPage.value = 1;
                                  controller.schoolLevelId.value = "";
                                  controller.schoolLevelName.value = "Tất cả";
                                  controller.semesterId.value = controller.listSemester[0].id!;
                                  controller.semesterName.value = controller.listSemester[0].name!;
                                  controller.blockId.value = "";
                                  controller.blockName.value = "Tất cả";
                                  controller.classId.value = "";
                                  controller.className.value = "Tất cả";
                                  controller.numberTop.value = 10;
                                  controller.getListSemester();
                                  controller.getListSchoolLevel();
                                  controller.getListBlock(AppCache().schoolYearId,controller.schoolLevelId.value);
                                  controller.listClassInBlockBySchoolLevel(AppCache().schoolYearId,controller.blockId.value,controller.schoolLevelId.value);
                                  controller.getTopBlockTranscript(controller.semesterId.value);
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                                  alignment: Alignment.center,
                                  height: 36.h,
                                  margin: EdgeInsets.only(left: 4.w),
                                  decoration: BoxDecoration(border: Border.all(color: Colors.green),
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  child: const Text("Reset bộ lọc",style: TextStyle(color: Colors.green),),
                                ),
                              ))
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 8.h,
              ),
              Expanded(
                  child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: IntrinsicWidth(
                        child: Container(
                          width: 460.w,
                          color: Colors.white,
                          padding: EdgeInsets.all(8.h),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                height: 30.h,
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(top: 8.h),
                                decoration: BoxDecoration(
                                    color: const Color.fromRGBO(235, 235, 235, 1),
                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(8.r),topRight: Radius.circular(8.r))
                                ),
                                child: ListView.builder(
                                  shrinkWrap: true,

                                  itemCount: controller.listHeader.length,
                                  physics: const NeverScrollableScrollPhysics(),
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (context, index) {
                                    return SizedBox(
                                      width: controller.setWidthHeader(controller.listHeader[index]),
                                      child: Text(controller.listHeader[index],textAlign: TextAlign.center),
                                    );
                                  },),
                              ),
                              Expanded(child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(8.r),bottomRight: Radius.circular(8.r))
                                ),
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  controller: controller.controllerLoadMore,
                                  itemCount: controller.itemsStatisticalTranscript.length,
                                  itemBuilder: (context, index) {
                                    return IntrinsicWidth(
                                      child: Container(
                                          margin: EdgeInsets.symmetric(vertical: 8.h),
                                          child: Container(
                                            decoration: const BoxDecoration(
                                                border: Border(bottom: BorderSide(color: Colors.black26))),
                                            child: SizedBox(
                                              height: 40.h,
                                              child: Row(
                                                children: [
                                                  SizedBox(
                                                    width: 50.w,
                                                    child: Text("${index+1}",
                                                        textAlign: TextAlign.center),
                                                  ),
                                                  Column(
                                                    children: [
                                                      SizedBox(
                                                        width: 150.w,
                                                        child: Text("${controller.itemsStatisticalTranscript[index].student?.fullName}",
                                                            textAlign: TextAlign.center),
                                                      ),
                                                      SizedBox(height: 4.h,),
                                                      SizedBox(
                                                        width: 120.w,
                                                        child: Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.itemsStatisticalTranscript[index].student?.birthday??0)).toString(),
                                                            textAlign: TextAlign.center),
                                                      )
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    width: 120.w,
                                                    child: Text("${controller.itemsStatisticalTranscript[index].clazz?.classCategory?.name}",
                                                        textAlign: TextAlign.center),
                                                  ),
                                                  SizedBox(
                                                    width: 120.w,
                                                    child: Center(
                                                      child: SizedBox(
                                                        height: 16.h,
                                                        child: ListView.builder(
                                                          shrinkWrap: true,
                                                          scrollDirection: Axis.horizontal,
                                                          itemCount: controller.itemsStatisticalTranscript[index].value?.mediumScore?.length,
                                                          itemBuilder: (context, indexMedium) {
                                                            return Column(
                                                              children: [
                                                                Container(
                                                                  margin: EdgeInsets.symmetric(horizontal: 8.w),
                                                                  child: Center(child: Text(controller.itemsStatisticalTranscript[index].value!.mediumScore![indexMedium].toString(),
                                                                      style: const TextStyle(
                                                                      ),
                                                                      textAlign: TextAlign.center)),
                                                                ),
                                                              ],
                                                            );
                                                          },),
                                                      ),
                                                    ),
                                                  ),

                                                ],
                                              ),
                                            ),
                                          )
                                      ),
                                    );
                                  },),
                              ))
                            ],
                          ),
                        ),
                      )))
            ],
          ),
        ))
    );
  }




}