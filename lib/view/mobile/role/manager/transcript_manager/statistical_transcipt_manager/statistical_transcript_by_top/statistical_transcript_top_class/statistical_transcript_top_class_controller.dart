import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/data/model/res/transcript/statistical_transcript.dart';
import 'package:slova_lms/data/model/res/transcript/transcript.dart';
import 'package:slova_lms/data/repository/transcript_repo/transcript_repo.dart';

import '../../../../../../../../commom/app_cache.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/home_room_teacher.dart';
import '../../../../../../../../data/model/res/class/block.dart';
import '../../../../../../../../data/model/res/school/school.dart';
import '../../../../../../../../data/model/res/transcript/semester.dart';
import '../../../../../../../../data/repository/school/school_repo.dart';
import '../../../../../../../../data/repository/subject/class_office_repo.dart';
import '../../../../manager_home_controller.dart';


class StatisticalTranscriptTopClassController extends GetxController{
  final TranscriptsRepo _transcriptsRepo = TranscriptsRepo();
  var transcript = <Transcript>[].obs;
  var semesters = <Semester>[].obs;
  var listSemester = <SemesterTranscript>[].obs;
  var controllerDateStart = TextEditingController().obs;
  var controllerDateEnd = TextEditingController().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy HH:mm');
  var semesterName ="".obs;
  var semesterId ="".obs;
  var selectedDay = DateTime.now().obs;
  var isPublic = "".obs;
  var indexPage = 1.obs;
  var numberTop = 10.obs;
  var statisticalTranscript = StatisticalTranscript().obs;
  var itemsStatisticalTranscript = <ItemsStatisticalTranscript>[].obs;
  var itemsStatisticalTranscriptLoadMore = <ItemsStatisticalTranscript>[].obs;
  var controllerLoadMore = ScrollController();
  var listHeader = ["STT","HỌ VÀ TÊN","LỚP","ĐIỂM TRUNG BÌNH"].obs;
  final ClassOfficeRepo _classOfficeRepo = ClassOfficeRepo();
  var blocks= <Block>[].obs;
  var listBlock = <Block1>[].obs;
  var blockId = "".obs;
  var blockName = "".obs;
  var classId = "".obs;
  var className = "".obs;
  var classes = <ClassBySchoolLevel>[].obs;
  var listClass = <Class1>[].obs;
  var listSchoolLevel = <SchoolLevel>[].obs;
  final SchoolRepo _schoolRepo = SchoolRepo();
  var schoolLevelName = "".obs;
  var schoolLevelId = "".obs;
  var school= School().obs;
  @override
  void onInit() {
    getListSemester();
    getListSchoolLevel();
    getListBlock(AppCache().schoolYearId,schoolLevelId.value);
    listClassInBlockBySchoolLevel(AppCache().schoolYearId,blockId.value,schoolLevelId.value);
    getTopBlockTranscript(semesters[0].id);
    controllerLoadMore = ScrollController()..addListener(_scrollListenerLoadMore);
    super.onInit();
  }

  @override
  dispose() {
    controllerLoadMore.dispose();
    super.dispose();
  }

  void _scrollListenerLoadMore() {
    if(numberTop.value>10){
      if (controllerLoadMore.position.pixels == controllerLoadMore.position.maxScrollExtent) {
        if(itemsStatisticalTranscriptLoadMore.isNotEmpty){
          indexPage++;
        }
        _transcriptsRepo.getTopStudentTranscript(indexPage.value, 20, blockId.value, classId.value, semesterId.value, numberTop.value,schoolLevelId.value).then((value) {
          if (value.state == Status.SUCCESS) {
            itemsStatisticalTranscriptLoadMore.value = [];
            statisticalTranscript.value = value.object!;
            itemsStatisticalTranscriptLoadMore.value = statisticalTranscript.value.items!;
            itemsStatisticalTranscript.addAll(itemsStatisticalTranscriptLoadMore);
            itemsStatisticalTranscript.refresh();
          }
        });
      }
    }

  }

  getListSchoolLevel(){
    listSchoolLevel.value = [];
    listSchoolLevel.add(SchoolLevel(id: "",name: "Tất cả"));
    _schoolRepo.getListSchoolLevel(Get.find<ManagerHomeController>().userProfile.value.school?.id).then((value) {
      if (value.state == Status.SUCCESS) {
        school.value = value.object!;
        for(int i =0;i< school.value.schoolLevel!.length ;i++){
          listSchoolLevel.add(SchoolLevel(id: "",name: ""));
          listSchoolLevel[i+1].name = school.value.schoolLevel![i].name;
          listSchoolLevel[i+1].id = school.value.schoolLevel![i].id!;
        }
        schoolLevelId.value = listSchoolLevel[0].id!;
        schoolLevelName.value = listSchoolLevel[0].name!;
      }
    });

    listSchoolLevel.refresh();
  }




  getListBlock(schoolYearId,levelId){
    blocks.value = [];
    listBlock.value = [];
    listBlock.add(Block1(id: "",name: "Tất cả"));
    blockId.value = listBlock[0].id!;
    blockName.value = listBlock[0].name!;
    _classOfficeRepo.listBlockBySchoolLevel(schoolYearId,levelId).then((value){
      if (value.state == Status.SUCCESS) {
        blocks.value = value.object!;
        for(int i =0;i< blocks.length ;i++){
          listBlock.add(Block1(id: "",name: ""));
          listBlock[i+1].name = blocks[i].blockCategory!.name;
          listBlock[i+1].id = blocks[i].id!;
        }
      }
    });
    listBlock.refresh();
    blocks.refresh();
  }




  listClassInBlockBySchoolLevel(schoolYearId,blockId,schoolLevelId){
    classes.value = [];
    listClass.value = [];
    listClass.add(Class1(id: "",name: "Tất cả"));
    classId.value = listClass[0].id!;
    className.value = listClass[0].name!;
    _classOfficeRepo.listClassInBlockBySchoolLevel(schoolYearId,blockId,schoolLevelId).then((value){
      if (value.state == Status.SUCCESS) {
        classes.value = value.object!;
        for(int i =0;i< classes.length ;i++){
          listClass.add(Class1(id: "",name: ""));
          listClass[i+1].name = classes[i].name;
          listClass[i+1].id = classes[i].classId!;
        }
      }

    });
    classes.refresh();
    listClass.refresh();


  }


  getTopBlockTranscript(semesterId){
    itemsStatisticalTranscript.value = [];
    _transcriptsRepo.getTopStudentTranscript(0, 20, blockId.value, classId.value, semesterId, numberTop.value,schoolLevelId.value).then((value) {
      if (value.state == Status.SUCCESS) {
        statisticalTranscript.value = value.object!;
        itemsStatisticalTranscript.value = statisticalTranscript.value.items!;
      }
    });
  }

  setWidthHeader(title){
    switch(title){
      case "STT":
        return 50.w;
      case "HỌ VÀ TÊN":
        return 150.w;
      default:
        return 120.w;
    }
  }
  setWidthBody(title){
    switch(title){
      case "order":
        return 50.w;
      case "fullName":
        return 150.w;
      default:
        return 120.w;
    }
  }




  getListSemester(){
    listSemester.value = [];
    _transcriptsRepo.getListSemester().then((value) {
      if (value.state == Status.SUCCESS) {
        semesters.value= value.object!;
        for(int i =0;i< semesters.length ;i++){
          listSemester.add(SemesterTranscript(id:semesters[i].id!,name:semesters[i].name!));
          listSemester[i].name = semesters[i].name!;
          listSemester[i].id = semesters[i].id!;
        }
        semesterName.value = listSemester[0].name!;
        semesterId.value = listSemester[0].id!;

      }
    });

    listSemester.refresh();
  }

}