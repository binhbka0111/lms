import 'package:get/get.dart';
import 'package:slova_lms/routes/app_pages.dart';

class StatisticalTranscriptManagerController extends GetxController{

getDetailStatisticalTranscriptSchoolInPercentPage(){
  Get.toNamed(Routes.statisticalTranscriptSchoolInPercentPage);
}
getDetailStatisticalTranscriptByTopPage(){
  Get.toNamed(Routes.statisticalTranscriptByTopPage);
}
}