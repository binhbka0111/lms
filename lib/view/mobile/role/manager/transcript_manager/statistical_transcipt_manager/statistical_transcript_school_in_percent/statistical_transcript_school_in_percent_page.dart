import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/data/model/res/class/block.dart';
import 'package:slova_lms/data/model/res/transcript/transcript.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/manager/transcript_manager/statistical_transcipt_manager/statistical_transcript_school_in_percent/statistical_transcript_school_in_percent_controller.dart';
import '../../../../../../../../commom/app_cache.dart';
import '../../../../../../../commom/widget/hexColor.dart';
import '../../../../../../../data/model/common/home_room_teacher.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';
import '../../../../../../../data/model/res/transcript/statistical_transcript.dart';


class StatisticalTranscriptSchoolInPercentPage extends GetView<StatisticalTranscriptSchoolInPercentController>{

  final StatisticalTranscriptSchoolInPercentController controller = Get.put(StatisticalTranscriptSchoolInPercentController());

  StatisticalTranscriptSchoolInPercentPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromRGBO(246, 246, 246, 1),
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                comeToHome();
              },
              icon: const Icon(Icons.home),
              color: Colors.white,
            )
          ],
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title: Text(
            'Thống kê bảng điểm trung bình theo %',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body: Obx(() => SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(top: 16.h,left: 16.w,right: 16.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(16.h),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.r),
                      color: Colors.white
                  ),
                  child: IntrinsicHeight(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child: IntrinsicWidth(
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                                    margin: EdgeInsets.only(right: 4.w),
                                    height: 36.h,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                            color: const Color.fromRGBO(192, 192, 192, 1)),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Theme(
                                      data: Theme.of(context).copyWith(
                                        canvasColor: Colors.white,
                                      ),
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton(
                                          isExpanded: true,
                                          iconSize: 0,
                                          icon: const Visibility(
                                              visible: false,
                                              child: Icon(Icons.arrow_downward)),
                                          elevation: 16,
                                          hint: controller.semesterName.value != ""
                                              ? Row(
                                            children: [
                                              Text(
                                                controller.semesterName.value,
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: ColorUtils.PRIMARY_COLOR),
                                              ),
                                              Expanded(child: Container()),
                                              const Icon(
                                                Icons.keyboard_arrow_down,
                                                color: Colors.black,
                                                size: 18,
                                              )
                                            ],
                                          )
                                              : Row(
                                            children: [
                                              Text(
                                                'Học kỳ',
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: Colors.black),
                                              ),
                                              Expanded(child: Container()),
                                              const Icon(
                                                Icons.keyboard_arrow_down,
                                                color: Colors.black,
                                                size: 18,
                                              )
                                            ],
                                          ),
                                          items: controller.listSemester.map(
                                                (value) {
                                              return DropdownMenuItem<SemesterTranscript>(
                                                value: value,
                                                child: Text(
                                                  value.name!,
                                                  style: TextStyle(
                                                      fontSize: 14.sp,
                                                      fontWeight: FontWeight.w400,
                                                      color: ColorUtils.PRIMARY_COLOR),
                                                ),
                                              );
                                            },
                                          ).toList(),
                                          onChanged: (SemesterTranscript? value) {
                                            controller.semesterName.value = value!.name!;
                                            controller.semesterId.value = value.id!;
                                            controller.getTranscriptSchoolPercent();
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                )),
                            Expanded(
                                flex: 1,
                                child:  IntrinsicWidth(
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                                    margin: EdgeInsets.only(left: 4.w),
                                    height: 36.h,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                            color: const Color.fromRGBO(192, 192, 192, 1)),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Theme(
                                      data: Theme.of(context).copyWith(
                                        canvasColor: Colors.white,
                                      ),
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton(
                                          isExpanded: true,
                                          iconSize: 0,
                                          icon: const Visibility(
                                              visible: false,
                                              child: Icon(Icons.arrow_downward)),
                                          elevation: 16,
                                          hint: controller.schoolLevelName.value != ""
                                              ? Row(
                                            children: [
                                              Text(
                                                controller.schoolLevelName.value,
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: ColorUtils.PRIMARY_COLOR),
                                              ),
                                              Expanded(child: Container()),
                                              const Icon(
                                                Icons.keyboard_arrow_down,
                                                color: Colors.black,
                                                size: 18,
                                              )
                                            ],
                                          )
                                              : Row(
                                            children: [
                                              Text(
                                                'Cấp học',
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: Colors.black),
                                              ),
                                              Expanded(child: Container()),
                                              const Icon(
                                                Icons.keyboard_arrow_down,
                                                color: Colors.black,
                                                size: 18,
                                              )
                                            ],
                                          ),
                                          items: controller.listSchoolLevel.map(
                                                (value) {
                                              return DropdownMenuItem<SchoolLevel>(
                                                value: value,
                                                child: Text(
                                                  "${value.name}",
                                                  style: TextStyle(
                                                      fontSize: 14.sp,
                                                      fontWeight: FontWeight.w400,
                                                      color: ColorUtils.PRIMARY_COLOR),
                                                ),
                                              );
                                            },
                                          ).toList(),
                                          onChanged: (SchoolLevel? value) {
                                            controller.schoolLevelName.value = value!.name!;
                                            controller.schoolLevelId.value = value.id!;
                                            controller.getListBlock(AppCache().schoolYearId,controller.schoolLevelId.value);
                                            controller.getTranscriptSchoolPercent();
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: 8.h,
                        ),
                        Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child:  IntrinsicWidth(
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                                    margin: EdgeInsets.only(right: 4.w),
                                    height: 36.h,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                            color: const Color.fromRGBO(192, 192, 192, 1)),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Theme(
                                      data: Theme.of(context).copyWith(
                                        canvasColor: Colors.white,
                                      ),
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton(
                                          isExpanded: true,
                                          iconSize: 0,
                                          icon: const Visibility(
                                              visible: false,
                                              child: Icon(Icons.arrow_downward)),
                                          elevation: 16,
                                          hint: controller.blockName.value != ""
                                              ? Row(
                                            children: [
                                              Text(
                                                controller.blockName.value,
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: ColorUtils.PRIMARY_COLOR),
                                              ),
                                              Expanded(child: Container()),
                                              const Icon(
                                                Icons.keyboard_arrow_down,
                                                color: Colors.black,
                                                size: 18,
                                              )
                                            ],
                                          )
                                              : Row(
                                            children: [
                                              Text(
                                                'Khối',
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: Colors.black),
                                              ),
                                              Expanded(child: Container()),
                                              const Icon(
                                                Icons.keyboard_arrow_down,
                                                color: Colors.black,
                                                size: 18,
                                              )
                                            ],
                                          ),
                                          items: controller.listBlock.map(
                                                (value) {
                                              return DropdownMenuItem<Block1>(
                                                value: value,
                                                child: Text(
                                                  value.name!,
                                                  style: TextStyle(
                                                      fontSize: 14.sp,
                                                      fontWeight: FontWeight.w400,
                                                      color: ColorUtils.PRIMARY_COLOR),
                                                ),
                                              );
                                            },
                                          ).toList(),
                                          onChanged: (Block1? value) {
                                            controller.blockName.value = value!.name!;
                                            controller.blockId.value = value.id!;
                                            controller.listClassInBlockBySchoolLevel(AppCache().schoolYearId,controller.blockId.value,controller.schoolLevelId.value);
                                            controller.getTranscriptSchoolPercent();
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                )),
                            Expanded(
                                flex: 1,
                                child: IntrinsicWidth(
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                                    margin: EdgeInsets.only(left: 4.w),
                                    height: 36.h,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                            color: const Color.fromRGBO(192, 192, 192, 1)),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: Theme(
                                      data: Theme.of(context).copyWith(
                                        canvasColor: Colors.white,
                                      ),
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton(
                                          isExpanded: true,
                                          iconSize: 0,
                                          icon: const Visibility(
                                              visible: false,
                                              child: Icon(Icons.arrow_downward)),
                                          elevation: 16,
                                          hint: controller.className.value != ""
                                              ? Row(
                                            children: [
                                              Text(
                                                controller.className.value,
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: ColorUtils.PRIMARY_COLOR),
                                              ),
                                              Expanded(child: Container()),
                                              const Icon(
                                                Icons.keyboard_arrow_down,
                                                color: Colors.black,
                                                size: 18,
                                              )
                                            ],
                                          )
                                              : Row(
                                            children: [
                                              Text(
                                                'Lớp',
                                                style: TextStyle(
                                                    fontSize: 14.sp,
                                                    fontWeight: FontWeight.w400,
                                                    color: Colors.black),
                                              ),
                                              Expanded(child: Container()),
                                              const Icon(
                                                Icons.keyboard_arrow_down,
                                                color: Colors.black,
                                                size: 18,
                                              )
                                            ],
                                          ),
                                          items: controller.listClass.map(
                                                (value) {
                                              return DropdownMenuItem<Class1>(
                                                value: value,
                                                child: Text(
                                                  value.name!,
                                                  style: TextStyle(
                                                      fontSize: 14.sp,
                                                      fontWeight: FontWeight.w400,
                                                      color: ColorUtils.PRIMARY_COLOR),
                                                ),
                                              );
                                            },
                                          ).toList(),
                                          onChanged: (Class1? value) {
                                            controller.className.value = value!.name!;
                                            controller.classId.value = value.id!;
                                            controller.getTranscriptSchoolPercent();
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                )),
                          ],
                        ),
                        SizedBox(
                          height: 8.h,
                        ),
                        Row(

                          children: [
                            Expanded(child: Container()),
                            Expanded(child: InkWell(
                              onTap: ()  {
                               controller.resetFilter();
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 16.w),
                                alignment: Alignment.center,
                                height: 36.h,
                                margin: EdgeInsets.only(left: 4.w),
                                decoration: BoxDecoration(border: Border.all(color: Colors.green),
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                child: const Text("Reset bộ lọc",style: TextStyle(color: Colors.green),),
                              ),
                            ))
                          ],
                        ),


                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 8.h,
                ),
                controller.isShowChart.value?Center(
                    child: SfCircularChart(
                        legend: Legend(isVisible: true),
                        series: <CircularSeries>[
                          // Render pie chart

                          PieSeries<StatisticalTranscriptPercent, String>(
                            dataSource: controller.statisticalTranscript,
                            pointColorMapper:(StatisticalTranscriptPercent data,  _) => HexColor("${data.color}"),
                            yValueMapper: (StatisticalTranscriptPercent data, _) => data.total,
                            xValueMapper: (StatisticalTranscriptPercent sales, _) => "Điểm ${sales.startScore} - ${sales.endScore}",
                          )
                        ]
                    )
                ): Center(
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: 16.h),
                    child: Text("không có dữ liêụ"),
                  ),
                ),

                SizedBox(
                  height: 8.h,
                ),
                Container(
                  padding: const EdgeInsets.all(8),
                  color: const Color.fromRGBO(235, 235, 235, 1),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("Tổng số học sinh"),
                      Text("${controller.totalStudent.value}")
                    ],
                  ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: controller.statisticalTranscript.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        checkClickFeature(Get.find<HomeController>().userGroupByApp, () => controller.getDetailListStudent(index), StringConstant.FEATURE_TRANSCRIPT_PERCENT_DETAIL);
                      },
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        color: index%2!=1?Colors.white:const Color.fromRGBO(235, 235, 235, 1),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Số học sinh có điểm trung bình từ ${controller.statisticalTranscript[index].startScore} đến ${controller.statisticalTranscript[index].endScore}"),
                            Text("${controller.statisticalTranscript[index].total}")
                          ],
                        ),
                      ),
                    );
                  },)
              ],
            ),
          ),
        ))
    );
  }




}