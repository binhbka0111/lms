import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/data/model/res/school/school.dart';
import 'package:slova_lms/data/model/res/transcript/statistical_transcript.dart';
import 'package:slova_lms/data/model/res/transcript/transcript.dart';
import 'package:slova_lms/data/repository/school/school_repo.dart';
import 'package:slova_lms/data/repository/transcript_repo/transcript_repo.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/role/manager/manager_home_controller.dart';
import '../../../../../../../../commom/app_cache.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/res/class/block.dart';
import '../../../../../../../../data/model/res/transcript/semester.dart';
import '../../../../../../../../data/repository/subject/class_office_repo.dart';
import '../../../../../../../data/model/common/home_room_teacher.dart';


class StatisticalTranscriptSchoolInPercentController extends GetxController{
  final TranscriptsRepo _transcriptsRepo = TranscriptsRepo();
  var transcript = <Transcript>[].obs;
  var semesters = <Semester>[].obs;
  var listSemester = <SemesterTranscript>[].obs;
  var controllerDateStart = TextEditingController().obs;
  var controllerDateEnd = TextEditingController().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputDateFormatV2 = DateFormat('dd/MM/yyyy HH:mm');
  var semesterName ="".obs;
  var semesterId ="".obs;
  var selectedDay = DateTime.now().obs;
  var isPublic = "".obs;
  var indexPage = 1.obs;

  var schoolLevelName = "".obs;
  var schoolLevelId = "".obs;
  var statisticalTranscript = <StatisticalTranscriptPercent>[].obs;
  final ClassOfficeRepo _classOfficeRepo = ClassOfficeRepo();
  final SchoolRepo _schoolRepo = SchoolRepo();
  var blocks= <Block>[].obs;
  var listBlock = <Block1>[].obs;
  var blockId = "".obs;
  var blockName = "".obs;
  var classId = "".obs;
  var className = "".obs;
  var classes = <ClassBySchoolLevel>[].obs;
  var listClass = <Class1>[].obs;
  var school= School().obs;
  var listSchoolLevel = <SchoolLevel>[].obs;
  var totalStudent = 0.obs;
  RxBool isShowChart = false.obs;
  @override
  void onInit() {
    getListSemester();
    getListSchoolLevel();
    getListBlock(AppCache().schoolYearId,schoolLevelId.value);
    listClassInBlockBySchoolLevel(AppCache().schoolYearId,blockId.value,schoolLevelId.value);
    super.onInit();
  }


  getListSchoolLevel()async {
    listSchoolLevel.value = [];
    await _schoolRepo.getListSchoolLevel(Get.find<ManagerHomeController>().userProfile.value.school?.id).then((value) {
      if (value.state == Status.SUCCESS) {
        school.value = value.object!;
        for(int i =0;i< school.value.schoolLevel!.length ;i++){
          listSchoolLevel.add(SchoolLevel(id: "",name: ""));
          listSchoolLevel[i].name = school.value.schoolLevel![i].name;
          listSchoolLevel[i].id = school.value.schoolLevel![i].id!;
        }
        schoolLevelId.value = listSchoolLevel[0].id!;
        schoolLevelName.value = listSchoolLevel[0].name!;
        getTranscriptSchoolPercent();
      }
    });

    listSchoolLevel.refresh();
  }


  setVisibleChart(){
    for(int i = 0 ;i <statisticalTranscript.length;i++){
      if(statisticalTranscript[i].total != 0){
        isShowChart.value = true;
        break;
      }else{
        isShowChart.value = false;
      }
    }
  }



  getListBlock(schoolYearId,levelId)async{
    blocks.value = [];
    listBlock.value = [];
    listBlock.add(Block1(id: "",name: "Tất cả"));
    blockId.value = listBlock[0].id!;
    blockName.value = listBlock[0].name!;
    await _classOfficeRepo.listBlockBySchoolLevel(schoolYearId,levelId).then((value)async {
      if (value.state == Status.SUCCESS) {
        blocks.value = value.object!;
        for(int i =0;i< blocks.length ;i++){
          listBlock.add(Block1(id: "",name: ""));
          listBlock[i+1].name = blocks[i].blockCategory!.name;
          listBlock[i+1].id = blocks[i].id!;
        }
      }
      await getTranscriptSchoolPercent();
    });
    listBlock.refresh();
    blocks.refresh();
  }




  listClassInBlockBySchoolLevel(schoolYearId,blockId,schoolLevelId)async{
    classes.value = [];
    listClass.value = [];
    listClass.add(Class1(id: "",name: "Tất cả"));
    classId.value = listClass[0].id!;
    className.value = listClass[0].name!;
    await _classOfficeRepo.listClassInBlockBySchoolLevel(schoolYearId,blockId,schoolLevelId).then((value){
      if (value.state == Status.SUCCESS) {
        classes.value = value.object!;
        for(int i =0;i< classes.length ;i++){
          listClass.add(Class1(id: "",name: ""));
          listClass[i+1].name = classes[i].name;
          listClass[i+1].id = classes[i].classId!;
        }
      }
      getTranscriptSchoolPercent();
    });
    classes.refresh();
    listClass.refresh();


  }





  getTranscriptSchoolPercent()async {
    totalStudent.value = 0;
    await _transcriptsRepo.getTranscriptSchoolPercent(
        schoolLevelId.value,
        semesterId.value,
        blockId.value,
        classId.value).then((value) {
      if (value.state == Status.SUCCESS) {
        statisticalTranscript.value = value.object!;

        for(int i =0;i<  statisticalTranscript.length ;i++){
          totalStudent.value += statisticalTranscript[i].total!;
        }
      }
    });
    await setVisibleChart();
  }

  setWidthHeader(title){
    switch(title){
      case "STT":
        return 50.w;
      case "HỌ VÀ TÊN":
        return 150.w;
      default:
        return 120.w;
    }
  }
  setWidthBody(title){
    switch(title){
      case "order":
        return 50.w;
      case "fullName":
        return 150.w;
      default:
        return 120.w;
    }
  }




  getListSemester()async{
    listSemester.value = [];
    await _transcriptsRepo.getListSemester().then((value) {
      if (value.state == Status.SUCCESS) {
        semesters.value= value.object!;
        for(int i =0;i< semesters.length ;i++){
          listSemester.add(SemesterTranscript(id: "",name: ""));
          listSemester[i].name = semesters[i].name!;
          listSemester[i].id = semesters[i].id!;
        }
        semesterName.value = listSemester[0].name!;
        semesterId.value = listSemester[0].id!;
      }
      getTranscriptSchoolPercent();
    });
    listSemester.refresh();
  }
  getDetailListStudent(index){
    Get.toNamed(Routes.listStudentTranscriptPercentPage,arguments: [statisticalTranscript[index].startScore,statisticalTranscript[index].endScore]);

  }


  resetFilter()async{
    semesterId.value ="";
    blockId.value = "";
    classId.value = "";
    await getListSchoolLevel();
    await getListSemester();

    await getListBlock(AppCache().schoolYearId,schoolLevelId.value);
    await listClassInBlockBySchoolLevel(AppCache().schoolYearId, blockId.value, schoolLevelId.value);
    await  getTranscriptSchoolPercent();
  }

}