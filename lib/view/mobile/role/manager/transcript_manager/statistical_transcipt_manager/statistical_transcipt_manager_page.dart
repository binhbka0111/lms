import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/manager/transcript_manager/statistical_transcipt_manager/statistical_transcipt_manager_controller.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';


class StatisticalTranscriptManagerPage
    extends GetWidget<StatisticalTranscriptManagerController> {
  @override
  final controller = Get.put(StatisticalTranscriptManagerController());

  StatisticalTranscriptManagerPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            elevation: 0,
            title: Text(
              "Thống kê bảng điểm",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w500),
            ),
            actions: [
              InkWell(
                onTap: () {
                  comeToHome();
                },
                child: const Icon(
                  Icons.home,
                  color: Colors.white,
                ),
              ),
              Padding(padding: EdgeInsets.only(right: 16.w))
            ],
          ),
          body: Container(
            padding: const EdgeInsets.all(16),
            child: Column(
              children: [
             Visibility(
                 visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_STATISTICAL_SYNTHETIC_PERCENT),
                 child:    InkWell(
               onTap: () {
                 checkClickFeature(Get.find<HomeController>().userGroupByApp,() => controller.getDetailStatisticalTranscriptSchoolInPercentPage(), StringConstant.FEATURE_TRANSCRIPT_PERCENT_LIST);
               },
               child: Card(
                 elevation: 1,
                 shape: RoundedRectangleBorder(
                     borderRadius: BorderRadius.circular(6)),
                 child: Container(
                   padding: const EdgeInsets.all(16),
                   child: Row(
                     children: [

                       Expanded(child:   Text(
                         "Thống kê bảng điểm trung bình toàn trường theo %",
                         style: TextStyle(
                             fontSize: 12.sp,
                             color: const Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.w500),
                       )),
                       const Icon(
                         Icons.arrow_forward_ios,
                         color: ColorUtils.PRIMARY_COLOR,
                         size: 18,
                       )

                     ],
                   ),
                 ),
               ),
             )),
                SizedBox(height: 8.h,),
              Visibility(
                  visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_STATISTICAL_SYNTHETIC_TOP),
                  child:   InkWell(
                onTap: () {
                  controller.getDetailStatisticalTranscriptByTopPage();
                },
                child: Card(
                  elevation: 1,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(6)),
                  child: Container(
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      children: [
                        Text(
                          "Thống kê bảng điểm trung bình theo top",
                          style: TextStyle(
                              fontSize: 12.sp,
                              color: const Color.fromRGBO(26, 26, 26, 1),
                              fontWeight: FontWeight.w500),
                        ),
                        Expanded(child: Container()),
                        const Icon(
                          Icons.arrow_forward_ios,
                          color: ColorUtils.PRIMARY_COLOR,
                          size: 18,
                        ),

                      ],
                    ),
                  ),
                ),
              )),
              ],
            ),
          ),
        );
  }
}
