import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import '../../../../../../routes/app_pages.dart';
import 'list_block_transcript_manager_controller.dart';

class ListBlockTranscriptManagerPage extends GetWidget<ListBlockTranscriptManagerController>{
  @override
  final controller = Get.put(ListBlockTranscriptManagerController());

  ListBlockTranscriptManagerPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: const Text("Danh sách khối"),
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        elevation: 0,
      ),
      body:Obx(() => ListView.builder(
          itemCount: controller.blocks.length,
          itemBuilder: (context,index){
            return Obx(() => InkWell(
              onTap: () {
                Get.toNamed(Routes.listClassInBlockTranscriptManagerPage,arguments: controller.blocks[index]);
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(16.w, 4.h, 16.w, 4.h),
                child: Column(
                  children: [
                    Card(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Container(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Text(
                              "${controller.blocks[index].blockCategory?.name}",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontWeight: FontWeight.bold),
                            ),
                            Expanded(child: Container()),
                            const Icon(
                              Icons.keyboard_arrow_right,
                              color: ColorUtils.PRIMARY_COLOR,
                              size: 28,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
          })),
    );
  }

}