import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'class_transcript_controller.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';



class ClassTranscriptManagerPage extends GetWidget<ClassTranscriptManagerController>{
  @override
  final controller = Get.put(ClassTranscriptManagerController());

  ClassTranscriptManagerPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromRGBO(246, 246, 246, 1),
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                comeToHome();
              },
              icon: const Icon(Icons.home),
              color: Colors.white,
            )
          ],
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title:  Text(
            'Bảng điểm ${controller.classes.value.classCategory?.name}',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 16.w,vertical: 16.h),
          child: Column(
            children: [
             Visibility(
                 visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_TRANSCRIPT_SYNTHETIC),
                 child:  InkWell(
               onTap: () {
                checkClickFeature(Get.find<HomeController>().userGroupByApp,() => controller.getDetailTranscriptSyntheticManager(), StringConstant.FEATURE_TRANSCRIPT_SYNTHETIC_LIST);
               },
               child: Card(
                 elevation: 1,
                 shape: RoundedRectangleBorder(
                     borderRadius: BorderRadius.circular(6)),
                 child: Container(
                   padding: const EdgeInsets.all(16),
                   child: Row(
                     children: [
                       Text(
                         "Bảng điểm tổng hợp",
                         style: TextStyle(
                             fontSize: 14.sp,
                             color: const Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.bold),
                       ),
                       Expanded(child: Container()),
                       const Icon(
                         Icons.keyboard_arrow_right,
                         color: ColorUtils.PRIMARY_COLOR,
                         size: 28,
                       ),

                     ],
                   ),
                 ),
               ),
             )),
              SizedBox(
                height: 8.h,
              ),
             Visibility(
                 visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_TRANSCRIPT),
                 child:  InkWell(
               onTap: () {
                 checkClickFeature(Get.find<HomeController>().userGroupByApp,() => controller.getDetailTranscriptManager(), StringConstant.FEATURE_TRANSCRIPT_SYNTHETIC_LIST);

               },
               child: Card(
                 elevation: 1,
                 shape: RoundedRectangleBorder(
                     borderRadius: BorderRadius.circular(6)),
                 child: Container(
                   padding: const EdgeInsets.all(16),
                   child: Row(
                     children: [
                       Text(
                         "Bảng điểm môn học",
                         style: TextStyle(
                             fontSize: 14.sp,
                             color: const Color.fromRGBO(26, 26, 26, 1),
                             fontWeight: FontWeight.bold),
                       ),
                       Expanded(child: Container()),
                       const Icon(
                         Icons.keyboard_arrow_right,
                         color: ColorUtils.PRIMARY_COLOR,
                         size: 28,
                       ),

                     ],
                   ),
                 ),
               ),
             )),
            ],
          ),
        )
    );
  }




}