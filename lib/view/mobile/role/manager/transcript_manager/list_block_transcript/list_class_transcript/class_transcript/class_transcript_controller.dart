import 'package:get/get.dart';
import 'package:slova_lms/data/model/res/class/block.dart';
import 'package:slova_lms/routes/app_pages.dart';
class ClassTranscriptManagerController extends GetxController{
  var classes = Class().obs;


  @override
  void onInit() {
    super.onInit();
    var data = Get.arguments;
    if(data != ""|| data!= null){
      classes.value = data;
    }

  }

  getDetailTranscriptSyntheticManager(){
    Get.toNamed(Routes.viewTranscriptsSyntheticManagerPage);
  }

  getDetailTranscriptManager(){
    Get.toNamed(Routes.listSubjectByClassManagerPage);
  }
}