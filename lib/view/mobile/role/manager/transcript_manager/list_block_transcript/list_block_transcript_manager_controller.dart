import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/repository/subject/class_office_repo.dart';
import 'package:get/get.dart';
import '../../../../../../commom/app_cache.dart';
import '../../../../../../data/model/res/class/block.dart';
class ListBlockTranscriptManagerController extends GetxController {
  final ClassOfficeRepo _classOfficeRepo = ClassOfficeRepo();
  var blocks= <Block>[].obs;


  @override
  void onInit() {
    super.onInit();
    getListBlock(AppCache().schoolYearId);
  }

  getListBlock(schoolYearId){
    blocks.clear();
    _classOfficeRepo.listBlock(schoolYearId).then((value){
      if (value.state == Status.SUCCESS) {
        blocks.value = value.object!;
      }
    });
    blocks.refresh();
  }


}