import 'package:get/get.dart';
import 'package:slova_lms/commom/app_cache.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/widget/logout.dart';
import 'package:slova_lms/data/model/common/user_profile.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/event_news/event_news_controller.dart';
import 'package:slova_lms/view/mobile/role/manager/schedule_manager/schedule_manager_controller.dart';
import 'package:slova_lms/view/mobile/role/teacher/teacher_home_controller.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../commom/constants/string_constant.dart';
import '../../../../data/model/common/Position.dart';
import '../../../../data/model/common/school_year.dart';
import '../../../../data/model/common/static_page.dart';
import '../../../../data/model/res/class/School.dart';
import '../../../../data/model/res/class/classTeacher.dart';
import '../../../../data/repository/common/common_repo.dart';
import '../../../../data/repository/person/personal_info_repo.dart';
import '../../../../data/repository/school_year/school_year_repo.dart';
import '../../../../data/repository/subject/class_office_repo.dart';
import '../../home/home_controller.dart';
import '../../phonebook/phone_book_controller.dart';
import '../../schedule/schedule_controller.dart';

class ManagerHomeController extends GetxController {
  var indexClass = 0.obs;

  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();
  var userProfile = UserProfile().obs;
  var school = SchoolData().obs;
  var position = Position().obs;
  var staticPage = ItemsStaticPage().obs;
  final CommonRepo _commonRepo = CommonRepo();
  final SchoolYearRepo _schoolYearRepo = SchoolYearRepo();
  RxList<SchoolYear> schoolYears = <SchoolYear>[].obs;
  var clickSchoolYear = <bool>[].obs;
  var fromYearPresent = 0.obs;
  var toYearPresent = 0.obs;
  RxList<Clazzs> classByTeacher = <Clazzs>[].obs;
  RxList<Clazzs>  listclass = <Clazzs>[].obs;

  var listPageClassroomManagement = <ItemHomePageTeacher>[];
  var listPageInformationFromSchool = <ItemHomePageTeacher>[];
  static const diligentManagement = "Quản Lý Chuyên Cần";
  static const transcript = "Bảng điểm";
  static const sendNotify = "Gửi Thông Báo";
  static const listStudentInClass = "Danh Sách Học Sinh";
  static const newAndEvent = "Tin Tức Sự Kiện";
  static const notification = "Thông báo";

  var isReady = false.obs;
  Rx<ClassId> currentClass = ClassId().obs;
  final ClassOfficeRepo _classRepo = ClassOfficeRepo();
  RxList<ClassId> classOfManager = <ClassId>[].obs;
  @override
  void onInit() {
    super.onInit();
    getSchoolYearByUser();
    getDetailProfile();
  }


  getListClassroomManagement(){
    if(Get.find<HomeController>().userGroupByApp.isEmpty){
      listPageClassroomManagement.add(ItemHomePageTeacher(name: diligentManagement,image: "assets/images/img_amicroteacher.png"));
      listPageClassroomManagement.add(ItemHomePageTeacher(name: listStudentInClass,image: "assets/images/img_list_student.png"));
      listPageClassroomManagement.add(ItemHomePageTeacher(name: sendNotify,image: "assets/images/img_sendnotify.png"));
      listPageClassroomManagement.add(ItemHomePageTeacher(name: transcript,image: "assets/images/pana.png"));
    }else{
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_MANAGE_DILIGENCE)){
        listPageClassroomManagement.add(ItemHomePageTeacher(name: diligentManagement,image: "assets/images/img_amicroteacher.png"));
      }
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_LIST_STUDENT)){
        listPageClassroomManagement.add(ItemHomePageTeacher(name: listStudentInClass,image: "assets/images/img_list_student.png"));
      }
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_SEND_NOTIFY)){
        listPageClassroomManagement.add(ItemHomePageTeacher(name: sendNotify,image: "assets/images/img_sendnotify.png"));
      }
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_MANAGE_TRANSCRIPT)){
        listPageClassroomManagement.add(ItemHomePageTeacher(name: transcript,image: "assets/images/pana.png"));
      }
    }
  }

  getListInformationFromSchool(){
    if(Get.find<HomeController>().userGroupByApp.isEmpty){
      listPageInformationFromSchool.add(ItemHomePageTeacher(name: newAndEvent,image: "assets/images/img_event.png"));
      listPageInformationFromSchool.add(ItemHomePageTeacher(name: notification,image: "assets/images/img_notify.png"));
    }else{
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_NEWS)){
        listPageInformationFromSchool.add(ItemHomePageTeacher(name: newAndEvent,image: "assets/images/img_event.png"));
      }
      if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_HOME_NOTIFY)){
        listPageInformationFromSchool.add(ItemHomePageTeacher(name: notification,image: "assets/images/img_notify.png"));
      }

    }
  }

  onClickSchoolYears(index) {
    clickSchoolYear.value = <bool>[].obs;
    for (int i = 0; i < schoolYears.length; i++) {
      clickSchoolYear.add(false);
    }
    clickSchoolYear[index] = true;
  }

  getSchoolYearByUser() async {
    await _schoolYearRepo.getListSchoolYearByUser().then((value) async{
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        if (schoolYears.isNotEmpty) {
          fromYearPresent.value = schoolYears[0].fromYear!;
          toYearPresent.value = schoolYears[0].toYear!;
          await getListClassByManager();
          // Get.find<DetailDiligentByClassManagerController>().setDateSchoolYears();
        }
        for (int i = 0; i < schoolYears.length; i++) {
          clickSchoolYear.add(false);
        }
      }
      await Get.find<HomeController>().getCountNotifyNotSeen();
    });

  }
  getType(type, subject){
    switch(type){
      case "HOME_ROOM_TEACHER":
        return "Giáo viên chủ nhiệm";
      case "SUBJECT_TEACHER":
        return "Giáo viên $subject";
      default:
        return "";
    }

  }
  getListClassByManager() async{
    var teacherId = userProfile.value.id;
    await _classRepo.listClassByTeacher(teacherId).then((value) {
      if (value.state == Status.SUCCESS) {
        classOfManager.value = value.object!;
        for (var f in classOfManager) {
          f.checked = false;
        }
        classOfManager.first.checked = true;
        currentClass.value = classOfManager.first;
        classOfManager.refresh();
      }
    });
  }

  selectedClass(ClassId classId, index) {
    currentClass.value = classId;
    for (var f in classOfManager) {
      if (f.id != classId.id) {
        f.checked = false;
      } else {
        f.checked = true;
      }
    }
    classOfManager.refresh();
  }

  void submitLogout() async {
    logout();
  }


  getDetailProfile() async{
    await _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        AppCache().userId = userProfile.value.id ?? "";
        if (userProfile.value.school == null) {
          school.value.name = "";
          school.value.id = "";
        } else {
          school.value = userProfile.value.school!;
          Get.put(EventNewsController());
          userProfile.value.school!.id!;
          Get.find<EventNewsController>().setSchoolId(userProfile.value.school!.id!);
          Get.find<EventNewsController>().onRefresh();
        }
        if (userProfile.value.position == null) {
          position.value.name = "";
          position.value.id = "";
        } else {
          position.value = userProfile.value.position!;
        }
      }
    });
    isReady.value = true;
  }



  onSelectSchoolYears(index) {
    Get.find<ScheduleManagerController>().blocks.clear();
    AppCache().setSchoolYearId(schoolYears[index].id??"");
    fromYearPresent.value = schoolYears[index].fromYear!;
    toYearPresent.value = schoolYears[index].toYear!;
    Get.find<ScheduleController>().queryTimeCalendar(AppCache().userType);
    Get.find<PhoneBookController>().getListClassByStudent();
    getListClassByManager();
    Get.find<ScheduleManagerController>().getListBlock(schoolYears[index].id!);
    Get.find<ScheduleManagerController>().blocks.refresh();

    Get.back();
  }

  onRefresh() {
    getDetailProfile();
  }

  getStaticPage(type) {
    _commonRepo.getListStaticPage(type).then((value) {
      if (value.state == Status.SUCCESS) {
        staticPage.value = value.object!;
        goToStaticPage();
      }
    });
  }

  goToStaticPage() {
    Get.toNamed(Routes.staticPage, arguments: staticPage.value);
  }

  void goToStudentListPage() {
    Get.toNamed(Routes.studentListPage, arguments: currentClass.value);
  }

  void goToLogin() {
    Get.toNamed(Routes.login);
  }

  goToUpdateInfoUser() {
    Get.toNamed(Routes.updateInfoManager);
  }

  void goToDetailAvatar() {
    Get.toNamed(Routes.getavatar, arguments: userProfile.value.image);
  }

  void goToChangePassPage() {
    Get.toNamed(Routes.changePass, arguments: userProfile.value.id);
  }
}
