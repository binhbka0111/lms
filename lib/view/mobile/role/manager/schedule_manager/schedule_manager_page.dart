import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/role/manager/schedule_manager/schedule_manager_controller.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../../routes/app_pages.dart';
import '../../../home/home_controller.dart';
class ScheduleManagerPage extends GetWidget<ScheduleManagerController>{
  @override
  final controller = Get.put(ScheduleManagerController());

  ScheduleManagerPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: const Text("Công việc"),
        leading: BackButton(
          color: Colors.white,
          onPressed: () {
            Get.find<HomeController>().comeBackHome();
          },
        ),
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        elevation: 0,
      ),
      body:Obx(() => controller.isReady.value?ListView.builder(
          itemCount: controller.blocks.length,
          itemBuilder: (context,index){
            return Obx(() => InkWell(
              onTap: () {
                Get.toNamed(Routes.listClassInBlock,arguments: controller.blocks[index]);
              },
              child: Container(
                margin: EdgeInsets.fromLTRB(16.w, 4.h, 16.w, 4.h),
                child: Column(
                  children: [
                    Card(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      child: Container(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Text(
                              "${controller.blocks[index].blockCategory?.name}",
                              style: TextStyle(
                                  fontSize: 14.sp,
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontWeight: FontWeight.bold),
                            ),
                            Expanded(child: Container()),
                            const Icon(
                              Icons.keyboard_arrow_right,
                              color: Color.fromRGBO(248, 129, 37, 1),
                              size: 28,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
          }):const Center(
    child: CircularProgressIndicator(color: ColorUtils.PRIMARY_COLOR,),
    )),
    );
  }

}