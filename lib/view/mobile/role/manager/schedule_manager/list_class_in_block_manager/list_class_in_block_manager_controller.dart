import 'package:get/get.dart';
import '../../../../../../commom/app_cache.dart';
import '../../../../../../data/base_service/api_response.dart';
import '../../../../../../data/model/res/class/block.dart';
import '../../../../../../data/repository/subject/class_office_repo.dart';


class ListClassInBlockManagerController extends GetxController {
  final ClassOfficeRepo _classOfficeRepo = ClassOfficeRepo();
  var classes = <Class>[].obs;
  RxInt clickClass = 0.obs;
  var block = Block().obs;
  var classId = "".obs;
  var isReady = false.obs;
  @override
  void onInit() {
    super.onInit();
    var data = Get.arguments;
    if(data != ""|| data!= null){
      block.value = data;
    }
    getListClassInBlock(AppCache().schoolYearId,block.value.id);
  }


  getListClassInBlock(schoolYearId,blockId){
    classes.clear();
    _classOfficeRepo.listClassInBlock(schoolYearId,blockId).then((value){
      if (value.state == Status.SUCCESS) {
        classes.value = value.object!;
      }
    });
    classes.refresh();
    isReady.value = true;
  }


}
