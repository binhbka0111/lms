import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import '../../../../../commom/app_cache.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/common/schedule.dart';
import '../../../../../data/model/res/class/block.dart';
import '../../../../../data/repository/subject/class_office_repo.dart';

class ScheduleManagerController extends GetxController {
  final ClassOfficeRepo _classOfficeRepo = ClassOfficeRepo();
  var blocks= <Block>[].obs;
  var block = Block().obs;
  RxList<Appointment> timeTable = <Appointment>[].obs;
  RxList<Schedule> schedule = <Schedule>[].obs;
  var isReady = false.obs;
  @override
  void onInit() {
    super.onInit();
    getListBlock(AppCache().schoolYearId);
  }

  getListBlock(schoolYearId){
    blocks.clear();
    _classOfficeRepo.listBlock(schoolYearId).then((value){
      if (value.state == Status.SUCCESS) {
        blocks.value = value.object!;
      }
    });
    blocks.refresh();
    isReady.value = true;
  }


}
