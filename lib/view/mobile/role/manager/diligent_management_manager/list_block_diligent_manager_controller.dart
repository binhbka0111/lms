import 'package:get/get.dart';
import 'package:slova_lms/routes/app_pages.dart';
import '../../../../../commom/app_cache.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/res/class/block.dart';
import '../../../../../data/repository/subject/class_office_repo.dart';

class ListBlockDiligentManagerController extends GetxController{
  final ClassOfficeRepo _classOfficeRepo = ClassOfficeRepo();
  var blocks= <Block>[].obs;

  @override
  void onInit() {
    getListBlock(AppCache().schoolYearId);
    super.onInit();
  }

  getListBlock(schoolYearId){
    blocks.clear();
    _classOfficeRepo.listBlock(schoolYearId).then((value){
      if (value.state == Status.SUCCESS) {
        blocks.value = value.object!;
      }
    });
    blocks.refresh();
  }



  gotoDetailListClass(index){
    Get.toNamed(Routes.listClassInBlockDiligentManager, arguments: blocks[index]);
  }
}