import 'package:get/get.dart';
import 'list_block_diligent_manager_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class ListBlockDiligentManagerPage
    extends GetView<ListBlockDiligentManagerController> {
  @override
  final controller = Get.put(ListBlockDiligentManagerController());

  ListBlockDiligentManagerPage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: const Text("Chuyên Cần"),
        leading: BackButton(
          color: const Color.fromRGBO(245, 245, 245, 1),
          onPressed: () {
            Get.back();
          },
        ),
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        elevation: 0,
      ),
      body: Obx(() => Container(
        margin: EdgeInsets.fromLTRB(16.w, 4.h, 16.w, 4.h),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16.w),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: Colors.white,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(padding: EdgeInsets.only(top: 4.h)),
                  Text(
                    "Khối",
                    style: TextStyle(
                        color: const Color.fromRGBO(133, 133, 133, 1),
                        fontWeight: FontWeight.w500,
                        fontSize: 12.sp),
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 4.h)),
                  controller.blocks.isNotEmpty
                      ? ListView.builder(
                      itemCount: controller.blocks.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return Obx(() => InkWell(
                          onTap: () {
                            controller.gotoDetailListClass(index);
                          },
                          child: Column(
                            children: [
                              Card(
                                elevation: 1,
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                    BorderRadius.circular(6)),
                                child: Container(
                                  padding: const EdgeInsets.all(16),
                                  child: Row(
                                    children: [
                                      Text(
                                        "${controller.blocks[index].blockCategory?.name}",
                                        style: TextStyle(
                                            fontSize: 14.sp,
                                            color: const Color
                                                .fromRGBO(
                                                26, 26, 26, 1),
                                            fontWeight:
                                            FontWeight.bold),
                                      ),
                                      Expanded(child: Container()),
                                      const Icon(
                                        Icons.keyboard_arrow_right,
                                        color: Colors.black,
                                        size: 24,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ));
                      })
                      : Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    child: const Text("Không có dữ liệu"),
                  ),
                  Padding(padding: EdgeInsets.only(top: 16.h)),
                ],
              ),
            )
          ],
        ),
      )),
    );
  }
}
