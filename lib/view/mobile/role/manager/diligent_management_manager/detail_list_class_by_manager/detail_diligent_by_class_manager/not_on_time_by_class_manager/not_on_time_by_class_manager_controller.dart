import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/diligence.dart';
import '../../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../manager_home_controller.dart';
import '../detail_diligent_by_class_manager_controller.dart';


class NotOnTimeByClassManagerController extends GetxController{
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudentNotOnTime = <Diligents>[].obs;
  var listStudent= <Diligents>[].obs;
  var student = StudentDiligent().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var selectedDate = DateTime.now().obs;
  @override
  void onInit() {
    super.onInit();
  }



  getListStudentNotOnTime(){
    if(Get.isRegistered<DetailDiligentByClassManagerController>()){
      selectedDate.value = Get.find<DetailDiligentByClassManagerController>().selectedDay.value;
    }else{
      if(DateTime.now().month<=12&&DateTime.now().month>9){
        selectedDate.value = DateTime( Get.find<ManagerHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day).toLocal();
      }else{
        selectedDate.value = DateTime(Get.find<ManagerHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day).toLocal();
      }
    }
    var fromdate = DateTime(selectedDate.value.year, selectedDate.value.month, selectedDate.value.day,00,00).toLocal().millisecondsSinceEpoch;
    var todate = DateTime(selectedDate.value.year, selectedDate.value.month, selectedDate.value.day,23,59).toLocal().millisecondsSinceEpoch;

    _diligenceRepo.getListStudentDetailDiligence(Get.find<DetailDiligentByClassManagerController>().clazz.value.id, fromdate, todate, "").then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudent.value = diligenceClass.value.studentDiligent!;
        listStudentNotOnTime.value = listStudent.where((element) => element.statusDiligent == "NOT_ON_TIME").toList();
      }
    });
  }
}