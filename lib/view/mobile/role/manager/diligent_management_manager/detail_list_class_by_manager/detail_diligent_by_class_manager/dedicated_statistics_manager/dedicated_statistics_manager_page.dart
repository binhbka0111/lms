import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/constants/date_format.dart';
import 'package:slova_lms/commom/utils/date_time_picker.dart';
import 'package:slova_lms/commom/utils/time_utils.dart';
import 'package:slova_lms/data/model/common/contacts.dart';
import 'package:slova_lms/view/mobile/role/manager/diligent_management_manager/detail_list_class_by_manager/detail_diligent_by_class_manager/dedicated_statistics_manager/dedicated_statistics_manager_controller.dart';

import '../../../../../../../../commom/utils/app_utils.dart';

class DedicatedStatisticsManagerPage
    extends GetWidget<DedicatedStatisticsManagerController> {
  final controller = Get.put(DedicatedStatisticsManagerController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
          elevation: 0,
          title: Text(
            'Thống kê Chuyên Cần',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body: Obx(() => RefreshIndicator(
              child: SingleChildScrollView(
                  child: Container(
                color: const Color.fromRGBO(245, 245, 245, 1),
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(padding: EdgeInsets.only(top: 12.h)),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                      height: 60.h,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                              color: const Color.fromRGBO(192, 192, 192, 1)),
                          borderRadius: BorderRadius.circular(8)),
                      child: Row(
                        children: [
                          Expanded(
                              child: TextFormField(
                                style: TextStyle(
                                  fontSize: 12.0.sp,
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                ),
                                onTap: () {
                                  selectDateTimeStart(
                                      controller.controllerDateStart.value.text,
                                      DateTimeFormat.formatDateShort,
                                      context);
                                },
                                cursorColor:
                                    const Color.fromRGBO(248, 129, 37, 1),
                                controller: controller.controllerDateStart.value,
                                readOnly: true,
                                decoration: InputDecoration(
                                  suffixIcon: Container(
                                    margin: EdgeInsets.zero,
                                    child: SvgPicture.asset(
                                      "assets/images/icon_date_picker.svg",
                                      fit: BoxFit.scaleDown,
                                    ),
                                  ),
                                  labelText: "Từ thời gian",
                                  border: InputBorder.none,
                                  labelStyle: TextStyle(
                                      color:
                                          const Color.fromRGBO(177, 177, 177, 1),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500,
                                      fontFamily:
                                          'assets/font/static/Inter-Medium.ttf'),
                                ),
                              )),
                          Image.asset(
                            "assets/images/icon_To.png",
                            width: 16,
                            height: 16,
                          ),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(left: 16.w),
                              child: TextFormField(
                                style: TextStyle(
                                  fontSize: 12.0.sp,
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                ),
                                onTap: () {
                                  selectDateTimeEnd(
                                      controller.controllerDateEnd.value.text,
                                      DateTimeFormat.formatDateShort,
                                      context);
                                },
                                cursorColor:
                                    const Color.fromRGBO(248, 129, 37, 1),
                                controller: controller.controllerDateEnd.value,
                                readOnly: true,
                                decoration: InputDecoration(
                                  suffixIcon: Container(
                                    margin: EdgeInsets.zero,
                                    child: SvgPicture.asset(
                                      "assets/images/icon_date_picker.svg",
                                      fit: BoxFit.scaleDown,
                                    ),
                                  ),
                                  labelText: "Đến thời gian",
                                  border: InputBorder.none,
                                  labelStyle: TextStyle(
                                      color: const Color.fromRGBO(
                                          177, 177, 177, 1),
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w500,
                                      fontFamily:
                                          'assets/font/static/Inter-Medium.ttf'),
                                ),
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(left: 8.w)),
                        ],
                      ),
                    ),
                    const Padding(padding: EdgeInsets.only(top: 16)),
                    Obx(() => Container(
                          padding: EdgeInsets.symmetric(horizontal: 16.w),
                          height: 40,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color:
                                      const Color.fromRGBO(192, 192, 192, 1)),
                              borderRadius: BorderRadius.circular(8)),
                          child: Theme(
                            data: Theme.of(context).copyWith(
                              canvasColor: Colors.white,
                            ),
                            child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                              isExpanded: true,
                              iconSize: 0,
                              icon: const Visibility(
                                  visible: false,
                                  child: Icon(Icons.arrow_downward)),
                              elevation: 16,
                              hint: controller.nameStudentSelect.value != ""
                                  ? Row(
                                      children: [
                                        Text(
                                          controller.nameStudentSelect.value,
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w400,
                                              color: const Color.fromRGBO(
                                                  248, 129, 37, 1)),
                                        ),
                                        Expanded(child: Container()),
                                        const Icon(
                                          Icons.keyboard_arrow_down,
                                          color: Colors.black,
                                          size: 18,
                                        )
                                      ],
                                    )
                                  : Row(
                                      children: [
                                        Text(
                                          'Chọn học sinh',
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w400,
                                              color: Colors.black),
                                        ),
                                        Expanded(child: Container()),
                                        const Icon(
                                          Icons.keyboard_arrow_down,
                                          color: Colors.black,
                                          size: 18,
                                        )
                                      ],
                                    ),
                              items: controller.listStudentByClass.map(
                                (value) {
                                  return DropdownMenuItem<Student>(
                                    value: value,
                                    child: Text(
                                      value.fullName!,
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w400,
                                          color: const Color.fromRGBO(
                                              248, 129, 37, 1)),
                                    ),
                                  );
                                },
                              ).toList(),
                              onChanged: (Student? value) {
                                controller.nameStudentSelect.value = value!.fullName!;
                                controller.idStudentSelect.value = value.id!;
                                controller.getDiaryDiligenceTeacherClickDate(controller.idStudentSelect.value);
                              },
                            )),
                          ),
                        )),
                    Padding(padding: EdgeInsets.only(top: 12.h)),
                    Container(
                        child: controller.nameStudentSelect.value == "Tất cả"
                            ? ListView.builder(
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: controller.items.length,
                                shrinkWrap: true,
                                itemBuilder: (context, index) {
                                  return Container(
                                    margin: EdgeInsets.only(top: 12.h),
                                    padding: const EdgeInsets.all(12),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        color: const Color.fromRGBO(
                                            255, 255, 255, 1)),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          '${controller.items[index].fullName}',
                                          style: TextStyle(
                                              color: const Color.fromRGBO(
                                                  51, 157, 255, 1),
                                              fontSize: 14.sp,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        const Padding(
                                            padding: EdgeInsets.only(top: 12)),
                                        Row(
                                          children: [
                                            Text(
                                              "Đi học đúng giờ",
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 12.sp),
                                            ),
                                            const Spacer(),
                                            Text(
                                              "${controller.items[index].totalItemsOnTime} lần",
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      26, 26, 26, 1),
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                        const Padding(
                                            padding: EdgeInsets.only(top: 12)),
                                        Row(
                                          children: [
                                            Text(
                                              "Đi học muộn",
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 12.sp),
                                            ),
                                            Expanded(child: Container()),
                                            Text(
                                              "${controller.items[index].totalItemsNotOnTime} lần",
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      26, 26, 26, 1),
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                        const Padding(
                                            padding: EdgeInsets.only(top: 12)),
                                        Row(
                                          children: [
                                            Text(
                                              "Nghỉ học có phép",
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 12.sp),
                                            ),
                                            Expanded(child: Container()),
                                            Text(
                                              "${controller.items[index].totalStudentsExcusedAbsence} lần",
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      26, 26, 26, 1),
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                        const Padding(
                                            padding: EdgeInsets.only(top: 12)),
                                        Row(
                                          children: [
                                            Text(
                                              "Nghỉ học không phép",
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 12.sp),
                                            ),
                                            Expanded(child: Container()),
                                            Text(
                                              "${controller.items[index].totalStudentsAbsentWithoutLeave} lần",
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      26, 26, 26, 1),
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                        const Padding(
                                            padding: EdgeInsets.only(top: 12)),
                                      ],
                                    ),
                                  );
                                })
                            : ListView.builder(
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount:
                                    controller.selectStudent.length,
                                shrinkWrap: true,
                                itemBuilder: (context, index) {
                                  return Container(
                                    margin: EdgeInsets.only(top: 12.h),
                                    padding: const EdgeInsets.all(12),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        color: const Color.fromRGBO(
                                            255, 255, 255, 1)),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          '${controller.selectStudent[index].fullName}',
                                          style: const TextStyle(
                                              color: Color.fromRGBO(
                                                  51, 157, 255, 1),
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        const Padding(
                                            padding: EdgeInsets.only(top: 12)),
                                        Row(
                                          children: [
                                            const Text(
                                              "Đi học đúng giờ",
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 12),
                                            ),
                                            Expanded(child: Container()),
                                            Text(
                                              "${controller.selectStudent[index].totalItemsOnTime} lần",
                                              style: const TextStyle(
                                                  color: Color.fromRGBO(
                                                      26, 26, 26, 1),
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                        const Padding(
                                            padding: EdgeInsets.only(top: 12)),
                                        Row(
                                          children: [
                                            const Text(
                                              "Đi học muộn",
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 12),
                                            ),
                                            Expanded(child: Container()),
                                            Text(
                                              "${controller.selectStudent[index].totalItemsNotOnTime} lần",
                                              style: const TextStyle(
                                                  color: Color.fromRGBO(
                                                      26, 26, 26, 1),
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                        const Padding(
                                            padding: EdgeInsets.only(top: 12)),
                                        Row(
                                          children: [
                                            const Text(
                                              "Nghỉ học có phép",
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 12),
                                            ),
                                            Expanded(child: Container()),
                                            Text(
                                              "${controller.selectStudent[index].totalStudentsExcusedAbsence} lần",
                                              style: const TextStyle(
                                                  color: Color.fromRGBO(
                                                      26, 26, 26, 1),
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                        const Padding(
                                            padding: EdgeInsets.only(top: 12)),
                                        Row(
                                          children: [
                                            const Text(
                                              "Nghỉ học không phép",
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      133, 133, 133, 1),
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 12),
                                            ),
                                            Expanded(child: Container()),
                                            Text(
                                              "${controller.selectStudent[index].totalStudentsAbsentWithoutLeave} lần",
                                              style: const TextStyle(
                                                  color: Color.fromRGBO(
                                                      26, 26, 26, 1),
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                        const Padding(
                                            padding: EdgeInsets.only(top: 12)),
                                      ],
                                    ),
                                  );
                                }))
                  ],
                ),
              )),
              onRefresh: () async {
                controller.onRefresh();
              },
            )));
  }

  selectDateTimeStart(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
            initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy')
                .parse(controller.controllerDateEnd.value.text)
                .millisecondsSinceEpoch) {
          controller.controllerDateStart.value.text = date;
          controller.getDiaryDiligenceTeacherClickDate(controller.idStudentSelect.value);
          controller.selectStudent.refresh();
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian bắt đầu nhỏ hơn thời gian kết thúc");
        }
      } else {
        controller.controllerDateStart.value.text = date;
        controller.getDiaryDiligenceTeacherClickDate(controller.idStudentSelect.value);
        controller.selectStudent.refresh();
      }
    }
  }

  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
            initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy')
                .parse(controller.controllerDateStart.value.text)
                .millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch) {
          controller.controllerDateEnd.value.text = date;
          controller.getDiaryDiligenceTeacherClickDate(controller.idStudentSelect.value);
          controller.selectStudent.refresh();
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian kết thúc lớn hơn thời gian bắt đầu");
        }
      } else {
        controller.controllerDateEnd.value.text = date;
        controller.getDiaryDiligenceTeacherClickDate(controller.idStudentSelect.value);
        controller.selectStudent.refresh();
      }
    }
  }
}
