import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/model/common/diligence.dart';
import 'package:slova_lms/data/repository/diligence/diligence_repo.dart';
import 'package:slova_lms/view/mobile/role/manager/manager_home_controller.dart';
import '../detail_diligent_by_class_manager_controller.dart';

class DiaryOfDiligenceManagerController extends GetxController{
  final category = ''.obs;
  var focusdateStart = FocusNode().obs;
  var controllerdateStart = TextEditingController().obs;
  var focusdateEnd = FocusNode().obs;
  var controllerdateEnd = TextEditingController().obs;
  var outputDateToDateFormat = DateFormat('dd/MM/yyyy');
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiaryDiligenceTeacher().obs;
  RxList<ItemDiaryDiligence> items = <ItemDiaryDiligence>[].obs;
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;


  @override
  void onInit() {
    super.onInit();
    setDateSchoolYears();
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      controllerdateStart.value.text =   outputDateToDateFormat.format(DateTime(Get.find<ManagerHomeController>().fromYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerdateEnd.value.text = outputDateToDateFormat.format(DateTime(Get.find<ManagerHomeController>().fromYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));

    }else{
      controllerdateStart.value.text =  outputDateToDateFormat.format(DateTime(Get.find<ManagerHomeController>().toYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      controllerdateEnd.value.text = outputDateToDateFormat.format(DateTime(Get.find<ManagerHomeController>().toYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));

    }
  }

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }

  getDiaryDiligenceTeacherClickDate(){
    var classId = Get.find<DetailDiligentByClassManagerController>().clazz.value.id;
    var fromdate = DateTime(int.parse(controllerdateStart.value.text.substring(6,10)), int.parse(controllerdateStart.value.text.substring(3,5)),int.parse(controllerdateStart.value.text.substring(0,2))).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(controllerdateEnd.value.text.substring(6,10)), int.parse(controllerdateEnd.value.text.substring(3,5)),int.parse(controllerdateEnd.value.text.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.getListDiaryDiligenceTeacher(classId, fromdate, todate).then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value= value.object!;
        items.value = diligenceClass.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  getDiaryDiligenceTeacherToday(){
    var classId = Get.find<DetailDiligentByClassManagerController>().clazz.value.id;
    var todate = DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var fromdate = DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.getListDiaryDiligenceTeacher(classId, fromdate, todate).then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value= value.object!;
        items.value = diligenceClass.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<ManagerHomeController>().fromYearPresent.value == DateTime.now().year) {
        getDiaryDiligenceTeacherToday();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ManagerHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<ManagerHomeController>().fromYearPresent}";
        getDiaryDiligenceTeacherToYear(fromYearSelect.value, toYearSelect.value);
      }
    } else {
      if (Get.find<ManagerHomeController>().toYearPresent.value == DateTime.now().year) {
        getDiaryDiligenceTeacherToday();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ManagerHomeController>().toYearPresent}";
        toYearSelect.value = "${Get.find<ManagerHomeController>().toYearPresent}";
        getDiaryDiligenceTeacherToYear(fromYearSelect.value, toYearSelect.value);
      }
    }
  }

  getDiaryDiligenceTeacherToYear(fromdate,todate){
    var classId = Get.find<DetailDiligentByClassManagerController>().clazz.value.id;
    var fromdate = DateTime(int.parse(toYearSelect.value.substring(0,4)), findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.getListDiaryDiligenceTeacher(classId, fromdate, todate).then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value= value.object!;
        items.value = diligenceClass.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  onRefresh(){
    items.clear();
    getDiaryDiligenceTeacherToday();
    items.refresh();
  }

}