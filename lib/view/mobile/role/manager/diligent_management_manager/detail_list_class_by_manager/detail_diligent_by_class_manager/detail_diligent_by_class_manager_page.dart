import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/view/mobile/role/manager/diligent_management_manager/detail_list_class_by_manager/detail_diligent_by_class_manager/dedicated_statistics_manager/dedicated_statistics_manager_page.dart';
import 'package:slova_lms/view/mobile/role/manager/diligent_management_manager/detail_list_class_by_manager/detail_diligent_by_class_manager/diary_of_diligence_manager/diary_of_diligence_manager_page.dart';
import 'absent_without_leave_by_class_manager/absent_without_leave_by_class_manager_controller.dart';
import 'detail_diligent_by_class_manager_controller.dart';
import 'excused_absence_by_class_manager/excused_absence_by_class_manager_controller.dart';
import 'not_on_time_by_class_manager/not_on_time_by_class_manager_controller.dart';
import 'on_time_by_class_manager/on_time_by_class_manager_controller.dart';

class DetailDiligentByClassManagerPage extends GetWidget<DetailDiligentByClassManagerController>{
  @override
  final controller = Get.put(DetailDiligentByClassManagerController());

  DetailDiligentByClassManagerPage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(248, 129, 37, 1),
        elevation: 0,
        actions: [
          PopupMenuButton(
              icon: const Icon(Icons.more_vert),
              padding:
              EdgeInsets
                  .zero,
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.all(Radius.circular(6.0
                      .r))),
              itemBuilder: (context) {
                return controller.getListPopupMenuItem();
              }, onSelected: (value) {
            if(value== 0){
              Get.to(DedicatedStatisticsManagerPage());
            }else if(value== 1){
              Get.to(DiaryOfDiligenceManagerPage());
            }
          }),
          Padding(padding: EdgeInsets.only(right: 8.w))
        ],
        title: Text(
          'Chuyên Cần ${controller.clazz.value.classCategory?.name}',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() =>
          Column(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(16.w, 8.h, 16.w, 8.h),
            padding: EdgeInsets.all(16.h),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(6)),
            child:
            Row(
              children: [
                Text(
                  DateFormat('d').format(controller.selectedDay.value),
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontSize: 44.sp),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 4.w),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Thứ ${controller.selectedDay.value.weekday+1}",
                      style: TextStyle(
                          color: const Color.fromRGBO(133, 133, 133, 1),
                          fontWeight: FontWeight.w500,
                          fontSize: 14.sp),
                    ),
                    Text(
                      "Tháng ${controller.selectedDay.value.month.toString()}, ${controller.getTextYear()}",
                      style: TextStyle(
                          color: const Color.fromRGBO(133, 133, 133, 1),
                          fontWeight: FontWeight.w500,
                          fontSize: 14.sp),
                    ),
                  ],
                ),
                Expanded(child: Container()),
                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    padding:
                    EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(
                            248, 129, 37, 1),
                        borderRadius: BorderRadius.circular(6.r)),
                    child: SizedBox(
                      height: 20.h,
                      width: 20.h,
                      child: InkWell(
                        onTap: () {
                          showModalBottomSheet(
                              context: context,
                              builder: (context) {
                                return Wrap(
                                  children: [
                                    Row(
                                      children: [
                                        TextButton(
                                            onPressed: () {
                                              Get.back();
                                            },
                                            style: ElevatedButton.styleFrom(
                                                backgroundColor: Colors.white),
                                            child: const Text(
                                              "Hủy",
                                              style: TextStyle(
                                                  color:
                                                  Color.fromRGBO(123, 123, 123, 1)),
                                            )),
                                        Expanded(child: Container()),
                                        TextButton(
                                            onPressed: () {
                                              controller.selectedDay.value = controller.cupertinoDatePicker.value;
                                              controller.getListStudent();
                                              Get.find<OnTimeByClassManagerController>().getListStudentOntime();
                                              Get.find<NotOnTimeByClassManagerController>().getListStudentNotOnTime();
                                              Get.find<ExcusedAbsenceByClassManagerController>().getListStudentExcusedAbsence();
                                              Get.find<AbsentWithoutLeaveByClassManagerController>().getListStudentAbsentWithoutLeave();
                                              Get.back();
                                            },
                                            style: ElevatedButton.styleFrom(
                                                backgroundColor: Colors.white),
                                            child: const Text(
                                              "Xong",
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      248, 129, 37, 1)),
                                            )),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 300, // Just as an example
                                      child: CupertinoDatePicker(
                                        mode: CupertinoDatePickerMode.date,
                                        initialDateTime: controller.selectedDay.value,
                                        onDateTimeChanged: (DateTime dateTime) {
                                          controller.cupertinoDatePicker.value = dateTime;
                                        },
                                      ),
                                    ),
                                  ],
                                );
                              });
                        },
                        child: Image.asset("assets/images/icon_date_picker_detail.png"),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 16.w),
            height: 40,
            child: ListView.builder(
                itemCount: controller.listStatus.length,
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context,index){
                  return Obx(() => TextButton(
                      onPressed: () {
                        controller.indexClick.value = index;
                        controller.showColor(controller.indexClick.value);
                        controller.pageController.animateToPage(index,
                            duration: const Duration(seconds: 1),
                            curve: Curves.easeOutBack);
                      },
                      child: Text(
                        "${controller.listStatus[index]} (${controller.getListStatus(index) ?? 0})",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 12.sp,
                            color: controller.indexClick.value == index
                                ? const Color.fromRGBO(248, 129, 37, 1)
                                : const Color.fromRGBO(177, 177, 177, 1)),
                      )));
                }),
          ),
          Expanded(
              child: PageView(
                  onPageChanged: (value) {
                    controller.onPageViewChange(value);
                  },
                  controller: controller.pageController,
                  physics: const ScrollPhysics(),
                  children: controller.listPage
              )),
        ],
      )
      ),
    );
  }

}