import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'absent_without_leave_by_class_manager_controller.dart';

class AbsentWithoutLeaveByClassManagerPage
    extends GetView<AbsentWithoutLeaveByClassManagerController> {
  @override
  final controller = Get.put(AbsentWithoutLeaveByClassManagerController());

  AbsentWithoutLeaveByClassManagerPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
        child: Obx(() => Container(
              margin: EdgeInsets.only(left: 16.w, right: 16.w),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: Colors.white,
              ),
              child: ListView.builder(
                  itemCount:
                      controller.listStudentAbsentWithoutLeave.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {},
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 16.w, right: 8.w),
                                child: Text(
                                  '${index + 1}',
                                  style: const TextStyle(
                                      color: Color.fromRGBO(26, 26, 26, 1),
                                      fontSize: 14),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 8.h, bottom: 4.h),
                                child: Row(
                                  children: [
                                    SizedBox(
                                      width: 48.h,
                                      height: 48.h,
                                      child:
                                      CacheNetWorkCustom(urlImage: '${ controller
                                              .listStudentAbsentWithoutLeave[index]
                                              .student!
                                              .image }')

                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(right: 4.w)),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        RichText(
                                            text: TextSpan(children: [
                                           TextSpan(
                                              text: 'Học Sinh: ',
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      248, 129, 37, 1),
                                                  fontSize: 14.sp)),
                                          TextSpan(
                                              text:
                                                  '${controller.listStudentAbsentWithoutLeave[index].student?.fullName}',
                                              style: TextStyle(
                                                  color: const Color.fromRGBO(
                                                      26, 26, 26, 1),
                                                  fontSize: 14.sp)),
                                        ])),
                                        Padding(
                                            padding: EdgeInsets.only(top: 4.h)),
                                        Visibility(
                                          visible: controller
                                              .listStudentAbsentWithoutLeave[index]
                                              .student!
                                              .birthday!=null,
                                            child: Text(
                                          controller.outputDateFormat.format(
                                              DateTime.fromMillisecondsSinceEpoch(
                                                  controller
                                                      .listStudentAbsentWithoutLeave[index]
                                                      .student!
                                                      .birthday ??
                                                      0)),
                                          style: const TextStyle(
                                              color: Color.fromRGBO(
                                                  133, 133, 133, 1),
                                              fontSize: 14),
                                        ))
                                      ],
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                          const Divider()
                        ],
                      ),
                    );
                  }),
            )));
  }
}
