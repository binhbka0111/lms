import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/model/common/contacts.dart';
import 'package:slova_lms/data/model/common/diligence.dart';
import 'package:slova_lms/data/repository/diligence/diligence_repo.dart';
import 'package:slova_lms/view/mobile/role/manager/manager_home_controller.dart';
import '../detail_diligent_by_class_manager_controller.dart';


class DedicatedStatisticsManagerController extends GetxController{
  final nameStudentSelect = ''.obs;
  final idStudentSelect = ''.obs;
  var focusDateStart = FocusNode().obs;
  var controllerDateStart = TextEditingController().obs;
  var focusDateEnd = FocusNode().obs;
  var controllerDateEnd = TextEditingController().obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var dedicatedStatistical = DedicatedStatisticalTeacher().obs;
  RxList<ItemsDedicatedStatistical> items = <ItemsDedicatedStatistical>[].obs;
  RxList<ItemsDedicatedStatistical> selectStudent = <ItemsDedicatedStatistical>[].obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;
  var listStudentByClass = <Student>[].obs;
  var detailStudent = Student().obs;

  @override
  void onInit() {
    super.onInit();
    nameStudentSelect.value = "Tất cả";
    setDateSchoolYears();
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      controllerDateStart.value.text =   outputDateFormat.format(DateTime(Get.find<ManagerHomeController>().fromYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<ManagerHomeController>().fromYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));

    }else{
      controllerDateStart.value.text =   outputDateFormat.format(DateTime(Get.find<ManagerHomeController>().toYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<ManagerHomeController>().toYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));

    }


  }

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }
  getDiaryDiligenceTeacherToday(){
    listStudentByClass.value = [];
    listStudentByClass.add(Student(id: "",fullName: "Tất cả"));
    var classId = Get.find<DetailDiligentByClassManagerController>().clazz.value.id;
    var todate = DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var fromdate = DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.getListDedicatedStatisticalTeacher(classId, fromdate, todate).then((value) {
      if (value.state == Status.SUCCESS) {
        dedicatedStatistical.value= value.object!;
        items.value = dedicatedStatistical.value.items!;
        for(int i =0;i< items.length ;i++){
          listStudentByClass.add(Student(id: "",fullName: ""));
          listStudentByClass[i+1].fullName = items[i].fullName!;
          listStudentByClass[i+1].id = items[i].id!;
        }
        listStudentByClass.refresh();
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  getDiaryDiligenceTeacherClickDate(studentId){
    listStudentByClass.value = [];
    listStudentByClass.add(Student(id: "",fullName: "Tất cả"));
    var classId = Get.find<DetailDiligentByClassManagerController>().clazz.value.id;
    var fromdate = DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2))).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.getListDedicatedStatisticalTeacher(classId, fromdate, todate).then((value) {
      if (value.state == Status.SUCCESS) {
        dedicatedStatistical.value= value.object!;
        items.value = dedicatedStatistical.value.items!;
        for(int i =0;i< items.length ;i++){
          listStudentByClass.add(Student(id: "",fullName: ""));
          listStudentByClass[i+1].fullName = items[i].fullName!;
          listStudentByClass[i+1].id = items[i].id!;
        }
        listStudentByClass.refresh();
        selectStudent.value = items.where((element) => element.id == studentId).toList();
        items.refresh();
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }


  getDiaryDiligenceTeacherToYear(fromdate,todate){
    var classId = Get.find<DetailDiligentByClassManagerController>().clazz.value.id;
    var fromdate = DateTime(int.parse(toYearSelect.value.substring(0,4)), findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.getListDedicatedStatisticalTeacher(classId, fromdate, todate).then((value) {
      if (value.state == Status.SUCCESS) {
        dedicatedStatistical.value= value.object!;
        items.value = dedicatedStatistical.value.items!;
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }

  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<ManagerHomeController>().fromYearPresent.value == DateTime.now().year) {
        getDiaryDiligenceTeacherToday();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ManagerHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<ManagerHomeController>().fromYearPresent}";
        getDiaryDiligenceTeacherToYear(fromYearSelect.value, toYearSelect.value);

      }
    } else {
      if (Get.find<ManagerHomeController>().toYearPresent.value == DateTime.now().year) {
        getDiaryDiligenceTeacherToday();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ManagerHomeController>().toYearPresent}";
        toYearSelect.value = "${Get.find<ManagerHomeController>().toYearPresent}";
        getDiaryDiligenceTeacherToYear(fromYearSelect.value, toYearSelect.value);

      }
    }
  }

  onRefresh(){
    items.clear();
    getDiaryDiligenceTeacherClickDate("");
    items.refresh();
  }



}