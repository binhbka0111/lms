import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../../data/model/common/diligence.dart';
import '../../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../manager_home_controller.dart';
import '../detail_diligent_by_class_manager_controller.dart';



class AbsentWithoutLeaveByClassManagerController extends GetxController{
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudentAbsentWithoutLeave = <Diligents>[].obs;
  var listStudent = <Diligents>[].obs;
  var student = StudentDiligent().obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var selectedDate = DateTime.now().obs;
  @override
  void onInit() {
    super.onInit();
  }



  getListStudentAbsentWithoutLeave(){
    if(Get.isRegistered<DetailDiligentByClassManagerController>()){
      selectedDate.value = Get.find<DetailDiligentByClassManagerController>().selectedDay.value;
    }else{
      if(DateTime.now().month<=12&&DateTime.now().month>9){
        selectedDate.value = DateTime( Get.find<ManagerHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day).toLocal();
      }else{
        selectedDate.value = DateTime(Get.find<ManagerHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day).toLocal();
      }

    }
    var fromDate = DateTime(selectedDate.value.year, selectedDate.value.month, selectedDate.value.day,00,00).toLocal().millisecondsSinceEpoch;
    var toDate = DateTime(selectedDate.value.year, selectedDate.value.month, selectedDate.value.day,23,59).toLocal().millisecondsSinceEpoch;


    _diligenceRepo.getListStudentDetailDiligence(Get.find<DetailDiligentByClassManagerController>().clazz.value.id, fromDate, toDate, "").then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudent.value = diligenceClass.value.studentDiligent!;
        listStudentAbsentWithoutLeave.value = listStudent.where((element) => element.statusDiligent == "ABSENT_WITHOUT_LEAVE").toList();
      }
    });
  }
}