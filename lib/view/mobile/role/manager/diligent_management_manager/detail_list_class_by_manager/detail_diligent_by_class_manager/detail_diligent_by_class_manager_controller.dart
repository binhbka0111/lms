import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/repository/diligence/diligence_repo.dart';
import 'package:slova_lms/view/mobile/role/manager/manager_home_controller.dart';
import '../../../../../../../data/model/common/diligence.dart';
import '../../../../../../../data/model/res/class/block.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'on_time_by_class_manager/on_time_by_class_manager_page.dart';
import 'excused_absence_by_class_manager/excused_absence_by_class_manager_page.dart';
import 'not_on_time_by_class_manager/not_on_time_by_class_manager_page.dart';
import 'absent_without_leave_by_class_manager/absent_without_leave_by_class_manager_page.dart';
import '../../../../../home/home_controller.dart';

class DetailDiligentByClassManagerController extends GetxController{
  DateTime now = DateTime.now();
  RxInt indexClick = 0.obs;
  RxList<bool> click = <bool>[].obs;
  var cupertinoDatePicker = DateTime.now().obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var diligenceClass = DiligenceClass().obs;
  var listStudent = <Diligents>[].obs;
  var student = StudentDiligent().obs;
  RxList<StudentDiligent> studentDiligence = <StudentDiligent>[].obs;
  var classCategoryId = "".obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var clazz = Class().obs;
  var listStudentOnTime = <Diligents>[].obs;
  var listStudentNotOnTime = <Diligents>[].obs;
  var listStudentExcusedAbsence = <Diligents>[].obs;
  var listStudentAbsentWithoutLeave = <Diligents>[].obs;
  var selectedDay = DateTime.now().obs;
  var listStatus = <String>[].obs;
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;

  var listPage = <Widget>[];

  var listItemPopupMenu = <PopupMenuItem>[];

  var heightStatisticalDiligent= 30.0;


  var showStatisticalDiligent = true;


  var indexStatisticalDiligent = 0;


  var onTime = OnTimeByClassManagerPage();
  var notOnTimePage = NotOnTimeByClassManagerPage();
  var excusedAbsencePage = ExcusedAbsenceByClassManagerPage();
  var absentWithoutLeavePage = AbsentWithoutLeaveByClassManagerPage();


  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  @override
  void onInit() {
    var data = Get.arguments;
    if(data!= null){
      clazz.value = data;
    }
    if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_ON_TIME_LIST)){
      listStatus.add("Đi học đúng giờ");
      listPage.add(onTime);
    }
    if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_NOT_ON_TIME_LIST)){
      listStatus.add("Đi học muộn");
      listPage.add(notOnTimePage);
    }
    if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_EXCUSED_ABSENCE_LIST)){
      listStatus.add("Nghỉ có phép");
      listPage.add(excusedAbsencePage);
    }
    if(checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_ABSENT_WITHOUT_LIST)){
      listStatus.add("Nghỉ không phép");
      listPage.add(absentWithoutLeavePage);
    }


    if(DateTime.now().month<=12&&DateTime.now().month>9){
     selectedDay.value = DateTime( Get.find<ManagerHomeController>().fromYearPresent.value,DateTime.now().month,DateTime.now().day).toLocal();
    }else{
      selectedDay.value = DateTime(Get.find<ManagerHomeController>().toYearPresent.value,DateTime.now().month,DateTime.now().day).toLocal();
    }

    listStudent.clear();
    listStudentOnTime.clear();
    listStudentNotOnTime.clear();
    listStudentExcusedAbsence.clear();
    listStudentAbsentWithoutLeave.clear();
    setDateSchoolYears();
    listStudent.refresh();
    listStudentOnTime.refresh();
    listStudentNotOnTime.refresh();
    listStudentExcusedAbsence.refresh();
    listStudentAbsentWithoutLeave.refresh();
    getListPopupMenuItem();
    super.onInit();
  }

  @override
  dispose() {
    pageController.dispose();
    super.dispose();
  }

  getListPopupMenuItem(){
    listItemPopupMenu = [];
    var statisticalDiligent =  PopupMenuItem<int>(
        value: 0,
        padding: EdgeInsets.zero,
        height: heightStatisticalDiligent,
        child: showStatisticalDiligent
            ?Container(
          padding: EdgeInsets.symmetric(vertical: 4.h),
          decoration:  const BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black26))),
          child: const Center(
            child: Text("Thống kê chuyên cần"),
          ),
        )
            :const Center(
          child: Text("Thống kê chuyên cần"),
        )
    );
    var diaryDiligent =   const PopupMenuItem<int>(
      value: 1,
      padding: EdgeInsets.zero,
      height: 30,
      child: Center(child: Text("Nhật ký chuyên cần"),),
    );


    if(checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_STATISTICAL_DILIGENCE)){
      listItemPopupMenu.add(statisticalDiligent);
    }
    if(checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_DIARY_DILIGENCE)){
      listItemPopupMenu.add(diaryDiligent);
    }

    indexStatisticalDiligent = listItemPopupMenu.indexOf(statisticalDiligent);



    showStatisticalDiligent = setVisiblePopupMenuItem(indexStatisticalDiligent,listItemPopupMenu);


    heightStatisticalDiligent = setHeightPopupMenuItem(indexStatisticalDiligent,listItemPopupMenu);


    update();

    return listItemPopupMenu;
  }

  getListStatus(index){
    switch(index){
      case 0:
        return listStudentOnTime.length;
      case 1:
        return listStudentNotOnTime.length;
      case 2:
        return listStudentExcusedAbsence.length;
      case 3:
        return listStudentAbsentWithoutLeave.length;
    }
  }


  onPageViewChange(int page) {
    indexClick.value = page;
    if(page != 0) {
      indexClick.value-1;
    } else {
      indexClick.value = 0;
    }

  }


  showColor(index) {
    click.value = [];
    for (int i = 0; i < 4; i++) {
      click.add(false);
    }
    click[index] = true;
  }
  getListStudent(){
    var classId = clazz.value.id;
    var fromDate = DateTime(selectedDay.value.year, selectedDay.value.month, selectedDay.value.day,00,00).millisecondsSinceEpoch;
    var toDate = DateTime(selectedDay.value.year, selectedDay.value.month, selectedDay.value.day,23,59).millisecondsSinceEpoch;
    _diligenceRepo.getListStudentDetailDiligence(classId, fromDate, toDate, "").then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudent.value = diligenceClass.value.studentDiligent!;
        listStudentOnTime.value = listStudent.where((element) => element.statusDiligent == "ON_TIME").toList();
        listStudentNotOnTime.value = listStudent.where((element) => element.statusDiligent == "NOT_ON_TIME").toList();
        listStudentExcusedAbsence.value = listStudent.where((element) => element.statusDiligent == "EXCUSED_ABSENCE").toList();
        listStudentAbsentWithoutLeave.value = listStudent.where((element) => element.statusDiligent == "ABSENT_WITHOUT_LEAVE").toList();
      }
    });
  }
  getListStudentToYear(fromDate, toDate){
    var classId = clazz.value.id;
    var fromDate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), selectedDay.value.month,selectedDay.value.day).millisecondsSinceEpoch;
    var toDate = DateTime(int.parse(toYearSelect.value.substring(0,4)),selectedDay.value.month,selectedDay.value.day).millisecondsSinceEpoch;
    _diligenceRepo.getListStudentDiligence(classId, fromDate, toDate, "").then((value) {
      if (value.state == Status.SUCCESS) {
        diligenceClass.value = value.object!;
        listStudent.value = diligenceClass.value.studentDiligent!;
        listStudentOnTime.value = listStudent.where((element) => element.statusDiligent == "ON_TIME").toList();
        listStudentNotOnTime.value = listStudent.where((element) => element.statusDiligent == "NOT_ON_TIME").toList();
        listStudentExcusedAbsence.value = listStudent.where((element) => element.statusDiligent == "EXCUSED_ABSENCE").toList();
        listStudentAbsentWithoutLeave.value = listStudent.where((element) => element.statusDiligent == "ABSENT_WITHOUT_LEAVE").toList();
      }
    });
  }
  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<ManagerHomeController>().fromYearPresent.value == DateTime.now().year) {
        getListStudent();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ManagerHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<ManagerHomeController>().toYearPresent}";
        getListStudentToYear(fromYearSelect.value, toYearSelect.value);
      }
    } else {
      if (Get.find<ManagerHomeController>().toYearPresent.value == DateTime.now().year) {
        getListStudent();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ManagerHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<ManagerHomeController>().toYearPresent}";
        getListStudentToYear(fromYearSelect.value, toYearSelect.value);
      }
    }
  }

  getTextYear(){
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<ManagerHomeController>().fromYearPresent.value == DateTime.now().year) {
        return DateTime.now().year.toString();
      } else {
        return Get.find<ManagerHomeController>().toYearPresent.toString();
      }
    } else {
      if (Get.find<ManagerHomeController>().toYearPresent.value == DateTime.now().year) {
        return DateTime.now().year.toString();
      } else {
        return Get.find<ManagerHomeController>().toYearPresent.toString();
      }
    }
  }


}