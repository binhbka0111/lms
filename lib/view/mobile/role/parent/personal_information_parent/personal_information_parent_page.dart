import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/commom/widget/loading_custom.dart';
import 'package:slova_lms/view/mobile/role/parent/personal_information_parent/personal_information_parent_controller.dart';

import '../../../../../commom/utils/color_utils.dart';
import '../../../../../commom/utils/file_device.dart';
import '../../../../../commom/widget/custom_view.dart';
import '../../../../../commom/widget/logout.dart';
//ignore: must_be_immutable
class PersonalInformationParentPage extends GetView<PersonalInformationParentController> {

  @override
  final controller = Get.put(PersonalInformationParentController());

  PersonalInformationParentPage({super.key});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: const Text("Chi tiết"),
          leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Get.delete<PersonalInformationParentController>();
              Get.back();
            },
          ),
          backgroundColor: ColorUtils.PRIMARY_COLOR,
        ),
        backgroundColor: const Color.fromRGBO(249, 249, 249, 1),
        body: Obx(() => controller.isReady.value?Center(
          child: Container(
            margin:  EdgeInsets.only(top: 16.h, left: 16.w, right: 16.w),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  profileAvatarWidget(),
                  informationWidget(),
                  help(),
                ],
              ),
            ),
          ),
        ):const LoadingCustom()));
  }

  var styleValue = TextStyle(
      color: const Color.fromRGBO(26, 26, 26, 1),
      fontSize: 14.sp,
      fontFamily: 'static/Inter-Medium.ttf',
      fontWeight: FontWeight.w500);
  var styleTitle = TextStyle(
      color: const Color.fromRGBO(133, 133, 133, 1),
      fontSize: 12.sp,
      fontFamily: 'static/Inter-Medium.ttf',
      fontWeight: FontWeight.w500);
  var styleTitleHelp = TextStyle(
      color: const Color.fromRGBO(24, 29, 39, 1),
      fontSize: 14.sp,
      fontWeight: FontWeight.w500,
      fontFamily: 'static/Inter-Medium.ttf');

  profileAvatarWidget() {
    return Obx(() => Container(
      width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6.r),
            color: const Color.fromRGBO(255, 255, 255, 1),
          ),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 10.h),
                child: Stack(
                  children: [
                    InkWell(
                      onTap: () {
                        controller.goToDetailAvatar();
                      },
                      child: SizedBox(
                        width: 80.h,
                        height: 80.h,
                        child:
                        CacheNetWorkCustom(urlImage:'${controller.userProfile.value.image}' ,)

                      ),
                    ),
                    Positioned(
                        right: 0,
                        bottom: 0,
                        child: InkWell(
                          onTap: () {
                            pickerImage();
                          },
                          child: Image.asset(
                              'assets/images/img_cam.png',width: 24.h,height: 24.h,),
                        ))
                  ],
                ),
              ),
              SizedBox(
                height: 8.h,
              ),
              Center(
                child: Text(
                  controller.userProfile.value.fullName ?? "",
                  style: TextStyle(
                      color: const Color.fromRGBO(26, 26, 26, 1),
                      fontSize: 18.sp,
                      fontFamily: 'static/Inter-Medium.ttf',
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 8.h,
              ),
              Text(
                controller.userProfile.value.email ?? "",
                style: TextStyle(
                  color: const Color.fromRGBO(133, 133, 133, 1),
                  fontSize: 10.sp,
                ),
              ),
              SizedBox(
                height: 16.h,
              ),
            ],
          ),
        ));
  }

  informationWidget() {
    return Obx(() => Container(
          margin: EdgeInsets.only(top: 16.h),
          padding: EdgeInsets.only(left: 6.w, right: 6.w),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            color: const Color.fromRGBO(255, 255, 255, 1),
          ),
          child: Column(
            children: [
              SizedBox(height: 8.h),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 8),
                child:         Row(
                  children: [
                    Text(
                      'Thông Tin Cá Nhân',
                      style: styleTitle,
                    ),
                    const Spacer(),
                    InkWell(
                      onTap: () {
                        controller.goToUpdateInfoUser();
                      },
                      child:
                      SizedBox(
                        child: Row(
                          children: [
                            Text('Chỉnh sửa',
                                style: TextStyle(
                                    color: ColorUtils.PRIMARY_COLOR,
                                    fontSize: 12.sp,
                                    fontFamily: 'static/Inter-Regular.ttf')),
                            const SizedBox(
                              width: 8,
                            ),
                            Icon(
                              Icons.edit_outlined,
                              color: ColorUtils.PRIMARY_COLOR,
                              size: 16.sp,
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 16.h,),
              Container(
                  margin: const EdgeInsets.all(9),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            'Họ và tên: ',
                            style: styleTitle,
                          ),
                          Expanded(child: Container()),
                          SizedBox(
                              width: 200.w,
                              child:Text(
                                controller.userProfile.value.fullName ?? "",
                                style: styleValue,
                                textAlign: TextAlign.right,
                              )
                          ),
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 10.h)),
                      Row(
                        children: [
                          Text(
                            'Số điện thoại: ',
                            style: styleTitle,
                          ),
                          Expanded(child: Container()),
                          SizedBox(
                              width: 200.w,
                              child:Text(
                                controller.userProfile.value.phone ?? "",
                                style: styleValue,
                                textAlign: TextAlign.right,
                              )
                          ),

                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 10.h)),
                      Row(
                        children: [
                          Text(
                            'Email: ',
                            style: styleTitle,
                          ),

                          Expanded(child: Container()),
                          SizedBox(
                              width: 200.w,
                              child:Text(
                                controller.userProfile.value.email ?? "",
                                style: styleValue,
                                textAlign: TextAlign.right,
                              )
                          ),

                        ],
                      ),
                      Padding(padding: EdgeInsets.only(top: 10.h)),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 100.w,
                            child: Text(
                              'Vai trò: ',
                              style: styleTitle,
                            ),
                          ),
                          Expanded(child: getRoleParent()),
                        ],
                      ),
                    ],
                  ))
            ],
          ),
        ));
  }

  Widget getRoleParent() {
    return controller.listStudent.isNotEmpty?
    RichText(
      textAlign: TextAlign.end,
      text: TextSpan(
        text: 'Phụ huynh em ',
        style: TextStyle(
            color: const Color.fromRGBO(26, 26, 26, 1),
            fontSize: 14.sp,
            fontWeight: FontWeight.w500,
            fontFamily: 'static/Inter-Medium.ttf'),

        children: controller.listStudent.map((e) {
          var index = controller.listStudent.indexOf(e);
          var showSplit = ", ";
          if (index == controller.listStudent.length - 1) {
            showSplit = "";
          }
          return TextSpan(
              text: "${e.fullName}$showSplit",
              style: TextStyle(
                  color: ColorUtils.PRIMARY_COLOR,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf'));
        }).toList(),
      ),
    ):Container();
  }

  Container help() {
    return Container(
      margin: EdgeInsets.only(top: 16.h),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.r),
        color: const Color.fromRGBO(255, 255, 255, 1),
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 8.h, left: 12.w, right: 12.w),
            child: Row(
              children: [
                Text(
                  'Cài Đặt',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: const Color.fromRGBO(133, 133, 133, 1),
                      fontSize: 12.sp,
                      fontFamily: 'static/Inter-Medium.ttf',
                      fontWeight: FontWeight.w500),
                ),
                Expanded(child: Container())
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(top: 8.h)),
          Container(
            margin: EdgeInsets.only(
              left: 12.w,
            ),
            child: Column(
              children: [
                InkWell(
                  onTap: () {
                    controller.getStaticPage("SUPPORT");
                  },
                  child: Row(
                    children: [
                      Text(
                        'Hỗ Trợ',
                        style: TextStyle(
                            color: const Color.fromRGBO(24, 29, 39, 1),
                            fontSize: 14.sp,
                            fontFamily: 'static/Inter-Medium.ttf'),
                      ),
                      Expanded(child: Container()),
                      IconButton(
                          iconSize: 24.h,
                          onPressed: () {
                            controller.getStaticPage("SUPPORT");
                          },
                          splashColor: Colors.transparent,
                          icon: const Icon(
                            Icons.navigate_next,
                            color: ColorUtils.PRIMARY_COLOR,
                          ))
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    controller.goToChangePassPage();
                  },
                  child: Row(
                    children: [
                      Text(
                        'Đổi Mật Khẩu ',
                        style: TextStyle(
                            color: const Color.fromRGBO(24, 29, 39, 1),
                            fontSize: 14.sp,
                            fontFamily: 'static/Inter-Medium.ttf'),
                      ),
                      Expanded(child: Container()),
                      IconButton(
                          iconSize: 24.h,
                          onPressed: () {
                            controller.goToChangePassPage();
                          },
                          splashColor: Colors.transparent,
                          icon: const Icon(
                            Icons.navigate_next,
                            color: ColorUtils.PRIMARY_COLOR,
                          ))
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    controller.getStaticPage("RULES");
                  },
                  child: Row(
                    children: [
                      Text(
                        'Chính Sách Bảo Mật',
                        style: TextStyle(
                            color: const Color.fromRGBO(24, 29, 39, 1),
                            fontSize: 14.sp,
                            fontFamily: 'static/Inter-Medium.ttf'),
                      ),
                      Expanded(child: Container()),
                      IconButton(
                          iconSize: 24.h,
                          onPressed: () {
                            controller.getStaticPage("RULES");
                          },
                          splashColor: Colors.transparent,
                          icon: const Icon(
                            Icons.navigate_next,
                            color: ColorUtils.PRIMARY_COLOR,
                          ))
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    controller.getStaticPage("SECURITY");
                  },
                  child: Row(
                    children: [
                      Text(
                        'Điều Khoản Sử Dụng',
                        style: TextStyle(
                            color: const Color.fromRGBO(24, 29, 39, 1),
                            fontSize: 14.sp,
                            fontFamily: 'static/Inter-Medium.ttf'),
                      ),
                      Expanded(child: Container()),
                      IconButton(
                          iconSize: 24.h,
                          onPressed: () {
                            controller.getStaticPage("SECURITY");
                          },
                          splashColor: Colors.transparent,
                          icon: const Icon(
                            Icons.navigate_next,
                            color: ColorUtils.PRIMARY_COLOR,
                          ))
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    Get.bottomSheet(const Logout());
                  },
                  child: Row(
                    children: [
                      Text(
                        'Đăng Xuất',
                        style: TextStyle(
                            color: const Color.fromRGBO(24, 29, 39, 1),
                            fontSize: 14.sp,
                            fontFamily: 'static/Inter-Medium.ttf'),
                      ),
                      Expanded(child: Container()),
                      IconButton(
                          iconSize: 24.h,
                          onPressed: () {
                            Get.bottomSheet(const Logout());
                          },
                          icon: const Icon(
                            Icons.navigate_next,
                            color: ColorUtils.PRIMARY_COLOR,
                          ))
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  buildAvatarSelect() {
    var file = controller.files[0].file;
    return SizedBox(
      width: double.infinity,
      child: Container(
          width: 180,
          height: 180,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(100.r))),
          child: CircleAvatar(
            backgroundImage: Image.file(
              file!,
              width: 200,
              height: 200,
            ).image,
          )),
    );
  }

  Future<void> pickerImage() async {
    var file = await FileDevice.showSelectFileV2(Get.context! ,image: true);
    if (file.isNotEmpty) {
      controller.files.clear();
      controller.files.addAll(file);
      controller.files.refresh();
      Get.bottomSheet(
          getDialogConfirm(
              "Chọn hình đại diện",
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: buildAvatarSelect(),
              ),
              colorBtnOk: ColorUtils.COLOR_GREEN_BOLD,
              colorTextBtnOk: ColorUtils.COLOR_WHITE,
              btnRight: "Xác nhận", funcLeft: () {
            Get.back();
          }, funcRight: () {
            Get.back();
            controller.uploadFile();
          }),
          isScrollControlled: true);
    }
  }
}
