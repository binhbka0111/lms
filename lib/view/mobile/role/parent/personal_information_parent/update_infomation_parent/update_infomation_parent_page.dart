import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/role/parent/personal_information_parent/personal_information_parent_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/personal_information_parent/update_infomation_parent/update_infomation_parent_controller.dart';
import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../commom/utils/color_utils.dart';
import '../../../../../../commom/utils/file_device.dart';
import '../../../../../../commom/utils/global.dart';
import '../../../../../../commom/widget/custom_view.dart';
import '../../../../../../commom/widget/text_field_custom.dart';

class UpdateInfoParentPage extends GetWidget<UpdateInfoParentControlller> {
  @override
  final controller = Get.put(UpdateInfoParentControlller());

  UpdateInfoParentPage({super.key});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            leading: InkWell(
              onTap: () {
                Get.back(result: controller.userProfile.value);
                Get.find<PersonalInformationParentController>().onInit();
              },
              child: const Icon(Icons.arrow_back_rounded,color: Colors.white,),
            ),
            backgroundColor:ColorUtils.PRIMARY_COLOR,
            elevation: 0,
            title: Text(
              "Chỉnh Sửa Thông Tin Cá Nhân",
              style: TextStyle(
                  fontSize: 16.sp,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
            ),
          ),
          body: Obx(() => WillPopScope(
              child: SingleChildScrollView(
                  reverse: true, // this is new
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Center(
                        child: Column(
                          children: [
                            Padding(padding: EdgeInsets.only(top: 16.h)),
                            Container(
                              height: 80,
                              margin: EdgeInsets.only(top: 10.h),
                              child: Stack(
                                children: [
                               InkWell(
                                 onTap: (){
                                   controller.goToDetailAvatar();
                                 },
                                 child:    SizedBox(
                                   width: 80.h,
                                   height: 80.h,
                                   child:
                                   CacheNetWorkCustom(urlImage:'${controller.userProfile.value.image}')

                                 ),
                               ),
                                  Positioned(
                                      right: 0,
                                      bottom: 0,
                                      child:
                                      InkWell(
                                        onTap: () {
                                          pickerImage();
                                        },
                                        child: Image.asset(
                                          'assets/images/img_cam.png',width: 24.h,height: 24.h,),
                                      ))
                                ],
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(top: 16.h, bottom: 8.h),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${controller.userProfile.value.fullName}',
                                      style: TextStyle(
                                          color: const Color.fromRGBO(26, 26, 26, 1),
                                          fontSize: 16.sp,
                                          fontFamily: 'static/Inter-Medium.ttf',
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Padding(padding: EdgeInsets.only(top: 4.h)),
                                    Text(
                                      '${controller.userProfile.value.email}',
                                      style: TextStyle(
                                        color: const Color.fromRGBO(133, 133, 133, 1),
                                        fontSize: 10.sp,
                                      ),
                                    )
                                  ],
                                ))
                          ],
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      OutlineBorderTextFormField(
                        enable: true,
                        focusNode: controller.focusName.value,
                        iconPrefix: "",
                        iconSuffix: "",
                        state: controller.stateInput,
                        labelText: "Họ và tên",
                        autofocus: false,
                        controller: controller.controllerName.value,
                        helperText: "",
                        showHelperText: false,
                        ishowIconPrefix: false,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        validation: (textToValidate) {
                          return controller.getTempIFSCValidation(textToValidate);
                        },
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      OutlineBorderTextFormField(
                        enable: false,
                        focusNode: controller.focusPhone.value,
                        iconPrefix: null,
                        iconSuffix: "",
                        state: StateType.DEFAULT,
                        labelText: "Số Điện thoại",
                        autofocus: false,
                        controller: controller.controllerPhone.value,
                        helperText: "",
                        showHelperText: false,
                        textInputAction: TextInputAction.next,
                        ishowIconPrefix: false,
                        keyboardType: TextInputType.text,
                        validation: (textToValidate) {
                          return controller.getTempIFSCValidation(textToValidate);
                        },
                      ),
                      Padding(padding: EdgeInsets.only(top: 8.h)),
                      OutlineBorderTextFormField(
                        enable: false,
                        focusNode: controller.focusEmail.value,
                        iconPrefix: "",
                        iconSuffix: "",
                        state: StateType.DEFAULT,
                        labelText: "Email",
                        autofocus: false,
                        controller: controller.controllerEmail.value,
                        helperText: "",
                        showHelperText: false,
                        ishowIconPrefix: false,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        validation: (textToValidate) {
                          return controller.getTempIFSCValidation(textToValidate);
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 14.h,right: 14.h),
                        child: ListView.builder(
                            itemCount: controller.listStudent.length,
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              return Column(
                                children: [
                                  Padding(padding: EdgeInsets.only(top: 8.h)),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    padding: const EdgeInsets.all(2.0),
                                    decoration: BoxDecoration(
                                        color: const Color.fromRGBO(246, 246, 246, 1),
                                        borderRadius:
                                        const BorderRadius.all(Radius.circular(6.0)),
                                        border: Border.all(
                                          width: 1,
                                          style: BorderStyle.solid,
                                          color:  const Color.fromRGBO(192, 192, 192, 1),
                                        )),
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(16.h, 8.h, 16.h, 8.h),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Phụ huynh em",
                                            style: TextStyle(
                                                fontSize: 10.0.sp,
                                                color: const Color.fromRGBO(177, 177, 177, 1),
                                                fontWeight: FontWeight.w500,
                                                fontFamily:
                                                'assets/font/static/Inter-Medium.ttf'),
                                          ),
                                          Padding(padding: EdgeInsets.only(top: 4.h)),
                                          Text(
                                            "${controller.listStudent[index].fullName}",
                                            style: TextStyle(
                                              fontSize: 12.0.sp,
                                              color: const Color.fromRGBO(26, 26, 26, 1),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              );
                            }),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.fromLTRB(16.h, 32.h, 16.h, 0.h),
                        child: SizedBox(
                          height: 46,
                          child: ElevatedButton(
                            onPressed: () {
                              dismissKeyboard();
                              controller.tmpUserProfile.value.fullName =
                                  controller.controllerName.value.text;
                              if(controller.controllerName.value.text.trim().isEmpty){
                                controller.stateInput = StateType.ERROR;
                                AppUtils.shared.snackbarError(
                                    "Họ và tên không được để trống","");
                              }else{
                                controller.updateInfoUser(controller.tmpUserProfile.value,
                                    controller.userProfile.value.id);
                                controller.stateInput = StateType.DEFAULT;
                              }
                            },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: ColorUtils.PRIMARY_COLOR,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6))),
                            child: Text("Cập Nhật",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.sp,
                                    fontWeight: FontWeight.w400)),
                          ),
                        ),
                      ),
                      Padding(
                        // this is new
                          padding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom)),
                    ],
                  )),
              onWillPop: () async {
                Get.back(result: controller.userProfile.value);
                Get.find<PersonalInformationParentController>().onInit();
                return true;
              }))),
    );
  }

  buildAvatarSelect() {
    var file = controller.files[0].file;
    return SizedBox(
      width: double.infinity,
      child: Container(
          width: 180,
          height: 180,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(100.r))),
          child: CircleAvatar(
            backgroundImage: Image.file(
              file!,
              width: 200,
              height: 200,
            ).image,
          )),
    );
  }

  Future<void> pickerImage() async {
    var file = await FileDevice.showSelectFileV2(Get.context!,image: true);
    if (file.isNotEmpty) {
      controller.files.clear();
      controller.files.addAll(file);
      controller.files.refresh();
      Get.bottomSheet(
          getDialogConfirm(
              "Chọn hình đại diện",
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: buildAvatarSelect(),
              ),
              colorBtnOk: ColorUtils.COLOR_GREEN_BOLD,
              colorTextBtnOk: ColorUtils.COLOR_WHITE,
              btnRight: "Xác nhận", funcLeft: () {
            Get.back();
          }, funcRight: () {
            Get.back();
            controller.uploadFile();
          }),
          isScrollControlled: true);
    }
  }
}
