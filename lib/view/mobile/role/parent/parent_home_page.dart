import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/date_time_utils.dart';
import 'package:slova_lms/commom/widget/appointment_builder_timelineday.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/commom/widget/meeting_data_source.dart';
import 'package:slova_lms/view/mobile/event_news/event_body.dart';
import 'package:slova_lms/view/mobile/event_news/event_news_controller.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';
import '../../../../commom/app_cache.dart';
import '../../../../commom/constants/string_constant.dart';
import '../../../../commom/widget/loading_custom.dart';
import '../../../../routes/app_pages.dart';
import '../../schedule/schedule_controller.dart';

class ParentHomePage extends GetView<ParentHomeController> {
  @override
  final controller = Get.put(ParentHomeController());

  ParentHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    var isPhone = Get.context?.isPhone ?? true;
    return SafeArea(
        child: Scaffold(

        body: Obx(() => controller.isReady.value?RefreshIndicator(
            color: ColorUtils.PRIMARY_COLOR,
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Stack(
                children: [
                  buildBanner(),
                  Container(
                    margin: EdgeInsets.only(top: (isPhone) ? 160.h : 180.h),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        headerStudent(),
                        Padding(padding: EdgeInsets.only(top: 8.h)),


                        Visibility(
                          visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_SUBJECT),
                          child: Column(
                            children: [
                              Container(
                                margin: const EdgeInsets.fromLTRB(16, 20, 16, 6),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          child: Text("Môn Học",
                                              style: TextStyle(
                                                  fontSize: 20.sp,
                                                  color: const Color.fromRGBO(
                                                      26, 26, 26, 1))),
                                        ),
                                        const Padding(
                                            padding: EdgeInsets.only(top: 6)),
                                        Visibility(
                                          visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_LIST_SUBJECT),
                                          child:
                                          SizedBox(
                                            child: Text(
                                              "Năm ${controller.setTextSchoolYears()} bạn có ${controller.items.length} môn học",
                                              style: TextStyle(
                                                  fontSize: 12.sp,
                                                  color: const Color.fromRGBO(
                                                      133, 133, 133, 1)),
                                            ),
                                          ),)
                                      ],
                                    ),
                                    Flexible(child: Container()),
                                    Visibility(
                                        visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_LIST_SUBJECT),
                                        child: Visibility(
                                          visible: controller.items.isNotEmpty,
                                            child: InkWell(
                                          onTap: () {
                                            controller.toSubjectPage();
                                          },
                                          child: Text(
                                            "Xem Tất cả",
                                            style: TextStyle(
                                                fontSize: 12.sp,
                                                color: ColorUtils.PRIMARY_COLOR),
                                          ),
                                        )))
                                  ],
                                ),
                              ),
                              Visibility(
                                child:  scheduleSubject(),
                                visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_LIST_SUBJECT),)

                            ],
                          ),
                        ),


                        Visibility(
                            visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_LESSON_DASHBROAD_LIST),
                            child:    Column(
                              children: [
                                Container(
                                  margin: const EdgeInsets.fromLTRB(16, 20, 16, 6),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children:  [
                                          SizedBox(
                                            child: Text("Thời khóa biểu",
                                                style: TextStyle(
                                                    fontSize: 20.sp,
                                                    color: const Color.fromRGBO(
                                                        26, 26, 26, 1))),
                                          ),
                                        ],
                                      ),),
                                      Visibility(
                                          visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_LESSON_DASHBROAD_LIST),
                                          child:    InkWell(
                                            onTap: (){
                                              Get.find<HomeController>().comeCalendar();
                                            },
                                            child: Text(
                                              "Xem Tất cả",
                                              style: TextStyle(
                                                  fontSize: 12.sp,
                                                  color: ColorUtils.PRIMARY_COLOR),
                                            ),
                                          ))
                                    ],
                                  ),
                                ),
                                Padding(padding: EdgeInsets.only(top: 8.h)),
                                Visibility (
                                    visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_LESSON_DASHBROAD_LIST),
                                    child:
                                    Card(
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                                      margin: const EdgeInsets.only(left: 16, right: 16, bottom: 20),
                                      child:
                                      Obx(() => SfCalendar(
                                        view: CalendarView.timelineDay,
                                        firstDayOfWeek: 1,
                                        allowViewNavigation: false,
                                        controller: controller.calendarController.value,
                                        dataSource: MeetingDataSource(controller.timeTable),
                                        todayHighlightColor: const Color.fromRGBO(249, 154, 81, 1),
                                        selectionDecoration: BoxDecoration(
                                          border: Border.all(color: const Color.fromRGBO(249, 154, 81, 1), width: 2), // Sử dụng màu sắc của border
                                        ),
                                        appointmentBuilder: appointmentBuilderTimeLineDay,
                                        timeSlotViewSettings: TimeSlotViewSettings(
                                          timeFormat: "H a",
                                          timeIntervalWidth: 100.w,
                                          timelineAppointmentHeight: 50,
                                        ),
                                        scheduleViewSettings: const ScheduleViewSettings(
                                            appointmentItemHeight: 20, hideEmptyScheduleWeek: true),
                                      )),
                                    )),
                              ],
                            )),

                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: const EdgeInsets.symmetric(horizontal: 16),
                              child: Text("Tin tức , sự kiện",
                                  style: TextStyle(
                                      fontSize: 20.sp,
                                      color: const Color.fromRGBO(
                                          26, 26, 26, 1))),
                            ),
                            Visibility(
                                visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_NEWS),
                                child:
                                const EventBody(isInHome: true))
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ), onRefresh: ()async{
          await controller.onRefresh();
          await Get.find<EventNewsController>().onRefresh();
        }):const LoadingCustom())));
  }

  buildIndicatorBanner() {
    return
      SizedBox(
        height: 6.h,
        child: ListView.builder(
            itemCount: controller.itemBanner.length,
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {

              return
                Obx(() =>   controller.indexSwiper.value == index
                    ? Container(
                  width: 12.w,
                  height: 10.w,
                  margin: EdgeInsets.only(left: 4.w, right: 4.w),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(6)),
                )
                    : SizedBox(
                  width: 10.w,
                  height: 10.w,
                  child: const CircleAvatar(
                    backgroundColor: Color.fromRGBO(252, 252, 254, 0.5),
                  ),
                ));
            }),
      );
  }
  buildBanner() {
    return Obx(() => SizedBox(
      height: 200.h,
      child:      Swiper(
        controller: controller.controllerSwiperStudent,
        autoplayDelay: 5000,
        autoplay: true,
        onIndexChanged: (value) {
          controller.indexSwiper.value = value;
          // controller.itemBanner[index].selected= value;
        },
        itemCount: controller.itemBanner.length,
        itemBuilder: (context, index) {
          return CacheNetWorkCustomBanner2(urlImage: "${controller.itemBanner[index].url}");
        },


      ),
    ));
  }

  scheduleSubject() {
    return Obx(() => GridView.builder(
        padding: const EdgeInsets.all(10),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
        ),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        physics: const ClampingScrollPhysics(),
        itemCount: (controller.items.length > 6)
            ? 6
            : controller.items.length,
        itemBuilder: (context, index) {
          return
           InkWell(
             onTap: (){
              checkClickPage(Get.find<HomeController>().userGroupByApp, () => controller.getDetailSubject(index), StringConstant.PAGE_SUBJECT_DETAIL);
             },
             child:  SizedBox(
               height: 96.w,
               child: Card(
                 elevation: 0.5,
                 shape: RoundedRectangleBorder(
                     borderRadius: BorderRadius.circular(8)),
                 child: Column(
                   mainAxisAlignment: MainAxisAlignment.center,
                   children: [
                     SizedBox(
                       height: 50.w,
                       width: 50.w,
                       child:
                       CacheNetWorkCustomBanner(urlImage: '${controller.items[index].subjectCategory?.image}',)

                     ),
                     Padding(padding: EdgeInsets.only(top: 8.h)),
                     Container(
                       margin: EdgeInsets.symmetric(horizontal: 8.w),
                       child: Text(
                         textAlign: TextAlign.center,
                         "${controller.items[index].subjectCategory?.name}",
                         style: TextStyle(
                             color: const Color.fromRGBO(26, 26, 26, 1),
                             fontSize: 12.sp),
                       ),
                     )
                   ],
                 ),
               ),
             ),
           );
        }));
  }

  headerStudent() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8.w),
      child: Column(
        children: [
          buildIndicatorBanner(),
          Padding(padding: EdgeInsets.only(bottom: 8.h)),
          Container(
            decoration: BoxDecoration(
                color: const Color.fromRGBO(255, 255, 255, 1),
                borderRadius: BorderRadius.circular(8.r)),
            child: Column(
              children: [
                Padding(padding: EdgeInsets.only(bottom: 8.h)),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 16.w, top: 16.w),
                    ),
                    Image.asset(
                      "assets/images/img_header.png", height: 48.h,width: 48.w,),
                    Padding(padding: EdgeInsets.only(left: 8.h)),
                 Expanded(child:    Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                     Obx(() =>Text(
                       "Chào, ${controller.userProfile.value.fullName}",
                       style: TextStyle(
                           fontSize: 16.sp,
                           color: const Color.fromRGBO(26, 26, 26, 1),
                           fontWeight: FontWeight.w400),
                     ),),
                     SizedBox(height: 4.h,),
                     InkWell(
                       onTap: () {
                         Get.bottomSheet(bottomSheetStudentProfile());
                       },
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         children: [
                           RichText(
                             textAlign: TextAlign.start,
                             text: TextSpan(
                               text:
                               '${controller.studentName.value} ',
                               style: TextStyle(
                                   color: const Color.fromRGBO(
                                       26, 26, 26, 1),
                                   fontSize: 14.sp,
                                   fontWeight: FontWeight.w400,
                                   fontFamily:
                                   'static/Inter-Medium.ttf'),
                               children: controller.className.map((e) {
                                 var index = controller.className.indexOf(e);
                                 var showSplit = ", ";
                                 if (index ==
                                     (controller.className.length) -
                                         1) {
                                   showSplit = "";
                                 }
                                 return TextSpan(
                                     text: "${e.name}$showSplit",
                                     style: TextStyle(
                                         color: ColorUtils.PRIMARY_COLOR,
                                         fontSize: 14.sp,
                                         fontWeight: FontWeight.w500,
                                         fontFamily:
                                         'static/Inter-Medium.ttf'));
                               }).toList(),
                             ),
                           ),
                           Padding(padding: EdgeInsets.only(left: 8.h)),
                           Icon(
                             Icons.keyboard_arrow_down_outlined,
                             size: 18.h,
                             color: Colors.black,
                           )
                         ],
                       ),
                     )
                   ],
                 ))
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 16.h)),
                Row(
                  children: [
                    Padding(padding: EdgeInsets.only(left: 16.w)),
                  Visibility(
                    visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_DILIGENCE),
                    child:
                  Expanded(
                    flex: 2,
                    child: InkWell(
                      onTap: () {
                        controller.goToDiligenceParent();
                      },
                      child: Column(
                        children: [
                          Image.asset("assets/images/icon_diligence_std.png",width: 24.w,height: 24.h),
                          Padding(padding: EdgeInsets.only(top: 11.h)),
                          Text(
                            "Chuyên cần",
                            style: TextStyle(
                                color: const Color.fromRGBO(26, 26, 26, 1),
                                fontSize: 12.sp),
                          )
                        ],
                      ),
                    ),
                  ),),
                   Visibility(
                       visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_TRANSCRIPT_SYNTHETIC),
                       child:  Expanded(
                     flex: 2,
                     child: InkWell(
                       onTap: () {
                         Get.toNamed(Routes.transcriptsSyntheticParentPage);
                       },
                       child: Column(
                         children: [
                           Image.asset("assets/images/icon_bangdiem_std.png",width: 24.w,height: 24.h),
                           Padding(padding: EdgeInsets.only(top: 11.h)),
                           Text(
                             "Bảng điểm",
                             style: TextStyle(
                                 color: const Color.fromRGBO(26, 26, 26, 1),
                                 fontSize: 12.sp),
                           )
                         ],
                       ),
                     ),
                   )),
                   Visibility(
                       visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_SCHOOL_YEARS_LIST),
                       child:  Expanded(
                     flex: 2,
                     child: InkWell(
                       onTap: () {
                         Get.bottomSheet(
                             StatefulBuilder(builder: (context, state) {
                               return Container(
                                 decoration: BoxDecoration(
                                   borderRadius: BorderRadius.only(
                                       topRight: Radius.circular(32.r),
                                       topLeft: Radius.circular(32.r)),
                                   color: Colors.white,
                                 ),
                                 child: Column(
                                   mainAxisAlignment: MainAxisAlignment.start,
                                   children: [
                                     Container(
                                       decoration: BoxDecoration(
                                           borderRadius:
                                           BorderRadius.circular(6.r),
                                           color: const Color.fromRGBO(
                                               210, 212, 216, 1)),
                                       height: 6.h,
                                       width: 48.w,
                                       alignment: Alignment.topCenter,
                                       margin: EdgeInsets.only(top: 16.h),
                                     ),
                                     Padding(
                                         padding: EdgeInsets.only(top: 12.h)),
                                     Text(
                                       "Năm Học",
                                       style: TextStyle(
                                           fontSize: 16.sp,
                                           color: Colors.black,
                                           fontWeight: FontWeight.w500),
                                     ),
                                     controller.schoolYears.isNotEmpty?
                                     Container(
                                       height: 300.h,
                                       margin: EdgeInsets.symmetric(horizontal: 8.w),
                                       child: ListView.builder(
                                           itemCount: controller.schoolYears.length,
                                           shrinkWrap: true,
                                           physics: const AlwaysScrollableScrollPhysics(),
                                           itemBuilder: (context, index) {
                                             if(controller.schoolYears.isNotEmpty){
                                               controller.clazzsOfSchool.value = controller.schoolYears[index].clazz!;
                                             }
                                             return controller.schoolYears.isEmpty?Container():InkWell(
                                               child: Container(
                                                 color:
                                                 controller.clickSchoolYear[index] == true ? const Color.fromRGBO(254, 230, 211, 1)
                                                     : Colors.white,
                                                 padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                                                 child: Row(
                                                   mainAxisAlignment: MainAxisAlignment.center,
                                                   crossAxisAlignment: CrossAxisAlignment.start,
                                                   children: [
                                                     SizedBox(
                                                         width: 36.h,
                                                         height: 36.h,
                                                         child: CacheNetWorkCustom(urlImage: '${controller.schoolYears[index].image}',)

                                                     ),
                                                     const Padding(padding: EdgeInsets.only(left: 8)),

                                                     Expanded(child:
                                                     Column(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                       crossAxisAlignment: CrossAxisAlignment.start,
                                                       children: [
                                                         Row(
                                                           mainAxisAlignment: MainAxisAlignment.start,
                                                           children: [
                                                             Expanded(child: RichText(
                                                               text: TextSpan(
                                                                   children: [
                                                                     TextSpan(
                                                                         text:
                                                                         'Học Sinh: ',
                                                                         style: TextStyle(
                                                                             color: ColorUtils.PRIMARY_COLOR,
                                                                             fontFamily: 'static/Inter-Regular.ttf',
                                                                             fontSize: 14.sp)),
                                                                     TextSpan(
                                                                         text:
                                                                         controller.schoolYears[index].fullName!,
                                                                         style: TextStyle(
                                                                             color: Colors.black,
                                                                             fontFamily: 'static/Inter-Regular.ttf',
                                                                             fontSize: 14.sp)),
                                                                   ]),
                                                             )),
                                                             SizedBox(width: 8.w,),
                                                             Text(controller.currentStudentProfile.value.birthday != null
                                                                 ? DateTimeUtils.convertLongToStringTime(controller.currentStudentProfile.value.birthday)
                                                                 : ""),
                                                             Padding(padding: EdgeInsets.only(top: 4.h)),
                                                           ],
                                                         ),
                                                         Row(
                                                           mainAxisAlignment: MainAxisAlignment.start,
                                                           children: [
                                                             Expanded(child: RichText(
                                                               textAlign: TextAlign.start,
                                                               text: TextSpan(
                                                                 text: 'Lớp: ',
                                                                 style: TextStyle(
                                                                     color: const Color.fromRGBO(26, 26, 26, 1),
                                                                     fontSize: 14.sp,
                                                                     fontWeight: FontWeight.w500,
                                                                     fontFamily: 'static/Inter-Medium.ttf'),

                                                                 children: controller.clazzsOfSchool.map((e) {
                                                                   var index = controller.clazzsOfSchool.indexOf(e);
                                                                   var showSplit = ", ";
                                                                   if (index == controller.clazzsOfSchool.length - 1) {
                                                                     showSplit = "";
                                                                   }
                                                                   return TextSpan(
                                                                       text: "${e.name}$showSplit",
                                                                       style: TextStyle(
                                                                           color: ColorUtils.PRIMARY_COLOR,
                                                                           fontSize: 14.sp,
                                                                           fontWeight: FontWeight.w500,
                                                                           fontFamily: 'static/Inter-Medium.ttf'));
                                                                 }).toList(),
                                                               ),
                                                             )),
                                                             SizedBox(width: 8.w,),
                                                             Row(
                                                               children: [
                                                                 Text("Năm học:",
                                                                   style: TextStyle(
                                                                       color: const Color.fromRGBO(133, 133, 133, 1),
                                                                       fontWeight: FontWeight.w500,
                                                                       fontSize: 14.sp),
                                                                 ),
                                                                 Text(
                                                                   "${controller.schoolYears[index].fromYear}",
                                                                   style: TextStyle(
                                                                       color: const Color.fromRGBO(133, 133, 133, 1),
                                                                       fontWeight: FontWeight.w500,
                                                                       fontSize: 14.sp),
                                                                 ),
                                                                 Text(
                                                                   "-",
                                                                   style: TextStyle(
                                                                       color: const Color.fromRGBO(133, 133, 133, 1),
                                                                       fontWeight: FontWeight.w500, fontSize: 14.sp),
                                                                 ),
                                                                 Text(
                                                                   "${controller.schoolYears[index].toYear}",
                                                                   style: TextStyle(
                                                                       color: const Color.fromRGBO(133, 133, 133, 1),
                                                                       fontWeight: FontWeight.w500,
                                                                       fontSize: 14.sp),
                                                                 ),
                                                               ],
                                                             )
                                                           ],
                                                         )
                                                       ],
                                                     ),),

                                                   ],
                                                 ),
                                               ),
                                               onTap: () {
                                                 controller.timeTable.clear();
                                                 Get.find<ScheduleController>().timeTable.clear();
                                                 updated(state, index);
                                                 controller.className.value = controller.schoolYears[index].clazz!;
                                                 AppCache().setSchoolYearId(controller.schoolYears[index].id??"");
                                                 controller.getListSubject(controller.currentStudentProfile.value.clazz?[0].id);
                                                 controller.getScheduleParent(controller.currentStudentProfile.value.id);
                                                 Get.find<ScheduleController>().queryTimeTableByStudent(controller.currentStudentProfile.value.id);
                                                 controller.fromYearPresent.value = controller.schoolYears[index].fromYear!;
                                                 controller.toYearPresent.value = controller.schoolYears[index].toYear!;
                                                 controller.timeTable.refresh();
                                                 controller.setTimeCalendar(index);
                                                 Get.find<ScheduleController>().queryTimeCalendar( AppCache().userType);
                                                 Get.find<ScheduleController>().timeTable.refresh();
                                                 Get.back();
                                               },
                                             );
                                           }),
                                     ):Container()

                                   ],
                                 ),
                               );
                             }));
                       },
                       child: Column(
                         children: [
                           Image.asset("assets/images/school_year_parent.png",width: 24.w,height: 24.h,),
                           Padding(padding: EdgeInsets.only(top: 11.h)),
                           Text(
                             "Năm Học",
                             style: TextStyle(
                                 color: const Color.fromRGBO(26, 26, 26, 1),
                                 fontSize: 12.sp),
                           )
                         ],
                       ),
                     ),
                   )),
                    Expanded(
                        flex: 2,
                        child: InkWell(
                          onTap: () {
                            controller.gotoInformation();
                          },
                          child: Column(
                            children: [
                              Image.asset("assets/images/icon_profile.png",width: 24.w,height: 24.h,),
                              Padding(padding: EdgeInsets.only(top: 11.h)),
                              Text(
                                "Cá nhân",
                                style: TextStyle(
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                    fontSize: 12.sp),
                              )
                            ],
                          ),
                        )),
                    Padding(padding: EdgeInsets.only(right: 16.w)),
                  ],
                ),
                Padding(padding: EdgeInsets.only(bottom: 16.h)),
              ],
            ),
          ),
        ],
      ),
    );
  }

  bottomSheetStudentProfile() {
    return StatefulBuilder(builder: (context,state){
      return Wrap(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(32.r),
                  topLeft: Radius.circular(32.r)),
              color: Colors.white,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6.r),
                      color: const Color.fromRGBO(210, 212, 216, 1)),
                  height: 6.h,
                  width: 48.w,
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.only(top: 16.h),
                ),
                Padding(padding: EdgeInsets.only(top: 12.h)),
                Text(
                  "Học Sinh",
                  style: TextStyle(
                      fontSize: 16.sp,
                      color: Colors.black,
                      fontWeight: FontWeight.w500),
                ),
                SingleChildScrollView(
                  child: Container(
                    height: 300.h,
                    margin: EdgeInsets.symmetric(vertical: 16.h, horizontal: 8.w),
                    child: ListView.builder(
                        itemCount: controller.listStudent.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          if(controller.listStudent[index].clazz!.isNotEmpty){
                            controller.clazzsOfStudent.value = controller.listStudent[index].clazz!;
                          }
                          return InkWell(
                            child: Column(
                              children: [
                                Container(
                                  padding: const EdgeInsets.fromLTRB(10, 8, 10, 0),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                          width: 40.h,
                                          height: 40.h,
                                          child:
                                          CacheNetWorkCustom(urlImage: '${controller.listStudent[index].image}',)
                                      ),

                                      Padding(padding: EdgeInsets.only(left: 8.w)),
                                      Expanded(child: Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          RichText(
                                            text: TextSpan(children: [
                                              TextSpan(
                                                  text: 'Học Sinh: ',
                                                  style: TextStyle(
                                                      color: ColorUtils.PRIMARY_COLOR,
                                                      fontFamily: 'static/Inter-Regular.ttf',
                                                      fontSize: 12.sp)),
                                              TextSpan(
                                                  text: controller.listStudent[index].fullName ?? "",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily: 'static/Inter-Regular.ttf',
                                                      fontSize: 14.sp)),
                                            ]),
                                          ),
                                          RichText(
                                            textAlign: TextAlign.end,
                                            text: TextSpan(
                                              text: 'Lớp: ',
                                              style: TextStyle(
                                                  color: ColorUtils.PRIMARY_COLOR,
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: 'static/Inter-Medium.ttf'),

                                              children: controller.clazzsOfStudent.map((e) {
                                                var index = controller.clazzsOfStudent.indexOf(e);
                                                var showSplit = ", ";
                                                if (index == (controller.clazzsOfStudent.length) - 1) {
                                                  showSplit = "";
                                                }
                                                return TextSpan(
                                                    text: "${e.name??""}$showSplit",
                                                    style: TextStyle(
                                                        color: ColorUtils.PRIMARY_COLOR,
                                                        fontSize: 14.sp,
                                                        fontWeight: FontWeight.w500,
                                                        fontFamily: 'static/Inter-Medium.ttf'));
                                              }).toList(),
                                            ),
                                          )
                                        ],
                                      )),
                                      SizedBox(
                                        width: 8.w,
                                      ),
                                      Text(
                                        controller.listStudent[index].birthday != null
                                            ? DateTimeUtils.convertLongToStringTime(controller.listStudent[index].birthday)
                                            : "",
                                        style: TextStyle(
                                            color: const Color.fromRGBO(133, 133, 133, 1),
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14.sp),
                                      ),
                                    ],
                                  ),
                                ),
                                const Divider()
                              ],
                            ),
                            onTap: () {
                              controller.onSelectUser(controller.listStudent[index]);
                              controller.className.value = controller.listStudent[index].clazz!;
                              Get.back();
                            },
                          );
                        }),
                  ),
                ),
                Padding(padding: EdgeInsets.only(bottom: 60.h))
              ],
            ),
          )
        ],
      );
    });
  }

  Future<void> updated(StateSetter updateState, int index) async {
    updateState(() {
      for(int i = 0;i<controller.schoolYears.length;i++){
        controller.clickSchoolYear.add(false);
      }
    });
  }
}
