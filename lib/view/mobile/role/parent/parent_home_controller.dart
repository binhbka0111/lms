import 'dart:ui';

import 'package:card_swiper/card_swiper.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/data/model/common/banner.dart';
import 'package:slova_lms/data/model/common/contacts.dart';
import 'package:slova_lms/data/model/common/subject.dart';
import 'package:slova_lms/data/model/common/user_profile.dart';
import 'package:slova_lms/data/model/res/class/classTeacher.dart';
import 'package:slova_lms/data/repository/common/common_repo.dart';
import 'package:slova_lms/data/repository/person/personal_info_repo.dart';
import 'package:slova_lms/data/repository/subject/class_office_repo.dart';
import 'package:slova_lms/view/mobile/event_news/event_news_controller.dart';
import 'package:slova_lms/view/mobile/schedule/schedule_controller.dart';
import '../../../../commom/app_cache.dart';
import '../../../../data/model/common/schedule.dart';
import '../../../../data/model/common/school_year.dart';
import '../../../../data/model/common/student_by_parent.dart';
import '../../../../data/repository/schedule/schedule_repo.dart';
import '../../../../data/repository/school_year/school_year_repo.dart';
import '../../../../routes/app_pages.dart';
import '../../home/home_controller.dart';


class ParentHomeController extends GetxController {
  var listStudent = <StudentByParent>[].obs;
  final CommonRepo _commonRepo = CommonRepo();
  final PersonalInfoRepo _personalInfoRepo = PersonalInfoRepo();
  final ClassOfficeRepo _subjectRepo = ClassOfficeRepo();
  final ScheduleRepo _scheduleRepo = ScheduleRepo();
  var userProfile = UserProfile().obs;
  var currentStudentProfile = StudentByParent().obs;
  RxList<Schedule> schedule = <Schedule>[].obs;
  RxList<SubjectRes> items = <SubjectRes>[].obs;
  RxList<Appointment> timeTable = <Appointment>[].obs;
  final SchoolYearRepo _schoolYearRepo = SchoolYearRepo();
  var clickSchoolYear = <bool>[].obs;
  RxList<Schedule> scheduleStudent = <Schedule>[].obs;
  RxList<SchoolYear> schoolYears = <SchoolYear>[].obs;
  RxList<Clazzs> clazzsOfSchool  = <Clazzs>[].obs;
  RxList<Clazzs> clazzsOfStudent  = <Clazzs>[].obs;
  var clazzInSchoolYear = Clazzs().obs;
  RxList<Clazzs> className =<Clazzs>[].obs;
  RxString studentName = "".obs;
  var contacts = Contacts().obs;
  RxList<ItemContact>  itemContact = <ItemContact>[].obs;
  RxList<ClassId> classOfUser = <ClassId>[].obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  RxList<Schedule> scheduleToday = <Schedule>[].obs;
  var calendarController = CalendarController().obs;
  var fromYearPresent = (DateTime.now().year-1).obs;
  var toYearPresent = (DateTime.now().year).obs;
  var subjectId = "".obs;
  var isReady = false.obs;
  var indexSwiper = 0.obs;
  SwiperController controllerSwiperStudent = SwiperController();
  @override
  void onInit() {
    super.onInit();
    getDetailProfile();
    getBanner();

  }

  getDetailProfile() async{
   await _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
        Get.put(EventNewsController());
        userProfile.value.school!.id!;
        Get.find<EventNewsController>().setSchoolId(userProfile.value.school!.id!);
        Get.find<EventNewsController>().onRefresh();
        AppCache().userId = userProfile.value.id ?? "";
        if(userProfile.value.item!.students!.isNotEmpty){
          getSchoolYearByStudent(userProfile.value.item?.students![0].id);
          studentName.value = userProfile.value.item!.students![0].fullName!;
        }
      }
    });
  }

  getListStudent()  async{
    var userId = userProfile.value.id;
    await _personalInfoRepo.listStudentByParent(userId).then((value) async{
      if (value.state == Status.SUCCESS) {
        listStudent.value =value.object!;
        currentStudentProfile.value = listStudent[0];
        className.value = currentStudentProfile.value.clazz!;
        await getListSubject(listStudent[0].clazz?[0].id);
        await Get.find<ScheduleController>().queryTimeTableByStudent(currentStudentProfile.value.id);
      }
    });
  }

  getSchoolYearByStudent(userId) async {
    await _schoolYearRepo.getListSchoolYearByStudent(userId).then((value) async{
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        for(int i = 0;i<schoolYears.length;i++){
          clickSchoolYear.add(false);
        }
        if(schoolYears.isNotEmpty){
          fromYearPresent.value = schoolYears[0].fromYear!;
          toYearPresent.value = schoolYears[0].toYear!;
          if(DateTime.now().month<=12&&DateTime.now().month>9){
            calendarController.value.displayDate = DateTime(fromYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
          }else{
            calendarController.value.displayDate = DateTime(toYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
          }
        }
      }
      await getListStudent();
      if(userProfile.value.item!.students!.isNotEmpty){
        await getScheduleParent(userProfile.value.item?.students![0].id);

      }
      await Get.find<HomeController>().getCountNotifyNotSeen();
    });
   isReady.value = true;

  }

  setTextSchoolYears(){
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      if(fromYearPresent.value == DateTime.now().year){
        return "nay";
      }else{
        return "${fromYearPresent.value} - ${toYearPresent.value}";
      }
    }else{
      if(toYearPresent.value == DateTime.now().year){
        return "nay";
      }else{
        return "${fromYearPresent.value} - ${toYearPresent.value}";
      }
    }
  }

  onClickShoolYears(index){
    clickSchoolYear.value = <bool>[].obs;
    for(int i = 0;i<schoolYears.length;i++){
      clickSchoolYear.add(false);
    }
    clickSchoolYear[index] = true;
  }


  setTimeCalendar(index){
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      calendarController.value.displayDate = DateTime(schoolYears[index].fromYear!.toInt(),DateTime.now().month,DateTime.now().day);
    }else{
      calendarController.value.displayDate = DateTime(schoolYears[index].toYear!.toInt(),DateTime.now().month,DateTime.now().day);
    }
  }

  toDetailPhonebook(type,index){
    Get.toNamed(Routes.detailPhonebook,arguments: [itemContact,type,classOfUser[index].name, classOfUser[index].id]);
  }

  getScheduleParent(studentId) async {
    await _scheduleRepo.getScheduleStudent(studentId).then((value) {
      if (value.state == Status.SUCCESS) {
        scheduleStudent.value = value.object!;
        getAppointments();
      }
    });
  }

  getAppointments() {
    timeTable.clear();
    for(int i =0; i<scheduleStudent.length;i++){
      timeTable.add(Appointment(
        startTime: DateTime.fromMillisecondsSinceEpoch(scheduleStudent[i].startTime!),
        endTime: DateTime.fromMillisecondsSinceEpoch(scheduleStudent[i].endTime!),
        subject: scheduleStudent[i].subject!.subjectCategory!.name!,
        notes: scheduleStudent[i].subject?.clazz?.classCategory?.name!.toString(), // tên lớp
        resourceIds: scheduleStudent[i].subject!.clazz!.students,
          recurrenceId: scheduleStudent[i].subject?.teacher?.fullName!.toString(),
          color: const Color.fromRGBO(249, 154, 81, 1)
      ));
    }
    scheduleToday.value = scheduleStudent.where((element) => outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(element.startTime!))
        == outputDateFormat.format(DateTime.now())&&outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(element.endTime!))
        == outputDateFormat.format(DateTime.now())).toList();
    timeTable.refresh();
  }


  var banner = BannerHomePage().obs;
  RxList<ItemBanner> itemBanner = <ItemBanner>[].obs;
  RxInt indexPageBanner = 0.obs;

  selectPageBanner(int index) {
    indexPageBanner.value = index;
    for (var element in itemBanner) {
      element.selected = false;
    }
    itemBanner[index].selected = true;
    if (index != 0) {
      indexPageBanner.value - 1;
    } else {
      indexPageBanner.value = 0;
    }
    itemBanner.refresh();
  }

  getBanner() async {
    itemBanner.value = [];
    _commonRepo.getBanner().then((value) {
      if (value.state == Status.SUCCESS) {
        banner.value = value.object!;
        itemBanner.value = banner.value.items!;
        itemBanner[0].selected = true;
        itemBanner.refresh();
      }
    });
  }




  getListSubject(classId)  {
    items.clear();
    _subjectRepo.listSubject(classId).then((value) {
      if (value.state == Status.SUCCESS) {
        items.value =value.object!;
      }
    });
    items.refresh();
  }


  onSelectUser(StudentByParent value) async{
    timeTable.clear();
    currentStudentProfile.value = value;
    studentName.value = currentStudentProfile.value.fullName!;
    await _schoolYearRepo.getListSchoolYearByStudent(currentStudentProfile.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        for(int i = 0;i<schoolYears.length;i++){
          clickSchoolYear.add(false);
        }
      }
    });
    calendarController.value.displayDate = DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
    await getScheduleParent(currentStudentProfile.value.id);
    await getListSubject(currentStudentProfile.value.clazz?[0].id);
    await Get.find<ScheduleController>().queryTimeTableByStudent(currentStudentProfile.value.id);
    timeTable.refresh();
  }


  onRefresh() async{
    getBanner();
    await _personalInfoRepo.detailUserProfile().then((value) {
      if (value.state == Status.SUCCESS) {
        userProfile.value = value.object!;
      }
    });
    await _personalInfoRepo.listStudentByParent(userProfile.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        listStudent.value =value.object!;
      }
    });
    await getScheduleParent(currentStudentProfile.value.id);
    await getListSubject(currentStudentProfile.value.clazz?[0].id);
    await _schoolYearRepo.getListSchoolYearByStudent(currentStudentProfile.value.id).then((value) {
      if (value.state == Status.SUCCESS) {
        schoolYears.value = value.object!;
        for(int i = 0;i<schoolYears.length;i++){
          clickSchoolYear.add(false);
        }
      }
    });
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      calendarController.value.displayDate = DateTime(fromYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
    }else{
      calendarController.value.displayDate = DateTime(toYearPresent.value,DateTime.now().month,DateTime.now().day,DateTime.now().hour-1);
    }
  }


  void gotoInformation(){
    var tmpList = <StudentByParent>[];
    tmpList.addAll(listStudent);
    Get.toNamed(Routes.personalInfoParent,arguments: tmpList);
  }
  goToDiligenceParent(){
    Get.toNamed(Routes.diligenceParentPage);
  }

  toSubjectPage() {
    Get.toNamed(Routes.subject, arguments: items);
  }

  getDetailSubjectRole(role,index){
    subjectId.value= items[index].id!;
    if(role == "STUDENT"){
      Get.toNamed(Routes.detailSubjectStudentPage  );
    }else{
      Get.toNamed(Routes.detailSubjectParentPage);
    }

  }

  getDetailSubject(index){
    getDetailSubjectRole(AppCache().userType,index);
  }





}
