import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/commom/widget/loading_custom.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';
import '../../../../../commom/utils/click_subject_teacher.dart';
import '../../student/detail_subject/view_exercise/detail_exercise_student/view_all_notification/view_all_notification_page.dart';
import 'detail_subject_parent_controller.dart';

class DetailSubjectParentPage extends GetWidget<DetailSubjectParentController>{
  final controller = Get.put(DetailSubjectParentController());

  @override
  Widget build(BuildContext context) {
    return 
     Obx(() =>  controller.isReady.value?
         WillPopScope(child:
         Scaffold(
             appBar: AppBar(
               backgroundColor: ColorUtils.PRIMARY_COLOR,
               elevation: 0,
               title: Text(controller.nameSubject.value,style:  TextStyle(color: Colors.white,fontSize: 16.sp,fontWeight: FontWeight.w500),),
               actions:  [
                 IconButton(
                   onPressed: () {
                     Get.find<ParentHomeController>().subjectId.value = "";
                     Get.delete<DetailSubjectParentController>();
                     comeToHome();
                   },
                   icon: const Icon(Icons.home),
                   color: Colors.white,
                 )
               ],
             ),
             body:
             SingleChildScrollView(
               child:   Container(
                 margin: const EdgeInsets.all(16),
                 child:Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                     Row(children: [
                        Text('Thông Báo', style:  TextStyle(color: const Color.fromRGBO(26, 26, 26, 1), fontWeight: FontWeight.w500, fontSize: 16.sp, fontFamily: 'assets/font/static/Inter-Medium.ttf'),),
                       Expanded(child: Container()),
                       Visibility(
                           visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_NOTIFY_SUBJECT),
                           child:
                           InkWell(
                             onTap: (){
                               Get.to(ViewAllNotificationPage());
                             },
                             child:   Row(
                               children: [
                                 RichText(text:  TextSpan(children: [
                                   TextSpan(text: 'Xem Tất cả', style:  TextStyle(color: ColorUtils.PRIMARY_COLOR, fontSize: 12.sp, fontWeight: FontWeight.w400)),

                                 ])),
                                 const Padding(padding: EdgeInsets.only(right: 2)),
                                 Image.asset("assets/images/icon_nextTo.png",width: 12,height: 12,)
                               ],
                             ),
                           ))


                     ],),
                     const Padding(padding: EdgeInsets.only(top: 22)),

                     Visibility(
                       visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_NOTIFY_SUBJECT_PIN_LIST),
                       child:  ListView.builder(
                           itemCount:controller.items.length  ,
                           shrinkWrap: true,
                           physics: const NeverScrollableScrollPhysics(),
                           itemBuilder: (context, index){
                             if(controller.notificationSubject.value != "" || controller.items.isNotEmpty){
                               controller.files.value= controller.items[index].files!;
                             }else{
                               controller.files[index].name ="";
                               controller.files[index].link ="";
                             }
                             return
                               controller.items[index].status =="PIN" ?
                               InkWell(
                                 onTap: (){
                                   checkClickFeature(Get.find<HomeController>().userGroupByApp, () => controller.getDetailNotifySubject(index), StringConstant.FEATURE_NOTIFY_SUBJECT_PIN_DETAIL);
                                 },
                                 child: Container(
                                     padding: const EdgeInsets.all(12),
                                     decoration: BoxDecoration(
                                         color: const Color.fromRGBO(
                                             246, 246, 246, 1),
                                         borderRadius:
                                         BorderRadius.circular(8)),
                                     child:
                                     Column(
                                       crossAxisAlignment: CrossAxisAlignment.start,
                                       children: [
                                         Html(data: '${controller.items[index].title}',
                                           style: {
                                             "body":Style(
                                                 fontWeight: FontWeight.w400,
                                                 fontSize: FontSize(14.0),
                                                 color: const Color.fromRGBO(90, 90, 90, 1)

                                             )
                                           },),
                                         Html(data:'${controller.items[index].content}',
                                           style: {
                                             "body":Style(
                                                 fontWeight: FontWeight.w400,
                                                 fontSize: FontSize(14.0),
                                                 color: const Color.fromRGBO(90, 90, 90, 1)

                                             )
                                           },),
                                         const Padding(padding: EdgeInsets.only(top: 8)),

                                         ListView.builder(
                                             itemCount:controller.files.length,
                                             shrinkWrap: true,
                                             physics: const NeverScrollableScrollPhysics(),
                                             itemBuilder:(context, i){
                                               if(controller.files.isEmpty){
                                                 controller.files[index].name ="";
                                                 controller.files[index].link ="";
                                               }
                                               return
                                                 controller.items[index].files?[i].ext == "png" ||
                                                     controller.items[index].files?[i].ext == "jpg" ||
                                                     controller.items[index].files?[i].ext == "jpeg" ||
                                                     controller.items[index].files?[i].ext == "gif" ||
                                                     controller.items[index].files?[i].ext == "bmp"?
                                                 InkWell(
                                                     onTap: (){
                                                       controller.getAttackFile(index,i);
                                                     },
                                                     child:     Row(
                                                       children: [

                                                         SizedBox(
                                                           width: 24,
                                                           height: 24,
                                                           child:
                                                           CacheNetWorkCustomBanner(urlImage:  '${ controller.items[index].files?[i].link}',),
                                                         ),
                                                         Padding(
                                                             padding: EdgeInsets.only(
                                                                 right: 6.w)),
                                                         Expanded(
                                                           child:
                                                           Column(
                                                             crossAxisAlignment:
                                                             CrossAxisAlignment
                                                                 .start,
                                                             children: [
                                                               Text(
                                                                 '${ controller.items[index].files?[i].name}',
                                                                 style: TextStyle(
                                                                     color:
                                                                     const Color.fromRGBO(
                                                                         26,
                                                                         59,
                                                                         112,
                                                                         1),
                                                                     fontSize: 14.sp,
                                                                     fontWeight:
                                                                     FontWeight
                                                                         .w500),
                                                               ),
                                                               Text(
                                                                 "${ controller.items[index].files?[i].size}",
                                                                 style: TextStyle(
                                                                     color:
                                                                     const Color.fromRGBO(
                                                                         192,
                                                                         192,
                                                                         192,
                                                                         1),
                                                                     fontSize: 10.sp,
                                                                     fontWeight:
                                                                     FontWeight
                                                                         .w400),
                                                               ),
                                                             ],
                                                           ),
                                                         ),
                                                       ],
                                                     )
                                                 ):
                                                 Row(
                                                   children: [

                                                     Image.asset(
                                                       '${controller.getIconFile(controller.files[i].ext)}',
                                                       height: 24,
                                                       width: 24,
                                                     ),
                                                     Padding(
                                                         padding: EdgeInsets.only(
                                                             right: 6.w)),
                                                     Expanded(
                                                       child:    Column(
                                                         crossAxisAlignment:
                                                         CrossAxisAlignment
                                                             .start,
                                                         children: [
                                                           Text(
                                                             '${controller.files[i].name}',
                                                             style: TextStyle(
                                                                 color:
                                                                 const Color.fromRGBO(
                                                                     26,
                                                                     59,
                                                                     112,
                                                                     1),
                                                                 fontSize: 14.sp,
                                                                 fontWeight:
                                                                 FontWeight
                                                                     .w500),
                                                           ),
                                                           Text(
                                                             "${controller.files[i].name}",
                                                             style: TextStyle(
                                                                 color:
                                                                 const Color.fromRGBO(
                                                                     192,
                                                                     192,
                                                                     192,
                                                                     1),
                                                                 fontSize: 10.sp,
                                                                 fontWeight:
                                                                 FontWeight
                                                                     .w400),
                                                           ),
                                                         ],
                                                       ),
                                                     ),
                                                     Image.asset(
                                                       'assets/images/icon_upfile_subject.png',
                                                       height: 16,
                                                       width: 16,
                                                     ),
                                                   ],
                                                 );
                                             }),
                                         const Divider(),


                                       ],

                                     )
                                 ),
                               )
                                   :Container();
                           }),),


                     const Padding(padding: EdgeInsets.only(top: 12)),
                     Row(
                       children: [
                         Column(
                           children: [
                              Text("Giáo Viên", style: TextStyle(color: const Color.fromRGBO(26,26,26,1), fontSize: 16.sp, fontWeight: FontWeight.w500, fontFamily:'assets/font/static/Inter-Medium.ttf' ),),
                             Visibility(child:
                             Container(
                               width: 16,
                               height: 3,
                               decoration: BoxDecoration(
                                 color: const Color.fromRGBO(26, 59, 112, 1),
                                 borderRadius: BorderRadius.circular(3),
                               ),
                             ),)
                           ],
                         ),
                       ],
                     ),
                     const Divider(),
                     const Padding(padding: EdgeInsets.only(top: 12)),
                     Row(
                       children: [

                         SizedBox(
                           height: 40.h,
                           width: 40,
                           child:  CacheNetWorkCustom(urlImage: '${controller.teacher.value.image}',),
                         ),
                         Container(
                           margin: const EdgeInsets.only(left:8 ),
                           child: Column(
                             crossAxisAlignment: CrossAxisAlignment.start,
                             children: [
                               Text(controller.teacher.value.fullName ?? "",style:  TextStyle(color:const Color.fromRGBO(26, 26, 26, 1), fontSize: 14.sp, fontWeight: FontWeight.w700),),
                               const Padding(padding: EdgeInsets.only(top: 4)),
                               Text('Giáo viên môn ${controller.nameSubject.value}',style:  TextStyle(color:const Color.fromRGBO(72, 98, 141, 1), fontSize: 12.sp, fontWeight: FontWeight.w400),)

                             ],
                           ),
                         ) ,
                       ],
                     ),
                     const Divider(),
                     const Padding(padding: EdgeInsets.only(top:16 )),
                     Text(controller.nameSubject.value, style:  TextStyle(color: const Color.fromRGBO(26, 26, 26, 1), fontWeight: FontWeight.w500, fontSize: 16.sp),),
                     const Padding(padding: EdgeInsets.only(top: 26)),
                     Visibility(
                       visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_EXERCISE),
                       child:InkWell(
                         onTap: (){
                           controller.indexToClick.value =0;
                           controller.goToPageInIndex();
                         },
                         child:   Row(
                           children: [
                              Text('1. Bài Tập', style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 26), fontSize: 14.sp, fontWeight: FontWeight.w500),),
                             Expanded(child: Container()),
                             const Icon(Icons.navigate_next_outlined,color: Color.fromRGBO(26, 26, 26,2),),
                           ],
                         ),
                       ),),

                     const Padding(padding: EdgeInsets.only(top: 36)),
                     Visibility(
                         visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_TRANSCRIPT),
                         child: InkWell(
                           onTap: (){
                             controller.indexToClick.value =1;
                             controller.goToPageInIndex();
                           },
                           child:      Row(
                             children: [
                                Text('2. Điểm', style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 26), fontSize: 14.sp, fontWeight: FontWeight.w500),),
                               Expanded(child: Container()),
                               const Icon(Icons.navigate_next_outlined,color: Color.fromRGBO(26, 26, 26,2),),
                             ],
                           ),
                         )),
                     const Padding(padding: EdgeInsets.only(top: 36)),
                     Visibility(
                         visible: checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_EXAMS),
                         child:   InkWell(
                           onTap: (){
                             controller.indexToClick.value =2;
                             controller.goToPageInIndex();
                           },
                           child:
                           Row(
                             children: [
                                Text('3. Bài Kiểm Tra', style: TextStyle(color: const Color.fromRGBO(26, 26, 26, 26), fontSize: 14.sp, fontWeight: FontWeight.w500),),
                               Expanded(child: Container()),
                               const Icon(Icons.navigate_next_outlined,color: Color.fromRGBO(26, 26, 26,2),),
                             ],
                           )
                           ,
                         ))

                   ],
                 ),
               ),
             )
         ), onWillPop: () async {
           Get.find<ParentHomeController>().subjectId.value ="";
           Get.back();
           return true;
         },)

         : const LoadingCustom()
     );
  }
}