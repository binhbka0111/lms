import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/notification/notification_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/subjects/subjects_controller.dart';

import '../../../../../../commom/utils/app_utils.dart';
import '../../../../../../data/base_service/api_response.dart';
import '../../../../../../data/model/common/learning_managerment.dart';
import '../../../../../../data/repository/learning_managerment/learning_managerment_repo.dart';
import '../../../../../../routes/app_pages.dart';

class ViewExerciseParentController extends GetxController{
  final LearningManagermentRepo _learningRepo = LearningManagermentRepo();

  var focusdateStart = FocusNode().obs;
  var controllerDateStart = TextEditingController().obs;
  var focusdateEnd = FocusNode().obs;
  var controllerDateEnd = TextEditingController().obs;
  var dateStart ="".obs;
  var dateEnd ="".obs;
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;
  var  listExercise = ExerciseStudent().obs;
  RxList<ItemsExercise> items = <ItemsExercise>[].obs;
  var itemLoadMoreEx = <ItemsExercise>[].obs;
  var outputDateformat = DateFormat('dd/MM/yyyy hh:mm:ss');
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var indexHiden = 0.obs;
  var showhiden =<bool>[].obs;
  var statusExercise = "".obs;
  var statusResultExx = "".obs;
  var txtSubmitted ="Đã nộp bài".obs;
  var txtLateSubmission ="Nộp bài muộn".obs;
  var txtNotSubmission ="Không nộp bài".obs;
  var txtUnSubmited ="Chưa nộp bài".obs;
  var txtGraded ="Đã chấm điểm".obs;
  var statusExerciseStudent = ''.obs;
  var subjectDetail = ''.obs;
  var controllerEx = ScrollController();
  var indexPage = 1.obs;

  @override
  void onInit() {
    super.onInit();

    if(DateTime.now().month<=12&&DateTime.now().month>9){
      controllerDateStart.value.text =  outputDateFormat.format(DateTime(Get.find<ParentHomeController>().fromYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<ParentHomeController>().fromYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
    }else{
      controllerDateStart.value.text =   outputDateFormat.format(DateTime(Get.find<ParentHomeController>().toYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<ParentHomeController>().toYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
    }
    getListExerciseStudentToday(statusExercise.value,statusResultExx.value);
    controllerEx = ScrollController()..addListener(_scrollExsecise);

  }

  @override
  dispose() {
    controllerEx.dispose();
    super.dispose();
  }

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }
  getListExerciseStudentToday(status,statusEx){

    var subjectId = "";

    if(Get.find<ParentHomeController>().subjectId.value != "") {
      subjectId = Get.find<ParentHomeController>().subjectId.value;
    }else if(Get.isRegistered<SubjectController>()){
      subjectId = Get.find<SubjectController>().idSubject.value;
    }else{
      subjectId = Get.find<NotificationController>().subjectId;
    }

    var studentId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var fromdate = DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2),),00,00).millisecondsSinceEpoch;
    var todate = DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2)),23,59).millisecondsSinceEpoch;
    _learningRepo.listExerciseStudent(subjectId,status,fromdate, todate,studentId,statusEx,"","").then((value) {
      if (value.state == Status.SUCCESS) {
        listExercise.value= value.object!;
        if(listExercise.value.items != null || listExercise.value.items !=""){
          items.value = listExercise.value.items!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });
  }
  void _scrollExsecise(){
    if(controllerEx.position.pixels == controllerEx.position.maxScrollExtent){
      var subjectId = "";
      if(Get.find<ParentHomeController>().subjectId.value != "") {
        subjectId = Get.find<ParentHomeController>().subjectId.value;
      }else if(Get.isRegistered<SubjectController>()){
        subjectId = Get.find<SubjectController>().idSubject.value;
      }else{
        subjectId = Get.find<NotificationController>().subjectId;
      }
      if(itemLoadMoreEx.isNotEmpty){
        indexPage++;
      }
      _learningRepo.listExerciseStudent( subjectId,"",DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2),),00,00).millisecondsSinceEpoch,
          DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2)),23,59).millisecondsSinceEpoch,
          Get.put(ParentHomeController()).currentStudentProfile.value.id,"",indexPage.value,20).then((value) {
        if (value.state == Status.SUCCESS) {
          itemLoadMoreEx.value =[];
          listExercise.value= value.object!;
          itemLoadMoreEx.value =listExercise.value.items!;
          items.addAll(itemLoadMoreEx);
          items.refresh();
          AppUtils.shared.hideLoading();
          Future.delayed(const Duration(seconds: 1), () {
          });
        } else {
        }

      });

    }
  }






  getStatusExercise(status){
    switch(status){
      case 'CONSTRUCTED_RESPONSE':
        return "Tự luận";
      case 'SELECTED_RESPONSE':
        return"Trắc nghiệm";
      default:
        return "Trắc nghiêm & tự luận";

    }

  }

  setStastusExercise(type){
    switch(type){
      case "Đã nộp bài":
        return "SUBMITTED";
      case "Chưa nộp bài":
        return "UNSUBMITTED";
      default:
        return "";
    }
  }

  setTypeExercise(type){
    switch(type){
      case "Đến hạn":
        return "DUE";
      case "Đã hết hạn":
        return "EXPIRED";
      case "Sắp đến hạn":
        return "DEADLINE_COMING_SOON";
      default:
        return "";
    }
  }
  showTypeExercise(type){
    switch(type){
      case "DUE":
        return "Đến hạn";
      case "EXPIRED":
        return "Đã hết hạn";
      case "DEADLINE_COMING_SOON":
        return "Sắp đến hạn";
      default:
        return "";
    }
  }

  getTextStatus(state) {
    switch (state) {
      case "SUBMITTED":
        return txtSubmitted;
      case "LATE_SUBMISSION":
        return txtSubmitted;
      case "NOT_SUBMISSION":
        return txtUnSubmited;
      case "UNSUBMITTED":
        return txtUnSubmited;
      default:
        return txtGraded;
    }
  }


  showColorTypeExercise(type){
    switch(type){
      case "DUE":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "EXPIRED":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "DEADLINE_COMING_SOON":
        return ColorUtils.PRIMARY_COLOR;
      default:
        return "";
    }
  }



  getColorTextStatus(state) {
    switch (state) {
      case "SUBMITTED":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "LATE_SUBMISSION":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "NOT_SUBMISSION":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "UNSUBMITTED":
        return const Color.fromRGBO(255, 69, 89, 1);
      default:
        return const Color.fromRGBO(136, 136, 136, 1);
    }
  }


  goToDetailExerciseStudentPage(index){
    Get.toNamed(Routes.detailExerciseParentPage,arguments: items[index].id);
  }

  goToStudentDoingExercise(index){
    Get.toNamed(Routes.studentDoingExercise,arguments: items[index].id);
  }
  getDetailEx(index){

        goToDetailExerciseStudentPage(index);

  }


}