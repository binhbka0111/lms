import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/constants/date_format.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/date_time_picker.dart';
import 'package:slova_lms/commom/utils/time_utils.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/detail_subject_parent/detail_subject_parent_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/detail_subject_parent/view_exam_parent/view_exam_parent_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';

import '../../../../../../commom/utils/click_subject_teacher.dart';

class ViewExamParentPage extends GetWidget<ViewExamParentController>{
  final controller = Get.put(ViewExamParentController());

  @override
  Widget build(BuildContext context) {
 return   Scaffold(
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
        appBar: AppBar(
          actions: [
            IconButton(
              onPressed: () {
                Get.find<ParentHomeController>().subjectId.value = "";
                Get.delete<DetailSubjectParentController>();
                Get.delete<ViewExamParentController>();
                comeToHome();
              },
              icon: const Icon(Icons.home),
              color: Colors.white,
            )
          ],
          backgroundColor: ColorUtils.PRIMARY_COLOR,
          title: Text(
            'Bài Kiểm Tra',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontFamily: 'static/Inter-Medium.ttf'),
          ),
        ),
        body:
        Obx(() =>
            RefreshIndicator(
              onRefresh: ()async {
                controller.getListExamStudentToday(controller.statusExercise.value,"");
              },
              child:
              SingleChildScrollView(
                controller: controller.controllerExam,
                child:  Column(
                  children: [
                    Container(
                      height: 60.h,
                      padding: EdgeInsets.symmetric(vertical: 14.h),
                      color: Colors.white,
                      child: Row(
                        children: [
                          Expanded(
                              child:
                              Container(
                                margin: EdgeInsets.only(left: 16.w),
                                child:
                                TextFormField(
                                  style: TextStyle(
                                    fontSize: 12.0.sp,
                                    color: const Color.fromRGBO(26, 26, 26, 1),
                                  ),
                                  onTap: () {
                                    selectDateTimeStart(controller.controllerDateStart.value.text, DateTimeFormat.formatDateShort,context);
                                  },
                                  cursorColor: ColorUtils.PRIMARY_COLOR,
                                  controller:
                                  controller.controllerDateStart.value,
                                  readOnly: true,
                                  decoration: InputDecoration(
                                    suffixIcon: Container(
                                      margin: EdgeInsets.zero,
                                      child: SvgPicture.asset(
                                        "assets/images/icon_date_picker.svg",
                                        fit: BoxFit.scaleDown,
                                      ),
                                    ),
                                    labelText: "Từ ngày",
                                    border: const OutlineInputBorder(),
                                    labelStyle: TextStyle(
                                        color: const Color.fromRGBO(177, 177, 177, 1),
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: 'assets/font/static/Inter-Medium.ttf'
                                    ),
                                  ),
                                ),
                              )
                          ),
                          Expanded(
                            child:
                            Container(
                              margin: EdgeInsets.only(left: 16.w),
                              child:
                              TextFormField(
                                style: TextStyle(
                                  fontSize: 12.0.sp,
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                ),
                                onTap: () {
                                  selectDateTimeEnd(controller.controllerDateEnd.value.text, DateTimeFormat.formatDateShort,context);
                                },
                                cursorColor: ColorUtils.PRIMARY_COLOR,
                                controller:
                                controller.controllerDateEnd.value,
                                readOnly: true,
                                decoration: InputDecoration(
                                  suffixIcon: Container(
                                    margin: EdgeInsets.zero,
                                    child: SvgPicture.asset(
                                      "assets/images/icon_date_picker.svg",
                                      fit: BoxFit.scaleDown,
                                    ),
                                  ),
                                  labelText: "Đến ngày",
                                  border: const OutlineInputBorder(),
                                  labelStyle: TextStyle(
                                      color: const Color.fromRGBO(177, 177, 177, 1),
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: 'assets/font/static/Inter-Medium.ttf'
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(left: 16.w)),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 16.w),
                      padding: EdgeInsets.symmetric(horizontal: 16.w),
                      height: 40,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                              color: const Color.fromRGBO(192, 192, 192, 1)),
                          borderRadius: BorderRadius.circular(8)),
                      child: Theme(
                        data: Theme.of(context).copyWith(
                          canvasColor: Colors.white,
                        ),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              isExpanded: true,
                              iconSize: 0,
                              icon: const Visibility(
                                  visible: false,
                                  child: Icon(Icons.arrow_downward)),
                              elevation: 16,
                              hint: controller.statusResultExx.value != ""
                                  ? Row(
                                children: [
                                  Text(
                                    controller.statusResultExx.value,
                                    style: TextStyle(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w400,
                                        color: ColorUtils.PRIMARY_COLOR),
                                  ),
                                  Expanded(child: Container()),
                                  const Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.black,
                                    size: 18,
                                  )
                                ],
                              )
                                  : Row(
                                children: [
                                  Text(
                                    'Trạng thái nộp bài',
                                    style: TextStyle(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.black),
                                  ),
                                  Expanded(child: Container()),
                                  const Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.black,
                                    size: 18,
                                  )
                                ],
                              ),
                              items: ["Tất cả","Đã nộp bài","Chưa nộp bài"].map(
                                    (value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(
                                      value,
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w400,
                                          color: ColorUtils.PRIMARY_COLOR),
                                    ),
                                  );
                                },
                              ).toList(),
                              onChanged: (String? value) {
                                controller.statusResultExx.value = value!;
                                controller.items.clear();
                                controller.getListExamStudentToday("", controller.setStastusExercise(controller.statusResultExx.value));
                                controller.items.refresh();
                              },
                            )),
                      ),
                    ),
                    const Padding(padding: EdgeInsets.only(top: 8)),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 16.w),
                      padding: EdgeInsets.symmetric(horizontal: 16.w),
                      height: 40,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                              color: const Color.fromRGBO(192, 192, 192, 1)),
                          borderRadius: BorderRadius.circular(8)),
                      child: Theme(
                        data: Theme.of(context).copyWith(
                          canvasColor: Colors.white,
                        ),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              isExpanded: true,
                              iconSize: 0,
                              icon: const Visibility(
                                  visible: false,
                                  child: Icon(Icons.arrow_downward)),
                              elevation: 16,
                              hint: controller.statusExercise.value != ""
                                  ? Row(
                                children: [
                                  Text(
                                    controller.statusExercise.value,
                                    style: TextStyle(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w400,
                                        color: ColorUtils.PRIMARY_COLOR),
                                  ),
                                  Expanded(child: Container()),
                                  const Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.black,
                                    size: 18,
                                  )
                                ],
                              )
                                  : Row(
                                children: [
                                  Text(
                                    'Hạn Nộp',
                                    style: TextStyle(
                                        fontSize: 14.sp,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.black),
                                  ),
                                  Expanded(child: Container()),
                                  const Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.black,
                                    size: 18,
                                  )
                                ],
                              ),
                              items: ["Tất cả","Đến hạn","Đã hết hạn","Sắp đến hạn"].map(
                                    (value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(
                                      value,
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.w400,
                                          color: ColorUtils.PRIMARY_COLOR),
                                    ),
                                  );
                                },
                              ).toList(),
                              onChanged: (String? value) {
                                controller.statusExercise.value = value!;
                                controller.items.clear();
                                controller.getListExamStudentToday(controller.setTypeExercise(controller.statusExercise.value),controller.setStastusExercise(controller.statusResultExx.value));
                                controller.items.refresh();
                              },
                            )),
                      ),
                    ),
                    Visibility(
                        visible:  checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_EXAM_LIST),
                        child: Container(
                      margin: const EdgeInsets.only( right: 16, left: 16),
                      child: ListView.builder(
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount:controller.items.length,
                          itemBuilder: (context, index) {
                            return
                              InkWell(
                                onTap: (){
                                  checkClickFeature(Get.find<HomeController>().userGroupByApp, () => controller.getDetailExamParent(index), StringConstant.FEATURE_EXAM_DETAIL);
                                },
                                child:   Column(
                                  children: [
                                    const Padding(padding: EdgeInsets.only(top: 16)),

                                    const Padding(padding: EdgeInsets.only(top: 8)),
                                    Row(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(bottom: 24.h),
                                          child:  Image.asset('assets/images/icon_subject.png',width: 24,height: 24,),
                                        ),
                                        const Padding(padding: EdgeInsets.only(right: 8)),
                                        Expanded(child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${controller.items[index].title}',
                                              style:  TextStyle(
                                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                                  fontSize: 14.sp,
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily:
                                                  'assets/font/static/Inter-Medium.ttf'),
                                            ),
                                            const Padding(padding: EdgeInsets.only(top: 4)),
                                            RichText(
                                                text: TextSpan(children: [
                                                   TextSpan(
                                                      text: 'Thời gian bắt đầu: ',
                                                      style: TextStyle(
                                                          color: const Color.fromRGBO(133, 133, 133, 1),
                                                          fontWeight: FontWeight.w500,
                                                          fontSize: 12.sp,
                                                          fontFamily:
                                                          'assets/font/static/Inter-Medium.ttf')),
                                                  TextSpan(
                                                      text:
                                                      controller.outputDateformat.format(DateTime.fromMillisecondsSinceEpoch(controller.items[index].startTime!)),
                                                      style:  TextStyle(
                                                          color: const Color.fromRGBO(26, 26, 26, 1),
                                                          fontSize: 12.sp,
                                                          fontWeight: FontWeight.w500,
                                                          fontFamily:
                                                          'assets/font/static/Inter-Medium.ttf'))
                                                ])),
                                            const Padding(padding: EdgeInsets.only(top: 4)),
                                            RichText(
                                                text: TextSpan(children: [
                                                   TextSpan(
                                                      text: 'Thời gian kết thúc: ',
                                                      style: TextStyle(
                                                          color: const Color.fromRGBO(133, 133, 133, 1),
                                                          fontWeight: FontWeight.w500,
                                                          fontSize: 12.sp,
                                                          fontFamily:
                                                          'assets/font/static/Inter-Medium.ttf')),
                                                  TextSpan(
                                                      text:
                                                      controller.outputDateformat.format(DateTime.fromMillisecondsSinceEpoch(controller.items[index].endTime!)),
                                                      style:  TextStyle(
                                                          color: const Color.fromRGBO(26, 26, 26, 1),
                                                          fontSize: 12.sp,
                                                          fontWeight: FontWeight.w500,
                                                          fontFamily:
                                                          'assets/font/static/Inter-Medium.ttf'))
                                                ])),
                                            const Padding(padding: EdgeInsets.only(top: 4)),
                                            RichText(
                                                text:  TextSpan(children: [
                                                   TextSpan(
                                                      text: 'Loại bài tập: ',
                                                      style: TextStyle(
                                                          color: const Color.fromRGBO(133, 133, 133, 1),
                                                          fontWeight: FontWeight.w500,
                                                          fontSize: 12.sp,
                                                          fontFamily:
                                                          'assets/font/static/Inter-Medium.ttf')),
                                                  TextSpan(
                                                      text: '${controller.getStatusExercise(controller.items[index].typeExercise)}',
                                                      style:  TextStyle(
                                                          color: ColorUtils.PRIMARY_COLOR,
                                                          fontSize: 12.sp,
                                                          fontWeight: FontWeight.w500,
                                                          fontFamily:
                                                          'assets/font/static/Inter-Medium.ttf'))
                                                ])),
                                            const Padding(padding: EdgeInsets.only(top: 4)),
                                            RichText(
                                                text:  TextSpan(children: [
                                                   TextSpan(
                                                      text: 'Hạn nộp: ',
                                                      style: TextStyle(
                                                          color: const Color.fromRGBO(133, 133, 133, 1),
                                                          fontWeight: FontWeight.w500,
                                                          fontSize: 12.sp,
                                                          fontFamily:
                                                          'assets/font/static/Inter-Medium.ttf')),
                                                  TextSpan(
                                                      text: '${controller.showTypeExercise(controller.items[index].status)}',
                                                      style:  TextStyle(
                                                          color: controller.showColorTypeExercise(controller.items[index].status),
                                                          fontSize: 12.sp,
                                                          fontWeight: FontWeight.w500,
                                                          fontFamily:
                                                          'assets/font/static/Inter-Medium.ttf'))
                                                ])),
                                            const Padding(padding: EdgeInsets.only(top: 4)),
                                            RichText(
                                                text:  TextSpan(children: [
                                                   TextSpan(
                                                      text: 'Trạng thái nộp bài: ',
                                                      style: TextStyle(
                                                          color: const Color.fromRGBO(133, 133, 133, 1),
                                                          fontWeight: FontWeight.w500,
                                                          fontSize: 12.sp,
                                                          fontFamily:
                                                          'assets/font/static/Inter-Medium.ttf')),
                                                  TextSpan(
                                                      text: controller.items[index].isSubmit == "TRUE"? 'Đã nộp bài':"Chưa nộp bài",
                                                      style:  TextStyle(
                                                          color:controller.items[index].isSubmit == "TRUE" ?  const Color.fromRGBO(77, 197, 145, 1) :const Color.fromRGBO(255, 69, 89, 1) ,
                                                          fontSize: 12.sp,
                                                          fontWeight: FontWeight.w500,
                                                          fontFamily:
                                                          'assets/font/static/Inter-Medium.ttf'))
                                                ])),
                                          ],
                                        )),

                                      ],
                                    ),

                                  ],
                                ),
                              );
                          }),
                    ))
                  ],
                ),
              ),
            )
        )
    );
  }
  selectDateTimeStart(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy')
                .parse(controller.controllerDateEnd.value.text)
                .millisecondsSinceEpoch) {
          controller.controllerDateStart.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian Từ ngày nhỏ hơn thời gian Đến ngày");
        }
      } else {
        controller.controllerDateStart.value.text = date;
      }
      controller.getListExamStudentToday(controller.setTypeExercise(controller.statusExercise.value),"");

    }
  }

  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy')
            .parse(controller.controllerDateStart.value.text)
            .millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch) {
          controller.controllerDateEnd.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian Đến ngày lớn hơn thời gian Từ ngày");
        }
      } else {
        controller.controllerDateEnd.value.text = date;
      }

    }
    controller.getListExamStudentToday(controller.setTypeExercise(controller.statusExercise.value),"");

  }
}