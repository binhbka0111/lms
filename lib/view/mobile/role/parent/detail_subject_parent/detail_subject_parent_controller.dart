import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/ViewPdf.dart';
import 'package:slova_lms/commom/utils/open_url.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';
import 'package:slova_lms/view/mobile/role/student/subjects/subjects_controller.dart';
import '../../../../../data/base_service/api_response.dart';
import '../../../../../data/model/common/subject.dart';
import '../../../../../data/repository/subject/class_office_repo.dart';
import '../../../../../routes/app_pages.dart';
import '../../../notification/notification_controller.dart';

class DetailSubjectParentController extends GetxController{
  var indexToClick =0.obs;
  var nameSubject = "".obs;
  var subjectId ="".obs;
  final ClassOfficeRepo _subjectRepo = ClassOfficeRepo();
  var detailSubject = DetailSubject().obs;
  var teacher = DetailSubjectTeacher().obs;
  var category = SubjectCategory().obs;
  var notificationSubject = ShowNotification().obs;
  RxList<Items> items = <Items>[].obs;
  RxList<Files> files = <Files>[].obs;
  var isReady = false.obs;


  goToPageInIndex(){
    if(indexToClick.value==0){
      Get.toNamed(Routes.viewExerciseParentPage, arguments: subjectId.value);
    }else if(indexToClick.value==1){
      Get.toNamed(Routes.viewTranscriptSubjectParentPage);
    }else{
      Get.toNamed(Routes.viewExamParentPage, arguments: subjectId.value);

    }
  }

  @override
  void onInit() {
    super.onInit();

    getDetailSubjectParent();
    getNotificationSubject();
  }

  getDetailSubjectParent() async{
    var subjectId = "";
    if(Get.find<ParentHomeController>().subjectId.value != "") {
      subjectId = Get.find<ParentHomeController>().subjectId.value;
    }else if(Get.isRegistered<SubjectController>()){
        subjectId = Get.find<SubjectController>().idSubject.value;
      }else{
      subjectId = Get.find<NotificationController>().subjectId;
    }


    await _subjectRepo.detailSubject(subjectId).then((value) {
      if (value.state == Status.SUCCESS) {
        detailSubject.value = value.object!;
        if(detailSubject.value.teacher != null){
          teacher.value =detailSubject.value.teacher!;
        }else{
          teacher.value.fullName ='';
          teacher.value.image ='';
        }

        if(detailSubject.value.subjectCategory?.name != "" || detailSubject.value.subjectCategory?.name !=null){
          nameSubject.value = detailSubject.value.subjectCategory!.name!;
        }

      }
      isReady.value = true;
    });
  }



  getAttackFile(index,i){
    var action = 0;
    if (items[index].files?[i].ext == "png" ||
        items[index].files?[i].ext  == "jpg" ||
        items[index].files?[i].ext  == "jpeg" ||
        items[index].files?[i].ext  == "gif" ||
        items[index].files?[i].ext  == "bmp") {
      action = 1;
    } else if (items[index].files?[i].ext == "pdf") {
      action = 2;
    } else {
      action = 0;
    }
    switch (action) {
      case 1:
        OpenUrl.openImageViewer(Get.context!,items[index].files?[i].link !);
        break;
      case 2:
        Get.to(ViewPdfPage(url: items[index].files![i].link!));
        break;
      default:
        OpenUrl.openFile(items[index].files![i].link!);
        break;
    }
  }


  getNotificationSubject(){
    var subjectId = "";
    if(Get.find<ParentHomeController>().subjectId.value != ""){
      if(Get.isRegistered<SubjectController>()){
        subjectId = Get.find<SubjectController>().idSubject.value;
      }else{
        subjectId = Get.find<ParentHomeController>().subjectId.value;
      }
    }else{
      subjectId = Get.find<NotificationController>().subjectId;
    }
    var idClass = Get.put(ParentHomeController()).currentStudentProfile.value.clazz?[0].id;
    _subjectRepo.notificationSubject(subjectId, idClass).then((value) {
      if (value.state == Status.SUCCESS) {
        notificationSubject.value = value.object!;
        if(notificationSubject.value.items != null){
          items.value = notificationSubject.value.items!;
        }
      }else{
      }
    });
  }
  getIconFile(ext){
    switch(ext){
      case "jpg":
        return "assets/icons/file/icon_image.png";
      case "png":
        return "assets/icons/file/icon_image.png";
      case "docx":
        return "assets/icons/file/icon_word.png";
      case "doc":
        return "assets/icons/file/icon_word.png";
      case "xls":
        return "assets/icons/file/icon_excel.png";
      case "xlsx":
        return "assets/icons/file/icon_excel.png";
      case "zip":
        return "assets/images/icon_image_zip.png";
      case "rar":
        return "assets/images/icon_image_rar.png";
      case "pptx":
        return "assets/icons/file/icon_ppt.png";
      case "ppt":
        return "assets/icons/file/icon_ppt.png";
      case "pdf":
        return "assets/icons/file/icon_pdf.png";
      default:
        return "assets/icons/file/icon_unknow.png";
    }

  }
  goToDetailNotificationSubject(index){
    Get.toNamed(Routes.detailNotificationSubjectPage, arguments: items[index].id);
  }

  getDetailNotifySubject(index){
    goToDetailNotificationSubject(index);
  }



}

