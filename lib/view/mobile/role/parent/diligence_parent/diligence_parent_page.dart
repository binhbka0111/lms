import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/role/parent/diligence_parent/diligence_parent_controller.dart';

class DiligenceParentPage extends GetWidget<DiligenceParentController> {

  final controller = Get.put(DiligenceParentController());

  DiligenceParentPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
      appBar: AppBar(
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Text(
          'Chuyên Cần',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 16.h, right: 16.h, left: 16.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Chuyên Cần',
              style: TextStyle(
                  color: const Color.fromRGBO(177, 177, 177, 1),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'static/Inter-Medium.ttf',
                  fontSize: 12.sp),
            ),
            Padding(padding: EdgeInsets.only(top: 8.h)),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.r),
                  color: const Color.fromRGBO(255, 255, 255, 1)),
              width: double.infinity,
              alignment: Alignment.centerLeft,
              height: 80.h,
              child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: controller.listIcon.length,
                      itemBuilder: (context, index){
                        return
                         Container(
                           margin:  EdgeInsets.symmetric(horizontal: 16.w,vertical: 16.h),
                           child:
                             InkWell(
                               onTap: () {
                                 if(index == controller.listIcon.indexWhere((element) => element.name == controller.nameApproval)){
                                   controller. goToApprovalPage();
                                 }
                                 if(index == controller.listIcon.indexWhere((element) => element.name == controller.nameDiary)){
                                   controller. goToDiaryPage();
                                 }
                                 if(index == controller.listIcon.indexWhere((element) => element.name == controller.nameStatistical)){
                                   controller. goToStatisticalPage();
                                 }
                               },
                               child:
                               SizedBox(
                                 height: 76.h,
                                 child: Column(
                                   children: [
                                     Image.asset('${controller.listIcon[index].image  }',width: 24.w,height: 24.h,),
                                     Padding(padding: EdgeInsets.only(top: 8.h)),
                                     Text(
                                       '${controller.listIcon[index].name}',
                                       style: TextStyle(
                                           color: const Color.fromRGBO(26, 26, 26, 1),
                                           fontSize: 12.sp,
                                           fontWeight: FontWeight.w500,
                                           fontFamily:
                                           'assets/font/static/Inter-Medium.ttf'),
                                     )
                                   ],
                                 ),
                               ),
                             )
                         );
                      }),
            )
          ],
        ),
      ),
    );
  }
}
