
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/date_format.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/date_time_picker.dart';
import 'package:slova_lms/commom/utils/time_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/diligence_parent/diary_parent/diary_parent_controller.dart';
import 'package:flutter_svg/svg.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_page.dart';


class DiaryParentPage extends GetWidget<DiaryParentController> {
  final controller = Get.put(DiaryParentController());

  DiaryParentPage({super.key});
  @override
  Widget build(BuildContext context) {
    return
  Obx(() =>  Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              Get.to(ParentHomePage());
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Text(
          'Nhật Ký Chuyên Cần',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body:
     RefreshIndicator(
       onRefresh: () async{
         controller.onRefresh();
       },
       child:
       Column(
         children: [
           Container(
             height: 60.h,
             padding: EdgeInsets.symmetric(vertical: 14.h),
             color: Colors.white,
             child: Row(
               children: [
                 Expanded(
                   child:
                   Container(
                     margin: EdgeInsets.only(left: 16.w),
                     child:
                     TextFormField(
                       style: TextStyle(
                         fontSize: 12.0.sp,
                         color: const Color.fromRGBO(26, 26, 26, 1),
                       ),
                       onTap: () {
                         selectDateTimeStart(controller.controllerdateStart.value.text, DateTimeFormat.formatDateShort,context);
                       },
                       cursorColor: ColorUtils.PRIMARY_COLOR,
                       controller:
                       controller.controllerdateStart.value,
                       readOnly: true,
                       decoration: InputDecoration(
                         suffixIcon: Container(
                           margin: EdgeInsets.zero,
                           child: SvgPicture.asset(
                             "assets/images/icon_date_picker.svg",
                             fit: BoxFit.scaleDown,
                           ),
                         ),
                         labelText: "Từ ngày",
                         border: const OutlineInputBorder(),
                         labelStyle: TextStyle(
                             color: const Color.fromRGBO(177, 177, 177, 1),
                             fontSize: 14.sp,
                             fontWeight: FontWeight.w500,
                             fontFamily: 'assets/font/static/Inter-Medium.ttf'
                         ),
                       ),
                     ),
                   )
                 ),
                 Expanded(
                   child:
                   Container(
                     margin: EdgeInsets.only(left: 16.w),
                     child:
                     TextFormField(
                       style: TextStyle(
                         fontSize: 12.0.sp,
                         color: const Color.fromRGBO(26, 26, 26, 1),
                       ),
                       onTap: () {
                         selectDateTimeEnd(controller.controllerdateEnd.value.text, DateTimeFormat.formatDateShort,context);
                       },
                       cursorColor: ColorUtils.PRIMARY_COLOR,
                       controller:
                       controller.controllerdateEnd.value,
                       readOnly: true,
                       decoration: InputDecoration(
                         suffixIcon: Container(
                           margin: EdgeInsets.zero,
                           child: SvgPicture.asset(
                             "assets/images/icon_date_picker.svg",
                             fit: BoxFit.scaleDown,
                           ),
                         ),
                         labelText: "Đến ngày",
                         border: const OutlineInputBorder(),
                         labelStyle: TextStyle(
                             color: const Color.fromRGBO(177, 177, 177, 1),
                             fontSize: 14.sp,
                             fontWeight: FontWeight.w500,
                             fontFamily: 'assets/font/static/Inter-Medium.ttf'
                         ),
                       ),
                     ),
                   ),
                 ),
                 Padding(padding: EdgeInsets.only(left: 8.w)),
                 InkWell(
                   onTap: () {
                     Get.bottomSheet(onBottomSheetStatus());
                   },
                   child: Container(
                     margin: EdgeInsets.only(right: 8.w),
                     height: 30.h,
                     width: 30.h,
                     alignment: Alignment.center,
                     decoration: BoxDecoration(
                       borderRadius: BorderRadius.circular(6),
                       color: ColorUtils.PRIMARY_COLOR,
                     ),
                     child:  const Icon(
                       Icons.filter_alt,
                       size: 18,
                       color: Colors.white,
                     ),
                   ),
                 )
               ],
             ),
           ),
           Expanded(
               child: Container(
                 padding: EdgeInsets.only(right: 16.w, left: 16.w),
                 color: const Color.fromRGBO(245, 245, 245, 1),
                 child: Visibility(
                     visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_DIARY_DILIGENCE_LIST),
                     child: detail_list()),
               ))],
       ),
     )
  ));
  }

  ListView detail_list() {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: controller.item.length,
        itemBuilder: (context, index) {
          return
         InkWell(
           onTap: (){
             checkClickFeature(Get.find<HomeController>().userGroupByApp,() => controller.goToDetailStatisticalParentOnSchoolPage(index), StringConstant.FEATURE_DIARY_DILIGENCE_DETAIL);
           },
           child:   Container(
             padding:
             EdgeInsets.only(top: 5.h, left: 16.w, right: 16.w, bottom: 4.h),
             margin: EdgeInsets.only(top: 9.h, bottom: 9.h),
             decoration: BoxDecoration(
                 color: const Color.fromRGBO(255, 255, 255, 1),
                 borderRadius: BorderRadius.circular(6)),
             child: Column(
               children: [
                 Row(
                   children: [
                     SizedBox(
                         height: 40.h,
                         width: 40.h,
                         child:
                         CacheNetWorkCustom(urlImage: '${ controller.item[index].student?.image}',)

                     ),
                     Padding(padding: EdgeInsets.only(right: 4.w)),
                     Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         Text("${controller.item[index].student!.fullName}",
                             style: styleTitleName()),
                         Visibility(
                           visible: controller.item[index].student!.birthday != null,
                             child: Text(' ${controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.item[index].student!.birthday??0))}',
                             style: styleContent()))
                       ],
                     ),
                     Expanded(child: Container())
                     ,Text(controller.outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.item[index].date!)), style: styleContent(),)
                   ],
                 ),
                 Padding(padding: EdgeInsets.only(top: 9.h)),
                 Row(
                   children: [
                     Text(
                         'Điểm danh lúc ${controller.outputFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.item[index].updatedAt!))} ',
                         style: styleContent()),
                     Expanded(child: Container()),
                     Text(
                       '${controller.getTextStatus(controller.item[index].statusDiligent)}',
                       style: TextStyle(
                           color: controller.getColorTextStatus(
                               controller.item[index].statusDiligent),
                           fontWeight: FontWeight.w500,
                           fontSize: 12.sp,
                           fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                     )
                   ],
                 ),
                 Padding(
                   padding: EdgeInsets.only(top: 4.h),
                 ),
               ],
             ),
           ),
         );
        });
  }


  styleTitleName() {
    return TextStyle(
        color: const Color.fromRGBO(26, 26, 26, 1),
        fontWeight: FontWeight.w500,
        fontSize: 12.sp,
        fontFamily: 'assets/font/static/Inter-Medium.ttf');
  }

  styleContent() {
    return TextStyle(
        color: const Color.fromRGBO(177, 177, 177, 1),
        fontFamily: 'assets/font/static/Inter-Regular.ttf',
        fontSize: 12.sp,
        fontWeight: FontWeight.w400);
  }

  styleTextStatusBottomSheet() {
    return TextStyle(
        color: const Color.fromRGBO(255, 255, 255, 1),
        fontWeight: FontWeight.w400,
        fontSize: 12.sp,
        fontFamily: 'assets/font/static/Inter-Regular.ttf');
  }

  onBottomSheetStatus(){
    return
      Obx(() =>  Container(
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(topRight: Radius.circular(16), topLeft: Radius.circular(16)),
            color: Colors.white
        ),
        child:  Column(
          children: <Widget>[
            Container(
              height: 6.h,
              width: 64.w,
              margin: const EdgeInsets.only(top: 16),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  color: const Color.fromRGBO(242, 243, 249, 1)
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 16.w, left: 16.w,top: 16.h),
              child:Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Trạng Thái', style:  TextStyle(color: const Color.fromRGBO(177, 177, 177, 1), fontSize: 12.sp, fontWeight: FontWeight.w500, fontFamily:'assets/font/static/Inter-Medium.ttf' ),),
                  Padding(padding: EdgeInsets.only(top: 16.h)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(child: InkWell(
                        onTap: (){
                          controller.indexHiden.value =0;
                          controller.showBorder(controller.indexHiden.value);
                        },
                        child:
                        Container(
                          height: 32.h,
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(right: 10.w),
                          padding: EdgeInsets.only(top: 6.h, bottom: 6.h, left: 10.h, right: 10.h),
                          decoration: BoxDecoration(
                              color:    controller.indexHiden.value ==0 ? ColorUtils.PRIMARY_COLOR: const Color.fromRGBO(255, 255, 255, 1),
                              borderRadius: BorderRadius.circular(6),
                              border:  Border.all(color: controller.indexHiden.value ==0 ? ColorUtils.PRIMARY_COLOR :const Color.fromRGBO(177, 177, 177, 1))
                          ),
                          child: Text('Tất Cả', style: TextStyle( color: controller.indexHiden.value ==0 ? const Color.fromRGBO(255, 255, 255, 1): const Color.fromRGBO(177, 177, 177, 1), fontWeight: FontWeight.w400, fontSize: 12.sp, fontFamily:'assets/font/static/Inter-Regular.ttf' ),),
                        ),
                      )),
                     Expanded(child:  InkWell(
                       onTap: (){
                         controller.indexHiden.value =1;
                         controller.showBorder(controller.indexHiden.value);
                       },
                       child:
                       Container(
                         height: 32.h,
                         alignment: Alignment.center,
                         margin: EdgeInsets.only(right: 10.w),
                         decoration: BoxDecoration(
                             color:    controller.indexHiden.value ==1 ? ColorUtils.PRIMARY_COLOR: const Color.fromRGBO(255, 255, 255, 1),
                             borderRadius: BorderRadius.circular(6),
                             border:  Border.all(color: controller.indexHiden.value ==1 ? ColorUtils.PRIMARY_COLOR :const Color.fromRGBO(177, 177, 177, 1))
                         ),
                         child: Text('Đi Học Đúng Giờ', style: TextStyle( color: controller.indexHiden.value ==1 ? const Color.fromRGBO(255, 255, 255, 1): const Color.fromRGBO(177, 177, 177, 1), fontWeight: FontWeight.w400, fontSize: 12.sp, fontFamily:'assets/font/static/Inter-Regular.ttf' ),),
                       ),
                     )),
                     Expanded(child:  InkWell(
                       onTap: (){
                         controller.indexHiden.value =2;
                         controller.showBorder(controller.indexHiden.value);
                       },
                       child:     Container(
                         height: 32.h,
                         alignment: Alignment.center,
                         margin: EdgeInsets.only(right: 10.w),
                         padding: EdgeInsets.only(top: 6.h, bottom: 6.h, left: 10.w, right: 10.w),
                         decoration: BoxDecoration(
                             color:    controller.indexHiden.value ==2 ? ColorUtils.PRIMARY_COLOR: const Color.fromRGBO(255, 255, 255, 1),
                             borderRadius: BorderRadius.circular(6),
                             border:  Border.all(color: controller.indexHiden.value ==2 ? ColorUtils.PRIMARY_COLOR :const Color.fromRGBO(177, 177, 177, 1))
                         ),
                         child: Text('Đi Học Muộn', style: TextStyle( color: controller.indexHiden.value ==2 ? const Color.fromRGBO(255, 255, 255, 1): const Color.fromRGBO(177, 177, 177, 1), fontWeight: FontWeight.w400, fontSize: 12.sp, fontFamily:'assets/font/static/Inter-Regular.ttf' ),),
                       ),
                     )),

                    ],
                  ),
                  Padding(padding: EdgeInsets.only(top: 16.h)),
                  Row(
                    children: [
                      Expanded(child: InkWell(
                        onTap: (){
                          controller.indexHiden.value =3;
                          controller.showBorder(controller.indexHiden.value);

                        },
                        child:   Container(
                          height: 32.h,
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(right: 10.w),
                          padding: EdgeInsets.only(top: 6.h, bottom: 6.h, left: 10.w, right: 10.w),
                          decoration: BoxDecoration(
                              color:    controller.indexHiden.value ==3 ? ColorUtils.PRIMARY_COLOR: const Color.fromRGBO(255, 255, 255, 1),
                              borderRadius: BorderRadius.circular(6),
                              border:  Border.all(color: controller.indexHiden.value ==3 ? ColorUtils.PRIMARY_COLOR :const Color.fromRGBO(177, 177, 177, 1))
                          ),
                          child: Text('Nghỉ Có Phép', style: TextStyle( color: controller.indexHiden.value ==3 ? const Color.fromRGBO(255, 255, 255, 1): const Color.fromRGBO(177, 177, 177, 1), fontWeight: FontWeight.w400, fontSize: 12.sp, fontFamily:'assets/font/static/Inter-Regular.ttf' ),),
                        ),
                      )),
                      Expanded(child: InkWell(
                        onTap: (){
                          controller.indexHiden.value =4;
                          controller.showBorder(controller.indexHiden.value);

                        },
                        child:  Container(
                          height: 32.h,
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(right: 10.w),
                          decoration: BoxDecoration(
                              color:    controller.indexHiden.value ==4 ? ColorUtils.PRIMARY_COLOR: const Color.fromRGBO(255, 255, 255, 1),
                              borderRadius: BorderRadius.circular(6),
                              border:  Border.all(color: controller.indexHiden.value ==4 ? ColorUtils.PRIMARY_COLOR :const Color.fromRGBO(177, 177, 177, 1))
                          ),
                          child: Text('Nghỉ Không Phép', style: TextStyle( color: controller.indexHiden.value ==4 ? const Color.fromRGBO(255, 255, 255, 1): const Color.fromRGBO(177, 177, 177, 1), fontWeight: FontWeight.w400, fontSize: 12.sp, fontFamily:'assets/font/static/Inter-Regular.ttf' ),),
                        ),
                      )),
                      Expanded(child: Container(),),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(child: Container()),
            Container(
              margin: EdgeInsets.only(right: 16.w, left: 16.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(child:
                  SizedBox(
                    height: 36.h,
                    child:  ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
                            side: const BorderSide(
                                color: ColorUtils.PRIMARY_COLOR,width: 1
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(6),
                            )
                        ),
                        onPressed: (){
                          controller.item.clear();
                          controller.getListDiligenceAll( DateTime(int.parse(controller.dateStart.value.substring(6,10)), int.parse(controller.dateStart.value.substring(4,5)),int.parse(controller.dateStart.value.substring(1,2))).millisecondsSinceEpoch,
                              DateTime(int.parse(controller.dateEnd.value.substring(6,10)), int.parse(controller.dateEnd.value.substring(4,5)),int.parse(controller.dateEnd.value.substring(0,2))).millisecondsSinceEpoch);
                          controller.item.refresh();
                          Get.back();
                        },
                        child: Text('Làm Mới', style: TextStyle(color: ColorUtils.PRIMARY_COLOR, fontSize: 16.sp, fontWeight: FontWeight.w400, fontFamily: 'assets/font/static/Inter-Regular.ttf'),)),
                  )),
                  Padding(padding: EdgeInsets.only(right: 8.w)),
                  Expanded(child:SizedBox(
                    height: 36.h,
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: ColorUtils.PRIMARY_COLOR,
                            side: const BorderSide(
                                color: ColorUtils.PRIMARY_COLOR,width: 1
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(6),
                            )
                        ),
                        onPressed: (){
                          if(controller.indexHiden.value ==0){
                            controller.item.clear();
                            controller.getListDiligenceAll( DateTime(int.parse(controller.dateStart.value.substring(6,10)), int.parse(controller.dateStart.value.substring(3,5)),int.parse(controller.dateStart.value.substring(0,2))).millisecondsSinceEpoch,
                                DateTime(int.parse(controller.dateEnd.value.substring(6,10)), int.parse(controller.dateEnd.value.substring(3,5)),int.parse(controller.dateEnd.value.substring(0,2))).millisecondsSinceEpoch);
                            controller.item.refresh();
                          }else if(controller.indexHiden.value ==1){
                            controller.item.clear();
                            controller.getListDiligenceOnTime( DateTime(int.parse(controller.dateStart.value.substring(6,10)), int.parse(controller.dateStart.value.substring(3,5)),int.parse(controller.dateStart.value.substring(0,2))).millisecondsSinceEpoch,
                                DateTime(int.parse(controller.dateEnd.value.substring(6,10)), int.parse(controller.dateEnd.value.substring(3,5)),int.parse(controller.dateEnd.value.substring(0,2))).millisecondsSinceEpoch);
                            controller.item.refresh();
                          }else if(controller.indexHiden.value ==2){
                            controller.item.clear();
                            controller.getListDiligenceNotOnTime( DateTime(int.parse(controller.dateStart.value.substring(6,10)), int.parse(controller.dateStart.value.substring(3,5)),int.parse(controller.dateStart.value.substring(0,2))).millisecondsSinceEpoch,
                                DateTime(int.parse(controller.dateEnd.value.substring(6,10)), int.parse(controller.dateEnd.value.substring(3,5)),int.parse(controller.dateEnd.value.substring(0,2))).millisecondsSinceEpoch);
                            controller.item.refresh();
                          }else if(controller.indexHiden.value ==3){
                            controller.item.clear();
                            controller.getListDiligenceExecud( DateTime(int.parse(controller.dateStart.value.substring(6,10)), int.parse(controller.dateStart.value.substring(3,5)),int.parse(controller.dateStart.value.substring(0,2))).millisecondsSinceEpoch,
                                DateTime(int.parse(controller.dateEnd.value.substring(6,10)), int.parse(controller.dateEnd.value.substring(3,5)),int.parse(controller.dateEnd.value.substring(0,2))).millisecondsSinceEpoch);
                            controller.item.refresh();
                          }else{
                            controller.item.clear();
                            controller.getListDiligenceAbsuebWithout( DateTime(int.parse(controller.dateStart.value.substring(6,10)), int.parse(controller.dateStart.value.substring(3,5)),int.parse(controller.dateStart.value.substring(0,2))).millisecondsSinceEpoch,
                                DateTime(int.parse(controller.dateEnd.value.substring(6,10)), int.parse(controller.dateEnd.value.substring(3,5)),int.parse(controller.dateEnd.value.substring(0,2))).millisecondsSinceEpoch);
                            controller.item.refresh();
                          }
                          Get.back();
                        },
                        child: Text('Xác Nhận', style: TextStyle(color: const Color.fromRGBO(255, 255, 255, 1), fontSize: 16.sp, fontWeight: FontWeight.w400, fontFamily: 'assets/font/static/Inter-Regular.ttf'),)),
                  ))
                ],
              ),
            ),
            const Padding(padding: EdgeInsets.only(top: 24)),


          ],
        ),
      ));
  }
  selectDateTimeStart(stringTime, format,context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      controller.controllerdateStart.value.text = date;
      controller.dateStart.value = date;
      controller.getListDiligenceAll(controller.dateStart.value, controller.dateEnd.value);
    }

  }

  selectDateTimeEnd(stringTime, format,context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
        initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      controller.controllerdateEnd.value.text = date;
      controller.dateEnd.value = date;
      controller.getListDiligenceAll(controller.dateStart.value, controller.dateEnd.value);
    }

  }

}
