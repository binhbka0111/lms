import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/constants/date_format.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/date_time_picker.dart';
import 'package:slova_lms/commom/utils/time_utils.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/diligence_parent/statistical_parent/statistical_parent_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_page.dart';

class StatisticalParentPage extends GetWidget<StatisticalParentController> {
  final controller = Get.put(StatisticalParentController());

  StatisticalParentPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {
                  Get.to(ParentHomePage());
                },
                icon: const Icon(Icons.home),
                color: Colors.white,
              )
            ],
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            title: Text(
              'Thống Kê',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body: Column(
            children: [
              headerPage(),
              Container(
                margin: EdgeInsets.only(
                    top: 16.h, bottom: 8.h, right: 16.w, left: 16.w),
                child: Row(
                  children: [
                    Text(
                      'Thống Kê Chuyên Cần',
                      style: TextStyle(
                          color: const Color.fromRGBO(26, 26, 26, 1),
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'assets/font/static/Inter-Medium.ttf'),
                    ),
                    Expanded(child: Container()),
                    InkWell(
                      onTap: () {
                        Get.dialog(Dialog(
                          child: IntrinsicHeight(
                              child: Container(
                            margin: const EdgeInsets.symmetric(
                                horizontal: 16, vertical: 16),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(bottom: 8.h),
                                  child: Text(
                                    "Vui lòng chọn ngày",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12.sp),
                                  ),
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        alignment: Alignment.centerLeft,
                                        padding: EdgeInsets.only(left: 8.w),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(6.0)),
                                            border: Border.all(
                                              width: 1,
                                              style: BorderStyle.solid,
                                              color: const Color.fromRGBO(
                                                  192, 192, 192, 1),
                                            )),
                                        child: TextFormField(
                                          keyboardType: TextInputType.multiline,
                                          maxLines: null,
                                          style: TextStyle(
                                            fontSize: 10.0.sp,
                                            color: const Color.fromRGBO(
                                                26, 26, 26, 1),
                                          ),
                                          onTap: () {
                                            selectDateTimeStart(
                                                controller.controllerDateStart
                                                    .value.text,
                                                DateTimeFormat.formatDateShort,
                                                context);
                                          },
                                          cursorColor: ColorUtils.PRIMARY_COLOR,
                                          controller: controller
                                              .controllerDateStart.value,
                                          readOnly: true,
                                          decoration: InputDecoration(
                                            suffixIcon: SizedBox(
                                              height: 1,
                                              width: 1,
                                              child: SvgPicture.asset(
                                            "assets/images/icon_date_picker.svg",
                                            fit: BoxFit.scaleDown,
                                              ),
                                            ),
                                            labelText: "Từ ngày",
                                            border: InputBorder.none,
                                            labelStyle: TextStyle(
                                                color: const Color.fromRGBO(
                                                    177, 177, 177, 1),
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w500,
                                                fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf'),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(right: 16.w)),
                                    Expanded(
                                      child: Container(
                                        alignment: Alignment.centerLeft,
                                        padding: EdgeInsets.only(left: 8.w),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(6.0)),
                                            border: Border.all(
                                              width: 1,
                                              style: BorderStyle.solid,
                                              color: const Color.fromRGBO(
                                                  192, 192, 192, 1),
                                            )),
                                        child: TextFormField(
                                          keyboardType: TextInputType.multiline,
                                          maxLines: null,
                                          style: TextStyle(
                                            fontSize: 10.0.sp,
                                            color: const Color.fromRGBO(
                                                26, 26, 26, 1),
                                          ),
                                          onTap: () {
                                            selectDateTimeEnd(
                                                controller.controllerDateEnd
                                                    .value.text,
                                                DateTimeFormat.formatDateShort,
                                                context);
                                            FocusScope.of(context).nextFocus();
                                          },
                                          readOnly: true,
                                          cursorColor: ColorUtils.PRIMARY_COLOR,
                                          controller: controller
                                              .controllerDateEnd.value,
                                          decoration: InputDecoration(
                                            suffixIcon: SizedBox(
                                              height: 1,
                                              width: 1,
                                              child: SvgPicture.asset(
                                                "assets/images/icon_date_picker.svg",
                                                fit: BoxFit.scaleDown,
                                              ),
                                            ),
                                            label: const Text("Đến ngày"),
                                            border: InputBorder.none,
                                            labelStyle: TextStyle(
                                                color: const Color.fromRGBO(
                                                    177, 177, 177, 1),
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w500,
                                                fontFamily:
                                                    'assets/font/static/Inter-Medium.ttf'),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const Padding(
                                    padding: EdgeInsets.only(top: 16)),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                        child: SizedBox(
                                      height: 24.h,
                                      child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                              backgroundColor:
                                                  const Color.fromRGBO(
                                                      255, 255, 255, 1),
                                              side: const BorderSide(
                                                  color: ColorUtils.PRIMARY_COLOR,
                                                  width: 1),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(6),
                                              )),
                                          onPressed: () {
                                            Get.back();
                                            controller.getDay();
                                          },
                                          child: Text(
                                            'Hủy',
                                            style: TextStyle(
                                                color: ColorUtils.PRIMARY_COLOR,
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w400,
                                                fontFamily:
                                                    'assets/font/static/Inter-Regular.ttf'),
                                          )),
                                    )),
                                    Padding(
                                        padding: EdgeInsets.only(right: 8.w)),
                                    Expanded(
                                        child: SizedBox(
                                      height: 24.h,
                                      child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                              backgroundColor: ColorUtils.PRIMARY_COLOR,
                                              side: const BorderSide(
                                                  color: ColorUtils.PRIMARY_COLOR,
                                                  width: 1),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(6),
                                              )),
                                          onPressed: () {
                                            controller.dayAbsent.clear();
                                            controller.dayExcused.clear();
                                            controller.dayOnSchool.clear();
                                            controller.dayNotOnTime
                                                .clear();
                                            controller.getListDiligence(
                                                DateTime(
                                                    int.parse(controller.controllerDateStart.value.text.substring(6, 10)),
                                                    int.parse(controller.controllerDateStart.value.text.substring(4, 5)),
                                                    int.parse(controller.controllerDateStart.value.text.substring(1, 2))).millisecondsSinceEpoch,
                                                DateTime(
                                                        int.parse(controller.controllerDateEnd.value.text.substring(6, 10)),
                                                        int.parse(controller.controllerDateEnd.value.text.substring(4, 5)),
                                                        int.parse(controller.controllerDateEnd.value.text.substring(0, 2)))
                                                    .millisecondsSinceEpoch);

                                            controller.dateStart.value = controller.controllerDateStart.value.text;
                                            controller.dateEnd.value = controller.controllerDateEnd.value.text;

                                            controller.dayOnSchool.refresh();
                                            controller.dayNotOnTime.refresh();
                                            controller.dayAbsent.refresh();
                                            controller.dayExcused.refresh();
                                            Get.back();
                                          },
                                          child: Text(
                                            'Xác Nhận',
                                            style: TextStyle(
                                                color: const Color.fromRGBO(
                                                    255, 255, 255, 1),
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w400,
                                                fontFamily:
                                                    'assets/font/static/Inter-Regular.ttf'),
                                          )),
                                    ))
                                  ],
                                ),
                              ],
                            ),
                          )),
                        ));
                      },
                      child: Row(
                        children: [
                          Text(
                              "${controller.dateStart.value} - ${controller.dateEnd.value}",
                              style: TextStyle(
                                  color: const Color.fromRGBO(26, 26, 26, 1),
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w500,
                                  fontFamily:
                                      'assets/font/static/Inter-Medium.ttf')),
                          Padding(padding: EdgeInsets.only(right: 4.w)),
                          SizedBox(
                            child: Image.asset(
                              "assets/images/icon_calenda.png",
                              height: 16,
                              width: 16,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Visibility(
                  visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_STATISTICAL_DILIGENCE_LIST),
                  child: bodyPage())
            ],
          ),
        ));
  }

  ListView listItemNotOnTime() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: controller.dayNotOnTime.length,
        itemBuilder: (context, index) {
          return GestureDetector(
              onTap: () {},
              child: Container(
                color: const Color.fromRGBO(245, 245, 245, 1),
                margin: EdgeInsets.only(top: 8.h),
                child: Row(
                  children: [
                    Text(
                      "${controller.getConvertDateNotOn(index)} lúc ",
                      style: styleContent(),
                    ),
                    Text(
                      controller.outputTimeFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.dayNotOnTime[index].updatedAt!)),
                      style: TextStyle(
                          fontSize: 12.sp,
                          color: controller.getColorTextStatus(controller
                              .dayNotOnTime[index].statusDiligent)),
                    )
                  ],
                ),
              ));
        });
  }

  ListView listItemOnTime() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: controller.dayOnSchool.length,
        itemBuilder: (context, index) {
          return
           Obx(() =>  GestureDetector(
               onTap: () {
               },
               child:
               Container(
                 margin: EdgeInsets.only(top: 8.h),
                 child:
                 Row(
                   children: [
                     Text("${controller.getConvertDateOnTime(index)}",
                       style: styleContent(),
                     ),
                     Expanded(child: Container()),
                   ],
                 ),
               )
           ));
        });
  }

  ListView listItemExcusedAbsence() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: controller.dayExcused.length,
        itemBuilder: (context, index) {
          return GestureDetector(
              onTap: () {},
              child: Container(
                color: const Color.fromRGBO(245, 245, 245, 1),
                margin: EdgeInsets.only(top: 8.h),
                child: Row(
                  children: [
                    Text(
                      "${controller.getConvertDateExcused(index)} lúc ",
                      style: styleContent(),
                    ),
                    Text(
                      controller.outputTimeFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.dayExcused[index].updatedAt!)),
                      style: TextStyle(
                          fontSize: 12,
                          color: controller.getColorTextStatus(controller
                              .dayExcused[index].statusDiligent)),
                    )
                  ],
                ),
              ));
        });
  }

  ListView listItemAbsentWithoutLeave() {
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: controller.dayAbsent.length,
        itemBuilder: (context, index) {
          return GestureDetector(
              onTap: () {},
              child: Container(
                color: const Color.fromRGBO(245, 245, 245, 1),
                margin: EdgeInsets.only(top: 8.h),
                child: Row(
                  children: [
                    Text(
                      "${controller.getConvertDateAbsend(index)} lúc ",
                      style: styleContent(),
                    ),
                    Text(
                      controller.outputTimeFormat.format(DateTime.fromMillisecondsSinceEpoch(controller.dayAbsent[index].updatedAt!)),
                      style: TextStyle(
                          fontSize: 12,
                          color: controller.getColorTextStatus(controller
                              .dayAbsent[index].statusDiligent)),
                    )
                  ],
                ),
              ));
        });
  }

  styleTitle() {
    return TextStyle(
        color: const Color.fromRGBO(26, 26, 26, 1),
        fontSize: 12.sp,
        fontWeight: FontWeight.w400,
        fontFamily: 'assets/font/static/Inter-Regular.ttf');
  }

  styleContent() {
    return TextStyle(
        color: const Color.fromRGBO(90, 90, 90, 1),
        fontSize: 12.sp,
        fontWeight: FontWeight.w400,
        fontFamily: 'assets/font/static/Inter-Regular.ttf');
  }

  headerPage() {
    return Container(
      padding: EdgeInsets.all(16.h),
      color: const Color.fromRGBO(255, 255, 255, 1),
      child: Row(
        children: [

          SizedBox(
              height: 40.h,
              width: 40.h,
              child:
              CacheNetWorkCustom(urlImage: '${ Get.find<ParentHomeController>()
                  .currentStudentProfile
                  .value
                  .image}',)

          ),

          Container(
            margin: EdgeInsets.only(left: 8.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${Get.find<ParentHomeController>().currentStudentProfile.value.fullName}",
                  style: TextStyle(
                      color: const Color.fromRGBO(26, 26, 26, 1),
                      fontSize: 12.sp),
                ),
                Text(
                  '${Get.find<ParentHomeController>().currentStudentProfile.value.school!.name}',
                  style: TextStyle(
                      color: const Color.fromRGBO(177, 177, 177, 1),
                      fontSize: 12.sp),
                ),
              ],
            ),
          ),
          const Spacer(),
     Visibility(
         visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp, StringConstant.FEATURE_STATISTICAL_DILIGENCE_DETAIL),
         child:
     InkWell(
       onTap: () {
         controller.goToDetailStatisticalParent();
       },
       child: Container(
         alignment: Alignment.center,
         decoration: BoxDecoration(
             borderRadius: BorderRadius.circular(6),
             color: const Color.fromRGBO(254, 230, 211, 1)),
         width: 80.w,
         height: 18.h,
         child: Text(
           "Chi tiết",
           style: TextStyle(
               color: ColorUtils.PRIMARY_COLOR,
               fontSize: 12.sp,
               fontFamily: 'static/Inter-Medium.ttf'),
         ),
       ),
     ))
        ],
      ),
    );
  }

  bodyPage() {
    return Obx(() => Container(
          margin: EdgeInsets.only(left: 16.w, right: 16.w),
          padding:
              EdgeInsets.only(top: 11.h, bottom: 11.h, right: 16.w, left: 16.w),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6.r),
              color: const Color.fromRGBO(255, 255, 255, 1)),
          child: Column(
            children: [
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      controller.aDayGotoSchool.value =
                          !controller.aDayGotoSchool.value;
                    },
                    child: Row(
                      children: [
                        Text(
                          'Ngày đi học:',
                          style: controller.textColor.value
                              ? styleTitle()
                              : TextStyle(
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 13.sp),
                        ),
                        const Spacer(),
                        Row(
                          children: [
                            Text(
                              "${controller.totalDayOnTime.value} ngày",
                              style: TextStyle(
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 14.sp),
                            ),
                            Padding(padding: EdgeInsets.only(right: 9.w)),
                            Icon(controller.aDayGotoSchool.value
                                ? Icons.keyboard_arrow_up
                                : Icons.keyboard_arrow_down_outlined),
                          ],
                        )
                      ],
                    ),
                  ),
                  Visibility(
                      visible: controller.aDayGotoSchool.value,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          color: const Color.fromRGBO(245, 245, 245, 1),
                        ),
                        padding: const EdgeInsets.all(4),
                        margin: EdgeInsets.only(top: 11.h),
                        child: listItemOnTime(),
                      ))
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 22.h)),
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      controller.dayOff.value = !controller.dayOff.value;
                    },
                    child: Row(
                      children: [
                        Text(
                          'Ngày nghỉ có phép',
                          style: controller.textColor.value
                              ? styleTitle()
                              : TextStyle(
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 13.sp),
                        ),
                        Expanded(child: Container()),
                        Row(
                          children: [
                            Text(
                              "${controller.totalDayExSeCud.value} ngày",
                              style: TextStyle(
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 14.sp),
                            ),
                            Padding(padding: EdgeInsets.only(right: 9.w)),
                            Icon(controller.dayOff.value
                                ? Icons.keyboard_arrow_up
                                : Icons.keyboard_arrow_down_outlined),
                          ],
                        )
                      ],
                    ),
                  ),
                  Visibility(
                    visible: controller.dayOff.value,
                    child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          color: const Color.fromRGBO(245, 245, 245, 1),
                        ),
                        padding: const EdgeInsets.all(4),
                        margin: const EdgeInsets.only(top: 11),
                        child: Column(
                          children: [
                            listItemExcusedAbsence(),
                          ],
                        )),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 22.h)),
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      controller.dayOffExcused.value =
                          !controller.dayOffExcused.value;
                    },
                    child: Row(
                      children: [
                        Text(
                          'Ngày nghỉ không phép:',
                          style: controller.textColor.value
                              ? styleTitle()
                              : TextStyle(
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 13.sp),
                        ),
                        Expanded(child: Container()),
                        Row(
                          children: [
                            Text(
                              "${controller.totalDayAbSend.value} ngày",
                              // '${controller.diligence.value.studentNotOnTime!.totalDaysNotOnTime} ngày',
                              style: TextStyle(
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 14.sp),
                            ),
                            Padding(padding: EdgeInsets.only(right: 9.w)),
                            Icon(controller.dayOffExcused.value
                                ? Icons.keyboard_arrow_up
                                : Icons.keyboard_arrow_down_outlined),
                          ],
                        )
                      ],
                    ),
                  ),
                  Visibility(
                    visible: controller.dayOffExcused.value,
                    child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          color: const Color.fromRGBO(245, 245, 245, 1),
                        ),
                        padding: const EdgeInsets.all(4),
                        margin: const EdgeInsets.only(top: 11),
                        child: Column(
                          children: [listItemAbsentWithoutLeave()],
                        )),
                  )
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 22.h)),
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      controller.lateForSchool.value =
                          !controller.lateForSchool.value;
                    },
                    child: Row(
                      children: [
                        Text(
                          'Đến muộn:',
                          style: controller.textColor.value
                              ? styleTitle()
                              : TextStyle(
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 13.sp),
                        ),
                        Expanded(child: Container()),
                        Row(
                          children: [
                            Text(
                              "${controller.totalDayNotOnTime.value} ngày",
                              style: TextStyle(
                                  color: const Color.fromRGBO(133, 133, 133, 1),
                                  fontSize: 14.sp),
                            ),
                            Padding(padding: EdgeInsets.only(right: 9.w)),
                            Icon(controller.lateForSchool.value
                                ? Icons.keyboard_arrow_up
                                : Icons.keyboard_arrow_down_outlined),
                          ],
                        )
                      ],
                    ),
                  ),
                  Visibility(
                    visible: controller.lateForSchool.value,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: const Color.fromRGBO(245, 245, 245, 1),
                      ),
                      padding: const EdgeInsets.all(4),
                      margin: EdgeInsets.only(top: 11.h),
                      child:  listItemNotOnTime(),
                    ),
                  )
                ],
              ),
            ],
          ),
        ));
  }

  selectDateTime(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
            initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      controller.textDate.value = date;
      controller.selectedDate.value = date;
      controller.dayAbsent.clear();
      controller.dayExcused.clear();
      controller.dayOnSchool.clear();
      controller.dayNotOnTime.clear();
      controller.dayOnSchool.refresh();
      controller.dayNotOnTime.refresh();
      controller.dayAbsent.refresh();
      controller.dayExcused.refresh();
    }
  }

  selectDateTimeStart(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
            initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy')
                .parse(controller.controllerDateEnd.value.text)
                .millisecondsSinceEpoch) {
          controller.controllerDateStart.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian bắt đầu nhỏ hơn thời gian kết thúc");
        }
      } else {
        controller.controllerDateStart.value.text = date;
      }

    }
  }

  selectDateTimeEnd(stringTime, format, context) async {
    var curent = DateTime.now();
    if (!stringTime.isEmpty) {
      curent = TimeUtils.convertStringToDate(stringTime, format);
    } else {
      curent = DateTime.now();
    }
    var date = "";
    await DateTimePicker.showDatePicker(Get.context!, curent,
            initialDate: DateTime(DateTime.now().year - 10))
        .then((value) async {
      date =
          TimeUtils.convertDateTimeToFormat(value, DateTimeFormat.formatDate);
    });

    if (date.isNotEmpty) {
      if (controller.controllerDateStart.value.text != "") {
        if (DateFormat('dd/MM/yyyy')
                .parse(controller.controllerDateStart.value.text)
                .millisecondsSinceEpoch <=
            DateFormat('dd/MM/yyyy').parse(date).millisecondsSinceEpoch) {
          controller.controllerDateEnd.value.text = date;
        } else {
          AppUtils.shared.showToast(
              "Vui lòng chọn thời gian kết thúc lớn hơn thời gian bắt đầu");
        }
      } else {
        controller.controllerDateEnd.value.text = date;
      }
    }

  }
}
