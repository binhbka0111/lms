import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/data/model/common/diligence.dart';
import 'package:slova_lms/data/repository/diligence/diligence_repo.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/commom/utils/app_utils.dart';
import 'package:slova_lms/data/base_service/api_response.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';


class StatisticalParentController extends GetxController{
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputDateToDateFormat = DateFormat('dd/MM/yyyy');
  var outputDate = DateFormat('EEEE,dd/MM/yyyy');
  var outputTimeFormat = DateFormat('HH:mm:ss ');
  var aDayGotoSchool = false.obs;
  var dayOff= false.obs;
  var dayOffExcused= false.obs;
  var textDate = "".obs;
  var textColor= false.obs;
  var lateForSchool = false.obs;
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var totalDay = 0.obs;
  RxList<StatusDetailDiligence> dayOnSchool = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayOnSchoolOnTime = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayNotOnTime = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayAbsent = <StatusDetailDiligence>[].obs;
  RxList<StatusDetailDiligence> dayExcused = <StatusDetailDiligence>[].obs;
  var detailDiligence = DetailDiligence().obs;
  var totalDayOnTime = 0.obs;
  var totalDayNotOnTime = 0.obs;
  var totalDayAbSend = 0.obs;
  var totalDayExSeCud = 0.obs;
  var selectedDate ="".obs;
  var controllerDateStart = TextEditingController().obs;
  var controllerDateEnd = TextEditingController().obs;
  var dateStart ="".obs;
  var dateEnd ="".obs;
  var fromYearSelect ="".obs;
  var toYearSelect ="".obs;

  @override
  void onInit() {
    setDateSchoolYears();
    if(DateTime.now().month<=12&&DateTime.now().month>9){
      controllerDateStart.value.text =   outputDateFormat.format(DateTime(Get.find<ParentHomeController>().fromYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<ParentHomeController>().fromYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      dateEnd.value =outputDateFormat.format(DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      dateStart.value =outputDateFormat.format(DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));

    }else{
      controllerDateStart.value.text =   outputDateFormat.format(DateTime(Get.find<ParentHomeController>().toYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
      controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<ParentHomeController>().toYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      dateEnd.value =outputDateFormat.format(DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
      dateStart.value =outputDateFormat.format(DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));

    }
    super.onInit();
 }
getDay(){
  if(DateTime.now().month<=12&&DateTime.now().month>9){
    controllerDateStart.value.text =   outputDateFormat.format(DateTime(Get.find<ParentHomeController>().fromYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
    controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<ParentHomeController>().fromYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
  }else{
    controllerDateStart.value.text =   outputDateFormat.format(DateTime(Get.find<ParentHomeController>().toYearPresent.value, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day));
    controllerDateEnd.value.text = outputDateFormat.format(DateTime(Get.find<ParentHomeController>().toYearPresent.value, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day));
  }
}


  getColorTextStatus(state){
    switch(state){
      case "ON_TIME":
        return const Color.fromRGBO(77,197,145, 1);
      case "NOT_ON_TIME":
        return const Color.fromRGBO(253,185,36, 1);
      case "EXCUSED_ABSENCE":
        return const Color.fromRGBO(136, 136, 136, 1);
      default:
        return const Color.fromRGBO(255,69,89,1);
    }
  }
  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }

  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime
        .add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }

  getListDiligence(fromDate,toDate){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var fromDate = DateTime(int.parse(controllerDateStart.value.text.substring(6,10)), int.parse(controllerDateStart.value.text.substring(3,5)),int.parse(controllerDateStart.value.text.substring(0,2))).millisecondsSinceEpoch;
    var toDate = DateTime(int.parse(controllerDateEnd.value.text.substring(6,10)), int.parse(controllerDateEnd.value.text.substring(3,5)),int.parse(controllerDateEnd.value.text.substring(0,2))).millisecondsSinceEpoch;
    _diligenceRepo.detailListDiligence(userId, fromDate, toDate,"").then((value) {
      if (value.state == Status.SUCCESS) {
          detailDiligence.value = value.object!;
          dayOnSchool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
          dayOnSchoolOnTime.value = dayOnSchool.where((element) => element.statusDiligent== "ON_TIME").toList();
          dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
          dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
          dayNotOnTime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
          if(detailDiligence.value.studentOnSchool != null){
            totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
          }
          if(detailDiligence.value.studentNotOnTime != null){
            totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
          }
          if(detailDiligence.value.studentAbsentWithoutLeave != null){
            totalDayAbSend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
          }
          if(detailDiligence.value.studentExcusedAbsence != null){
            totalDayExSeCud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
          }
          AppUtils.shared.hideLoading();
          Future.delayed(const Duration(seconds: 1), () {
          });

      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }


  getListDiligenceToday(){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var toDate = DateTime(findLastDateOfTheWeek(DateTime.now()).year, findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var fromDate = DateTime(findFirstDateOfTheWeek(DateTime.now()).year, findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.detailListDiligence(userId, fromDate, toDate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayOnSchool.value = detailDiligence.value.studentOnSchool!.dayOnSchool!;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotOnTime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbSend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExSeCud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  getListDiligenceToSelectYear(fromDate, toDate){
    var userId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    var fromDate = DateTime(int.parse(fromYearSelect.value.substring(0,4)), findFirstDateOfTheWeek(DateTime.now()).month,findFirstDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    var toDate = DateTime(int.parse(toYearSelect.value.substring(0,4)), findLastDateOfTheWeek(DateTime.now()).month,findLastDateOfTheWeek(DateTime.now()).day).millisecondsSinceEpoch;
    _diligenceRepo.detailListDiligence(userId, fromDate, toDate,"").then((value) {
      if (value.state == Status.SUCCESS) {
        detailDiligence.value = value.object!;
        dayOnSchool.value = detailDiligence.value.studentOnSchool!.dayOnSchool! ;
        dayExcused.value = detailDiligence.value.studentExcusedAbsence!.dayExcusedAbsence!;
        dayAbsent.value = detailDiligence.value.studentAbsentWithoutLeave!.dayAbsentWithoutLeave!;
        dayNotOnTime.value = detailDiligence.value.studentNotOnTime!.dayNotOnTime!;
        if(detailDiligence.value.studentOnSchool != null){
          totalDayOnTime.value = detailDiligence.value.studentOnSchool!.totalDaysOnSchool!;
        }
        if(detailDiligence.value.studentNotOnTime != null){
          totalDayNotOnTime.value = detailDiligence.value.studentNotOnTime!.totalDaysNotOnTime!;
        }
        if(detailDiligence.value.studentAbsentWithoutLeave != null){
          totalDayAbSend.value = detailDiligence.value.studentAbsentWithoutLeave!.totalDaysAbsentWithoutLeave!;
        }
        if(detailDiligence.value.studentExcusedAbsence != null){
          totalDayExSeCud.value = detailDiligence.value.studentExcusedAbsence!.totalDaysExcusedAbsence!;
        }
        AppUtils.shared.hideLoading();
        Future.delayed(const Duration(seconds: 1), () {
        });
      } else {
        AppUtils.shared.hideLoading();
        AppUtils.shared.snackbarError("Lấy danh sách chuyên cần thất bại", value.message ?? "");
      }

    });

  }
  setDateSchoolYears() {
    if (DateTime.now().month <= 12 && DateTime.now().month > 9) {
      if (Get.find<ParentHomeController>().fromYearPresent.value == DateTime.now().year) {
        getListDiligenceToday();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ParentHomeController>().fromYearPresent}";
        toYearSelect.value = "${Get.find<ParentHomeController>().fromYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
      }
    } else {
      if (Get.find<ParentHomeController>().toYearPresent.value == DateTime.now().year) {
        getListDiligenceToday();
        return;
      } else {
        fromYearSelect.value = "${Get.find<ParentHomeController>().toYearPresent}";
        toYearSelect.value = "${Get.find<ParentHomeController>().toYearPresent}";
        getListDiligenceToSelectYear(fromYearSelect.value, toYearSelect.value);
      }
    }
  }

  getConvertDateNotOn(index){
    var  dayNotOn = outputDate.format(DateTime.fromMillisecondsSinceEpoch(dayNotOnTime[index].date!));
    if(dayNotOn.substring(0,6)=="Monday"){
      return "Thứ 2, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotOnTime[index].date!))} ";
    }else if(dayNotOn.substring(0,7)=="Tuesday"){
      return "Thứ 3, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotOnTime[index].date!))} ";
    }else if(dayNotOn.substring(0,9)=="Wednesday"){
      return "Thứ 4, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotOnTime[index].date!))} ";
    }else if(dayNotOn.substring(0,8)=="Thursday"){
      return "Thứ 5, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotOnTime[index].date!))} ";
    }else if(dayNotOn.substring(0,6)=="Friday"){
      return "Thứ 6, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotOnTime[index].date!))} ";
    }else if(dayNotOn.substring(0,8)=="Saturday"){
      return "Thứ 7, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotOnTime[index].date!))} ";
    }else if(dayNotOn.substring(0,6)=="Sunday"){
      return "Chủ nhật, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayNotOnTime[index].date!))} ";
    }else{
      return "";
    }
  }

  getConvertDateOnTime(index){
    var  dayOnTime = outputDate.format(DateTime.fromMillisecondsSinceEpoch(dayOnSchool[index].date!));
    if(dayOnTime.substring(0,6)=="Monday"){
      return "Thứ 2, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnSchool[index].date!))} ";
    }else if(dayOnTime.substring(0,7)=="Tuesday"){
      return "Thứ 3, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnSchool[index].date!))} ";
    }else if(dayOnTime.substring(0,9)=="Wednesday"){
      return "Thứ 4, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnSchool[index].date!))} ";
    }else if(dayOnTime.substring(0,8)=="Thursday"){
      return "Thứ 5, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnSchool[index].date!))} ";
    }else if(dayOnTime.substring(0,6)=="Friday"){
      return "Thứ 6, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnSchool[index].date!))} ";
    }else if(dayOnTime.substring(0,8)=="Saturday"){
      return "Thứ 7, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnSchool[index].date!))} ";
    }else if(dayOnTime.substring(0,6)=="Sunday"){
      return "Chủ nhật, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayOnSchool[index].date!))} ";
    }else{
      return "";
    }
  }
  getConvertDateAbsend(index){
    var  dayAbsend = outputDate.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent[index].date!));
    if(dayAbsend.substring(0,6)=="Monday"){
      return "Thứ 2, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent[index].date!))} ";
    }else if(dayAbsend.substring(0,7)=="Tuesday"){
      return "Thứ 3, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent[index].date!))} ";
    }else if(dayAbsend.substring(0,9)=="Wednesday"){
      return "Thứ 4, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent[index].date!))} ";
    }else if(dayAbsend.substring(0,8)=="Thursday"){
      return "Thứ 5, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent[index].date!))} ";
    }else if(dayAbsend.substring(0,6)=="Friday"){
      return "Thứ 6, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent[index].date!))} ";
    }else if(dayAbsend.substring(0,8)=="Saturday"){
      return "Thứ 7, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent[index].date!))} ";
    }else if(dayAbsend.substring(0,6)=="Sunday"){
      return "Chủ nhật, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayAbsent[index].date!))} ";
    }else{
      return "";
    }
  }

  getConvertDateExcused(index){
    var  dayexcused = outputDate.format(DateTime.fromMillisecondsSinceEpoch(dayExcused[index].date!));
    if(dayexcused.substring(0,6)=="Monday"){
      return "Thứ 2, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused[index].date!))} ";
    }else if(dayexcused.substring(0,7)=="Tuesday"){
      return "Thứ 3, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused[index].date!))} ";
    }else if(dayexcused.substring(0,9)=="Wednesday"){
      return "Thứ 4, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused[index].date!))} ";
    }else if(dayexcused.substring(0,8)=="Thursday"){
      return "Thứ 5, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused[index].date!))} ";
    }else if(dayexcused.substring(0,6)=="Friday"){
      return "Thứ 6, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused[index].date!))} ";
    }else if(dayexcused.substring(0,8)=="Saturday"){
      return "Thứ 7, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused[index].date!))} ";
    }else if(dayexcused.substring(0,6)=="Sunday"){
      return "Chủ nhật, ${outputDateToDateFormat.format(DateTime.fromMillisecondsSinceEpoch(dayExcused[index].date!))} ";
    }else{
      return "";
    }
  }
  goToDetailStatisticalParent(){
    Get.toNamed(Routes.detailAllStasticalPageParent, arguments: [controllerDateStart.value.text.toString(), controllerDateEnd.value.text.toString()]);

  }

}

