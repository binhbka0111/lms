import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neat_and_clean_calendar/flutter_neat_and_clean_calendar.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:slova_lms/data/model/common/diligence.dart';
class DetailStatisticalParentController extends GetxController{
  var format = CalendarFormat.month.obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var outputFormat = DateFormat('HH:mm:ss');
  var focusedDay = DateTime.now().obs;
  var selectedDay = DateTime.now().obs;
  var focusName = FocusNode().obs;
  var controllerName = TextEditingController().obs;
  var timeNow = DateFormat.Hms().format(DateTime.now()).obs;
  var isAddShowJob = false.obs;
  var dateNow = "".obs;
  var showDetailDiligence = true.obs;
  var detailDiligence = DetailDiligence().obs; 
  RxList<Items> item = <Items>[].obs;
  RxInt date= 0.obs;
  RxInt time= 0.obs;
  var status = ''.obs;
  var quarterTurns = 270.obs;
  var weekNumber = 1.obs;
  var heightOffset = (Get.width/5).obs;
  var weekController = DateRangePickerController().obs;
  var timelineController = CalendarController().obs;
  String ON_TIME_TEXT = "Đúng giờ";
  String NOT_ON_TIME_TEXT= "Đi muộn";
  String ABSENT_WITHOUT_LEAVE_TEXT = "Không phép";
  String EXCUSED_ABSENCE_TEXT = "Có phép";
  var allStatus = <StatusDetailDiligence>[].obs;
  var eventList = <NeatCleanCalendarEvent>[].obs;


  @override
  void onInit() {
    // TODO: implement onInit
    var data = Get.arguments;
    if (data != null) {
      date.value = data[0];
      status.value = data[1];
      time.value = data[2];
    }
    dateNow.value=outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(date.value));
    focusedDay.value= DateTime(int.parse(dateNow.value.substring(6,10)),int.parse(dateNow.value.substring(3,5)),int.parse(dateNow.value.substring(0,2)));
    super.onInit();
  }
  getColorTextStatus(state){
    switch(state){
      case "ON_TIME":
        return const Color.fromRGBO(77,197,145, 1);
      case "NOT_ON_TIME":
        return const Color.fromRGBO(253,185,36, 1);
      case "EXCUSED_ABSENCE":
        return const Color.fromRGBO(136, 136, 136, 1);
      default:
        return const Color.fromRGBO(255,69,89,1);
    }
  }
  getColorStatus(state) {
    switch (state) {
      case "ON_TIME":
        return const Color.fromRGBO(77,197,145, 1);
      case "NOT_ON_TIME":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "ABSENT_WITHOUT_LEAVE":
        return const Color.fromRGBO(255,69,89,1);
      default:
        return const Color.fromRGBO(210, 210, 210, 1);
    }
  }
  getTextStatus(state) {
    switch (state) {
      case "ON_TIME":
        return ON_TIME_TEXT;
      case "NOT_ON_TIME":
        return NOT_ON_TIME_TEXT;
      case "ABSENT_WITHOUT_LEAVE":
        return ABSENT_WITHOUT_LEAVE_TEXT;
      default:
        return EXCUSED_ABSENCE_TEXT;
    }
  }
  getStatusAll(fromdate,todate){
    for(int i=0; i<allStatus.length; i++){
      eventList.add(
        NeatCleanCalendarEvent('Chi tiết',
          location: outputFormat.format(DateTime.fromMillisecondsSinceEpoch(allStatus[i].updatedAt!)),
          description:"${getTextStatus(allStatus[i].statusDiligent)}",
          startTime: DateTime.fromMillisecondsSinceEpoch(allStatus[i].date!),
          endTime:DateTime.fromMillisecondsSinceEpoch(allStatus[i].date!),
          color:getColorTextStatus(allStatus[i].statusDiligent),
        ),
      );
    }
  }
  }




