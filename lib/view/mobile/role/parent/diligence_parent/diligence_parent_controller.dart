
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/diligence_parent/diary_parent/diary_parent_page.dart';
import 'package:slova_lms/view/mobile/role/parent/diligence_parent/statistical_parent/statistical_parent_page.dart';
import '../../../../../routes/app_pages.dart';



class DiligenceParentController extends GetxController{
  var listIcon = <ItemDiligence>[];
  String nameApproval = "Đơn xin nghỉ phép";
  String nameDiary = "Nhật ký";
  String nameStatistical = "Thống kê";

  @override
  void onInit() {
    getIconDiligence();
    super.onInit();
  }

 goToStatisticalPage(){
   Get.to(StatisticalParentPage());
 }
 goToDiaryPage(){
   Get.to(DiaryParentPage());
 }
 goToApprovalPage(){
   Get.toNamed(Routes.approvalParent);
 }




 getIconDiligence(){
   if(Get.find<HomeController>().userGroupByApp.isEmpty){
     listIcon.add(ItemDiligence(name: nameStatistical, image: "assets/images/thongke.png"));
     listIcon.add(ItemDiligence(name: nameDiary, image: "assets/images/diary.png"));
     listIcon.add(ItemDiligence(name: nameApproval, image: "assets/images/approval.png"));
   }else{
     if(checkVisiblePage(Get.find<HomeController>().userGroupByApp,StringConstant.PAGE_LEAVE_APPLICATION)){
       listIcon.add(ItemDiligence(name: nameApproval, image: "assets/images/approval.png"));
     }
     if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_STATISTICAL)){
       listIcon.add(ItemDiligence(name: nameStatistical, image: "assets/images/thongke.png"));
     }
     if(checkVisiblePage(Get.find<HomeController>().userGroupByApp, StringConstant.PAGE_DIARY)){
       listIcon.add(ItemDiligence(name: nameDiary, image: "assets/images/diary.png"));
     }
   }
 }







 }




class ItemDiligence {
  String ? name;
  String? image;
  ItemDiligence({required this.name, required this.image});
}




