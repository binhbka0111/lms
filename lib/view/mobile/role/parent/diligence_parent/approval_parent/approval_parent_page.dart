import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/role/parent/diligence_parent/approval_parent/approval_parent_controller.dart';
import '../../../../../../routes/app_pages.dart';

class ApprovalParentPage extends GetWidget<ApprovalParentController> {
  final controller = Get.put(ApprovalParentController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: const Color.fromRGBO(245, 245, 245, 1),
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {
              Get.toNamed(Routes.home);
            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          )
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Text(
          'Danh sách đơn xin nghỉ phép',
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() => RefreshIndicator(
        color:  ColorUtils.PRIMARY_COLOR,
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              children: [
                Padding(padding: EdgeInsets.only(top: 16.h)),
                Row(
                  children: [
                    Expanded(child: Container()),
                    Visibility(
                        visible:checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_LEAVE_APPLICATION_ADD),
                        child: InkWell(
                      onTap: () {
                        Get.toNamed(Routes.createALeavingApplication);
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 16.w, vertical: 8.h),
                        decoration: BoxDecoration(
                            color: ColorUtils.PRIMARY_COLOR,
                            borderRadius: BorderRadius.circular(6.r)),
                        child: Text(
                          "Tạo Đơn",
                          style: TextStyle(
                              fontSize: 12.sp,
                              fontWeight: FontWeight.w400,
                              color: Colors.white),
                        ),
                      ),
                    )),
                    Padding(padding: EdgeInsets.only(right: 16.w))
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 16.w)),
                ListView.builder(
                    itemCount: controller.itemsLeavingApplications.length,
                    shrinkWrap: true,
                    physics: const ScrollPhysics(),
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: 16.w, vertical: 4.h),
                        padding: EdgeInsets.symmetric(
                            horizontal: 16.w, vertical: 8.h),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(6)),
                        child: Column(
                          children: [
                            Padding(padding: EdgeInsets.only(top: 16.h)),
                            Visibility(
                                visible: controller.isShowEditAndDeleteLeavingApplication(controller.itemsLeavingApplications[index].status),
                                child: Row(
                                  children: [
                                    Expanded(child: Container()),
                                    Visibility(
                                        visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_LEAVE_APPLICATION_EDIT),
                                        child:  InkWell(
                                      onTap: () {
                                        Get.toNamed(Routes.editLeavingApplication,arguments: controller.itemsLeavingApplications[index]);
                                      },
                                      child: Text(
                                        "Chỉnh sửa",
                                        style: TextStyle(
                                            color: const Color.fromRGBO(
                                                26, 59, 112, 1),
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16.sp),
                                      ),
                                    )),
                                    Padding(padding: EdgeInsets.only(right: 16.w)),
                                    Visibility(
                                        visible: checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_LEAVE_APPLICATION_CANCEL),
                                        child: InkWell(
                                      onTap: () {
                                        Get.toNamed(Routes.cancelLeavingApplication,arguments: controller.itemsLeavingApplications[index].id);
                                      },
                                      child: Text(
                                        "Hủy",
                                        style: TextStyle(
                                            color: Colors.red,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16.sp),
                                      ),
                                    )),
                                  ],
                                )),
                            Visibility(
                                visible: controller.isShowEditAndDeleteLeavingApplication(controller.itemsLeavingApplications[index].status),
                                child:  const Divider()),
                            InkWell(
                              onTap: () {
                                checkVisibleFeature(Get.find<HomeController>().userGroupByApp,StringConstant.FEATURE_LEAVE_APPLICATION_DETAIL)
                                    ? Get.toNamed(Routes.detailLeaveApplicationParentPage,arguments: controller.itemsLeavingApplications[index].id)
                                    : showToastNotPermission();
                              },
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 80.w,
                                        child: Text(
                                          "Thời gian:",
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              color: const Color.fromRGBO(
                                                  133, 133, 133, 1),
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                      Flexible(
                                          child: RichText(
                                              text: TextSpan(children: [
                                                TextSpan(
                                                  text: controller.setTimeStartLeavingApplication(index),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.w400,
                                                      fontSize: 14.sp),
                                                ),
                                                TextSpan(
                                                  text: controller.setTimeLeavingApplication(index),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.w400,
                                                      fontSize: 14.sp),
                                                ),
                                                TextSpan(
                                                  text: controller.setTimeEndLeavingApplication(index),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.w400,
                                                      fontSize: 14.sp),
                                                ),
                                              ])))
                                    ],
                                  ),
                                  Padding(padding: EdgeInsets.only(top: 8.w)),
                                  Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        SizedBox(
                                          width: 80.w,
                                          child: Text(
                                            "Giáo viên:",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Text(
                                          "${controller.itemsLeavingApplications[index].teacher?.fullName}",
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              color: const Color.fromRGBO(
                                                  51, 157, 255, 1),
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ]),
                                  Padding(padding: EdgeInsets.only(top: 8.w)),
                                  Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        SizedBox(
                                          width: 80.w,
                                          child: Text(
                                            "Học sinh:",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Text(
                                          "${controller.itemsLeavingApplications[index].student?.fullName}",
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              color: const Color.fromRGBO(
                                                  51, 157, 255, 1),
                                              fontWeight: FontWeight.w400),
                                        ),
                                        Expanded(child: Container()),
                                      ]),
                                  Padding(padding: EdgeInsets.only(top: 8.w)),
                                  Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          width: 80.w,
                                          child: Text(
                                            "Lý do:",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Flexible(
                                          child: Text(
                                            "${controller.itemsLeavingApplications[index].reason}",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        )
                                      ]),
                                  Padding(padding: EdgeInsets.only(top: 8.w)),
                                  Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        SizedBox(
                                          width: 80.w,
                                          child: Text(
                                            "Trạng thái:",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: const Color.fromRGBO(
                                                    133, 133, 133, 1),
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Container(
                                          padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                                          decoration: BoxDecoration(
                                              color: controller.getColorBackgroundStatus(controller.itemsLeavingApplications[index].status),
                                              borderRadius: BorderRadius.circular(6)
                                          ),
                                          child: Text(
                                            "${controller.getStatus(controller.itemsLeavingApplications[index].status)}",
                                            style: TextStyle(
                                                fontSize: 14.sp,
                                                color: controller.getColorTextStatus(controller.itemsLeavingApplications[index].status),
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Expanded(child: Container()),
                                      ]),
                                ],
                              ),
                            )

                          ],
                        ),
                      );
                    }),
                Padding(padding: EdgeInsets.only(top: 16.h)),
              ],
            ),
          ),
          onRefresh: () async {
            controller.getListLeavingApplicationParent();
          })),
    );
  }
}
