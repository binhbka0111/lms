import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/view/mobile/role/parent/parent_home_controller.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/home_room_teacher.dart';
import '../../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../approval_parent_controller.dart';

class CreateALeavingApplicationController extends GetxController {
  String dropdown = '';
  var focusReason = FocusNode();
  var controllerReason = TextEditingController();
  var focusAssure = FocusNode();
  var controllerAssure = TextEditingController();

  var focusNote = FocusNode();
  var controllerNote = TextEditingController();

  var focusdateStart = FocusNode();
  var controllerdateStart = TextEditingController();

  var focusdateEnd = FocusNode();
  var controllerdateEnd = TextEditingController();
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var teacher = HomeRomeTeacher().obs;
  var studentName = "".obs;
  String getTempIFSCValidation(String text) {
    return text.length > 7 ? "This line is helper text" : "";
  }

  var isShowListDate = false.obs;
  var listDayLeavingApplication = <DateTime>[].obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var listLeavingDetail = <LeaveDetail>[].obs;
  var leaveDetail = LeaveDetail().obs;
  var isConfirmCreate = <bool>[].obs;
  var sessionDay = <String>[].obs;
  var indexClick = 0.obs;

  @override
  void onInit() {
    getHomeRoomTeacher();
    studentName.value =
        Get.find<ParentHomeController>().currentStudentProfile.value.fullName!;
    super.onInit();
  }

  getHomeRoomTeacher() async {
    var studentId =
        Get.find<ParentHomeController>().currentStudentProfile.value.id;
    _diligenceRepo.getHomeRoomTeacher(studentId).then((value) {
      if (value.state == Status.SUCCESS) {
        teacher.value = value.object!;
      }
    });
  }

  showListDate() {
    if (controllerdateStart.text != "" && controllerdateEnd.text != "") {
      isShowListDate.value = true;
    } else {
      isShowListDate.value = false;
    }
  }

  getStatus(status) {
    switch (status) {
      case true:
        return "TRUE";
      case false:
        return "FALSE";
    }
  }

  getListDayLeavingApplication() {
    if (controllerdateStart.text != "" && controllerdateEnd.text != "") {
      var startDate = DateFormat("dd/MM/yyyy").parse(controllerdateStart.text);
      var endDate = DateFormat("dd/MM/yyyy").parse(controllerdateEnd.text);
      listDayLeavingApplication.value = [];
      listLeavingDetail.value = [];
      for (int i = 0; i <= endDate.difference(startDate).inDays; i++) {
        listDayLeavingApplication.add(startDate.add(Duration(days: i)));
        listLeavingDetail.add(LeaveDetail(date: 0,sessionDay: "AM,PM"));
        listLeavingDetail[i].date = listDayLeavingApplication[i].millisecondsSinceEpoch;
        listLeavingDetail[i].sessionDay = "AM,PM";
      }
      listLeavingDetail.refresh();
      listDayLeavingApplication.refresh();
    }
  }

  checkBoxMorning(index) {
    indexClick.value = index;
    if(listLeavingDetail[indexClick.value].sessionDay?.contains("AM") == true){
      if(listLeavingDetail[indexClick.value].sessionDay?.contains("PM") == true){
        listLeavingDetail[indexClick.value].sessionDay = "PM";
      }else{
        listLeavingDetail[indexClick.value].sessionDay = "";
      }

    }else{
      if(listLeavingDetail[indexClick.value].sessionDay?.contains("PM") == true){
        listLeavingDetail[indexClick.value].sessionDay = "AM,PM";
      }else{
        listLeavingDetail[indexClick.value].sessionDay = "AM";
      }
    }
    listLeavingDetail.refresh();
  }

  checkBoxAfternoon(index) {
    indexClick.value = index;
    if(listLeavingDetail[indexClick.value].sessionDay?.contains("PM") == true){
      if(listLeavingDetail[indexClick.value].sessionDay?.contains("AM") == true){
        listLeavingDetail[indexClick.value].sessionDay = "AM";
      }else{
        listLeavingDetail[indexClick.value].sessionDay = "";
      }

    }else{
      if(listLeavingDetail[indexClick.value].sessionDay?.contains("AM") == true){
        listLeavingDetail[indexClick.value].sessionDay = "AM,PM";
      }else{
        listLeavingDetail[indexClick.value].sessionDay = "PM";
      }
    }
    listLeavingDetail.refresh();
  }

  createALeaveApplication() {
    var startDate = DateFormat("dd/MM/yyyy").parse(controllerdateStart.text);
    var endDate = DateFormat("dd/MM/yyyy").parse(controllerdateEnd.text);
    for (int i = 0; i <= endDate.difference(startDate).inDays; i++) {
      if (listLeavingDetail[i].sessionDay == "") {
        isConfirmCreate.add(false);
        AppUtils.shared.showToast("Vui lòng chọn đầy đủ thời gian");
      } else {
        isConfirmCreate.add(true);
      }
    }

    if (isConfirmCreate.contains(false) == false) {
      _diligenceRepo
          .createALeaveApplication(
              teacher.value.homeroomTeacher!.id!,
              Get.find<ParentHomeController>().currentStudentProfile.value.id!,
              DateFormat('dd/MM/yyyy hh:mm')
                  .parse("${controllerdateStart.value.text} 00:00")
                  .millisecondsSinceEpoch,
              DateFormat('dd/MM/yyyy hh:mm')
                  .parse("${controllerdateEnd.value.text} 23:59")
                  .millisecondsSinceEpoch,
              controllerReason.text.trim(),
              controllerNote.text.trim(),
              controllerAssure.text.trim(),
              listLeavingDetail)
          .then((value) {
        if (value.state == Status.SUCCESS) {
          AppUtils.shared.showToast("Tạo đơn thành công!");
          Get.back();
          Future.delayed(const Duration(seconds: 1), () {
            Get.find<ApprovalParentController>().onInit();
          });
        } else {
          AppUtils.shared.hideLoading();
          AppUtils.shared
              .snackbarError("Tạo đơn thất bại", value.message ?? "");
        }
      });
    }
  }

  setWeekDay(weekDay){
    switch(weekDay){
      case 8:
        return "Chủ Nhật";
      default:
        return "Thứ $weekDay";
    }
  }

}
