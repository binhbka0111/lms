import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import '../../../../../../../../commom/utils/global.dart';
import '../../../../../../../../commom/widget/text_field_custom.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../commom/utils/click_subject_teacher.dart';
import 'cancel_leave_application_controller.dart';



class CancelLeaveApplicationPage
    extends GetWidget<CancelLeaveApplicationController> {
  final controller = Get.put(CancelLeaveApplicationController());

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            elevation: 0,
            actions: [
              InkWell(
                onTap: () {
                  comeToHome();
                },
                child: const Icon(
                  Icons.home,
                  color: Colors.white,
                ),
              ),
              Padding(padding: EdgeInsets.only(right: 16.w))
            ],
            title: const Text(
              'Chi Tiết Đơn',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body: GestureDetector(
            onTap: () {
              dismissKeyboard();
            },
            child: Obx(() => Column(
              children: [
                Expanded(
                    child: SingleChildScrollView(
                      physics: const BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Visibility(
                              visible: controller.deatilNotifyLeavingApplication.value.html != null,
                              child: Container(
                                decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(6)),
                                margin: EdgeInsets.symmetric(horizontal: 8.w,vertical: 16.h),
                                padding: EdgeInsets.all(16.h),
                                child: Html(data: controller.deatilNotifyLeavingApplication.value.html??"",
                                ),
                              )),
                        ],
                      ),
                    )),
                Container(
                  decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(6)),
                  padding: EdgeInsets.symmetric(vertical: 8.h),
                  child:  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      OutlineBorderTextFormField(
                        enable: true,
                        focusNode: controller.focusFeedback,
                        iconPrefix: "",
                        iconSuffix: "",
                        state: StateType.DEFAULT,
                        labelText: "Lý do hủy (*)",
                        autofocus: false,
                        controller: controller.controllerFeedback,
                        helperText: "",
                        showHelperText: false,
                        ishowIconPrefix: false,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.text,
                        validation: (textToValidate) {
                          return controller
                              .getTempIFSCValidation(textToValidate);
                        },
                      ),
                      Padding(padding: EdgeInsets.only(top: 16.h)),
                     Container(
                       margin: EdgeInsets.symmetric(horizontal: 16.w),
                       child:  Row(
                           crossAxisAlignment: CrossAxisAlignment.center,
                           mainAxisAlignment: MainAxisAlignment.start,
                           children: [
                             SizedBox(
                               width: 80.w,
                               child: Text(
                                 "Trạng thái:",
                                 style: TextStyle(
                                     fontSize: 14.sp,
                                     color: const Color.fromRGBO(
                                         133, 133, 133, 1),
                                     fontWeight: FontWeight.w400),
                               ),
                             ),
                             Container(
                               padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                               decoration: BoxDecoration(
                                   color: controller.getColorBackgroundStatus(controller.deatilNotifyLeavingApplication.value.status),
                                   borderRadius: BorderRadius.circular(6)
                               ),
                               child: Text(
                                 "${controller.getStatus(controller.deatilNotifyLeavingApplication.value.status)??""}",
                                 style: TextStyle(
                                     fontSize: 14.sp,
                                     color: controller.getColorTextStatus(controller.deatilNotifyLeavingApplication.value.status),
                                     fontWeight: FontWeight.w400),
                               ),
                             ),
                             Expanded(child: Container()),
                           ]),
                     ),
                      Padding(padding: EdgeInsets.only(top: 16.h)),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 12.w),
                        color: Colors.white,
                        child:InkWell(
                          onTap: () {
                            if(controller.controllerFeedback.value.text.trim() == ""){
                              AppUtils.shared.showToast(
                                  "Vui lòng nhập lý do hủy đơn");
                            }else{
                              controller.cancelLeaveApplication();
                            }

                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 16.w, vertical: 6.h),
                            decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(6.r)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Hủy đơn",
                                  style: TextStyle(
                                      fontSize: 12.sp,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white),
                                ),
                                Padding(padding: EdgeInsets.only(right: 4.w)),
                                const Icon(Icons.clear,color: Colors.white,size: 20,)
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )),
          ),
        ));
  }
}
