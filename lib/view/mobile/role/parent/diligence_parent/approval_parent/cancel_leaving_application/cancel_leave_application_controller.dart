import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../../../../../commom/utils/app_utils.dart';
import '../../../../../../../data/base_service/api_response.dart';
import '../../../../../../../data/model/common/detail_notify_leaving_application.dart';
import '../../../../../../../data/model/common/leaving_application.dart';
import '../../../../../../../data/repository/diligence/diligence_repo.dart';
import '../../../../../../../data/repository/notification_repo/notification_repo.dart';
import '../approval_parent_controller.dart';

class CancelLeaveApplicationController extends GetxController{
  var focusFeedback = FocusNode();
  var controllerFeedback = TextEditingController();
  final DiligenceRepo _diligenceRepo = DiligenceRepo();

  var itemsLeavingApplication = ItemsLeavingApplication().obs;
  String getTempIFSCValidation(String text) {
    return text.length > 7 ? "This line is helper text" : "";
  }
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  var isHomeRoomTeacher = false.obs;
  var transId = "".obs;
  final NotificationRepo _notificationRepo = NotificationRepo();
  var deatilNotifyLeavingApplication = DetailNotifyLeavingApplication().obs;
  @override
  void onInit() {
    var tmpTransId = Get.arguments;
    if (tmpTransId != null) {
      transId.value = tmpTransId;
      getDetailNotify();
    }
    super.onInit();
  }


  getDetailNotify(){
    _notificationRepo.getDetailNotifyLeavingApplication(transId.value).then((value) {
      if (value.state == Status.SUCCESS) {
        deatilNotifyLeavingApplication.value = value.object!;
      }
    });
  }



  cancelLeaveApplication(){
    _diligenceRepo.cancelLeaveApplication(deatilNotifyLeavingApplication.value.id,controllerFeedback.value.text).then((value) {
      if (value.state == Status.SUCCESS) {
        AppUtils.shared.showToast("Hủy đơn thành công!");
        Get.back();
        Future.delayed(const Duration(seconds: 1), () {
          Get.find<ApprovalParentController>()
              .onInit();
        });
      }else {
        AppUtils.shared.hideLoading();
        AppUtils.shared
            .snackbarError("Hủy đơn thất bại", value.message ?? "");
      }
    });
  }


  getStatus(status){
    switch(status){
      case "CANCEL":
        return "Đã hủy";
      case "PENDING":
        return "Chờ duyệt";
      case "REFUSE":
        return "Từ chối";
      case "APPROVE":
        return "Đã duyệt";
    }
  }


  getColorTextStatus(status){
    switch(status){
      case "PENDING":
        return const Color.fromRGBO(253, 185, 36, 1);
      case "APPROVE":
        return const Color.fromRGBO(77, 197, 145, 1);
      case "REFUSE":
        return const Color.fromRGBO(255, 69, 89, 1);
      case "CANCEL":
        return const Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getColorBackgroundStatus(status){
    switch(status){
      case "PENDING":
        return const Color.fromRGBO(255, 243, 218, 1);
      case "APPROVE":
        return const Color.fromRGBO(192, 242, 220, 1);
      case "REFUSE":
        return const Color.fromRGBO(252, 211, 215, 1);
      case "CANCEL":
        return const Color.fromRGBO(252, 211, 215, 1);
    }
  }


}