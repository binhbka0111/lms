import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import '../../../../../../../../commom/utils/global.dart';
import '../../../../../../../commom/utils/click_subject_teacher.dart';
import 'detail_leave_application_controller.dart';
class DetailLeaveApplicationParentPage
    extends GetWidget<DetailLeaveApplicationParentController> {
  final controller = Get.put(DetailLeaveApplicationParentController());

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
          appBar: AppBar(
            backgroundColor: ColorUtils.PRIMARY_COLOR,
            elevation: 0,
            actions: [
              InkWell(
                onTap: () {
                  comeToHome();
                },
                child: const Icon(
                  Icons.home,
                  color: Colors.white,
                ),
              ),
              Padding(padding: EdgeInsets.only(right: 16.w))
            ],
            leading: InkWell(
              onTap: () {
                Get.back();
              },
              child: const Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
            title: Text(
              'Chi Tiết Đơn',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.sp,
                  fontFamily: 'static/Inter-Medium.ttf'),
            ),
          ),
          body: GestureDetector(
            onTap: () {
              dismissKeyboard();
            },
            child: Obx(() => Column(
              children: [
                Expanded(
                    child: SingleChildScrollView(
                      physics: const BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Visibility(
                              visible: controller.deatilNotifyLeavingApplication.value.html != null,
                              child: Container(
                                decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(6)),
                                margin: EdgeInsets.symmetric(horizontal: 16.w,vertical: 16.h),
                                padding: EdgeInsets.all(16.h),
                                child: Html(data: controller.deatilNotifyLeavingApplication.value.html??"",
                                ),
                              )),
                          Container(
                            decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(6)),
                            margin: EdgeInsets.symmetric(horizontal: 16.w,vertical: 8.h),
                            padding: EdgeInsets.all(16.h),
                            child:  Column(
                              children: [
                                Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: 80.w,
                                        child: Text(
                                          "Trạng thái:",
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              color: const Color.fromRGBO(
                                                  133, 133, 133, 1),
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 4),
                                        decoration: BoxDecoration(
                                            color: controller.getColorBackgroundStatus(controller.deatilNotifyLeavingApplication.value.status),
                                            borderRadius: BorderRadius.circular(6)
                                        ),
                                        child: Text(
                                          "${controller.getStatus(controller.deatilNotifyLeavingApplication.value.status)??""}",
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              color: controller.getColorTextStatus(controller.deatilNotifyLeavingApplication.value.status),
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                      Expanded(child: Container()),
                                    ]),
                                Padding(padding: EdgeInsets.only(top: 8.h)),
                                Visibility(
                                  visible: controller.deatilNotifyLeavingApplication.value.status == "APPROVE"||controller.deatilNotifyLeavingApplication.value.status == "REFUSE"||controller.deatilNotifyLeavingApplication.value.status == "CANCEL",
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: controller.getWidthFeedback(controller.deatilNotifyLeavingApplication.value.status??""),
                                        child: Text(
                                          controller.getTextFeedback(controller.deatilNotifyLeavingApplication.value.status)??"",
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              color: const Color.fromRGBO(
                                                  133, 133, 133, 1),
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                      Flexible(child: Visibility(visible: controller.deatilNotifyLeavingApplication.value.feedback != ""||controller.deatilNotifyLeavingApplication.value.feedback != null,
                                        child: Text(
                                          controller.deatilNotifyLeavingApplication.value.feedback??"",
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              color: const Color.fromRGBO(
                                                  133, 133, 133, 1),
                                              fontWeight: FontWeight.w400),
                                        ),))
                                    ]),)
                              ],
                            )
                            ),
                        ],
                      ),
                    )),

              ],
            )),
          ),
        );
  }
}
