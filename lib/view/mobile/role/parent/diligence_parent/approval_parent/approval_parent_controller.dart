import 'dart:ui';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/data/repository/diligence/diligence_repo.dart';
import '../../../../../../data/base_service/api_response.dart';
import '../../../../../../data/model/common/leaving_application.dart';
import '../../parent_home_controller.dart';



class ApprovalParentController extends GetxController {
  final DiligenceRepo _diligenceRepo = DiligenceRepo();
  var leavingApplication = LeavingApplication().obs;
  var itemsLeavingApplications = <ItemsLeavingApplication>[].obs;
  var outputDateFormat = DateFormat('dd/MM/yyyy');
  @override
  void onInit() {
    getListLeavingApplicationParent();
    super.onInit();
  }


  getListLeavingApplicationParent(){
    var studentId = Get.find<ParentHomeController>().currentStudentProfile.value.id;
    _diligenceRepo.getListLeavingApplication(studentId).then((value) {
      if (value.state == Status.SUCCESS) {
        itemsLeavingApplications.value = value.object!;
      }
    });
  }


  getStatus(status){
    switch(status){
      case "PENDING":
        return "Chờ duyệt";
      case "APPROVE":
        return "Phê duyệt";
      case "REFUSE":
        return "Từ chối";
      case "CANCEL":
        return "Đã hủy";
    }
  }

  getColorTextStatus(status){
    switch(status){
      case "PENDING":
        return Color.fromRGBO(253, 185, 36, 1);
      case "APPROVE":
        return Color.fromRGBO(77, 197, 145, 1);
      case "REFUSE":
        return Color.fromRGBO(255, 69, 89, 1);
      case "CANCEL":
        return Color.fromRGBO(255, 69, 89, 1);
    }
  }

  getColorBackgroundStatus(status){
    switch(status){
      case "PENDING":
        return Color.fromRGBO(255, 243, 218, 1);
      case "APPROVE":
        return Color.fromRGBO(192, 242, 220, 1);
      case "REFUSE":
        return Color.fromRGBO(252, 211, 215, 1);
      case "CANCEL":
        return Color.fromRGBO(252, 211, 215, 1);
    }
  }



  isShowEditAndDeleteLeavingApplication(status){
    switch(status){
      case "PENDING":
        return true;
      case "APPROVE":
        return false;
      case "REFUSE":
        return false;
        case "CANCEL":
      return false;

      default:
        return true;
    }
  }


  getImageStatus(status){
    switch(status){
      case "PENDING":
        return "";
      case "APPROVE":
        return "assets/images/image_approved.png";
      case "REFUSE":
        return "assets/images/image_rejected.png";
      case "CANCEL":
        return "";
    }
  }




  setTimeStartLeavingApplication(index){
    if(itemsLeavingApplications[index].fromDate == null){
      return "";
    }else{
      return outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(itemsLeavingApplications[index].fromDate!));
    }
  }

  setTimeEndLeavingApplication(index){
    if(itemsLeavingApplications[index].toDate == null ){
      return "";
    }else{
      return outputDateFormat.format(DateTime.fromMillisecondsSinceEpoch(itemsLeavingApplications[index].toDate!));
    }
  }

  setTimeLeavingApplication(index){
    if(itemsLeavingApplications[index].toDate == null || itemsLeavingApplications[index].fromDate == null){
      return "";
    }else{
      return " đến ";
    }
  }




}
