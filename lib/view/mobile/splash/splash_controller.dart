import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:get/get.dart';
import 'package:slova_lms/routes/app_pages.dart';
import '../../../commom/app_cache.dart';
import '../../../commom/utils/app_utils.dart';
import '../../../data/base_service/api_response.dart';
import '../../../data/repository/account/auth_repo.dart';

class SplashController extends GetxController {
  final AuthRepo _loginRepo = AuthRepo();
  var isReady = false.obs;


  @override
  void onInit() async {
    await checkLogged();
    super.onInit();
  }

  Future<void> checkLogged() async {
    AppCache appCache = AppCache();
    String fcmTokenCache = await appCache.getFcmToken;
    String? fcmTokenFirebase;
    try {
      fcmTokenFirebase = await FirebaseMessaging.instance.getToken();

      if (fcmTokenFirebase != null) {
        if (fcmTokenCache == fcmTokenFirebase) {
          // fcm token không thay đổi
          requestLogin(fcmTokenCache);
        } else {
          // fcm token thay đổi.
          await AppCache().saveFcmToken(fcmToken: fcmTokenFirebase);
          requestLogin(fcmTokenFirebase);
        }
      }
    } catch (e) {
      await checkLogged();
    }

  }


  requestLogin(fcmKey)async{
    String? userName = await AppCache().username ??"";
    String? password = await AppCache().password ??"";
    if (userName != "" && password != "") {
      await _loginRepo.requestToken(userName, password, true, fcmKey).then((value) {
        if (value.state == Status.SUCCESS) {
          var userType = value.object?.userType ?? "";
          AppCache().setUserType(userType);
          AppUtils.shared.showToast("Đăng nhập thành công!");
          AppCache().saveFcmToken(fcmToken: fcmKey);
          pushToHome();
        } else {
          AppUtils.shared.hideLoading();
          isReady.value = true;
          FlutterAppBadger.removeBadge();
        }
      });
    }else{
      isReady.value = true;
      FlutterAppBadger.removeBadge();
    }
  }






  pushToHome() {
    Get.toNamed(Routes.home);
  }

  pushToLogin() {
    Get.toNamed(Routes.login);
  }


}






