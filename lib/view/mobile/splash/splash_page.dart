import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/view/mobile/splash/splash_controller.dart';

class SplashPage extends StatelessWidget {
  @override
  final controller = Get.put(SplashController());

  SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
      resizeToAvoidBottomInset: true,
      body: controller.isReady.value ? Container(
        margin:
        EdgeInsets.only(top: 40.h, left: 24.w, right: 24.w, bottom: 24.h),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: Get.height*0.4,
              width: double.infinity,
              child: Image.asset('assets/images/img_Splat.png',fit: BoxFit.contain,),
            ),
            Container(
              margin: EdgeInsets.only(top: 56.h),
              width: double.infinity,
              child: const Text(
                'Đồng Hành Và Phát Triển Giáo Dục',
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 35,
                    color: Color.fromRGBO(26, 26, 26, 1),
                    fontFamily: 'assets/font/static/Inter-Medium.ttf',
                    fontWeight: FontWeight.w500),
              ),
            ),
            Container(
              margin:  EdgeInsets.only(top: 8.h),
              height: 35.h,
              width: double.infinity,
              child: const Text(
                'Hỗ trợ và giảm thiểu thời gian, công sức trong việc quá trình học tập của Học Sinh cho Giáo Viên, Phụ Huynh',
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Color.fromRGBO(133, 133, 133, 1),
                    fontSize: 12.5,
                    fontFamily: 'static/Inter-Regular.ttf'),
              ),
            ),
            const Spacer(),
            SizedBox(
              width: double.infinity,
              height: 40.h,
              child: ElevatedButton(
                  onPressed: () {
                    controller.pushToLogin();
                  },
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)),
                      backgroundColor: const Color.fromRGBO(248, 129, 37, 1)),
                  child: const Text(
                    'Đăng Nhập',
                    style: TextStyle(fontSize: 16),
                  )),
            ),
            SizedBox(
              height:24.h,
            )
          ],
        ),
      ):const Center(
          child: CircularProgressIndicator(
            color: Color.fromRGBO(248, 129, 37, 1),
          )),
    ));
  }
}
