import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/textstyle.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/message/search_message/message_personal/message_personal_controller.dart';

class ItemNewsWidget extends StatelessWidget {
  const ItemNewsWidget(
      {Key? key,
      required this.title,
      required this.content,
      required this.time,
      this.image,
      required this.icon,
        required this.url
      })
      : super(key: key);
  final String title;
  final String content;
  final String time;
  final String? image;
  final IconData icon;
  final String? url;

  @override
  Widget build(BuildContext context) {
    return (image == null)
        ? Card(
            margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 8.0.h),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Padding(
              padding: EdgeInsets.only(left: 16.0.w, right: 16.0.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 16.0.h,
                  ),
                  Text(
                    title,
                    style: TextStyleUtils.sizeText16Weight500()
                        ?.copyWith(color: ColorUtils.PRIMARY_COLOR),
                  ),
                  SizedBox(
                    height: 14.0.h,
                  ),
                  Text(
                    content,
                    style: TextStyleUtils.sizeText14Weight400(),
                  ),
                  SizedBox(
                    height: 14.0.h,
                  ),
                  Row(
                    children: [
                      Text(
                        time,
                        style: TextStyleUtils.sizeText12Weight500(),
                      ),
                      const Spacer(),
                      InkWell(
                        splashColor: Colors.transparent,
                        onTap: () => sharePost(),
                        child: Transform(
                          alignment: Alignment.center,
                          transform: Matrix4.rotationY(pi),
                          child: Icon(
                            icon,
                            size: 22.sp,
                            color: ColorUtils.PRIMARY_COLOR,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16.0.h,
                  ),
                ],
              ),
            ),
          )
        : Card(
            margin: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 8.0.h),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 16.0.w,
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          const SizedBox(height: 10,),
                          Text(
                            title,
                            style: TextStyleUtils.sizeText16Weight500()
                                ?.copyWith(color: ColorUtils.PRIMARY_COLOR),
                            softWrap: true,
                          ),
                          const SizedBox(
                            height: 9.0,
                          ),
                          Text(
                            content,
                            maxLines: 3,
                            style:
                                TextStyleUtils.sizeText14Weight400()?.copyWith(
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 16.0.h,
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(4.0),
                            child: Image.network(
                              image!,
                              height: 120.0.h,
                              width: 132.0.w,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 16.0.w,
                    ),
                  ],
                ),
                SizedBox(
                  height: 16.0.h,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 16.0.w,
                    ),
                    Text(
                      time,
                      style: TextStyleUtils.sizeText12Weight500(),
                    ),
                    const Spacer(),
                    InkWell(
                      splashColor: Colors.transparent,
                      onTap: () => sharePost(),
                      child: Transform(
                        alignment: Alignment.center,
                        transform: Matrix4.rotationY(pi),
                        child: Icon(
                          icon,
                          size: 22.sp,
                          color: ColorUtils.PRIMARY_COLOR,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 16.0.w,
                    ),
                  ],
                ),
                SizedBox(
                  height: 16.0.h,
                ),
              ],
            ),
          );
  }

  void sharePost(){
    Get.toNamed(Routes.searchMessagePage);
    if(Get.isRegistered<MessagePersonalController>()){
      Get.find<MessagePersonalController>().getListUserSearch(0, 50, '');
    }else{
      Get.put(MessagePersonalController());
      Get.find<MessagePersonalController>().getListUserSearch(0, 50, '');
    }
    Get.find<MessagePersonalController>().isSharePost.value = true;
    Get.find<MessagePersonalController>().contentSharePost.value = url!;
    Get.find<MessagePersonalController>().update();
  }
}
