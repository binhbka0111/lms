import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/textstyle.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/message/search_message/message_personal/message_personal_controller.dart';

class EventItemWidget extends StatelessWidget {
  const EventItemWidget({Key? key,
    required this.image,
    required this.title,
    required this.time,
    required this.author,
    this.icon,
    required this.url
    })
      : super(key: key);
  final String image;
  final String title;
  final String time;
  final String author;
  final IconData? icon;
  final String? url;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.3),
                offset: const Offset(0.3, 0.5)
            )
          ],
          color: Colors.white,
          borderRadius: const BorderRadius.all(Radius.circular(8)),
          border: Border.all(width: 0.4, color: Colors.grey.withOpacity(0.3))
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: SizedBox(
              height: 175,
              child: CachedNetworkImage(
                imageUrl: image.isNotEmpty ? image :'https://files.lms.xteldev.com/files/n1/static/img',
                imageBuilder: (context, imageProvider) =>
                    Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: const BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,),
                      ),
                    ),
                placeholder: (context, url) =>  Center(child: Image.asset('assets/images/loading.gif')),
                color: ColorUtils.PRIMARY_COLOR,
                errorWidget: (context, url, error) =>
                    Image.network( "https://files.lms.xteldev.com/files/n1/static/img",),
              ),
            ),
          ),
          const SizedBox(
            height: 10.0,
          ),
          Container(
            width: Get.width,
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title,
                  style: (Device.get().isTablet)? TextStyleUtils.sizeText14Weight500(): TextStyleUtils.sizeText13Weight500()
                      ?.copyWith(color: ColorUtils.COLOR_BLACK),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
                Row(
                  children: [
                    Text(
                      time,
                      style: TextStyleUtils.sizeText13Weight400(),
                    ),
                    Container(
                      width: 4.0.w,
                      height: 4.0.h,
                      margin: EdgeInsets.symmetric(horizontal: 8.0.w),
                      decoration: const BoxDecoration(
                        color: ColorUtils.COLOR_BLACK,
                        shape: BoxShape.circle,
                      ),
                    ),
                    Text(
                      author,
                      style: TextStyleUtils.sizeText12Weight400(),
                    ),
                    const Spacer(),
                    GestureDetector(
                      onTap: () {
                        sharePost();
                      },
                      child: Transform(
                        alignment: Alignment.center,
                        transform: Matrix4.rotationY(pi),
                        child: Icon(
                          icon,
                          size: 22.sp,
                          color: ColorUtils.PRIMARY_COLOR,
                        ),
                      ),
                    ),
                    const SizedBox(width: 10,),
                  ],
                )
              ],
            ),
          ),
          // const SizedBox(
          //   height: 8.0,
          // ),
        ],
      ),
    );
  }

  void sharePost(){
    Get.toNamed(Routes.searchMessagePage);
    if(Get.isRegistered<MessagePersonalController>()){
      Get.find<MessagePersonalController>().getListUserSearch(0, 50, '');
    }else{
      Get.put(MessagePersonalController());
      Get.find<MessagePersonalController>().getListUserSearch(0, 50, '');
    }
    Get.find<MessagePersonalController>().isSharePost.value = true;
    Get.find<MessagePersonalController>().contentSharePost.value = url!;
    Get.find<MessagePersonalController>().update();
  }
}

class CacheNetworkCustomEvent extends StatelessWidget {
  const CacheNetworkCustomEvent({Key? key, this.imageUrl}) : super(key: key);
  final String? imageUrl;
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl ??'https://files.lms.xteldev.com/files/n1/static/img',
      imageBuilder: (context, imageProvider) =>
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,),
            ),
          ),
      placeholder: (context, url) =>  Center(child: Image.asset('assets/images/loading.gif')),
      color: ColorUtils.PRIMARY_COLOR,
      errorWidget: (context, url, error) =>
      CacheNetworkCustomEvent(imageUrl: imageUrl),
    );
  }
}

