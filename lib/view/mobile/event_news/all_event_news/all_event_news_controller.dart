import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:slova_lms/data/model/common/news_model.dart';
import 'package:slova_lms/data/repository/news/news_repo.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/event_news/event_news_controller.dart';

class AllEventNewsController extends GetxController {
  final _repo = NewsRepo();
  final int pageSize = 20;
  final eventNewsController = Get.find<EventNewsController>();
  RxList list = <Item>[].obs;
  RxInt currentPage = 0.obs;

  RxBool isFirstLoading = true.obs;
  RxBool isNextPage = true.obs;
  RxBool isLoadMore = false.obs;
  RxBool isLoadingMore = false.obs;

  RxBool isLoading = true.obs;
  RxBool isAddLoading = false.obs;
  String categoryId = Get.arguments['categoryId'];
  String categoryName = Get.arguments['categoryName'];
  var scrollController = ScrollController().obs;
  String schoolId = Get.find<EventNewsController>().schoolId;
  final eventController = Get.find<EventNewsController>();

  @override
  void onInit() {
    // TODO: implement onInit
    loadData(currentPage.value, pageSize);

    scrollController.value.addListener(() {
        loadMore();
    });

    super.onInit();
  }

  /// Method load data to screen see all
  loadData(int currentPage, int pageSize) async {
    await _repo
        .getEventNews(currentPage, pageSize, schoolId, categoryId)
        .then((value) {
      if (value.object!.isEmpty) {
        isFirstLoading.value = false;
        isLoading.value = false;
      } else {
        list.addAll(value.object!);
        list.refresh();
        isFirstLoading.value = false;
        isLoading.value = false;
      }

      update();
    });
  }

  void onRefresh() async {
    currentPage.value = 0;
    list.clear();
    await loadData(0, pageSize);
  }

  /// Method load more
  void loadMore() async {
    if (isNextPage.value &&
        !isFirstLoading.value &&
        !isLoadMore.value &&
        !isLoadingMore.value &&
        scrollController.value.position.pixels >=
            scrollController.value.position.maxScrollExtent - 300) {
      isLoadingMore.value = true; // Set isLoadingMore to true
      currentPage.value += 1;

      await _repo
          .getEventNews(currentPage.value, pageSize, schoolId, categoryId)
          .then((value) {


        if (value.object != null) {
          list.addAll(value.object!);
          list.refresh();
        } else {
          isNextPage.value = false;
          isLoadMore.value = false;
        }
      });


      isLoadingMore.value = false; // Set isLoadingMore to false
      update();
    }
  }


  functionEvent(index) {
    Get.toNamed(Routes.detailNewsPage);
    eventController.getDetail(list[index].id);
    update();
  }
}
