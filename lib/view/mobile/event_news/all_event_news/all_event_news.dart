import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/view/mobile/event_news/all_event_news/all_event_news_controller.dart';
import 'package:slova_lms/view/mobile/event_news/widgets/item_news.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';

class AllEventNewsPage extends StatelessWidget {
  AllEventNewsPage({Key? key}) : super(key: key);
  final controller = Get.put(AllEventNewsController());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Text(
          (controller.categoryName),
          style: TextStyle(
              color: Colors.white,
              fontSize: 16.sp,
              fontFamily: 'static/Inter-Medium.ttf'),
        ),
      ),
      body: Obx(() {
        return (controller.isLoading.value == true)?
        const Center(child: CircularProgressIndicator(color: ColorUtils.PRIMARY_COLOR,),) :
        RefreshIndicator(
          onRefresh: () async{
            controller.onRefresh();
          },
          color: ColorUtils.PRIMARY_COLOR,
          child: Container(
            margin: EdgeInsets.only(top: 10.h),
            height: Get.height,
            child: ListView.builder(
              physics: const AlwaysScrollableScrollPhysics(),
              shrinkWrap: true,
              controller: controller.scrollController.value,
              itemCount: controller.list.length,

              itemBuilder: (context, index) {
                if (index < controller.list.length) {
                  return GestureDetector(
                    onTap: () {
                      checkClickFeature(
                        Get.find<HomeController>().userGroupByApp,
                            () => controller.functionEvent(index),
                        StringConstant.FEATURE_NEWS_DETAIL,
                      );
                    },
                    child: ItemNewsWidget(
                      title: controller.list[index].title,
                      content: controller.eventController.getContent(controller.list[index].excerpt),
                      time: controller.eventController.displayDate(controller.list[index].createdAt),
                      image: controller.list[index].image,
                      icon: Icons.reply,
                      url: controller.list[index].url,
                    ),
                  );
                }
              },
            ),
          ),
        );
      }),
    );
  }
}
