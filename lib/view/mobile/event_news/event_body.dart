import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/constants/string_constant.dart';
import 'package:slova_lms/commom/utils/check_user_group_permission.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/date_time_utils.dart';
import 'package:slova_lms/commom/utils/textstyle.dart';
import 'package:slova_lms/routes/app_pages.dart';
import 'package:slova_lms/view/mobile/event_news/event_news_controller.dart';
import 'package:slova_lms/view/mobile/event_news/widgets/item_event.dart';
import 'package:slova_lms/view/mobile/event_news/widgets/item_news.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
class EventBody extends StatelessWidget {
  const EventBody({Key? key,required this.isInHome}) : super(key: key);
  final bool isInHome;

  @override
  Widget build(BuildContext context) {
    EventNewsController controller;
    if(Get.isRegistered<EventNewsController>()){
      controller = Get.find<EventNewsController>();
    }else{
      controller = Get.put(EventNewsController());
    }

    return RefreshIndicator(
      color: ColorUtils.PRIMARY_COLOR,
      onRefresh: () async {
        await controller.onRefresh();
      },
      child: Obx(() {
        if (controller.isReady.value == true) {
          if (controller.checkData.value == false) {
            return const Center(child: Text(''));
          } else {
            if ((controller.categories.isNotEmpty)) {
              if ((controller.data.isNotEmpty)) {
                return SingleChildScrollView(
                  physics: isInHome ? const NeverScrollableScrollPhysics(): null,
                  child: Column(
                    children: controller.data.entries.map((entry) {
                      if ((entry.value != null)) {
                        if(controller.data.length < 2) {
                          return SizedBox(
                            height: Get.height,
                            child: Column(
                              children: [
                                const SizedBox(height: 20,),
                                Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: 16.h,
                                    ),
                                    Text(
                                      controller.categories.firstWhere((element) => (element.name == entry.key)).name,
                                      style: TextStyleUtils
                                          .sizeText16Weight500()
                                          ?.copyWith(
                                          color: const Color(
                                              0xFFB1B1B1)),
                                    ),
                                    const Spacer(),
                                    GestureDetector(
                                      onTap: () {
                                        Get.toNamed(
                                            Routes.allEventNewsPage,
                                            arguments: {
                                              'categoryId': controller
                                                  .categories
                                                  .firstWhere((element) => (element.name == entry.key)).id,
                                              'categoryName': controller
                                                  .categories
                                                  .firstWhere((element) => (element.name == entry.key)).code
                                            });
                                      },
                                      child: Text(
                                        'Xem Tất Cả',
                                        style: TextStyleUtils
                                            .sizeText14Weight400()
                                            ?.copyWith(
                                            color: ColorUtils
                                                .PRIMARY_COLOR),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 16.0,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 16.0.h,
                                ),
                                Expanded(
                                  child: ListView.builder(
                                    physics:
                                    isInHome? const NeverScrollableScrollPhysics():const AlwaysScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    scrollDirection: Axis.vertical,
                                    itemCount: entry.value.length,
                                    itemBuilder: (context, index) {
                                      return GestureDetector(
                                        onTap: () {
                                          checkClickFeature(Get.find<HomeController>().userGroupByApp,
                                              () {
                                                controller.getDetail(
                                                    entry.value[index].id);
                                                controller.update();
                                                Get.toNamed(
                                                    Routes.detailNewsPage,
                                                    arguments: entry.value[index].id);
                                              }, StringConstant.FEATURE_NEWS_DETAIL);
                                        },
                                        child: ItemNewsWidget(
                                          title:
                                          entry.value[index].title,
                                          content: entry.value[index].excerpt,
                                          time: controller.displayDate(entry.value[index].createdAt),
                                          icon: Icons.reply,
                                          image:
                                          entry.value[index].image, url: entry.value[index].url,
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          );
                        } else if ((entry.key) == controller.data.keys.toList()[0]) {
                          return SizedBox(
                            width: double.infinity,
                            child: Column(
                              children: [
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  children: [
                                    const SizedBox(
                                      width: 16,
                                    ),
                                    Text(
                                      controller.categories.firstWhere((element) => (element.name == entry.key)).name,
                                      style: TextStyleUtils.sizeText14Weight500()?.copyWith(color: const Color(0xFFB1B1B1)),
                                    ),
                                    const Spacer(),
                                    GestureDetector(
                                  onTap: () {
                                    Get.toNamed(Routes.allEventNewsPage,
                                        arguments: {
                                          'categoryId': controller.categories.firstWhere((element) => (element.name == entry.key)).id,
                                          'categoryName': controller.categories.firstWhere((element) => (element.name == entry.key)).name
                                        });
                                  },
                                  child: Text(
                                    'Xem Tất Cả',
                                    style: TextStyleUtils
                                        .sizeText12Weight400()
                                        ?.copyWith(
                                        color:
                                        ColorUtils.PRIMARY_COLOR),
                                  ),
                                ),
                                    const SizedBox(
                                      width: 16.0,
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 16.0,
                                ),
                                SizedBox(
                                  height: 278,
                                  child: ListView.builder(
                                    physics: const BouncingScrollPhysics(),
                                    scrollDirection: Axis.horizontal,
                                    itemCount: entry.value.length,
                                    itemBuilder: (context, index) {
                                      return GestureDetector(
                                        onTap: () {
                                          checkClickFeature(Get.find<HomeController>().userGroupByApp,
                                                  () {
                                                    Get.toNamed(Routes.detailNewsPage);
                                                    controller.getDetail(
                                                        entry.value[index].id);
                                                    controller.update();
                                              }, StringConstant.FEATURE_NEWS_DETAIL);
                                        },
                                        child: Container(
                                          width: !Device.get().isTablet ? Get.width: Get.width / 2 - 16,
                                          margin: const EdgeInsets.only(
                                              left: 16.0),
                                          child: EventItemWidget(
                                            image: entry.value[index].image ?? '',
                                            title: entry.value[index].title,
                                            time: DateTimeUtils
                                                .convertLongToStringTime(
                                                entry.value[index]
                                                    .createdAt,
                                                formatTime:
                                                '${DateTimeUtils.HH_MM} a'),
                                            author: entry.value[index]
                                                .createdBy.fullName,
                                            icon: Icons.reply,
                                            url: entry.value[index].url,
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          );
                        } else {
                          return SizedBox(
                            child: Column(
                              children: [
                                const SizedBox(height: 20,),
                                Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: 16.h,
                                    ),
                                    Text(
                                      controller.categories
                                          .firstWhere((element) =>
                                      (element.name ==
                                          entry.key))
                                          .name,
                                      style: TextStyleUtils
                                          .sizeText14Weight500()
                                          ?.copyWith(
                                          color: const Color(
                                              0xFFB1B1B1)),
                                    ),
                                    const Spacer(),
                                    GestureDetector(
                             onTap: () {
                               Get.toNamed(
                                   Routes.allEventNewsPage,
                                   arguments: {
                                     'categoryId': controller.categories.firstWhere((element) => (element.name == entry.key)).id,
                                     'categoryName': controller.categories.firstWhere((element) => (element.name == entry.key)).name
                                   });
                             },
                             child: Text(
                               'Xem Tất Cả',
                               style: TextStyleUtils
                                   .sizeText12Weight400()
                                   ?.copyWith(
                                   color: ColorUtils
                                       .PRIMARY_COLOR),
                             ),
                           ),
                                    const SizedBox(
                                      width: 16.0,
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 12,),
                                (entry.value.length <= 3) ?
                                ListView.builder(
                                  physics:
                                  const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  scrollDirection: Axis.vertical,
                                  itemCount: entry.value.length,
                                  itemBuilder: (context, index) {
                                    return GestureDetector(
                                      onTap: () {
                                        controller.getDetail(
                                            entry.value[index].id);
                                        controller.update();
                                        Get.toNamed(
                                            Routes.detailNewsPage,
                                            arguments: entry
                                                .value[index].id);
                                      },
                                      child: ItemNewsWidget(
                                        title:
                                        entry.value[index].title,
                                        content: entry
                                            .value[index].excerpt,
                                        time: controller.displayDate(
                                            entry.value[index]
                                                .createdAt),
                                        icon: Icons.reply,
                                        image:
                                        entry.value[index].image, url: entry.value[index].url,
                                      ),
                                    );
                                  },
                                ):
                                ListView.builder(
                                  physics:
                                  const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  scrollDirection: Axis.vertical,
                                  itemCount: 3,
                                  itemBuilder: (context, index) {
                                    return GestureDetector(
                                      onTap: () {
                                        checkClickFeature(Get.find<HomeController>().userGroupByApp,
                                                () {
                                                controller.getDetail(
                                                    entry.value[index].id);
                                                controller.update();
                                                Get.toNamed(
                                                    Routes.detailNewsPage,
                                                    arguments: entry
                                                        .value[index].id);
                                        }, StringConstant.FEATURE_NEWS_DETAIL);
                                      },
                                      child: ItemNewsWidget(
                                        title:
                                        entry.value[index].title,
                                        content: entry
                                            .value[index].excerpt,
                                        time: controller.displayDate(
                                            entry.value[index]
                                                .createdAt),
                                        icon: Icons.reply,
                                        image:
                                        entry.value[index].image, url: entry
                                          .value[index].url,
                                      ),
                                    );
                                  },
                                ),
                              ],
                            ),
                          );
                        }
                      } else {
                        return const SizedBox();
                      }
                    }).toList(),
                  ),
                );
              } else {
                return const SizedBox();
              }
            } else {
              return const Center(
                child: Text('Không có dữ liệu!'),
              );
            }
          }
        } else {
          return const Center(
            child: SizedBox(),
          );
        }
      }),
    );
  }
}
