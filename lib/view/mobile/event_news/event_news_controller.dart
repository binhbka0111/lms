import 'package:get/get.dart';
import 'package:html/parser.dart';
import 'package:intl/intl.dart';
import 'package:slova_lms/commom/utils/date_time_utils.dart';
import 'package:slova_lms/data/model/common/news_model.dart';
import 'package:slova_lms/data/repository/news/news_repo.dart';
import 'package:slova_lms/data/repository/news_category_repo/news_category_repo.dart';

class EventNewsController extends GetxController {
  final _repo = NewsRepo();
  final _categoryRepo = NewsCategoryRepo();
  final int pageSize = 20;
  String schoolId ='';
  RxBool checkData = true.obs;

  /// categories
  RxList categories = [].obs;
  int currentPageCategory = 0;

  RxMap data = <String, dynamic>{}.obs;

  RxBool isReady = false.obs;

  /// object detail
  var itemDetail = Item().obs;
  RxBool isReadyDetail = false.obs;

  /// Method refresh
  onRefresh() async{
    isReady.value = false;
    categories.clear();
    data.clear();
    await getData(0, pageSize);
  }

  void refreshDetail() async{
    await getDetail(itemDetail.value.id!);
  }

  void setSchoolId(String value){
    schoolId  = value;
    update;
  }

  /// todo: Method get data
  getData(int currentPage, int pageSize) async {
    _categoryRepo.getNewsCategory().then((value) {
      value;
      if (value.object == null) {
        checkData.value == false;
        isReady.value = true;
      } else {
        categories.value = value.object!.toList();

        if (categories.isEmpty) {
          isReady.value = true;
        }

        for (var element in categories) {
          _repo
              .getEventNews(currentPage, pageSize, schoolId, element.id)
              .then((value) {
            if(value.object != null ) {
              value.object!
                  .sort((a, b) => b.createdAt!.compareTo(a.createdAt as num));
              data[element.name] = value.object!
                  .where((element) => element.isActive == true)
                  .toList();
            }
          });

          isReady.value = true;
        }
      }
    });
  }

  /// Method get detail item by id
  getDetail(String id) async {
    await _repo.getDetailItem(id).then((value) {
      itemDetail.value = value.object!;

      isReadyDetail.value = true;
    });
  }

  /// get content from string html
  getContent(String content) {
    return parse(content).documentElement?.text;
  }

  /// Method display date time
  displayDate(int millis) {
    String strTime = DateTimeUtils.convertLongToStringTime(millis, formatTime: 'dd/MM/yyyy HH:mm:ss');
    DateFormat format = DateFormat('dd/MM/yyyy HH:mm:ss');
    DateTime dateTime = format.parse(strTime);
    return DateTimeUtils.convertToAgo(dateTime);
  }
}
