import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:slova_lms/commom/utils/color_utils.dart';
import 'package:slova_lms/commom/utils/open_url.dart';
import 'package:slova_lms/commom/utils/textstyle.dart';
import 'package:slova_lms/commom/widget/cache_network_custom.dart';
import 'package:slova_lms/commom/utils/click_subject_teacher.dart';
import 'package:slova_lms/view/mobile/event_news/event_news_controller.dart';

class DetailNewsPage extends StatelessWidget {
  DetailNewsPage({Key? key}) : super(key: key);
  final controller = Get.find<EventNewsController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {

              comeToHome();

            },
            icon: const Icon(Icons.home),
            color: Colors.white,
          ),
        ],
        backgroundColor: ColorUtils.PRIMARY_COLOR,
        title: Obx(() {
          if (controller.itemDetail.value.id == '') {
            return const Text('');
          } else {
            return (controller.itemDetail.value.newsCategory?.name == null)
                ? const Text('')
                : Text(
                    controller.itemDetail.value.newsCategory!.name!,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.sp,
                        fontFamily: 'static/Inter-Medium.ttf'),
                  );
          }
        }),
      ),
      body: Obx(() {
        if (controller.isReadyDetail.value == true) {
          if (controller.itemDetail.value != null) {
            return RefreshIndicator(
              color: ColorUtils.PRIMARY_COLOR,
              onRefresh: () async {
                 controller.refreshDetail();
              },
              child: SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: (controller.itemDetail.value.id == null)
                    ? const SizedBox()
                    : Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                '${controller.itemDetail.value.title}',
                                style: TextStyleUtils.sizeText16Weight500()!.copyWith(color: Colors.black),
                              ),
                            ),
                            const SizedBox(height: 10,),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                '${controller.itemDetail.value.excerpt}',
                                style: TextStyleUtils.sizeText13Weight300()!.copyWith(color: Colors.black),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            const SizedBox(height: 10,),
                            (controller.itemDetail.value.image == null)
                                ? const SizedBox()
                                : Container(
                                    constraints: BoxConstraints(
                                      maxHeight: 150.h,
                                    ),
                                    color: Colors.white,
                                    width: Get.width,
                                    child: CacheNetWorkCustom(
                                      urlImage:
                                          '${controller.itemDetail.value.image}',
                                      radiusParam: 8,
                                    ),
                                  ),
                            SizedBox(
                              height: 16.0.h,
                            ),
                            (controller.itemDetail.value.content!.isNotEmpty)?
                            Html(
                              anchorKey: GlobalKey(),
                              data: controller.itemDetail.value.content,
                              style: {
                                "table": Style(
                                  backgroundColor: const Color.fromARGB(
                                      0x50, 0xee, 0xee, 0xee),
                                ),
                                "tr": Style(
                                  border: const Border(
                                      bottom: BorderSide(color: Colors.grey)),
                                ),
                                "th": Style(
                                  backgroundColor: Colors.grey,
                                ),
                                "td": Style(
                                  alignment: Alignment.topLeft,
                                ),
                                'h5': Style(
                                    maxLines: 2,
                                    textOverflow: TextOverflow.ellipsis,
                                    fontSize: FontSize.medium),
                                'h4': Style(fontSize: FontSize.large),
                                'p': Style(fontSize: FontSize.large),
                                'a': Style(
                                    fontSize: FontSize.large,
                                    color: ColorUtils.COLOR_PRIMARY),
                              },

                              onCssParseError: (css, messages) {
                                debugPrint("css that errored: $css");
                                debugPrint("error messages:");
                                for (var element in messages) {
                                  debugPrint(element.toString());
                                }
                                return '';
                              },
                            ) :
                            const SizedBox(),
                          ],
                        ),
                      ),
              ),
            );
          } else {
            return const Center(
              child: Text('Không có dữ liệu bài viết'),
            );
          }
        } else {
          return const Center(
            child: SizedBox(),
          );
        }
      }),
    );
  }
}
