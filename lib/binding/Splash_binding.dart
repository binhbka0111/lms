import 'package:get/get.dart';
import 'package:slova_lms/view/mobile/splash/splash_controller.dart';

class SplashBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(SplashController());
  }
}
