import 'package:get/get.dart';
import 'package:slova_lms/commom/connectivity_service.dart';
import 'package:slova_lms/view/mobile/screen_config/screen_config_controller.dart';

import '../notification/notification_controller.dart';
import '../view/mobile/lifecycle_controller.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ConnectivityService());
    Get.put(NotifyController());
    Get.put(ScreenConfigController());
    Get.put(LifeCycleController());
  }
}
