import 'package:get/get.dart';
import 'package:slova_lms/view/mobile/home/home_controller.dart';
import 'package:slova_lms/view/mobile/schedule/schedule_controller.dart';

import '../commom/app_cache.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    if(AppCache().userType != "MANAGER"){
      Get.put(ScheduleController());
    }
    Get.put(HomeController());

  }
}
